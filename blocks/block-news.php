<?
if (!defined('BLOCK_FILE')) {
 Header("Location: ../index.php");
 exit;
}

global $CURUSER, $tracker_lang;

$blocktitle = $tracker_lang['news'].(get_user_class() >= UC_ADMINISTRATOR ? " - [<a class=\"altlink_white\" href=\"news.php\"><b>".$tracker_lang['create']."</b></a>]" : "").($CURUSER ? " - [<a class=\"altlink_white\" href=\"newsarchive.php\"><b>".$tracker_lang['news_olders']."</b></a>]":"");


if (!$CURUSER){
$cacheStatFile = "block-news_guest"; 
$expire = 180*60; // 180  minutes 
} elseif ($CURUSER && get_user_class() < UC_ADMINISTRATOR){
$cacheStatFile = "block-news_users"; 
$expire = 60*60; // 60 minutes 
} else {
$cacheStatFile = "block-news_admins"; 
$expire = 30*60; // 30 minutes 
}

$filecache = file_query("", $cache = array("type" => "disk", "file" => $cacheStatFile, "time" => $expire, "action" => "get"));


if ($filecache <> false) {
$content = $filecache; 
} else {


$resource = sql_query("SELECT *, (SELECT COUNT(*) FROM comments WHERE news = news.id) AS comme FROM news ORDER BY added DESC LIMIT 6") or sqlerr(__FILE__, __LINE__);


if (mysql_num_rows($resource)) {

    $content .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\">";
    $show=true;
    while($array = mysql_fetch_array($resource)) {

   list ($data,$time) = explode(" ", $array["added"]);


if ($array['added']>get_date_time(gmtime() - 86400*2) && $show==true)// 2 ���
{
$content .=format_comment($array['body']);
$show=false;
}
else
{
$content .="<div class=\"spoiler-wrap\" id=\"".$array["id"]."\"><div class=\"spoiler-head folded clickable\">".$data.": ".$array['subject']."</div><div class=\"spoiler-body\" style=\"display: none;\">".format_comment($array['body'])." </div></div>";
}


   if ($CURUSER){
    $content .="<div align=\"center\"><b>".$tracker_lang['commens']."</b>: ".$array["comme"]."</div><div align=\"right\">".(get_user_class() >= UC_ADMINISTRATOR ? "<b>[</b><a href=\"news.php?action=edit&newsid=".$array['id']."&returnto=".htmlentities($_SERVER['PHP_SELF'])."\">".$tracker_lang['edit']."</a><b>]</b> - <b>[</b><a href=\"news.php?action=delete&newsid=".$array['id']."&returnto=".htmlentities($_SERVER['PHP_SELF'])."\">".$tracker_lang['delete']."</a><b>]</b> - ":"")."
	 <b>[</b><a href=\"newsoverview.php?id=".$array['id']."\">".$tracker_lang['commenting']."</a><b>]</b></div>";
    }
	else 
	$content .="<br />";

	}

	$content .= "</table>\n";
} else {
	$content .= "<table class=\"main\" align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\"><tr><td>";
	$content .= "<div align=\"center\"><h3>".$tracker_lang['no_news']."</h3></div>\n";
	$content .= "</td></tr></table>";
}



file_query($content, $cache = array("type" => "disk", "file" => $cacheStatFile, "time" => $expire, "action" => "set"));
}

?>