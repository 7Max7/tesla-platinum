<?
require_once("include/bittorrent.php");

dbconn(false);
loggedinorreturn();

if (get_user_class() < UC_SYSOP){
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

accessadministration();

if ($_SERVER["REQUEST_METHOD"] == "POST"){

$image = htmlspecialchars_uni(isset($_POST["image"]) ? trim($_POST["image"]):"");
$descr = htmlspecialchars_uni(isset($_POST["descr"]) ? trim($_POST["descr"]):"");
$url = htmlspecialchars_uni(isset($_POST["url"]) ? trim($_POST["url"]):"");

$purl = parse_url($url);
if (!empty($purl["host"]))
$purl = (!empty($purl["scheme"]) ? $purl["scheme"]."://":"http://")."".$purl["host"].(!empty($purl["path"]) ? $purl["path"]:"/").(!empty($purl["query"]) ? "?".$purl["query"]:"");

if (empty($purl) || empty($purl["host"]) || empty($url))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['friend_urlban']);

if (!preg_match('#^((http)|(https):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['my_website']);

if (empty($_POST["descr"]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['description']);

sql_query("INSERT INTO friendsblock (added, image, descr, url, visible) VALUES (".sqlesc(get_date_time()).", ".sqlesc($image).", ".sqlesc($descr).", ".sqlesc($purl).", ".sqlesc(($_POST["visible"] == "yes" ? "yes":"no")).")") or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER["username"]." ������� ����� ".($descr)." (".$url.")", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "other");

unsql_cache("block-friends");

header("Location: friensblockadd.php");
die;
}

$action = (isset($_GET["action"]) ? (string) $_GET["action"]:"");
if (empty($action))
$action = (isset($_POST["action"]) ? (string) $_POST["action"]:"");

if ($action == 'delete' && isset($_GET["id"])){

$id = (!empty($_GET["id"]) ? intval($_GET["id"]):"");

if (!is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$query = sql_query("SELECT descr, url FROM friendsblock WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$query_view = mysql_fetch_array($query);

$descr = $query_view["descr"];
$url = $query_view["url"];

if (!isset($_GET["sure"]))
stderr($tracker_lang['delete'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $tracker_lang['friend_urlban'].": <strong>".$url."</strong>", "friensblockadd.php?action=delete&id=".$id."&sure=1"));

sql_query("DELETE FROM friendsblock WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER["username"]." ������ ����� ".$descr." (".$url.")", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "other");

unsql_cache("block-friends");

header("Location: friensblockadd.php");
die;

}

if ($action == 'vi' && isset($_GET["id"])){

$id = (!empty($_GET["id"]) ? intval($_GET["id"]):0);

sql_query("UPDATE friendsblock SET visible = ".sqlesc(($_GET["visible"] == "yes" ? "yes":"no"))." WHERE visible = ".sqlesc(($_GET["visible"] <> "yes" ? "yes":"no"))." AND id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("block-friends");

header("Location: friensblockadd.php");
die;

}

if ($action == 'edit'){

$id = (int) $_GET["id"];

if (!is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$res = sql_query("SELECT * FROM friendsblock WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($res) != 1)
stderr($tracker_lang['error'], $tracker_lang['no_data']);

$arr = mysql_fetch_array($res);

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

$image = htmlspecialchars_uni(isset($_POST["image"]) ? trim($_POST["image"]):"");
$descr = htmlspecialchars_uni(isset($_POST["descr"]) ? trim($_POST["descr"]):"");
$url = htmlspecialchars_uni(isset($_POST["url"]) ? trim($_POST["url"]):"");

$purl = parse_url($url);
if (!empty($purl["host"]))
$purl = (!empty($purl["scheme"]) ? $purl["scheme"]."://":"http://")."".$purl["host"].(!empty($purl["path"]) ? $purl["path"]:"/").(!empty($purl["query"]) ? "?".$purl["query"]:"");

if (empty($purl) || empty($purl["host"]) || empty($url))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['friend_urlban']);

if (!preg_match('#^((http)|(https):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['my_website']);

if (empty($_POST["descr"]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['description']);

sql_query("UPDATE friendsblock SET image = ".sqlesc($image).", descr = ".sqlesc($descr).", url = ".sqlesc($purl).", visible = ".sqlesc($_POST["visible"] == "yes"? "yes":"no")." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("block-friends");

header("Location: friensblockadd.php");
die;

} else {

stdhead($tracker_lang['add_my_friends']);

echo ("<form method=\"post\" action=\"friensblockadd.php?action=edit&id=".$id."\">");
echo ("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">");

echo ("<tr><td colspan=\"5\" class=\"colhead\" align=\"left\">".$tracker_lang['editing']."</td></tr>");

echo ("<tr><td class=\"b\"><b>".$tracker_lang['friend_urlban']."</b>: </td><td colspan=\"2\" class=\"a\" align=\"left\"><input type=\"text\" size=\"90\" name=\"image\" value=\"".htmlspecialchars_uni($arr["image"])."\" /> ".sprintf($tracker_lang['max_n_limit'], 1024)."</tr>");

echo ("<tr><td class=\"a\"><b>".$tracker_lang['description']."</b>: </td><td colspan=\"2\" class=\"b\" align=\"left\"><textarea name=\"descr\" rows=\"7\" cols=\"80\">".htmlspecialchars_uni($arr["descr"])."</textarea> <br />".sprintf($tracker_lang['max_n_limit'], 2048)."</tr>");

echo ("<tr><td class=\"b\"><b>".$tracker_lang['my_website']."</b>: </td><td colspan=\"2\" class=\"a\" align=\"left\"><input type=\"text\" size=\"90\" name=\"url\" value=\"".htmlspecialchars_uni($arr["url"])."\"> ".sprintf($tracker_lang['max_n_limit'], 1024)."</tr>");

echo ("<tr><td class=\"a\"><b>".$tracker_lang['who_viewblock']."</b>: </td><td colspan=\"2\" class=\"b\" align=\"left\"><input type=\"radio\" name=\"visible\" ".($arr["visible"] == "yes" ? " checked" : "")." value=\"yes\" />".$tracker_lang['yes']." <input type=\"radio\" name=\"visible\" ".($arr["visible"] == "no" ? "checked" : "")." value=\"no\">".$tracker_lang['no']."</td></tr>");

echo ("<tr><td class=\"a\" colspan=\"5\" align=\"left\"><input type=\"hidden\" name=\"action\" value=\"edit\" /> <input type=\"submit\" value=\"".$tracker_lang['edit']."\" class=\"btn\" /></td></tr>");

echo ("</form></table>");

stdfoot();
die;

}

header("Location: friensblockadd.php");
die;
}

stdhead($tracker_lang['add_my_friends']);

echo "<form action=\"friensblockadd.php\" method=\"post\">";
echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td align=\"center\" colspan=\"2\" class=\"colhead\">".$tracker_lang['add_my_friends']."</td></tr>";
echo "<tr><td align=\"left\" class=\"b\">".$tracker_lang['friend_urlban']."</td><td class=\"a\"><input type=\"text\" name=\"image\" size=\"80\" /> ".sprintf($tracker_lang['max_n_limit'], 1024)."</td></tr>";
echo "<tr><td align=\"left\" class=\"b\">".$tracker_lang['description']."</td><td class=\"a\"><textarea name=\"descr\" rows=\"7\" cols=\"80\"></textarea> <br />".sprintf($tracker_lang['max_n_limit'], 2048)."</td></tr>";
echo "<tr><td align=\"left\" class=\"b\">".$tracker_lang['my_website']."</td><td class=\"a\"><input type=\"text\" name=\"url\" size=\"80\" /> ".sprintf($tracker_lang['max_n_limit'], 1024)."</td></tr>";
echo "<tr><td align=\"left\" class=\"b\">".$tracker_lang['who_viewblock']."</td><td class=\"a\"><input type=\"radio\" name=\"visible\" checked value=\"yes\">".$tracker_lang['yes']." <input type=\"radio\" name=\"visible\" value=\"no\">".$tracker_lang['no']."</td></tr>";
echo "<tr><td align=\"center\" class=\"a\" colspan=\"2\"><input class=\"btn\" type=\"submit\" value=\"".$tracker_lang['act_add']."\" /></td></tr>";

echo "</table>";
echo "</form><br />";



$count = get_row_count("friendsblock");
list ($pagertop, $pagerbottom, $limit) = pager(20, $count, "friensblockadd.php?");


echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"100%\">";

if (empty($count)){

echo "<tr><td colspan=\"6\">".$tracker_lang['no_data']."</td></tr>";
echo "</table>";
stdfoot();
die;

}

echo "<tr><td colspan=\"6\">".$pagertop."</td></tr>";

echo "<tr>
<td class=\"a\">".$tracker_lang['friend_urlban']."</td>
<td class=\"a\" align=\"left\">".$tracker_lang['description']."</td>
<td class=\"a\">".$tracker_lang['my_website']." / ".$tracker_lang['clock']." / ".$tracker_lang['who_viewblock']."</td>
<td class=\"a\" width=\"10%\">".$tracker_lang['action']."</td>
</tr>";

$res = sql_query("SELECT * FROM friendsblock ORDER BY added DESC ".$limit) or sqlerr(__FILE__, __LINE__);

$num = 0;
while ($arr = mysql_fetch_assoc($res)) {

if ($num%2==0){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

$url = htmlspecialchars_uni($arr["url"]);

$p_se = parse_url($url);

$count_ref = number_format(get_row_count("referrers", "WHERE parse_url = ".sqlesc($p_se["host"])." OR parse_url = ".sqlesc($url)));
$count_away = number_format(get_row_count("reaway", "WHERE parse_url = ".sqlesc($p_se["host"])." OR parse_url = ".sqlesc($url)));

echo "<tr>";

echo "<td ".$cl2." valign=\"top\"><img title=\"".strip_tags($arr["descr"])."\" width=\"88px\" height=\"31px\" src=\"".$arr["image"]."\" border=\"0\" /></td>";

echo "<td ".$cl1." style=\"margin: 0; padding: 0;\" align=\"left\">

<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"100%\">
<tr><td class=\"b\" align=\"left\" colspan=\"2\">".htmlspecialchars_uni($arr["descr"])."</td></tr>
<tr>
<td ".$cl1." align=\"center\">".$tracker_lang['tabs_red'].": ".$count_away."</td>
<td ".$cl2." align=\"center\">".$tracker_lang['referrals'].": ".$count_ref."</td>
</tr>
</table>

</td>";

echo "<td ".$cl2." style=\"margin: 0; padding: 0;\" align=\"left\" width=\"30%\">

<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"100%\">
<tr><td class=\"b\" align=\"left\" colspan=\"2\"><a href=\"".$url."\">".$url."</a></td></tr>
<tr>
<td ".$cl1." width=\"50%\" align=\"center\">".$arr["added"]."</td>
<td ".$cl2." width=\"50%\" align=\"center\">".($arr["visible"] == "yes" ? $tracker_lang['yes']:$tracker_lang['no'])."</td>
</tr>
</table>

</td>";

echo "<td ".$cl1." width=\"5%\" align=\"center\">

".($arr["visible"] == "yes" ? "<a title=\"".$tracker_lang['deny']."\" href=\"friensblockadd.php?action=vi&visible=no&id=".$arr['id']."\"><img title=\"".$tracker_lang['deny']."\" src=\"./pic/admin/inactive.gif\" />":"<a title=\"".$tracker_lang['allow']."\" href=\"friensblockadd.php?action=vi&visible=yes&id=".$arr['id']."\"><img title=\"".$tracker_lang['allow']."\" src=\"./pic/admin/activate.gif\" />")."</a>

<a href=\"friensblockadd.php?action=edit&id=".$arr['id']."\"><img title=\"".$tracker_lang['edit']."\" src=\"./pic/admin/edit.gif\" /></a> 
<a href=\"friensblockadd.php?action=delete&id=".$arr['id']."\"><img title=\"".$tracker_lang['delete']."\" src=\"./pic/admin/delete.gif\" /></a>
</td>";

echo "</tr>";

++$num;
}

echo "<tr><td colspan=\"6\">".$pagerbottom."</td></tr>";

echo "</table>";

stdfoot();

?>