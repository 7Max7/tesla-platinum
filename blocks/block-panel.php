<?php
if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}


global $CURUSER, $tracker_lang;


$h = date('H'); // ��������� ���
if ($h > 6 && $h < 12) $view_time="������ ����";
if ($h >= 12 && $h < 18) $view_time="������ ����";
if ($h >= 18 && $h <= 23) $view_time="������ �����";
if ($h >= 00 && $h <= 6) $view_time="������ ����";

$blocktitle = $view_time;


$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-class_".$CURUSER["class"], "time" => (get_user_class() == UC_SYSOP ? 10*60:30*60), "action" => "get"));

if (get_user_class() >= UC_SYSOP)
$new_content = " / <a title=\"�������������� ������� ��� ������ ������ �� ���� ��������! ��������: ������� ���� ������� ��� �� �������������, �������� � ������� ������������� ��� ������!\" href=\"".$_SERVER['SCRIPT_NAME']."?".(empty($_SERVER['QUERY_STRING']) ? "cache_deleting":(preg_replace("/(\&cache_deleting|cache_deleting)/is", '', $_SERVER['QUERY_STRING']))."&cache_deleting")."\">�����</a>";

if ($filecache <> false) {

$content = $filecache;

if (!empty($new_content)) $content = preg_replace("/<\!--������--\!>/is", $new_content, $content);

} else {
	

$content = "<center>";

if (get_user_class() == UC_SYSOP) {
$expire = 24*60*60; // 60 ����� �� ���, ����� ���������� 

$all_errors = array();

////////// ���� �� xss
$cachesql = ROOT_PATH."/cache/hacklog.txt";
if ((time() - $expire) <= filemtime($cachesql))
$all_errors[] = "<a title=\"������� xss ����: ����� ��������� ���� �� ����������, ������� '������' ��� � ����, ����������������� � ���������. ������ ����: ".mksize(filesize($cachesql))."\" href=\"errors.php?type=xss\"><span style=\"color: red;\">XSS</span></a>";
elseif (file_exists($cachesql))
$all_errors[] = "<a title=\"������� xss ����: ����� ��������� ���� �� ����������, ������� '������' ��� � ����, ����������������� � ���������.\" href=\"errors.php?type=xss\"><span style=\"color: green;\">XSS</span></a>";
////////// ���� �� xss


////////// ������� ������� �� info
$cacheinfo = ROOT_PATH."/cache/info_cache_stat.txt";
if ((time() - $expire) <= filemtime($cacheinfo))
$all_errors[] = "<a title=\"��������� ������� ������ ������� (�������, ip, ��������), ����� ������������ ��, ������� info(); � php ����� � �����, ����� require_once(\"include/bittorrent.php\"); ������ ����: ".mksize(filesize($cacheinfo))."\" href=\"errors.php?type=info\"><span style=\"color: red;\">Info</span></a>";
elseif (file_exists($cacheinfo))
$all_errors[] = "<a title=\"��������� ������� ������ ������� (�������, ip, ��������), ����� ������������ ��, ������� info(); � php ����� � �����, ����� require_once(\"include/bittorrent.php\");\" href=\"errors.php?type=info\"><span style=\"color: green;\">Info</span></a>";
////////// ������� ������� �� info

////////// ������ mysql
$cacheerror = ROOT_PATH."/cache/sqlerror.txt";
if ((time() - $expire) <= filemtime($cacheerror))
$all_errors[] = "<a title=\"������� ������ ������ ���� ������, �� ��� ����� ���������� ��������� �������� ��������, � ����� �� �� ��������� � ����������� ����������. ������ ����: ".mksize(filesize($cacheerror))."\" href=\"errors.php?type=sql\"><span style=\"color: red;\">SQL</span></a>";
elseif (file_exists($cacheerror))
$all_errors[] = "<a title=\"������� ������ ������ ���� ������, �� ��� ����� ���������� ��������� �������� ��������, � ����� �� �� ��������� � ����������� ����������.\" href=\"errors.php?type=sql\"><span style=\"color: green;\">SQL</span></a>";
////////// ������ mysql

////////// ������ ��������
$cachebit = ROOT_PATH."/cache/error_torrent.txt";
if ((time() - $expire) <= filemtime($cachebit))
$all_errors[] = "<a title=\"������� ������ ������ �������-�������� (����������� � �������� ������, ����� �������� ����/����), �� ��� ����� ���������� ��������� �������� ��������, � ����� �� �� ��������� � ����������� ����������. ������ ����: ".mksize(filesize($cachebit))."\" href=\"errors.php?type=bit\"><span style=\"color: red;\" title=\"����� ������ (����� ��� �����)\">Bit</span></a>";
elseif (file_exists($cachebit))
$all_errors[] = "<a title=\"������� ������ ������ �������-�������� (����������� � �������� ������, ����� �������� ����/����), �� ��� ����� ���������� ��������� �������� ��������, � ����� �� �� ��������� � ����������� ����������.\" href=\"errors.php?type=bit\"><span style=\"color: green;\" title=\"���������� ���� (� ������� ��� �� ��� �������� ��� ����� ����)\">Bit</span></a>";
////////// ������ ��������


////////// ������� ������������ ����� ��������� �����
$cachelang = ROOT_PATH."/cache/fix_lang.txt";
if ((time() - $expire) <= filemtime($cachelang))
$all_errors[] = "<a title=\"���� � �������� ����� ����������� ������� ����� ��� ����������� �� ��� ��������� � ���� ������ �������. ������ ����� � � ����� ����� �� ���������. ������ ����: ".mksize(filesize($cachelang))."\" href=\"errors.php?type=lang\"><span style=\"color: red;\">Lang</span></a>";
elseif (file_exists($cachelang))
$all_errors[] = "<a title=\"���� � �������� ����� ����������� ������� ����� ��� ����������� �� ��� ��������� � ���� ������ �������. ������ ����� � � ����� ����� �� ���������.\" href=\"errors.php?type=lang\"><span style=\"color: green;\">Lang</span></a>";
////////// ������� ������������ ����� ��������� �����


$content.= "<b><font color=\"#".get_user_rgbcolor(UC_SYSOP)."\">-=".$tracker_lang['class_sysop']."=-</font></b><br />";

$content.= "<a title=\"�������� ���������� ������� ��������\" href=\"admincp.php\">�������</a> : <a title=\"���������� ������� ������ �� ���������. �������: ����� ��� ������, ����������� ��� �������� �����. ����������� ��������� ��� �������� ��.\" href=\"admincp.php?op=BlocksAdmin\">�����</a><br />";
$content.= "<a title=\"���������� ��������, �������, ����� id ������������. ����� �������� �������: ����� ���������, ��������� ��������� ���������� (����. �.�.) � ������ ���������.\" href=\"admincp.php?op=iUsers\">������� ������</a><br />";


$content.= "�������: <a title=\"������ ������� ���� (��������� ������ ��� �������� � ������). ��������: ������� ���� ������� ��� �� �������������, �������� � ������� �������������!\" onClick=\"return confirm('".$tracker_lang['warn_sure_action']."')\" href=\"admincp.php?op=DropCache\">����</a><!--������--!><br />";

$content.= "<a title=\"���������� ������ ������ �����, ����� ������ � ������ ������������� ������.\" href=\"friensblockadd.php\">������</a> : <a title=\"���������� �������� �� �����, �������� - ��������, �������������� ������. � ����� �������� ���������� �������.\" href=\"groups.php\">������</a> <br />";

$content.= "<a title=\"�������� � �������������� ����� (����� ������), ������� ������ �����. ��������: ������� ������������ ���� �� ����� ���/���� ����, ���������� ������, �� ������� ������ ���������, �������� ����� ������� ��� ����������.\" href=\"/backup/\">���� ������</a><br />";

$content.= "<a title=\"�������� � ����������������� �������� ������ ������� (������������ �� max ��������).\" href=\"cheaters.php\">������</a><br />";


if (count($all_errors)){
$content.= implode(" : ", $all_errors)."<br />";

if (count($all_errors) > 1)
$content.= "<a title=\"�������� ������ ������ ���� ����� ������.\" href=\"errors.php?type=all&d\">�������� ������</a><br />";
}

$content.= "<a title=\"�������������� ������� �������� � ����������� ������, ��� ��������� ����� �� �����, �������� ������������� ���� � ��������, ������� ����� ��� ������������ ������ �� ������� ������.\" href=\"repair_tags.php\">��������� � ����.</a><br />";
$content.= "<a title=\"����� ���������������� �������������, ��� ������� ������������������, �� ��� �� ������������ ����� ����� ���� �������. ��������: ������ 2 ��� ������� ������� ������ ��� �������������, ���� �� ��� ����������� �������.\" href=\"unco.php\">�������.�����</a><br />";
$content.= "<a title=\"������� ����������� ������� � �����, �� ip ��� ��������� ip �������.\" href=\"bans.php\">��� IP</a> : <a title=\"������� ����������� ������� � �����, �� ����� ��� ������ �����. ��� ����������� ������ ������ � ������ ������� � ����� �������.\" href=\"bannedemails.php\">�����</a><br />";
$content.= "<a title=\"������� ����������� ����������� ��� ��������� (��������, ��������������, ��������). �������� � ������� ���� (��), ����� �������������� ������.\" href=\"category.php\">���������</a><br />";
$content.= "<a title=\"������� seo ������ ��� �����. �������� �������� �������������� ����� �� ���� ������ xml ����������� ���������� �������������� ����� ���� ����. ���������� ���� ������ ����� � �������� ���������, ����� ��� �� ������ ������� � ������������� �� ���������� ����������� � ����������.\" href=\"sitemap_parts.php\">Sitemap.xml</a><br />";
$content.= "<a title=\"������� ���������� ����������������� ��������� (��� �� ��� �������).\" href=\"memall.php\">��� �������</a><br />";
$content.= "<a title=\"����� � �������� ������� ������. ������ ����������� �������� � ��������, ���� ��� ������� ������������ ���� ����� - ������ � ����������� ���������� �������.\" href=\"stop_dfiles.php\">���� �����</a><br />";
}



if (get_user_class() >= UC_ADMINISTRATOR) {
$content.= "<b><font color=\"#".get_user_rgbcolor(UC_ADMINISTRATOR)."\">-=".$tracker_lang['class_administrator']."=-</font></b><br />";
$content.= "<a title=\"�������� ��� �� ����� � �� ����� �������� � ��������� ������� ���������� ��������, ip ������ � ��� �������� ������.\" href=\"0nline.php\">������</a> : <a title=\"������� ����������� ����������, ����� � ������ ������ � ���������, �������� ���������� ������������ �� ���� ��������� �����.\" href=\"useragent.php\">��������</a><br />";
$content.= "<a title=\"������ �� �������� ������������ �� �����, � ���������� �����������.\" href=\"adduser.php\">�������� �����</a><br />";
$content.= "<a title=\"��������� ���, ��� �� ����� �� ��������� ���� ��������� �� �����. ��������: ������������� ��� ������������� �� ���� �����!\" href=\"msg.php\">���������</a><br />";

$content.= "<a title=\"����� ���������� ����������. �������� ������ �� ����� �����, �� ���������� �������.\" href=\"statistics.php\">�����</a> : <a title=\"����� �������������� ����������. �������� ������ �� ������ ����������� ������������ (�� ������ ��� ����� id), �� ���������� �������.\" href=\"ustatistics.php\">�������</a><br />";
$content.= "<a title=\"������� �� ���������� ������� � ����� ������ �� �������� ������������ ������� ����� � ������� (�).\" href=\"maxlogin.php\">������� �����</a><br />";
$content.= "<a title=\"������� ������ '������' � ����� �� ������.\" href=\"redirect.php\">��������</a> : <a title=\"������� ������ � ��������� ����������� ������, ��� � ����� � ��� ������� ����� � ��� - ������ ����.\" href=\"referer.php\">��������</a><br />";
$content.= "<a title=\"������ ������������� ������������� � �����������, �������� � ���� ��������� - ����������.\" href=\"adminbookmark.php\">�������������</a><br />";
$content.= "<a title=\"������� ������������ ������ � ������� ����������, ������ �� ���������� ������� ������� ��� ��� - ����� ������� ������� '������' �� �������� � ��������� ������� ������������ � ������ �������.\" href=\"downcheck.php\">C������������</a><br />";

$report = get_row_count("report");
if (!empty($report))
$content.= "<a title=\"���������� ������������ ����� ".number_format($report).", �� ������������ ��� ��� �������.\" href=\"viewreport.php\">������ (".number_format($report).")</a><br />";

$content.= "<a title=\"������ ���������� ���������������� ��������������, ��� �������, ����� � �������. �� ����������� �������� ��� ������ ��������������.\" href=\"warned.php\">�������.</a> : <a href=\"hiderating.php\">�������.</a><br />";

$content.= "�������� <a title=\"���� �������� �������� ��������� ������������ ������������� �� ������.\" href=\"staffmess.php\">��</a> : <a title=\"���� �������� �������� ��������� ������������ ������������� �� �������� � ������.\" href=\"groupsmess.php\">������</a></a><br />";

$content.= "<a title=\"���������� ��������, ����� - ������� - ������, �������.\" href=\"mybonus.php?action=menu\">����� ��������</a></a><br />";

}


if (get_user_class() >= UC_MODERATOR) {
$content.= "<b><font color=\"#".get_user_rgbcolor(UC_MODERATOR)."\">-=".$tracker_lang['class_moderator']."=-</font></b><br />";
$content.= "<a title=\"���� ������ ������������ � ���������� ����������� ������.\" href=\"usersearch.php\">����� �����</a><br />";

$uploadapp = (get_row_count("uploadapp"));
if (!empty($uploadapp))
$content.= "<a title=\"���������� ������ ".number_format($uploadapp).", �� ��������� � ������������. ���������� ������� ��������!\" href=\"uploadapp.php\">������ (".number_format($uploadapp).")</a><br />";

$content.= "<a title=\"������ ���������� � ������ ����� �������������, �������� - �� ������ ����� ������!\" href=\"usersearch.php?dt=2&d=".date("Y-m-d", time()-(24*60*60*7))."\">����� �����</a><br />";

$content.= "<a title=\"������ ������ ���� (������� �� ������ ���������� � �����) ������������� �����.\" href=\"users.php\">�����</a> : <a title=\"���� ������ �������, ��� ������ ��� �������� ������������� ������.\" href=\"smilies.php\">������</a><br />";

$content.= "<a title=\"������ ������ ��������� ������������� �� max ���������.\" href=\"antichiters.php\">��������</a> : <a title=\"������ ������ ������������� � ����������� ���������� (������ ��� �������).\" href=\"seeders.php\">���-��</a><br />";
$content.= "<a title=\"������ ���������� ������, ������������ � ����� ��������� ���������� �� ������ � ������ �� �����.\" href=\"tags.php\">�������� �����</a><br />";
$content.= "������� <a title=\"������ ������ ���������� �������������.\" href=\"ipcheck.php?old\">IP</a> : <a title=\"������ ������ ���������� �������������. ��������: ������� �� �������� - ��� ����� � ����� �����!\" href=\"ipcheck.php\">��������</a><br />";
$content.= "<a title=\"������ ������ �������������� ������ �� ������ ������������, ��� ���������� ���������� ������ ���� �������� ������� � ��.\" href=\"stats.php\">�����-��</a> : <a title=\"������ ������ ���������� �� �����, ���������� �������� ��� � ��������� �������.\" href=\"uploaders.php\">��������</a><br />";
$content.= "<a title=\"������ ������ ������� �������������� �������������\" href=\"topten.php\">TOP10</a> : <a title=\"������ ������ ������ ������������� � ��������� ������� � �������.\" href=\"votes.php\">������</a><br />";

$content.= "<a title=\"������ ������ �� ���������� ip ��� ������ �� ����.\" href=\"testip.php\">�������� IP / �����</a><br />";

}

$content.= "<a title=\"������ ������ ��������� ��������� �� �����, ��� (��������� �������) �� ����� ���������� �����, ���� � ���� ������.\" href=\"license.php\">��������� �������</a><br />";

$content.= "<a title=\"������ �������������, ����� ����� ������ ������ �� ���� ������� �������������.\" href=\"premod.php\">".$tracker_lang['modercp']."</a><br />";

$content.= "</center>";


file_query($content, $cache = array("type" => "disk", "file" => "block-class_".$CURUSER["class"], "time" => (get_user_class() == UC_SYSOP ? 10*60:30*60), "action" => "set"));

if (!empty($new_content)) $content = preg_replace("/<\!--������--\!>/is", $new_content, $content);
}


?>
<script>
function usermod_online() {
jQuery.post("block-user_jquery.php" , {"action":"usermod"} , function(response) {
jQuery("#usermod_online").html(response);
}, "html");
setTimeout("usermod_online();", 90000);
}
usermod_online();
</script>
<?

$content.='<div id="usermod_online"></div><!----secretpage.php--->';

?>