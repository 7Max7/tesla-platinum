<?
require "include/bittorrent.php";
dbconn();
loggedinorreturn();

if (get_user_class() < UC_SYSOP) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}
accessadministration();


if (isset($_POST["delete"]) && is_array($_POST["delete"])) {

$delete = $_POST["delete"];
if (count($delete))
sql_query("DELETE FROM users WHERE status = 'pending' AND id IN (".implode(", ", array_map("intval", $delete)).")") or sqlerr(__FILE__, __LINE__);

header("Location: ".$DEFAULTBASEURL."/unco.php");
die;
}

if (isset($_POST["confirm"]) && is_array($_POST["confirm"])) {

$confirm = $_POST["confirm"];
if (count($confirm))
sql_query("UPDATE users SET status = 'confirmed', last_login = ".sqlesc(get_date_time()).", last_access = ".sqlesc(get_date_time())." WHERE status = 'pending' AND id IN (".implode(", ", array_map("intval", $confirm)).")") or sqlerr(__FILE__, __LINE__);

header("Location: ".$DEFAULTBASEURL."/unco.php");
die;
}


$count = get_row_count("users", "WHERE status = 'pending'");
list ($pagertop, $pagerbottom, $limit) = pager(100, $count, "unco.php?");

if (empty($count))
stderr($tracker_lang['error'], $tracker_lang['no_data_now']);


$res = sql_query("SELECT u.id, u.username, u.class, u.email, u.ip, u.added, (SELECT COUNT(*) FROM users WHERE users.ip = u.ip) AS countip FROM users AS u WHERE u.status = 'pending' ORDER BY u.ip ".$limit) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) > 0) {

stdhead($tracker_lang['unco_users']);

?>
<script language="Javascript" type="text/javascript">
jQuery(document).ready(function() {

jQuery("#confirm_all").click(function () {
if (!jQuery("#confirm_all").is(":checked"))
jQuery(".confirm").removeAttr("checked");
else
jQuery(".confirm").attr("checked","checked");
});

jQuery("#delete_all").click(function () {
if (!jQuery("#delete_all").is(":checked"))
jQuery(".delete").removeAttr("checked");
else
jQuery(".delete").attr("checked","checked");
});

}); 
</script>
<?

echo "<form method=\"post\" action=\"unco.php\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"a\" colspan=\"4\">".$pagertop."</td></tr>";

echo "<tr>";
echo "<td class=\"colhead\">".$tracker_lang['signup']."</td>";
echo "<td class=\"colhead\">".$tracker_lang['signup_username']." / ".$tracker_lang['email']."</td>";
echo "<td class=\"colhead\">".$tracker_lang['user_ip']."</td>";
echo "<td class=\"colhead\" align=\"center\"><label><input type=\"checkbox\" id=\"confirm_all\" />".$tracker_lang['confirmation']."</label> / <label>".$tracker_lang['delete']." <input type=\"checkbox\" id=\"delete_all\" /></label></td>";
echo "</tr>";

$num = 0;
while ($row = mysql_fetch_assoc($res)) {

$cl2 = 'class = "b"'; $cl1 = 'class = "a"';

if ($num % 2 == 1){
$cl1 = 'class = "b"';
$cl2 = 'class = "a"';
}

$bot = is_spamer($row["username"], $row["email"]);

echo "<tr>";
echo "<td ".$cl2." align=\"left\">".$row["added"]."<br />(".$tracker_lang['antibot_system'].": ".$bot."%)</td>";

echo "<td ".$cl2."><a href=\"userdetails.php?id=".$row["id"]."\">".get_user_class_color($row["class"], $row["username"])."</a><br />".protectmail($row["email"])."</td>";

echo "<td ".$cl1." align=\"left\"><a target=\"_blank\" title=\"".$tracker_lang["search_ip"]."\" href=\"usersearch.php?ip=".$row["ip"]."\">".$row["ip"]."</a> ".($row["countip"] > 1 ? "(".$row["countip"].")":"")."</td>";

echo "<td ".$cl1." align=\"left\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td align=\"center\" class=\"a\"><input type=\"checkbox\" title=\"".$tracker_lang['confirm_user']."\" name=\"confirm[]\" class=\"confirm\" value=\"".$row["id"]."\" /></td><td align=\"center\" class=\"b\"><input type=\"checkbox\" name=\"delete[]\" ".($bot > 80 ? "checked":"")." title=\"".$tracker_lang['completely_remove']."\" class=\"delete\" value=\"".$row["id"]."\" /></td></tr>
</table></td>";

echo "</tr>";

++$num;
}


echo "<tr><td class=\"b\" align=\"right\" colspan=\"4\"><input type=\"submit\" value=\"".$tracker_lang['b_action']."\" class=\"btn\" style=\"height: 20px; width: 200px\"></td></tr>";

echo "<tr><td class=\"a\" colspan=\"4\">".$pagerbottom."</td></tr>";

echo "</table></form>";

} else 
stderr($tracker_lang['error'], $tracker_lang['no_data_now']);

stdfoot();
?>