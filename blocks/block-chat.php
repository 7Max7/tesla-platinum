<?php
if (!defined('BLOCK_FILE')) {
 Header("Location: ../index.php");
 exit;
}

global $tracker_lang, $CURUSER;

$blocktitle = $tracker_lang['chat'];

$ex_1 = array_map('trim', explode(", ", $tracker_lang['bb_bcode']));
$ex_2 = array_map('trim', explode(", ", $tracker_lang['bb_brepl']));


$content = "";

if (!in_array(end(explode('/', htmlspecialchars_uni($_SERVER["SCRIPT_FILENAME"]))), array("tracker-chat.php")) || !$CURUSER) {

$content .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"js/jScrollPane.css\" />";
$content .= "<div id=\"chatContainer\">";

/*
$content .= "<div id=\"chatBottomBar\" style=\"float:center;\" class=\"rounded\">";
$content .= "Online: <span id=\"chatUsers\" class=\"rounded\"></span>";
$content .= "</div>";
*/

$content .= "<div id=\"chatLineHolder\"></div>";
$content .= "<div id=\"chatBottomBar\" class=\"rounded\" align=\"center\"><div class=\"tip\"></div>";

$content .= "<form id=\"submitForm\" method=\"post\" action=\"\" name=\"tesla\">";

$content .= "<input class=\"btn\" type=\"button\" value=\"".$ex_1[26]." (".$tracker_lang['list'].")\" name=\"Smailes_View\" title=\"".$ex_2[26]." (".$tracker_lang['list'].")\" style=\"width: 120px;\" /> <input id=\"chatText\" name=\"chatText\" size=\"70%\" class=\"rounded\" /> <input type=\"submit\" name=\"post\" class=\"btn\" title=\"CTRL+ENTER ".$tracker_lang['sendmessage']."\" value=\"".$tracker_lang['sendmessage']."\" /><div id=\"prevsmalie\" align=\"center\" name=\"tesla:chatText\"></div>";

$content .= "</form>";
$content .= "<br />";
$content .= "</div>";

$content .= "<script src=\"js/jquery.mousewheel.js\"></script>";
$content .= "<script src=\"js/jScrollPane.min.js\"></script>";
$content .= "<script src=\"js/jquery.chat.scripts.js\"></script>";


$content .= "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";
$content .= "<tr><td class=\"b\" align=\"center\" colspan=\"5\"><a class=\"alink\" href=\"tracker-chat.php\">".$tracker_lang['chat_fullver']."</a></td></tr>";
$content .= "</table>";


} else {

$content .= "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";
$content .= "<tr><td class=\"b\" align=\"center\" colspan=\"5\"><a class=\"alink\" href=\"tracker-chat.php\">".$tracker_lang['chat_fullver']."</a></td></tr>";
$content .= "</table>";

}


?>