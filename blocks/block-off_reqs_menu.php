<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $tracker_lang;

$blocktitle = $tracker_lang['offers'];

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-off_reqs_menu", "time" => 60*60, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

$content = "";
$bt = sql_query("SELECT id, name FROM off_reqs WHERE perform = 'no'") or sqlerr(__FILE__, __LINE__);
while ($row = mysql_fetch_assoc($bt)){
$content.= "<a class=\"menu\" title=\"".$tracker_lang['requests_section']."\" href=\"detailsoff.php?id=".$row["id"]."\">".htmlspecialchars_uni($row["name"])."</a>";
}

$filecache = file_query($content, $cache = array("type" => "disk", "file" => "block-off_reqs_menu", "time" => 60*60, "action" => "set"));
}
?>