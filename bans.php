<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn(); 

if (get_user_class() <= UC_MODERATOR){
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

if (isset($_GET['remove'])) {

$remove = (!empty($_GET["remove"]) ? intval($_GET["remove"]):0);
$res = sql_query("SELECT first, last FROM bans WHERE id = ".sqlesc($remove)) or sqlerr(__FILE__, __LINE__);

$ip = mysql_fetch_array($res);
$first = long2ip($ip["first"]);
$last = long2ip($ip["last"]);

if ($first && $last && mysql_num_rows($res)) {

write_log("��� IP ������ # ".$remove." (".($first == $last? $first : $first." - ".$last).") ����� ������������� ".$CURUSER["username"], "704ffd", "bans");

sql_query("DELETE FROM bans WHERE id = ".sqlesc($remove)) or sqlerr(__FILE__, __LINE__);

unsql_cache("bans_first_last");

header("Location: bans.php");
die;
}

}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$true_ban = true;
$first = trim(!empty($_POST["first"]) ? htmlspecialchars_uni($_POST["first"]):"");
$last = trim(!empty($_POST["last"]) ? htmlspecialchars_uni($_POST["last"]):"");
$cbanslength = (!empty($_POST["banslength"]) ? intval($_POST["banslength"]):"");
$comment = trim(!empty($_POST["comment"]) ? htmlspecialchars_uni($_POST["comment"]):"");

if (empty($last) && !empty($first))
$last = $first;

if (empty($comment))
stderr($tracker_lang['error'], $tracker_lang['missing_form_data']);

$first = ip2long($first);
$last = ip2long($last);

if (empty($first) || empty($last) || !is_numeric($first) || !is_numeric($last))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$ip_bans = get_row_count("bans", "WHERE ".sqlesc($first)." >= first AND ".sqlesc($last)." <= last");

if ($first == -1 || $last == -1 || empty($last) || empty($first))
stderr($tracker_lang['error'], $tracker_lang['invalid_ip']);

$banua = get_row_count("users", "WHERE ip >= ".sqlesc(long2ip($first))." AND ip <= ".sqlesc(long2ip($last))." AND class >= ".sqlesc(UC_MODERATOR));

if (!empty($banua) || !empty($ip_bans))
$true_ban = false;

if (empty($cbanslength))
$time_until = "0000-00-00 00:00:00";
else
$time_until = get_date_time(gmtime() + $cbanslength * 604800);

if ($true_ban == true){
sql_query("INSERT INTO bans (added, addedby, first, last, bans_time, comment) VALUES (".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($first).", ".sqlesc($last).", ".sqlesc($time_until).", ".sqlesc(htmlspecialchars_uni($comment)).")") or sqlerr(__FILE__, __LINE__);

$l = long2ip($last);
$f = long2ip($first);

write_log("IP ����� (a): ".($f == $l? $f." �������":" ".$f." - ".$l." ��������")." ������������� ".$CURUSER["username"], "ff3a3a", "bans");
unsql_cache("bans_first_last");
}

header("Location: bans.php");
die;
}

gzip();

global $use_ipbans;
if (empty($use_ipbans))
stderr($tracker_lang['bans'], $tracker_lang['func_disabled']);

$bansip = (!empty($_GET["bansip"]) ? ip2long($_GET["bansip"]):"");
if (!empty($bansip))
$count_b = get_row_count("bans", "WHERE ".sqlesc($bansip)." >= first AND ".sqlesc($bansip)." <= last");

if (empty($count_b))
$count = get_row_count("bans");
else
$count = $count_b;

$perpage = 100;

list ($pagertop, $pagerbottom, $limit) = pager($perpage, $count, "bans.php?");

stdhead($tracker_lang['bans']);

echo ("<form method=\"post\" action=\"bans.php\">\n");

echo "<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">";

echo ("<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['ban_ips']."</td></tr>");
echo ("<tr><td class=\"b\">".$tracker_lang['ban_ipb']."</td><td class=\"a\"> <input type=\"text\" name=\"first\" size=\"40\" /></td></tr>\n");
echo ("<tr><td class=\"a\">".$tracker_lang['ban_ipe']."</td><td class=\"b\"> <input type=\"text\" name=\"last\" size=\"40\" /></td></tr>\n");
echo ("<tr><td class=\"b\">".$tracker_lang['comment']."</td><td class=\"a\"> <input type=\"text\" name=\"comment\" size=\"40\" /> ".sprintf($tracker_lang['max_n_limit'], 255)."</td></tr>\n");

echo ("<tr><td class=\"a\">".$tracker_lang['bans_for']."</td><td class=\"b\">

<select name=\"banslength\">
<option value=\"0\">".$tracker_lang['unlimited_time']."</option>
<option value=\"1\">1 ".$tracker_lang['week_one']."</option>
<option value=\"2\">2 ".$tracker_lang['week_all']."</option>
<option value=\"3\">3 ".$tracker_lang['week_all']."</option>
<option value=\"4\">4 ".$tracker_lang['week_all']."</option>
<option value=\"5\">5 ".$tracker_lang['week_only']."</option>
<option value=\"6\">6 ".$tracker_lang['week_only']."</option>
<option value=\"7\">7 ".$tracker_lang['week_only']."</option>
<option value=\"8\">8 ".$tracker_lang['week_only']."</option>
<option value=\"9\">9 ".$tracker_lang['week_only']."</option>
<option value=\"10\">10 ".$tracker_lang['week_only']."</option>
<option value=\"11\">11 ".$tracker_lang['week_only']."</option>
<option value=\"12\">12 ".$tracker_lang['week_only']."</option>
</select>

</td></tr>\n");

echo ("<tr><td class=\"b\" align=\"center\" colspan=\"2\"><input type=\"submit\" value=\"".$tracker_lang['ban_ips']."\" class=\"btn\" /></td></tr>\n");

echo "</table>";
echo ("</form>\n");
echo ("<br />\n");

if (empty($count)){
echo "<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">";
echo "<tr><td align=\"center\">".$tracker_lang['no_data_now']."</td></tr>"; 
echo "</table>";
stdfoot();
die;
}

echo ("<form method=\"get\" action=\"bans.php\"><table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\"><tr><td class=\"b\" colspan=\"5\">".$tracker_lang['test_ip'].": <input type=\"text\" value=\"".trim(long2ip($bansip))."\" name=\"bansip\" size=\"40\" /> <input type=\"submit\" value=\"".$tracker_lang['search_btn']."\" class=\"btn\" /></td></tr></table></form><br />");

$res = sql_query("SELECT b.*, u.username, u.class
FROM bans AS b
LEFT JOIN users AS u ON u.id = b.addedby ".(!empty($count_b) ? "WHERE ".sqlesc($bansip)." >= b.first AND ".sqlesc($bansip)." <= b.last":"")."
ORDER BY b.added DESC ".$limit) or sqlerr(__FILE__, __LINE__);

echo "<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">";

echo ("<tr><td class=\"colhead\" colspan=\"5\">".$tracker_lang['bans']."</td></tr>");

if ($count > $perpage)
echo "<tr><td colspan=\"5\">".$pagertop."</td></tr>"; 

echo ("<tr>
<td width=\"15%\" align=\"center\" class=\"colhead\">".$tracker_lang['clock']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['ban_ipb']." / ".$tracker_lang['ban_ipe']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['news_poster']." / ".$tracker_lang['comment']."</td>
<td width=\"18%\" class=\"colhead\" align=\"center\">".$tracker_lang['bans_for']."</td>
<td width=\"8%\" align=\"center\" class=\"colhead\">".$tracker_lang['action']."</td>
</tr>");

$num = 0;
while ($arr = mysql_fetch_assoc($res)) {

if ($num%2==0){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

$arr["first"] = long2ip($arr["first"]);
$arr["last"] = long2ip($arr["last"]);

$coun_tdeny = '';

$file_name = ROOT_PATH."/torrents/txt/banned_".$arr["id"].".txt";

if (file_exists($file_name)){
$coun_tdeny = file_get_contents($file_name)." ��.";
$coun_tdeny .= " (".get_date_time(filemtime($file_name)).")";
}


echo ("<tr>
<td ".$cl1." align=\"center\">".$arr["added"]."</td>
<td ".$cl2." align=\"center\"><nobr>".($arr["first"] == $arr["last"] ? "<a title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$arr["last"]."\">".$arr["last"]."</a>":"<a title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$arr["first"]."\">".$arr["first"]."</a> - <a title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$arr["last"]."\">".$arr["last"]."</a>")."</nobr></td>
<td ".$cl1." align=\"left\">".(!empty($arr["username"]) ? "<a href=\"userdetails.php?id=".$arr["addedby"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a>":$tracker_lang['anonymous']).": ".format_comment($arr["comment"])."</td>
<td ".$cl2." align=\"center\">".($arr["bans_time"] == "0000-00-00 00:00:00" ? "<i>".$tracker_lang['unlimited_time']."</i>":$arr["bans_time"])."".(!empty($coun_tdeny) ? "<br />".$coun_tdeny:"")."</td>
<td ".$cl1." align=\"center\"><a href=\"bans.php?remove=".$arr["id"]."\">".$tracker_lang['disselect']."</a></td>
</tr>\n");

++$num;
}

if ($count > $perpage)
echo "<tr><td colspan=\"5\">".$pagerbottom."</td></tr>"; 

echo "</table>";

stdfoot();
?>