<?
if (!defined('BLOCK_FILE')) {
 Header("Location: ../index.php");
 exit;
}
global $CURUSER ,$tracker_lang;


$blocktitle = $tracker_lang['poll']." - ".(get_user_class() > UC_MODERATOR ? "[<a class=\"altlink_white\" href=\"makepoll.php\"><b>".$tracker_lang['create']."</b></a>] - " : "")."[<a class=\"altlink_white\" href=\"polls.php\"><b>".$tracker_lang['polls_olders']."</b></a>]";

if ($CURUSER) {
?>
<script type="text/javascript" src="js/poll.core.js"></script>

<script type="text/javascript">
jQuery(document).ready(
function(){
loadpoll();
}
);
</script>
<?
$content.= "<table width=\"100%\" class=\"main\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\"><tr>
<td class=\"text\" align=\"center\">
<div id=\"poll_container\">
<div id=\"loading_poll\" style=\"display:none\"></div>
<noscript><b>".$tracker_lang['noscript']."</b></noscript>
</div>
</td>
</tr></table>";

} else {

$content .= "<table class=\"main\" align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\"><tr><td class=\"text\">";
$content .= "<div align=\"center\"><h3>".$tracker_lang['polls_guest']."</h3></div>\n";
$content .= "</td></tr></table>";

}

?>