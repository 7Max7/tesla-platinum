<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

if (get_user_class() < UC_MODERATOR && $CURUSER['override_class'] <> 255) {
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

if (isset($_GET['action']) == 'editclass') {
	
$returnto = (isset($_GET["returnto"]) ? $_GET["returnto"]:false);
if (empty($returnto) || $returnto == false)
$returnto = $DEFAULTBASEURL;

$newclass = (int) $_GET['class'];
$maxclass = get_user_class() - 1;

if ($newclass > $maxclass)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
else
sql_query("UPDATE users SET override_class = ".sqlesc($CURUSER['class']).", class = ".sqlesc($newclass). " WHERE id = ".sqlesc($CURUSER['id'])); 

unsql_cache("arrid_".$CURUSER["id"]);

header("Location: ".$returnto);
die();
}

stdhead($tracker_lang['test_class']);

$returlink = $_SERVER["HTTP_REFERER"];
$site = parse_url($returlink, PHP_URL_HOST);

echo ("<form method=\"get\" action=\"setclass.php\">

<table width=\"100%\" border=\"2\" cellspacing=\"5\" cellpadding=\"5\">
<tr><td><b>".$tracker_lang['class']."</b>:</td><td align=\"left\"><select name=\"class\">");

$maxclass = get_user_class() - 1;
for ($i = 0; $i <= $maxclass; ++$i)
echo ("<option value=\"".$i."\">".get_user_class_name($i)."</option>");

echo ("</select></td></tr>
".(!empty($site) ? "<tr><td><b>".$tracker_lang['back_inlink']."</b>:</td><td align=\"left\">".$returlink." <input type=\"checkbox\" name=\"vol\" checked value=\"yes\" /> <input type=\"hidden\" name=\"action\" value=\"editclass\" /><input type=\"hidden\" name=\"returnto\" value=\"".$returlink."\" /></td></tr>":"")."
<tr><td colspan=\"3\" align=\"center\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['test_class']."\"></td></tr>
</form></table><br />");


$res = sql_query("SELECT id, username, class, override_class FROM users WHERE override_class <> '255'");
while ($row = mysql_fetch_assoc($res))
$ar_class[] = "<a href=\"userdetails.php?id=".$row["id"]."\">".get_user_class_color($row["override_class"], $row["username"])."</a> (".get_user_class_name($row["class"]).")";

if (count($ar_class))
echo "<table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"5\"><tr><td><b>".$tracker_lang['testing_class']."</b>:</td><td align=\"left\">".implode(", ", $ar_class)."</td></tr></table><br />";

stdfoot();
?>