<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $tracker_lang;
$blocktitle = $tracker_lang['calendar_torrents'];

?>
<style>
.cl1 {border:solid 1px #00CCFF; text-align:center}
.cl3hov {background-color:#FF6633; border-color:#FF531A; text-align:center}
.cl2 {background-color:#CCFFCC; color:#00B32D; text-align:center}
</style>
<?

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-calendar", "time" => 60*60, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

global $tracker_lang; 

$array = array();

$res_ = sql_query("SELECT DATE_FORMAT(added, '%Y-%m-%d') AS date FROM torrents AS tor WHERE tor.added > DATE_SUB(NOW(), INTERVAL 31 DAY) GROUP BY date") or sqlerr(__FILE__, __LINE__);

while ($arr_ = mysql_fetch_assoc($res_)){

$array[$arr_["date"]] = get_row_count("torrents", "WHERE added > '".$arr_["date"]." 00:00:00' AND added < '".$arr_["date"]." 23:59:59'");

}

list($today, $mounth, $year) = explode("-", date("j-m-Y")); 

$day = mktime(0, 0, 0, $mounth, 1, $year); 
$dayofweek = date("w", $day); 
$back = ($dayofweek + 6) % 7; 
$day -= 86400 * $back; 
$day_tmp = $day; 

$day_name = array("Mon" => $tracker_lang['Monday'], "Tue" => $tracker_lang['Tuesday'], "Wed" => $tracker_lang['Wednesday'], "Thu" => $tracker_lang['Thursday'], "Fri" => $tracker_lang['Friday'], "Sat" => $tracker_lang['Saturday'], "Sun" => $tracker_lang['Sunday']);

$content = '<table align="center" border="0" cellpadding="3" cellspacing="0" width="100%"> 
<tr>'; 

for ($i=0; $i<7; $i++) {
$content.= '<td class="colhead">'.$day_name[date('D', $day_tmp)].'</td>'; 
$day_tmp += 86400; 
}

$content.= '</tr>'; 

while (true) {

$content.= '<tr>';

for ($i=0; $i<7; $i++) {

$date = date('j', $day);
$mounth_tmp = date("m", $day);

$mounfor = date('Y-m-d', $day);

$content.= '<td';

//if ($today == $date)
if (isset($array[date('Y-m-d', $day)]) && $mounth == $mounth_tmp)
$content.= ' class="cl3hov" title="'.$tracker_lang['tracker_torrents'].': '.$array[date('Y-m-d', $day)].'"';

elseif (isset($array[date('Y-m-d', $day)]) && $mounth <> $mounth_tmp)
$content.= ' class="cl2" title="'.$tracker_lang['tracker_torrents'].': '.$array[date('Y-m-d', $day)].'"';

elseif ($mounth <> $mounth_tmp)
$content.= ' class="cl2"';
else
$content.= ' class="cl1" title="'.sprintf($tracker_lang['search_for_day'], $mounfor).'"';

if (isset($array[date('Y-m-d', $day)]) && $mounth == $mounth_tmp)
$content.= '><a class="altlink_white" href="browse.php?date='.$mounfor.'">'.$date.'</a></td>';
else
$content.= '><a href="browse.php?date='.$mounfor.'">'.$date.'</a></td>';

$day += 86400;

}

$content.= '</tr>'; 

if ($mounth <> $mounth_tmp) break; 
}


$content.= '</table>';

//60*60*24
$filecache = file_query($content, $cache = array("type" => "disk", "file" => "block-calendar", "time" => 5, "action" => "set"));
}

?> 