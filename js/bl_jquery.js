//vars
LoadBarGray  = '<div align="center"><img src="http://static.alexlan.org/images/loaders/big-gray.gif" width="208" height="13" border="0" alt="��� ��������"></div>';
LoadBarBlue  = '<div align="center"><img src="http://static.alexlan.org/images/loaders/big-blue.gif" border="0" alt="��� ��������"></div>';
LoadBarBlack = '<div align="center"><img src="http://static.alexlan.org/images/loaders/big-black.gif" border="0" alt="��� ��������"></div>';
loadBarBlack = '<div align="center"><img src="http://static.alexlan.org/images/loaders/black.gif" border="0" style="margin-top: 5px;" alt="��� ��������"></div>';
loadBarBlue  = '<div align="center"><img src="http://static.alexlan.org/images/loaders/blue.gif" border="0" alt="��� ��������"></div>';
Spinner      = '<div align="center"><img src="http://static.alexlan.org/images/loaders/spinner.gif" border="0" alt="��� ��������"></div>';
Spinner2     = '<div align="center"><img src="http://static.alexlan.org/images/loaders/spinner2.gif" border="0" alt="��� ��������"></div>';
Spinner3     = '<div align="center"><img src="http://static.alexlan.org/images/loaders/spinner3.gif" border="0" alt="��� ��������"></div>';
spinner      = '<div align="center"><img src="http://static.alexlan.org/images/loaders/spinner-small.gif" border="0" alt="��� ��������"></div>';
//array func
function implode( glue, pieces )
{
    return ((pieces instanceof Array) ? pieces.join(glue) : pieces);
}

function explode(delimiter, string)
{
    var emptyArray = {0: ''};

    if (arguments.length != 2
        || typeof arguments[0] == 'undefined'
        || typeof arguments[1] == 'undefined')
    {
        return null;
    }

    if (delimiter === ''
        || delimiter === false
        || delimiter === null)
    {
        return false;
    }

    if (typeof delimiter == 'function'
        || typeof delimiter == 'object'
        || typeof string == 'function'
        || typeof string == 'object')
    {
        return emptyArray;
    }

    if (delimiter === true) {
        delimiter = '1';
    }
    return string.toString().split(delimiter.toString());
}

function in_array(needle, haystack, strict)
{
    var found = false, key, strict = !!strict;
    for (key in haystack)
    {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle))
        {
            found = true;
            break;
        }
    }
    return found;
}
//cookies func
function getCookie(name)
{
    var start = document.cookie.indexOf(name + "=");
    var len = start + name.length + 1;
    if ((!start) && (name != document.cookie.substring(0, name.length)))
    {
        return null;
    }
    if (start == -1)
        return null;
    var end = document.cookie.indexOf(';', len);
    if (end == -1)
        end = document.cookie.length;
    return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure)
{
    var today = new Date();

    today.setTime(today.getTime());

    if (expires)
        expires = expires * 1000 * 60 * 60 * 24;

    var expires_date = new Date(today.getTime() + (expires));
    document.cookie = name+'='+escape(value) + ((expires) ? ';expires='+expires_date.toGMTString() : '') + ((path) ? ';path=' + path : ';path=/') + ((domain) ? ';domain=' + domain : '') + ((secure) ? ';secure' : '');
}
//collapse func
function collapse(block, archive)
{
    var collapse_no = jQuery("#" + block + " em.collapse-no");

    if (collapse_no.length > 0)
    {
        collapse_no.hide();

        return;
    }

    jQuery("#" + block + " div.collapsed_content").slideToggle("slow");
    jQuery("#" + block + " em").toggleClass("close");

    if (archive !== false) {
        var collapsed = jQuery("em.collapse-yes em.close").get();
        var arr = new Array();

        jQuery(collapsed).each(
            function()
            {
                arr.push(jQuery(this).attr("lang"));
            }
        );

        data = implode('|', arr);
        setCookie('collapsed', data, 365);
    }
}


function getHeight() {
    // handle IE 6
    if (jQuery.browser.msie && jQuery.browser.version < 7) {
        var scrollHeight = Math.max(
	    document.documentElement.scrollHeight,
	    document.body.scrollHeight
	);
	var offsetHeight = Math.max(
	    document.documentElement.offsetHeight,
	    document.body.offsetHeight
	);

	if (scrollHeight < offsetHeight) {
	    return jQuery(window).height() + 'px';
	} else {
	    return scrollHeight + 'px';
	}
    // handle "good" browsers
    } else {
	return jQuery(document).height() + 'px';
    }
}

function getWidth() {
    // handle IE 6
    if (jQuery.browser.msie && jQuery.browser.version < 7) {
        var scrollWidth = Math.max(
	    document.documentElement.scrollWidth,
	    document.body.scrollWidth
	);
	var offsetWidth = Math.max(
	    document.documentElement.offsetWidth,
	    document.body.offsetWidth
	);

	if (scrollWidth < offsetWidth) {
	    return jQuery(window).width() + 'px';
	} else {
	    return scrollWidth + 'px';
	}
    // handle "good" browsers
    } else {
        return jQuery(document).width() + 'px';
    }
}
