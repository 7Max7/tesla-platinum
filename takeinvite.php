<?
require "include/bittorrent.php";
dbconn();

if ($CURUSER) {
if (!headers_sent()){
@header("Location: ".$DEFAULTBASEURL);
die;
} else
die("<script>setTimeout('document.location.href=\"".$DEFAULTBASEURL."\"', 10);</script>");
}

$wantusername = (isset($_POST["wantusername"]) ? htmlspecialchars($_POST["wantusername"]):"");
$passagain = (isset($_POST["passagain"]) ? htmlspecialchars($_POST["passagain"]):"");
$wantpassword = (isset($_POST["wantpassword"]) ? htmlspecialchars($_POST["wantpassword"]):"");
$signup = ((isset($_GET["signup"]) && $_GET["signup"] == "yes") ? "yes":"no");
$secret = (isset($_POST["secret"]) ? htmlentities($_POST["secret"]):"");

if ($signup == "yes"){

if (empty($wantusername) || empty($wantpassword))
stderr($tracker_lang['error'], $tracker_lang['data_noempty'].": <br /> <strong>
".(empty($wantusername)? $tracker_lang['signup_username']."<br />":"")."
".(empty($wantpassword)? $tracker_lang['signup_password']."<br />":"")."
</strong>");

if (strlen($wantusername) > 12)
stderr($tracker_lang['error'], $tracker_lang['signup_username'].": ".sprintf($tracker_lang['max_simp_of'], 12));

elseif ($wantpassword <> $passagain)
stderr($tracker_lang['error'], $tracker_lang['password_mismatch']);

elseif (strlen($wantpassword) < 7)
stderr($tracker_lang['error'], $tracker_lang['signup_password'].": ".sprintf($tracker_lang['min_simp_of'], 7));

elseif (strlen($wantpassword) > 40)
stderr($tracker_lang['error'], $tracker_lang['signup_password'].": ".sprintf($tracker_lang['max_simp_of'], 40));

elseif ($wantpassword == $wantusername)
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": <strong>".$tracker_lang['signup_password']." (".$wantpassword.") = ".$tracker_lang['signup_username']." (".$wantpassword.")</strong>");

elseif (!validusername($wantusername))
stderr($tracker_lang['error'], $tracker_lang['validusername']);

$psecret_in = (isset($_POST["secret"]) ? htmlentities($_POST["secret"]):"");

$res = sql_query("SELECT id, inviter,email FROM invites WHERE confirmd5 = ".sqlesc($psecret_in)) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_assoc($res);

if (!validemail($row["email"]))
stderr($tracker_lang['error'], $tracker_lang['validemail']);

if (empty($row["id"]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": psecret_in");

$email = $row["email"];
$inviter = $row["inviter"];

$secret = mksecret();
$wantpasshash = md5($secret . $wantpassword . $secret);
$editsecret = mksecret();

$ret = sql_query("INSERT INTO users (username, ip, passhash, secret, editsecret, email, status, added, invitedby, last_checked) VALUES (" .implode(",", array_map("sqlesc", array($wantusername, getip(), $wantpasshash, $secret, $editsecret, $email, "confirmed", get_date_time(), $inviter, get_date_time()))).")");

if (!$ret && mysql_errno() == 1062)
stderr($tracker_lang['error'], sprintf($tracker_lang['user_is_signup'], $wantusername));

$id = mysql_insert_id();

sql_query("DELETE FROM invites WHERE confirmd5 = ".sqlesc($psecret_in))or sqlerr(__FILE__,__LINE__);

write_log("��������������� ����� ������������ ".$wantusername, "000000", "tracker");

logincookie($id, $wantpasshash, "ip");

@header("Refresh: 0; url=userdetails.php?id=".$id);

die;
}


$psecret = (isset($_GET["psecret"]) ? htmlentities($_GET["psecret"]):"");

$res = sql_query("SELECT id, inviter, email FROM invites WHERE confirmd5 = ".sqlesc($psecret)) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_assoc($res);

if (!empty($row["id"])){

stdhead($tracker_lang['signup'],true);

echo "<script language=\"JavaScript\" src=\"js/ajax.js\" type=\"text/javascript\"></script>
<form method=\"post\" action=\"takeinvite.php?signup=yes\">
<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\">

<tr valign=\"top\"><td align=\"right\" class=\"heading\">".$tracker_lang['signup_username']."</td><td align=\"left\"><input type=\"text\" size=\"60\" class=\"login\" name=\"wantusername\" id=\"wantusername\" onblur=\"signup_check('username'); return false;\"/><div id=\"check_username\"></div></td></tr>
<tr valign=\"top\"><td align=\"right\" class=\"heading\">".$tracker_lang['signup_password']."</td><td align=\"left\"><input type=\"password\" size=\"60\" class=\"pass\" name=\"wantpassword\" id=\"wantpassword\" /></td></tr>

<tr valign=\"top\"><td align=\"right\" class=\"heading\">".$tracker_lang['signup_password_again']."</td><td align=\"left\"><input type=\"password\" class=\"pass\" size=\"60\" name=\"passagain\" id=\"passagain\" onblur=\"signup_check('password'); return false;\" /><div id=\"check_password\"></div></td></tr>

<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input type=\"hidden\" name=\"secret\" value=\"".$psecret."\" /> <input class=\"btn\" type=\"submit\" value=\"������� ������� ������\" style='height: 25px;width: 200px' /></td></tr>

</table>
</form>";

stdfoot(true);

} else 

stderr($tracker_lang['error'], $tracker_lang['validinvite']);

echo ("<div id='loading-layer'></div>")
?>