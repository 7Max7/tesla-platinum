<?
require "include/bittorrent.php";
gzip();

// 0 - ��� ������ debug ��������; 1 - �������� ����� � sql ������ 2 - �������� ������ sql ������
$DEBUG_MODE = 0;

dbconn();
loggedinorreturn();

if (get_user_class() < UC_MODERATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}


if (isset($_POST["text_reason"]) && isset($_POST["pmees"]) && !empty($_POST["text_reason"]) && get_user_class() == UC_SYSOP){

$text_reason = htmlspecialchars($_POST["text_reason"]);
$pmees = htmlspecialchars($_POST["pmees"]);

if (empty($text_reason)){
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);
die;
}

if (substr($pmees, 0, 10) <> "FROM users"){
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die;
}


$res = sql_query("SELECT id, username, avatar, ip, email ".$pmees) or sqlerr(__FILE__,__LINE__);
$count = mysql_num_rows($res);

if (empty($count)){
stderr($tracker_lang['error'], $tracker_lang['no_user_for_rules']);
die;
}


while ($arr = mysql_fetch_assoc($res)) {

if (!empty($_POST["ban_email"]) && !empty($arr["email"])){

$email = $arr["email"];

$bmail = get_row_count("bannedemails", "WHERE email = ".sqlesc($email));

if (empty($bmail)) {
sql_query("INSERT IGNORE INTO bannedemails (added, addedby, comment, email) VALUES(".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($text_reason." (".htmlspecialchars($arr["username"]).")").", ".sqlesc($email).")") or sqlerr(__FILE__, __LINE__);
}

}


if (!empty($_POST["ban_ip"]) && !empty($arr["ip"])){

$true_ban = true;

$first = trim($arr["ip"]);
$last = trim($arr["ip"]);
$first = ip2long($first);
$last = ip2long($last);

if (!is_numeric($first) || !is_numeric($last))
$true_ban = false;


$ip_bans = get_row_count("bans", "WHERE ".sqlesc($first)." >= first AND ".sqlesc($last)." <= last");

if ($first == -1 || $last == -1 || !empty($ip_bans))
$true_ban = false;

$banua = get_row_count("users", "WHERE ip >= ".sqlesc(long2ip($first))." AND ip <= ".sqlesc(long2ip($last))." AND class >= ".sqlesc(UC_MODERATOR));

if (!empty($banua)) $true_ban = false;

if ($true_ban == true) {

sql_query("INSERT INTO bans (added, addedby, first, last, bans_time, comment) VALUES (".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($first).", ".sqlesc($last).", ".sqlesc("0000-00-00 00:00:00").", ".sqlesc($text_reason).")") or sqlerr(__FILE__, __LINE__);

write_log("IP �����".(long2ip($first) == long2ip($last)? " ".long2ip($first)." ��� �������":"� � ".long2ip($first)." �� ".long2ip($last)." ���� ��������")." ������������� ".$CURUSER["username"].".", "ff3a3a", "bans");

}

}


sql_query("DELETE FROM cheaters WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM users WHERE id = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM messages WHERE receiver = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM karma WHERE user = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE friendid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM bookmarks WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM invites WHERE inviter = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM peers WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM readposts WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM report WHERE userid = ".sqlesc($arr["id"])." OR usertid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM simpaty WHERE fromuserid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM pollanswers WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM shoutbox WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM ratings WHERE user = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM snatched WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM thanks WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM checkcomm WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);

if (!empty($arr["avatar"]))
@unlink(ROOT_PATH."/pic/avatar/".$arr["avatar"]);

@unlink(ROOT_PATH."/cache/monitoring_".$arr["id"].".txt");


write_log("������������ ".$arr["username"]." (".$arr["email"].") ��� ������ ������������� ".$CURUSER["id"].". �������: ".$text_reason,"590000","bans");

}

unsql_cache("bans_first_last");
stderr($tracker_lang['success'], sprintf($tracker_lang['deleting_users_of'], "<b>".$count."</b"));
die;

}


$timenow = "".$tracker_lang['help'].": ".date("Y-m-d")." (".$tracker_lang['now'].")";

function mkdate($date){
if (strpos($date,'-'))
$a = explode('-', $date);
elseif (strpos($date,'/'))
$a = explode('/', $date);
else
return 0;
for ($i=0;$i<3;$i++)
if (!is_numeric($a[$i]))
return 0;
if (checkdate($a[1], $a[2], $a[0]))
return  date ("Y-m-d", mktime (0,0,0,$a[1],$a[2],$a[0]));
else
return 0;
}



function ratios($up,$down, $color = True) {

if ($down > 0) {
$r = number_format($up / $down, 2);
if ($color)
$r = "<font color=".get_ratio_color($r).">$r</font>";
}
else
if ($up > 0)
$r = "Inf";
else
$r = "---";
return $r;
}

// checks for the usual wildcards *, ? plus mySQL ones
function haswildcard($text){
if (strpos($text,'*') === False && strpos($text,'?') === False && strpos($text,'%') === False && strpos($text,'_') === False)
return False;
else
return True;
}


stdhead($tracker_lang['search_admin'], true);

if ($_GET['h'])
echo "<table cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
<tr>
<td valign=\"center\" class=\"b\"><b>".$tracker_lang['help_shablon']."</b>: <br />

<li>".$tracker_lang['user_shablon']."</li>
<li>".$tracker_lang['user_shablont']."</li>

</td></tr></table>";


$highlight = " class=\"a\"";

echo "<form method=get action=\"".$_SERVER["PHP_SELF"]."\">
<table cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";


if (!$_GET['h'])
echo "<tr><td valign=\"center\" colspan=\"10\" class=\"b\"><a href='".$_SERVER["PHP_SELF"]."?h=1'>".$tracker_lang['help_shablon']."</a> &nbsp;-&nbsp; <a href='".$_SERVER["PHP_SELF"]."'>".$tracker_lang['reset']."</a></td></tr>";
else
echo "<input type=\"hidden\" name=\"h\" value=\"1\" />";

echo "<tr>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['username'].":</td>";

echo "<td ".($_GET['n'] ? $highlight:"")."><input name=\"n\" type=\"text\" value=\"".htmlspecialchars($_GET['n'])."\" size=\"15\"></td>";


echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['ratio'].":</td>";

echo "<td ".($_GET['r']?$highlight:"").">";

echo "<select name=\"rt\">";
$options = array($tracker_lang['exactly'], $tracker_lang['above'], $tracker_lang['below'], $tracker_lang['between']);

for ($i = 0; $i < count($options); $i++){
echo "<option value=".$i." ".(($_GET['rt']==$i)?"selected":"").">".$options[$i]."</option>\n";
}
echo "</select>";

echo " <input name=\"r\" title=\"".$tracker_lang['enter_strlen']." ".$tracker_lang['help'].": 2.1\" type=\"text\" value=\"".htmlspecialchars($_GET['r'])."\" size=\"5\" maxlength=\"4\">";
echo " <input name=\"r2\" title=\"".$tracker_lang['enter_strlen']." ".$tracker_lang['help'].": 2.1\" type=\"text\" value=\"".htmlspecialchars($_GET['r2'])."\" size=\"5\" maxlength=\"4\">";

echo "</td>";


echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['status'].":</td>";

echo "<td ".($_GET['st']?$highlight:"")."><select name=\"st\">";

$options = array($tracker_lang['nosel_need'], $tracker_lang['confirmed'], $tracker_lang['pending']);
for ($i = 0; $i < count($options); $i++){
echo "<option value=".$i." ".($_GET['st']==$i ? "selected":"").">".$options[$i]."</option>\n";
}
echo "</select>";
echo "</td>";



echo "</tr>";

echo "<tr>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['email'].":</td>";

echo "<td ".($_GET['em'] ? $highlight:"").">";

if (get_user_class() > UC_ADMINISTRATOR)
echo "<input name=\"em\" type=\"text\" value=\"".htmlspecialchars($_GET['em'])."\" size=\"30\">";
else
echo "<b>������</b>";

echo "</td>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['user_ip'].":</td>";

echo "<td ".($_GET['ip']?$highlight:"")."><input title=\"".$tracker_lang['user_ip'].". ".$tracker_lang['help'].": ".$CURUSER["ip"]."\" name=\"ip\" type=\"text\" value=\"".htmlspecialchars($_GET['ip'])."\" size=\"15\" maxlength=\"15\"></td>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['disabled'].":</td>";

echo "<td ".($_GET['as'] ? $highlight:"").">";

echo "<select name=\"as\">";
$options = array($tracker_lang['nosel_need'], $tracker_lang['no'], $tracker_lang['yes']);
for ($i = 0; $i < count($options); $i++){
echo "<option value=".$i." ".($_GET['as']==$i ? "selected":"").">".$options[$i]."</option>\n";
}

echo "</select>";

echo "</td>";
echo "</tr>";

echo "<tr>";
echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['history'].":</td>";

echo "<td ".($_GET['co'] || $_GET['co2'] ? $highlight:"").">�: <input title=\"".$tracker_lang['modcomment']."\" name=\"co\" type=\"text\" value=\"".htmlspecialchars($_GET['co'])."\" size=\"30\"> �: <input title=\"".$tracker_lang['usercomment']."\" name=\"co2\" type=\"text\" value=\"".htmlspecialchars($_GET['co2'])."\" size=\"30\"></td>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['mask_ip'].":</td>";

echo "<td ".($_GET['ma']?$highlight:"")."><input title=\"".sprintf($tracker_lang['enter_maskip'], $tracker_lang['help'])."\" name=\"ma\" type=\"text\" value=\"".htmlspecialchars($_GET['ma'])."\" size=\"15\" maxlength=\"17\"></td>";

echo "<td valign=\"center\" class=\"a\">".$tracker_lang['class'].":</td>";

echo "<td ".($_GET['c'] <> 1 ? $highlight:"").">";

echo "<select name=\"c\"><option value=\"1\">".$tracker_lang['nosel_need']."</option>";

if (!is_valid_id($_GET['c']))
$class = '';

$class = (int) $_GET['c'];

for ($i = 2; ;++$i) {
if ($c = get_user_class_name($i-2))
echo "<option style=\"color: #".get_user_rgbcolor($i-2).";\" value='".$i."' ".($class && $class == $i ? "selected" : "").">".$c."</option>\n";
else
break;
}


echo "</select>";
echo "</td>";
echo "</tr>";

echo "<tr>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['signup'].":</td>";

echo "<td ".($_GET['d'] ? $highlight:"").">";

echo "<select name=\"dt\">";

$options = array($tracker_lang['in'], $tracker_lang['before'], $tracker_lang['after'], $tracker_lang['between']);
for ($i = 0; $i < count($options); $i++){
echo "<option value=".$i." ".($_GET['dt']==$i ? "selected":"").">".$options[$i]."</option>\n";
}
echo "</select> ";

echo "<input title=\"".$tracker_lang['date'].", ".$timenow."\" name=\"d\" type=\"text\" value=\"".htmlspecialchars($_GET['d'])."\" size=\"12\" maxlength=\"12\"> ";
echo "<input title=\"".$tracker_lang['date'].", ".$timenow."\" name=\"d2\" type=\"text\" value=\"".htmlspecialchars($_GET['d2'])."\" size=\"12\" maxlength=\"12\">";

echo "</td>";


echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['uploaded'].":</td>";

echo "<td ".($_GET['ul'] ? $highlight:"").">";

echo "<select name=\"ult\" id=\"ult\">";
$options = array($tracker_lang['exactly'], $tracker_lang['morea'], $tracker_lang['lower'], $tracker_lang['between']);
for ($i = 0; $i < count($options); $i++){
echo "<option value=".$i." ".($_GET['ult']==$i ? "selected":"").">".$options[$i]."</option>\n";
}
echo "</select>";

echo " <input name=\"ul\" title=\"".$tracker_lang['enter_strlen']." ".$tracker_lang['help'].": 2.5\" type=\"text\" id=\"ul\" size=\"6\" maxlength=\"7\" value=\"".htmlspecialchars($_GET['ul'])."\">";
echo " <input name=\"ul2\" title=\"".$tracker_lang['enter_strlen']." ".$tracker_lang['help'].": 2.5\" type=\"text\" id=\"ul2\" size=\"6\" maxlength=\"7\" value=\"".htmlspecialchars($_GET['ul2'])."\">";
echo "</td>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['donor'].":</td>";

echo "<td ".($_GET['do']?$highlight:"").">";

echo "<select name=\"do\">";

$options = array($tracker_lang['nosel_need'], $tracker_lang['yes'], $tracker_lang['no']);
for ($i = 0; $i < count($options); $i++){
echo "<option value=$i ".(((int)$_GET['do']=="$i")?"selected":"").">".$options[$i]."</option>\n";
}

echo "</select>";
echo "</td>";
echo "</tr>";

echo "<tr>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['online_now'].":</td>";

echo "<td ".($_GET['ls']?$highlight:"").">";
echo "<select name=\"lst\">";

$options = array($tracker_lang['in'], $tracker_lang['before'], $tracker_lang['after'], $tracker_lang['between']);
for ($i = 0; $i < count($options); $i++){
echo "<option value=".$i." ".($_GET['lst']==$i ? "selected":"").">".$options[$i]."</option>\n";
}

echo "</select>";

echo " <input name=\"ls\" title=\"".$tracker_lang['date'].",  ".$timenow."\"  type=\"text\" value=\"".htmlspecialchars($_GET['ls'])."\" size=\"12\" maxlength=\"10\">";
echo " <input name=\"ls2\" title=\"".$tracker_lang['date'].",  ".$timenow."\"  type=\"text\" value=\"".htmlspecialchars($_GET['ls2'])."\" size=\"12\" maxlength=\"10\">";

echo "</td>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['downloaded'].":</td>";

echo "<td ".($_GET['dl']?$highlight:"").">";

echo "<select name=\"dlt\">";
$options = array($tracker_lang['exactly'], $tracker_lang['morea'], $tracker_lang['lower'], $tracker_lang['between']);
for ($i = 0; $i < count($options); $i++){
echo "<option value=".$i." ".($_GET['dlt']==$i ? "selected":"").">".$options[$i]."</option>\n";
}
echo "</select>";

echo " <input name=\"dl\" title=\"".$tracker_lang['enter_strlen']." ".$tracker_lang['help'].": 2.5\" type=\"text\" size=\"6\" maxlength=\"7\" value=\"".htmlspecialchars($_GET['dl'])."\">";
echo " <input title=\"".$tracker_lang['enter_strlen']." ".$tracker_lang['help'].": 2.5\" name=\"dl2\" type=\"text\" size=\"6\" maxlength=\"7\" value=\"".htmlspecialchars($_GET['dl2'])."\">";

echo "</td>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['warning_is'].":</td>";

echo "<td ".($_GET['w']?$highlight:"").">";

echo "<select name=\"w\">";
$options = array($tracker_lang['nosel_need'],$tracker_lang['yes'], $tracker_lang['no']);
for ($i = 0; $i < count($options); $i++){
echo "<option value=".$i." ".($_GET['w']==$i ? "selected":"").">".$options[$i]."</option>\n";
}
echo "</select>";

echo "</td>";
echo "</tr>";
echo "<tr> ";
echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['num_warned'].":</td>";

echo "<td ".($_GET['as1']?$highlight:"").">";
echo "<select name=\"as1\">";



$options = array($tracker_lang['nosel_need'],"1","2","3","4","5");

for ($i = 0; $i < count($options); $i++) {
echo "<option value=".$i." ".($_GET['as1']==$i ? "selected":"").">".$options[$i]."</option>\n";
}

echo "</select>";
echo "</td>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['seeding'].":</td>";
echo "<td ".($_GET['ac'] ? $highlight:"")."><input name=\"ac\" type=\"checkbox\" value=\"1\" ".($_GET['ac']?"checked":"")."></td>";

echo "<td valign=\"middle\" class=\"a\">".$tracker_lang['banned'].": </td>";
echo "<td ".($_GET['dip'] ? $highlight:"")."><input name=\"dip\" type=\"checkbox\" value=\"1\" ".($_GET['dip']?"checked":"")."></td>";

echo "</tr>";

echo "<tr><td colspan=\"6\" align=center><input type=\"submit\" class=\"btn\" style=\"height: 25px; width:300px\" value=\"".$tracker_lang['search_users']."\"/></td></tr>";

echo "</table>";
echo "<br /><br />";
echo "</form>";



if (isset($_GET) && count($_GET) >= 1) {
// name
//  $names = explode(' ',trim($_GET['n']));
$names = explode(' ',trim(htmlspecialchars($_GET['n'])));
if ($names[0] !== "")  {
foreach($names as $name){
if (substr($name,0,1) == '~') {
if ($name == '~') continue;
$names_exc[] = substr($name,1);
} else
$names_inc[] = $name;
}
if (is_array($names_inc)) {
$where_is .= isset($where_is)?" AND (":"(";
foreach($names_inc as $name) {
if (!haswildcard($name))
$name_is .= (isset($name_is)?" OR ":"")."u.username = ".sqlesc($name);
else {
$name = str_replace(array('?','*'), array('_','%'), $name);
$name_is .= (isset($name_is)?" OR ":"")."u.username LIKE ".sqlesc($name);
}
}
$where_is .= $name_is.")";
unset($name_is);
}
if (is_array($names_exc)) {
$where_is .= isset($where_is)?" AND NOT (":" NOT (";

foreach($names_exc as $name) {
if (!haswildcard($name))
$name_is .= (isset($name_is)?" OR ":"")."u.username = ".sqlesc($name);
else {
$name = str_replace(array('?','*'), array('_','%'), $name);
$name_is .= (isset($name_is)?" OR ":"")."u.username LIKE ".sqlesc($name);
}
}
$where_is .= $name_is.")";
}

$q .= ($q ? "&" : "") . "n=".urlencode(trim(htmlspecialchars($_GET['n'])));
}

if (isset($_GET['h']))
$q .= ($q ? "&" : "") . "h=1";

if (get_user_class() > UC_ADMINISTRATOR) {

$emaila = explode(' ', trim(htmlspecialchars($_GET['em'])));
if ( $emaila[0] !== "") {
$where_is .= isset($where_is)?" AND (":"(";

foreach($emaila as $email) {
if (strpos($email,'*') === False && strpos($email,'?') === False
&& strpos($email,'%') === False) {
if (!validemail($email)) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": email");
stdfoot(true);
die();
}
$email_is .= (isset($email_is)?" OR ":"")."u.email =".sqlesc($email);
} else {
$sql_email = str_replace(array('?','*'), array('_','%'), $email);
$email_is .= (isset($email_is)?" OR ":"")."u.email LIKE ".sqlesc($sql_email);
}
}

$where_is .= $email_is.")";
$q .= ($q ? "&" : "") . "em=".urlencode(trim(htmlspecialchars($_GET['em'])));
}
}
//class
// NB: the c parameter is passed as two units above the real one
$class = (int) $_GET['c'] - 2;
if (is_valid_id($class + 1)) {
$where_is .= (isset($where_is)?" AND ":"")."u.class=$class";
$q .= ($q ? "&" : "") . "c=".($class+2);
}
// IP

$ip = trim($_GET['ip']);
if ($ip) {
$regex = "/^(((1?\d{1,2})|(2[0-4]\d)|(25[0-5]))(\.\b|$)){4}$/";
if (!preg_match($regex, $ip)) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ip");
stdfoot(true);
die();
}
$mask = trim($_GET['ma']);
if ($mask == "" || $mask == "255.255.255.255")
$where_is .= (isset($where_is)?" AND ":"")."u.ip = '$ip'";
else {
if (substr($mask,0,1) == "/") {
$n = substr($mask, 1, strlen($mask) - 1);
if (!is_numeric($n) or $n < 0 or $n > 32) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": mask");
stdfoot(true);
die();
}
else
$mask = long2ip(pow(2,32) - pow(2,32-$n));
} elseif (!preg_match($regex, $mask)) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": mask");
stdfoot(true);
die();
}
$where_is .= (isset($where_is)?" AND ":"")."INET_ATON(u.ip) & INET_ATON('$mask') = INET_ATON('$ip') & INET_ATON('$mask')";
$q .= ($q ? "&" : "") . "ma=$mask";
}
$q .= ($q ? "&" : "") . "ip=$ip";
}

// ratio
$ratio = trim(htmlspecialchars($_GET['r']));
if ($ratio) {
if ($ratio == '---') {
$ratio2 = "";
$where_is .= isset($where_is)?" AND ":"";
$where_is .= " u.uploaded = 0 and u.downloaded = 0";
} elseif (strtolower(substr($ratio,0,3)) == 'inf') {
$ratio2 = "";
$where_is .= isset($where_is)?" AND ":"";
$where_is .= " u.uploaded > 0 and u.downloaded = 0";
} else {
if (!is_numeric($ratio) || $ratio < 0) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ratio");
stdfoot(true);
die();
}
$where_is .= isset($where_is)? " AND ":"";
$where_is .= " (u.uploaded/u.downloaded)";
$ratiotype = (int) $_GET['rt'];

$q .= ($q ? "&" : "") . "rt=$ratiotype";

if ($ratiotype == "3") {

$ratio2 = trim(htmlspecialchars($_GET['r2']));
if(!$ratio2) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ratio 1 ".$tracker_lang['or']." ratio 2");
stdfoot(true);
die();
}

if (!is_numeric($ratio2) or $ratio2 < $ratio) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ratio 2");
stdfoot(true);
die();
}

$where_is .= " BETWEEN $ratio and $ratio2";
$q .= ($q ? "&" : "") . "r2=$ratio2";
} elseif ($ratiotype == "2")
$where_is .= " < $ratio";
elseif ($ratiotype == "1")
$where_is .= " > $ratio";
else
$where_is .= " BETWEEN ($ratio - 0.004) and ($ratio + 0.004)";
}
$q .= ($q ? "&" : "") . "r=$ratio";
}



$comments = explode(' ',trim(htmlspecialchars($_GET['co'])));
if ($comments[0] !== "") {
foreach($comments as $comment) {

if (substr($comment,0,1) == '~') {
if ($comment == '~') continue;
$comments_exc[] = substr($comment,1);
} else
$comments_inc[] = $comment;
}
if (is_array($comments_inc)) {
$where_is .= isset($where_is)?" AND (":"(";

foreach($comments_inc as $comment) {
if (!haswildcard($comment))
$comment_is .= (isset($comment_is)?" OR ":"")."u.modcomment LIKE ".sqlesc("%".$comment."%");
else {
$comment = str_replace(array('?','*'), array('_','%'), $comment);
$comment_is .= (isset($comment_is)?" OR ":"")."u.modcomment LIKE ".sqlesc($comment);
}
}

$where_is .= $comment_is.")";
unset($comment_is, $comment);
}
if (is_array($comments_exc)) {
$where_is .= isset($where_is)?" AND NOT (":" NOT (";
foreach($comments_exc as $comment) {
if (!haswildcard($comment))
$comment_is .= (isset($comment_is)?" OR ":"")."u.modcomment LIKE ".sqlesc("%".$comment."%");
else {
$comment = str_replace(array('?','*'), array('_','%'), $comment);
$comment_is .= (isset($comment_is)?" OR ":"")."u.modcomment LIKE ".sqlesc($comment);
}
}
$where_is .= $comment_is.")";
}
$q .= ($q ? "&" : "") . "co=".htmlspecialchars_uni(trim($_GET['co']));
}



$comment_2s = explode(' ',trim(htmlspecialchars($_GET['co2'])));
if ($comment_2s[0] !== "") {
foreach($comment_2s as $comment_2) {

if (substr($comment_2,0,1) == '~') {
if ($comment_2 == '~') continue;
$comment_2s_exc[] = substr($comment_2,1);
} else
$comment_2s_inc[] = $comment_2;
}
if (is_array($comment_2s_inc)) {
$where_is .= isset($where_is)?" AND (":"(";

foreach($comment_2s_inc as $comment_2) {
if (!haswildcard($comment_2))
$comment_2_is .= (isset($comment_2_is)?" OR ":"")."u.usercomment LIKE ".sqlesc("%".$comment_2."%");
else {
$comment_2 = str_replace(array('?','*'), array('_','%'), $comment_2);
$comment_2_is .= (isset($comment_2_is)?" OR ":"")."u.usercomment LIKE ".sqlesc($comment_2);
}
}

$where_is .= $comment_2_is.")";
unset($comment_2_is, $comment_2);
}
if (is_array($comment_2s_exc)) {
$where_is .= isset($where_is)?" AND NOT (":" NOT (";
foreach($comment_2s_exc as $comment_2) {
if (!haswildcard($comment_2))
$comment_2_is .= (isset($comment_2_is)?" OR ":"")."u.usercomment LIKE ".sqlesc("%".$comment_2."%");
else {
$comment_2 = str_replace(array('?','*'), array('_','%'), $comment_2);
$comment_2_is .= (isset($comment_2_is)?" OR ":"")."u.usercomment LIKE ".sqlesc($comment_2);
}
}
$where_is .= $comment_2_is.")";
}
$q .= ($q ? "&" : "") . "co2=".htmlspecialchars_uni(trim($_GET['co2']));
}

$unit = 1073741824;		// 1GB

$ul = trim((int)$_GET['ul']);
if ($ul) {
if (!is_numeric($ul) || $ul < 0) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": upload");
stdfoot(true);
die();
}
$where_is .= isset($where_is)?" AND ":"";
$where_is .= " u.uploaded ";
$ultype = (int)$_GET['ult'];
$q .= ($q ? "&" : "") . "ult=$ultype";

if ($ultype == "3") {
$ul2 = (int) $_GET['ul2'];

if(!$ul2) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": upload 1 ".$tracker_lang['or']." upload 2 ");
stdfoot(true);
die();
}

if (!is_numeric($ul2) or $ul2 < $ul) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": upload 2");
stdfoot(true);
die();
}

$where_is .= " BETWEEN ".$ul*$unit." and ".$ul2*$unit;
$q .= ($q ? "&" : "") . "ul2=$ul2";
} elseif ($ultype == "2")
$where_is .= " < ".$ul*$unit;
elseif ($ultype == "1")
$where_is .= " >". $ul*$unit;
else
$where_is .= " BETWEEN ".($ul - 0.004)*$unit." and ".($ul + 0.004)*$unit;
$q .= ($q ? "&" : "") . "ul=$ul";
}


$dl = (int) $_GET['dl'];

if ($dl) {
if (!is_numeric($dl) || $dl < 0) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": download");
stdfoot(true);
die();
}
$where_is .= isset($where_is)?" AND ":"";
$where_is .= " u.downloaded ";
$dltype =  (int)($_GET['dlt']);
$q .= ($q ? "&" : "") . "dlt=$dltype";

if ($dltype == "3") {

$dl2 = (int)$_GET['dl2'];

if(!$dl2) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": download 1 ".$tracker_lang['or']." download 2");
stdfoot(true);
die();
}

if (!is_numeric($dl2) or $dl2 < $dl) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_id_value'].": download 2");
stdfoot(true);
die();
}
$where_is .= " BETWEEN ".$dl*$unit." and ".$dl2*$unit;
$q .= ($q ? "&" : "") . "dl2=$dl2";
}
elseif ($dltype == "2")
$where_is .= " < ".$dl*$unit;
elseif ($dltype == "1")
$where_is .= " > ".$dl*$unit;
else
$where_is .= " BETWEEN ".($dl - 0.004)*$unit." and ".($dl + 0.004)*$unit;
$q .= ($q ? "&" : "") . "dl=$dl";
}
// date joined
$date = trim($_GET['d']);
if ($date) {
if (!$date = mkdate($date)) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_date']);
stdfoot(true);
die();
}


$q .= ($q ? "&" : "") . "d=$date";
$datetype = (int)$_GET['dt'];
$q .= ($q ? "&" : "") . "dt=$datetype";

if ($datetype == "0")

// For mySQL 4.1.1 or above use instead
// $where_is .= (isset($where_is)?" AND ":"")."DATE(added) = DATE('$date')";
$where_is .= (isset($where_is)?" AND ":"")."(UNIX_TIMESTAMP(added) - UNIX_TIMESTAMP('$date')) BETWEEN 0 and 86400";
else {
$where_is .= (isset($where_is)?" AND ":"")."u.added ";
if ($datetype == "3") {
$date2 = mkdate(trim($_GET['d2']));
if ($date2) {
if (!$date = mkdate($date)) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_date']);
stdfoot(true);
die();
}
$q .= ($q ? "&" : "") . "d2=$date2";
$where_is .= " BETWEEN '$date' and '$date2'";
} else {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_date'].": 1 ".$tracker_lang['or']." 2");
stdfoot(true);
die();
}
}
elseif ($datetype == "1")
$where_is .= "< '$date'";
elseif ($datetype == "2")
$where_is .= "> '$date'";
}
}
// date last seen
$last = trim($_GET['ls']);
if ($last) {
if (!$last = mkdate($last)) {
stdmsg($tracker_lang['error'], $tracker_lang['invalid_date']);
stdfoot(true);
die();
}

$q .= ($q ? "&" : "") . "ls=".$last;
$lasttype = (int)$_GET['lst'];
$q .= ($q ? "&" : "") . "lst=".$lasttype;

if ($lasttype == "0")
$where_is .= (isset($where_is)?" AND ":"")."(UNIX_TIMESTAMP(last_access) - UNIX_TIMESTAMP('$last')) BETWEEN 0 and 86400";
else {
$where_is .= (isset($where_is)?" AND ":"")."u.last_access ";
if ($lasttype == "3") {
$last2 = mkdate(trim($_GET['ls2']));
if ($last2) {
$where_is .= " BETWEEN '$last' and '$last2'";
$q .= ($q ? "&" : "") . "ls2=$last2";
} else {
stdmsg($tracker_lang['error'], $tracker_lang['announce_missing_parameter'].": 2 ".$tracker_lang['date']);
stdfoot(true);
die();
}
} elseif ($lasttype == "1")
$where_is .= "< '$last'";
elseif ($lasttype == "2")
$where_is .= "> '$last'";
}
}
// status
$status = (int) $_GET['st'];
if ($status) {
$where_is .= ((isset($where_is))?" AND ":"");
if ($status == "1")
$where_is .= "u.status = 'confirmed'";
else
$where_is .= "u.status = 'pending'";
$q .= ($q ? "&" : "") . "st=".$status;
}
// account status
$accountstatus = (int) $_GET['as'];
if ($accountstatus) {
$where_is .= (isset($where_is)) ? " AND ":"";
if ($accountstatus == "1")
$where_is .= " u.enabled = 'yes'";
else
$where_is .= " u.enabled = 'no'";
$q .= ($q ? "&" : "") . "as=".$accountstatus;
}
//donor
$donor = (int)$_GET['do'];
if ($donor) {
$where_is .= (isset($where_is))?" AND ":"";
if ($donor == 1)
$where_is .= " u.donor = 'yes'";
else
$where_is .= " u.donor = 'no'";
$q .= ($q ? "&" : "") . "do=$donor";
}
//warned
$warned =  (int)$_GET['w'];
if ($warned) {
$where_is .= (isset($where_is))?" AND ":"";
if ($warned == 1)
$where_is .= " u.warned = 'yes'";
else
$where_is .= " u.warned = 'no'";
$q .= ($q ? "&" : "") . "w=$warned";
}
$num_warned = (int) $_GET['as1'] - 1;
if (is_valid_id($num_warned + 1)) {
$where_is .= (isset($where_is)?" AND ":"")."u.num_warned=$num_warned";
$q .= ($q ? "&" : "") . "as1=".($num_warned+1);
}

// disabled IP

$disabled = htmlspecialchars($_GET['dip']);
if ($disabled) {
$distinct = "DISTINCT ";
$join_is .= " LEFT JOIN users AS u2 ON u.ip = u2.ip";
$where_is .= ((isset($where_is))?" AND ":"")."u2.enabled = 'no'";
$q .= ($q ? "&" : "") . "dip=$disabled";
}

// active

$active = (int) $_GET['ac'];

if ($active == "1") {
$distinct = "DISTINCT ";
$join_is .= " LEFT JOIN peers AS p ON u.id = p.userid";
$q .= ($q ? "&" : ""). "ac=$active";
}

$from_is = "users AS u".$join_is;
$distinct = isset($distinct)?$distinct:"";
$queryc = "SELECT COUNT(".$distinct."u.id) FROM ".$from_is.(($where_is == "")?"":" WHERE $where_is ");
$querypm = "FROM ".$from_is.(($where_is == "")?" ":" WHERE $where_is ");
$select_is = "u.id, u.username,u.class, u.email, u.status, u.added, u.last_access, u.ip, u.class, u.uploaded, u.downloaded, u.donor, u.modcomment, u.usercomment, u.enabled, u.warned, u.num_warned";
$query = "SELECT ".$distinct." ".$select_is." ".$querypm;


if ($DEBUG_MODE > 0) {
stdmsg("������ ��������",$queryc);
echo "<br /><br />";
stdmsg("��������� ������",$query);
echo "<br /><br />";
stdmsg("URL ",$q);
if ($DEBUG_MODE == 2)
die();
echo "<br /><br />";
}



$res = sql_query($queryc) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_row($res);

$count = $arr[0];

$q = isset($q)?($q."&"):"";
$perpage = 30;
list($pagertop, $pagerbottom, $limit) = pager($perpage, $count, $_SERVER["PHP_SELF"]."?".$q);
$query.= $limit;

$res = sql_query($query) or sqlerr(__FILE__, __LINE__);

$num = 0;

if (mysql_num_rows($res) == 0)
stdmsg($tracker_lang['warning'], $tracker_lang['no_user_intable']);
else {

if ($count > $perpage)
echo $pagertop;

echo "<table width=\"100%\" cellspacing=0 cellpadding=5>\n";

echo "<tr>
<td class=colhead align=center>".$tracker_lang['signup_username']."</td>
<td class=colhead align=center>".$tracker_lang['ratio']."</td>
<td class=colhead align=center>".$tracker_lang['user_ip']."</td>
<td class=colhead align=center>".$tracker_lang['email']."</td>
<td class=colhead align=center>".$tracker_lang['signup']."</td>
<td class=colhead align=center>".$tracker_lang['online_now']."</td>
<td class=colhead align=center>".$tracker_lang['confirmation']."</td>
<td class=colhead align=center>".$tracker_lang['onn']." / ".$tracker_lang['offf']."</td>
<td class=colhead>".$tracker_lang['ratio']." *</td>
<td class=colhead>".$tracker_lang['uploaden']."*</td>
<td class=colhead>".$tracker_lang['downloaden']."*</td>
</tr>";

while ($user = mysql_fetch_array($res)) {

if ($num%2==0){
$cla1 = " class=\"b\" ";
$cla2 = " class=\"a\" ";
} else {
$cla2 = " class=\"b\" ";
$cla1 = " class=\"a\" ";
}


if ($user['added'] == '0000-00-00 00:00:00')
$user['added'] = '---';
if ($user['last_access'] == '0000-00-00 00:00:00')
$user['last_access'] = '---';

if ($user['ip']) {
$nip = ip2long($user['ip']);
$auxres = sql_query("SELECT COUNT(*) FROM bans WHERE ".sqlesc($nip)." >= first AND ".sqlesc($nip)." <= last") or sqlerr(__FILE__, __LINE__);
$array = mysql_fetch_row($auxres);

if ($array[0] == 0)
$ipstr = "<a title=\"".$tracker_lang['search_ip']."\" target='_blank' href=\"$DEFAULTBASEURL/usersearch.php?&ip=".$user['ip']."\">".$user['ip']."</a>";
}
else
$ipstr = "---";

$auxres = sql_query("SELECT SUM(uploaded) AS pul, SUM(downloaded) AS pdl FROM peers WHERE userid = ".sqlesc($user['id'])) or sqlerr(__FILE__, __LINE__);
$array = mysql_fetch_array($auxres);
$pul = $array['pul'];
$pdl = $array['pdl'];
$n_posts = $n[0];

if (get_user_class() <= UC_ADMINISTRATOR && $user['class'] > UC_ADMINISTRATOR)
$user['email'] = '<i>'.$tracker_lang['email_hide'].'</i>';
else
$user['email'] = "<b>".$user['email']."</b>";

if ($user['enabled']=="yes")
$user['enabled'] = "".$tracker_lang['onn']."";
else
$user['enabled'] = "<b>".$tracker_lang['offf']."</b>";

if ($user['status']=="confirmed")
$user['status'] = $tracker_lang['confirmed'];
else
$user['status'] = "<b>".$tracker_lang['pending']."</b>";

echo "<tr>
<td ".$cla1."><b><a href=\"userdetails.php?id=".$user['id']."\">".get_user_class_color($user['class'], $user['username'])."</td>
<td ".$cla2." align=\"center\">".ratios($user['uploaded'], $user['downloaded'])."</td>
<td ".$cla1." align=\"center\">".$ipstr."</td>
<td ".$cla2." align=\"center\">".$user['email']."</td>
<td ".$cla1.">".$user['added']."</td>
<td ".$cla2.">".$user['last_access']."</td>
<td ".$cla1.">".$user['status']."</td>
<td ".$cla2.">".$user['enabled']."</td>
<td ".$cla1.">".ratios($pul,$pdl)."</td>
<td ".$cla2.">".mksize($pul)."</td>
<td ".$cla1.">".mksize($pdl)."</td>
</tr>\n";

$modcomment = htmlspecialchars($user["modcomment"]);
$usercomment = htmlspecialchars($user["usercomment"]);

echo "<tr><td style=\"padding:1px;\" colspan=\"11\"><table width=\"100%\" cellspacing=\"0\" cellpadding=\"10\">";

echo "<td style=\"padding:1px;\"  class=\"b\" width=\"50%\" align=\"center\">
<div class=\"spoiler-wrap\" id=\"ad_".$user['id']."\"><div class=\"spoiler-head folded clickable\">".$tracker_lang['modcomment'].": ".$user['username']."</div><div class=\"spoiler-body\" style=\"display: none;\"><textarea cols=90% rows=20 readonly>".$modcomment."</textarea></div></div></td>";

if (get_user_class() > UC_MODERATOR)
echo "<td style=\"padding:1px;\" class=\"b\" width=\"50%\" align=\"center\"><div class=\"spoiler-wrap\" id=\"us_".$user['id']."\"><div class=\"spoiler-head folded clickable\">".$tracker_lang['usercomment'].": ".$user['username']."</div><div class=\"spoiler-body\" style=\"display: none;\"><textarea cols=90% rows=20 readonly>".$usercomment."</textarea></div></div></td>";

echo "</table></td></tr>\n";

++$num;
}

echo "</table>";

if ($count > $perpage)
echo $pagerbottom;



if (get_user_class() >= UC_ADMINISTRATOR){
echo "<br /><br />
<form method=\"post\" action=\"message.php\">
<table border=\"1\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\">
<tr>
<td class=\"b\">".$tracker_lang['mass_pm']."</td>
<td class=\"b\"><input name=\"pmees\" type=\"hidden\" value=\"".$querypm."\" size=\"10\">
<input name=\"PM\" type=\"submit\" value=\"".$tracker_lang['sending_pm']."\" class=\"btn\">
<input name=\"n_pms\" type=\"hidden\" value=\"".$count."\" size=\"10\">
<input name=\"action\" type=\"hidden\" value=\"mass_pm\" size=\"10\">
</td>
</tr>
</table>
</form>";
}


if (get_user_class() == UC_SYSOP){
echo "<br />
<form method=\"post\" action=\"usersearch.php\">
<table border=\"1\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\">

<tr>
<td class=\"b\">".$tracker_lang['my_reason'].": </td>
<td class=\"b\">
<input name=\"text_reason\" type=\"text\" size=\"100\" maxlength=\"100\">
</td>
</tr>

<tr>
<td class=\"b\">".$tracker_lang['more']."</td>
<td class=\"b\">
<label><input type=\"checkbox\" name=\"ban_ip\">".$tracker_lang['banned_for_ip']."</label>
<label><input type=\"checkbox\" name=\"ban_email\">".$tracker_lang['banned_for_email']."</label>
</td>
</tr>

<tr>
<td class=\"b\">".$tracker_lang['deleting_account']."</td>
<td class=\"b\"><input name=\"pmees\" type=\"hidden\" value=\"".$querypm."\" size=\"10\">
<input name=\"PM\" type=\"submit\" value=\"".$tracker_lang['delete']." (".$tracker_lang['selected'].")\" class=\"btn\">
</td>
</tr>

</table>
</form>";
}

}

}

echo $pagemenu."<br />".$browsemenu;

stdfoot(true);

?>