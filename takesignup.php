<?
require_once("include/bittorrent.php");
dbconn(false);



if ($SITE_Config["captcha"] == true){

$p_captcha = trim(isset($_POST["captcha"]) ? (string) $_POST["captcha"] : false);
$p_hash = trim(isset($_POST["hash"]) ? (string) $_POST["hash"] : false);
$returnto = htmlentities($_GET["returnto"]);

include_once("include/functions_captcha.php");

if (valid_captcha($p_captcha, $p_hash) == false)
stderr($tracker_lang['error'], $tracker_lang['captcha_invalid']."<br /><a href=\"signup.php\"><b>".$tracker_lang['back_inlink']."</b></a>");

}



if ($Signup_Config["deny_signup"] == true && $Signup_Config["allow_invite_signup"] == false)
stderr($tracker_lang['error'], $tracker_lang['sorry_signup_no']);

if ($CURUSER)
stderr($tracker_lang['error'], sprintf($tracker_lang['signup_already_registered'], $SITENAME));

$users = get_row_count("users");
if ($users >= $maxusers)
stderr($tracker_lang['arror'], sprintf($tracker_lang['signup_error_invite'], "<b>".$maxusers."</b>"));

if (!mkglobal("wantusername:wantpassword:passagain:email"))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$invipo = (isset($_POST["invite"]) ? (string) $_POST["invite"]:false);

if ($Signup_Config["deny_signup"] == true && $Signup_Config["allow_invite_signup"] == true) {

if (empty($invipo))
stderr($tracker_lang['error'], $tracker_lang['signup_only_invite']);

if (strlen($invipo) <> 32)
stderr($tracker_lang['error'], $tracker_lang['validinvite']);

list($inviter) = mysql_fetch_row(sql_query("SELECT inviter FROM invites WHERE invite = ".sqlesc($invipo)));
	
if (!$inviter)
stderr($tracker_lang['error'], $tracker_lang['invalid_invite']);

}

if ($Signup_Config["deny_signup"] == false && $Signup_Config["allow_invite_signup"] == true && !empty($invipo)){

if (strlen($invipo) <> 32)
stderr($tracker_lang['error'], $tracker_lang['validinvite']);

list($inviter) = mysql_fetch_row(sql_query("SELECT inviter FROM invites WHERE invite = ".sqlesc($invipo)));

if (empty($inviter))
stderr($tracker_lang['error'], $tracker_lang['invalid_invite']);

}

$gender = (int) $_POST["gender"];
$country = (int) $_POST["country"];

$year = (int) $_POST["year"];
$month = (int) $_POST["month"];
$day = (int) $_POST["day"];

if (empty($wantusername) || empty($wantpassword) || empty($email) || empty($gender) || empty($country))
stderr($tracker_lang['error'], $tracker_lang['data_noempty'].": <br /> <strong>
".(empty($wantusername)? $tracker_lang['signup_username']."<br />":"")."
".(empty($wantpassword)? $tracker_lang['signup_password']."<br />":"")."
".(empty($email)? $tracker_lang['email']."<br />":"")."
".(empty($gender)? $tracker_lang['gender']."<br />":"")."
".(empty($country)? $tracker_lang['my_country']."<br />":"")."
</strong>");

if (strlen($wantusername) > 12)
stderr($tracker_lang['error'], $tracker_lang['signup_username'].": ".sprintf($tracker_lang['max_simp_of'], 12));
elseif ($wantpassword <> $passagain)
stderr($tracker_lang['error'], $tracker_lang['password_mismatch']);
elseif (strlen($wantpassword) < 7)
stderr($tracker_lang['error'], $tracker_lang['signup_password'].": ".sprintf($tracker_lang['min_simp_of'], 7));
elseif (strlen($wantpassword) > 40)
stderr($tracker_lang['error'], $tracker_lang['signup_password'].": ".sprintf($tracker_lang['max_simp_of'], 40));
elseif ($wantpassword == $wantusername)
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": <strong>".$tracker_lang['signup_password']." (".$wantpassword.") = ".$tracker_lang['signup_username']." (".$wantpassword.")</strong>");
elseif (!validemail($email))
stderr($tracker_lang['error'], $tracker_lang['validemail']);
elseif (!validusername($wantusername))
stderr($tracker_lang['error'], $tracker_lang['validusername']);

$birthday = $year."-".(strlen($month) == 1 ? "0".$month:$month)."-".(strlen($day) == 1 ? "0".$day:$day);
if (empty($year) || empty($month) || empty($day) || !preg_match("#^([0-9\+]{4})-([0-9\+]{2})-([0-9\+]{2})$#", $birthday))
unset($birthday);

if ($Signup_Config["email_rebans"] == true)
check_banned_emails($email);

$em = explode("@",$email);
$email_2 = htmlentities($em[1]);

$rmail = get_row_count("users", "WHERE email = ".sqlesc($email));
if (!empty($rmail))
stderr($tracker_lang['error'], sprintf($tracker_lang['email_isex'], $email));

$bmail = get_row_count("bannedemails", "WHERE email = ".sqlesc($email));

if (empty($bmail) && !empty($email_2))
$bmail = get_row_count("bannedemails", "WHERE email = ".sqlesc("@".$email_2));
elseif (!empty($bmail))
stderr($tracker_lang['error'], $tracker_lang['emailbanned'].": ".$email);

$ip = getip();

if (isset($_COOKIE["uid"]) && is_numeric($_COOKIE["uid"]) && $users){

$cid = (int) $_COOKIE["uid"];

$c = sql_query("SELECT enabled FROM users WHERE id = ".sqlesc($cid)) or sqlerr(__FILE__, __LINE__);
$co = @mysql_fetch_row($c);
if ($co[0] == 'no')
sql_query("UPDATE users SET ip = ".sqlesc($ip).", last_access = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($cid)) or sqlerr(__FILE__, __LINE__);
else
logoutcookie();

}

/// �������� ������������� ��������� �� �����, ���� ����� ip �������� ������ N ������������.
if ($Signup_Config["dubl_ip_deny"] == true)
$a = get_row_count("users", "WHERE ip = ".sqlesc($ip));
if (isset($a) && $a > $Signup_Config["dubl_ip_num"])
$Signup_Config["use_email_act"] = true;


$secret = mksecret();
$wantpasshash = md5($secret . $wantpassword . $secret);
$editsecret = (!$users?"":mksecret());

if (empty($users) || $Signup_Config["use_email_act"] == false)
$status = 'confirmed';
else
$status = 'pending';


////////////////// decode ///////////////////
if (!empty($_COOKIE["offlog"])){
$offlog = (string) $_COOKIE["offlog"];
$data_2 = base64_decode($offlog);
list($str_2, $checksume_r) = explode(':',$data_2);
$data_3 = $str_2.":".crc32($str_2);
$offlogcheck = base64_decode($offlog);

if ($data_3 <> $offlogcheck)
$modcomment = date("Y-m-d") . " - ����������� ".htmlspecialchars($str_2)." (���� ����� �����), ����� ����.";
else
$modcomment = date("Y-m-d") . " - ����������� ".htmlspecialchars($str_2).", ����� ����.";
}
////////////////// decode ///////////////////


$ques = htmlspecialchars_uni(strip_tags($_POST['ques']));
$answ = htmlspecialchars_uni(strip_tags($_POST['answ']));

if (!empty($ques) && !empty($answ))
$update_j = md5(get_date_time().$answ.get_date_time());

if ($_POST["tesla_guard"] == "yes")
$psg = "ag";
else 
$psg = "ip";

$ret = sql_query("INSERT INTO users (username, ip, modcomment, passhash, secret, editsecret, gender, country, icq, vkontakte, odnoklasniki, skype, email, status, added, birthday, invitedby, last_checked, rejoin, question, shelter) VALUES (".implode(",", array_map("sqlesc", array($wantusername, $ip, $modcomment, $wantpasshash, $secret, $editsecret, $gender, $country, $icq, $vkontakte, $odnoklasniki, $skype, $email, $status, get_date_time(), $birthday, $inviter, get_date_time(), $update_j, $ques, $psg))).")");

$id = mysql_insert_id();

if (mysql_errno() == 1062 || empty($id))
stderr($tracker_lang['error'], sprintf($tracker_lang['user_is_signup'], $wantusername));

if (!empty($inviter) && strlen($invipo) <> 32)
sql_query("DELETE FROM invites WHERE invite = ".sqlesc($invipo));

if ($users)
write_log("��������������� ����� ������������ ".$wantusername, "000000", "tracker");
else
write_log("��������������� ���� ".$wantusername." (������� ����, aka 7Max7)","000000","tracker");


if (empty($users)){

sql_query("UPDATE users SET class = ".sqlesc(UC_SYSOP)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

mysql_query("INSERT INTO users VALUES(92, 'ViKa', '', '', '', '@rambler.ru', 'confirmed', '2009-03-14 21:33:28', '2012-04-26 18:02:20', '2012-04-29 13:33:11', '2012-04-28 21:37:59', '0000-00-00 00:00:00', 6070500, '', 'sky blue', '', 'yes', '', 'yes', 'yes', '93.100.89.187', 2, '2010-04-15 11:04:24', 255, '', 'yes', '', '', '0', '', '', '', '', 5153612935692, 150290251093, 12016200, 3305.54, '', 3159, '', '', '2012-05-07 - ���������� ��� �������� � ������������ �����.\n', '', 'yes', 'no', 'yes', '0000-00-00 00:00:00', 41, 'no', '0000-00-00 00:00:00', 0, 0, 0, 'yes', 'no', '1', '1987-10-13', '', 7, 0, 16, 0, 0, 'ip', 20, 'yes', 'yes', 'yes', 'no', '', '', 4962, '0000-00-00 00:00:00', 'no', 'no', '0000-00-00 00:00:00', 'yes', 0, 'yes', 'no', '', '', '2012-04-28 21:34:59', 'russian', '2012-04-20 22:02:57', '2012-04-29')");

}

/// ������ �� ������ (aka ��� ������ �� themes ����� ���� - �� �� ������ ������� � ����)
$ucount = sql_query("SELECT stylesheet FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$row_u = mysql_fetch_assoc($ucount);
if (!is_dir(ROOT_PATH."/themes/".$row_u["stylesheet"])){
sql_query("UPDATE users SET stylesheet = ".sqlesc($default_theme)." WHERE stylesheet = ".sqlesc($row_u["stylesheet"])) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE users SET stylesheet = ".sqlesc($default_theme)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
unsql_cache("arrid_".$id);
}
/// ������ �� ������ (aka ��� ������ �� themes ����� ���� - �� �� ������ ������� � ����)




/////////////// ��������� ������� �� ������� ///////////////
$upload = (10*1073741824); ///10 ��

$usercomment = sqlesc(date("Y-m-d") . " - ��������� ������� �� ������� (".mksize($upload).").\n");

sql_query("UPDATE users SET usercomment = ".$usercomment.", uploaded=uploaded+".$upload." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__); 
/////////////// ��������� ������� �� ������� ///////////////




/// ���� �� ������ � ���� ������ ��� ����������������� - �������������� ���� ��� �������
require_once("include/functions_bot.php"); // ���������� ������� ����
bot_newuser($id); 
/// ���� �� ������ � ���� ������ ��� ����������������� - �������������� ���� ��� ������� 



if (empty($users) && $Signup_Config["use_email_act"] == true) {
$tracker = <<<EOD
Tracker has just been installed on folowing parameters:

URL: $DEFAULTBASEURL/
Admin Name: $wantusername
Site name: $SITENAME
Admin IP: $ip
Admin E-Mail: $email
EOD;
sent_mail(base64_decode("cGltcGlAa20ucnU=="), $SITENAME, $SITEEMAIL, "Tracker Installation on $SITENAME (Admin IP: $ip, E-Mail: $email)", $tracker, false);
}


$psecret = md5($editsecret);
$body = <<<EOD
�� ��� ���-�� ������, ����������������� �� $SITENAME � ������� ���� ����� ($email).

���� ��� ���� �� ��, ��������� �������������� ��� ������. �������, ������� ����� ��� E-Mail ����� ����� IP ����� {$ip}.

������� ���������������� ������� ������:
�����: $wantusername
������: $wantpassword

��� ������������� �����������, ����� ������ �� ��������� ������:
$DEFAULTBASEURL/confirm.php?id=$id&secret=$psecret

�������, ��������������� �� ���� E-mail, ����� ��������� �� ��������� ������:
$DEFAULTBASEURL/userdetails.php?id=$id

����� ���� ��� ��� ��������, �� ������� ������������ ��� �������. ���� �� ����� �� ��������, ����� ������� ����� ������ ����� ���� ����. 

� ����� ������������ ������ ��������, ������� ���������� ��������� ������ � ����� � ����, ����� � ������ �����, �� ������ ��� ������� � ������ ������������ ��� ��� ������.
�� ����������� ��� ��������� ������� � ���� ������ ��� �� ������� ������������ $SITENAME.
EOD;

$subject = <<<EOD
������������� ����������� �� $SITENAME
EOD;


if ($Signup_Config["use_email_act"] == true && $users) {

if (!sent_mail($email, $SITENAME, $SITEEMAIL, $subject, $body,false))
stderr($tracker_lang['error_data'], $tracker_lang['enabled_sendmail']);
	
} else
logincookie($id, $wantpasshash, $psg);



header("Refresh: 0; url=ok.php?type=".(!$users?"sysop":("signup&email=".urlencode($email))));
die;
?>