<?php

require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

$newsid = (int) $_GET['id'];

if (!is_numeric($newsid) || empty($newsid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

if (get_user_class() < UC_USER)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

stdhead($tracker_lang['news'].": ".$tracker_lang['commenting']);

if (!empty($newsid)) {

$sql = sql_query("SELECT * FROM news WHERE id = ".sqlesc($newsid)." ORDER BY id") or sqlerr(__FILE__, __LINE__);

echo ("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">");

echo ("<tr><td class=\"colhead\">".$tracker_lang['news'].": ".$tracker_lang['commenting']."</td></tr>\n");

if (mysql_num_rows($sql) == 0) {
echo ("<tr><td colspan=\"2\">".$tracker_lang['no_news']."</td></tr></table>");
stdfoot();
exit;
}

$news = mysql_fetch_assoc($sql);

echo ("<tr><td colspan=2>
<div class=\"spoiler-wrap\" id=\"".$news['id']."\"><div class=\"spoiler-head folded clickable\">".$news['added'].": ".$news['subject']."</div><div class=\"spoiler-body\" style=\"display: none;\">".format_comment($news['body'])."</div></div>
</td></tr>\n");

echo ("</table><br />\n");

$subres = sql_query("SELECT COUNT(*) FROM comments WHERE news = ".sqlesc($newsid)." AND torrent = '0' AND poll = '0' AND offer = '0'") or sqlerr(__FILE__, __LINE__);
$subrow = mysql_fetch_array($subres);
$count = $subrow[0];

if ($CURUSER["postsperpage"]=="0")
$limited = 15;  
else
$limited = (int) $CURUSER["postsperpage"];


if (!$count) {

echo ("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
echo ("<tr><td class=colhead align=\"left\" colspan=\"2\">");
echo ("<div style=\"float: left; width: auto;\" align=\"left\"> :: ".$tracker_lang['comments_list']."</div>");

if ($CURUSER["commentpos"] == 'yes')
echo ("<div align=\"right\"><a href=#comments class=altlink_white>".$tracker_lang['add_comment']."</a></div>");

echo ("</td></tr><tr><td align=\"center\">");
echo ($tracker_lang['no_comments'].". ".($CURUSER["commentpos"] == 'yes' ? "<a href=#comments>".$tracker_lang['add_comment']."</a>" : "")."");
echo ("</td></tr></table><br />");

}  else {

list($pagertop, $pagerbottom, $limit) = pager($limited, $count, "newsoverview.php?id=$newsid&", array("lastpagedefault" => 1));

$subres = sql_query("SELECT nc.id, nc.ip, nc.text, nc.user, nc.added, nc.editedby, nc.editedat, u.avatar, u.warned, u.username, u.enabled, u.title, u.class, u.donor,u.signature,u.signatrue, u.downloaded, u.uploaded, u.gender, u.last_access, e.username AS editedbyname, (SELECT class FROM users WHERE id=nc.editedby) AS classbyname
FROM comments AS nc LEFT JOIN users AS u ON nc.user = u.id LEFT JOIN users AS e ON nc.editedby = e.id 
WHERE news = ".sqlesc($newsid)." AND torrent = '0' AND poll = '0' AND offer = '0' ORDER BY nc.id ".$limit) or sqlerr(__FILE__, __LINE__);

$allrows = array();

while ($subrow = mysql_fetch_array($subres))
$allrows[] = $subrow;

echo ("<table class=main cellspacing=\"0\" cellPadding=\"5\" width=\"100%\">");
echo ("<tr><td class=\"colhead\" align=\"center\">");
echo ("<div style=\"float: left; width: auto;\" align=\"left\"> :: ".$tracker_lang['comments_list']."</div>");

if ($CURUSER["commentpos"] == 'yes')
echo ("<div align=\"right\"><a href=#comments class=altlink_white>".$tracker_lang['add_comment']."</a></div>");

echo ("</td></tr>");

echo ("<tr><td>");
echo ($pagertop);
echo ("</td></tr>");

echo ("<tr><td>");
commenttable($allrows, "newscomment");
echo ("</td></tr>");

echo ("<tr><td>");
echo ($pagerbottom);
echo ("</td></tr>");
echo ("</table>");
}

if ($CURUSER["commentpos"] == 'yes'){

echo ("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
echo ("<tr><td class=colhead align=\"left\" colspan=\"2\"><a name=comments>&nbsp;</a><b>:: ".$tracker_lang['add_comment']."</b></td></tr>");
echo ("<tr><td width=\"100%\" align=\"center\" >");
echo ("<form name=news method=\"post\" action=\"newscomment.php?action=add\">");
echo ("<table border=\"0\"><tr><td class=\"clear\">");
echo ("<div align=\"center\">". textbbcode("news","text","", 1) ."</div>");
echo ("</td></tr></table>");
echo ("</td></tr><tr><td align=\"center\" colspan=\"2\">");
echo ("<input type=\"hidden\" name=\"nid\" value=\"".$newsid."\"/>");
echo ("<input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['send_comment']."\" class=\"btn\" value=\"".$tracker_lang['send_comment']."\" />");
echo ("</td></tr></form></table>");

}

}

stdfoot();
?>