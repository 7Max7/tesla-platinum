<?
if (!defined('BLOCK_FILE')) {
 Header("Location: ../index.php");
 exit;
}

global $tracker_lang;
$blocktitle = $tracker_lang['top_seeders'];


$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-topsid", "time" => 60*10, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

$seeds_sql = sql_query("SELECT *, COUNT(*) AS tnum FROM peers WHERE seeder = 'yes' GROUP BY userid ORDER BY `tnum` DESC LIMIT 10");

$content .= "<table width=\"100%\">
<td align=\"center\" class=\"b\" width=\"80%\">".$tracker_lang['username']."</td>
<td align=\"center\" class=\"b\" width=\"20%\">".$tracker_lang['uploadeder']."</td>\n";
$sf=0;

while ($seeds_array = mysql_fetch_array($seeds_sql)) {

if (!empty($seeds_array['userid'])){

$sa = mysql_fetch_array(sql_query("SELECT username, class FROM users WHERE id = ".$seeds_array['userid']));

$content .= "<tr>
<td align=\"center\" width=\"70%\"><a href=\"userdetails.php?id=".$seeds_array['userid']."\">".get_user_class_color($sa["class"], $sa["username"])."</a></td>
<td align=\"center\" width=\"30%\">".round($seeds_array['tnum'])."</td></tr>\n";
++$sf;

}

}

if ($sf==0){
$content .= "<tr><td align=\"center\" width=\"70%\" colspan=\"2\">".$tracker_lang['no_data']."</td></tr>\n";
}

$content .= "</table>\n";

file_query($content, $cache = array("type" => "disk", "file" => "block-topsid", "time" => 60*10, "action" => "set"));
}

?> 