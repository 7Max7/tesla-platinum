<script>
// zoomi - A zoom for images ~ Sean Catchpole - Version 0.9
(function(jQuery){
jQuery.fn.zoomi = function() {
  jQuery(this).filter("img").each(function(){
    if(!this.z) {
      jQuery(this).zoom1().mouseover(function(){jQuery(this).zoom2().show();});
      jQuery(this.z).mouseout(function(){jQuery(this).hide();}); }
  });
 return this;
}
jQuery.fn.zoom1 = function() {
  jQuery(this).each(function(){
    var e = this;
    jQuery(e).css({'position':'relative','z-index':'8'}).after('<img class="'+e.className+'">');
    e.z = e.nextSibling;
    jQuery(e.z).removeClass("zoomi").addClass("zoom2").attr("src",e.alt || e.src)
    .css({'position':'absolute','z-index':'10'});
    jQuery(e.z).hide();
  });
  return this;
}
jQuery.fn.zoom2 = function() {
  var s = [];
  this.each(function(){
    var e = this;
    if(!e.z) e = jQuery(e).zoom1()[0]; s.push(e.z);
    if(!e.z.complete) return;
    if(!e.z.width) { jQuery(e.z).show(); e.z.width=e.z.width; jQuery(e.z).hide(); }
    jQuery(e.z).css({left:jQuery(e).offsetLeft()-(e.z.width-e.scrollWidth)/2+'px',
    top:jQuery(e).offsetTop()-(e.z.height-e.scrollHeight)/2+'px'});
  });
  return this.pushStack(s);
}
jQuery.fn.offsetLeft = function() {
  var e = this[0];
  if(!e.offsetParent) return e.offsetLeft;
  return e.offsetLeft + jQuery(e.offsetParent).offsetLeft(); }
jQuery.fn.offsetTop = function() {
  var e = this[0];
  if(!e.offsetParent) return e.offsetTop;
  return e.offsetTop + jQuery(e.offsetParent).offsetTop(); }
jQuery(function(){ jQuery('img.zoomi').zoomi(); });
})(jQuery);
</script>
<div align="center">
<?
/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $CURUSER;
if ($CURUSER && !empty($CURUSER["notifs"])){
preg_match_all('/\[cat([0-9]+)\]/i', $CURUSER["notifs"], $notif);
if (count($notif[1])) $notifs = $notif[1];
}


if (count($notifs))
$bs_cat = sql_query("SELECT id, name, image FROM categories WHERE id IN (".implode(",", $notifs).")", $cache = array("type" => "disk", "time" => 24*7200)) or sqlerr(__FILE__, __LINE__);
else
$bs_cat = sql_query("SELECT id, name, image FROM categories", $cache = array("type" => "disk", "file" => "browse_cat_array", "time" => 24*7200)) or sqlerr(__FILE__, __LINE__);


while ($b_cat = mysql_fetch_assoc_($bs_cat)){
echo "<a href=\"browse.php?cat=".$b_cat["id"]."\"><img class=\"zoomi\" width=\"45\" height=\"45\" src=\"/pic/cats/".$b_cat["image"]."\" title=\"".htmlspecialchars($b_cat["name"])."\" style=\"position: relative; z-index: 8;\"/></a>";
}

?>
</div>