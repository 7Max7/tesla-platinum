<?
require_once("include/bittorrent.php");
dbconn();
header("Content-Type: text/html; charset=" . $tracker_lang['language_charset']);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] <> 'XMLHttpRequest')
die ('XMLHttpRequest');

include_once("include/functions_captcha.php");
$acaptcha = creating_captcha();

$old_hash = (isset($_POST['hash']) ? (string) $_POST['hash'] : false);
$old_upd = (isset($_POST['old']) ? (string) $_POST['old'] : false);

if (isset($_POST['update'])){

echo $acaptcha["echo"]."\n";
//echo "<input type=\"hidden\" name=\"hash\" value=\"".$acaptcha["hash"]."\" />\n";
echo "<input type=\"hidden\" id=\"old\" value=\"".$acaptcha["hash"]."\" />\n";

echo '<script type="text/javascript">
jQuery("input[name=hash]").val("'.$acaptcha["hash"].'");
</script>';

}

if (!empty($old_upd))
sql_query("DELETE FROM captcha WHERE hash = ".sqlesc($old_upd)) or sqlerr(__FILE__,__LINE__);

if (!empty($old_hash) && empty($old_upd))
sql_query("DELETE FROM captcha WHERE hash = ".sqlesc($old_hash)) or sqlerr(__FILE__,__LINE__);

?>