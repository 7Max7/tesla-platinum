<?
require_once("include/bittorrent.php");

define('IN_FORUM', true);





/////////////////
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {

dbconn(false, true);
header("Content-Type: text/html; charset=" .$tracker_lang['language_charset']);

$dt = get_date_time(gmtime() - 180);

$res_s = sql_query("SELECT id, username, class FROM users WHERE forum_access > ".sqlesc($dt)." ORDER BY forum_access DESC") or sqlerr(__FILE__,__LINE__);
$title_who_s = array();
while ($ar_r = mysql_fetch_assoc($res_s)) {
$title_who_s[] = "<a href=\"userdetails.php?id=".$ar_r['id']."\">".get_user_class_color($ar_r["class"], $ar_r["username"])."</a>";
}

$res_s = sql_query("SELECT ip FROM sessions WHERE time > ".sqlesc($dt)." AND LEFT(url, 11) = '/forums.php' AND uid = '-1' ORDER BY time DESC", $cache = array("type" => "disk", "file" => "forums_sessinbot", "time" => 60)) or sqlerr(__FILE__,__LINE__);

while ($ar_r = mysql_fetch_assoc_($res_s)) {
$title_who_s[] = $ar_r['ip'];
}

if (count($title_who_s)){
$title_who_s = array_unique($title_who_s);
echo implode(", ", $title_who_s);
} else echo $tracker_lang['no_data'];


die;
}
/////////////////


require_once(ROOT_PATH."/include/functions_forum.php");
dbconn(false);

if ($Forum_Config["guest"] == false)
loggedinorreturn();

get_cleanup();

if ($Forum_Config["on"] == false){
stderr($tracker_lang['warning'], $tracker_lang['forum_off']." ".(!empty($Forum_Config["off_reason"]) ? $tracker_lang['reason_off'].": ".$Forum_Config["off_reason"]:""));
die;
}



$action = isset($_GET["action"]) ? htmlentities(strip_tags($_GET["action"])) : '';
$forum_pic_url = "/pic/forumicons/";
$maxsubjectlength = 255;

$postsperpage = $CURUSER["postsperpage"];
if (empty($postsperpage) || $postsperpage > 100)
$postsperpage = 25;






switch($action) {



case 'edittopic': {

if (get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);


if ($CURUSER["forum_com"]<>"0000-00-00 00:00:00"){

if ($CURUSER){
header("Refresh: 15; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 15), $tracker_lang['banned_on_forum'].": ".$CURUSER["forum_com"].".");
die;
} else {
stderr($tracker_lang['error_data'], $tracker_lang['not_allow_write']." ".$tracker_lang['access_denied']);
die;
}

}

$topicid = (int) $_GET['topicid'];

if (!is_valid_id($topicid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$res = sql_query("SELECT * FROM topics WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res);

if (empty($arr))
stderr($tracker_lang['error'], $tracker_lang['no_message_with_such_id']);

$topic_name=format_comment_light($arr["subject"]);

stdhead($tracker_lang['editing']);


$modcomment=htmlspecialchars($arr["t_com"]);
$forums=htmlspecialchars($arr["subject"]);
$sticky=$arr["sticky"];
$visible=$arr["visible"];
$locked=$arr["locked"];

echo("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");

echo ("<tr><td class=\"b\" align=\"center\" colspan=\"2\"><a class=\"alink\" href=\"forums.php?action=viewtopic&topicid=".$topicid."\">".$tracker_lang['back_inlink']."</a></td></tr>");

echo("<tr><td class=\"colhead\" align=\"center\" colspan=\"2\"><a name=comments></a><b>.::: ".$tracker_lang['admining_topic']." :::.</b></td></tr>");

echo("<form method=\"post\" action=\"forums.php?action=edittopicmod&topicid=".$topicid."\">");
echo("<tr><td>");
echo("<tr><td class=\"a\"><b>".$tracker_lang['attaching_thread']."</b>:
<input type=radio name=sticky value='yes' " . ($sticky=="yes" ? " checked" : "") . "> ".$tracker_lang['yes']." <input type=radio name=sticky value='no' " . ($sticky=="no" ? " checked" : "") . "> ".$tracker_lang['no']." </td></tr>");

echo ("<tr><td class=\"b\"><b>".$tracker_lang['visible_topic']."</b>: <input type=radio name='visible' value='yes' " . ($visible=="yes" ? " checked" : "") . "> ".$tracker_lang['yes']." <input type=radio name='visible' value='no' " . ($visible=="no" ? " checked" : "") . "> ".$tracker_lang['no']." </td></tr>");

echo("<tr><td class=\"a\"><b>".$tracker_lang['blocked_topic']."</b>: <input type=radio name=locked value='yes' " . ($locked=="yes" ? " checked" : "") . "> ".$tracker_lang['yes']." <input type=radio name=locked value='no' " . ($locked=="no" ? " checked" : "") . "> ".$tracker_lang['no']." </td></tr>");

echo("<tr><td class=\"b\"><b>".$tracker_lang['rename_topic']."</b>: <input type=text name=subject size=60 maxlength='".$maxsubjectlength."' value=\"".$forums."\"></td></tr>");
// }

$res = sql_query("SELECT id, name, minclasswrite, minclassread, (SELECT COUNT(*) FROM topics WHERE topics.forumid=forums.id) AS exif FROM forums ORDER BY sort, name", $cache = array("type" => "disk", "file" => "forums.id_name", "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);
while ($arr = mysql_fetch_assoc_($res))
if ($arr["id"] <> $forumid && $CURUSER >= $arr["minclasswrite"])
$select.=("<option ".(empty($arr["exif"]) ? "style=\"font-weight:bold;\"":"")." value=" . $arr["id"] . ">" . $arr["name"] . "</option>\n");

echo("<tr><td class=\"a\">
<b>".$tracker_lang['move_topic']."</b>: <select name=forumid>
<option value=0>".$tracker_lang['no_choose']."</option>".$select."</select>
</td></tr>");


echo("<tr><td class=\"b\">
<b>".$tracker_lang['delete_topic']."</b>: <input name=\"delete_topic\" value=\"1\" type=\"checkbox\"><br />
<input type=text name=reson size=60 maxlength='$maxsubjectlength' value=\"".$tracker_lang['delete']."\"> <i>".$tracker_lang['reason_main']."</i>
</td></tr>");

echo("<tr><td class=\"a\" align=\"center\">".$tracker_lang['history_topic']." [".strlen($modcomment)."]<br />
<textarea cols=100% rows=6".(get_user_class() < UC_SYSOP ? " readonly" : " name=modcomment").">$modcomment</textarea>
</td></tr>
<tr><td class=\"b\" align=\"center\"><b>".$tracker_lang['add_note']."</b>: <br /><textarea cols=100% rows=3 name=modcomm></textarea></td></tr>");


echo("<tr><td align=\"center\" colspan=\"2\">");
echo("<input type=\"hidden\" value=\"".$topicid."\" name=\"topicid\"/>");
echo("<input type=\"submit\" class=btn value=\"".$tracker_lang['perform_action']."\" />");
echo("</td></tr></table></form><br />");

stdfoot();
die;

}
exit();
break;


case 'forum_fcom': {

if (get_user_class() >= UC_MODERATOR) {

$forumfid = (isset($_POST["forumfid"]) ? intval($_POST["forumfid"]):0);

if (!is_valid_id($forumfid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);


$mod_comment=htmlspecialchars($_POST["modcomment"]);
$mod_comm=htmlspecialchars($_POST["modcomm"]);

$res_fo = sql_query("SELECT f_com FROM forums WHERE id = ".sqlesc($forumfid)) or sqlerr(__FILE__, __LINE__);
$arr_for = mysql_fetch_assoc($res_fo);

if (empty($arr_for))
stderr($tracker_lang['error'], $tracker_lang['no_this_cat']);

else {

if (get_user_class() == UC_SYSOP)
$modik = $mod_comment;
else
$modik = $arr_for["f_com"]; /// �������� ����������� ��������� ������

if (!empty($mod_comm)) {
$modik = date("Y-m-d") . " - ������� �� ".$CURUSER["username"].": ".$mod_comm."\n".$modik;
sql_query("UPDATE forums SET f_com = ".sqlesc(strip_tags($modik))." WHERE id = ".sqlesc($forumfid)) or sqlerr(__FILE__, __LINE__);
}

unsql_cache("forumid_".$forumfid);

}

header("Refresh: 3; url=forums.php?action=viewforum&forumid=$forumfid");

stderr(sprintf($tracker_lang['succes_redir_of_sec'], 3), (!empty($mod_comm)? $tracker_lang['note_added']:$tracker_lang['note_update']).".<br /> <a href=\"forums.php?action=viewforum&forumid=".$forumfid."\">".$tracker_lang['click_on_wait']."</a>.");
die;
}


}
exit();
break;








case 'catchup': {

catch_up();

header("Refresh: 2; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 2), $tracker_lang['forum_uniread'].".<br /><a href=\"forums.php\">".$tracker_lang['click_on_wait']."</a>.");
die();

}
exit();
break;









case 'viewforum': {

$forumid = (isset($_GET["forumid"]) ? intval($_GET["forumid"]):0);

if (!is_valid_id($forumid)){
header("Location: forums.php");
die;
}

$page = isset($_GET["page"]) ? (int) $_GET["page"] : 0;
$userid = $CURUSER["id"];

$res = sql_query("SELECT * FROM forums WHERE id = ".sqlesc($forumid), $cache = array("type" => "disk", "file" => "forumid_".$forumid, "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);

if (!mysql_num_rows_($res)){
stderr($tracker_lang['warning'], $tracker_lang['access_denied']);
die;
}

$arr = mysql_fetch_assoc_($res);
$f_com = $arr["f_com"];

$forumname = htmlspecialchars($arr["name"]);
$description = htmlspecialchars($arr["description"]);

if ($arr["minclassread"] > get_user_class() && $arr["minclassread"] <> "0"){
header("Location: forums.php");
die();
}


$res = sql_query("SELECT COUNT(*) FROM topics WHERE forumid = ".sqlesc($forumid)) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_row($res);

$num = $arr[0];
$count = $arr[0];

list($pagertop, $pagerbottom, $limit) = pager($postsperpage, $count, "forums.php?action=viewforum&forumid=".$forumid."&");

$topicsres = sql_query("SELECT *,(SELECT COUNT(*) FROM posts WHERE posts.topicid = topics.id) AS num_po FROM topics WHERE forumid = ".sqlesc($forumid)." ORDER BY sticky, lastpost DESC ".$limit) or sqlerr(__FILE__, __LINE__);

if ($CURUSER && get_date_time(gmtime() - 60) >= $CURUSER["forum_access"]){
sql_query("UPDATE users SET forum_access = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
unsql_cache("arrid_".$CURUSER["id"]);
}

$numtopics = mysql_num_rows($topicsres);


stdhead($forumname);


if (get_user_class() == UC_SYSOP){
echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";

echo "<tr><td class=\"a\" align=\"center\"><a class=\"altlink_white\" href=\"forummanage.php\">".$tracker_lang['admining_category']." / ".$tracker_lang['creating_category']."</a></td></tr>";

echo "</table>";
}


fpanelka ();

echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
echo "<tr><td class=\"a\" align=\"center\"><h1>".$forumname."</h1><small>".$description."</small></td></tr>";
echo "</table>";


$comment_f = $f_com;

if ($CURUSER){
$array_read = array();
$r = sql_query("SELECT lastpostread, topicid FROM readposts WHERE userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
while ($a = mysql_fetch_assoc($r))
$array_read[$a["topicid"]] = $a["lastpostread"];
}

if ($numtopics > 0) {

if (get_user_class() == UC_SYSOP){

?>
<script language="Javascript" type="text/javascript">
<!-- Begin
var checkflag = "false";
var marked_row = new Array;
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
}
else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
}
}

</script>
<?

echo("<form method=\"post\" name=\"yepi\" action=\"forums.php?action=b_actions\">");

}

echo "<table cellspacing=\"0\" cellpadding=\"0\" class=\"forums\" width=\"100%\">";

echo "<tr><td colspan=\"5\">".$pagertop."</td></tr>";

echo "<tr>";
echo "<td class=\"colhead\" width=\"44px\" align=\"center\">".$tracker_lang['status']."</td>";
echo "<td class=\"colhead\">".$tracker_lang['subject']." / ".$tracker_lang['news_poster']."</td>";
echo "<td class=\"colhead\" align=\"center\">".$tracker_lang['answers']." / ".$tracker_lang['views']."</td>";
echo "<td class=\"colhead\">".$tracker_lang['subscribe_last_comment']."</td>";

if (get_user_class() == UC_SYSOP)
echo("<td class=\"colhead\" align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['mark_all']."\" onclick=\"this.value=check(document.yepi.elements);\"></td>\n");

echo "</tr>";

$da=0;

while ($topicarr = mysql_fetch_assoc($topicsres)) {

$topicid = $topicarr["id"];
$topic_userid = $topicarr["userid"];
$topic_views = $topicarr["views"];
$views = number_format($topic_views);
$locked = $topicarr["locked"] == "yes";
$sticky = $topicarr["sticky"] == "yes";

$polls_view = ($topicarr["polls"] == "yes" ? " <img width=\"13\" title=\"".$tracker_lang['poll']."\" src=\"/pic/forumicons/polls.gif\">":"");

$posts = $topicarr["num_po"];

$replies = max(0, $posts - 1);

$tpages = floor($posts / $postsperpage);

if ($tpages * $postsperpage != $posts)
++$tpages;

if ($tpages > 1) {
$topicpages = " [";
for ($i = 1; $i <= $tpages; ++$i){
$topicpages .= "".($i==1 ? "":" ")."<a title=\"$i\" href=\"forums.php?action=viewtopic&topicid=$topicid&page=$i\">$i</a>";
}
$topicpages .= "]";
} else
$topicpages = "";


$res = sql_query("SELECT p.*, t.forumid, p.body AS bodypost, t.visible, u.username AS ed_username, u.class AS ed_class, o.username AS or_username, o.class AS or_class
FROM posts AS p
LEFT JOIN topics AS t ON t.id = p.topicid
LEFT JOIN users AS u ON u.id = p.userid
LEFT JOIN users AS o ON o.id = ".sqlesc($topic_userid)."
WHERE p.topicid = ".sqlesc($topicid)." ORDER BY p.id ASC LIMIT 1") or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_assoc($res);

$subject_f = $forumname;

$combody_f = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", format_comment_light($arr["bodypost"])));

$pos1 = @strpos($combody_f, '.', 250); // $pos = 7, not 0
$pos2 = @strpos($combody_f, '.', 350); // $pos = 7, not 0

if (!empty($pos1) && strlen($combody_f) >= 250)
$combody_f = substr($combody_f, 0, ($pos1+1));
elseif (!empty($pos2) && strlen($combody_f) >= 350)
$combody_f = substr($combody_f, 0, ($pos2+1));
else
$combody_f = substr($combody_f, 0, 255);


$lppostid = $arr["id"];
$lppostadd = $arr["added"];
$lpuserid = $arr["userid"];
$lpadded = "<nobr>".($arr['added'])."</nobr>";


if (!empty($arr["ed_username"]) && !empty($topic_userid))
$lpusername = "<a href=\"userdetails.php?id=".$lpuserid."\"><b>".get_user_class_color($arr["ed_class"], $arr["ed_username"])."</b></a> <a href=\"forums.php?action=search_post&userid=$topic_userid\"><img title=\"".$tracker_lang['search_user_msg']."\" src=\"".$DEFAULTBASEURL."/pic/pm.gif\" /></a>";
elseif (empty($lpuserid))
$lpusername = "<font color=gray>[<b>System</b>]</font>";
elseif (!empty($lpuserid))
$lpusername = "id: ".$lpuserid;

$lpauthor = "";
if (!empty($arr["or_username"]) && !empty($topic_userid))
$lpauthor = "<a href=\"userdetails.php?id=".$topic_userid."\"><b>".get_user_class_color($arr["or_class"], $arr["or_username"])."</b></a>";
elseif (!empty($topic_userid))
$lpauthor = "id: ".$topic_userid;

//if (($lppostid > $a["lastpostread"]) AND ($postadd > get_date_time(gmtime() - $Forum_Config["readpost_expiry"])))
///$topicid � $array_read


//isset($array_read[$topicid]) &&
$new = ((($CURUSER && $lppostid > $array_read[$topicid]) || !$CURUSER) && $lppostadd > get_date_time(gmtime() - $Forum_Config["readpost_expiry"])) ? 1 : 0;

$topicpic = ($locked ? ($new ? "lockednew" : "locked") : ($new ? "unlockednew" : "unlocked"));
$view = ($locked ? ($new ? $tracker_lang['mail_unread_desc'] : $tracker_lang['block_is_topic']) : ($new ? $tracker_lang['mail_unread_desc'] : $tracker_lang['no_data_unread']));

$subject = ($sticky ? "<b>".$tracker_lang['stickyf']."</b>: " : "")."<a class=\"alink\" title=\"".$tracker_lang['go_to']."\" href=\"forums.php?action=viewtopic&topicid=$topicid&page=last\"><b>".trim($topicarr["subject"])."</b></a> ".$topicpages;

if ($da%2==0){
$cl1 = "class=a";
$cl2 = "class=b";
} else {
$cl2 = "class=a";
$cl1 = "class=b";
}

echo "<tr>";
echo "<td ".$cl1." align=\"center\" width=\"44px\"><img title=\"$view\" src=\"{$forum_pic_url}{$topicpic}.gif\" /></td>";

echo "<td ".$cl2.">".format_comment_light($subject).$polls_view." ".($arr["visible"] == "no" ? "[<b>".$tracker_lang['hide_topic']."</b>]":"")."<br />".$lpauthor." &#8594; <small>".$combody_f."</small></td>";

echo "<td ".$cl1." align=\"center\">".$replies." / ".$views."</td>";

echo "<td ".$cl2.">".$lpusername."<br />".$lpadded."</td>";

if (get_user_class() == UC_SYSOP)
echo "<td ".$cl1." align=\"center\"><input type=\"checkbox\" class=\"desacto\" name=\"cheKer[]\" value=\"".$topicid."\" /></td>";

echo "</tr>";

++$da;
}

echo "<tr><td colspan=\"5\">".$pagerbottom."</td></tr>";


if (get_user_class() == UC_SYSOP){

global $tracker_lang;

$view_lang = array_map("trim", explode("|", $tracker_lang['forum_baction']));

echo "<tr><td align=\"right\" colspan=\"5\">";

echo "<select name=\"actions\" id=\"myoptn\">\n
<option selected>".$tracker_lang['signup_not_selected']."</option>
<option value=\"main\">".$view_lang[0]."</option>
<option value=\"unmain\">".$view_lang[1]."</option>
<option value=\"lock\">".$view_lang[2]."</option>
<option value=\"unlock\">".$view_lang[3]."</option>
<option value=\"delpoll\">".$view_lang[4]."</option>
<option value=\"dellsmgs\">".$view_lang[5]."</option>
<option value=\"noown\">".$view_lang[6]."</option>
<option value=\"movcat\">".$view_lang[7]."</option>
<option value=\"delete\">".$view_lang[8]."</option>
<option value=\"del_wip\">".$view_lang[9]."</option>
<option value=\"del_wem\">".$view_lang[10]."</option>
<option value=\"del_wall\">".$view_lang[11]."</option>
</select>\n";

echo "<select name=\"pcat\" id=\"nmovcat\" style=\"display: none;\">\n";
echo "<option selected>".$tracker_lang['no_choose']."</option>";

$res = sql_query("SELECT id, name, minclasswrite, minclassread, (SELECT COUNT(*) FROM topics WHERE topics.forumid=forums.id) AS exif FROM forums ORDER BY sort, name", $cache = array("type" => "disk", "file" => "forums.id_name", "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);
while ($arr = mysql_fetch_assoc_($res))
echo "<option ".(empty($arr["exif"]) ? "style=\"font-weight:bold;\"":"")." value=\"".$arr["id"]."\" ".($currentforum == $arr["id"] ? " selected>" : ">").$arr["name"]."</option>\n";

echo "</select>\n";

echo "<input type=\"submit\" class=\"btn\" onClick=\"return confirm('".$tracker_lang['warn_sure_action']."')\" value=\"".$tracker_lang['b_action']."\" /></td></tr>\n";
}


echo "</table><br />";

if (get_user_class() == UC_SYSOP)
echo ("</form>");


} else
echo ("<table class=\"main\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tr><td class=\"embedded\"><div align=\"center\" class=\"error\"><b>".$tracker_lang['no_messages']."</b></div></td></tr>
</table><br />");

$arr = get_forum_access_levels($forumid);
$maypost = get_user_class() >= $arr["write"] && get_user_class() >= $arr["create"];

if (!$maypost && $CURUSER || ($CURUSER["forum_com"]<>"0000-00-00 00:00:00" && $CURUSER))
echo "<p><h3>".$tracker_lang['access_denied']."</h3></p>\n";


if ($CURUSER["forum_com"] == "0000-00-00 00:00:00" && $CURUSER){

echo "<form name=\"comment\" method=\"post\" action=\"forums.php?action=post\"><table cellspacing=\"0\" cellpadding=\"0\" class=\"forums\" width=\"100%\">";

echo "<tr><td align=\"center\" class=\"colhead\"><a name=\"comments\"></a><b>.::: ".$tracker_lang['creating_topic']." :::.</b></td></tr>";

echo "<tr><td align=\"left\" class=\"b\"><b>".$tracker_lang['name']."</b>: <input title=\"".$tracker_lang['enter']." ".$tracker_lang['name']."\" type=\"text\" size=\"80\" maxlength=\"".$maxsubjectlength."\" name=\"subject\"> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>";

echo "<tr><td align=\"center\" class=\"a\">";
echo(textbbcode("comment","body","", 1));
echo "</td></tr>";

echo "<tr><td align=\"center\" class=\"b\"><input type=\"hidden\" name=\"forumid\" value=\"".$forumid."\" /> <input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['creating_topic']."\" /></td></tr>";

echo "</table></form><br />";
}

if (get_user_class() >= UC_MODERATOR) {

if (empty($posts))
$comment_f=htmlspecialchars($f_com);

echo ("<form method=\"post\" action=\"forums.php?action=forum_fcom\"><table cellpadding=\"5\" width=\"100%\">

<tr><td class=\"colhead\" align=\"center\"><b>.::: ".$tracker_lang['admining_category']." :::.</b></td></tr>

<tr><td class=\"a\" align=\"center\">".$tracker_lang['history_topic']." [".strlen($comment_f)."]<br />
<textarea cols=\"100%\" rows=\"6\" ".(get_user_class() < UC_SYSOP ? "readonly" : "name=\"modcomment\"").">".$comment_f."</textarea>
</td></tr>

<tr><td class=\"a\" align=\"center\"><b>".$tracker_lang['add_note']."</b>: <br /><textarea cols=\"100%\" rows=\"3\" name=\"modcomm\"></textarea></td></tr>

<tr><td class=\"b\" align=\"center\">
<input type=\"hidden\" name=\"forumfid\" value=\"".$forumid."\" /> <input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['add_note']." ".$tracker_lang['in'].": ".$subject_f."\" /></td></tr>

</table></form><br />");
}

insert_quick_jump_menu($forumid);

stdfoot();
die;
}

exit();
break;



case 'b_actions': {

accessadministration();

stdhead();

$view_lang = array_map("trim", explode("|", $tracker_lang['forum_baction']));
//print_r($view_lang);

$actions = (string) $_POST["actions"]; /// ��� �����
$cheKer = ((isset($_POST["cheKer"]) && is_array($_POST["cheKer"])) ? $_POST["cheKer"]: false); /// ��� ������
$ref = (isset($_POST["referer"]) ? htmlspecialchars_uni($_POST["referer"]):htmlspecialchars_uni($_SERVER["HTTP_REFERER"]));
if ($ref == 'ADDREFLINK') $ref = htmlspecialchars_uni($_SERVER["HTTP_REFERER"]);

//print_r($cheKer);

$updateset = array();
$where = array();
$viewup = "";

if ($actions == "main"){
/// ���������� ��������
$updateset[] = "sticky = 'yes'";
$where = "sticky = 'no'";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." �������� ��������.\n";
$viewup = $view_lang[0];

} elseif ($actions == "unmain"){
/// ������ ��������
$updateset[] = "sticky = 'no'";
$where = "sticky = 'yes'";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." ���� ��������.\n";
$viewup = $view_lang[1];

} elseif ($actions == "lock"){
/// �������������
$updateset[] = "locked = 'yes'";
$where = "locked = 'no'";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." ������������.\n";
$viewup = $view_lang[2];

} elseif ($actions == "unlock"){
/// ��������������
$updateset[] = "locked = 'no'";
$where = "locked = 'yes'";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." �������������.\n";
$viewup = $view_lang[3];

} elseif ($actions == "noown"){
/// ������� ��� ������
$updateset[] = "userid = '0'";
$where = "userid <> '0'";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." ����������� ������.\n";
$viewup = $view_lang[6];

} elseif ($actions == "movcat"){
/// ������� ���������

$pcat = (int) $_POST["pcat"];

$res_cat = sql_query("SELECT name FROM forums WHERE id = ".sqlesc($pcat)) or sqlerr(__FILE__,__LINE__);
$row_cat = mysql_fetch_assoc($res_cat);

if (empty($row_cat["name"]) || empty($pcat)){
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_select_cat']."</td></tr>\n";
echo "</table>";
stdfoot();
die;
}

$updateset[] = "forumid = ".sqlesc($pcat);
$where = "forumid <> ".sqlesc($pcat);

$torrent_com = get_date_time() . " ".$CURUSER["username"]." ������ ��������� �� ".$row_cat["name"].".\n";
$viewup = $view_lang[7];

} elseif ($actions == "delpoll"){
/// ������� �����
$updateset[] = "polls = 'no'";
$where = "polls = 'yes'";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." ������ ������.\n";
$viewup = $view_lang[4];

} elseif ($actions == "dellsmgs"){
/// ������� ���������, ����� ������� � ����
$where = "(SELECT COUNT(*) FROM posts WHERE topics.id = posts.topicid) > 1";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." ������ ���������, ����� ������� � ����.\n";
$viewup = $view_lang[5];

} elseif (in_array($actions, array("delete", "del_wip", "del_wem", "del_wall"))){
/// ������� ���� (����������)

//$torrent_com = get_date_time() . " ".$CURUSER["username"]." ������ ���������, ����� ������� � ����.\n";

if ($actions == "delete")
$viewup = $view_lang[8];
elseif ($actions == "del_wip")
$viewup = $view_lang[9];
elseif ($actions == "del_wem")
$viewup = $view_lang[10];
elseif ($actions == "del_wall")
$viewup = $view_lang[11];

if ($actions <> "delete")
$allusers = get_row_count("users");

}



echo "<table class=\"embedded\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";

echo "<tr><td class=\"colhead\" align=\"center\" colspan=\"4\">".$tracker_lang['b_action']."</td></tr>";

echo "<tr>
<td class=\"a\" align=\"center\">".$tracker_lang['subject']."</td>
<td class=\"a\" width=\"25%\" align=\"center\">".$tracker_lang['action']."</td>
</tr>";



if (!count($cheKer) || $cheKer == false) {
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['bact_notop']."</td></tr>\n";

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

echo "</table>";
stdfoot();
die;
}

if (empty($actions) || !in_array($actions, array("main", "unmain", "lock", "unlock", "noown", "movcat", "delpoll", "dellsmgs", "delete", "del_wip", "del_wem", "del_wall"))) {

echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['bact_noacttop']."</td></tr>\n";

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

echo "</table>";
stdfoot();
die;
}

//if (count($cheKer) && count($where))
$res = sql_query("SELECT * FROM topics WHERE id IN (".implode(", ", array_map("intval", $cheKer)).") ".(!empty($where) ? "AND ".$where:"")) or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res) == 0){

echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_topic_bact_id']."</td></tr>\n";

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

} else {
while ($row = mysql_fetch_assoc($res)){


echo "<tr>";
echo "<td class=\"b\" align=\"left\">".$row["id"]." - <a class=\"alink\" href=\"forums.php?action=viewtopic&topicid=".$row["id"]."\">".$row["subject"]."</a></td>";
echo "<td width=\"25%\" align=\"center\" class=\"b\">".$viewup."</td>";
echo "</tr>";

if ($actions == "noown"){
sql_query("UPDATE posts SET userid = '0' WHERE topicid = ".sqlesc($row['id'])." ORDER BY id ASC LIMIT 1") or sqlerr(__FILE__,__LINE__);
write_log("������������ ".$CURUSER['username']." ������ ������ � ���� ".htmlspecialchars($row["subject"])." (# ".$row["id"].").", "D19143", "forum");
} elseif ($actions == "movcat"){
sql_query("UPDATE posts SET forumid = ".sqlesc($pcat)." WHERE topicid = ".sqlesc($row['id'])) or sqlerr(__FILE__,__LINE__);

write_log("������������ ".$CURUSER['username']." ���������� ���� ".htmlspecialchars($row["subject"])." (# ".$row["id"].") � ��������� ".$row_cat["name"]." (# ".$pcat.").", "2B9145", "forum");

echo "<tr><td class=\"a\" align=\"left\" colspan=\"3\">����� ���������: ".$row_cat["name"]."</td></tr>";
} elseif ($actions == "delpoll"){
sql_query("DELETE FROM polls WHERE forum = ".sqlesc($row['id'])) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM pollanswers WHERE forum = ".sqlesc($row['id'])) or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER['username']." ������ ����� � ���� ".htmlspecialchars($row["subject"])." (# ".$row["id"].").", "A859A1", "forum");

} elseif ($actions == "dellsmgs"){

$count = get_row_count("posts", "WHERE topicid = ".sqlesc($row['id']));
$res_2 = sql_query("SELECT id FROM posts WHERE topicid = ".sqlesc($row['id'])." ORDER BY id ASC LIMIT 1") or sqlerr(__FILE__,__LINE__);
$row_2 = mysql_fetch_assoc($res_2);
if ($count > 1 && !empty($row_2['id'])){
//sql_query("DELETE FROM posts WHERE topicid = ".sqlesc($row['id'])." ORDER BY id ASC LIMIT 1, ".$count) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM posts WHERE topicid = ".sqlesc($row['id'])." AND id <> ".sqlesc($row_2['id'])) or sqlerr(__FILE__,__LINE__);
write_log("������������ ".$CURUSER['username']." ������ ��� ��������� (".($count-1)." ��) � ���� ".htmlspecialchars($row["subject"])." (# ".$row["id"].").", "BF1B1B", "forum");
echo "<tr><td class=\"a\" align=\"left\" colspan=\"3\">����������: ".($count-1)." �� ".$count."</td></tr>";
}

} elseif (in_array($actions, array("delete", "del_wip", "del_wem", "del_wall"))){

$num_count = $num_count2 = $num_count3 = 0;

$usa_name = "System";

if (!empty($row["userid"]) && $row["userid"] <> $CURUSER["id"]){
$res_us = sql_query("SELECT id, username, avatar, ip, email, class FROM users WHERE id = ".sqlesc($row["userid"])) or sqlerr(__FILE__,__LINE__);

$row_us = mysql_fetch_assoc($res_us);
}

if (@mysql_num_rows($res_us) > 0 && $row_us["class"] < get_user_class()){

/// ������� ����������
$num_post1 = sql_query("SELECT COUNT(*) AS usercount,
(SELECT count(*) FROM topics WHERE userid = ".sqlesc($row["id"]).") AS usertopics,
(SELECT count(*) FROM posts WHERE topicid = ".sqlesc($row["id"])."  AND userid = ".sqlesc($row["userid"]).") AS usertpost
FROM posts WHERE userid = ".sqlesc($row["userid"])) or sqlerr(__FILE__, __LINE__);
$num_p1 = mysql_fetch_assoc($num_post1);
$num_count = number_format($num_p1["usercount"]);
$num_count2 = number_format($num_p1["usertopics"]);
$num_count3 = number_format($num_p1["usertpost"]);
/// ������� ����������



/// ��� �� ip
if (in_array($actions, array("del_wip", "del_wall")) && !empty($row_us["ip"])){
$true_ban = true;

$first = trim($row_us["ip"]);
$last = trim($row_us["ip"]);
$first = ip2long($first);
$last = ip2long($last);

if (!is_numeric($first) || !is_numeric($last)) $true_ban = false;

$ip_bans = get_row_count("bans", "WHERE ".sqlesc($first)." >= first AND ".sqlesc($last)." <= last");

if ($first == -1 || $last == -1 || !empty($ip_bans)) $true_ban = false;

$banua = get_row_count("users", "WHERE ip>=".sqlesc(long2ip($first))." AND ip<=".sqlesc(long2ip($last))." AND class >=".UC_MODERATOR);
$banuaall = get_row_count("users", "WHERE ip>=".sqlesc(long2ip($first))." AND ip<=".sqlesc(long2ip($last)));

if (!empty($banua) || $banuaall > ($allusers/10))
$true_ban = false;

if ($true_ban == true) {

sql_query("INSERT INTO bans (added, addedby, first, last, bans_time, comment) VALUES(".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($first).", ".sqlesc($last).", ".sqlesc("0000-00-00 00:00:00").", ".sqlesc("������ �����: (".htmlspecialchars($row_us["username"]).")").")") or sqlerr(__FILE__, __LINE__);

write_log("IP �����".(long2ip($first) == long2ip($last)? " ".long2ip($first)." ��� �������":"� � ".long2ip($first)." �� ".long2ip($last)." ���� ��������")." ������������� ".$CURUSER["username"].".","ff3a3a","bans");

echo "<tr><td class=\"a\" align=\"left\" colspan=\"3\">".$tracker_lang['delete_alltop_pos']." (".$tracker_lang['message_many'].": ".$num_count.", ".$tracker_lang['topics_many'].": ".$num_count2.")</td></tr>";

}

}

if (in_array($actions, array("del_wem", "del_wall")) && !empty($row_us["email"])){

$bmail = get_row_count("bannedemails", "WHERE email = ".sqlesc($row_us["email"]));

if (empty($bmail)) {
sql_query("INSERT IGNORE INTO bannedemails (added, addedby, comment, email) VALUES(".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc("������ �����: (".($row_us["username"]).")").", ".sqlesc($row_us["email"]).")") or sqlerr(__FILE__, __LINE__);

write_log ("��� ".$row_us["email"]." ��� �������� ������������� ".$CURUSER["username"], get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "bans");

echo "<tr><td class=\"a\" align=\"left\" colspan=\"3\">".$tracker_lang['delete_allposts']." (".$tracker_lang['message_many'].": ".$num_count3.")</td></tr>";

}

}

if (mysql_num_rows($res_us) > 0){
sql_query("DELETE FROM cheaters WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM users WHERE id = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM messages WHERE receiver = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM karma WHERE user = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE friendid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM bookmarks WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM invites WHERE inviter = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM peers WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM readposts WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM report WHERE userid = ".sqlesc($row_us["id"])." OR usertid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM simpaty WHERE fromuserid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM pollanswers WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM shoutbox WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM ratings WHERE user = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM snatched WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM thanks WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM checkcomm WHERE userid = ".sqlesc($row_us["id"])) or sqlerr(__FILE__,__LINE__);

if (!empty($row_us["avatar"]))
@unlink(ROOT_PATH."/pic/avatar/".$row_us["avatar"]);

@unlink(ROOT_PATH."/cache/monitoring_".$row_us["id"].".txt");

if (!empty($row_us["username"]))
write_log("������������ ".$row_us["username"]." (".$row_us["email"].") ��� ������ ������������� ".$CURUSER["username"].". �������: ������ �����.", "590000", "bans");

echo "<tr><td class=\"a\" align=\"left\" colspan=\"3\">".$tracker_lang['delete_user'].": ".$row_us["username"]."</td></tr>";

unsql_cache("bans_first_last");
}

unlinks();

$usa_name = $row_us["username"]." (id ".$row_us["id"].")";

} elseif (empty($row_us["username"]) && !empty($row["userid"]))
$usa_name = "id: ".$row["userid"];

sql_query("DELETE FROM posts WHERE topicid = ".sqlesc($row["id"])) or sqlerr(__FILE__, __LINE__);

if (empty($num_count)) $num_count = mysql_affected_rows();

sql_query("DELETE FROM topics WHERE id = ".sqlesc($row["id"])) or sqlerr(__FILE__, __LINE__);
if (empty($num_count2)) $num_count2 = mysql_affected_rows();

sql_query("DELETE FROM polls WHERE forum = ".sqlesc($row["id"])) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM pollanswers WHERE forum = ".sqlesc($row["id"])) or sqlerr(__FILE__, __LINE__);


write_log("������������ ".$CURUSER['username']." ������ ��� ��������� ($num_count ��) � ���� ($num_count2 ��), ������� ������ $usa_name.", "BF1B1B", "forum");

} else write_log("������������ ".$CURUSER['username']." �������� �������� ($viewup) � ���� ".htmlspecialchars($row["subject"])." (# ".$row["id"].").", "98A730", "forum");

unsql_cache("forums_ptop-".$row["id"]);
}

}

//if (!empty($torrent_com))
//$updateset[] = "torrent_com = CONCAT_WS('',".sqlesc($torrent_com).", torrent_com)";

if (mysql_num_rows($res) > 0 && count($updateset))
sql_query("UPDATE topics SET ".implode(",", $updateset)." WHERE id IN (" . implode(", ", array_map("sqlesc", $cheKer)) . ") ".(!empty($where) ? "AND ".$where:"")) or sqlerr(__FILE__,__LINE__);


unsql_cache("forums.id_name");
unsql_cache("forums_cat");
unsql_cache("forums.main");


if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";




echo "</table>";

stdfoot();
die;

}
exit();
break;



case 'viewtopic': {

$topicid = (isset($_GET["topicid"]) ? intval($_GET["topicid"]):0);
$page = isset($_GET["page"]) ? (int) $_GET["page"]: false;

if (!is_valid_id($topicid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": topicid");

$userid = $CURUSER["id"];

$res = sql_query("SELECT *, (SELECT COUNT(*) FROM posts WHERE topicid = ".sqlesc($topicid).") AS num_com FROM topics WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_assoc($res) or stderr($tracker_lang['error'], $tracker_lang['no_message_with_such_id']);


$t_com_arr=$arr["t_com"];
$locked = ($arr["locked"] == 'yes' ? $tracker_lang['yes']:$tracker_lang['no']);
$lock_ed = $arr["locked"];
$subject = format_comment_light($arr["subject"]);
$sticky = ($arr["sticky"] == "yes" ? $tracker_lang['yes']:$tracker_lang['no']);
$forumid = $arr["forumid"];
$topic_polls = $arr["polls"];
$views=number_format($arr["views"]);
$num_com=number_format($arr["num_com"]);
$lastdate = $arr["lastdate"];

sql_query("UPDATE topics SET views = views + 1 WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);


$res = sql_query("SELECT * FROM forums WHERE id = ".sqlesc($forumid), $cache = array("type" => "disk", "file" => "forumid_".$forumid, "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_assoc_($res) or stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": forumid");

$forum = htmlspecialchars($arr["name"]);

if ($arr["minclassread"] > get_user_class() && !empty($arr["minclassread"])){
stderr($tracker_lang['access_denied'], $tracker_lang['warning_minclass']);
die;
}


$res = sql_query("SELECT COUNT(*) FROM posts WHERE topicid = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_row($res);
$postcount = $arr[0];

$count = $arr[0];

list($pagertop, $pagerbottom, $limit) = pager($postsperpage, $count, "forums.php?action=viewtopic&topicid=".$topicid."&");


stdhead(strip_tags($subject));

fpanelka();

if (get_user_class() >= UC_MODERATOR)
$edit_link = "<a class=\"alink\" title=\"".$tracker_lang['go_to']."\" href='forums.php?action=edittopic&topicid=".$topicid."'>".$tracker_lang['admining_topic']."</a>";

echo "<table cellpadding=\"0\" cellspacing=\"0\" id=\"main\" width=\"100%\">";

echo "<tr><td class=\"b\" align=\"center\">
".(get_user_class()>= UC_MODERATOR ? "<div align=\"right\" style=\"margin: 0px; padding: 0px;\">".$edit_link."</div>":"")."
<time itemprop=\"startDate\" datetime=\"".date('c', strtotime($lastdate))."\"><h1><a class=\"alink\" href=\"forums.php?action=viewtopic&topicid=".$topicid."\">".$subject."</a></h1></time>
".$tracker_lang['category'].": <a class=\"alink\" title=\"".$tracker_lang['go_to']."\" name=\"poststop\" id=\"poststop\" href=\"forums.php?action=viewforum&forumid=".$forumid."\">".$forum."</a>
</td></tr>";


if ($topic_polls == "yes"){
echo "<tr><td width=\"100%\">";
if ($CURUSER){
echo "<script type=\"text/javascript\" src=\"".$DEFAULTBASEURL."/js/forums_poll.core.js\"></script>
<link href=\"".$DEFAULTBASEURL."/js/poll.core.css\" type=\"text/css\" rel=\"stylesheet\" />
<script type=\"text/javascript\">jQuery(document).ready(function(){loadpoll(\"".$topicid."\");});</script>
";

echo "<div id=\"poll_container\"><div id=\"loading_poll\" style=\"display:none\"></div><noscript><b>".$tracker_lang['noscript']."</b></noscript></div>";
} else echo $tracker_lang['polls_guest'];

echo "</td></tr>";
}


echo "<tr><td class=\"a\" align=\"center\">
<b>".$tracker_lang['views']."</b>: ".$views.",
<b>".$tracker_lang['commens']."</b>: ".$num_com.",
<b>".$tracker_lang['stickyf']."</b>: ".$sticky.",
<b>".$tracker_lang['block_is_topic']."</b>: ".$locked."
</td></tr>";


if (get_user_class()>= UC_MODERATOR && !empty($t_com_arr))
echo "<tr><td class=\"b\" align=\"center\"><div align=\"center\" width=\"100%\"class=\"tcat_t\">
<div class=\"spoiler-wrap\" id=\"115\"><div class=\"spoiler-head folded clickable\">".$tracker_lang['history_topic']."</div><div class=\"spoiler-body\" style=\"display: none;\">
<textarea cols=\"115%\" rows=\"5\" readonly>".$t_com_arr."</textarea>
</div></div>
</div></td></tr>";


echo "</table>";



$res = sql_query("SELECT p. *, u.username, u.class, u.last_access, u.ip, u.signatrue,u.forum_com, u.signature,u.avatar, u.title, u.enabled, u.warned, u.hiderating, u.uploaded, u.downloaded, u.donor, e.username AS ed_username, e.class AS ed_class, (select count(*) FROM posts WHERE userid = p.userid) AS num_topuser
FROM posts p
LEFT JOIN users u ON u.id = p.userid
LEFT JOIN users e ON e.id = p.editedby
WHERE topicid = ".sqlesc($topicid)." ORDER BY p.id ".$limit) or sqlerr(__FILE__, __LINE__);


if ($CURUSER && get_date_time(gmtime() - 60)>= $CURUSER["forum_access"]){
sql_query("UPDATE users SET forum_access = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
unsql_cache("arrid_".$CURUSER["id"]);
}

$pc = mysql_num_rows($res);
$pn = 0;
$r = sql_query("SELECT lastpostread FROM readposts WHERE userid = ".sqlesc($CURUSER["id"])." AND topicid = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

$a = mysql_fetch_assoc($r);
$lpr = $a["lastpostread"];

echo "<table class=\"forums\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
<tr><td>";

$num = 1;
while ($arr = mysql_fetch_assoc($res)) {

$alinke = array();
++$pn;

$ed_username = $arr["ed_username"];
$ed_class= $arr["ed_class"];
$postid = $arr["id"];
$posterid = $arr["userid"];
$added = ($arr['added']);
$postername = $arr["username"];
$posterclass = $arr["class"];

if (empty($postername) && $posterid <> 0)
$by = "<b>id</b>: ".$posterid;
elseif ($posterid==0 && empty($postername))
$by = "<i>".$tracker_lang['msg_is']." </i><font color=gray>[<b>System</b>]</font>";
else
$by = "<a href='userdetails.php?id=".$posterid."'><b>" .get_user_class_color($posterclass,  $postername). "</b></a>";

if ($posterid<>0 && !empty($postername)){

if (strtotime($arr["last_access"]) > gmtime() - 600) {
$online = "online";
$online_text = $tracker_lang['online'];
} else {
$online = "offline";
$online_text = $tracker_lang['offline'];
}

if ($arr["downloaded"] > 0) {
$ratio = $arr['uploaded'] / $arr['downloaded'];
$ratio = number_format($ratio, 2);
} elseif ($arr["uploaded"] > 0)
$ratio = "Infinity";
else
$ratio = "---";


if ($row["hiderating"]=="yes")
$print_ratio = "<b>+100%</b>";
else
$print_ratio = $ratio;

} else {
$ratio = "---";
$print_ratio = $ratio;
unset($online_text);
unset($online);
}


if ($CURUSER["cansendpm"]=='yes' && ($CURUSER["id"]<>$posterid && $posterid<>0 && !empty($postername)))
$cansendpm = " <a href='message.php?action=sendmessage&receiver=".$posterid."' title=\"".$tracker_lang['sendmessage']."\"><img src='".$DEFAULTBASEURL."/pic/button_pm.gif' border=0 alt=\"".$tracker_lang['sendmessage']."\" ></a>";
else unset($cansendpm);


if ($arr["forum_com"]<>"0000-00-00 00:00:00" && !empty($postername))
$ban = "<div><b>".$tracker_lang['banned_on']." </b>".$arr["forum_com"]."</div>";
else unset($ban);

$online_view = '';
if (!empty($online))
$online_view="<img src=\"".$DEFAULTBASEURL."/pic/button_".$online.".gif\" alt=\"".$online_text."\" title=\"".$online_text."\" style=\"position: relative; top: 2px;\" border=\"0\" height=\"14\">";

$numb_view="<span style=\"font-size: 10px;\"><strong><a title=\"".$tracker_lang['stable_link'].": ".$postid."\" href='forums.php?action=viewpost&id=".$postid."'>".$postid."</a></strong></span>";

if (!empty($arr["avatar"]))
$avatar = ($CURUSER["id"]==$posterid ? "<a href=\"my.php\"><img class=\"avatars\" alt=\"".$tracker_lang['click_on_avatar']."\" title=\"".$tracker_lang['click_on_avatar']."\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/".$arr["avatar"]."\" /></a>":"<img class=\"avatars\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/".$arr["avatar"]."\" />");
else
$avatar = "<img class=\"avatars\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/default_avatar.gif\"/>";


$body = format_comment($arr["body"], true);


if (get_user_class()>= UC_MODERATOR && !empty($arr['body_ori']))
$viworiginal = true;
else
$viworiginal = false;

echo ("<a name=\"".$postid."\"></a>\n");

if ($pn == $pc)
echo("<a name=\"last\"></a>\n");


echo "<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">

<tr>
<td colspan=\"4\" class=\"a\">

<div style=\"float: right; width: auto;\">".$numb_view."</div>
<div align=\"left\">".$by." ".$online_view.$cansendpm."
".get_user_class_name($arr["class"])." ".(empty($arr["title"]) ? "": "(".htmlspecialchars($arr["title"]).")")."
".($arr["donor"] == "yes" ? "<img src=\"".$DEFAULTBASEURL."/pic/star.gif\" alt='".$tracker_lang['donor']."'>" : "")
.($arr["enabled"] == "no" ? "<img src=\"".$DEFAULTBASEURL."/pic/disabled.gif\" alt=\"".$tracker_lang['disabled']."\" style='margin-left: 2px'>" : ($arr["warned"] == "yes" ? "<img src=\"".$DEFAULTBASEURL."/pic/warned.gif\" alt=\"".$tracker_lang['warned']."\" border=0>" : "")) . "
</div>

<div align=\"left\">
<b>".$tracker_lang['ratio']."</b>: ".$print_ratio.", <b>".$tracker_lang['uploaded']."</b>: ".mksize($arr["uploaded"]) .", <b>".$tracker_lang['downloaded']."</b>: ".mksize($arr["downloaded"])." ".$ban."
</div>

</td>
</tr>

<tr>

<td colspan=\"4\">
<var style=\"padding-right: 10px; clear: right; float: left;\">".$avatar."</var>
".$body."
".((is_valid_id($arr['editedby']) AND !empty($arr['editedby']) && (get_user_class() > UC_MODERATOR || $arr['editedby'] == $CURUSER['id'])) ? "<div style=\"margin-top: 10px\" align=\"right\"><small>".sprintf($tracker_lang['edit_lasttime'], "<a href=\"userdetails.php?id=".$row["editedby"]."\"><b>".get_user_class_color($ed_class, $ed_username)."</b></a>", $arr['editedat'])." ".($viworiginal == true ? "<a href='forums.php?action=viewpost&id=".$postid."&ori'>".$tracker_lang['original_post']."</a>":"")."</small></div>":"")."
".(($arr["signatrue"] == "yes" && $arr["signature"]) ? "<div style=\"margin-top: 10px\" align=\"right\"><small>".format_comment($arr["signature"])."</small></div>": "")."
</td>

</tr>";


echo "<tr><td class=\"a\" align=\"right\" colspan=\"4\">";

echo "<div style=\"float: left; width: auto;\">".normaltime(($arr['added']),true)."</div>";


if ($CURUSER && $CURUSER["id"] <> $posterid && !empty($posterid) && !empty($postername))
$alinke[] = "<a class=\"alink\" href='message.php?receiver=".$posterid."&action=sendmessage'>".$tracker_lang['sendmessage']."</a>";

if (!empty($posterid))
$alinke[] = "<a class=\"alink\" title=\"".$tracker_lang['messages_on_forum']." (".$arr["num_topuser"].")\" href='forums.php?action=search_post&userid=".$posterid."'>".$tracker_lang['search_message']."</a>";

if ($CURUSER && !empty($posterid) && $CURUSER["forum_com"] == "0000-00-00 00:00:00" && $lock_ed == 'no')
$alinke[] = "<a class=\"alink\" href='forums.php?action=quotepost&topicid=".$topicid."&postid=".$postid."'>".$tracker_lang['quote_message']."</a>";

if (get_user_class() >= UC_MODERATOR && !empty($arr["ip"]))
$alinke[] = "<a class=\"alink\" title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$arr["ip"]."\">".$arr["ip"]."</a>";

if (!empty($edit_link) && empty($_GET['page'])){
$alinke[] = $edit_link; unset($edit_link);
}


if (($CURUSER["forum_com"]=="0000-00-00 00:00:00" && $CURUSER["id"] == $posterid && $lock_ed == 'no') || get_user_class() >= UC_MODERATOR)
$alinke[] = "<a class=\"alink\" href='forums.php?action=editpost&postid=".$postid."'>".$tracker_lang['edit']."</a>";

if (get_user_class() >= UC_MODERATOR || ($CURUSER["forum_com"] == "0000-00-00 00:00:00" && $CURUSER["id"] == $posterid && $lock_ed == 'no'))
$alinke[] = "<a class=\"alink\" href='forums.php?action=deletepost&postid=".$postid."'>".$tracker_lang['delete']."</a>";

if (get_user_class() > UC_MODERATOR)
$alinke[] = "<a class=\"alink\" title=\"".$tracker_lang['delete_alltop_pos']."\" href=\"forums.php?action=banned&userid=".$posterid."&postid=".$postid."\">".$tracker_lang['spam']."</a>";

if (count($alinke))
echo "<div align=\"right\">".implode(", ", $alinke)."</div>";

echo "</td></tr>";


echo "</table>";


++$num;
}


echo "</td></tr>

<tr><td colspan=\"4\">".$pagerbottom."</td></tr>

</table>";






$namhtml = strip_tags($subject);

if (!empty($namhtml)){
echo "<script type=\"text/javascript\">function highlight(field) {field.focus();field.select();}</script>";
echo "<table style=\"margin-top: 2px;\" class=\"main\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_link'].'\' onclick=\'highlight(this)\' value=\''.$namhtml.': '.$DEFAULTBASEURL.'/forums.php?action=viewtopic&topicid='.$topicid.'\'>';
echo "</td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_html'].'\' onclick=\'highlight(this)\' value=\'<a href="'.$DEFAULTBASEURL.'/forums.php?action=viewtopic&topicid='.$topicid.'" target="_blank">'.$namhtml.'</a>\'>';
echo "</td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_forum'].'\' onclick=\'highlight(this)\' value=\'[url='.$DEFAULTBASEURL.'/forums.php?action=viewtopic&topicid='.$topicid.']'.$namhtml.'[/url]\'>';
echo "</td></tr>";

echo "</table>";
}





echo "<br />";



if (($lpr > $postid || empty($lpr)) && $CURUSER) {
if ($lpr)
sql_query("UPDATE readposts SET lastpostread = ".sqlesc($postid)." WHERE userid = ".sqlesc($CURUSER["id"])." AND topicid = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
else
sql_query("INSERT INTO readposts (userid, topicid, lastpostread) VALUES (".sqlesc($CURUSER["id"]).", ".sqlesc($topicid).", ".sqlesc($postid).")") or sqlerr(__FILE__, __LINE__);
}


if ($CURUSER["forum_com"] == "0000-00-00 00:00:00" && $CURUSER){

if ($lock_ed == 'no' || ($lock_ed == 'yes' && get_user_class() > UC_MODERATOR)){
echo "<form name=\"comment\" method=\"post\" action=\"forums.php?action=post\"><table cellspacing=\"0\" cellpadding=\"0\" class=\"forums\" width=\"100%\">";

echo "<tr><td align=\"center\" class=\"colhead\"><a name=comments></a><b>.::: ".$tracker_lang['add_post_on']." :::.</b></td></tr>";

echo "<tr><td align=\"center\" class=\"a\">";
echo textbbcode("comment","body","", 1);
echo "</td></tr>";

echo "<tr><td align=\"center\" class=\"b\">

<div><label><input type=\"checkbox\" title=\"������ ���� BB ��� �� ������\" name=\"nobb\" value=\"1\">nobb</label> <label><input type=\"checkbox\" title=\"�������� � ���� ������� - ��� [img]\" name=\"addurl\" value=\"1\">[img]</label></div>

<input type=\"hidden\" value=\"".$topicid."\" name=\"topicid\" /> <input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['send_message']."\" class=\"btn\" value=\"".$tracker_lang['send_message']."\" /></td></tr>";

echo "</table></form><br />";

}
}

insert_quick_jump_menu($forumid, $CURUSER);

stdfoot();
die;
}
exit();
break;









case 'banned' : {

$userid = (isset($_GET["userid"]) ? intval($_GET["userid"]):0);


if (get_user_class() <= UC_MODERATOR){
stderr($tracker_lang['access_denied'], $tracker_lang['no_rights_for_ban']);
die;
}

if (!empty($userid)){
$res = sql_query("SELECT id, username, avatar, ip, email, class FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_assoc($res);

if ($arr["class"] >= get_user_class() || $arr["id"] == $CURUSER["id"]){
stderr($tracker_lang['access_denied'], $tracker_lang['no_rights_for_ban']);
die;
}
}



if ($CURUSER["forum_com"]<>"0000-00-00 00:00:00"){

if ($CURUSER){
header("Refresh: 15; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 15), $tracker_lang['banned_on_forum'].": ".$CURUSER["forum_com"].".");
die;
} else {
stderr($tracker_lang['error_data'], $tracker_lang['not_allow_write']." ".$tracker_lang['access_denied']);
die;
}

}


$postid = (isset($_GET["postid"]) ? $_GET["postid"]:0);

$posted = sql_query("SELECT topicid FROM posts WHERE id = ".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);
$postp1 = mysql_fetch_assoc($posted);

$topicid = $postp1["topicid"];


if (!empty($userid)){

$num_post1 = sql_query("SELECT COUNT(*) AS usercount,
(SELECT count(*) FROM topics WHERE userid = ".sqlesc($userid).") AS usertopics,
(SELECT count(*) FROM posts WHERE topicid = ".sqlesc($topicid)."  AND userid = ".sqlesc($userid).") AS usertpost
FROM posts WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
$num_p1 = mysql_fetch_assoc($num_post1);
$num_count = number_format($num_p1["usercount"]);
$num_count2 = number_format($num_p1["usertopics"]);
$num_count3 = number_format($num_p1["usertpost"]);
}



if (isset($_POST["userid"])){

$userid = (int) $_POST["userid"];

$res = sql_query("SELECT id, username, avatar, ip, email, class FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_assoc($res);

if ($arr["class"] >= get_user_class() || $arr["id"] == $CURUSER["id"]){
stderr($tracker_lang['access_denied'], $tracker_lang['no_rights_for_ban']);
die;
}


$postid = (int) $_POST["postid"];

$posted = sql_query("SELECT topicid FROM posts WHERE id = ".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);
$postp1 = mysql_fetch_assoc($posted);

$topicid = $postp1["topicid"];



if (empty($userid)){
stderr($tracker_lang['error'], $tracker_lang['no_user_intable']);
die;
}



$dellmsg = (int) $_POST["dellmsg"];
/// �������� ��������� �� ����
if (!empty($dellmsg)){

$res4 = sql_query("SELECT id AS first, (SELECT COUNT(*) FROM posts WHERE topicid = ".sqlesc($topicid).") AS count FROM posts WHERE topicid = ".sqlesc($topicid)." ORDER BY id ASC LIMIT 1") or sqlerr(__FILE__, __LINE__);
$arr4 = mysql_fetch_assoc($res4);

if ($arr4["count"] > 1 && $arr4["first"]<>$postid){

//echo " ��� ������� ������ ��������� � ���� ";
sql_query("DELETE FROM posts WHERE userid = ".sqlesc($userid)." AND topicid = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER['username']." ������ ��� ��������� ������������ ".$arr["username"]." (id $userid).", "BF1B1B", "forum");

} else {

//echo " ��� ������� ������ ���� � ��� ��������� � ���";
sql_query("DELETE FROM posts WHERE topicid = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM topics WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM polls WHERE forum = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM pollanswers WHERE forum = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER['username']." ������ ��� ��������� � ���� ������������ ".$arr["username"]." (id $userid).", "BF1B1B", "forum");

}

unsql_cache("forums.id_name");
unsql_cache("forums_cat");
unsql_cache("forums.main");
unsql_cache("forums_ptop-".$topicid);

write_log("������������ ".$CURUSER['username']." ������ ��� ��������� ������������ ".$arr["username"]." (id $userid).", "BF1B1B", "forum");
}


$dellall = (int) $_POST["dellall"];
/// �������� ��������� � ���� ������������
if (!empty($dellall)){

$res2 = sql_query("SELECT id FROM topics WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
while ($arr2 = mysql_fetch_assoc($res2)){

sql_query("DELETE FROM posts WHERE topicid = ".sqlesc($arr2["id"])) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM topics WHERE id = ".sqlesc($arr2["id"])) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM polls WHERE forum = ".sqlesc($arr2["id"])) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM pollanswers WHERE forum = ".sqlesc($arr2["id"])) or sqlerr(__FILE__, __LINE__);
}

sql_query("DELETE FROM posts WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER['username']." ������ ��� ��������� � ���� ������������ ".$arr["username"]." (id $userid).", "BF1B1B", "forum");


unsql_cache("forums.id_name");
unsql_cache("forums_cat");
unsql_cache("forums.main");
unsql_cache("forums_ptop-".$arr2["id"]);

}


$bansact = (int) $_POST["bansact"];
/// �������� ��������� � ���� ������������
if (!empty($bansact)){

/// ��� �� ip
if ($bansact <> 2 && !empty($arr["ip"])){
$true_ban = true;

$first = trim($arr["ip"]);
$last = trim($arr["ip"]);
$first = ip2long($first);
$last = ip2long($last);

if (!is_numeric($first) || !is_numeric($last))
$true_ban = false;

$ip_bans = get_row_count("bans", "WHERE ".sqlesc($first)." >= first AND ".sqlesc($last)." <= last");

if ($first == -1 || $last == -1 || !empty($ip_bans))
$true_ban = false;

$banua = get_row_count("users", "WHERE ip>=".sqlesc(long2ip($first))." AND ip<=".sqlesc(long2ip($last))." AND class >=".UC_MODERATOR);

if (!empty($banua)) $true_ban = false;

if ($true_ban == true) {

sql_query("INSERT INTO bans (added, addedby, first, last, bans_time, comment) VALUES(".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($first).", ".sqlesc($last).", ".sqlesc("0000-00-00 00:00:00").", ".sqlesc("������ �����: (".htmlspecialchars($arr["username"]).")").")") or sqlerr(__FILE__, __LINE__);

write_log("IP �����".(long2ip($first) == long2ip($last)? " ".long2ip($first)." ��� �������":"� � ".long2ip($first)." �� ".long2ip($last)." ���� ��������")." ������������� ".$CURUSER["username"].".","ff3a3a","bans");

}

}

if ($bansact <> 1 && !empty($arr["email"])){

$bmail = get_row_count("bannedemails", "WHERE email = ".sqlesc($arr["email"]));

if (empty($bmail)) {
sql_query("INSERT IGNORE INTO bannedemails (added, addedby, comment, email) VALUES(".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc("������ �����: (".htmlspecialchars($arr["username"]).")").", ".sqlesc($arr["email"]).")") or sqlerr(__FILE__, __LINE__);

write_log ("��� ".$arr["email"]." ��� �������� ������������� ".$CURUSER["username"], get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "bans");
}

}


}



$actban = (int) $_POST["actban"];

/// �������� ��������� � ���� ������������
if (!empty($actban)){

sql_query("DELETE FROM cheaters WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM users WHERE id = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM messages WHERE receiver = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM karma WHERE user = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE friendid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM bookmarks WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM invites WHERE inviter = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM peers WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM readposts WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM report WHERE userid = ".sqlesc($arr["id"])." OR usertid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM simpaty WHERE fromuserid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM pollanswers WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM shoutbox WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM ratings WHERE user = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM snatched WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM thanks WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM checkcomm WHERE userid = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);

if (!empty($arr["avatar"]))
@unlink(ROOT_PATH."/pic/avatar/".$arr["avatar"]);

@unlink(ROOT_PATH."/cache/monitoring_".$arr["id"].".txt");

if (!empty($arr["username"]))
write_log("������������ ".$arr["username"]." (".$arr["email"].") ��� ������ ������������� ".$CURUSER["username"].". �������: ������ �����.", "590000", "bans");


} else {

$modcomment = date("Y-m-d") . " - �������� ������������� " . $CURUSER['username'] . ".\n�������: ������ �����\n";

sql_query("UPDATE users SET modcomment = CONCAT_WS('', ".sqlesc($modcomment).", modcomment), enabled = 'no'  WHERE id = ".sqlesc($arr["id"])) or sqlerr(__FILE__,__LINE__);
unsql_cache("arrid_".$arr["id"]);

}

unsql_cache("bans_first_last");

unlinks();

header("Location: forums.php");
die;
}


if (!is_valid_id($userid)){
header("Location: forums.php");
die;
}

if (get_user_class() < UC_MODERATOR){
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die;
}


stdhead($tracker_lang['choice_of_action']);


echo("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");

echo ("<tr><td class=\"b\" align=\"center\" colspan=\"2\"><a class=\"alink\" href=\"forums.php?action=viewtopic&topicid=".$topicid."\">".$tracker_lang['back_inlink']."</a></td></tr>");

echo("<tr><td class=colhead align=\"center\" colspan=\"2\">".$tracker_lang['choice_of_action']."</td></tr>");

echo("<form method=\"post\" action=\"forums.php?action=banned\">");
echo("<tr><td>");
echo("<tr><td class=\"a\"><b>".$tracker_lang['delete_allposts']."</b>:
<label><input type=\"radio\" name=\"dellmsg\" checked value=\"1\"> ".$tracker_lang['delete']."</label> <label><input type=radio name=\"dellmsg\" value=\"0\"> ".$tracker_lang['skip']."</label> (".$tracker_lang['message_many'].": ".$num_count3.")</td></tr>");

echo("<tr><td class=\"a\"><b>".$tracker_lang['delete_alltop_pos']."</b>:
<label><input type=\"radio\" name=\"dellall\" checked value=\"1\"> ".$tracker_lang['delete']."</label> <label><input type=radio name=\"dellall\" value=\"0\"> ".$tracker_lang['skip']."</label> (".$tracker_lang['message_many'].": ".$num_count.", ".$tracker_lang['topics_many'].": ".$num_count2.")</td></tr>");


echo("<tr><td class=\"a\">
<b>".$tracker_lang['perform_action']."</b>: <select name=\"bansact\">
<option value=\"0\">".$tracker_lang['no_choose']."</option>
<option value=\"1\">".$tracker_lang['banned_for_ip']."</option>\n
<option value=\"2\">".$tracker_lang['banned_for_email']."</option>\n
<option selected value=\"3\">".$tracker_lang['banned_for_ipem']."</option>\n
</select>
</td></tr>");

echo("<tr><td class=\"a\"><b>".$tracker_lang['type']."</b>:
<label><input type=radio name=\"actban\" value='1'>".$tracker_lang['delete_user']."</label> <label><input type=radio name=\"actban\" checked value='0'>".$tracker_lang['delete_no_off']."</label></td></tr>");


echo("<tr><td align=\"center\" colspan=\"2\">");
echo("<input type=\"hidden\" name=\"userid\" value=\"".$userid."\" />\n");
echo("<input type=\"hidden\" name=\"postid\" value=\"".$postid."\" />\n");

echo("<input type=\"submit\" class=btn value=\"".$tracker_lang['selected']."\" />");
echo("</td></tr></table></form><br />");

stdfoot();
die;

}

exit();
break;



case 'reply':
case 'quotepost': {

if ($CURUSER["forum_com"]<>"0000-00-00 00:00:00"){

if ($CURUSER){
header("Refresh: 15; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 15), $tracker_lang['banned_on_forum'].": ".$CURUSER["forum_com"].".");
die;
} else {
stderr($tracker_lang['error_data'], $tracker_lang['not_allow_write']." ".$tracker_lang['access_denied']);
die;
}

}

if ($action == "reply")  {

$topicid = (isset($_GET["topicid"]) ? intval($_GET["topicid"]):0);

if (!is_valid_id($topicid))
header("Location: forums.php");

stdhead($tracker_lang['creating_topic']);

begin_main_frame();
insert_compose_frame($topicid, false);
end_main_frame();

stdfoot();
die;
}

if ($action == "quotepost"){

$topicid = (int) $_GET["topicid"];

if (!is_valid_id($topicid))
header("Location: forums.php");

stdhead($tracker_lang['creating_answer']);

//   begin_main_frame();
insert_compose_frame($topicid, false, true);
//   end_main_frame();

stdfoot();
die;
}

}
exit();
break;





case 'post': {

if ($CURUSER["forum_com"]<>"0000-00-00 00:00:00"){

if ($CURUSER){
header("Refresh: 15; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 5), $tracker_lang['banned_on_forum'].": ".$CURUSER["forum_com"].".");
die;
}		else		{
stderr($tracker_lang['error_data'], $tracker_lang['not_allow_write']." ".$tracker_lang['access_denied']);
die;
}
}

$forumid = isset($_POST["forumid"]) ? (int) $_POST["forumid"] : 0;
$topicid = isset($_POST["topicid"]) ? (int) $_POST["topicid"] : 0;

if (!is_valid_id($forumid) && !is_valid_id($topicid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);


$newtopic = $forumid > 0;

if ($newtopic) {
$subject = htmlspecialchars_uni(strip_tags($_POST["subject"]));

if (!$subject)
stderr($tracker_lang['error'], $tracker_lang['no_subject']);

if (strlen($subject) > $maxsubjectlength)
stderr($tracker_lang['error'], sprintf($tracker_lang['subject_limit'], $maxsubjectlength));
}
else
$forumid = get_topic_forum($topicid) or stderr($tracker_lang['error'], $tracker_lang['no_this_cat']);

$arr = get_forum_access_levels($forumid) or stderr($tracker_lang['error'], $tracker_lang['no_this_cat']);

if (get_user_class() < $arr["write"] || ($newtopic && get_user_class() < $arr["create"]))
stderr($tracker_lang['error'], $tracker_lang['access_denied']);


$body = htmlspecialchars_uni(strip_tags($_POST["body"]));
if (empty($body))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": body");


if (isset($_POST["nobb"]) && !empty($_POST["nobb"]))
$body = preg_replace("/\[((\s|.)+?)\]/is", "", $body); /// ����� �� ������ [] � ��


if (isset($_POST["addurl"]) && !empty($_POST["addurl"]))
$body = preg_replace("/(http:\/\/[^\s'\"<>]+(\.(jpg|jpeg|gif|png)))/is", "[img]\\1[/img]", $body);


if ($Forum_Config['anti_spam'] == true){
////////////////////// ������� ���������� ��� ������ � ��� ������� (������� ������� ����������)
preg_match_all("/((http|ftp|https|ftps):\/\/[^()<>\s]+)/is", $body, $s1);

$stip = trim($_POST["body"]);

$numlinksin = count($s1[0]); /// ������� ������
$numlinksout = count(array_unique($s1[0])); /// ��� �� ������� �� ��� ����������

$alzie = (strlen($stip)/strlen($body));

if ($numlinksin >= 2 && $alzie > 2) { /// ������� �������

$modcomment = get_date_time() . " - �������� �� ������ �� ������� ������ (�� $numlinksin, ������� $numlinksout ".($numlinksout==1 ? "�����":"������").").\n". $CURUSER["usercomment"];

$forumbanutil = get_date_time(gmtime() + 2 * 604800);
$forum_dur = "2 ������";
$modcomment = gmdate("Y-m-d") . " - ����� ��� �� $forum_dur �� SYSTEM (��������).\n". $modcomment;

sql_query("UPDATE users SET usercomment = CONCAT_WS('', ".sqlesc($modcomment).", usercomment), forum_com = ".sqlesc($forumbanutil)." WHERE id = ".$CURUSER["id"]) or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER['username']." ������ (������� ������ � ���������� - $alzie%) - ������� ��������.", "921E93", "forum");

unsql_cache("arrid_".$CURUSER["id"]);

$DEFURL = htmlspecialchars_uni($_SERVER['HTTP_HOST']);

$subj = "��������� ������� ����� �� ������";
$all = "������� �������� ��������� �� ������: http://$DEFURL/userdetails.php?id=$CURUSER[id]  - $CURUSER[username] ($CURUSER[email])\n
������� ������������: $CURUSER[usercomment] $CURUSER[modcomment]\n
���������: (�� $numlinksin, ������� $numlinksout ".($numlinksout==1 ? "�����":"������").") ����� ������� % � ������� (html) - $alzie%\n
/////////////////////////////////////////////////
$body
/////////////////////////////////////////////////";

if (!empty($Mail_config['warning'])) {
if (!sent_mail($Mail_config['warning'], $SITENAME,$SITEEMAIL,$subj,$all,false)){


$res = sql_query("SELECT id FROM users WHERE class = ".sqlesc(UC_SYSOP)." AND last_access > ".sqlesc(get_date_time(gmtime() - 86400*3))." LIMIT 5") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
$res = sql_query("SELECT id FROM users WHERE support = 'yes' AND class > ".sqlesc(UC_MODERATOR)." LIMIT 5") or sqlerr(__FILE__, __LINE__);

while ($arr = mysql_fetch_array($res)){
sql_query("UPDATE users SET unread = unread+1 WHERE id = ".sqlesc($arr["id"])) or sqlerr(__FILE__, __LINE__);

sql_query("INSERT INTO messages (sender, receiver, added, msg, subject, poster) VALUES (0, ".sqlesc($arr["id"]).", ".sqlesc(get_date_time()).", ".sqlesc($all).", ".sqlesc($subj).", 0)") or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$arr["id"]);
}


}
}

stderr($tracker_lang['antispam_warning'], "��������� ������� ���������, �� �������� �� ������. <br />���� �������� ��� �������, ���������� �������� ������������� �����.");
}
////////////////////// ������� ���������� ��� ������ � ��� ������� (������� ������� ����������)
}

$userid = $CURUSER["id"];


if ($newtopic) {

sql_query("INSERT INTO topics (userid, forumid, subject) VALUES (".sqlesc($userid).", ".sqlesc($forumid).", ".sqlesc($subject).")") or sqlerr(__FILE__, __LINE__);

$topicid = mysql_insert_id() or stderr($tracker_lang['error'], $tracker_lang['no_data']);

unsql_cache("forums.id_name");
}
else
{
$res = sql_query("SELECT * FROM topics WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res) or stderr($tracker_lang['error'], $tracker_lang['no_message_with_such_id']);

if ($arr["locked"] == 'yes' && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['block_is_topic']);

$forumid = $arr["forumid"];
}

sql_query("INSERT INTO posts (topicid, forumid, userid, added, body) VALUES (".sqlesc($topicid).", ".sqlesc($forumid).",".sqlesc($userid).", ".sqlesc(get_date_time()).", ".sqlesc($body).")") or sqlerr(__FILE__, __LINE__);

$postid = mysql_insert_id() or stderr($tracker_lang['error'], $tracker_lang['no_message_with_such_id']);

update_topic_last_post($topicid,$added);
unlinks();
unsql_cache("forums_cat");
unsql_cache("forums.main");


if (!empty($newtopic)) {

header("Refresh: 5; url=forums.php?action=viewtopic&topicid=".$topicid."&page=last");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 5), $tracker_lang['message_add']." <br /> <a href=\"forums.php?action=viewtopic&topicid=".$topicid."&page=last\">".$tracker_lang['click_on_wait']."</a>.");
die();
}
else
header("Refresh: 5; url=forums.php?action=viewtopic&topicid=".$topicid."&page=last#".$postid."");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 5), $tracker_lang['message_add'].": ".(get_date_time())."<br /> <a href=\"forums.php?action=viewtopic&topicid=".$topicid."&page=last#".$postid."\">".$tracker_lang['click_on_wait']."</a>.");
die("catch_up");
}
exit();
break;



case 'newtopic': {

///////////////forum_new_topic///////////////

$forumid = (int)$_GET["forumid"];

if (!is_valid_id($forumid))
header("Location: forums.php");

if ($CURUSER["forum_com"]<>"0000-00-00 00:00:00"){

if ($CURUSER){
header("Refresh: 15; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 15), $tracker_lang['banned_on_forum'].": ".$CURUSER["forum_com"].".");
die;
}		else		{
stderr($tracker_lang['error_data'], $tracker_lang['not_allow_write']." ".$tracker_lang['access_denied']);
die;
}
}

stdhead($tracker_lang['topic_new']);
begin_main_frame();
insert_compose_frame($forumid);
end_main_frame();
stdfoot();
die;
///////////////forum_new_topic///////////////
}


exit();
break;


case 'search_post':  {
///////////////����� ������///////////////

$userid = (isset($_GET["userid"]) ? intval($_GET["userid"]):0);

if (!is_valid_id($userid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$tuser = get_row_count("users", "WHERE id = ".sqlesc($userid));

if (empty($tuser))
stderr($tracker_lang['error'], $tracker_lang['no_user_for_rules']);

stdhead($tracker_lang['search']);


$res = sql_query("SELECT COUNT(*) AS count, u.username FROM posts
LEFT JOIN users AS u ON u.id = userid
WHERE userid = ".sqlesc($userid)." GROUP BY userid") or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_assoc($res);

$sear = $arr["username"];
$count = $arr["count"];

list ($pagertop, $pagerbottom, $limit) = pager($postsperpage, $count, "forums.php?action=search_post&userid=".$userid."&");

fpanelka();

echo "<table class=\"forums\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";

echo "<tr><td class=\"a\" align=\"center\">";
echo "<form method=\"get\" action=\"forums.php\">".$tracker_lang['enter_iduser'].": <input type=\"hidden\" name=\"action\" value=\"search_post\" /> <input type=\"text\" id=\"searchinput\" name=\"userid\" size=\"10\" class=\"searchgif\" value=\"".$userid."\" /> <input class=\"btn\" type=\"submit\" style=\"width: 300px\" value=\"".$tracker_lang['search_message']."\" /></form>";
echo "</td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";
echo $tracker_lang['search_message'].": ".(empty($count) ? "id ".$userid : $sear)." (".$count.") ";
echo "</td></tr>";

echo "</table>";


$cached = sql_query("SELECT f.sort, f.id, f.name, f.description, f.minclassread, f.minclasswrite, f.minclasscreate, (SELECT COUNT(*) FROM topics WHERE forumid=f.id) AS numtopics, (SELECT COUNT(*) FROM posts WHERE forumid=f.id) AS numposts
FROM forums AS f ORDER BY f.sort, f.name", $cache = array("type" => "disk", "file" => "forums_cat", "time" => 60*60)) or sqlerr(__FILE__, __LINE__);

$caty = array();
while ($for = mysql_fetch_assoc_($cached)){
$caty[$for['id']] = $for;
}

$uc = get_user_class();

$res = sql_query("SELECT p.*, u.username, u.class, u.last_access, u.ip, u.signatrue, u.forum_com, u.signature,u.avatar, u.title, u.enabled, u.warned, u.hiderating, u.uploaded, u.downloaded, u.donor, e.username AS ed_username, e.class AS ed_class, t.subject, t.locked, t.forumid
FROM posts p
LEFT JOIN topics AS t ON t.id = p.topicid
LEFT JOIN users u ON u.id = p.userid
LEFT JOIN users e ON e.id = p.editedby
WHERE p.userid = ".sqlesc($userid)." ORDER BY p.id ".$limit) or sqlerr(__FILE__, __LINE__);


if ($CURUSER && get_date_time(gmtime() - 60)>= $CURUSER["forum_access"]){
sql_query("UPDATE users SET forum_access = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
unsql_cache("arrid_".$CURUSER["id"]);
}

echo "<table class=\"forums\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";

echo "<tr><td>".$pagertop."</td></tr><tr><td>";

$num = 1;
while ($arr = mysql_fetch_assoc($res)) {

$avlink = array();
++$pn;

$topicid = $arr["topicid"];
$topiccount = number_format($caty[$arr['forumid']]['numtopics']);
$postcount = number_format($caty[$arr['forumid']]['numposts']);
$forumid = $arr["forumid"];
$forumname = htmlspecialchars($caty[$arr['forumid']]['name']);
$forumdescription = trim($caty[$arr['forumid']]['description']);

if (empty($topiccount)) $topiccount = $tracker_lang['no'];
if (empty($postcount)) $postcount = $tracker_lang['no'];

if ($uc < $caty[$arr["forumid"]]["minclassread"] && $caty[$arr["forumid"]]["minclassread"] <> '0') continue;

$ed_username = $arr["ed_username"];
$ed_class= $arr["ed_class"];
$postid = $arr["id"];
$posterid = $arr["userid"];
$added = ($arr['added']);
$postername = $arr["username"];
$posterclass = $arr["class"];


$numb_view="<span style=\"font-size: 10px;\"><strong><a title=\"".$tracker_lang['stable_link'].": ".$postid."\" href='forums.php?action=viewpost&id=".$postid."'>".$postid."</a></strong></span>";

if (!empty($arr["avatar"]))
$avatar = ($CURUSER["id"]==$posterid ? "<a href=\"my.php\"><img class=\"avatars\" alt=\"".$tracker_lang['click_on_avatar']."\" title=\"".$tracker_lang['click_on_avatar']."\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/".$arr["avatar"]."\" /></a>":"<img class=\"avatars\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/".$arr["avatar"]."\" />");
else
$avatar = "<img class=\"avatars\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/default_avatar.gif\"/>";

$body = format_comment($arr["body"], true);

if (get_user_class()>= UC_MODERATOR && !empty($arr['body_ori']))
$viworiginal = true;
else
$viworiginal = false;

echo ("<a name=\"".$postid."\"></a>\n");

if ($pn == $pc)
echo ("<a name=\"last\"></a>\n");


echo "<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">

<tr>
<td colspan=\"4\" class=\"a\">

<div style=\"float: right; width: auto;\">".$numb_view."</div>

<div align=\"left\">
<a class=\"alink\" title=\"".$tracker_lang['go_to']."\" href=\"forums.php?action=viewtopic&topicid=".$arr['topicid']."\">".format_comment_light($arr["subject"])."</a><br />
".$tracker_lang['category'].": <a class=\"alink\" title=\"".$forumdescription."\" href=\"forums.php?action=viewforum&forumid=".$forumid."\"><b>".$forumname."</b></a> <span class=\"smallfont\">(".$tracker_lang['topics_many']." <b>".$topiccount."</b>, ".$tracker_lang['message_many']." <b>".$postcount."</b>)</span>
</div>

</td>
</tr>

<tr>

<td colspan=\"4\">
<var style=\"padding-right: 10px; clear: right; float: left;\">".$avatar."</var>
".$body."
".((is_valid_id($arr['editedby']) AND !empty($arr['editedby']) && (get_user_class() > UC_MODERATOR || $arr['editedby'] == $CURUSER['id'])) ? "<div style=\"margin-top: 10px\" align=\"right\"><small>".sprintf($tracker_lang['edit_lasttime'], "<a href=\"userdetails.php?id=".$row["editedby"]."\"><b>".get_user_class_color($ed_class, $ed_username)."</b></a>", $arr['editedat'])." ".($viworiginal == true ? "<a href='forums.php?action=viewpost&id=".$postid."&ori'>".$tracker_lang['original_post']."</a>":"")."</small></div>":"")."
".(($arr["signatrue"] == "yes" && $arr["signature"]) ? "<div style=\"margin-top: 10px\" align=\"right\"><small>".format_comment($arr["signature"])."</small></div>": "")."
</td>

</tr>";


echo "<tr><td class=\"a\" align=\"right\" colspan=\"4\">";

echo "<div style=\"float: left; width: auto;\">".normaltime($arr['added'],true)."</div>";

if (!empty($posterid))
$avlink[] = "<a class=\"alink\" href='forums.php?action=search_post&userid=".$posterid."'>".$tracker_lang['search_message']."</a>";

if ($CURUSER && $CURUSER["id"] <> $posterid && !empty($posterid))
$avlink[] = "<a class=\"alink\" href='message.php?receiver=".$posterid."&action=sendmessage'>".$tracker_lang['sendmessage']."</a>";

if ($CURUSER && !empty($posterid) && $CURUSER["forum_com"]=="0000-00-00 00:00:00" && $arr["locked"] == 'no')
$avlink[] = "<a class=\"alink\" href='forums.php?action=quotepost&topicid=".$topicid."&postid=".$postid."'>".$tracker_lang['quote_message']."</a>";

if (get_user_class() >= UC_MODERATOR && !empty($arr["ip"]))
$avlink[] = "<a class=\"alink\" title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$arr["ip"]."\" >".$arr["ip"]."</a>";

if (($CURUSER["forum_com"] == "0000-00-00 00:00:00" && $CURUSER["id"] == $posterid && $arr["locked"] == 'no') || get_user_class() >= UC_MODERATOR)
$avlink[] = "<a class=\"alink\" href='forums.php?action=editpost&postid=".$postid."'>".$tracker_lang['edit']."</a>";

if (get_user_class() >= UC_MODERATOR || ($CURUSER["forum_com"] == "0000-00-00 00:00:00" && $CURUSER["id"] == $posterid && $arr["locked"] == 'no'))
$avlink[] = "<a class=\"alink\" href='forums.php?action=deletepost&postid=".$postid."'>".$tracker_lang['delete']."</a>";

if (get_user_class() > UC_MODERATOR)
$avlink[] = "<a class=\"alink\" title=\"".$tracker_lang['delete_alltop_pos']."\" href=\"forums.php?action=banned&userid=".$posterid."&postid=".$postid."\" >".$tracker_lang['spam']."</a>";

if (count($avlink))
echo "<div align=\"right\">".implode(", ", $avlink)."</div>";

echo "</td></tr>";

echo "</table>";

++$num;
}


echo "</td></tr>

<tr><td colspan=\"4\">".$pagerbottom."</td></tr>

</table>";

echo "<br />";


stdfoot();
die;

}
exit();
break;










case 'editpoll': {

$pnew = (string) $_POST["new"];
$topics = (int)$_GET["topics"];
$postid = (int)$_POST["postid"];

if (!is_valid_id($topics))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);


$res = sql_query("SELECT * FROM topics WHERE id = ".sqlesc($topics))	or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_data']);
$arrpoll = mysql_fetch_assoc($res);

if ($CURUSER["id"] <> $arrpoll["userid"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);



$question = htmlspecialchars_uni($_POST["question"]);
$option0 = htmlspecialchars($_POST["option0"]);
$option1 = htmlspecialchars($_POST["option1"]);
$option2 = htmlspecialchars($_POST["option2"]);
$option3 = htmlspecialchars($_POST["option3"]);
$option4 = htmlspecialchars($_POST["option4"]);
$option5 = htmlspecialchars($_POST["option5"]);
$option6 = htmlspecialchars($_POST["option6"]);
$option7 = htmlspecialchars($_POST["option7"]);
$option8 = htmlspecialchars($_POST["option8"]);
$option9 = htmlspecialchars($_POST["option9"]);
$option10 = htmlspecialchars($_POST["option10"]);
$option11 = htmlspecialchars($_POST["option11"]);
$option12 = htmlspecialchars($_POST["option12"]);
$option13 = htmlspecialchars($_POST["option13"]);
$option14 = htmlspecialchars($_POST["option14"]);
$option15 = htmlspecialchars($_POST["option15"]);
$option16 = htmlspecialchars($_POST["option16"]);
$option17 = htmlspecialchars($_POST["option17"]);
$option18 = htmlspecialchars($_POST["option18"]);
$option19 = htmlspecialchars($_POST["option19"]);


if (!$question || !$option0 || !$option1)
stderr($tracker_lang['error'], "��������� ��� ���� �����!");

if ($_POST["ready"]=="yes"){

sql_query("UPDATE polls SET " .
"editby = " . sqlesc($CURUSER[id]) . ", " .
"edittime = " . sqlesc(get_date_time()) . ", " .
"question = " . sqlesc($question) . ", " .
"option0 = " . sqlesc($option0) . ", " .
"option1 = " . sqlesc($option1) . ", " .
"option2 = " . sqlesc($option2) . ", " .
"option3 = " . sqlesc($option3) . ", " .
"option4 = " . sqlesc($option4) . ", " .
"option5 = " . sqlesc($option5) . ", " .
"option6 = " . sqlesc($option6) . ", " .
"option7 = " . sqlesc($option7) . ", " .
"option8 = " . sqlesc($option8) . ", " .
"option9 = " . sqlesc($option9) . ", " .
"option10 = " . sqlesc($option10) . ", " .
"option11 = " . sqlesc($option11) . ", " .
"option12 = " . sqlesc($option12) . ", " .
"option13 = " . sqlesc($option13) . ", " .
"option14 = " . sqlesc($option14) . ", " .
"option15 = " . sqlesc($option15) . ", " .
"option16 = " . sqlesc($option16) . ", " .
"option17 = " . sqlesc($option17) . ", " .
"option18 = " . sqlesc($option18) . ", " .
"option19 = " . sqlesc($option19) . ", " .
"sort = " . sqlesc() . ", " .
"comment = " . sqlesc() . " " .
"WHERE forum=".sqlesc($topics)) or sqlerr(__FILE__, __LINE__);

unsql_cache("forums_ptop-".$topics);
sql_query("UPDATE topics SET polls='yes' WHERE id=".sqlesc($topics)) or sqlerr(__FILE__, __LINE__);
}

if ($pnew == "yes"){


sql_query("INSERT INTO polls VALUES(0" .
", " . sqlesc("") .
", " . sqlesc("") .
", " . sqlesc("$CURUSER[id]") .
", '" . get_date_time() . "'" .
", " . sqlesc($question) .
", " . sqlesc($option0) .
", " . sqlesc($option1) .
", " . sqlesc($option2) .
", " . sqlesc($option3) .
", " . sqlesc($option4) .
", " . sqlesc($option5) .
", " . sqlesc($option6) .
", " . sqlesc($option7) .
", " . sqlesc($option8) .
", " . sqlesc($option9) .
", " . sqlesc($option10) .
", " . sqlesc($option11) .
", " . sqlesc($option12) .
", " . sqlesc($option13) .
", " . sqlesc($option14) .
", " . sqlesc($option15) .
", " . sqlesc($option16) .
", " . sqlesc($option17) .
", " . sqlesc($option18) .
", " . sqlesc($option19) .
", " . sqlesc("") .
", " . sqlesc("") .
", " . sqlesc($topics) .
")") or sqlerr(__FILE__, __LINE__);

unsql_cache("forums_ptop-".$topics);
sql_query("UPDATE topics SET polls='yes' WHERE id=".sqlesc($topics)) or sqlerr(__FILE__, __LINE__);
}

header("Location: forums.php?action=editpost&postid=".$postid);
die;
}

exit();
break;

//////////////////////
case 'deletepost':
case 'editpost':
case 'deletetopic':
case 'editpostmod';
case 'edittopicmod';
{



if ($CURUSER["forum_com"]<>"0000-00-00 00:00:00"){

if ($CURUSER){
header("Refresh: 15; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 15), $tracker_lang['banned_on_forum'].": ".$CURUSER["forum_com"].".");
die;
} else {
stderr($tracker_lang['error_data'], $tracker_lang['not_allow_write']." ".$tracker_lang['access_denied']);
die;
}
}


if ($action == "edittopicmod" && get_user_class() >= UC_MODERATOR) {
$topicid = (int) $_GET["topicid"]; /// ����� ���������

if (!is_valid_id($topicid))
stderr($tracker_lang['error'], $tracker_lang['no_data'].": $topicid.");

$res = sql_query("SELECT * FROM topics AS t WHERE t.id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_data'].": $topicid.");

$arr = mysql_fetch_assoc($res);
$tp_subj = $arr['subject'];

$delete_topic = (int)$_POST['delete_topic'];
$reson_topic = htmlspecialchars(strip_tags($_POST['reson']));

if (!empty($arr["userid"])){

$res_u = sql_query("SELECT username FROM users WHERE id = ".sqlesc($arr["userid"])) or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($res_u) > 0){
$arr_u = mysql_fetch_assoc($res_u);
$us_name = $arr_u['username'];
} else $us_name = "id: ".$arr["userid"];

} else $us_name = "System";



if ($delete_topic == 1) {

if (empty($reson_topic)) $reson_topic = "�� �������!";

write_log("������������ ".$CURUSER['username']." ������ ���� ".(htmlspecialchars($arr["subject"]))." (#".$arr["id"].") ������������ ".$us_name.", �������: $reson_topic", "BF1B1B", "forum");

unsql_cache("forumid_".$arr["forumid"]); /// ������� ���

sql_query("DELETE FROM topics WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM posts WHERE topicid = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM polls WHERE forum = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM pollanswers WHERE forum = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
//// ������� ����� � ���� � ������ � ������ �� ������

unlinks(); /// ������� ��� ������
unsql_cache("forums.id_name");
unsql_cache("forums_cat");
unsql_cache("forums.main");
unsql_cache("forums_ptop-".$topicid);

header("Refresh: 5; url=forums.php?action=viewforum&forumid=".$arr["forumid"]."");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 5), $tracker_lang['topic_deleted']."<br /> <a href=\"forums.php?action=viewforum&forumid=".$arr["forumid"]."\">".$tracker_lang['click_on_wait']."</a>.");
die();

}

$subject = htmlspecialchars_uni($_POST['subject']);
if ($subject <> $tp_subj) {
$updateset[] = "subject = ".sqlesc($subject);
write_log("������������ ".$CURUSER['username']." ������ �������� (".$subject.") � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "98A730", "forum");
}

$locked = (string) $_POST["locked"];
$lock_arr = $arr["locked"];

if ($locked == "yes" && $lock_arr == "no") {
$updateset[] = "locked = 'yes'";
write_log("������������ ".$CURUSER['username']." �������� �������� (�������������) � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "98A730", "forum");
} elseif ($locked == "no" && $lock_arr == "yes") {
$updateset[] = "locked = 'no'";
write_log("������������ ".$CURUSER['username']." �������� �������� (��������������) � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "98A730", "forum");
}


if ($_POST["visible"] == "no" && $arr["visible"] == "yes") {
$updateset[] = "visible = 'no'";
write_log("������������ ".$CURUSER['username']." �������� �������� (��������� ������) � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "98A730", "forum");
} elseif ($_POST["visible"] == "yes" && $arr["visible"] == "no") {
$updateset[] = "visible = 'yes'";
write_log("������������ ".$CURUSER['username']." �������� �������� (��������� �������) � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "98A730", "forum");
}

$sticky = (string)$_POST["sticky"];
$sti_arr = $arr["sticky"];


if ($sticky == "yes" && $sti_arr == "no"){

$updateset[] = "sticky = 'yes'";

write_log("������������ ".$CURUSER['username']." �������� �������� (�������� ��������) � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "98A730", "forum");

} elseif ($sticky == "no" && $sti_arr == "yes"){

$updateset[] = "sticky = 'no'";

write_log("������������ ".$CURUSER['username']." �������� �������� (���� ��������) � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "98A730", "forum");

}



$forumid = (int) $_POST["forumid"];
$for_arr = $arr["forumid"];

if ($forumid<>$for_arr && is_valid_id($forumid) && !empty($forumid)){

$re3 = sql_query("SELECT name FROM forums WHERE id = ".sqlesc($forumid)) or sqlerr(__FILE__, __LINE__);
$rom = mysql_fetch_assoc($re3);

$ree3 = sql_query("SELECT name FROM forums WHERE id = ".sqlesc($for_arr)) or sqlerr(__FILE__, __LINE__);
$room = mysql_fetch_assoc($ree3);

$new_f = $rom["name"];
$now_f = $room["name"];

if (!empty($new_f)){

$updateset[] = "forumid = ".sqlesc($forumid);

write_log("������������ ".$CURUSER['username']." ���������� ���� ".htmlspecialchars($tp_subj)." � ��������� ".htmlspecialchars($new_f)." (# ".$forumid.").", "2B9145", "forum");

}

}


$modcomm = htmlspecialchars(strip_tags($_POST["modcomm"]));
if (!empty($modcomm))
$updateset[] = "t_com = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� �� ".$CURUSER["username"].": ".$modcomm."\n").", t_com)";

if (count($updateset))
sql_query("UPDATE topics SET ".implode(", ", $updateset)." WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

unlinks(); /// ������� ���

header("Refresh: 3; url=forums.php?action=viewtopic&topicid=$topicid");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 5), $tracker_lang['message_update']." <br /><a href=\"forums.php?action=viewtopic&topicid=$topicid\">".$tracker_lang['click_on_wait']."</a>.");
die();

}


if ($action == "editpostmod" && get_user_class() >= UC_MODERATOR) {

$postid = (int) $_GET["postid"]; /// ����� ���������

if (!is_valid_id($postid))
stderr($tracker_lang['error'], $tracker_lang['no_data'].": postid.");

$res = sql_query("SELECT p.*, f.name AS name_forum, t.id AS topic_id, t.t_com, t.forumid FROM posts AS p
LEFT JOIN topics AS t ON t.id = p.topicid
LEFT JOIN forums AS f ON f.id = t.forumid
WHERE p.id = ".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_message_with_such_id']);

$arr = mysql_fetch_assoc($res);

$forumid = $arr["forumid"];
$release = (int) $_POST['release'];

if (!empty($release) && get_user_class() >= UC_ADMINISTRATOR){
$re = sql_query("SELECT username FROM users WHERE id = ".sqlesc($release)) or sqlerr(__FILE__, __LINE__);
$ro = mysql_fetch_assoc($re);


$r1 = sql_query("SELECT username FROM users WHERE id = ".sqlesc($arr["userid"])) or sqlerr(__FILE__, __LINE__);
$re1 = mysql_fetch_assoc($r1);
$orig_user_post = $re1["username"];


if (!empty($ro["username"]) && $orig_user_post<>$ro["username"]){

/// ��������� ������� ��������� ���� ���� ������� ���� ��������� � topics - ������� ������=����� ������������
$one_ans = sql_query("SELECT COUNT(*) FROM posts WHERE topicid = ".sqlesc($arr["topicid"])) or sqlerr(__FILE__, __LINE__);
$one = mysql_fetch_row($one_ans);
$topicidi = $one[0];

if ($topicidi <= 1)
sql_query("UPDATE topics SET userid = ".sqlesc($release)." WHERE id = ".sqlesc($arr["topic_id"])) or sqlerr(__FILE__, __LINE__);

sql_query("UPDATE posts SET userid = ".sqlesc($release).", forumid = ".sqlesc($forumid)." WHERE id = ".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER['username']." ������ ������ ($orig_user_post �� ".$ro["username"].") ��������� (# $postid) � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "338D89", "forum");

}

}

$set_system = (int) $_POST["set_system"];
if ($set_system == 1 && !empty($arr["userid"]) && get_user_class() >= UC_ADMINISTRATOR){

/// ��������� ������� ��������� ���� ���� ������� ���� ��������� � topics - ������� ������=����� ������������
$one_ans = sql_query("SELECT COUNT(*) FROM posts WHERE topicid = ".sqlesc($arr["topicid"])) or sqlerr(__FILE__, __LINE__);
$one = mysql_fetch_row($one_ans);
$topicidi = $one[0];

if ($topicidi <= 1)
sql_query("UPDATE topics SET userid = '0' WHERE id = ".sqlesc($arr["topic_id"])) or sqlerr(__FILE__, __LINE__);

sql_query("UPDATE posts SET userid = '0' WHERE id = ".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);

write_log("������������ ".$CURUSER['username']." ������ ������ ($orig_user_post �� System) ��������� (# $postid) � ���� ".htmlspecialchars($tp_subj)." (# ".$topicid.").", "338D89", "forum");
}

$modcomm = htmlspecialchars($_POST["modcomm"]);
if (!empty($modcomm))
sql_query("UPDATE topics SET t_com = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� �� ".$CURUSER["username"].": ".$modcomm."\n").", t_com) WHERE id = ".sqlesc($arr["topicid"])) or sqlerr(__FILE__, __LINE__);


unlinks(); /// ������� ���

header("Refresh: 3; url=forums.php?action=editpost&postid=$postid");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 3), $tracker_lang['message_update']."<br /> <a href=\"forums.php?action=editpost&postid=$postid\">".$tracker_lang['click_on_wait']."</a>.");
die();


//////////////////////////////
}



if ($action == "editpost")   {

$postid = (int) $_GET["postid"];

if (!is_valid_id($postid))
stderr($tracker_lang['error'], $tracker_lang['no_data'].": postid.");

$res = sql_query("SELECT p.*, t.t_com,t.polls,t.forumid, t.subject,  u.username,u.class
FROM posts AS p
LEFT JOIN topics AS t ON t.id=p.topicid
LEFT JOIN users AS u ON u.id=p.userid
WHERE p.id=".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_message_with_such_id']);

$arr = mysql_fetch_assoc($res);
$forumi=$arr["forumid"];

$res2 = sql_query("SELECT locked FROM topics WHERE id = " . sqlesc($arr["topicid"])) or sqlerr(__FILE__, __LINE__);
$arr2 = mysql_fetch_assoc($res2);

if (mysql_num_rows($res)==0)
stderr($tracker_lang['error'], $tracker_lang['no_this_cat']);

$locked = ($arr2["locked"] == 'yes');

if (($CURUSER["id"] <> $arr["userid"] || $locked) && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
$body = htmlspecialchars_uni($_POST['body']);

if (empty($body))
stderr($tracker_lang['error'], $tracker_lang['no_data'].": body.");

if (isset($_POST["nobb"]) && !empty($_POST["nobb"]))
$body = preg_replace("/\[((\s|.)+?)\]/is", "", $body); /// ����� �� ������ [] � ��

if (isset($_POST["addurl"]) && !empty($_POST["addurl"]))
$body = preg_replace("/(http:\/\/[^\s'\"<>]+(\.(jpg|jpeg|gif|png)))/is", "[img]\\1[/img]", $body);

if ($body<>$arr["body"]) {

$editedat = get_date_time();

if (empty($arr["body_orig"]))
$updatbody[] = "body_orig = ".sqlesc(htmlspecialchars($arr["body"]));

$updatbody[] = "forumid = ".sqlesc($forumi);
$updatbody[] = "editedby = ".sqlesc($CURUSER["id"]);
$updatbody[] = "editedat = ".sqlesc($editedat);
$updatbody[] = "body = ".sqlesc($body);

sql_query("UPDATE posts SET " . implode(",", $updatbody) . " WHERE id=".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);

unlinks();
}

$returnto = htmlspecialchars_uni($_POST["returnto"]);

if (!empty($returnto)) {
$returnto .= "#".$postid; /// ��������� ����� ��������� �����

header("Refresh: 5; url=$returnto");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 5), $tracker_lang['message_update']." ".$tracker_lang['time_stamp_of'].": ".($editedat).".<br /> <a href=\"$returnto\">".$tracker_lang['click_on_wait']."</a>.");
die();

//	header("Location: $returnto");
}
else
header("Refresh: 5; url=forums.php?action=viewforum&forumid=".$arr["forumid"]."#$postid");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 5), $tracker_lang['message_update']." ".$tracker_lang['time_stamp_of'].": ".($editedat).".<br /> <a href=\"forums.php?action=viewforum&forumid=".$arr["forumid"]."#$postid\">".$tracker_lang['click_on_wait']."</a>.");
die();

}

stdhead($tracker_lang['editing']);

echo ("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
echo ("<tr><td class=\"b\" align=\"center\" colspan=\"2\"><a class=\"alink\" href=\"forums.php?action=viewtopic&topicid=".$arr["topicid"]."\">".$tracker_lang['back_inlink']."</a></td></tr>");
echo ("<tr><td class=\"colhead\" align=\"center\" colspan=\"2\"><a name=\"comments\"></a><b>.::: ".$tracker_lang['editing']." :::.</b></td></tr>");
echo ("<tr><td width=\"100%\" align=\"center\">");
echo ("<form name=\"comment\" method=\"post\" action=\"forums.php?action=editpost&postid=$postid\">");
echo ("<center><table border=\"0\"><tr><td class=\"clear\">");
echo ("<div align=\"center\">". textbbcode("comment","body",($arr["body"]), 1) ."</div>");
echo ("</td></tr></table></center>");
echo ("</td></tr><tr><td align=\"center\" colspan=\"2\">");

echo "<label><input type=\"checkbox\" title=\"������ ���� BB ��� �� ������\" name=\"nobb\" value=\"1\">nobb</label> \n";
echo "<label><input type=\"checkbox\" title=\"�������� � ���� ������� - ��� [img]\" name=\"addurl\" value=\"1\">[img]</label><br />\n";


echo ("<input type=\"hidden\" value=\"$topicid\" name=\"topicid\"/>
<input type=hidden name=returnto value=\"forums.php?action=viewtopic&topicid=".$arr["topicid"]."\">");
echo ("<input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['edit']."\" class=\"btn\" value=\"".$tracker_lang['edit']."\" />");
echo ("</td></tr></table></form><br />");


$res1 = sql_query("SELECT id AS first, (SELECT COUNT(*) FROM posts WHERE topicid=".sqlesc($arr["topicid"]).") AS count FROM posts WHERE topicid=".sqlesc($arr["topicid"])." ORDER BY id ASC LIMIT 1") or sqlerr(__FILE__, __LINE__);

$arr1 = mysql_fetch_assoc($res1);


if (($arr["first"]==$postid && $CURUSER["id"]==$arr["userid"]) || get_user_class() >= UC_MODERATOR) {


$res = sql_query("SELECT * FROM polls WHERE forum=".sqlesc($arr["topicid"])) or sqlerr(__FILE__, __LINE__);
//	stderr($tracker_lang['error'],"��� ������ � ����� ID.");
$poll = mysql_fetch_array($res);

if (empty($poll["id"]) && $arr["polls"]=="no"){
echo ("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
echo ("<tr><td class=colhead align=\"center\" colspan=\"2\"><a name=\"comments\"></a><b><center>.::: ".$tracker_lang['poll']." :::.</b></center></td></tr>");
echo ("<form method=\"post\" action=\"forums.php?action=editpoll&topics=".$arr["topicid"]."\">");
echo ("<tr><td>");

echo"<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td class=rowhead>".$tracker_lang['question']." <font color=red>*</font></td><td align=left><input name=question size=80 maxlength=255 value=".htmlspecialchars($poll['question'])."></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 1 <font color=red>*</font></td><td align=left><input name=option0 size=80 maxlength=255 value=".htmlspecialchars($poll['option0'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 2 <font color=red>*</font></td><td align=left><input name=option1 size=80 maxlength=255 value=".htmlspecialchars($poll['option1'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 3</td><td align=left><input name=option2 size=80 maxlength=255 value=".htmlspecialchars($poll['option2'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 4</td><td align=left><input name=option3 size=80 maxlength=255 value=".htmlspecialchars($poll['option3'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 5</td><td align=left><input name=option4 size=80 maxlength=255 value=".htmlspecialchars($poll['option4'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 6</td><td align=left><input name=option5 size=80 maxlength=255 value=".htmlspecialchars($poll['option5'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 7</td><td align=left><input name=option6 size=80 maxlength=255 value=".htmlspecialchars($poll['option6'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 8</td><td align=left><input name=option7 size=80 maxlength=255 value=".htmlspecialchars($poll['option7'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 9</td><td align=left><input name=option8 size=80 maxlength=255 value=".htmlspecialchars($poll['option8'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 10</td><td align=left><input name=option9 size=80 maxlength=255 value=".htmlspecialchars($poll['option9'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 11</td><td align=left><input name=option10 size=80 maxlength=255 value=".htmlspecialchars($poll['option10'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 12</td><td align=left><input name=option11 size=80 maxlength=255 value=".htmlspecialchars($poll['option11'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 13</td><td align=left><input name=option12 size=80 maxlength=255 value=".htmlspecialchars($poll['option12'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 14</td><td align=left><input name=option13 size=80 maxlength=255 value=".htmlspecialchars($poll['option13'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 15</td><td align=left><input name=option14 size=80 maxlength=255 value=".htmlspecialchars($poll['option14'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 16</td><td align=left><input name=option15 size=80 maxlength=255 value=".htmlspecialchars($poll['option15'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 17</td><td align=left><input name=option16 size=80 maxlength=255 value=".htmlspecialchars($poll['option16'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 18</td><td align=left><input name=option17 size=80 maxlength=255 value=".htmlspecialchars($poll['option17'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 19</td><td align=left><input name=option18 size=80 maxlength=255 value=".htmlspecialchars($poll['option18'])."><br /></td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 20</td><td align=left><input name=option19 size=80 maxlength=255 value=".htmlspecialchars($poll['option19'])."><br /></td></tr>

<tr><td class=rowhead>".$tracker_lang['selected']."</td><td>
<input type=radio name=new value=yes> ".$tracker_lang['yes']."
<input type=radio name=new checked value=no> ".$tracker_lang['no']."
</td></tr>
</table>";

echo("<tr><td align=\"center\" colspan=\"2\">");
echo("<input type=\"hidden\" value=\"".$postid."\" name=\"postid\" />");
echo("<input type=\"submit\" class=btn value=\"".$tracker_lang['create']." ".$tracker_lang['poll']."\" />");
echo("</td></tr></table></form><br />");

} elseif (!empty($poll["id"]) && $arr["polls"]=="yes") {

echo("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
echo("<tr><td class=colhead align=\"center\" colspan=\"2\"><a name=\"comments\"></a><b><center>.::: ".$tracker_lang['admining_poll']." :::.</b></center></td></tr>");
echo("<form method=\"post\" action=\"forums.php?action=editpoll&topics=".$arr["topicid"]."\">");
echo("<tr><td>");
echo"<table border=0 width=\"100%\" cellspacing=0 cellpadding=5>
<tr><td class=rowhead>".$tracker_lang['question']." <font color=red>*</font></td><td align=left><input name=question size=80 maxlength=255 value='".$poll['question']."'><br />".$poll['question']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 1 <font color=red>*</font></td><td align=left><input name=option0 size=80 maxlength=255 value='".$poll['option0']."'><br />".$poll['option0']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 2 <font color=red>*</font></td><td align=left><input name=option1 size=80 maxlength=255 value='".$poll['option1']."'><br />".$poll['option1']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 3</td><td align=left><input name=option2 size=80 maxlength=255 value='".$poll['option2']."'><br />".$poll['option2']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 4</td><td align=left><input name=option3 size=80 maxlength=255 value='".$poll['option3']."'><br />".$poll['option3']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 5</td><td align=left><input name=option4 size=80 maxlength=255 value='".$poll['option4']."'><br />".$poll['option4']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 6</td><td align=left><input name=option5 size=80 maxlength=255 value='".$poll['option5']."'><br />".$poll['option5']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 7</td><td align=left><input name=option6 size=80 maxlength=255 value='".$poll['option6']."'><br />".$poll['option6']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 8</td><td align=left><input name=option7 size=80 maxlength=255 value='".$poll['option7']."'><br />".$poll['option7']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 9</td><td align=left><input name=option8 size=80 maxlength=255 value='".$poll['option8']."'><br />".$poll['option8']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 10</td><td align=left><input name=option9 size=80 maxlength=255 value='".$poll['option9']."'><br />".$poll['option9']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 11</td><td align=left><input name=option10 size=80 maxlength=255 value='".$poll['option10']."'><br />".$poll['option10']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 12</td><td align=left><input name=option11 size=80 maxlength=255 value='".$poll['option11']."'><br />".$poll['option11']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 13</td><td align=left><input name=option12 size=80 maxlength=255 value='".$poll['option12']."'><br />".$poll['option12']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 14</td><td align=left><input name=option13 size=80 maxlength=255 value='".$poll['option13']."'><br />".$poll['option13']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 15</td><td align=left><input name=option14 size=80 maxlength=255 value='".$poll['option14']."'><br />".$poll['option14']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 16</td><td align=left><input name=option15 size=80 maxlength=255 value='".$poll['option15']."'><br />".$poll['option15']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 17</td><td align=left><input name=option16 size=80 maxlength=255 value='".$poll['option16']."'><br />".$poll['option16']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 18</td><td align=left><input name=option17 size=80 maxlength=255 value='".$poll['option17']."'><br />".$poll['option17']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 19</td><td align=left><input name=option18 size=80 maxlength=255 value='".$poll['option18']."'><br />".$poll['option18']."</td></tr>
<tr><td class=rowhead>".$tracker_lang['option']." 20</td><td align=left><input name=option19 size=80 maxlength=255 value='".$poll['option19']."'><br />".$poll['option19']."</td></tr>

<tr><td class=rowhead>".$tracker_lang['confirmation']."</td><td>
<input type=radio name=ready value=yes>".$tracker_lang['yes']."
<input type=radio name=ready checked value=no> ".$tracker_lang['no']."
</td></tr>
</table>";

echo("<tr><td align=\"center\" colspan=\"2\">");
echo("<input type=\"hidden\" value=\"".$postid."\" name=\"postid\"/>");
echo("<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['edit']." ".$tracker_lang['poll']."\" />");
echo("</td></tr></table></form><br />");

}
}
if (get_user_class() >= UC_MODERATOR) {

$modcomment=htmlspecialchars($arr["t_com"]);
$forums=htmlspecialchars($arr["subject"]);
echo("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
echo("<tr><td class=\"colhead\" align=\"center\" colspan=\"2\"><a name=\"comments\"></a><b><center>.::: ".$tracker_lang['admining_topic']." :::.</b></center></td></tr>");

$user_orig="<a href=\"userdetails.php?id=".$arr["userid"]."\">".get_user_class_color($arr["class"], $arr["username"]) . "</a>";

echo("<form method=\"post\" action=\"forums.php?action=editpostmod&postid=$postid\">");
echo("<tr><td>");

if (get_user_class() >= UC_ADMINISTRATOR) {
echo("<tr><td class=\"a\"><b>".$tracker_lang['msg_is']." SyStem</b>:  <input type=checkbox name=set_system value=1> <i>".$tracker_lang['msg_from_system']."</i></td></tr>");

echo("<tr><td class=\"a\"><b>".$tracker_lang['assign_author']."</b>:  <input type=\"text\" size=\"8\" name=\"release\"> <i>".sprintf($tracker_lang['new_topic_rename'], $user_orig)."</i></td></tr>");
}

echo("<tr><td class=\"a\" align=\"center\">".$tracker_lang['history_topic']." <b>$forums</b> [".strlen($modcomment)."]<br />
<textarea cols=100% rows=6".(get_user_class() < UC_SYSOP ? " readonly" : " name=modcomment").">$modcomment</textarea>
</td></tr>
<tr><td class=\"a\" align=\"center\"><b>".$tracker_lang['add_note']."</b>:<br /><textarea cols='100%' rows='3' name='modcomm'></textarea>
</td></tr>
");

echo("<tr><td align=\"center\" colspan=\"2\">");
echo("<input type=\"hidden\" value=\"$topicid\" name=\"topicid\"/>");
echo("<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['edit']."\" />");
echo("</td></tr></table></form><br />");
}

stdfoot();
die;
}

if ($action == "deletepost") {
$postid = (int) $_GET["postid"];

$sure = (int) $_GET["sure"];

if (!is_valid_id($postid))
stderr($tracker_lang['error'], $tracker_lang['no_data'].": postid.");


$res = sql_query("SELECT topicid,userid FROM posts WHERE id=".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res) or stderr($tracker_lang['error'], $tracker_lang['no_message_with_such_id']);

$topicid = $arr["topicid"];
$userid=$arr["userid"];


if (get_user_class() < UC_MODERATOR && $userid<>$CURUSER["id"])
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$res = sql_query("SELECT id AS first, (SELECT COUNT(*) FROM posts WHERE topicid=".sqlesc($topicid).") AS count FROM posts WHERE topicid=".sqlesc($topicid)." ORDER BY id ASC LIMIT 1") or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_assoc($res);

if ($arr["count"] < 2 || $arr["first"]==$postid)
stderr($tracker_lang['selected'], sprintf($tracker_lang['confirm_del_post_topics'], "forums.php?action=deletetopic&topicid=$topicid&sure=1"));




$res = sql_query("SELECT id FROM posts WHERE topicid=".sqlesc($topicid)." AND id < ".sqlesc($postid)." ORDER BY id DESC LIMIT 1") or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($res) == 0)
$redirtopost = "";
else
{
$arr = mysql_fetch_row($res);
//$redirtopost = "&page=p$arr[0]#$arr[0]";
}

if (!$sure)
stderr($tracker_lang['delete'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $tracker_lang['message_one'], "forums.php?action=deletepost&postid=$postid&sure=1"));


$res_fo = sql_query("SELECT t.subject,t.forumid,t.t_com, u.username
FROM topics AS t
LEFT JOIN forums AS f ON f.id=t.forumid
LEFT JOIN users AS u ON u.id = ".sqlesc($userid)."
WHERE t.id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
$arr_for = mysql_fetch_assoc($res_fo);

if (!empty($arr_for)){
$modiki=$arr_for["t_com"]; /// �������� ����������� ��������� ������
$subject=$arr_for["subject"];

if (empty($arr_for["username"]) && $userid<>0)
$usera="user: $userid";
elseif (empty($arr_for["username"]) && $userid==0)
$usera="System";
else
$usera=$arr_for["username"];

$modiki = date("Y-m-d") . " - $CURUSER[username] ������ ��������� # $postid ($usera).\n" . $modiki;

sql_query("UPDATE topics SET t_com =".sqlesc($modiki)." WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
}

sql_query("DELETE FROM posts WHERE id=".sqlesc($postid)) or sqlerr(__FILE__, __LINE__);

$added = "0";
update_topic_last_post($topicid, $added, $postid);
unlinks();

unsql_cache("forums_cat");
unsql_cache("forums.main");


header("Refresh: 3; url=forums.php?action=viewtopic&topicid=$topicid");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 3), $tracker_lang['message_deleted']."<br /> <a href=\"forums.php?action=viewtopic&topicid=$topicid\">".$tracker_lang['click_on_wait']."</a>.");
die();
}

if ($action == "deletetopic" && $_GET["sure"] == 1){

$topicid = (int) $_GET["topicid"];

if (!is_valid_id($topicid))
stderr($tracker_lang['error'], $tracker_lang['no_data'].": topicid.");

$res_fo = sql_query("SELECT t.subject, t.forumid, t.userid
FROM topics AS t
LEFT JOIN forums AS f ON f.id = t.forumid
WHERE t.id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res_fo) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_message_with_such_id']);


$arr_for = mysql_fetch_assoc($res_fo);

if (get_user_class() < UC_MODERATOR && $CURUSER["id"] <> $arr_for["userid"]){
header("Refresh: 3; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 3), $tracker_lang['access_denied']);
die();
}


$subject = $arr_for["subject"];

if (!empty($arr_for["userid"])){

$res_u = sql_query("SELECT username FROM users WHERE id = ".sqlesc($arr_for["userid"])) or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($res_u) > 0){
$arr_u = mysql_fetch_assoc($res_u);
$us_name = $arr_u['username'];
} else $us_name = "id: ".$arr_for["userid"];

} else $us_name = "System";

write_log("������������ ".$CURUSER['username']." ������ ���� $subject (# $topicid) � ��� ��������� � ���, ������������ $us_name.", "BF1B1B", "forum");

$forumfid = $arr_for["forumid"];

sql_query("DELETE FROM topics WHERE id = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM posts WHERE topicid = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM polls WHERE forum = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM pollanswers WHERE forum = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

unlinks();

unsql_cache("forums_cat");
unsql_cache("forums.main");
unsql_cache("forums_ptop-".$topicid);

if ($forumfid){

header("Refresh: 3; url=forums.php?action=viewforum&forumid=$forumfid");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 3), $tracker_lang['topic_deleted'].".<br /> <a href=\"forums.php?action=viewforum&forumid=$forumfid\">".$tracker_lang['click_on_wait']."</a>.");
die();
} else
@header("Location: forums.php");

die;
}



}
exit();
break;





case 'viewunread': {

$userid = $CURUSER['id'];

if (empty($userid)){
header("Refresh: 3; url=forums.php");
stderr(sprintf($tracker_lang['succes_redir_of_sec'], 3), $tracker_lang['access_denied']);
die();
}

$maxresults = 100;

$cached = sql_query("SELECT f.sort, f.id, f.name, f.description, f.minclassread, f.minclasswrite, f.minclasscreate, (SELECT COUNT(*) FROM topics WHERE forumid=f.id) AS numtopics, (SELECT COUNT(*) FROM posts WHERE forumid=f.id) AS numposts
FROM forums AS f ORDER BY f.sort, f.name", $cache = array("type" => "disk", "file" => "forums_cat", "time" => 60*60)) or sqlerr(__FILE__, __LINE__);

$caty = array();
while ($for = mysql_fetch_assoc_($cached)){
$caty[$for['id']] = $for;
}


$res = sql_query("SELECT st.views, st.id, st.forumid, st.subject, st.lastpost, u.class, u.username, sp.added, sp.userid, sp.id AS lastposti
FROM topics AS st
LEFT JOIN posts AS sp ON st.lastpost = sp.id
LEFT JOIN users AS u ON u.id = sp.userid
WHERE sp.added > ".sqlesc(get_date_time(gmtime() - $Forum_Config["readpost_expiry"]))." ORDER BY st.forumid LIMIT 100") or sqlerr(__FILE__, __LINE__);

stdhead($tracker_lang['mail_unread_desc']);

fpanelka ();

echo "<table border='0' cellspacing='0' cellpadding='5' width='100%'><tr><td class=\"colhead\" align=\"center\">".$tracker_lang['mail_unread_desc']."</td></tr></table>";

$n = 0;
$uc = get_user_class();

while ($arr = mysql_fetch_assoc($res)) {

$topiccount = number_format($caty[$arr['forumid']]['numtopics']);
$postcount = number_format($caty[$arr['forumid']]['numposts']);
$forumname = htmlspecialchars($caty[$arr['forumid']]['name']);
$forumdescription = trim($caty[$arr['forumid']]['description']);

if (empty($topiccount)) $topiccount = $tracker_lang['no'];
if (empty($postcount)) $postcount = $tracker_lang['no'];

if ($n%2==0){
$cl1 = "class=\"a\"";
$cl2 = "class=\"b\"";
} else {
$cl2 = "class=\"a\"";
$cl1 = "class=\"b\"";
}

$topicid = $arr['id'];
$lastposti=$arr['lastposti'];
$forumid = $arr['forumid'];
$username = $arr['username'];
$class = $arr['class'];
$time = ($arr["added"]);
$use_id= $arr["userid"];

if (empty($use_id) && empty($username))
$user_view = "<font color=gray>[System]</font>";
elseif (!empty($use_id) && empty($username))
$user_view = "<b>id</b>: ".$use_id;
else
$user_view = "<b><a href=\"userdetails.php?id=".$use_id."\">".get_user_class_color($class, $username)."</a></b>";



$count_l = get_row_count("readposts", "WHERE userid = ".sqlesc($userid)." AND topicid = ".sqlesc($topicid)." AND lastpostread = ".sqlesc($arr['lastpost']));
if (!empty($count_l)) continue;

if (($caty[$forumid]['minclassread'] >= $uc && $CURUSER) || ($caty[$forumid]['minclassread'] > 0 && !$CURUSER)) continue;

if ($n > $maxresults) break;

if ($n == 0) {

if (get_user_class() == UC_SYSOP){

?>
<script language="Javascript" type="text/javascript">
<!-- Begin
var checkflag = "false";
var marked_row = new Array;
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
}
else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
}
}

</script>
<?

echo("<form method=\"post\" name=\"yepi\" action=\"forums.php?action=b_actions\">");

}


echo("<table border=\"0\" cellspacing=\"0\" width=\"100%\" cellpadding=\"5\">\n");
echo "<tr>";

echo "
<td class=\"colhead\" align=\"center\" width=\"40px\">".$tracker_lang['status']."</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['subject']."</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['category']."</td>
";

if (get_user_class() == UC_SYSOP)
echo("<td class=\"colhead\" align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['mark_all']."\" onclick=\"this.value=check(document.yepi.elements);\"></td>\n");

echo "</tr>";
}

echo "<tr>";

echo("<td ".$cl1." align=\"center\"><img src=\"{$forum_pic_url}unlockednew.gif\"></td>
<td ".$cl2."><a class=\"alink\" title=\"".$tracker_lang['go_to']."\" href=\"forums.php?action=viewtopic&topicid=$topicid&page=last#$lastposti\"><b>".format_comment_light($arr["subject"])."</b></a><br />".$tracker_lang['subscribe_last_comment'].": $user_view ".$tracker_lang['in']." $time</td>
<td align=\"left\" ".$cl1."><a class=\"alink\" title=\"".$forumdescription."\" href=\"forums.php?action=viewforum&forumid=".$forumid."\"><b>".$forumname."</b></a> <span class=\"smallfont\">(".$tracker_lang['topics_many']." <b>".$topiccount."</b>, ".$tracker_lang['message_many']." <b>".$postcount."</b>)</span><br /><small>".$forumdescription."</small></td>");

if (get_user_class() == UC_SYSOP)
echo "<td ".$cl2." align=\"center\"><input type=\"checkbox\" class=\"desacto\" name=\"cheKer[]\" value=\"".$topicid."\" /></td>";

echo "</tr>";

++$n;
}

if ($n > 0) {


if (get_user_class() == UC_SYSOP){

global $tracker_lang;

$view_lang = array_map("trim", explode("|", $tracker_lang['forum_baction']));

echo "<tr><td align=\"right\" colspan=\"5\">";

echo "<select name=\"actions\" id=\"myoptn\">\n
<option selected>".$tracker_lang['signup_not_selected']."</option>
<option value=\"main\">".$view_lang[0]."</option>
<option value=\"unmain\">".$view_lang[1]."</option>
<option value=\"lock\">".$view_lang[2]."</option>
<option value=\"unlock\">".$view_lang[3]."</option>
<option value=\"delpoll\">".$view_lang[4]."</option>
<option value=\"dellsmgs\">".$view_lang[5]."</option>
<option value=\"noown\">".$view_lang[6]."</option>
<option value=\"movcat\">".$view_lang[7]."</option>
<option value=\"delete\">".$view_lang[8]."</option>
<option value=\"del_wip\">".$view_lang[9]."</option>
<option value=\"del_wem\">".$view_lang[10]."</option>
<option value=\"del_wall\">".$view_lang[11]."</option>
</select>\n";

echo "<select name=\"pcat\" id=\"nmovcat\" style=\"display: none;\">\n";
echo "<option selected>".$tracker_lang['no_choose']."</option>";

$res = sql_query("SELECT id, name, minclasswrite, minclassread, (SELECT COUNT(*) FROM topics WHERE topics.forumid=forums.id) AS exif FROM forums ORDER BY sort, name", $cache = array("type" => "disk", "file" => "forums.id_name", "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);
while ($arr = mysql_fetch_assoc_($res))
echo "<option ".(empty($arr["exif"]) ? "style=\"font-weight:bold;\"":"")." value=\"".$arr["id"]."\" ".($currentforum == $arr["id"] ? " selected>" : ">").$arr["name"]."</option>\n";

echo "</select>\n";

echo "<input type=\"submit\" class=\"btn\" onClick=\"return confirm('".$tracker_lang['warn_sure_action']."')\" value=\"".$tracker_lang['b_action']."\" /></td></tr>\n";
}



echo ("</table>\n");

if (get_user_class() == UC_SYSOP)
echo ("</form>");

//if ($n > $maxresults)
//echo ("<p><b>".sprintf($tracker_lang['found_of'], $maxresults)."</b></p>\n");

echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\"><tr><td class=\"b\" align=\"center\"><a class=\"alink\" href=\"forums.php?action=catchup\"><b>".$tracker_lang['forum_markread']."</b></a></td></tr></table>\n";
} else
echo "<table class=\"main\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"embedded\"><div align=\"center\" class=\"error\">".$tracker_lang['nothing_found']."</div></td></tr></table>";

echo "<br />";

stdfoot();
die;
}

exit();
break;





case 'search': {

stdhead($tracker_lang['search']);

fpanelka ();

$keywords = (isset($_GET["keywords"]) ? trim(htmlspecialchars_uni($_GET["keywords"])) : "");

echo ("<form method=\"get\" action=\"forums.php\">
<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">

<tr><td class=\"a\" title=\"".$tracker_lang['search']."\" align=\"center\">
<input type=\"text\" size=\"55\" name=\"keywords\" value=\"".htmlspecialchars($keywords)."\" />

<input type=\"hidden\" name=\"action\" value=\"search\" />
<input type=\"submit\" value=\"".$tracker_lang['search']."\" class=\"btn\" />
</td></tr>

</table></form><br />");


if ($CURUSER && empty($keywords)){
echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";

echo "<tr><td class=\"b\">

<fieldset class=\"fieldset\">
<legend>".$tracker_lang['warning']."</legend>
".sprintf($tracker_lang['forum_selcat'], "<strong>".$tracker_lang['fast_redirect']."</strong>", ".::: <strong>".$tracker_lang['creating_topic']."</strong> :::.")."</fieldset>

</td></tr>";

echo "</table><br />";
}


if (!empty($keywords)) {

$page = max(1, isset($_GET["page"]) ? (intval($_GET["page"])) : 0);


$cached = sql_query("SELECT f.sort, f.id, f.name, f.description, f.minclassread, f.minclasswrite, f.minclasscreate, (SELECT COUNT(*) FROM topics WHERE forumid=f.id) AS numtopics, (SELECT COUNT(*) FROM posts WHERE forumid = f.id) AS numposts
FROM forums AS f ORDER BY f.sort, f.name", $cache = array("type" => "disk", "file" => "forums_cat", "time" => 60*60)) or sqlerr(__FILE__, __LINE__);

$caty = array();
while ($for = mysql_fetch_assoc_($cached)){
$caty[$for['id']] = $for;
}

$search = htmlspecialchars_uni($keywords);
$q2 = str_replace("."," ", sqlesc("%".sqlwildcardesc(trim($search))."%"));

$search = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", $search));

$sear = array("'","\"","%","$","/","`","`","<",">");
$search = str_replace($sear, " ", $search);

$list = explode(" ", $search);

$listrow = array();

foreach ($list AS $lis){
if (strlen($lis) > 3)
$listrow[] = "+".$lis;
}

if (strlen($search) >= 4 && count($listrow))
$res = sql_query("SELECT COUNT(*) FROM posts WHERE MATCH (posts.body) AGAINST ('".trim(implode(" ", $listrow))."' IN BOOLEAN MODE)") or sqlerr(__FILE__, __LINE__);
elseif (!empty($search))
$res = sql_query("SELECT COUNT(*) FROM posts WHERE posts.body LIKE {$q2}") or sqlerr(__FILE__, __LINE__);

$arr = @mysql_fetch_row($res);
$hits = $arr[0];

echo("<table border='0' cellspacing='0' cellpadding='5' width='100%'><tr><td class=b align=center>".$tracker_lang['search'].": ".htmlspecialchars($search)."</b></td></tr></table>");

if (empty($hits)){
echo "<table border='0' cellspacing='0' cellpadding='5' width='100%'><tr><td class=b align=center>".$tracker_lang['no_data']."</td></tr></table>";
} else {

$count = $hits;

list($pagertop, $pagerbottom, $limit) = pager($postsperpage, $count, "forums.php?action=search&keywords=".htmlspecialchars($keywords)."&");


if (strlen($search) >= 4 || count($ekeywords))
$res = sql_query("SELECT id, topicid, userid, added FROM posts WHERE MATCH (posts.body) AGAINST ('".trim(implode(" ", $listrow))."' IN BOOLEAN MODE) ".$limit) or sqlerr(__FILE__, __LINE__);
elseif (!empty($search))
$res = sql_query("SELECT id, topicid, userid, added FROM posts WHERE posts.body LIKE {$q2} ".$limit) or sqlerr(__FILE__, __LINE__);


$num = @mysql_num_rows($res);

if (get_user_class() == UC_SYSOP){

?>
<script language="Javascript" type="text/javascript">
<!-- Begin
var checkflag = "false";
var marked_row = new Array;
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
}
else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
}
}

</script>
<?

echo("<form method=\"post\" name=\"yepi\" action=\"forums.php?action=b_actions\">");

}

echo("<table border=0 cellspacing=0 cellpadding=5 width='100%'>\n");

echo ("<tr><td colspan=\"4\">".$pagertop."</td></tr>");

echo("<tr>
<td class=\"colhead\">#</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['subject']." / ".$tracker_lang['news_poster']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['category']."</td>
");

if (get_user_class() == UC_SYSOP)
echo("<td class=\"colhead\" align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['mark_all']."\" onclick=\"this.value=check(document.yepi.elements);\"></td>\n");

echo "</tr>\n";

for ($i = 0; $i < $num; ++$i) {

$topiccount = number_format($caty[$topic["forumid"]]['numtopics']);
$postcount = number_format($caty[$topic["forumid"]]['numposts']);

if (empty($topiccount)) $topiccount = $tracker_lang['no'];
if (empty($postcount)) $postcount = $tracker_lang['no'];

$post = mysql_fetch_assoc($res);
$res2 = sql_query("SELECT forumid, subject FROM topics WHERE id = ".sqlesc($post["topicid"])) or sqlerr(__FILE__, __LINE__);

$topic = mysql_fetch_assoc($res2);

if (get_user_class() < $caty[$topic["forumid"]]["minclassread"] && $caty[$topic["forumid"]]["minclassread"] <> '0'){
--$hits;
continue;
}

$res2 = sql_query("SELECT username, class FROM users WHERE id = ".sqlesc($post["userid"])) or sqlerr(__FILE__, __LINE__);
$user = mysql_fetch_assoc($res2);


if (!empty($post["userid"]))
$user = "id: ".$post["userid"];
elseif (empty($post["userid"]))
$user = "<font color=gray>[<b>System</b>]</font>";
else
$user = "<a href=\"userdetails.php?id=".$post["userid"]."\">".get_user_class_color($user["class"], $user["username"])."</a>";

$forumdescription = htmlspecialchars($caty[$topic["forumid"]]["description"]);
$forumname = htmlspecialchars($caty[$topic["forumid"]]["name"]);


if ($i%2==0){
$cl1 = "class=a";
$cl2 = "class=b";
} else {
$cl2 = "class=a";
$cl1 = "class=b";
}

echo ("<tr>
<td $cl2>".$post["id"]."</td>
<td $cl1 align=\"left\"><a class=\"alink\" title=\"".$tracker_lang['go_to']."\" href=\"forums.php?action=viewtopic&topicid=".$post["topicid"]."&page=p".$post["id"]."#".$post["id"]."\"><b>".format_comment_light($topic["subject"])."</b></a>
<div><b>".$user."</b>, ".($post["added"])."</div>
</td>
<td $cl2 align=\"center\"><a class=\"alink\" title=\"".$forumdescription."\" href=\"forums.php?action=viewforum&forumid=".$topic["forumid"]."\"><b>".$forumname."</b></a> <span class=\"smallfont\">(".$tracker_lang['topics_many']." <b>".$topiccount."</b>, ".$tracker_lang['message_many']." <b>".$postcount."</b>)</span><br /><small>".$forumdescription."</small></td>
");

if (get_user_class() == UC_SYSOP)
echo "<td ".$cl1." align=\"center\"><input type=\"checkbox\" class=\"desacto\" name=\"cheKer[]\" value=\"".$post["topicid"]."\" /></td>";

echo "</tr>";

}

if (get_user_class() == UC_SYSOP){

global $tracker_lang;

$view_lang = array_map("trim", explode("|", $tracker_lang['forum_baction']));

echo "<tr><td align=\"right\" colspan=\"5\">";

echo "<select name=\"actions\" id=\"myoptn\">\n
<option selected>".$tracker_lang['signup_not_selected']."</option>
<option value=\"main\">".$view_lang[0]."</option>
<option value=\"unmain\">".$view_lang[1]."</option>
<option value=\"lock\">".$view_lang[2]."</option>
<option value=\"unlock\">".$view_lang[3]."</option>
<option value=\"delpoll\">".$view_lang[4]."</option>
<option value=\"dellsmgs\">".$view_lang[5]."</option>
<option value=\"noown\">".$view_lang[6]."</option>
<option value=\"movcat\">".$view_lang[7]."</option>
<option value=\"delete\">".$view_lang[8]."</option>
<option value=\"del_wip\">".$view_lang[9]."</option>
<option value=\"del_wem\">".$view_lang[10]."</option>
<option value=\"del_wall\">".$view_lang[11]."</option>
</select>\n";

echo "<select name=\"pcat\" id=\"nmovcat\" style=\"display: none;\">\n";
echo "<option selected>".$tracker_lang['no_choose']."</option>";

$res = sql_query("SELECT id, name, minclasswrite, minclassread, (SELECT COUNT(*) FROM topics WHERE topics.forumid = forums.id) AS exif FROM forums ORDER BY sort, name", $cache = array("type" => "disk", "file" => "forums.id_name", "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);
while ($arr = mysql_fetch_assoc_($res))
echo "<option ".(empty($arr["exif"]) ? "style=\"font-weight:bold;\"":"")." value=\"".$arr["id"]."\" ".($currentforum == $arr["id"] ? " selected>" : ">").$arr["name"]."</option>\n";

echo "</select>\n";

echo "<input type=\"submit\" class=\"btn\" onClick=\"return confirm('".$tracker_lang['warn_sure_action']."')\" value=\"".$tracker_lang['b_action']."\" /></td></tr>\n";
}


echo ("<tr><td colspan=\"4\">".$pagerbottom."</td></tr>");

echo("</table></form><br />");

}

}

if (empty($keywords))
insert_quick_jump_menu();

stdfoot();
die;
}

exit();
break;













case 'viewpost': {

$id = (isset($_GET["id"]) ? intval($_GET["id"]) :0);

if (!is_valid_id($id)){
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);
die;
}

$res = sql_query("SELECT * FROM topics WHERE id = (SELECT topicid FROM posts WHERE id = ".sqlesc($id).")") or sqlerr(__FILE__,__LINE__);
$arr_2 = mysql_fetch_assoc($res) or stderr($tracker_lang['error'], $tracker_lang['topic_deleted']);

$topicid = $arr_2['id'];

$forum_id = $arr_2['forumid'];

$sticky = $arr_2["sticky"];
$visible = $arr_2["visible"];
$lock_ed = $arr_2["locked"];
$subject = format_comment_light($arr_2["subject"]);

$locked = ($arr_2["locked"] == 'yes' ? $tracker_lang['yes']:$tracker_lang['no']);
$sticky = ($arr_2["sticky"] == "yes" ? $tracker_lang['yes']:$tracker_lang['no']);


$cached = sql_query("SELECT f.sort, f.id, f.name, f.description, f.minclassread, f.minclasswrite, f.minclasscreate, (SELECT COUNT(*) FROM topics WHERE forumid=f.id) AS numtopics, (SELECT COUNT(*) FROM posts WHERE forumid=f.id) AS numposts
FROM forums AS f ORDER BY f.sort, f.name", $cache = array("type" => "disk", "file" => "forums_cat", "time" => 60*60)) or sqlerr(__FILE__, __LINE__);

$caty = array();
while ($for = mysql_fetch_assoc_($cached)){
$caty[$for['id']] = $for;
}


$topiccount = number_format($caty[$forum_id]['numtopics']);
$postcount = number_format($caty[$forum_id]['numposts']);
$forumid = $arr_2['forumid'];
$forumname = htmlspecialchars($caty[$forum_id]['name']);
$forumdescription = trim($caty[$forum_id]['description']);

if (empty($topiccount)) $topiccount = $tracker_lang['no'];
if (empty($postcount)) $postcount = $tracker_lang['no'];

stdhead($tracker_lang['message_view']);

fpanelka();


echo "<table cellpadding=\"0\" cellspacing=\"0\" id=\"main\" width=\"100%\">";

echo "<tr><td class=\"b\" align=\"center\">
<time itemprop=\"startDate\" datetime=\"".date('c', strtotime($lastdate))."\"><h1><a class=\"alink\" title=\"".$tracker_lang['go_to']."\" href=\"forums.php?action=viewtopic&topicid=".$arr_2['id']."\">".$subject."</a></h1></time>
".$tracker_lang['category'].": <a class=\"alink\" title=\"".$tracker_lang['go_to']."\" href=\"forums.php?action=viewforum&forumid=".$forum_id."\">".$caty[$forum_id]['name']."</a> <span class=\"smallfont\">(".$tracker_lang['topics_many']." <b>".$topiccount."</b>, ".$tracker_lang['message_many']." <b>".$postcount."</b>)</span>
</td></tr>";

$views=number_format($arr_2["views"]);
$num_com=number_format($arr_2["num_com"]);

echo "<tr>
<td class=\"a\" align=\"center\">
<b>".$tracker_lang['views']."</b>: ".$views.",
<b>".$tracker_lang['commens']."</b>: ".$num_com.",
<b>".$tracker_lang['stickyf']."</b>: ".$sticky.",
<b>".$tracker_lang['block_is_topic']."</b>: ".$locked."
</td>
</tr>";


echo "</table>";


$res = sql_query("SELECT p. *, u.username, u.class, u.last_access, u.ip, u.signatrue,u.forum_com, u.signature,u.avatar, u.title, u.enabled, u.warned, u.hiderating, u.uploaded, u.downloaded, u.donor, e.username AS ed_username, e.class AS ed_class, (select count(*) FROM posts WHERE userid = p.userid) AS num_topuser
FROM posts p
LEFT JOIN users u ON u.id = p.userid
LEFT JOIN users e ON e.id = p.editedby
WHERE p.id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);


if ($CURUSER && get_date_time(gmtime() - 60)>= $CURUSER["forum_access"]){
sql_query("UPDATE users SET forum_access = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
unsql_cache("arrid_".$CURUSER["id"]);
}

$pc = mysql_num_rows($res);
$pn = 0;
$r = sql_query("SELECT lastpostread FROM readposts WHERE userid = ".sqlesc($CURUSER["id"])." AND topicid = ".sqlesc($topicid)) or sqlerr(__FILE__, __LINE__);

$a = mysql_fetch_assoc($r);
$lpr = $a["lastpostread"];

echo "<table class=\"forums\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"><tr><td>";

$num = 1;
while ($arr = mysql_fetch_assoc($res)) {

$alinke = array();
++$pn;

$ed_username = $arr["ed_username"];
$ed_class= $arr["ed_class"];
$postid = $arr["id"];
$posterid = $arr["userid"];
$added = ($arr['added']);
$postername = $arr["username"];
$posterclass = $arr["class"];

if (empty($postername) && $posterid <> 0)
$by = "<b>id</b>: ".$posterid;
elseif ($posterid==0 && empty($postername))
$by = "<i>".$tracker_lang['msg_is']." </i><font color=gray>[<b>System</b>]</font>";
else
$by = "<a href='userdetails.php?id=".$posterid."'><b>" .get_user_class_color($posterclass,  $postername). "</b></a>";

if ($posterid<>0 && !empty($postername)){

if (strtotime($arr["last_access"]) > gmtime() - 600) {
$online = "online";
$online_text = $tracker_lang['online'];
} else {
$online = "offline";
$online_text = $tracker_lang['offline'];
}

if ($arr["downloaded"] > 0) {
$ratio = $arr['uploaded'] / $arr['downloaded'];
$ratio = number_format($ratio, 2);
} elseif ($arr["uploaded"] > 0)
$ratio = "Infinity";
else
$ratio = "---";


if ($row["hiderating"]=="yes")
$print_ratio = "<b>+100%</b>";
else
$print_ratio = $ratio;

} else {
$ratio = "---";
$print_ratio = $ratio;
unset($online_text);
unset($online);
}


if ($arr["forum_com"]<>"0000-00-00 00:00:00" && !empty($postername))
$ban = "<div><b>".$tracker_lang['banned_on']." </b>".$arr["forum_com"]."</div>";
else unset($ban);


$online_view = "<img src=\"".$DEFAULTBASEURL."/pic/button_".$online.".gif\" alt=\"".$online_text."\" title=\"".$online_text."\" style=\"position: relative; top: 2px;\" border=\"0\" height=\"14\">";

$numb_view = "<span style=\"font-size: 10px;\"><strong><a title=\"".$tracker_lang['stable_link'].": ".$postid."\" href='forums.php?action=viewpost&id=".$postid."'>".$postid."</a></strong></span>";

if (!empty($arr["avatar"]))
$avatar = ($CURUSER["id"]==$posterid ? "<a href=\"my.php\"><img class=\"avatars\" alt=\"".$tracker_lang['click_on_avatar']."\" title=\"".$tracker_lang['click_on_avatar']."\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/".$arr["avatar"]."\" /></a>":"<img class=\"avatars\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/".$arr["avatar"]."\" />");
else
$avatar = "<img class=\"avatars\" width=\"".$Ava_config['width']."\" src=\"".$DEFAULTBASEURL."/pic/avatar/default_avatar.gif\" />";


$body = format_comment($arr["body"], true);


if (get_user_class()>= UC_MODERATOR && !empty($arr['body_ori']))
$viworiginal = true;
else
$viworiginal = false;

echo ("<a name=\"".$postid."\"></a>\n");

if ($pn == $pc)
echo("<a name=\"last\"></a>\n");


echo "<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">

<tr>
<td colspan=\"4\" class=\"a\">

<div style=\"float: right; width: auto;\">".$numb_view."</div>
<div align=\"left\">".$by." ".$online_view."
".get_user_class_name($arr["class"])." ".(empty($arr["title"]) ? "": "(".htmlspecialchars($arr["title"]).")")."
".($arr["donor"] == "yes" ? "<img src=\"".$DEFAULTBASEURL."/pic/star.gif\" alt='".$tracker_lang['donor']."'>" : "")
.($arr["enabled"] == "no" ? "<img src=\"".$DEFAULTBASEURL."/pic/disabled.gif\" alt=\"".$tracker_lang['disabled']."\" style='margin-left: 2px'>" : ($arr["warned"] == "yes" ? "<img src=\"".$DEFAULTBASEURL."/pic/warned.gif\" alt=\"".$tracker_lang['warned']."\" border=0>" : "")) . "
</div>

<div align=\"left\">
<b>".$tracker_lang['ratio']."</b>: ".$print_ratio.", <b>".$tracker_lang['uploaded']."</b>: ".mksize($arr["uploaded"]) .", <b>".$tracker_lang['downloaded']."</b>: ".mksize($arr["downloaded"])." ".$ban."
</div>

</td>
</tr>

<tr>

<td colspan=\"4\">
<var style=\"padding-right: 10px; clear: right; float: left;\">".$avatar."</var>
".$body."
".((is_valid_id($arr['editedby']) AND !empty($arr['editedby']) && (get_user_class() > UC_MODERATOR || $arr['editedby'] == $CURUSER['id'])) ? "<div style=\"margin-top: 10px\" align=\"right\"><small>".sprintf($tracker_lang['edit_lasttime'], "<a href=\"userdetails.php?id=".$row["editedby"]."\"><b>".get_user_class_color($ed_class, $ed_username)."</b></a>", $arr['editedat'])." ".($viworiginal == true ? "<a href=\"forums.php?action=viewpost&id=".$postid."&ori\">".$tracker_lang['original_post']."</a>":"")."</small></div>":"")."
".(($arr["signatrue"] == "yes" && $arr["signature"]) ? "<div style=\"margin-top: 10px\" align=\"right\"><small>".format_comment($arr["signature"])."</small></div>": "")."
</td>

</tr>";


echo "<tr><td class=\"a\" align=\"right\" colspan=\"4\">";

echo "<div style=\"float: left; width: auto;\">".normaltime($arr['added'], true)."</div>";

if (!empty($posterid))
$alinke[] = "<a class=\"alink\" title=\"".$tracker_lang['messages_on_forum']." ".$arr["num_topuser"]."\" href=\"forums.php?action=search_post&userid=".$posterid."\">".$tracker_lang['search_message']."</a>";

if ($CURUSER && $CURUSER["id"] <> $posterid && !empty($posterid))
$alinke[] = "<a class=\"alink\" href='message.php?receiver=".$posterid."&action=sendmessage'>".$tracker_lang['sendmessage']."</a>";

if ($CURUSER && !empty($posterid) && $CURUSER["forum_com"]=="0000-00-00 00:00:00" && $lock_ed == 'no')
$alinke[] = "<a class=\"alink\" href='forums.php?action=quotepost&topicid=".$topicid."&postid=".$postid."'>".$tracker_lang['quote_message']."</a>";

if (get_user_class() >= UC_MODERATOR && !empty($arr["ip"]))
$alinke[] = "<a class=\"alink\" title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$arr["ip"]."\">".$arr["ip"]."</a>";

if (($CURUSER["forum_com"] == "0000-00-00 00:00:00" && $CURUSER["id"] == $posterid && $lock_ed == 'no') || get_user_class() >= UC_MODERATOR)
$alinke[] = "<a class=\"alink\" href='forums.php?action=editpost&postid=".$postid."'>".$tracker_lang['edit']."</a>";

if (get_user_class() >= UC_MODERATOR || ($CURUSER["forum_com"] == "0000-00-00 00:00:00" && $CURUSER["id"] == $posterid && $lock_ed == 'no'))
$alinke[] = "<a class=\"alink\" href='forums.php?action=deletepost&postid=".$postid."'>".$tracker_lang['delete']."</a>";

if (get_user_class() > UC_MODERATOR)
$alinke[] = "<a class=\"alink\" title=\"".$tracker_lang['delete_alltop_pos']."\" href=\"forums.php?action=banned&userid=".$posterid."&postid=".$postid."\">".$tracker_lang['spam']."</a>";

if (count($alinke))
echo "<div align=\"right\">".implode(", ", $alinke)."</div>";

echo "</td></tr>";


echo "</table>";


++$num;
}

echo "</td></tr></table>";




$namhtml = strip_tags($subject);

if (!empty($namhtml)){
echo "<script type=\"text/javascript\">function highlight(field) {field.focus();field.select();}</script>";
echo "<table style=\"margin-top: 2px;\" class=\"main\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_link'].'\' onclick=\'highlight(this)\' value=\''.$namhtml.': '.$DEFAULTBASEURL.'/forums.php?action=viewpost&id='.$postid.'\'>';
echo "</td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_html'].'\' onclick=\'highlight(this)\' value=\'<a href="'.$DEFAULTBASEURL.'/forums.php?action=viewpost&id='.$postid.'" target="_blank">'.$namhtml.'</a>\'>';
echo "</td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_forum'].'\' onclick=\'highlight(this)\' value=\'[url='.$DEFAULTBASEURL.'/forums.php?action=viewpost&id='.$postid.']'.$namhtml.'[/url]\'>';
echo "</td></tr>";

echo "</table>";
}





echo "<br />";

insert_quick_jump_menu($forumid, $CURUSER);


stdfoot();
die;
}

exit();
break;


default:
std_view();
break;
}


function std_view() {

global $Forum_Config, $CURUSER, $SITENAME, $DEFAULTBASEURL, $tracker_lang;

$forum_pic_url=$DEFAULTBASEURL."/pic/forumicons/";

$added = get_date_time();

if ($CURUSER && get_date_time(gmtime() - 60) >= $CURUSER["forum_access"]){
sql_query("UPDATE users SET forum_access = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($CURUSER["id"])."") or sqlerr(__FILE__, __LINE__);
unsql_cache("arrid_".$CURUSER["id"]);
}

stdhead($tracker_lang['forum']);


if (get_user_class() == UC_SYSOP){
echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";

echo "<tr><td class=\"a\" align=\"center\"><a class=\"altlink_white\" href=\"forummanage.php\">".$tracker_lang['admining_category']." / ".$tracker_lang['creating_category']."</a></td></tr>";

echo "</table>";
}


fpanelka ();


echo "<table cellspacing=\"0\" cellpadding=\"0\" class=\"forums\" width=\"100%\">";


echo "<tr>
<td class=\"colhead\" align=\"center\">".$tracker_lang['status']."</td>
<td class=\"colhead\">".$tracker_lang['category']."</td>
</tr>";

$da=0;


$cached = sql_query("SELECT f.sort, f.id, f.name, f.description, f.minclassread, f.minclasswrite, f.minclasscreate, (SELECT COUNT(*) FROM topics WHERE forumid=f.id) AS numtopics, (SELECT COUNT(*) FROM posts WHERE forumid=f.id) AS numposts
FROM forums AS f ORDER BY f.sort, f.name", $cache = array("type" => "disk", "file" => "forums_cat", "time" => 60*60)) or sqlerr(__FILE__, __LINE__);

$caty = $arsort = array();
while ($for = mysql_fetch_assoc_($cached)){
$caty[$for['id']] = $for;
$arsort[] = $for['id'];
}

//print_R($caty);

$cache = sql_query("SELECT p.added, p.topicid AS topicidi, p.userid, u.username, u.class, t.subject, t.forumid, top.lastpost, top.locked, top.lastdate
FROM topics AS t

LEFT JOIN topics AS top ON top.lastdate = (SELECT MAX(lastdate) FROM topics WHERE forumid=t.forumid AND visible='yes') AND top.visible='yes'
LEFT JOIN posts AS p ON p.id = top.lastpost
LEFT JOIN users u ON p.userid = u.id

WHERE p.topicid = t.id AND t.visible = 'yes' ORDER BY FIELD (t.forumid, ".implode(',', $arsort).")", $cache = array("type" => "disk", "file" => "forums.main", "time" => 60*60)) or sqlerr(__FILE__, __LINE__);

/// ORDER BY f.sort, f.name
while ($forums_arr = mysql_fetch_assoc_($cache)){


$topiccount = number_format($caty[$forums_arr['forumid']]['numtopics']);
$postcount = number_format($caty[$forums_arr['forumid']]['numposts']);
$forumid = $forums_arr["forumid"];
$forumname = htmlspecialchars($caty[$forums_arr['forumid']]['name']);
$forumdescription = trim($caty[$forums_arr['forumid']]['description']);


if (empty($topiccount)) $topiccount = $tracker_lang['no'];
if (empty($postcount)) $postcount = $tracker_lang['no'];

if ($forums_arr["lastpost"]){

$lastpostid = $forums_arr["lastpost"];
$lasttopicid= $forums_arr["topicidi"];
$lastposterid = $forums_arr["userid"];
$lastpostdate = normaltime(($forums_arr["added"]),true);
$lastposter = htmlspecialchars($forums_arr['username']);
$lasttopic = format_comment_light($forums_arr['subject']);

///////////////////// ��������
if ($forums_arr["userid"]==0 && !$forums_arr['username'])
$view_user = "<font color=gray>[System]</font>";
elseif ($forums_arr["userid"]<>0 && !$forums_arr['username'])
$view_user = "<i><b>id</b>: ".$forums_arr["userid"]."</i>";
else
$view_user = "<a href=\"userdetails.php?id=$lastposterid\"><b>".get_user_class_color($forums_arr['class'], $lastposter)."</b></a> <a href=\"forums.php?action=search_post&userid=$lastposterid\"><img title=\"".$tracker_lang['search_user_msg']."\" src=\"".$DEFAULTBASEURL."/pic/pm.gif\" /></a>";

}


if ($da%2==1) $class="a";
else $class="b";

if ((get_user_class() >= $caty[$forums_arr['forumid']]['minclassread'] && $CURUSER) || ($caty[$forums_arr['forumid']]['minclassread'] == "0" && !$CURUSER)){

if (!$CURUSER){

if ($forums_arr['added'] > get_date_time(gmtime() - 86400)) $new=true;
else $new=false;

} else {

/*
if ($CURUSER){
$r = sql_query("SELECT lastpostread FROM readposts WHERE userid = ".sqlesc($CURUSER['id'])." AND topicid = ".sqlesc($lasttopicid)) or sqlerr(__FILE__, __LINE__);
$a = mysql_fetch_row($r);
$npostcheck = ($forums_arr['added'] > get_date_time(gmtime() - $Forum_Config["readpost_expiry"])) ? (!$a OR $lastpostid > $a[0]) : 0;
}
*/

}

$topicpic = ($forums_arr['locked']=="yes" ? ($new ? "lockednew" : "locked") : ($new ? "unlockednew" : "unlocked"));


echo "<tr>
<td class=\"".$class."\" align=\"center\"><img title=\"".$tracker_lang['status']."\" src=\"{$forum_pic_url}{$topicpic}.gif\" /></td>
<td class=\"".$class."\">
<div><a title=\"".$forumdescription."\" class=\"alink\" href=\"forums.php?action=viewforum&forumid=$forumid\"><b>$forumname</b></a> <span class=\"smallfont\">(".$tracker_lang['topics_many']." <b>$topiccount</b>, ".$tracker_lang['message_many']." <b>$postcount</b>)</span></div>
<small>".$forumdescription."</small>

".(empty($forums_arr["lastpost"]) ? "
<div class=\"smallfont\">".$tracker_lang['no_messages']."</div>
":"
<div class=\"smallfont\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$tracker_lang['subscribe_last_comment']." &#8594; <a class=\"alink\" title=\"".$tracker_lang['go_to']."\" href=\"forums.php?action=viewtopic&topicid=$lasttopicid&page=last#$lastpostid\"><strong>$lasttopic</strong></a> $view_user <span class=\"time\">".$lastpostdate."</span></div>
")."
</td>
</tr>";

++$da;

unset($view_add,$forumname,$postcount,$topiccount,$forumname);

}

}


echo "</table><br />";



/// ����������
if ($CURUSER && !empty($num_p_f)){

$num_post1=sql_query("SELECT COUNT(*) AS my_user FROM posts WHERE userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
$num_p1=mysql_fetch_assoc($num_post1);

$num_post=sql_query("SELECT COUNT(*) AS cou, (SELECT COUNT(*) FROM posts WHERE editedby > '0000-00-00 00:00:00') AS mod_cou FROM posts", $cache = array("type" => "disk", "file" => "forums.cou_mod_cou", "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);
$num_p=mysql_fetch_assoc_($num_post);

$num_post2=sql_query("SELECT COUNT(*) AS cou_user FROM posts GROUP BY userid", $cache = array("type" => "disk", "file" => "forums.cou_user", "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);
$num_p2=mysql_fetch_assoc_($num_post2);


$num_p_f=$num_p["cou"];
$num_p_modded=$num_p["mod_cou"];
$num_p_u=$num_p2["cou_user"];
$num_p_m=$num_p1["my_user"];
$num_cat_id=$num_cat_sql["name"];

//// �������� ��� ����� ����
$time_user=sql_timestamp_to_unix_timestamp($CURUSER["added"]);
$time_now = time();
if ($time_now>=$time_user)
$diff = $time_now-$time_user;
else
$diff = $time_user-$time_now;

if($diff>=86400){
$day = floor($diff/86400);
} else $day=1;

//// �������� ��� ����� ����
$size_to_day=@number_format((1 - $num_p_m/$day),2);
$size_to_all=@number_format(100*($num_p_m/$num_p_f),2);


echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";

echo "<tr><td class=\"b\">

<fieldset class=\"fieldset\">
<legend>".$tracker_lang['statistic']."</legend>
<b>�</b> ����� ����� ��������� �� ������: <b>".$num_p_m."</b> | <b>".$size_to_day."</b> ��������� � ����.<br/>
<b>�</b> � ��� <b>".$size_to_all."%</b> �� ���� ��������� �� ������.<br/><br/>
<b>� �</b> ".$tracker_lang['messages_on_forum'].": <b>".$num_p_f."</b> (����������������: <b>".$num_p_modded."</b>)<br />
<b>� �</b> ��������� ��������������: <b>".$num_p_u."</b><br/>
</fieldset>

</td></tr>";

echo "</table><br />";
}



if ($CURUSER && empty($num_p_f)){
echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";

echo "<tr><td class=\"b\">

<fieldset class=\"fieldset\">
<legend>".$tracker_lang['warning']."</legend>
".sprintf($tracker_lang['forum_selcat'], "<strong>".$tracker_lang['fast_redirect']."</strong>", ".::: <strong>".$tracker_lang['creating_topic']."</strong> :::.")."</fieldset>

</td></tr>";

echo "</table><br />";
}



insert_quick_jump_menu($forumid);

stdfoot();

die;
}
?>