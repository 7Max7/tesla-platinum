<?
require("include/bittorrent.php");
dbconn();
loggedinorreturn();
parked();


$allowed_types = array(
"application/vnd.ms-xpsdocument" => "xps",
"video/x-msvideo" => "avi",
"video/x-ms-wvx" => "wvx",
"video/x-ms-wmx" => "wmx",
"video/x-ms-wmv" => "wmv",
"video/x-ms-wm" => "wm",
"video/x-ms-asf" => "asx",
"video/3gpp" => "3gp",
"video/3gpp2" => "3g2",
"text/x-vcalendar" => "vcs",
"text/x-java" => "java",
"text/csv" => "csv",
"text/css" => "css",
"application/x-shockwave-flash" => "swf",
"image/x-photoshop" => "psd",
"image/vnd.djvu" => "djvu",
"image/tiff" => "tiff",
"audio/x-mpegurl" => "m3u",
"application/x-flac" => "flac",
"application/x-7z-compressed" => "7zip",
"application/xml" => "xml",
"application/rtf" => "rtf",
"application/pdf" => "pdf",
"application/xhtml+xml" => "xhtml",
"application/rss+xml" => "rss",
"application/x-bittorrent" => "torrent",
"application/msword" => "doc",
"text/plain" => "txt",
"audio/mpeg" => "mp3",
"image/jpeg" => "jpg",
"image/png" => "png",
"image/gif" => "gif",
"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => "xlsx",
"application/vnd.ms-excel" => "xls",
"application/vnd.android.package-archive" => "apk",
"application/x-msi" => "msi",
"text/x-vcard" => "vcf",
"text/html" => "html",
"text/xml" => "xml",
"text/x-sql" => "sql",
"audio/x-wav" => "wav",
"video/mp4" => "mp4",
"video/ogg" => "ogg",
"application/powerpoint" => "doc",
"video/quicktime" => "mov",
"audio/x-realaudio" => "realaudio",
"audio/x-aiff" => "aiff",
"application/x-tar" => "tar",
"application/x-gtar" => "gtar",
"application/zip" => "zip",
"application/x-zip" => "x-zip",
"application/x-gzip" => "gzip",
"application/x-bzip2" => "bzip2",
"application/x-zip-compressed" => "zip (compress)",
"application/x-rar-compressed" => "rar (compress)",
);


if (isset($_GET["download"])){

$fileid = (!empty($_GET["download"]) ? intval($_GET["download"]):0);

if (empty($fileid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$res = sql_query("SELECT * FROM attachments WHERE id = ".sqlesc($fileid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_assoc($res);

if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['tfile_nofile']);

if ($arr["amd"] == "yes" && get_user_class() <= UC_MODERATOR && $arr["uploadby"] <> $CURUSER["id"])
stderr($tracker_lang['privat_torrent'], $tracker_lang['access_denied']);

$fn2 = ROOT_PATH."/torrents/tfiles/".$arr["filename"];

if (!is_file($fn2) || !is_readable($fn2)){
sql_query("DELETE FROM attachments WHERE id = ".sqlesc($fileid));
stderr($tracker_lang['error'], $tracker_lang['tfile_nofile']);
}

mysql_query("UPDATE attachments SET hits = hits+1 WHERE id = ".sqlesc($fileid)) or sqlerr(__FILE__,__LINE__);

header ("Expires: Tue, 1 Jan 1980 00:00:00 GMT");
header ("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header ("Cache-Control: no-store, no-cache, must-revalidate");
header ("Cache-Control: post-check=0, pre-check=0", false);
header ("Content-Length: ".filesize(ROOT_PATH."/torrents/tfiles/".$arr["filename"]));
header ("Pragma: no-cache");
header ("Accept-Ranges: bytes");
header ("Connection: close");
header ("Content-Transfer-Encoding: binary");
header ("Content-Disposition: attachment; filename=\"".$arr["title"]."\"");
header ("Content-Type: ".$arr["type"]);

ob_implicit_flush(true);

readfile(ROOT_PATH."/torrents/tfiles/".$arr["filename"]);

die;
}





if (isset($_GET["add"]) && $_GET["add"] == "clean_all" && get_user_class() == UC_SYSOP){

accessadministration();

$dh = opendir(ROOT_PATH."/torrents/tfiles/");
while ($file = readdir($dh)) : $file_orig = $file;
if (preg_match('/^(.+)\.$/si', $file, $matches)) $file = $matches[1];

if (!stristr($file,'.htaccess')) @unlink(ROOT_PATH."/torrents/tfiles/".$file);

endwhile;
closedir($dh);

sql_query("TRUNCATE TABLE attachments");

write_log($CURUSER["username"]." �������� ���� ��������.", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "tfiles");

header("Location: tfiles.php");
die;
}


/// ������ �������� ������������� ///
$rs = sql_query("SELECT sum(size) AS my_size FROM attachments WHERE uploadby = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);
$as = mysql_fetch_assoc($rs);
$my_size = $as["my_size"];
/// ������ �������� ������������� ///

$limit_size = (!empty($Tfiles_Config[$CURUSER["class"]]) ? ($Tfiles_Config[$CURUSER["class"]]*1024*1024):0);


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET["add"]) && $_GET["add"] == "yes") {

$errors = array(
0 => "There is no error, the file uploaded with success",
1 => "The uploaded file exceeds the upload_max_filesize directive in php.ini",
2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
3 => "The uploaded file was only partially uploaded",
4 => "No file was uploaded",
6 => "Missing a temporary folder"
);

$file = $_FILES['file'];

if (isset($file['error']) && !empty($file['error']))
stderr($tracker_lang['error']." / <a href=\"tfiles.php\">".$tracker_lang['back_inlink']."</a>", $errors[$file['error']]);

if (!isset($file) || empty($file["size"]) || empty($file["name"]))
stderr($tracker_lang['error']." / <a href=\"tfiles.php\">".$tracker_lang['back_inlink']."</a>", sprintf($tracker_lang['tfile_limit_of'], (($limit_size-$my_size) > 0 ? mksize($limit_size-$my_size):mksize(0)), mksize($limit_size)));

if (isset($file) && $file["size"] >= file_upload_max_size())
stderr($tracker_lang['error']." / <a href=\"tfiles.php\">".$tracker_lang['back_inlink']."</a>", $tracker_lang['invalid_id_value']." - ".$tracker_lang['size'].": ".mksize($file["size"])."<br />".$tracker_lang['max_file_size'].": ".mksize(file_upload_max_size()));

$chto = array("'","'","\"","<",">","&","$","#","@","%","^","&","?","*","/",":","|");

$title = $filenameee = trim(strip_tags(str_replace($chto, "", $file["name"])));

if (strlen($title) > 50)
$title = preg_replace("/\[((\s|.)+?)\]/is", "", preg_replace("/\(((\s|.)+?)\)/is", "", $title)); /// ����� �� ������ [] � ��

$filenameee = md5($filenameee);

if (file_exists(ROOT_PATH."/torrents/tfiles/".$filenam1)){
$filenameee = md5($filenameee.time());
$title = $title;
}

$type = $file["type"];


if (!array_key_exists($type, $allowed_types) && !in_array(end(explode(".", end(explode("/", $title)))), $allowed_types))
stderr($tracker_lang['error']." / <a href=\"tfiles.php\">".$tracker_lang['back_inlink']."</a>", $tracker_lang['tfile_notype'].": ".$type."<br />".$tracker_lang['tfile_type'].": ".implode(", ", $allowed_types));



if (!empty($limit_size) && ($my_size+$file["size"]) > $limit_size)
stderr($tracker_lang['error']." / <a href=\"tfiles.php\">".$tracker_lang['back_inlink']."</a>", sprintf($tracker_lang['tfile_limit_of'], (($limit_size-$my_size) > 0 ? mksize($limit_size-$my_size):mksize(0)), mksize($limit_size)));

$file_contents = file_get_contents($file["tmp_name"]);
$functions_to_shell = array ("include", "file", "fwrite", "script", "body", "java", "fopen", "fread", "require", "exec", "system", "passthru", "eval", "copy");

$true = false;
foreach ($functions_to_shell AS $funct){

if (preg_match("/".$funct."+(\\s||)+[(]/", $file_contents)) {

if (!stristr($CURUSER["usercomment"], "������� ������ shell") && $CURUSER["monitoring"] <> "yes")
sql_query("UPDATE users SET usercomment = CONCAT_WS('', ".sqlesc(get_date_time()." ������� ������ shell (-".$funct."-) � tfiles.\n").", usercomment), monitoring = 'yes' WHERE id = ".$CURUSER["id"]) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$CURUSER["id"]);

write_log($CURUSER["username"]." ������� ������ shell � tfiles (".$file["size"].").", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "error");

$true = true;
break;
}

}

if ($true == true){
header("Location: tfiles.php");
die;
}

if ($type == "image/gif" || $type == "image/jpeg" || $type == "image/jpg" || $type == "image/png")
validimage($file["tmp_name"], "tfiles");

if (!move_uploaded_file($file["tmp_name"], ROOT_PATH."/torrents/tfiles/".$filenameee))
stderr($tracker_lang['error_data']." / <a href=\"tfiles.php\">".$tracker_lang['back_inlink']."</a>", $tracker_lang['dox_cannot_move']);

sql_query("INSERT INTO attachments (title, filename, added, uploadby, size, type, amd) VALUES (".sqlesc(htmlspecialchars_uni($title)).", ".sqlesc($filenameee).", ".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($file["size"]).", ".sqlesc($type).", ".sqlesc(empty($_POST["amd"]) ? "no":"yes").")")or sqlerr(__FILE__,__LINE__);

write_log($CURUSER["username"]." ����� ".$title." (".$file["type"].")", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "tfiles");

if (isset($_POST["my"]))
header("Location: tfiles.php?userid=".$CURUSER["id"]);
else
header("Location: tfiles.php");
die;
}

if (isset($_POST["remove"]) && is_array($_POST["remove"])) {

$r = sql_query("SELECT * FROM attachments WHERE id IN (".implode(",", array_map('intval', $_POST['remove'])).")".(get_user_class() > UC_MODERATOR ? "":" AND uploadby = ".sqlesc($CURUSER["id"]))) or sqlerr(__FILE__,__LINE__);

$numi = 0;
if (mysql_num_rows($r) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_data_now']);

while ($a = mysql_fetch_assoc($r)){

if ($a["uploadby"] == $CURUSER["id"])
++$numi;

if (!@unlink(ROOT_PATH."/torrents/tfiles/".$a["filename"]) && @file_exists(ROOT_PATH."/torrents/tfiles/".$a["filename"]))
stderr($tracker_lang['error_data']." / <a href=\"tfiles.php\">".$tracker_lang['back_inlink']."</a>", sprintf($tracker_lang['dox_cannot_delete'], $a['filename']));

sql_query("DELETE FROM attachments WHERE id = ".sqlesc($a["id"])) or sqlerr(__FILE__, __LINE__);

write_log($CURUSER["username"]." ������ ".$a["title"]." (".$a["type"].")", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "tfiles");

}

if ($numi == mysql_num_rows($r) && !isset($_POST["all"]))
header("Location: tfiles.php?userid=".$CURUSER["id"]);
else
header("Location: tfiles.php");

die;
}


$userid = (isset($_GET["userid"]) ? intval($_GET["userid"]): 0);

$array_mod = array();
if (get_user_class() <= UC_MODERATOR && $userid <> $CURUSER["id"])
$array_mod[] = "attachments.amd = 'no'";
if (!empty($userid))
$array_mod[] = "attachments.uploadby = ".sqlesc($userid);

if (count($array_mod))
$count = get_row_count("attachments", "WHERE ".implode(" AND ", $array_mod));
else
$count = get_row_count("attachments");

list ($pagertop, $pagerbottom, $limit) = pager(50, $count, "tfiles.php?".(!empty($userid) ? "userid=".$userid."&":""));

if (!empty($count))
$my_count = get_row_count("attachments", "WHERE uploadby = ".sqlesc($CURUSER["id"]));

stdhead($tracker_lang['dox']);

sort($allowed_types);

echo "<form enctype=\"multipart/form-data\" method=\"post\" action=\"tfiles.php?add=yes\"><table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";

echo "<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['dox_upload_file']."</td></tr>";

$true = (!empty($my_size) ? $my_size:file_upload_max_size());
if (!empty($limit_size)){

$end = explode("/", $tracker_lang['rem_space_busy']);

echo "<tr><td align=\"left\" class=\"b\">".trim($end[1])." / ".trim($end[0])." / ".$tracker_lang['total']."</td><td align=\"left\" class=\"b\">".mksize($my_size)." / ".(($limit_size-$my_size) > 0 ? mksize($limit_size-$my_size):mksize(0))." / ".mksize($limit_size)."</td></tr>";

$true = (($limit_size-$my_size) > 0 ? ($limit_size-$my_size):0);

}

echo "<tr><td class=\"a\" colspan=\"2\"><input type=\"file\" ".(!empty($true) ? "":"disabled")." name=\"file\" size=\"100\"> <b>".$tracker_lang['max_file_size']."</b>: ".mksize(file_upload_max_size())."</td></tr>";
echo "<tr><td class=\"b\" colspan=\"2\"><b>".$tracker_lang['tfile_type']."</b> (".(count($allowed_types))."): ".implode(", ", $allowed_types)."</td></tr>";


echo "<tr><td class=\"b\" colspan=\"2\"><label><input type=\"checkbox\" name=\"amd\" value=\"yes\" /> ".$tracker_lang['privat_torrent']." (".$tracker_lang['tfile_private'].")</label></td></tr>";

echo "<tr><td colspan=\"2\" align=\"left\" class=\"a\">".(!empty($userid) ? "<input type=\"hidden\" name=\"my\" value=\"true\" /> ":"")."<input ".(!empty($true) ? "":"disabled")." type=\"submit\" title=\"".$tracker_lang['dox_upload_file']."\" value=\"".$tracker_lang['dox_upload_file']."\" class=\"btn\" /></td></tr>";

echo "</table></form>";

if (!empty($userid))
echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" border=\"0\"><tr><td align=\"center\" class=\"b\"><strong>".sprintf($tracker_lang['tags_backmain'], "tfiles.php")."</strong></td></tr></table>";



$res = sql_query("SELECT attachments.*, u.class, u.username
FROM attachments
LEFT JOIN users AS u ON attachments.uploadby = u.id
".(count($array_mod) ? "WHERE ".implode(" AND ", $array_mod):"")."
ORDER BY attachments.id DESC ".$limit) or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res) == 0)
echo "<table border=\"0\" cellspacing=\"0\" width=\"100%\" cellpadding=\"5\"><td class=\"b\" align=\"center\">".$tracker_lang['no_data_now']."</td></tr></table><br />";

else {

if (!empty($my_count) || get_user_class() > UC_MODERATOR){

?>
<script language="Javascript" type="text/javascript">
jQuery(document).ready(function() {

jQuery("#remove").click(function () {
if (!jQuery("#remove").is(":checked"))
jQuery(".removeo").removeAttr("checked");
else
jQuery(".removeo").attr("checked","checked");
});

}); 
</script>
<?

echo "<form method=\"post\" action=\"tfiles.php\">";
}


echo "<table border=\"0\" cellspacing=\"0\" width=\"100%\" cellpadding=\"5\">";

echo "<tr><td align=\"center\" colspan=\"5\">".$pagertop."</td></tr>";

echo "<tr>
<td width=\"3%\" align=\"center\" class=\"colhead\">#</td>
<td width=\"50%\" class=\"colhead\" align=\"left\">".$tracker_lang['name']." / ".$tracker_lang['size']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['added']." / ".$tracker_lang['news_poster']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['details_leeched']."</td>
<td width=\"20%\" align=\"center\" class=\"colhead\">".$tracker_lang['type']." ".(!empty($my_count) || get_user_class() > UC_MODERATOR ? "/ <label>".$tracker_lang['action']." <input type=\"checkbox\" id=\"remove\" /></label>":"")."</td>
</tr>";

$num = 0;
while ($arr = mysql_fetch_assoc($res)) {

if ($num%2 == 0){
$cl1 = "class=\"b\"";
$cl2 = "class=\"a\"";
} else {
$cl2 = "class=\"b\"";
$cl1 = "class=\"a\"";
}

echo "<tr>";

echo "<td ".$cl1." align=\"center\">".$arr['id']."</td>";

echo "<td ".$cl2." align=\"left\">".($arr['amd'] == "yes" ? "<strong>".$tracker_lang['privat_torrent']."</strong>: ":"")."<a title=\"".$tracker_lang['download']."\" href=\"tfiles.php?download=".$arr["id"]."&name=".urlencode($arr["title"])."\"><b>".htmlspecialchars($arr["title"])."</b></a> (".mksize($arr['size']).")".(get_user_class() > UC_MODERATOR ? "<br />".$arr['filename']."":"")."</td>";

echo "<td ".$cl1." align=\"center\">".$arr["added"]."<br />".(!empty($arr["username"]) ? "<a href=\"userdetails.php?id=".$arr["uploadby"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a>":$tracker_lang['anonymous'])." (<a title=\"".$tracker_lang['open_list']."\" href=\"tfiles.php?userid=".$arr["uploadby"]."\">".$tracker_lang['files']."</a>)</td>";

echo "<td ".$cl2." align=\"center\">".number_format($arr['hits'])." ".$tracker_lang['time_s']."</td>";

echo "<td ".$cl1." align=\"left\">".$arr['type']."<br />".(get_user_class() > UC_MODERATOR || $arr["uploadby"] == $CURUSER["id"] ? "<div align=\"right\"><input title=\"".$tracker_lang['delete_user']."\" type=\"checkbox\" class=\"removeo\" name=\"remove[]\" value=\"".$arr["id"]."\" /></div>":"")."</td>";

echo "</tr>";

++$num;
}

if (!empty($my_count) || get_user_class() > UC_MODERATOR)
echo "<tr><td align=\"right\" colspan=\"5\">".(empty($userid) ? "<input type=\"hidden\" name=\"all\" value=\"true\" /> ":"")."<input type=\"submit\" name=\"submit\" class=\"btn\" value=\"".$tracker_lang['b_action']."\" /></td></tr>";

if (get_user_class() == UC_SYSOP)
echo "<tr><td align=\"center\" class=\"a\" colspan=\"5\"><a title=\"".$tracker_lang['tfile_trunall']."\" href=\"tfiles.php?add=clean_all\"><b>".$tracker_lang['tfile_trunall']."</b></a></td></tr>";

echo "<tr><td align=\"center\" colspan=\"5\">".$pagerbottom."</td></tr>";

echo "</table>";

if (!empty($my_count) || get_user_class() > UC_MODERATOR)
echo "</form>";

}

stdfoot();

?>