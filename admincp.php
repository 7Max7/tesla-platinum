<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn(); 

if (get_user_class() < UC_SYSOP || get_user_class() <> UC_SYSOP){
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

accessadministration();


if (ini_get('register_globals') == '1' || strtolower(ini_get('register_globals')) == 'on')
die ("��������� regiser_globals � php.ini/.htaccess");
elseif (ini_get('short_open_tag') == 'no' || ini_get('short_open_tag') == false)
die ("�������� short_open_tag � php.ini/.htaccess (����������� ����������)");
elseif (!function_exists('imagecreatetruecolor') || !extension_loaded('gd'))
die ("���������� ���������� php ��� ��������� gd.so");
elseif (!extension_loaded('zip'))
die ("���������� ���������� php ��� ��������� zip.so");
elseif (!extension_loaded('mbstring'))
die ("���������� ���������� php ��� ��������� mbstring.so");




stdhead($tracker_lang['admincp']);


if (version_compare(PHP_VERSION, '5.4.0', '>='))
stdmsg("������ ����������!", "������ php �� ������������� ������������� ���������� ������. ����������, ������������ ���������� ������ 5.3.*. ������ �����������: ".PHP_VERSION.":<br/>
��������� ��������: ������ ��������� ( � ������� ����������, ���������� ������� � �������� (����� ��������� � Windows-1251 �� UTF-8), � ��� ����� ).", 'error');


$debug_time = true; /// or false

$op = (!isset($_GET['op'])) ? "Main" : htmlentities($_GET['op']);

if (get_magic_quotes_gpc()) {
if (!empty($_GET)) $_GET = strip_magic_quotes($_GET);
if (!empty($_POST)) $_POST = strip_magic_quotes($_POST);
if (!empty($_COOKIE)) $_COOKIE = strip_magic_quotes($_COOKIE);
}

foreach ($_GET as $key => $value)
$GLOBALS[$key] = $value;
foreach ($_POST as $key => $value)
$GLOBALS[$key] = $value;

global $Cache_Config;

$phpfiles_on_tesla = array('box-jquery.php', 'memall.php', 'box.php', '0im.php', 'sitemap_parts_3.php', '0nline.php', '403.php', '404.php', '500.php', 'adduser.php', 'adminbookmark.php', 'admincp.php', 'alltags.php', 'announce.php', 'announce_net.php', 'antichiters.php', 'autocomplete.php', 'bannedemails.php', 'bans.php', 'block-com_jquery.php', 'block-details_ajax.php', 'block-details_jquery.php', 'block-forums_jquery.php', 'block-last_files_jquery.php', 'block-myhumor_jquery.php', 'block-online_details_jquery.php', 'block-online_jquery.php', 'block-user_jquery.php', 'block_top_jquery.php', 'bookmark.php', 'bookmarks.php', 'browse.php', 'browselight.php', 'b_actions.php', 'category.php', 'dump3r.php', 'cheaters.php', 'check.php', 'checkcomm.php', 'checkdelete.php', 'check_signup.php', 'citydd.php', 'comment.php', 'commentoff.php', 'comments_last.php', 'confirm.php', 'creating_adminpas.php', 'creating_block.php', 'demo.php', 'details.php', 'detailsoff.php', 'docleanup.php', 'downcheck.php', 'download.php', 'block-category_line.php', 'edit.php', 'faq.php', 'editpersons.php', 'errors.php', 'faq_tesla.php', 'fonts.php', 'formats.php', 'forummanage.php', 'forums.php', 'forums_poll.core.php', 'friends.php', 'friensblockadd.php', 'getrss.php', 'gettorrentdetails.php', 'get_free.php', 'groups.php', 'groupsmess.php', 'hiderating.php', 'browseday.php', 'humor.php', 'humorall.php', 'index.php', 'invite.php', 'ipcheck.php', 'karma.php', 'license.php', 'likeme.php', 'log.php', 'login.php', 'block_3view_jquery.php', 'logout.php', 'makepoll.php', 'maxlogin.php', 'md5.php', 'mematch.php', 'message.php', 'modtask.php', 'monitoring.php', 'msg.php', 'my.php', 'mybonus.php',  'mysimpaty.php', 'mysql_stats.php', 'mytorrents.php', 'news.php', 'newsarchive.php', 'newscomment.php', 'newsoverview.php', 'ok.php', 'online.php', 'parser_k.php','parser_toruconfig.php', 'parser_toru.php', 'parser_d.php', 'parser_linkadd.php', 'parser_nnm.php', 'parser_pirat.php', 'parser_rutorg.php', 'parser_t.php', 'persons.php', 'poll.core.php', 'pollcomment.php', 'polloverview.php', 'polls.php', 'premod.php', 'proxy.php', 'random.php', 'rating.php', 'recover.php', 'redir.php', 'redirect.php', 'referer.php', 'remote_gets.php', 'repair_tags.php', 'report.php', 'restoreclass.php', 'rss.php', 'rule.php', 'rules.php', 'scrape.php', 'secretpage.php', 'seeders.php', 'setclass.php', 'shoutbox.php', 'showtags.php', 'signup.php', 'simpaty.php', 'sitemap.php', 'sitemap_parts.php', 'sitemap_parts_2.php', 'sitemap_parts_4.php', 'sitemap_parts_5.php','smilies.php', 'staff.php', 'staffmess.php', 'statistics.php', 'stats.php', 'subnet.php', 'support.php', 'tags.php', 'takedelbookmark.php', 'takeedit.php', 'takeeditoff.php', 'takeinvite.php', 'takelogin.php', 'takeprofedit.php', 'takerate.php', 'takereseed.php', 'takesignup.php', 'takeupload.php', 'testip.php', 'testmail.php', 'tfiles.php', 'thanks.php', 'thumbnail.php', 'stop_dfiles.php', 'top.php', 'topten.php', 'toptracker.php', 'tracker-chat.php', 'unco.php', 'upload.php', 'uploadapp.php', 'uploaders.php', 'uploadnext.php', 'uploadoff.php', 'useragent.php', 'useragreement.php', 'userdetails.php', 'userhistory.php', 'users.php', 'usersearch.php', 'ustatistics.php', 'viewreport.php', 'votes.php', 'warned.php', 'zupload.php', 'words.php');
//////////////


////////// ��������
function end_chmod($dir, $chm) {

global $tracker_lang;

if (file_exists($dir)) {
$pdir = decoct(fileperms($dir));
$per = substr($pdir, 2);
if ($per <> $chm)
return $tracker_lang['chmod_ins'].": <font color=\"red\" title=\"".$per."\">".$chm."</font>.";
else
return "".$tracker_lang['chmod'].": <font color=\"green\">".$chm."</font>.";
} else
return "".$tracker_lang['chmod'].": N/A";
}

function end_chmod_($dir, $chm) {

global $tracker_lang;

if (file_exists($dir)) {
$pdir = decoct(fileperms($dir));
$per = substr($pdir, 3);
if ($per <> $chm)
return $tracker_lang['chmod_ins'].": <font color=\"red\" title=\"".$per."\">".$chm."</font>.";
else
return false;
} else
return false;

}

function file_exist_($dir) {

global $tracker_lang;

if (file_exists(ROOT_PATH.$dir.".htaccess")) {
return $tracker_lang['file_exis'].": <font color=\"green\">.htaccess</font>.";
} else
return $tracker_lang['file_noexis'].": <font color=\"green\">.htaccess</font> (".$tracker_lang['need_back'].").";
}
/////////////////////


function theme_is_valid ($name){

$array_num = array();
if (!is_dir(ROOT_PATH."/themes/".$name)) $array_num[] = $name."/";
if (!is_dir(ROOT_PATH."/themes/".$name."/html/")) $array_num[] = $name."/html/";

if (!count($array_num) || count($array_num) == 1){
if (!file_exists(ROOT_PATH."/themes/".$name."/stdhead.php")) $array_num[] = $name."/stdhead.php";
if (!file_exists(ROOT_PATH."/themes/".$name."/stdheadchat.php")) $array_num[] = $name."/stdheadchat.php";
if (!file_exists(ROOT_PATH."/themes/".$name."/stdfoot.php")) $array_num[] = $name."/stdfoot.php";
if (!file_exists(ROOT_PATH."/themes/".$name."/stdfootchat.php")) $array_num[] = $name."/stdfootchat.php";
if (!file_exists(ROOT_PATH."/themes/".$name."/template.php")) $array_num[] = $name."/template.php";
}

return array("work" => (count($array_num) ? false:true), "error" => $array_num);

}

function main_menu($clear = false) {

global $tracker_lang;

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td class=\"a\" colspan=\"6\">".$tracker_lang['admincp']." (v:30.12.2012)</td></tr>";

BuildMenu("admincp.php?op=addtheme", $tracker_lang['addtheme'], "theme.png");
BuildMenu("admincp.php?op=BlocksAdmin", $tracker_lang['blocksadmin'], "block.png");
BuildMenu("admincp.php?op=iUsers", $tracker_lang['iusers'], "password.png");
BuildMenu("admincp.php?op=StatusDB", $tracker_lang['statusdb'], "db.png");
BuildMenu("docleanup.php", $tracker_lang['docleanup_act'], "db_process.png");
BuildMenu("category.php", $tracker_lang['category_tags'], "cat.png");
BuildMenu("backup/", $tracker_lang['dump3r'], "db_.png");
BuildMenu("admincp.php?op=DropCache", $tracker_lang['dropcache'], "db_up.png");
BuildMenu("admincp.php?op=CHmod", $tracker_lang['chmod_files'], "folder.png");

if (!empty($_GET) || !empty($_POST))
BuildMenu("admincp.php", $tracker_lang['admincp_back'], "faq.png");



/*
* 11.08.2018
* �������� � ������ �������� ����� �� ����������� ������.
* ����� �������� ������� �� ������������ ������ - �������� ���� ���, ��� ������� �����.
* */

/// ������� �� ��������� ������ - ������ ����������, ����� ��������� ��������////
if (!count($_GET) AND !empty($my_own_licence)){

$filecache = file_query("", $cache = array("type" => "disk", "file" => "admincp_news", "time" => 60*60*24*7, "action" => "get"));
if ($filecache <> false) {
$ctext = @unserialize(@base64_decode($filecache));
} else {
global $my_own_licence;

    $ctext = '';
// ������������� ����������
    $fp = fsockopen("614a0525082c.sn.mynetname.net", 8485, $errno, $errstr, 5);
    if ($fp) {

        $ctext = @file_get_contents('http://614a0525082c.sn.mynetname.net:8485/demo.php?textme&l='.base64_encode($my_own_licence), false, @stream_context_create(array($scheme => array('method' => 'GET','header' => 'User-Agent: Tesla (Tesla Tracker)/11082018', 'timeout' => 3))));
        @fclose($fp);
    }

    file_query(@base64_encode($ctext), $cache = array("type" => "disk", "file" => "admincp_news", "time" => 60*60*24*7, "action" => "set"));
    if (!empty($ctext))
    $ctext = @unserialize($ctext);


}



if (is_array($ctext) && count($ctext)){

echo "<tr><td align=\"left\" class=\"a\" width=\"100%\" colspan=\"6\">���� ��������� �� ��������� ������, ��� ������� - ��������� ������� � ������: (���������� ������ ��� � 7 ���)</td></tr>";

echo "<tr><td class=\"b\" width=\"100%\" colspan=\"6\">".format_comment(@implode(" ", $ctext))."</td></tr>";

}
}
/// ������� �� ��������� ������ - ������ ����������, ����� ��������� ��������////



echo "<tr><td align=\"center\" class=\"b\" width=\"100%\" colspan=\"6\"><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['chmod_help']."</td></tr>";

echo "</table>";
echo "<br />";
}

function BuildMenu($url, $title, $image = '') {

global $counter, $tracker_lang;;

$image_link = "/pic/admin/".$image;

echo "<td align=\"center\" class=\"b\" valign=\"top\" width=\"15%\" style=\"border: none;\"><a href=\"".$url."\" title=\"".$title."\">".($image != '' ? "<img src=\"".$image_link."\" border=\"0\" alt=\"".$title."\" title=\"".$title."\">" : "")."<br /><b>".$title."</b></a></td>";

if ($counter == 5) {
echo "</tr><tr>";
$counter = 0;
} else {
++$counter;
}

}

switch ($op) {

case "Main":

main_menu (true);


$df = @disk_free_space(ROOT_PATH);
$dt = @disk_total_space(ROOT_PATH);

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";

echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\">".$tracker_lang['about_server_plugins']."</td></tr>";

echo "<tr><td class=\"b\">".$tracker_lang['rem_space_busy']."</td><td align=\"right\" class=\"a\">".mksize($df)." / ".mksize($dt-$df)." (".mksize($dt).")</td></tr>";

$avgload = get_server_load();

if (strtolower(substr(PHP_OS, 0, 3)) != 'win')
$percent = $avgload * 4;
else
$percent = $avgload;


if (!empty($percent))
echo "<tr><td class=\"a\">".$tracker_lang['load_proces']."</td><td align=\"right\" class=\"b\">".$percent."%</td></tr>";

echo "<tr><td class=\"b\">".$tracker_lang['version_php']."</td><td align=\"right\" class=\"a\">".phpversion()."</td></tr>";

echo "<tr><td class=\"a\">".$tracker_lang['display_erview']."</td><td align=\"right\" class=\"b\">".(ini_get('display_errors')<>'Off' ? "<b>".$tracker_lang['no']."</b>":$tracker_lang['yes'])."</td></tr>";

if (ini_get('display_errors')<>'Off')
echo "<tr><td class=\"b\" colspan=\"2\" align=\"center\"><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['afte_inst'].":<br />@ini_set('display_errors', '<b>Off</b>'); /// ON - ".$tracker_lang['view_on']." <b>OFF</b> - ".$tracker_lang['view_off'].".</td></tr>";


if (function_exists("memcache_get_version")){
$view_mem = $tracker_lang['on_install'];
if ($Cache_Config["memcache_on"] == true && $memcache_server <> false)
$stat_memcache = memcache_get_stats($memcache_server);
}
else
$view_mem = $tracker_lang['on_deinstall'];


echo "<tr><td class=\"a\">Memcache</td><td align=\"right\" class=\"b\">".$view_mem."</td></tr>";

if (count($stat_memcache)){

$wor1 = array("pid","uptime","time","version","pointer_size","curr_items","total_items","curr_connections","total_connections","connection_structures","cmd_get","cmd_set","get_hits","get_misses","bytes_read","bytes_written","limit_maxbytes","bytes");

$wor2 = array($tracker_lang['memc_pid'], $tracker_lang['memc_uptime'], $tracker_lang['memc_time'], $tracker_lang['memc_version'], $tracker_lang['memc_pointer_size'], $tracker_lang['memc_curr_items'], $tracker_lang['memc_total_items'], $tracker_lang['memc_curr_conn'], $tracker_lang['memc_total_conn'], $tracker_lang['memc_conn'], $tracker_lang['memc_cmd_get'], $tracker_lang['memc_cmd_set'], $tracker_lang['memc_get_hits'], $tracker_lang['memc_get_misses'], $tracker_lang['memc_bytes_read'], $tracker_lang['memc_bytes_written'], $tracker_lang['memc_limit_maxbytes'], $tracker_lang['memc_bytes']);

$php_bite = array("bytes_read","bytes_read","bytes_written","limit_maxbytes","bytes");

foreach ($stat_memcache AS $id1 => $id2){

if ($num5%2 == 0) {
$cl1 = "class = \"a\"";
$cl2 = "class = \"b\"";
} else {
$cl2 = "class = \"a\"";
$cl1 = "class = \"b\"";
}

if ($id1 == "uptime" || $id1 == "pid")
$id2 = get_seed_time($id2);
elseif (in_array($id1, $php_bite))
$id2 = mksize($id2);
elseif ($id1 == "time")
$id2 = get_date_time($id2);
else number_format($id2);

$data_rus = str_replace($wor1, $wor2, $id1);

echo "<tr><td ".$cl2.">".$data_rus." aka <u>".$id1."</u></td><td align=\"right\" ".$cl1.">".$id2."</td></tr>";

++$num5;
}

echo "<tr><td class=\"b\" colspan=\"2\"></td></tr>";

}

echo "<tr><td class=\"b\">Xcache</td><td align=\"right\" class=\"a\">".(extension_loaded("XCache") ? $tracker_lang['on_install']:$tracker_lang['on_deinstall'])."</td></tr>";

echo "<tr><td class=\"b\" colspan=2></td></tr>";

echo "<tr><td class=\"b\">MYSQL</td><td align=\"right\" class=\"a\">".mysql_get_client_info()."</td></tr>";

$wor1 = array("Uptime","Threads","Questions","Slow queries","Opens","Flush tables","Open tables","Queries per second avg");

$wor2 = array($tracker_lang['mysql_uptime'], $tracker_lang['mysql_threads'], $tracker_lang['mysql_questions'], $tracker_lang['mysql_slow_queries'], $tracker_lang['mysql_opens'], $tracker_lang['mysql_flush_tables'], $tracker_lang['mysql_open_tables'], $tracker_lang['mysql_queries_avg'], );

$array_stat = explode("  ", mysql_stat());

foreach ($array_stat AS $stat){

if ($num4%2 == 0) {
$cl1 = "class = \"a\"";
$cl2 = "class = \"b\"";
} else {
$cl2 = "class = \"a\"";
$cl1 = "class = \"b\"";
}

list ($data1,$data2) = explode(":",$stat);

if ($data1 <> "Uptime" && $data1 <> "Queries per second avg")
$data2 = number_format($data2);
elseif ($data1 == "Uptime")
$data2 = get_seed_time($data2);

$data1 = str_replace($wor1, $wor2, $data1);

echo "<tr><td ".$cl1.">".$data1."</td><td align=\"right\" ".$cl2.">".$data2."</td></tr>";
++$num4;
}

echo "</tr>";
echo "</table><br />";

break;

case "DropCache": {

main_menu (true);

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";

echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\">".$tracker_lang['trun_tempfile']."</td></tr>";

if ($debug_time==true){
// ��������� ������� �����
$start_time = microtime();
// ��������� ������� � ������������
//(���������� ���������� ��������� ������ �������-������)
$start_array = explode(" ",$start_time);
// ��� � ���� ��������� �����
$start_time = $start_array[1] + $start_array[0];
}


$num = 0;
$file_size = 0;
$dh = opendir(ROOT_PATH.'/cache/');
while ($file = readdir($dh)) :
if (preg_match('/^(.+)\.$/si', $file, $matches))
$file = $matches[1];
$file_end = end(explode('.', $file));

$txt_main = array('info_cache_stat.txt','sqlerror.txt','error_torrent.txt');

if ($file_end == 'txt' && !stristr($file,'monitoring_') && !in_array($file, $txt_main)){
$file_size += filesize(ROOT_PATH."/cache/".$file);
@unlink(ROOT_PATH."/cache/".$file);
++$num;
}

endwhile;
closedir($dh);

echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"><b>".$tracker_lang['all_size']."</b>: ".mksize($file_size)." (".$num.")</td></tr>";

if (function_exists("memcache_flush") && $Cache_Config["memcache_on"] == true && (empty($Cache_Config["type"]) || $Cache_Config["type"] == "memcache")){
$tru = memcache_flush($memcache_server);

if ($tru == true)
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\">".$tracker_lang['mem_truntemp'].".</td></tr>";

}


/*
if (extension_loaded("XCache") && $Cache_Config["type"] == "xcache"){

if ($type != XC_TYPE_PHP && $type != XC_TYPE_VAR)
$type = null;

ini_set('xcache.admin.enable_auth', 0);

$count = xcache_count($type);

for ($cacheid = 0; $cacheid < $count; $cacheid ++) {
xcache_clear_cache($type, $cacheid);
}

//ini_set('xcache.admin.enable_auth', $xcache_auth);
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\">������� xcache ���� �������� �������.</td></tr>";

}
*/

if ($debug_time==true){
$end_time = microtime();
$end_array = explode(" ",$end_time);
$end_time = $end_array[1] + $end_array[0];
// �������� �� ��������� ������� ���������
$time = $end_time - $start_time;
$time = substr($time, 0, 8);

echo "<tr><td align=\"center\" colspan=\"2\" class=\"a\"><b>".$tracker_lang['time_end']."</b>: ".$time." ".$tracker_lang['sec']."</td></tr>";
}

echo "<tr><td align=\"center\" colspan=\"2\" class=\"a\">".$tracker_lang['success']."</td></tr>";

echo "</tr></table><br />";

}
break;

//// �������� ����� � ������ �� ������������ ��������� � �������������
case "CHmod": {

main_menu (true);

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";

echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\">".$tracker_lang['catalog_main']."</td></tr>";

$num = 0;
$array_php = array();

$dh = opendir(ROOT_PATH);
while ($file = readdir($dh)) :
$file_orig=$file;
if (preg_match('/^(.+)\.$/si', $file, $matches))
$file = $matches[1];

$file_end = end(explode('.', $file));
$array_types = array("","php","txt","xml");

if ($file_end == "php" && $file<>"php")
$array_php[] = $file;

elseif (!in_array($file_end, $array_types)){

if ($num%2 == 0) {
$clas1 = "class = \"a\"";
$clas2 = "class = \"b\"";
} else {
$clas2 = "class = \"a\"";
$clas1 = "class = \"b\"";
}

if ($file_end == "htaccess")
$cmod = end_chmod(ROOT_PATH.$file,"555");
elseif ($file_end == "backup"){
$cmod = end_chmod(ROOT_PATH.$file,"000");
$file_exist = file_exist_("/backup/");
} elseif ($file_end == "blocks"){
$cmod = end_chmod(ROOT_PATH.$file,"555"); /// ���������!
$file_exist = file_exist_("/blocks/");
}
elseif ($file_end == "cache"){
$cmod = end_chmod(ROOT_PATH.$file,"777");
$file_exist = file_exist_("/cache/");
}
elseif ($file_end == "include"){
$cmod = end_chmod(ROOT_PATH.$file,"555");
$file_exist = file_exist_("/include/");
}
elseif ($file_end == "js"){
$cmod = end_chmod(ROOT_PATH.$file,"555");
$file_exist = file_exist_("/js/");
}
elseif ($file_end == "languages"){
$cmod = end_chmod(ROOT_PATH.$file,"555");
$file_exist = file_exist_("/languages/");
}
elseif ($file_end == "pic"){
$cmod = end_chmod(ROOT_PATH.$file,"555");
$file_exist = file_exist_("/pic/");
}
elseif ($file_end == "swf"){
$cmod = end_chmod(ROOT_PATH.$file,"555");
$file_exist = file_exist_("/swf/");
}
elseif ($file_end == "themes"){
$cmod = end_chmod(ROOT_PATH.$file,"555");
$file_exist = file_exist_("/themes/");
}
elseif ($file_end == "torrents"){
$cmod = end_chmod(ROOT_PATH.$file,"777");
$file_exist = file_exist_("/torrents/");
}
elseif ($file_end == "torzip"){
$cmod = end_chmod(ROOT_PATH.$file,"000");
$file_exist = file_exist_("/torzip/");
} else
$cmod = "N/A. ".$tracker_lang['chmod_is'].": 555";

echo "<tr><td ".$clas1."><u>".$file."</u></td><td align=\"right\" ".$clas2.">".$cmod."</td></tr>";

if (!empty($file_exist))
echo "<tr><td ".$clas1.">".$file."/<u>.htaccess</u></td><td align=\"right\" ".$clas2.">".$file_exist."</td></tr>";

if ($file_end == "pic"){

$cmod = end_chmod(ROOT_PATH.$file."/avatar","777");
$file_exist = file_exist_("/pic/avatar/");

echo "<tr><td ".$clas1.">".$file."/<u>avatar</u></td><td align=\"right\" ".$clas2.">".$cmod."</td></tr>";

echo "<tr><td ".$clas1.">".$file."/avatar/<u>.htaccess</u></td><td align=\"right\" ".$clas2.">".$file_exist."</td></tr>";

} elseif ($file_end == "torrents"){

$cmod = end_chmod(ROOT_PATH.$file."/images","777");
$file_exist = file_exist_("/torrents/images/");

echo "<tr><td ".$clas1.">".$file."/<u>images</u></td><td align=\"right\" ".$clas2.">".$cmod."</td></tr>";

echo "<tr><td ".$clas1.">".$file."/images/<u>.htaccess</u></td><td align=\"right\" ".$clas2.">".$file_exist."</td></tr>";

$cmod = end_chmod(ROOT_PATH.$file."/tfiles","777");
$file_exist = file_exist_("/torrents/tfiles/");

echo "<tr><td ".$clas1.">".$file."/<u>tfiles</u></td><td align=\"right\" ".$clas2.">".$cmod."</td></tr>";

echo "<tr><td ".$clas1.">".$file."/tfiles/<u>.htaccess</u></td><td align=\"right\" ".$clas2.">".$file_exist."</td></tr>";


$cmod = end_chmod(ROOT_PATH.$file."/thumbnail","777");
$file_exist = file_exist_("/torrents/thumbnail/");

echo "<tr><td ".$clas1.">".$file."/<u>thumbnail</u></td><td align=\"right\" ".$clas2.">".$cmod."</td></tr>";

echo "<tr><td ".$clas1.">".$file."/thumbnail/<u>.htaccess</u></td><td align=\"right\" ".$clas2.">".$file_exist."</td></tr>";

$cmod = end_chmod(ROOT_PATH.$file."/txt","777");
$file_exist = file_exist_("/torrents/txt/");

echo "<tr><td ".$clas1.">".$file."/<u>txt</u></td><td align=\"right\" ".$clas2.">".$cmod."</td></tr>";

echo "<tr><td ".$clas1.">".$file."/txt/<u>.htaccess</u></td><td align=\"right\" ".$clas2.">".$file_exist."</td></tr>";

}

unset($cmod, $file, $file_exist);

++$num;
}

endwhile;

closedir($dh);


if (count($array_php)){

$num2 = 0;
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"> </td></tr>";

echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\">".$tracker_lang['my_tesla_files']."</td></tr>";

$phpbad_files = array();

foreach ($array_php AS $php){

if ($num2%2 == 0) {
$clas1 = "class = \"a\"";
$clas2 = "class = \"b\"";
} else {
$clas2 = "class = \"a\"";
$clas1 = "class = \"b\"";
}

if (!in_array($php, $phpfiles_on_tesla))
$phpbad_files[] = $php;

$cmod = end_chmod_(ROOT_PATH.$php,"555");

if ($cmod <> false){
echo "<tr><td ".$clas1.">./<u>".$php."</u></td><td align=\"right\" ".$clas2.">".$cmod."</td></tr>";
++$num2;
}
}

if (empty($num2))
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\">".$tracker_lang['chmod_no_error']."</td></tr>";

if (count($phpbad_files)){
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"> </td></tr>";
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"><b>".$tracker_lang['chmod_allowdel'].": ".count($phpbad_files)."</b></td></tr>";

foreach ($phpbad_files AS $php){

if ($num2%2 == 0) {
$clas1 = "class = \"a\"";
$clas2 = "class = \"b\"";
} else {
$clas2 = "class = \"a\"";
$clas1 = "class = \"b\"";
}

echo "<tr><td ".$clas1.">./<u>".$php."</u></td><td align=\"right\" ".$clas2.">".$tracker_lang['del_nowfile']."</td></tr>";
++$num2;
}

echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\">".$tracker_lang['help_listlicen']."</td></tr>";
}

unset($cmod);
}

echo "</tr></table><br />";

}

break;

case "updatetheme" : {

$ucount = sql_query("SELECT name FROM stylesheets WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$row_u = mysql_fetch_array($ucount);

if (!empty($row_u["name"]) && is_dir(ROOT_PATH."/themes/".$row_u["name"])){

sql_query("ALTER TABLE `users` CHANGE `stylesheet` `stylesheet` VARCHAR( 100 ) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT ".sqlesc($row_u["name"])) or sqlerr(__FILE__,__LINE__);

stdmsg($tracker_lang['success'], $tracker_lang['theme_is_main']);
} else
stdmsg($tracker_lang['error_data'], $tracker_lang['theme_is_error']);

if (!headers_sent())
header("Location: admincp.php?op=addtheme");
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=addtheme\"', 10);</script>");
die;

}
break;

case "addtheme": {

function addtheme($name, $action, $id, $themes_name = false) {

global $tracker_lang;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

unsql_cache("my_style");

if ($action == "add") {

if (!empty($themes_name) && empty($name))
$name = $themes_name;

$actthem = theme_is_valid($name);

if ($actthem['work'] == true && !empty($name)){
sql_query("INSERT INTO stylesheets (`name`, `uri`) VALUES (".sqlesc($name).", ".sqlesc($name).")");
$next_id = mysql_insert_id();
if ($next_id)
stdmsg($tracker_lang['success'], $tracker_lang['theme_is_add']);
else
stdmsg($tracker_lang['success'], $tracker_lang['theme_is_dybl']);
} else
stdmsg($tracker_lang['error_data'], $tracker_lang['theme_is_noadd'].(($actthem['work'] == false && count($actthem['error'])) ? "<br />".$tracker_lang['announce_missing_parameter'].": ".implode(", ", $actthem['error']):""));

if (!headers_sent() && !count($actthem['error']))
header("Location: admincp.php?op=addtheme");
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=addtheme\"', ".(count($actthem['error']) ? "15000":"10").");</script>");
die;

}

if ($action == "delete" && !empty($id) && is_valid_id($id)) {

global $default_theme;

$dus = sql_query("SELECT uri FROM stylesheets WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$ru = mysql_fetch_array($dus);

if (!empty($ru["uri"]) && !empty($default_theme))
sql_query("UPDATE users SET stylesheet = ".sqlesc($default_theme)." WHERE stylesheet = ".sqlesc($ru["uri"])) or sqlerr(__FILE__, __LINE__);

$text = mysql_modified_rows();

sql_query("DELETE FROM stylesheets WHERE id=".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

stdmsg($tracker_lang['success'], $tracker_lang['theme_is_delete']." ".($text >= 1 ? "<br />".$tracker_lang['theme_of'].": <b>".$text."</b>":""));

if ($text >= 1){

if (!headers_sent())
header("Refresh: 15; url=admincp.php?op=addtheme");
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=addtheme\"', 15000);</script>");
die;

} else {

if (!headers_sent())
header("Refresh: 3; url=admincp.php?op=addtheme");
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=addtheme\"', 3000);</script>");
die;

}

}

}
else {

main_menu (true);

global $default_theme;

echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";

echo "<tr><td class=\"colhead\" colspan=\"4\">".$tracker_lang['theme_all']."</td></tr>";

echo "<tr>
<td class=\"colhead\" align=\"center\">#</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['theme_lang']." / ".$tracker_lang['theme_work']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['action']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['delete']."</td>
</tr>";

$sql_field = sql_query("SHOW FULL FIELDS FROM users LIKE 'stylesheet'") or sqlerr(__FILE__,__LINE__);
$r_field = mysql_fetch_array($sql_field);

$rarray = array();
$res = sql_query("SELECT id, name FROM stylesheets ORDER BY id") or sqlerr(__FILE__,__LINE__);
$num = 0;
while ($row = mysql_fetch_array($res)){

$rarray[] = $row["name"];

if ($num%2 == 0) {
$cla1 = "class = \"b\"";
$cla2 = "class = \"a\"";
} else {
$cla1 = "class = \"a\"";
$cla2 = "class = \"b\"";
}

$sql = sql_query("SELECT COUNT(*) FROM users WHERE stylesheet = ".sqlesc($row["name"])) or sqlerr(__FILE__,__LINE__);
$r = mysql_fetch_array($sql);

$actthem = theme_is_valid($row['name']);

echo "<tr>
<td ".$cla2." align=\"center\">".$row['id']."</td>
<td ".$cla1." align=\"left\" valign=\"top\">".(count($actthem['error']) ? "<font color=\"red\"><b title=\"".$tracker_lang['announce_missing_parameter'].": ".implode(", ", $actthem['error'])."\">".$row['name']."</b></font>":"<b>".$row['name']."</b>")."

<br />".sprintf($tracker_lang['theme_user_is'], number_format($r[0]))."</td>

<td ".$cla2." align=\"center\"><form method=\"post\" action=\"admincp.php?op=updatetheme\"><input ".($r_field["Default"] == $row['name'] ? "disabled value=\"".$tracker_lang['theme_is_user']."\" ":"value=\"".$tracker_lang['theme_asmain']."\" ")." type=\"submit\" class=\"btn\" title=\"".$tracker_lang['theme_ishelp']."\" /> <input type=\"hidden\" name=\"op\" value=\"updatetheme\" /> <input type=\"hidden\" name=\"action\" value=\"update\" /> <input type=\"hidden\" name=\"id\" value=\"".$row['id']."\" /></form></td>

<td ".$cla1." align=\"center\">".($actthem['work'] == false ? "<font color=\"red\">".$tracker_lang['file_noexis']."</font>":"")."<form method=\"post\" action=\"admincp.php?op=addtheme\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['delete']."\"/> <input type=\"hidden\" name=\"op\" value=\"addtheme\" /> <input type=\"hidden\" name=\"action\" value=\"delete\" /> <input type=\"hidden\" name=\"id\" value=\"".$row['id']."\" /></form></td>
</tr>";

++$num;
}

echo "<tr><td align=\"left\" colspan=\"4\" class=\"b\">".$tracker_lang['theme_config'].": <b>".$default_theme."</b></td></tr>";

echo "</table>";
echo "<br />";

echo "<form method=\"post\" action=\"admincp.php?op=addtheme\">
<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['theme_add_new']."</td></tr>";

echo "<tr>
<td class=\"b\" width=\"20%\">".$tracker_lang['name']."</td>
<td class=\"a\"><input name=\"name\" type=\"text\" style=\"width:150\"> ".$tracker_lang['theme_valid']."</td>
</tr>";

$dh = opendir(ROOT_PATH.'/themes/');

while ($file = readdir($dh)) :
if (preg_match('/^(.+)\.$/si', $file, $matches))
$file = $matches[1];

if (!in_array($file, $rarray) && count(explode('.', $file)) == 1){

if (is_dir(ROOT_PATH.'/themes/'.$file))
$array_themes[] = "<option value=\"".$file."\">".$file."</option>";

}
endwhile;
closedir($dh);

if (count($array_themes)){
echo "<tr>
<td class=\"b\" width=20%>   </td>
<td class=\"a\">
<select name=\"themes_name\">".implode("\n", $array_themes)."</select>
".$tracker_lang['theme_exis']."</td>
</tr>";
}

echo "<tr>
<td class=\"b\" colspan=\"2\" align=\"center\"><input type=\"submit\" class=\"btn\" name=\"isub\" value=\"".$tracker_lang['theme_add_new']."\"></td></tr>
</table>
<input type=\"hidden\" name=\"op\" value=\"addtheme\" /> <input type=\"hidden\" name=\"action\" value=\"add\" />
</form>";

echo "<br />";

}
}

switch ($op) {
case "addtheme":
addtheme($name, $action, $id, $themes_name);
break;
}
}
break;

case "BlocksAdmin":
case "BlocksNew":
case "BlocksFile":
case "BlocksFileEdit":
case "BlocksAdd":
case "BlocksEdit":
case "Blockschecks":
case "BlocksEditSave":
case "BlocksChange":
case "BlocksDelete":
case "BlocksFixweight":
case "BlocksShow":
case "BlocksChecksAll":
case "BlocksOrder": {


$allowed_modules = array(
'adduser' => $tracker_lang['add_user'],
'adminbookmark' => $tracker_lang['admin_bookmark'],
'admincp' => $tracker_lang['admincp'],
'alltags' => $tracker_lang['all_tags'],
'antichiters' => $tracker_lang['traffic_tracker'],
'bannedemails' => $tracker_lang['ban_emails_is'],
'bans' => $tracker_lang['bans'],
'bookmarks' => $tracker_lang['bookmarks'],
'browse' => $tracker_lang['browse'],
'category' => $tracker_lang['category_tags'],
'cheaters' => $tracker_lang['cheating'],
'checkcomm' => $tracker_lang['monitor']." / ".$tracker_lang['monitor_comments'],
'citydd' => $tracker_lang['my_city_is'],
'comments_last' => $tracker_lang['forum_last_comments'],
'details' => $tracker_lang['torrent_details'],
'detailsoff' => $tracker_lang['requests_section'],
'docleanup' => $tracker_lang['docleanup_act'],
'downcheck' => $tracker_lang['downcheck_i'],
'errors' => $tracker_lang['errors'],
'faq' => $tracker_lang['faq'],
'faq_tesla' => 'FAQ �� ������ Tesla',
'fonts' => "��� ������ �����",
'formats' => '������� ������',
'forummanage' => $tracker_lang['admining_forum'],
'forums' => $tracker_lang['forum_main'],
'friends' => $tracker_lang['friends_list']." / ".$tracker_lang['blocked_list'],
'friensblockadd' => $tracker_lang['add_my_friends'],
'getrss' => $tracker_lang['rss']." / ".$tracker_lang['upload'],
'groups' => $tracker_lang['upload_groups'],
'groupsmess' => $tracker_lang['mass_pm']." (".$tracker_lang['group_realese'].")",
'hiderating' => $tracker_lang['delete_rating'],
'humor' => $tracker_lang['view_humor'].": ".$tracker_lang['add_humor'],
'humorall' => $tracker_lang['jokes'],
'invite' => $tracker_lang['my_invite'],
'ipcheck' => $tracker_lang['ipcheck'],
'license' => $tracker_lang['licence_files'],
'likeme' => "�������� ��� ����������",
'log' => $tracker_lang['log'],
'login' => $tracker_lang['login'],
'makepoll' => $tracker_lang['creating_polls'],
'maxlogin' => $tracker_lang['tabs_maxlogin'],
'mematch' => $tracker_lang['mematch'],
'message' => $tracker_lang['messages'],
'monitoring' => $tracker_lang['monitoring_sytem'],
'msg' => $tracker_lang['all_msg'],
'my' => $tracker_lang['my'],
'mybonus' => $tracker_lang['my_bonus'],
'mysimpaty' => $tracker_lang['respect_my'],
'mysql_stats' => "��������� Mysql",
'mytorrents' => $tracker_lang['torrent_my'],
'news' => $tracker_lang['news'],
'newsarchive' => $tracker_lang['news_olders'],
'newsoverview' => $tracker_lang['news'].": ".$tracker_lang['message_view'],
'persons' => $tracker_lang['persons_movie'],
'polloverview' => $tracker_lang['polls'],
'polls' => $tracker_lang['polls_olders'],
'premod' => $tracker_lang['modercp'],
'rating' => $tracker_lang['mark_download'],
'recover' => $tracker_lang['recover'],
'redirect' => $tracker_lang['tabs_red'],
'referer' => $tracker_lang['referrals'],
'repair_tags' => $tracker_lang['repair_tags'],
'rule' => $tracker_lang['rules']." I",
'rules' => $tracker_lang['rules']." II",
'seeders' => $tracker_lang['traffic_tracker'].": ".$tracker_lang['details_seeding'],
'setclass' => $tracker_lang['test_class'],
'signup' => $tracker_lang['signup'],
'sitemap' => "��� �������",
'smilies' => $tracker_lang['smilies'],
'staff' => $tracker_lang['staff'],
'staffmess' => $tracker_lang['mass_pm'],
'statistics' => $tracker_lang['statistics'],
'stats' => $tracker_lang['statistic'],
'subnet' => $tracker_lang['neighbours'],
'support' => $tracker_lang['support'],
'tags' => $tracker_lang['tags_bb_code'],
'testip' => $tracker_lang['test_ip']." / ".$tracker_lang['test_port'],
'testmail' => "������ �������� �����",
'tfiles' => $tracker_lang['dox'],
'tracker-chat' => $tracker_lang['chat'],
'topten' => "TOP 10",
'unco' => $tracker_lang['unco_users'],
'upload' => $tracker_lang['upload'],
'uploadapp' => $tracker_lang['offer_uploaders'],
'uploaders' => $tracker_lang['bl_uploader'],
'uploadoff' => $tracker_lang['requests_section'].": ".$tracker_lang['upload'],
'useragent' => $tracker_lang['tabs_agent'],
'userdetails' => $tracker_lang['profile'],
'userhistory' => $tracker_lang['userhistory'],
'users' => $tracker_lang['users'],
'usersearch' => $tracker_lang['search_admin'],
'ustatistics' => $tracker_lang['ustatistics'],
'viewreport' => $tracker_lang['complaints'],
'votes' => $tracker_lang['tab_rating'],

'zupload.php' => $tracker_lang['zupload'],
'warned' => $tracker_lang['warned_users'],
'words' => $tracker_lang['table_victorina']." / ".$tracker_lang['table_vika'],

'box' => $tracker_lang['view_online_ts'],

'403' => $tracker_lang['error'].': 403',
'404' => $tracker_lang['error'].': 404',
'500' => $tracker_lang['error'].': 500',
);


function BlocksNavi() {

global $SITE_Config, $tracker_lang;

echo "<h2>".$tracker_lang['block_act']." [<a class=\"altlink_white\" href=\"admincp.php?op=BlocksAdmin\">".$tracker_lang['block_main']."</a> | <a class=\"altlink_white\" href=\"admincp.php?op=BlocksNew\">".$tracker_lang['block_addnew']."</a>".($SITE_Config["integrity"] == true ? " | <a class=\"altlink_white\" href=\"admincp.php?op=BlocksChecksAll\">".$tracker_lang['block_checkmark']."</a>":"")."]</h2>";
}

function BlocksAdmin() {

global $SITE_Config, $tracker_lang;


$search = htmlspecialchars_uni(isset($_GET['search']) ? trim($_GET['search']):false);

if (!empty($search)){
$q = sqlesc("%".sqlwildcardesc(trim($search))."%");
$count = get_row_count("orbital_blocks", "WHERE title LIKE {$q} OR blockfile LIKE {$q}");
}

main_menu (false);

echo "<table width=\"100%\" border=\"0\" align=\"center\"><tr><td class=\"b\">".BlocksNavi(true)."</td></tr></table><br />";

echo "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" width=\"100%\">";


echo "<tr><td class=\"b\" align=\"center\" colspan=\"8\">
<form method=\"get\" action=\"admincp.php\"><input type=\"hidden\" name=\"op\" value=\"BlocksAdmin\" /> <b>".$tracker_lang['search']."</b>: <input type=\"text\" value=\"".(!empty($search) ? $search:"")."\" name=\"search\" size=\"40\"/>
<input type=\"submit\" style=\"height: 25px; width:150px\" class=\"btn\" value=\"".$tracker_lang['search']."\">
</form>
</td></tr>";


if (!empty($search) && empty($count))
echo "<tr><td class=\"a\" align=\"center\" colspan=\"8\">
<h3>".$tracker_lang['nothing_found']."</h3>
</td></tr>";
elseif (!empty($search) && !empty($count))
echo "<tr><td class=\"colhead\" align=\"center\" colspan=\"8\">
".$tracker_lang['arr_searched']."
</td></tr>";




echo "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" width=\"100%\">

<tr align=\"center\">
<td class=\"colhead\">#</td>
<td class=\"colhead\">".$tracker_lang['title']."</td>
<td class=\"colhead\">".$tracker_lang['position']."</td>
<td colspan=\"2\" class=\"colhead\">".$tracker_lang['stand']."</td>
<td class=\"colhead\">".$tracker_lang['status']."</td>
<td class=\"colhead\">".$tracker_lang['who_viewblock']."</td>
<td class=\"colhead\">".$tracker_lang['action']."</td>
</tr>";

$oldbposition = "";
$result = sql_query("SELECT a.bid, a.title, a.bposition, a.weight, a.active, a.blockfile, a.view, a.expire, a.action, a.md5, b.bid, b.bposition, b.weight, c.bid, c.bposition, c.weight, a.which, a.mobile_view
FROM orbital_blocks AS a
LEFT JOIN orbital_blocks AS b ON (b.bposition = a.bposition AND b.weight = a.weight-1)
LEFT JOIN orbital_blocks AS c ON (c.bposition = a.bposition AND c.weight = a.weight+1)
".((isset($search) && !empty($count)) ? "WHERE a.title LIKE {$q} OR a.blockfile LIKE {$q}":"")."
ORDER BY a.bposition, a.weight") or sqlerr(__FILE__,__LINE__);


while (list($bid, $title, $bposition, $weight, $active, $blockfile, $view, $expire, $action, $md5, $con1, $bposition1, $weight1, $con2, $bposition2, $weight2, $which, $mobile_view) = mysql_fetch_row($result)) {

if ($c%2==0) { $class="a"; $class2="b"; } else {$class="b"; $class2="a"; }

if ($oldbposition<>$bposition && !empty($c))
echo "<tr><td class=\"colhead\" colspan=\"9\"></tr><tr>";

if (($expire && $expire < time()) || (!$active && $expire)) {

if ($action == "d")
sql_query("UPDATE orbital_blocks SET active = '0', expire = '0' WHERE bid = ".sqlesc($bid));
elseif ($action == "r")
sql_query("DELETE FROM orbital_blocks WHERE bid = ".sqlesc($bid));

unsql_cache("blocks_all");

}
////////////
@chmod($blockfile, 0555);
/////////////
$weight_minus = $weight - 1;
$weight_plus = $weight + 1;

if (!file_exists("blocks/".$blockfile))
$view_colore="<font color=\"red\">".$blockfile."</font>";
else
$view_colore="<font color=\"green\">".$blockfile."</font>";

if (file_exists(ROOT_PATH."/blocks/".$blockfile))
$file_md5 = md5_file(ROOT_PATH."/blocks/".$blockfile);

if ($SITE_Config["integrity"] == true){

if ($file_md5<>$md5)
$check_file="<a href=\"admincp.php?op=Blockschecks&bid=$bid\"><img src=\"./pic/button_offline.gif\" title=\"".$tracker_lang['filemd5_nocheck']."\" border=\"0\"></a>";
else
$check_file="<img src=\"./pic/button_online.gif\" title=\"".$tracker_lang['filemd5_check']."\" border=\"0\">";

}
echo "<tr>
<td class=\"".$class2."\" align=\"center\">$bid</td>
<td class=\"".$class."\">".$title."<br />".$view_colore."</td>";


$oldbposition = $bposition;

if ($bposition == "l")
$bposition = $tracker_lang['bl_left'];
elseif ($bposition == "r")
$bposition = $tracker_lang['bl_right'];
elseif ($bposition == "c")
$bposition = $tracker_lang['bl_ctop'];
elseif ($bposition == "d")
$bposition = $tracker_lang['bl_cdown'];
elseif ($bposition == "b")
$bposition = $tracker_lang['bl_top'];
elseif ($bposition == "f")
$bposition = $tracker_lang['bl_down'];

echo "<td class=\"".$class2."\" align=\"center\">".$bposition."</td>";

echo "<td class=\"".$class."\" align=\"center\">".$weight."</td>";

echo "<td class=\"".$class2."\" align=\"center\">";

if ($con1)
echo "<a href=\"admincp.php?op=BlocksOrder&weight=$weight&bidori=$bid&weightrep=$weight_minus&bidrep=$con1\"><img src=\"./pic/admin/up.gif\" alt=\"".$tracker_lang['block_moveup']."\" title=\"".$tracker_lang['block_moveup']."\" border=\"0\"></a> ";

if ($con2)
echo "<a href=\"admincp.php?op=BlocksOrder&weight=$weight&bidori=$bid&weightrep=$weight_plus&bidrep=$con2\"><img src=\"./pic/admin/down.gif\" alt=\"".$tracker_lang['block_movedown']."\" title=\"".$tracker_lang['block_movedown']."\" border=\"0\"></a>";

echo "</td>";

$block_act = $active;
if ($active == 1) {
$active = "<font color=\"#009900\">".$tracker_lang['onn']."</font>";
$change = "title=\"".$tracker_lang['offf']."\" name=\"block_$bid\"><img src=\"./pic/admin/inactive.gif\" border=\"0\" alt=\"".$tracker_lang['offf']."\"></a>";
} elseif ($active == 0) {
$active = "<font color=\"#FF0000\">".$tracker_lang['offf']."</font>";
$change = "title=\"".$tracker_lang['onn'].".\"name=\"block_$bid\"><img src=\"./pic/admin/activate.gif\" border=\"0\" alt=\"".$tracker_lang['onn'].".\"></a>";
}

echo "<td class=\"".$class."\" align=\"center\">".$active." ".(!empty($check_file) ? "<br />".$check_file:"")."</td>";

if ($view == 0)
$who_view = "<font color=\"#009900\">".$tracker_lang['bl_all']."</font>";
elseif ($view == 1)
$who_view = "<font color=\"#D21E36\">".$tracker_lang['bl_users']."</font>";
elseif ($view == 2)
$who_view = "<font color=\"red\">".$tracker_lang['bl_staff']."</font>";
elseif ($view == 3)
$who_view = "<i>".$tracker_lang['bl_guest']."</i>";
elseif ($view == 4)
$who_view = "<font color=\"#9c2fe0\">".$tracker_lang['bl_vip_staff']."</font>";
elseif ($view == 5)
$who_view = "<font color=\"blue\">".$tracker_lang['bl_sysop']."</font>";
elseif ($view == 6)
$who_view = "<font color=\"#AA55FF\">".$tracker_lang['bl_onbots']."</font>";
elseif ($view == 7)
$who_view = "<font color=\"#FF7FD4\">".$tracker_lang['bl_alguest_nobot']."</font>";
elseif ($view == 8)
$who_view = "<font color=\"#FF7F2A\">".$tracker_lang['bl_uploader']."</font>";
elseif ($view == 9)
$who_view = "<font color=\"#CB21A0\">".$tracker_lang['bl_noratio']."</font>";
elseif ($view == 10)
$who_view = "<font color=\"#969696\">".$tracker_lang['bl_groupe']."</font>";
elseif ($view == 11)
$who_view = "<font color=\"#6B6BC0\">".$tracker_lang['bl_warn']."</font>";
elseif ($view == 12)
$who_view = "<font color=\"#0000FF\">".$tracker_lang['bl_forum']."</font>";
elseif ($view == 13)
$who_view = "<font color=\"#0000FF\">".$tracker_lang['bl_noforum']."</font>";
elseif ($view == 14)
$who_view = "<font color=\"#E5BB66\">".$tracker_lang['bl_chat']."</font>";
elseif ($view == 15)
$who_view = "<font color=\"#E5BB66\">".$tracker_lang['bl_nochat']."</font>";
elseif ($view == 16)
$who_view = "<font color=\"#ED19ED\">".$tracker_lang['bl_young']."</font>";
elseif ($view == 17)
$who_view = "<font color=\"#ED19ED\">".$tracker_lang['bl_pyoung']."</font>";
elseif ($view == 18)
$who_view = "<font color=\"#CB21A0\">".$tracker_lang['bl_ratio']."</font>";
elseif ($view == 19)
$who_view = "<font color=\"#E509FD\">".$tracker_lang['bl_vips']."</font>";
elseif ($view == 20)
$who_view = "<font color=\"#FD04A2\">".$tracker_lang['bl_hratio']."</font>";


$mobile_views = false;
if ($mobile_view == 'allow')
$mobile_views = "<font color='#0128FF'>������������ <ins>������</ins> �� ��������� �����������</font>";
elseif ($mobile_view == 'deny')
$mobile_views = "<font color='#E80101'><ins>�����������</ins> �� ��������� �����������</font>";
#elseif ($mobile_view == 'none')
#$mobile_views = "��� ����������, ����� ����������";

echo "<td class=\"".$class2."\" align=\"center\">".$who_view."".(!empty($mobile_views) ? "<br>".$mobile_views."":"")."</td>";

echo "<td class=\"".$class."\" align=\"center\"><a href=\"admincp.php?op=BlocksEdit&bid=$bid\" title=\"".$tracker_lang['edit']."\"><img src=\"./pic/admin/edit.gif\" border=\"0\" alt=\"".$tracker_lang['edit']."\"></a> <a href=\"admincp.php?op=BlocksChange&bid=".$bid."\" ".$change;
echo " <a href=\"admincp.php?op=BlocksDelete&bid=$bid\" OnClick=\"return DelCheck(this, '".$tracker_lang['delete']." &quot;$title&quot;?');\" title=\"".$tracker_lang['delete']."\"><img src=\"./pic/admin/delete.gif\" border=\"0\" alt=\"".$tracker_lang['delete']."\"></a>";
///	if ($block_act == 0)
echo " <a href=\"admincp.php?op=BlocksShow&bid=$bid\" title=\"".$tracker_lang['show_data']."\"><img src=\"./pic/admin/show.gif\" border=\"0\" alt=\"".$tracker_lang['show_data']."\"></a>";

++$c;

}

if (mysql_num_rows($result) == 0)
echo "<tr><td class=\"colhead\" colspan=\"9\">".$tracker_lang['no_data'];

echo "</td></tr></table><br />";
}

function BlocksNew() {

global $tracker_lang;

BlocksNavi();

$array_files = array();
$r = sql_query("SELECT * FROM orbital_blocks WHERE active = '1' ORDER BY weight ASC", $cache = array("type" => "disk", "file" => "blocks_all", "time" => 3*7200)) or sqlerr(__FILE__,__LINE__);
while ($row = mysql_fetch_assoc_($r)){
$array_files[] = $row['blockfile'];
}

echo "<h2>".$tracker_lang['block_addnew']."</h2>"
."<form action=\"admincp.php\" method=\"post\">"
."<table width=\"100%\" border=\"0\" align=\"center\">"
."<tr><td class=\"a\"><b>".$tracker_lang['title']."</b>:</td><td><input type=\"text\" name=\"title\" size=\"65\" maxlength=\"60\" /> <i>".sprintf($tracker_lang['max_simp_of'], 60)."</i></td></tr>"
."<tr><td class=\"a\"><b>".$tracker_lang['name_file_is']."</b>: </td><td>"
."<select name=\"blockfile\" id=\"values\">"
."<option name=\"blockfile\" value=\"\" selected>".$tracker_lang['no']."</option>";

$handle = opendir("blocks");
while ($file = readdir($handle)) {

if (preg_match("/^block\-(.+)\.php/", $file, $matches))
echo "<option ".(!in_array($file, $array_files) ? "style=\"font-weight:bold;\"":"")." value=\"".$file."\">".str_replace("_", " ", $matches[1])." (".$matches[0].")</option>\n";

}
closedir($handle);

echo "</select> ".$tracker_lang['or']." <input type=\"text\" name=\"mname\" size=\"30\" maxlength=\"60\" /> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>
<tr><td class=\"a\"><b>".$tracker_lang['position']."</b>:</td>
<td><select name=\"bposition\">
<option name=\"bposition\" value=\"l\">".$tracker_lang['bl_left']."</option>
<option name=\"bposition\" value=\"c\">".$tracker_lang['bl_ctop']."</option>
<option name=\"bposition\" value=\"d\">".$tracker_lang['bl_cdown']."</option>
<option name=\"bposition\" value=\"r\">".$tracker_lang['bl_right']."</option>
<option name=\"bposition\" value=\"b\">".$tracker_lang['bl_top']."</option>
<option name=\"bposition\" value=\"f\">".$tracker_lang['bl_down']."</option>
</select></td>
</tr>";

echo "<tr><td class=\"a\"><b>".$tracker_lang['block_inmodule']."</b>:</td><td class=\"b\" align=\"center\">
<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"left\"><tr>";

echo "<td class=\"b\" colspan=\"6\">";
echo "<select size=\"10\" MULTIPLE name=\"block_where[]\">";
global $allowed_modules;
$a = 1;
foreach ($allowed_modules as $name => $title) {
echo "<option value=\"".$name."\">".$title." (".$name.")</option>";
}
echo "</select>";
echo "</td>";

echo "</tr><tr>
<td class=\"a\"><input type=\"checkbox\" name=\"blockwhere[]\" value=\"ihome\"></td><td class=\"b\"><b>".$tracker_lang['homepage']."</b></td>
<td class=\"a\"><input type=\"checkbox\" name=\"blockwhere[]\" value=\"home\"></td><td><b>".$tracker_lang['block_indexpoc']."</b></td>
<td class=\"a\"><input type=\"checkbox\" name=\"blockwhere[]\" value=\"all\"></td><td><b>".$tracker_lang['block_allpoc']."</b></td>
</tr></table></td>

</tr>";

echo "<tr><td><b>".$tracker_lang['enable']."</b></td><td><input type=\"radio\" name=\"active\" value=\"1\" checked>".$tracker_lang['yes']." &nbsp;&nbsp; <input type=\"radio\" name=\"active\" value=\"0\">".$tracker_lang['no']."</td></tr>

<tr><td><b>".$tracker_lang['time_work_in']."</b>:</td><td><input type=\"text\" name=\"expire\" maxlength=\"3\" value=\"0\" size=\"5\"> <b>".$tracker_lang['expire_act']."</b>: <select name=\"action\">"
."<option name=\"action\" value=\"d\">".$tracker_lang['offf']."</option>"
."<option name=\"action\" value=\"r\">".$tracker_lang['delete']."</option></select>
</td></tr>"

."<tr><td><b>".$tracker_lang['who_viewblock']."</b></td><td><select name=\"view\">"
."<option value=\"0\">".$tracker_lang['bl_all']."</option>"
."<option value=\"1\">".$tracker_lang['bl_users']."</option>"
."<option value=\"2\">".$tracker_lang['bl_staff']."</option>"
."<option value=\"3\">".$tracker_lang['bl_guest']."</option>"
."<option value=\"4\">".$tracker_lang['bl_vip_staff']."</option>"
."<option value=\"19\">".$tracker_lang['bl_vips']."</option>"
."<option value=\"5\">".$tracker_lang['bl_sysop']."</option>"
."<option value=\"6\">".$tracker_lang['bl_onbots']."</option>"
."<option value=\"7\">".$tracker_lang['bl_alguest_nobot']."</option>"
."<option value=\"8\">".$tracker_lang['bl_uploader']."</option>"
."<option value=\"9\">".$tracker_lang['bl_noratio']."</option>"
."<option value=\"10\">".$tracker_lang['bl_groupe']."</option>"
."<option value=\"11\">".$tracker_lang['bl_warn']."</option>"
."<option value=\"12\">".$tracker_lang['bl_forum']."</option>"
."<option value=\"13\">".$tracker_lang['bl_noforum']."</option>"
."<option value=\"14\">".$tracker_lang['bl_chat']."</option>"
."<option value=\"15\">".$tracker_lang['bl_nochat']."</option>"
."<option value=\"16\">".$tracker_lang['bl_young']."</option>"
."<option value=\"17\">".$tracker_lang['bl_pyoung']."</option>"
."<option value=\"18\">".$tracker_lang['bl_ratio']."</option>"
."<option value=\"20\">".$tracker_lang['bl_hratio']."</option>"

."</select></td></tr>";

$mobile_view = (isset($_SESSION['is_mobile']) ? ($_SESSION['is_mobile'] == true ? "allow":"none"):"none");

echo "<tr><td><b>��������� ����������</b></td><td><select name=\"mobile_view\">
<option name=\"mobile_view\" value=\"none\" ".(($mobile_view == "none" OR !in_array($mobile_view, array('allow', 'deny'))) ? "selected" : "").">��� ����������, ����� ����������</option>
<option name=\"mobile_view\" value=\"allow\" ".($mobile_view == "allow" ? "selected" : "").">���������� ������ �� ���������</option>
<option name=\"mobile_view\" value=\"deny\" ".($mobile_view == "deny" ? "selected" : "").">��������� �� ��������� �����������</option>
</select></td></tr>";


echo "<tr><td colspan=\"2\" align=\"center\"><br /><input type=\"hidden\" name=\"op\" value=\"BlocksAdd\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['create']."\"></td></tr></table></form>";
}

function BlocksFile() {

global $tracker_lang;

BlocksNavi();

echo "<h2>".$tracker_lang['block_addnew']."</h2>"
."<form action=\"admincp.php\" method=\"post\">"
."<table border=\"0\" align=\"center\">"
."<tr><td>".$tracker_lang['name_file_is'].":</td><td><input type=\"text\" name=\"bf\" size=\"65\" maxlength=\"200\">"
."<tr><td>".$tracker_lang['type'].":</td><td><input type=\"radio\" name=\"flag\" value=\"php\" checked>PHP</td></tr>"
."<tr><td colspan=\"2\" align=\"center\"><br /><input type=\"hidden\" name=\"op\" value=\"BlocksbfEdit\" />"
."<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['create']."\" /></td></tr></table></form>";

}

function BlocksOrder($weightrep, $weight, $bidrep, $bidori) {

sql_query("UPDATE orbital_blocks SET weight = ".sqlesc($weight)." WHERE bid = ".sqlesc($bidrep)) or sqlerr(__FILE__,__LINE__);
sql_query("UPDATE orbital_blocks SET weight = ".sqlesc($weightrep)." WHERE bid = ".sqlesc($bidori)) or sqlerr(__FILE__,__LINE__);



/// ����������� ��������� � ������ ��������� ������ �� id
$r = sql_query("SELECT bid, bposition, weight FROM orbital_blocks ORDER BY bposition, weight ASC") or sqlerr(__FILE__,__LINE__);

while ($row = mysql_fetch_array($r)){

if (empty($old_pos) || $old_pos <> $row["bposition"]){
$old_pos = $row["bposition"];
$number = 1;
}

if ($number <> $row["weight"])
sql_query("UPDATE orbital_blocks SET weight = ".sqlesc($number)." WHERE bid = ".sqlesc($row["bid"])) or sqlerr(__FILE__,__LINE__);

$old_pos = $row["bposition"];

++$number;
}
/// ����������� ��������� � ������ ��������� ������ �� id





unsql_cache("blocks_all");

if (!headers_sent())
header("Location: admincp.php?op=BlocksAdmin#block_".$bidori);
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksAdmin#block_".$bidori."\"', 3000);</script>");
die;

}

function Blockschecks($bid) {

$row = mysql_fetch_array(sql_query("SELECT blockfile FROM orbital_blocks WHERE bid = ".sqlesc($bid)));
$blockfile=$row["blockfile"];

if ($blockfile && file_exists(ROOT_PATH."/blocks/".$blockfile)){

$file_md5 = md5_file(ROOT_PATH."/blocks/".$blockfile);

$result = sql_query("UPDATE orbital_blocks SET md5 = ".sqlesc($file_md5)." WHERE bid = ".sqlesc($bid)) or sqlerr(__FILE__,__LINE__);
unsql_cache("blocks_all");

if (!headers_sent())
header("Location: admincp.php?op=BlocksAdmin#block_".$bid);
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksAdmin#block_".$bid."\"', 10);</script>");
die;

} else {

if (!headers_sent())
header("Location: admincp.php?op=BlocksAdmin#no_file");
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksAdmin#no_file\"', 10);</script>");
die;
    
}

}

function BlocksChecksAll($id){

$r = sql_query("SELECT blockfile, bid FROM orbital_blocks") or sqlerr(__FILE__,__LINE__);

while ($row = mysql_fetch_array($r)){

$blockfile = $row["blockfile"];
$bid = $row["bid"];

if (file_exists(ROOT_PATH."/blocks/".$blockfile)){
$file_md5 = md5_file(ROOT_PATH."/blocks/".$blockfile);
$result = sql_query("UPDATE orbital_blocks SET md5 = ".sqlesc($file_md5)." WHERE bid = ".sqlesc($bid)) or sqlerr(__FILE__,__LINE__);
}

}
unsql_cache("blocks_all");

if (!headers_sent())
header("Location: admincp.php?op=BlocksAdmin#ready");
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksAdmin#ready\"', 10);</script>");
die;

}

function BlocksAdd($title, $bposition, $active, $blockfile, $view, $expire, $action, $mobile_view) {

global $allowed_modules, $tracker_lang;;

list($weight) = mysql_fetch_row(sql_query("SELECT weight FROM orbital_blocks WHERE bposition = ".sqlesc($bposition)." ORDER BY weight DESC"));

++$weight;

$btime = "";

$mname = htmlspecialchars_uni(trim(isset($_POST['mname']) ? strip_tags($_POST['mname']):false));
if (!empty($mname) && $mname <> false && file_exists(ROOT_PATH."/blocks/".$mname))
$blockfile = $mname;

if (!empty($blockfile) && empty($title))
$title = str_replace(array("block-", ".php"), "", $blockfile);


if (empty($blockfile) || empty($title)) {
stdmsg($tracker_lang['error'], $tracker_lang['block_empty'], 'error');
} else {

if (empty($expire) || $expire == 0 || !is_valid_id($expire))
$expire = 0;
else
$expire = time() + ($expire * 86400);


if (isset($_POST['blockwhere']) || isset($_POST['block_where'])) {

if (is_array($_POST['blockwhere']))
$blockwhere = $_POST['blockwhere'];
else
$blockwhere = array();

$temp = array();

if (is_array($_POST['block_where'])){

foreach ($_POST['block_where'] AS $blex){

if (key_exists($blex, $allowed_modules))
$blockwhere[] = $blex;

}

}

$which = "";
$which = (in_array("all", $blockwhere)) ? "all" : $which;
$which = (in_array("home", $blockwhere)) ? "home" : $which;

if (empty($which)) {

foreach ($blockwhere AS $binput){
$temp[] = $binput;

}

if (count($temp))
$which = implode(",", $temp);
}

}

if (file_exists(ROOT_PATH."/blocks/".$blockfile))
$md5 = md5_file(ROOT_PATH."/blocks/".$blockfile);

///title 	bposition 	weight 	active 	time 	blockfile 	md5 	view 	expire 	action 	which
sql_query("INSERT INTO orbital_blocks VALUES (NULL, ".implode(", ", array_map("sqlesc", array($title, $bposition, $weight, $active, $btime, $blockfile, $md5, $view, $expire, $action, $which, $mobile_view))).")") or sqlerr(__FILE__,__LINE__);

$id = mysql_insert_id();

unsql_cache("blocks_all");

if (!headers_sent())
header("Location: admincp.php?op=BlocksAdmin#block_".$id);
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksAdmin#block_".$id."\"', 10);</script>");
die;

}
}

function BlocksEdit($bid) {

BlocksNavi();

global $tracker_lang;

list($title, $bposition, $weight, $active, $blockfile, $view, $expire, $action, $which, $mobile_view) = mysql_fetch_row(sql_query("SELECT title, bposition, weight, active, blockfile, view, expire, action, which, mobile_view FROM orbital_blocks WHERE bid = ".sqlesc($bid)));


$array_files = array();
$r = sql_query("SELECT * FROM orbital_blocks WHERE active = '1' ORDER BY weight ASC", $cache = array("type" => "disk", "file" => "blocks_all", "time" => 3*7200)) or sqlerr(__FILE__,__LINE__);
while ($row = mysql_fetch_assoc_($r)){
$array_files[] = $row['blockfile'];
}


echo "<h2>".$tracker_lang['who_viewblock'].": ".$title." ".(empty($type)? "":$type)."</h2>"
."<form action=\"admincp.php\" method=\"post\">"
."<table width=\"100%\" border=\"0\" align=\"center\">"
."<tr><td class=\"a\"><b>".$tracker_lang['title']."</b>:</td><td><input type=\"text\" name=\"title\" maxlength=\"50\" size=\"65\" value=\"$title\" /> <i>".sprintf($tracker_lang['max_simp_of'], 60)."</i></td></tr>";

if (!empty($blockfile)) {
echo "<tr><td class=\"a\"><b>".$tracker_lang['name_file_is']."</b>:</td><td><select id=\"values\" name=\"blockfile\">";

$dir = opendir("blocks");
while ($file = readdir($dir)) {

if (preg_match("/^block\-(.+)\.php/", $file, $matches)){

echo "<option ".(!in_array($file, $array_files) ? "style=\"font-weight:bold;\"":"")." value=\"".$file."\" ".($blockfile == $file ? "selected" : "").">".str_replace("_", " ", $matches[1])." (".$matches[0].")</option>";
}


}
closedir($dir);

echo "</select> ".$tracker_lang['or']." <input type=\"text\" name=\"mname\" size=\"30\" maxlength=\"60\" /> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>";
}

$oldposition = $bposition;
echo "<input type=\"hidden\" name=\"oldposition\" value=\"".$oldposition."\">";

echo "<tr><td class=\"a\"><b>".$tracker_lang['position']."</b>:</td><td><select name=\"bposition\">"
."<option name=\"bposition\" value=\"l\" ".($bposition == "l" ? "selected" : "").">".$tracker_lang['bl_left']."</option>"
."<option name=\"bposition\" value=\"c\" ".($bposition == "c" ? "selected" : "").">".$tracker_lang['bl_ctop']."</option>"
."<option name=\"bposition\" value=\"d\" ".($bposition == "d" ? "selected" : "").">".$tracker_lang['bl_cdown']."</option>"
."<option name=\"bposition\" value=\"r\" ".($bposition == "r" ? "selected" : "").">".$tracker_lang['bl_right']."</option>"
."<option name=\"bposition\" value=\"b\" ".($bposition == "b" ? "selected" : "").">".$tracker_lang['bl_top']."</option>"
."<option name=\"bposition\" value=\"f\" ".($bposition == "f" ? "selected" : "").">".$tracker_lang['bl_down']."</option>"
."</select></td></tr>";

echo "<tr><td class=\"a\"><b>".$tracker_lang['block_inmodule']."</b>:</td>
<td align=\"center\" class=\"b\">
<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"left\"><tr>";

$where_mas = explode(",", $which);
$cel = ($where_mas[0] == "ihome") ? " checked" : "";

echo "<td class=\"b\" colspan=\"6\">";
echo "<select size=\"10\" MULTIPLE name=\"block_where[]\">";
global $allowed_modules;
$a = 1;
foreach ($allowed_modules as $name => $title) {
echo "<option ".(in_array($name, $where_mas) ? "selected":"")." value=\"".$name."\">".$title." (".$name.")</option>";
}
echo "</select>";
echo "</td>";

$where_mas = explode(",", $which);
$cel = "";
$hel = "";

switch ($where_mas[0]) {
case "all": $cel = " checked";
break;
case "home": $hel = " checked";
break;
}

echo "</tr><tr>
<td class=\"a\"><input type=\"checkbox\" name=\"blockwhere[]\" value=\"ihome\" ".($where_mas[0] == "ihome" ? "checked":"")."></td><td class=\"b\"><b>".$tracker_lang['homepage']."</b></td>
<td class=\"a\"><input type=\"checkbox\" name=\"blockwhere[]\" value=\"home\" ".(empty($hel)? "":$hel)."></td><td><b>".$tracker_lang['block_indexpoc']."</b></td>
<td class=\"a\"><input type=\"checkbox\" name=\"blockwhere[]\" value=\"all\" ".(empty($cel)? "":$cel)."></td><td><b>".$tracker_lang['block_allpoc']."</b></td>
</tr></table>
</td></tr>";

if (!empty($expire)){
$newexpire = 0;
$oldexpire = $expire;
$expire = intval(($expire - time()) / 3600);
$exp_day = $expire / 24;
$expire_text = "<input type=\"hidden\" name=\"expire\" value=\"".$oldexpire."\"><b>".$tracker_lang['left']."</b>: ".$expire." ".$tracker_lang['hours']." (".substr($exp_day,0,5)." ".$tracker_lang['day_all'].")";
} else {
$newexpire = 1;
$expire_text = "<input type=\"text\" name=\"expire\" value=\"0\" maxlength=\"3\" size=\"5\">";
}

echo "<tr>
<td class=\"a\"><b>".$tracker_lang['enable']."</b></td><td><input type=\"radio\" name=\"active\" value=\"1\" ".($active == 1 ? "checked" : "").">".$tracker_lang['yes']."&nbsp;&nbsp;<input type=\"radio\" name=\"active\" value=\"0\" ".($active == 0 ? "checked" : "").">".$tracker_lang['no']."</td></tr>
<tr><td class=\"a\"><b>".$tracker_lang['time_work_in']."</b>:</td><td>".$expire_text."

<b>".$tracker_lang['expire_act']."</b>: <select name=\"action\">
<option name=\"action\" value=\"d\" ".($action == "d" ? "selected" : "").">".$tracker_lang['offf']."</option>
<option name=\"action\" value=\"r\" ".($action == "r" ? "selected" : "").">".$tracker_lang['delete']."</option></select>

</td></tr>";

echo "</td></tr>
<tr><td class=\"a\"><b>".$tracker_lang['who_viewblock']."</b></td>
<td><select name=\"view\">"
."<option value=\"0\" ".($view == 0 ? "selected" : "").">".$tracker_lang['bl_all']."</option>"
."<option value=\"1\" ".($view == 1 ? "selected" : "").">".$tracker_lang['bl_users']."</option>"
."<option value=\"2\" ".($view == 2 ? "selected" : "").">".$tracker_lang['bl_staff']."</option>"
."<option value=\"3\" ".($view == 3 ? "selected" : "").">".$tracker_lang['bl_guest']."</option>"
."<option value=\"19\" ".($view == 19 ? "selected" : "").">".$tracker_lang['bl_vips']."</option>"
."<option value=\"4\" ".($view == 4 ? "selected" : "").">".$tracker_lang['bl_vip_staff']."</option>"
."<option value=\"5\" ".($view == 5 ? "selected" : "").">".$tracker_lang['bl_sysop']."</option>"
."<option value=\"6\" ".($view == 6 ? "selected" : "").">".$tracker_lang['bl_onbots']."</option>"
."<option value=\"7\" ".($view == 7 ? "selected" : "").">".$tracker_lang['bl_alguest_nobot']."</option>"
."<option value=\"8\" ".($view == 8 ? "selected" : "").">".$tracker_lang['bl_uploader']."</option>"
."<option value=\"9\" ".($view == 9 ? "selected" : "").">".$tracker_lang['bl_noratio']."</option>"
."<option value=\"10\" ".($view == 10 ? "selected" : "").">".$tracker_lang['bl_groupe']."</option>"
."<option value=\"11\" ".($view == 11 ? "selected" : "").">".$tracker_lang['bl_warn']."</option>"
."<option value=\"12\" ".($view == 12 ? "selected" : "").">".$tracker_lang['bl_forum']."</option>"
."<option value=\"13\" ".($view == 13 ? "selected" : "").">".$tracker_lang['bl_noforum']."</option>"
."<option value=\"14\" ".($view == 14 ? "selected" : "").">".$tracker_lang['bl_chat']."</option>"
."<option value=\"15\" ".($view == 15 ? "selected" : "").">".$tracker_lang['bl_nochat']."</option>"
."<option value=\"16\" ".($view == 16 ? "selected" : "").">".$tracker_lang['bl_young']."</option>"
."<option value=\"17\" ".($view == 17 ? "selected" : "").">".$tracker_lang['bl_pyoung']."</option>"
."<option value=\"18\" ".($view == 18 ? "selected" : "").">".$tracker_lang['bl_ratio']."</option>"
."<option value=\"20\" ".($view == 20 ? "selected" : "").">".$tracker_lang['bl_hratio']."</option>"


."</select></td></tr>";


echo "<tr><td class=\"a\"><b>��������� ����������</b></td><td><select name=\"mobile_view\">
<option name=\"mobile_view\" value=\"none\" ".(($mobile_view == "none" OR !in_array($mobile_view, array('allow', 'deny'))) ? "selected" : "").">��� ����������, ����� ����������</option>
<option name=\"mobile_view\" value=\"allow\" ".($mobile_view == "allow" ? "selected" : "").">���������� ������ �� ���������</option>
<option name=\"mobile_view\" value=\"deny\" ".($mobile_view == "deny" ? "selected" : "").">��������� �� ��������� �����������</option>
</select></td></tr>";




echo "</table><br />"
."<center><input type=\"hidden\" name=\"bid\" value=\"".$bid."\">"
."<input type=\"hidden\" name=\"newexpire\" value=\"".$newexpire."\">"
."<input type=\"hidden\" name=\"weight\" value=\"".$weight."\">"
."<input type=\"hidden\" name=\"op\" value=\"BlocksEditSave\">"
."<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['safe_editting']."\"></form></center>";
}

function BlocksEditSave($newexpire, $bid, $title, $oldposition, $bposition, $active, $weight, $blockfile, $view, $expire, $action, $mobile_view) {

global $allowed_modules;


$mname = htmlspecialchars_uni(trim(isset($_POST['mname']) ? strip_tags($_POST['mname']):false));
if (!empty($mname) && $mname <> false && file_exists(ROOT_PATH."/blocks/".$mname))
$blockfile = $mname;

if (!empty($blockfile) && empty($title))
$title = str_replace(array("block-", ".php"), "", $blockfile);



if (isset($_POST['blockwhere']) || isset($_POST['block_where'])) {

if (is_array($_POST['blockwhere']))
$blockwhere = $_POST['blockwhere'];
else
$blockwhere = array();

$temp = array();

if (is_array($_POST['block_where'])){

foreach ($_POST['block_where'] AS $blex){

if (key_exists($blex, $allowed_modules))
$blockwhere[] = $blex;

}

}

$which = "";
$which = (in_array("all", $blockwhere)) ? "all" : $which;
$which = (in_array("home", $blockwhere)) ? "home" : $which;

if (empty($which)) {

foreach ($blockwhere AS $binput){
$temp[] = $binput;

}

if (count($temp))
$which = implode(",", $temp);
}

sql_query("UPDATE orbital_blocks SET which = ".sqlesc($which)." WHERE bid = ".sqlesc($bid));
} else {
sql_query("UPDATE orbital_blocks SET which = '' WHERE bid = ".sqlesc($bid));
}

if ($oldposition <> $bposition) {
$result5 = sql_query("SELECT bid FROM orbital_blocks WHERE weight >= ".sqlesc($weight)." AND bposition = ".sqlesc($bposition));
$fweight = $weight;
$oweight = $weight;
while (list($nbid) = mysql_fetch_row($result5)) {
++$weight;
sql_query("UPDATE orbital_blocks SET weight = ".sqlesc($weight)." WHERE bid = ".sqlesc($nbid)) or sqlerr(__FILE__,__LINE__);
}

$result6 = sql_query("SELECT bid FROM orbital_blocks WHERE weight > ".sqlesc($oweight)." AND bposition = ".sqlesc($oldposition)) or sqlerr(__FILE__,__LINE__);
while (list($obid) = mysql_fetch_row($result6)) {
sql_query("UPDATE orbital_blocks SET weight = ".sqlesc($oweight)." WHERE bid = ".sqlesc($obid));
++$oweight;
}

list ($lastw) = mysql_fetch_row(sql_query("SELECT weight FROM orbital_blocks WHERE bposition = ".sqlesc($bposition)." ORDER BY weight DESC LIMIT 0, 1"));
if ($lastw <= $fweight) {
++$lastw;

if (file_exists(ROOT_PATH."/blocks/".$blockfile))
$file_md5 = md5_file(ROOT_PATH."/blocks/".$blockfile);

sql_query("UPDATE orbital_blocks SET title=".sqlesc($title).", bposition=".sqlesc($bposition).", weight=".sqlesc($lastw).", active=".sqlesc($active).", md5=".sqlesc($file_md5).", blockfile=".sqlesc($blockfile).", view=".sqlesc($view).", mobile_view = ".sqlesc($mobile_view)." WHERE bid=".sqlesc($bid)) or sqlerr(__FILE__,__LINE__);
} else {


if (file_exists(ROOT_PATH."/blocks/".$blockfile))
$file_md5 = md5_file(ROOT_PATH."/blocks/".$blockfile);


sql_query("UPDATE orbital_blocks SET title=".sqlesc($title).", bposition=".sqlesc($bposition).", weight=".sqlesc($fweight).", active=".sqlesc($active).", md5=".sqlesc($file_md5).", blockfile=".sqlesc($blockfile).", view=".sqlesc($view).", mobile_view = ".sqlesc($mobile_view)." WHERE bid=".sqlesc($bid)) or sqlerr(__FILE__,__LINE__);
}
} else {

if (file_exists(ROOT_PATH."/blocks/".$blockfile))
$file_md5 = md5_file(ROOT_PATH."/blocks/".$blockfile);

if (empty($expire))
$expire = 0;

if ($newexpire == 1 && !empty($expire))
$expire = time() + ($expire * 86400);

$result8 = sql_query("UPDATE orbital_blocks SET title = ".sqlesc($title).", bposition = ".sqlesc($bposition).", weight = ".sqlesc($weight).", md5 = ".sqlesc($file_md5).", active = ".sqlesc($active).", blockfile = ".sqlesc($blockfile).", view = ".sqlesc($view).", expire = ".sqlesc($expire).", action = ".sqlesc($action).", mobile_view = ".sqlesc($mobile_view)." WHERE bid = ".sqlesc($bid)) or sqlerr(__FILE__,__LINE__);

}

unsql_cache("blocks_all");

if (!headers_sent())
header("Location: admincp.php?op=BlocksEdit&bid=".$bid);
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksEdit&bid=".$bid."\"', 10);</script>");
die;

}

function BlocksShow($bid) {

global $tracker_lang;

BlocksNavi();

list ($bid, $title, $bposition, $blockfile, $md5) = mysql_fetch_row(sql_query("SELECT bid, title, bposition, blockfile, md5 FROM orbital_blocks WHERE bid = ".sqlesc($bid)));

echo "<h4>[<a href=\"admincp.php?op=BlocksChange&bid=".$bid."\">".$tracker_lang['enable']."</a> | <a href=\"admincp.php?op=BlocksEdit&bid=".$bid."\">".$tracker_lang['edit']."</a>";
echo " | <a href=\"admincp.php?op=BlocksDelete&bid=".$bid."\" OnClick=\"return DelCheck(this, '".$tracker_lang['delete']." &quot;$title&quot;?');\">".$tracker_lang['delete']."</a>";
echo " | <a href=\"admincp.php?op=BlocksAdmin\">".$tracker_lang['main']."</a>]</h4>";

render_blocks($bposition, $blockfile, $title,  $content, $bid, 'c', $md5);

echo "<br />";

}

function BlocksFileEdit() {

global $tracker_lang;

BlocksNavi();

echo "<h2>".$tracker_lang['edit']."</h2>"
."<form action=\"admincp.php\" method=\"post\">"
."<table border=\"0\" align=\"center\">"
."<tr><td>".$tracker_lang['name_file_is'].":</td><td>"
."<select name=\"bf\">";

$handle = opendir("blocks");
while ($file = readdir($handle)) {

if (preg_match("/^block\-(.+)\.php/", $file, $matches)) {
if (mysql_num_rows(sql_query("SELECT * FROM orbital_blocks WHERE blockfile=".sqlesc($file))) > 0) 
echo "<option value=\"".$file."\">".str_replace("-", " ", $matches[1])." (".$matches[0].")</option>\n";
}

}
closedir($handle);

echo "</select></td></tr>"
."<tr><td colspan=\"2\" align=\"center\"><input type=\"hidden\" name=\"op\" value=\"BlocksbfEdit\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['edit']."\"></td></tr></table></form>";

}

function BlocksChange($bid, $ok=0) {

global $tracker_lang;

$row = mysql_fetch_array(sql_query("SELECT active FROM orbital_blocks WHERE bid = ".sqlesc($bid)));

$active = intval($row['active']);

if (($ok) || ($active == 0)) {

if ($active == 0)
$active = 1;
elseif ($active == 1)
$active = 0;

$result = sql_query("UPDATE orbital_blocks SET active = ".sqlesc($active)." WHERE bid = ".sqlesc($bid));
unsql_cache("blocks_all");

if (!headers_sent()) {
Header("Location: admincp.php?op=BlocksAdmin");
die;
} else 
die("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksAdmin\"', 10);</script>");


} else {

list($title, $active) = mysql_fetch_row(sql_query("SELECT title, active FROM orbital_blocks WHERE bid=".sqlesc($bid)));

if ($active == 0)
echo "<center>".$tracker_lang['enable']." \"".$title."\"?<br /><br />";
else
echo "<center>".$tracker_lang['disable']." \"".$title."\"?<br /><br />";

echo "[<a href=\"admincp.php?op=BlocksChange&bid=$bid&ok=1\">".$tracker_lang['yes']."</a> | <a href=\"admincp.php?op=BlocksAdmin\">".$tracker_lang['no']."</a>]</center>";

}
if ($_GET["ok"]==1){

if (!headers_sent())
header("Location: admincp.php?op=BlocksAdmin");
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksAdmin\"', 10);</script>");
die;

}

}

function BlocksbfEdit() {

global $tracker_lang;

if ($_REQUEST['bf'] != "") {

$bf = $_REQUEST['bf'];
if (isset($_POST['flag'])) {
$flaged = $_POST['flag'];
$bf = str_replace("block-", "",$bf);
$bf = str_replace(".php", "",$bf);
$bf = 'block-'.$bf.'.php';
} else {
$bfstr = file_get_contents('blocks/'.$bf);
if (strpos($bfstr,'BLOCKHTML') === false) {
$flaged = 'php';
preg_match("/<\?php.*if.*\(\!defined\(\'BLOCK_FILE\'\)\).*exit;.*?}(.*)\?>/is", $bfstr, $out);
unset($out[0]);
} else {
$flaged = 'html';
preg_match("/<<<BLOCKHTML(.*)BLOCKHTML;/is", $bfstr, $out);
unset($out[0]);
}
}

BlocksNavi();
$permtest = end_chmod("blocks", 777);
if ($permtest)
stdmsg($tracker_lang['error'], $permtest, 'error');

echo "<h2>".$tracker_lang['who_viewblock'].": $bf</h2>"
."<form action=\"admincp.php\" method=\"post\">"
."<table border=\"0\" align=\"center\">"
."<tr><td colspan=\"2\" align=\"center\"><br /><input type=\"hidden\" name=\"bf\" value=\"".$bf."\" />"
."<input type=\"hidden\" name=\"flag\" value=\"".$flaged."\">"
."<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['safe_editting']."\"> <input type=\"button\" value=\"�����\" onClick=\"javascript:history.go(-1)\"></td></tr></table></form>";

} else {

if (!headers_sent()) {
Header("Location: admincp.php?op=BlocksFile");
die;
} else 
die("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksFile\"', 10);</script>");

}
}


switch($op) {

case "BlocksAdmin": BlocksAdmin();
break;

case "BlocksNew": BlocksNew();
break;

case "BlocksFile": BlocksFile();
break;

case "BlocksFileEdit": BlocksFileEdit();
break;

case "BlocksAdd": BlocksAdd($title, $bposition, $active, $blockfile, $view, $expire, $action);
break;

case "BlocksEdit": BlocksEdit($bid);
break;

case "Blockschecks": Blockschecks($bid);
break;

case "BlocksEditSave": BlocksEditSave($newexpire, $bid, $title, $oldposition, $bposition, $active, $weight, $blockfile, $view, $expire, $action, $mobile_view);
break;

case "BlocksChange": BlocksChange($bid, $ok, $de);
break;

case "BlocksDelete": {

$bid = (isset($_GET['bid']) ? intval($_GET['bid']):0);

if (!isset($_GET["sure"])){

$res = sql_query("SELECT title, blockfile FROM orbital_blocks WHERE bid = ".sqlesc($bid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) <> 0) {

$arr = mysql_fetch_array($res);

stdmsg($tracker_lang['warning'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], "<b>".$arr['title']."</b> (".$arr['blockfile'].")", "admincp.php?op=BlocksDelete&bid=".$bid."&sure"));
stdfoot();
die;
}

}

list ($bposition, $weight) = mysql_fetch_row(sql_query("SELECT bposition, weight FROM orbital_blocks WHERE bid = ".sqlesc($bid)));
$result = sql_query("SELECT bid FROM orbital_blocks WHERE weight > ".sqlesc($weight)." AND bposition = ".sqlesc($bposition)) or sqlerr(__FILE__, __LINE__);

while (list($nbid) = mysql_fetch_row($result)) {
sql_query("UPDATE orbital_blocks SET weight = ".sqlesc($weight)." WHERE bid = ".sqlesc($bid)) or sqlerr(__FILE__, __LINE__);
++$weight;
}

sql_query("DELETE FROM orbital_blocks WHERE bid = ".sqlesc($bid));

unsql_cache("blocks_all");

if (!headers_sent())
header("Location: admincp.php?op=BlocksAdmin");
else
echo ("<script>setTimeout('document.location.href=\"admincp.php?op=BlocksAdmin\"', 10);</script>");
die;

}
break;

case "BlocksOrder": BlocksOrder($weightrep, $weight, $bidrep, $bidori);
break;

case "BlocksFixweight": BlocksFixweight();
break;

case "BlocksShow": BlocksShow($bid);
break;

case "BlocksChecksAll": BlocksChecksAll($bid);
break;

case "BlocksbfEdit": BlocksbfEdit();
break;

}

}
break;

case "StatusDB": {

main_menu (true);

function StatusDB() {

global $tracker_lang;

include("include/passwords.php");

$dateba = (isset($_POST["datatable"]) ? $_POST["datatable"]:"");

$result = sql_query("SHOW TABLES FROM ".$Mysql_Config['db']);

echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\" align=\"center\">

<form method=\"post\" action=\"admincp.php\">
<tr>";

echo "<td class=\"a\"><select name=\"datatable[]\" size=\"10\" multiple=\"multiple\" style=\"width:300px\">";

while (list($name) = mysql_fetch_array($result)){

$n = @mysql_fetch_row(sql_query("SELECT COUNT(*) FROM ".$name));

echo "<option value=\"".trim($name)."\" ".(@in_array($name, $dateba) || empty($dateba) ? "SELECTED":"")." >".$name." (".number_format($n[0]).")</option>";
}

echo "</select><br /><i>".$tracker_lang['may_selecttable']."</i></td>";

echo "<td clas=\"b\">";

echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";

echo "<tr>
<td class=\"a\" valign=\"top\"><input type=\"radio\" name=\"type\" value=\"Optimize\" checked></td>
<td class=\"b\">".$tracker_lang['mysql_optimize']."<br /><font class=\"small\">".$tracker_lang['optim_help']."</font></td>
</tr>";

echo "<tr>
<td class=\"a\" valign=\"top\"><input type=\"radio\" name=\"type\" value=\"Repair\"></td>
<td class=\"b\">".$tracker_lang['mysql_repair']."<br /><font class=\"small\">".$tracker_lang['repair_help']."</font></td>
</tr>";

echo "<tr>
<td class=\"a\" valign=\"top\"><input type=\"radio\" name=\"type\" value=\"Enable_keys\"></td>
<td class=\"b\">".$tracker_lang['mysql_enable_key']."<br /><font class=\"small\">".$tracker_lang['enable_key_help'].".</font></td>
</tr>";

echo "</table></td></tr>";

echo "<input type=\"hidden\" name=\"op\" value=\"StatusDB\">
<tr>
<td colspan=\"2\" align=\"center\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['b_action']."\" /></td>
</tr>
</form>";


if (!in_array($_POST['type'], array("Optimize", "Repair" , "Enable_keys"))){
$res = sql_query("SELECT * FROM avps") or sqlerr(__FILE__, __LINE__);

echo "<tr><td colspan=\"3\" align=\"center\" class=\"a\"><b>".$tracker_lang['last_system']."</b></td></tr>";

echo "<tr>
<td align=\"left\" class=\"colhead\">#</td>
<td align=\"left\" class=\"colhead\">".$tracker_lang['description']."</td>
<td align=\"left\" class=\"colhead\">".$tracker_lang['clock']."</td>
</tr>";

$num = 0;
while ($r = mysql_fetch_array($res)) {

echo "<tr>
<td align=\"left\" ".($num%2 == 1 ? "class=\"a\"":"class=\"b\"").">".$r["arg"]."</td>
<td align=\"left\" ".($num%2 == 1 ? "class=\"b\"":"class=\"a\"").">".(!empty($r["value_s"]) ? $tracker_lang['more'].": ".$r["value_s"]."<br />":"")."
".$tracker_lang['number_all'].": ".$r["value_i"]."</td>
<td align=\"center\" ".($num%2 == 1 ? "class=\"a\"":"class=\"b\"")."><nobr>".$r["value_u"]."<br />".display_date_time($r["value_u"])."</nobr></td>
</tr>";
++$num;
}

echo "</table><br />";
}

if ($_POST['type'] == "Optimize") {

echo "<br />";

echo "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" width=\"100%\">
<tr>
<td class=\"b\" align=\"center\" colspan=\"4\">".$tracker_lang['mysql_optimize'].": ".$Mysql_Config['db']."</td>
</tr>
<tr>
<td class=\"colhead\" align=\"center\">".$tracker_lang['table']." (".$tracker_lang['size'].")</td>
<td class=\"colhead\" width=\"20%\" align=\"center\">".$tracker_lang['status']."</td>
<td class=\"colhead\" width=\"30%\" align=\"center\">".$tracker_lang['overhead']."</td>
</tr>";

$i = 0;
$result = sql_query("SHOW TABLE STATUS FROM ".$Mysql_Config['db']) or sqlerr(__FILE__, __LINE__);

while ($row = mysql_fetch_array($result)) {

if (in_array($row[0],$dateba)){
if ($i%2 == 1){
$clastd1 = "class=\"b\"";
$clastd2 = "class=\"a\"";
}else{
$clastd2 = "class=\"b\"";
$clastd1 = "class=\"a\"";
}

$total = $row['Data_length'] + $row['Index_length'];
$totaltotal += $total;
$free = ($row['Data_free']) ? $row['Data_free'] : 0;
$totalfree += $free;


sql_query("OPTIMIZE TABLE ".$row[0]) or sqlerr(__FILE__, __LINE__);

echo "<tr>
<td ".$clastd1."><b>".$row[0]."</b> (".mksize($total).")</td>
<td ".$clastd2." align=\"center\">".(empty($free) ? "<font color=\"#FF0000\">".$tracker_lang['mysql_noneedopt']."</font>" : "<font color=\"#009900\">".$tracker_lang['mysql_optimizing']."</font>")."</td>
<td ".$clastd1." align=\"center\">".mksize($free)."</td>
</tr>";

++$i;
}
}

echo "<tr><td class=\"b\" align=\"center\" colspan=\"4\">".$tracker_lang['mysql_optimize'].": ".$Mysql_Config['db']."<br />".$tracker_lang['all_size'].": ".mksize($totaltotal)."<br />".$tracker_lang['mysql_alltable'].": ".$i."<br />".$tracker_lang['overhead'].": ".mksize($totalfree)."</td></tr>";

echo "</table>";

} elseif ($_POST['type'] == "Repair") {

echo "<br />";

echo "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" width=\"100%\">
<tr>
<td class=\"b\" align=\"center\" colspan=\"4\">".$tracker_lang['mysql_repair'].": ".$Mysql_Config['db']."</td>
</tr>
<tr>
<td class=\"colhead\">".$tracker_lang['table']." (".$tracker_lang['size'].")</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['status']."</td>
</tr>";

$result = sql_query("SHOW TABLE STATUS FROM ".$Mysql_Config['db']);

while ($row = mysql_fetch_array($result)) {

if (in_array($row[0], $dateba)){
$total = $row['Data_length'] + $row['Index_length'];
$totaltotal += $total;

if ($i%2 == 1){
$clastd1 = "class=\"b\"";
$clastd2 = "class=\"a\"";
}else{
$clastd2 = "class=\"b\"";
$clastd1 = "class=\"a\"";
}

$rre1 = sql_query("REPAIR TABLE `".$row[0]."` EXTENDED") or sqlerr(__FILE__, __LINE__);
$rre2 = sql_query("REPAIR TABLE `".$row[0]."` USE_FRM") or sqlerr(__FILE__, __LINE__);

$rresult = $rre1+$rre2;

echo "<tr>
<td ".$clastd1."><b>".$row[0]."</b> (".mksize($total).")</td>
<td ".$clastd2." align=\"center\">".(empty($rresult) ? "<font color=\"#FF0000\">".$tracker_lang['err_misfix']."</font>" : "<font color=\"#009900\">".$tracker_lang['suc_noerrors']."</font>")."</td>
</tr>";

++$i;
}
}

echo "<tr><td class=\"b\" align=\"center\" colspan=\"4\">".$tracker_lang['mysql_repair'].": ".$Mysql_Config['db']."<br /><strong>".$tracker_lang['all_size']."</strong>: ".mksize($totaltotal)."</td></tr>";

echo "</table>";

} elseif ($_POST['type'] == "Enable_keys") {

echo "<br />";

echo "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" width=\"100%\">
<tr>
<td class=\"b\" align=\"center\" colspan=\"4\">".$tracker_lang['mysql_nenablekey'].": ".$Mysql_Config['db']."</td>
</tr>
<tr>
<td class=\"colhead\">".$tracker_lang['table']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['status']."</td>
</tr>";

$result = sql_query("SHOW TABLE STATUS FROM ".$Mysql_Config['db']);

while ($row = mysql_fetch_array($result)) {

if (in_array($row[0], $dateba)){

$total = $row['Data_length'] + $row['Index_length'];
$totaltotal += $total;

if ($i%2 == 1){
$clastd1 = "class=\"b\"";
$clastd2 = "class=\"a\"";
}else{
$clastd2 = "class=\"b\"";
$clastd1 = "class=\"a\"";
}

$rresult = sql_query("ALTER TABLE ".$row[0]." ENABLE KEYS") or sqlerr(__FILE__, __LINE__);

$result2 = sql_query("SHOW TABLE STATUS LIKE '".$row[0]."'");
$row2 = mysql_fetch_array($result2);
$total2 = $row2['Data_length'] + $row2['Index_length'];

echo "<tr>
<td ".$clastd1."><b>".$row[0]."</b></td>
<td ".$clastd2." align=\"center\">".($total2 <> $total ? "<font color=\"#009900\">".$tracker_lang['enable_suc_size'].": ".mksize($total)." / ".mksize($total2)."</font>" : "<font color=\"#FF0000\">".$tracker_lang['success'].". ".$tracker_lang['size'].": ".mksize($total)."</font>")."</td>
</tr>";

++$i;
}

}

echo "<tr><td class=\"b\" align=\"center\" colspan=\"4\">".$tracker_lang['all_size'].": ".mksize($totaltotal)."</td></tr>";
echo "</table>";
}

unset($Mysql_Config);

}

switch ($op) {
case "StatusDB":
StatusDB();
break;
}

}
break;

case "iUsers": {

global $CURUSER;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

/// ���� ������ ������ ///
if ($teslalue == 'pass'){

$updateset = $whereset = array();


if (!empty($ipass)) {
$secret = mksecret();
$hash = md5($secret.$ipass.$secret);
$updateset[] = "secret = ".sqlesc($secret);
$updateset[] = "passhash = ".sqlesc($hash);
}


if (!empty($iname) && validusername($iname))
$whereset[] = "username = ".sqlesc($iname);

if (!empty($idname) && is_valid_id($idname))
$whereset[] = "id = ".sqlesc($idname);

if (!empty($imail) && validemail($imail))
$updateset[] = "email = ".sqlesc($imail);


if (count($updateset) && count($whereset)) {

$res2 = sql_query("SELECT username, class, id, override_class FROM users WHERE ".implode(" OR ", $whereset)." LIMIT 1") or sqlerr(__FILE__, __LINE__);
$s = mysql_fetch_assoc($res2);

if ($CURUSER["id"] <> $s["id"] && !empty($idname))
$true = true;
elseif ($CURUSER["username"] <> $s["username"] && !empty($iname))
$true = true;
else
$true = false;

$username = "<a href=\"userdetails.php?id=".$s["id"]."\">".get_user_class_color(($s["override_class"] <> "255" ? $s["class"] : $s["override_class"]), $s["username"])."</a>";

if (($s["class"] >= get_user_class() && $s["override_class"] == "255") || ($s["override_class"] <> "255" && $s["override_class"] >= get_user_class())){

if ($true == true)
stderr($tracker_lang['error'], $tracker_lang['class_hidefault'].": ".$username."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}

$updateset[] = "modcomment = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� ������ (��� - ".htmlspecialchars($CURUSER["username"]).")" . "\n").", modcomment)";

sql_query("UPDATE users SET ".implode(", ", $updateset)." WHERE ".implode(" OR ", $whereset)." LIMIT 1") or sqlerr(__FILE__,__LINE__);

if ($true == false)
logincookie($CURUSER["id"], $hash, $CURUSER["shelter"], 1); // �������� �� ����

}

main_menu (true);

if (mysql_modified_rows() < 1)
stdmsg($tracker_lang['error'], (empty($iname) && empty($idname) ? $tracker_lang['invalid_id_value']:" ".$tracker_lang['user_noexis_in']."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>")."", "error");
else
stdmsg($tracker_lang['succ_edit_user'], "
".$tracker_lang['username'].": ".$username."<br />
".$tracker_lang['account_id'].": ".$s["id"]."<br />
".$tracker_lang['new_password'].": ".$ipass."<br />
".(!empty($imail) ? $tracker_lang['new_email'].": ".$imail."<br />" : "")."<a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}
/// ���� ������ ������ ///


/// ���� ������ id ///
elseif ($teslalue == 'updid'){

if (empty($likeid) || empty($haveid) || !is_valid_id($likeid) || !is_valid_id($haveid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

$res2 = sql_query("SELECT username, id, class, override_class, avatar FROM users WHERE id = ".sqlesc($haveid)) or sqlerr(__FILE__,__LINE__);

$s2 = mysql_fetch_assoc($res2);

if (($s2["class"] >= get_user_class() && $s2["override_class"] == "255") || ($s2["override_class"] <> "255" && $s2["override_class"] >= get_user_class())){

if ($CURUSER["id"] <> $s2["id"])
stderr($tracker_lang['error_data'], $tracker_lang['class_hidefault'].": <a href=\"userdetails.php?id=".$s["id"]."\">".get_user_class_color(($s2["override_class"] <> "255" ? $s2["class"] : $s2["override_class"]), $s2["username"])."</a><br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}

if (mysql_num_rows($res2) > 0)
sql_query("UPDATE users SET id = ".sqlesc($likeid)." WHERE id = ".sqlesc($haveid));
else
stderr($tracker_lang['error_data'], $tracker_lang['user_is_noexis'].": <b>".$haveid."</b><br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

if (mysql_errno() == 1062)
stderr($tracker_lang['error'], $tracker_lang['user_is_exis'].": <b>".$likeid."</b><br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");


if (!empty($s2["avatar"])){
$new_name = str_replace($haveid, $likeid, $s2["avatar"]);
$true = rename(ROOT_PATH."/pic/avatar/".$s2["avatar"], ROOT_PATH."/pic/avatar/".$new_name);

sql_query("UPDATE users SET ".($true == false ? "avatar = ".sqlesc($s2["avatar"]):"avatar = ".sqlesc($new_name))." WHERE id = ".sqlesc($likeid)) or sqlerr(__FILE__, __LINE__);
}

sql_query("UPDATE users SET modcomment = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� id (".$haveid." => ".$likeid.")" . "\n").", modcomment) WHERE id = ".sqlesc($likeid));

sql_query("UPDATE torrents SET moderatedby = ".sqlesc($likeid)." WHERE moderatedby = ".sqlesc($haveid));
sql_query("UPDATE bookmarks SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE checkcomm SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE friends SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE friends SET friendid = ".sqlesc($likeid)." WHERE friendid = ".sqlesc($haveid));
sql_query("UPDATE invites SET inviter = ".sqlesc($likeid)." WHERE inviter = ".sqlesc($haveid));
sql_query("UPDATE messages SET sender = ".sqlesc($likeid)." WHERE sender = ".sqlesc($haveid));
sql_query("UPDATE messages SET receiver = ".sqlesc($likeid)." WHERE receiver = ".sqlesc($haveid));
sql_query("UPDATE messages SET poster = ".sqlesc($likeid)." WHERE poster = ".sqlesc($haveid));
sql_query("UPDATE news SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE peers SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE pollanswers SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE comments SET user = ".sqlesc($likeid)." WHERE user = ".sqlesc($haveid));
sql_query("UPDATE polls SET createby = ".sqlesc($likeid)." WHERE createby = ".sqlesc($haveid));
sql_query("UPDATE ratings SET user = ".sqlesc($likeid)." WHERE user = ".sqlesc($haveid));
sql_query("UPDATE report SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE sessions SET uid = ".sqlesc($likeid)." WHERE uid = ".sqlesc($haveid));
sql_query("UPDATE shoutbox SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE simpaty SET touserid = ".sqlesc($likeid)." WHERE touserid = ".sqlesc($haveid));
sql_query("UPDATE simpaty SET fromuserid = ".sqlesc($likeid)." WHERE fromuserid = ".sqlesc($haveid));
sql_query("UPDATE snatched SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE thanks SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE thanks SET touid = ".sqlesc($likeid)." WHERE touid = ".sqlesc($haveid));
sql_query("UPDATE torrents SET owner = ".sqlesc($likeid)." WHERE owner = ".sqlesc($haveid));
sql_query("UPDATE uploadapp SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE posts SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE posts SET editedby = ".sqlesc($likeid)." WHERE editedby = ".sqlesc($haveid));

sql_query("UPDATE download SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE attachments SET uploadby = ".sqlesc($likeid)." WHERE uploadby = ".sqlesc($haveid));
sql_query("UPDATE loginattempts SET comment = REPLACE(comment , ',".$haveid.",', ',".$likeid.",')");

sql_query("UPDATE my_match SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));
sql_query("UPDATE my_search SET userid = ".sqlesc($likeid)." WHERE userid = ".sqlesc($haveid));

unsql_cache("arrid_".$likeid);
unsql_cache("arrid_".$haveid);

if ($CURUSER["id"] == $likeid)
logincookie($likeid, $CURUSER["passhash"], $CURUSER["shelter"], 1); // �������� �� ����

stderr($tracker_lang['succ_edit_user'], sprintf($tracker_lang['change_id_for'], "<strong>".$haveid."</strong>", "<strong>".$likeid."</strong>", "<a href=\"userdetails.php?id=".$likeid."\">".get_user_class_color($s2["class"], $s2["username"])."</a>")."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}
/// ���� ������ id ///


/// ���� ������ ����� ///
elseif ($teslalue == 'login'){

$updateset = $whereset = array();

if (!empty($username) && validusername($username))
$updateset[] = "username = ".sqlesc($username);

if (!empty($oldname) && validusername($oldname))
$whereset[] = "username = ".sqlesc($oldname);

if (!empty($oldid) && is_valid_id($oldid))
$whereset[] = "id = ".sqlesc($oldid);

if (count($updateset) && count($whereset)) {

$res3 = sql_query("SELECT username, class, id, override_class FROM users WHERE ".implode(" OR ", $whereset)." LIMIT 1") or sqlerr(__FILE__, __LINE__);
$s3 = mysql_fetch_assoc($res3);

if ($CURUSER["id"] <> $s3["id"] && !empty($oldid))
$true = true;
elseif ($CURUSER["username"] <> $s3["username"] && !empty($oldname))
$true = true;
else
$true = false;

if (mysql_num_rows($res3) == 0)
stderr($tracker_lang['error'], (!empty($oldname) ? sprintf($tracker_lang['no_user_isname'], "<strong>".$oldname."</strong>") : sprintf($tracker_lang['no_user_isname'], "<strong>".$oldid."</strong>"))." ".$tracker_lang['invalid_id_value']."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

if (($s3["class"] >= get_user_class() && $s3["override_class"] == "255") || ($s3["override_class"] <> "255" && $s3["override_class"] >= get_user_class())){

if ($true == true)
stderr($tracker_lang['error'], $tracker_lang['class_hidefault'].": <a href=\"userdetails.php?id=".$s3["id"]."\">".get_user_class_color(($s3["override_class"] <> "255" ? $s3["class"] : $s3["override_class"]), $s3["username"])."</a><br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}

$updateset[] = "modcomment = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� ����� (".htmlspecialchars($s3["username"])." => ".htmlspecialchars($username).")" . "\n").", modcomment)";

sql_query("UPDATE users SET ".implode(", ", $updateset)." WHERE ".implode(" OR ", $whereset)." LIMIT 1") or sqlerr(__FILE__,__LINE__);

if (mysql_errno() == 1062)
stderr($tracker_lang['error'], sprintf($tracker_lang['user_is_signup'], "<b>".$oldname."</b>")."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

main_menu (true);

if (mysql_modified_rows())
stderr($tracker_lang['succ_edit_user'], "
".$tracker_lang['username'].": <a href=\"userdetails.php?id=".$s3["id"]."\">".get_user_class_color(($s3["override_class"] <> "255" ? $s3["class"] : $s3["override_class"]), $username)."</a><br />
".$tracker_lang['account_id'].": ".$s3["id"]."<br />
".$tracker_lang['old_username'].": ".$s3["username"]."<br />
<a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");
else
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']." <br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

} else
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']." <br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}
/// ���� ������ ����� ///


/// ���� ������ ��������� ///
elseif ($teslalue == 'owner'){

$updateset = $whereset = $whereset2 = $update = array();

if (!empty($username) && validusername($username))
$updateset[] = "username = ".sqlesc($username);

if (!empty($newname) && validusername($newname))
$whereset[] = "username = ".sqlesc($newname);

if (!empty($newid) && is_valid_id($newid))
$whereset[] = "id = ".sqlesc($newid);

if (!empty($oldname) || !empty($oldid)){

if (!empty($oldname) && validusername($oldname))
$whereset2[] = "username = ".sqlesc($oldname);

if (!empty($oldid) && is_valid_id($oldid))
$whereset2[] = "id = ".sqlesc($oldid);

if (count($whereset2))
$res4 = sql_query("SELECT id, username FROM users WHERE ".implode(" OR ", $whereset2)." LIMIT 1") or sqlerr(__FILE__,__LINE__);

$s4 = mysql_fetch_assoc($res4);

if (empty($oldid) && !empty($s4["id"]) || $oldid <> $s4["id"])
$oldid = $s4["id"];

$owner_name = (!empty($s4["username"]) ? $s4["username"]:"id: ".$oldid);

}


if (count($whereset))
$res3 = sql_query("SELECT id, username FROM users WHERE ".implode(" OR ", $whereset)." LIMIT 1") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res3) == 0)
stderr($tracker_lang['error_data'], (empty($newid) ? sprintf($tracker_lang['no_user_isid'], "<b>".$newid."</b>"):printf($tracker_lang['no_user_isname'], "<b>".$newname."</b>"))."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

$s3 = mysql_fetch_assoc($res3);

if (empty($newid) && !empty($s3["id"]) || $newid <> $s3["id"])
$newid = $s3["id"];

$owner_new = (!empty($s3["username"]) ? $s3["username"]:"id: ".$newid);

$update[] = "owner = ".sqlesc($newid);

$update[] = "torrent_com = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� ��������, ��� ".$CURUSER["username"]." (".htmlspecialchars($owner_name)." => ".htmlspecialchars($owner_new).")" . "\n").", torrent_com)";

sql_query("UPDATE torrents SET ".implode(", ", $update)." WHERE owner = ".sqlesc($oldid)) or sqlerr(__FILE__,__LINE__);

$count = mysql_modified_rows();

if (!$count)
stderr($tracker_lang['error'], $tracker_lang['no_torrents']."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");
else {

sql_query("UPDATE users SET modcomment = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� ��������, ��� ".$CURUSER["username"]." (".htmlspecialchars($owner_name)." => ".htmlspecialchars($owner_new).", �����: ".$count.")" . "\n").", modcomment) WHERE id = ".sqlesc($oldid));

unsql_cache("arrid_".$oldid);

stderr($tracker_lang['succ_edit_user'], sprintf($tracker_lang['new_old_now_owner'], $count, $owner_name, $owner_new)."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}

}
/// ���� ������ ��������� ///


/// ���� ������ ������������ ///
elseif ($teslalue == 'moderatedby'){

$updateset = $whereset = $whereset2 = $update = array();

if (!empty($username) && validusername($username))
$updateset[] = "username = ".sqlesc($username);

if (!empty($newname) && validusername($newname))
$whereset[] = "username = ".sqlesc($newname);

if (!empty($newid) && is_valid_id($newid))
$whereset[] = "id = ".sqlesc($newid);

if (!empty($oldname) || !empty($oldid)){

if (!empty($oldname) && validusername($oldname))
$whereset2[] = "username = ".sqlesc($oldname);

if (!empty($oldid) && is_valid_id($oldid))
$whereset2[] = "id = ".sqlesc($oldid);

if (count($whereset2))
$res4 = sql_query("SELECT id, username FROM users WHERE ".implode(" OR ", $whereset2)." LIMIT 1") or sqlerr(__FILE__,__LINE__);

$s4 = mysql_fetch_assoc($res4);

if (empty($oldid) && !empty($s4["id"]) || $oldid <> $s4["id"])
$oldid = $s4["id"];

$owner_name = (!empty($s4["username"]) ? $s4["username"]:"id: ".$oldid);

}

if (count($whereset))
$res3 = sql_query("SELECT id, username FROM users WHERE ".implode(" OR ", $whereset)." LIMIT 1") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res3) == 0)
stderr($tracker_lang['error_data'], (empty($newid) ? sprintf($tracker_lang['no_user_isname'], "<strong>".$newname."</strong>") : sprintf($tracker_lang['no_user_isname'], "<strong>".$newid."</strong>"))."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

$s3 = mysql_fetch_assoc($res3);

if (empty($newid) && !empty($s3["id"]) || $newid <> $s3["id"])
$newid = $s3["id"];

$owner_new = (!empty($s3["username"]) ? $s3["username"]:"id: ".$newid);

$update[] = "moderatedby = ".sqlesc($newid);
$update[] = "moderatordate = ".sqlesc(get_date_time());
$update[] = "torrent_com = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� ��������, ������� �������� ������, ��� ".$CURUSER["username"]." (".htmlspecialchars($owner_name)." => ".htmlspecialchars($owner_new).")" . "\n").", torrent_com)";

sql_query("UPDATE torrents SET ".implode(", ", $update)." WHERE moderatedby = ".sqlesc($oldid)) or sqlerr(__FILE__,__LINE__);

$count = mysql_modified_rows();

if (!$count)
stderr($tracker_lang['error'], $tracker_lang['no_torrents']."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");
else {

sql_query("UPDATE users SET modcomment = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ������� ��������, ������� �������� ������, ��� ".$CURUSER["username"]." (".htmlspecialchars($owner_name)." => ".htmlspecialchars($owner_new).", �����: ".$count.")" . "\n").", modcomment) WHERE id = ".sqlesc($oldid));

unsql_cache("arrid_".$oldid);

stderr($tracker_lang['succ_edit_user'], sprintf($tracker_lang['new_old_now_mod'], $count, $owner_name, $owner_new)."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}

}
/// ���� ������ ������������ ///


/// ���� ��������� ���������� ///
if ($teslalue == 'mdbyme'){

$updateset = $whereset = $update_ = array();


if (!empty($iname) && validusername($iname))
$whereset[] = "username = ".sqlesc($iname);

if (!empty($idname) && is_valid_id($idname))
$whereset[] = "id = ".sqlesc($idname);

if (count($whereset)) {

$res2 = sql_query("SELECT username, class, id FROM users WHERE ".implode(" OR ", $whereset)." LIMIT 1") or sqlerr(__FILE__, __LINE__);
$s = mysql_fetch_assoc($res2);

if (mysql_num_rows($res2) == 0)
stderr($tracker_lang['error_data'], (empty($idname) ? sprintf($tracker_lang['no_user_isid'], $idname) : sprintf($tracker_lang['no_user_isname'], $iname))."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

$username = "<a href=\"userdetails.php?id=".$s["id"]."\">".get_user_class_color(($s["override_class"] <> "255" ? $s["class"] : $s["override_class"]), $s["username"])."</a>";

if ($s["class"] > UC_UPLOADER)
stderr($tracker_lang['error'], $tracker_lang['is_staff_ov'].": ".$username."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

$update_[] = "moderatedby = ".sqlesc($s["id"]);
$update_[] = "torrent_com = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " ".$CURUSER["username"]." ��� ��������� ���������� (�.�.) �����.\n").", torrent_com)";

sql_query("UPDATE torrents SET ".implode(", ", $update_)." WHERE owner = ".sqlesc($s["id"])." AND moderatedby <> ".sqlesc($s["id"])) or sqlerr(__FILE__,__LINE__);

$count = mysql_modified_rows();

$updateset[] = "modcomment = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ��� ��������� ���������� (�.�.) ������ (��� - ".htmlspecialchars($CURUSER["username"]).", �����: )" . "\n").", modcomment)";

sql_query("UPDATE users SET ".implode(", ", $updateset)." WHERE ".implode(" OR ", $whereset)." LIMIT 1") or sqlerr(__FILE__,__LINE__);

}

main_menu (true);

if (!$count)
stdmsg($tracker_lang['error'], $tracker_lang['no_torrents']."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>", "error");
else
stdmsg($tracker_lang['succ_edit_user'], sprintf($tracker_lang['owner_ov_of'], $username, $count)."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");

}
/// ���� ��������� ���������� ///

else
stderr($tracker_lang['error'], $tracker_lang['access_denied']."<br /><a href=\"admincp.php?op=iUsers\"><b>".$tracker_lang['back_inlink']."</b></a>");


} else {

main_menu (true);


echo "<form method=\"post\" action=\"admincp.php?op=iUsers\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['change_password']."</td></tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['account']."</b></td><td><b>".$tracker_lang['signup_username']."</b>: <input name=\"iname\" type=\"text\"> <i>".$tracker_lang['enter_login_or_empty']."</i><br /><b>".$tracker_lang['account_id']."</b>: <input name=\"idname\" type=\"text\"> <i>".$tracker_lang['enter_id_or_up']."</i><br /><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['first_main_second']."</td>
</tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['new_password']."</b></td><td><input name=\"ipass\" type=\"password\"></td>
</tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['new_email']."</b></td><td class=\"b\"><input name=\"imail\" type=\"text\"> <i>".$tracker_lang['no_chang_email']."</i></td>
</tr>
<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input class=\"btn\" type=\"submit\" name=\"isub\" value=\"".$tracker_lang['change_password']."\"></td></tr>
</table>
<input type=\"hidden\" name=\"op\" value=\"iUsers\" />
<input type=\"hidden\" name=\"teslalue\" value=\"pass\" />
</form><br />";


echo "<form method=\"post\" action=\"admincp.php?op=iUsers\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['rename_username']."</td></tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['account']."</b></td><td><b>".$tracker_lang['signup_username']."</b>: <input name=\"oldname\" type=\"text\"> <i>".$tracker_lang['enter_login_or_empty']."</i><br /><b>".$tracker_lang['account_id']."</b>: <input name=\"oldid\" type=\"text\"> <i>".$tracker_lang['enter_id_or_up']."</i><br /><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['first_main_second']."</td>
</tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['username_change_is']."</b></td><td><input name=\"username\" type=\"text\"> <i>".$tracker_lang['enter_new_name']."</i></td>
</tr>

<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input class=\"btn\" type=\"submit\" name=\"isub\" value=\"".$tracker_lang['rename_username']."\"></td></tr>
</table>
<input type=\"hidden\" name=\"op\" value=\"iUsers\" />
<input type=\"hidden\" name=\"teslalue\" value=\"login\" />
</form><br />";



echo "<form method=\"post\" action=\"admincp.php?op=iUsers\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['reid_username']."</td></tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['id_user_new']."</b></td><td><input name=\"haveid\" size=\"10\" type=\"text\"> <i>".$tracker_lang['enter_old_id']."</i></td>
</tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['isset_userid']."</b></td><td><input name=\"likeid\" size=\"10\" type=\"text\"> <i>".$tracker_lang['enter_new_id']."</i></td>
</tr>
<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input class=\"btn\" type=\"submit\" name=\"isub\" value=\"".$tracker_lang['reid_username']."\"></td></tr>
</table>
<input type=\"hidden\" name=\"op\" value=\"iUsers\" />
<input type=\"hidden\" name=\"teslalue\" value=\"updid\" />
</form><br />";



echo "<form method=\"post\" action=\"admincp.php?op=iUsers\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['rename_owner']."</td></tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['owner_releases']."</b></td><td><b>".$tracker_lang['signup_username']."</b>: <input name=\"oldname\" type=\"text\"> <i>".$tracker_lang['enter_login_or_empty']."</i><br /><b>".$tracker_lang['account_id']."</b>: <input name=\"oldid\" type=\"text\"> <i>".$tracker_lang['enter_id_or_up']."</i><br /><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['first_main_second']."</td>
</tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['is_new_owner']."</b></td><td><b>".$tracker_lang['signup_username']."</b>: <input name=\"newname\" type=\"text\"> <i>".$tracker_lang['enter_login_or_empty']."</i><br /><b>".$tracker_lang['account_id']."</b>: <input name=\"newid\" type=\"text\"> <i>".$tracker_lang['enter_id_or_up']."</i><br /><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['first_main_second']."</td>
</tr>
<tr><td class=\"a\" colspan=\"2\" align=\"center\">".$tracker_lang['warning'].": ".$tracker_lang['chang_o_v_help']."</td></tr>
<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input class=\"btn\" type=\"submit\" name=\"isub\" value=\"".$tracker_lang['rename_owner']."\"></td></tr>
</table>
<input type=\"hidden\" name=\"op\" value=\"iUsers\" />
<input type=\"hidden\" name=\"teslalue\" value=\"owner\" />
</form><br />";



echo "<form method=\"post\" action=\"admincp.php?op=iUsers\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['change_moderated']."</td></tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['owner_ov_tor']."</b></td><td><b>".$tracker_lang['signup_username']."</b>: <input name=\"oldname\" type=\"text\"> <i>".$tracker_lang['enter_login_or_empty']."</i><br /><b>".$tracker_lang['account_id']."</b>: <input name=\"oldid\" type=\"text\"> <i>".$tracker_lang['enter_id_or_up']."</i><br /><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['first_main_second']."</td>
</tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['change_is_moderate']."</b></td><td><b>".$tracker_lang['signup_username']."</b>: <input name=\"newname\" type=\"text\"> <i>".$tracker_lang['enter_login_or_empty']."</i><br /><b>".$tracker_lang['account_id']."</b>: <input name=\"newid\" type=\"text\"> <i>".$tracker_lang['enter_id_or_up']."</i><br /><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['first_main_second']."</td>
</tr>
<tr><td class=\"a\" colspan=\"2\" align=\"center\">".$tracker_lang['warning'].": ".$tracker_lang['moderate_trun_is']."</td></tr>
<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input class=\"btn\" type=\"submit\" name=\"isub\" value=\"".$tracker_lang['change_moderated']."\"></td></tr>
</table>
<input type=\"hidden\" name=\"op\" value=\"iUsers\" />
<input type=\"hidden\" name=\"teslalue\" value=\"moderatedby\" />
</form><br />";



echo "<form method=\"post\" action=\"admincp.php?op=iUsers\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['mdbyme_for_user']."</td></tr>
<tr>
<td class=\"b\"><b>".$tracker_lang['account']."</b></td><td><b>".$tracker_lang['signup_username']."</b>: <input name=\"iname\" type=\"text\"> <i>".$tracker_lang['enter_login_or_empty']."</i><br /><b>".$tracker_lang['account_id']."</b>: <input name=\"idname\" type=\"text\"> <i>".$tracker_lang['enter_id_or_up']."</i><br /><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['first_main_second']."</td>
</tr>

<tr>
<td class=\"b\" colspan=\"2\">".$tracker_lang['warning'].": ".$tracker_lang['tabs_imdbyme']."<br />".$tracker_lang['warning'].": ".$tracker_lang['mdbyme_edinfo']."</td>
</tr>

<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input class=\"btn\" type=\"submit\" name=\"isub\" value=\"".$tracker_lang['enable']." ".$tracker_lang['tabs_mdbyme']."\"></td></tr>
</table>
<input type=\"hidden\" name=\"op\" value=\"iUsers\" />
<input type=\"hidden\" name=\"teslalue\" value=\"mdbyme\" />
</form><br />";

}

}
break;

default: 
break;

}


stdfoot();

?>