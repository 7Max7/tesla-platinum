<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2013 v.Platinum
 */

global $tracker_lang;
$blocktitle = $tracker_lang['requests'];

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-detailsoff", "time" => 60*30, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

$content = "";

$res = sql_query("SELECT id, name FROM off_reqs WHERE perform = 'no' ORDER BY id DESC LIMIT 10") or sqlerr(__FILE__,__LINE__);
while ($row = mysql_fetch_assoc($res)){
$content.= "<a class=\"menu\" title=\"".$tracker_lang['detoff_nodone']."\" href=\"detailsoff.php?id=".$row["id"]."\">".$row["name"]." </a>";
}

if (mysql_num_rows($res) == 0) $content = "<center>".$tracker_lang['sum_nodata']."</center>";

$filecache = file_query($content, $cache = array("type" => "disk", "file" => "block-detailsoff", "time" => 60*30, "action" => "set"));
}

?>