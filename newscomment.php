<?
require_once("include/bittorrent.php");

dbconn(false);
loggedinorreturn();
parked();

$action = (string) $_GET["action"];

if ($CURUSER["commentpos"] == 'no')
stderr($tracker_lang['error_data'], $tracker_lang['commentpos_no']);


if ($action == "add") {

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$nid = (int) $_POST["nid"];
$text = htmlspecialchars_uni($_POST["text"]);

if (empty($text) || empty($nid))
stderr($tracker_lang['error'], $tracker_lang['comment_cant_be_empty']);

sql_query("INSERT INTO comments (user, news, added, text, ori_text, ip) VALUES (".sqlesc($CURUSER["id"]).", ".sqlesc($nid).", ".sqlesc(get_date_time()).", ".sqlesc($text).", ".sqlesc($text).", ".sqlesc(getip()).")") or sqlerr(__FILE__,__LINE__);

$newid = mysql_insert_id();

/// ������� ���
unsql_cache("block-news_admins");
unsql_cache("block-news_guest");
unsql_cache("block-news_users");
/// ������� ���

header("Refresh: 0; url=newsoverview.php?id=".$nid."&viewcomm=".$newid."#comm".$newid);
die;
}

$nid = (int) $_GET["nid"];
if (!is_valid_id($nid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['news'].": ".$tracker_lang['commenting']);

begin_frame($tracker_lang['commentpos'], true);

echo("<form name=\"comment\" method=\"post\" action=\"newscomment.php?action=add\">");
echo "<table class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";

echo "<tr><td class=\"colhead\">".$tracker_lang['add_comment']."</td></tr>";

echo "<tr><td align=\"center\">
".textbbcode("comment", "text", "")."
<input type=\"hidden\" name=\"nid\" value=\"".$nid."\"/><input type=\"submit\" name=\"post\" class=\"btn\" title=\"CTRL+ENTER ".$tracker_lang['add_comment']."\" value=\"".$tracker_lang['add_comment']."\" />
</td></tr>";

echo "</table>";
echo("</form>");

$res = sql_query("SELECT comments.id, text, comments.ip, comments.added, username, title, class, users.id as user, users.avatar, users.donor, users.enabled, users.warned, users.parked
FROM comments
LEFT JOIN users ON comments.user = users.id
WHERE news = ".sqlesc($nid)." AND torrent = '0' AND poll = '0' AND offer = '0' ORDER BY comments.id DESC") or sqlerr(__FILE__,__LINE__);

$allrows = array();
while ($row = mysql_fetch_array($res))
$allrows[] = $row;

if (count($allrows))
commenttable($allrows, "newscomment");

stdfoot();
die;

} elseif ($action == "quote") {

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT nc.*, n.id AS nid, u.username 
FROM comments AS nc 
LEFT JOIN news AS n ON nc.news = n.id 
LEFT JOIN users AS u ON nc.user = u.id 
WHERE nc.id = ".sqlesc($commentid)." AND torrent = '0' AND poll = '0' AND offer = '0'") or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);

if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['news'].": ".$tracker_lang['commenting']);

$text = "[quote=".$arr["username"]."]".$arr["text"]."[/quote]\n";

echo("<form method=\"post\" name=\"comment\" action=\"newscomment.php?action=add\">");

echo "<table class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" widht=\"100%\">";

echo "<tr><td class=\"colhead\">".$tracker_lang['add_comment']."</td></tr>";

echo "<tr><td align=\"center\">".textbbcode("comment", "text", htmlspecialchars_uni($text))."
<input type=\"hidden\" name=\"nid\" value=\"".$arr["nid"]."\" />
<input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['add_comment']."\" value=\"".$tracker_lang['add_comment']."\" />
</td></tr>";

echo "</table>";

echo("</form>");

stdfoot();

} elseif ($action == "edit") {

$commentid = (int) $_GET["cid"];

if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT nc.*, n.id AS nid 
FROM comments AS nc 
LEFT JOIN news AS n ON nc.news = n.id 
WHERE nc.id = ".sqlesc($commentid)." and nc.torrent = '0' and nc.poll = '0' and nc.offer = '0'") or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if ($arr["user"] <> $CURUSER["id"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$text = htmlspecialchars_uni($_POST["text"]);
$returnto = htmlentities($_POST["returnto"]);

if (empty($text))
stderr($tracker_lang['error'], $tracker_lang['comment_cant_be_empty']);

sql_query("UPDATE comments SET text = ".sqlesc($text).", ".(empty($arr["ori_text"]) ? "ori_text = ".sqlesc($arr["text"]).", ":"")." editedat = ".sqlesc(get_date_time()).", editedby = ".sqlesc($CURUSER["id"])." WHERE id = ".sqlesc($commentid)." AND torrent = '0' AND poll = '0' AND offer = '0'") or sqlerr(__FILE__, __LINE__);

if ($returnto)
header("Location: ".$returnto);
else
header("Location: ".$DEFAULTBASEURL);
die;

}

stdhead($tracker_lang['news'].": ".$tracker_lang['editing']);

begin_frame($tracker_lang['editing'], true);

echo ("<form method=\"post\" name=\"comment\" action=\"newscomment.php?action=edit&cid=".$commentid."\">\n");
echo ("<input type=\"hidden\" name=\"returnto\" value=\"newsoverview.php?id=".$arr["nid"]."&viewcomm=".$commentid."#comm".$commentid."\" />\n");
echo ("<input type=\"hidden\" name=\"cid\" value=\"".$commentid."\" />\n");

echo "<table class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";

echo "<tr><td class=\"colhead\">".$tracker_lang['news'].": ".$tracker_lang['editing']."</td></tr>";

echo "<tr><td>".textbbcode("comment", "text", htmlspecialchars_uni($arr["text"]))."
<input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['edit']."\" value=\"".$tracker_lang['edit']."\" />
</td></tr>";

echo "</table>";

echo ("</form>");

stdfoot();
die;

} elseif ($action == "delete") {

if (get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$commentid = (int) $_GET["cid"];

if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if (!isset($_GET["sure"]))
stderr($tracker_lang['delete']." ".$tracker_lang['comment'], sprintf($tracker_lang['you_want_to_delete_x_click_here'],$tracker_lang['comment'], "newscomment.php?action=delete&cid=".$commentid."&sure=1"));

$res = sql_query("SELECT news, user, (SELECT username FROM users WHERE id = comments.user) AS classusername, (SELECT subject FROM news WHERE id = comments.news) AS name_news
FROM comments WHERE id = ".sqlesc($commentid)." AND torrent = '0' AND poll = '0' AND offer = '0'")  or sqlerr(__FILE__,__LINE__);

$arr = mysql_fetch_array($res);
if ($arr)
$nid = $arr["news"];

write_log($CURUSER["username"]." ������ ����������� ".$arr["classusername"]." (".$commentid.") � ������� '".$arr["name_poll"]."' (".$nid.")", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "comment");

sql_query("DELETE FROM comments WHERE id = ".sqlesc($commentid)." AND torrent = '0' AND poll = '0' AND offer = '0'") or sqlerr(__FILE__,__LINE__);

/// ������� ���
unsql_cache("block-news_admins");
unsql_cache("block-news_guest");
unsql_cache("block-news_users");
/// ������� ���

list($commentid) = mysql_fetch_row(sql_query("SELECT id FROM scomments WHERE news = ".sqlesc($nid)." AND torrent = '0' AND poll = '0' AND offer = '0' ORDER BY added DESC LIMIT 1"));

$returnto = "newsoverview.php?id=".$nid."&viewcomm=".$commentid."#comm".$commentid;

if ($returnto && !empty($commentid))
header("Location: ".$returnto);
else
header("Location: ".$DEFAULTBASEURL);

die;

} elseif ($action == "vieworiginal" && get_user_class() > UC_MODERATOR) {

$commentid = (int) $_GET["cid"];

if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT nc.*, n.id AS nid 
FROM comments AS nc 
LEFT JOIN news AS n ON nc.news = n.id 
WHERE nc.id = ".sqlesc($commentid)." AND nc.torrent = '0' AND nc.poll = '0' AND nc.offer = '0'") or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);

if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['no_poll_with_such_id']);

stdhead($tracker_lang['news'].": ".$tracker_lang['message_view']);

if (empty($arr["ori_text"]))
$arr["ori_text"] = $arr["text"];

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"a\">".$tracker_lang['original_com'].": ".$commentid."</td></tr>";
echo "<tr><td class=\"b\">".format_comment($arr["ori_text"],true)."</td></tr>";
echo "<tr><td class=\"a\"><a href=\"newsoverview.php?id=".$arr["nid"]."&viewcomm=".$commentid."#comm".$commentid."\">".$tracker_lang['back_inlink']."</a></td></tr>";

echo "</table>";

stdfoot();
die;

} else
stderr($tracker_lang['error'], $tracker_lang['no_data']);

?>