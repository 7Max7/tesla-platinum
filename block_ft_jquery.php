<?
require_once("include/bittorrent.php");

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) © 2013 v.Platinum
 */

dbconn(false, true);
	
header("Content-Type: text/html; charset=" .$tracker_lang['language_charset']);
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$name_light = true; /// урезать название в полях до нормального вывода?

$page = $pageo = (isset($_GET["page"]) ? (int) $_GET["page"]:1);

$all_limit = get_row_count("torrents");
list ($pagertop, $pagerbottom, $limit) = pager(30, $all_limit, 'block_ft_jquery.php?');


if ($page < 0) $page = 0;

if ($page == 0)
$limit = "LIMIT 0, 30";
else
$limit = "LIMIT ".(30*($page-1)).",30";


/*
$res_ = sql_query("SELECT t.id, t.name, t.image1, t.size, t.tags, t.kinopoisk, t.imdb, t.owner, t.comments, t.moderated, t.category, (t.times_completed+t.f_times) AS times_completed, (t.leechers+t.f_leechers) AS f_leechers, (t.seeders+t.f_seeders) AS f_seeders, users.class, users.username 
FROM torrents AS t
LEFT JOIN users ON t.owner=users.id
ORDER BY t.id DESC ".$limit) or sqlerr(__FILE__, __LINE__);
*/

$res_ = sql_query("SELECT t.id, t.name, t.image1, t.size, t.tags, t.owner, t.comments, t.moderated, t.category, (t.times_completed+t.f_times) AS times_completed, (t.leechers+t.f_leechers) AS f_leechers, (t.seeders+t.f_seeders) AS f_seeders FROM torrents AS t ORDER BY t.id DESC ".$limit, $cache = array("type" => "disk", "file" => "block-ft_p".$page, "time" => 60*10)) or sqlerr(__FILE__, __LINE__);

$content = '';


while ($row_ = mysql_fetch_assoc_($res_)){

/*
if (!empty($row_["username"]))
$owner = "<a href='userdetails.php?id=".$row_["owner"]."'>".get_user_class_color($row_["class"], $row_["username"])."</a>";
else 
$owner = "id: ".$row_["owner"];
*/

$name = ($row_["name"]);

if ($name_light == true)
$name = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", $name));

$tags = array();
if (!empty($row_["tags"])){
foreach (explode(",", $row_["tags"]) as $tag) {
$tags[] = "<a style=\"font-weight:normal;\" href=\"browse.php?tag=".urlencode($tag)."&incldead=1\">".tolower($tag)."</a>";
}
$tags = array_slice($tags, 3);
}

$online = '';
if (in_array($row_["category"], array(6, 10, 11, 12, 13, 14, 15, 18, 22))) $online = "<i title=\"видео онлайн\"></i>\n";

if (empty($row_["image1"]))
$row_["image1"] = 'default_torrent.png';


$content.= addcslashes("<div class=\"movie\" id=\"movie_".$row_["id"]."\">\n<a title=\"".$name."\" href=\"details.php?id=".$row_["id"]."\">\n  ".$online."    <img alt=\"".$name."\" class=\"cover\" src=\"thumbnail.php?image=".$row_["image1"]."&for=details\" />\n<span class=\"title\">".$name."</span>\n</a>   \n\n<span>\n".(count($tags) ? implode(", ", $tags):"")."\n</span>\n</div>\n", '"');

}

$content = addcslashes($content, '/');
$content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);
$content = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $content);


$pagerbottom = addcslashes($pagerbottom, '"/');
$pagerbottom = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $pagerbottom);
$pagerbottom = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $pagerbottom);

?>
jQuery("#movies").html("\n<div id=\"movies\">\n  <div id=\"catalog-content\">\n     \n  <? echo $content; ?>  \n  <\/div>\n  \n  <div class=\"ajax pagination\"><? echo $pagerbottom; ?><\/div>\n<\/div>")
jQuery('html,body').animate({scrollTop: jQuery("#movies").offset().top - 50}, 500);
<? die; ?>