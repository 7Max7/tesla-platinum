<?php

if (!defined('BLOCK_FILE')) {   
 Header("Location: ../index.php");   
 exit;   
}


global $tracker_lang;

$blocktitle = $tracker_lang['sticky_torrent']." (<a title=\"".$tracker_lang['click_on_sticky_list']."\" class=\"altlink_white\" href='browse.php?incldead=4'>".$tracker_lang['list']." ".$tracker_lang['tracker_torrents']."</a>)";  

?>

<script src="../js/jquery.tools.min.js"></script>

<script>
jQuery(function() {
jQuery("div#main_scrollable").scrollable({ size: 1,next: 'a.nextPromo', prev: 'a.prevPromo',interval: 10000,loop: true});
jQuery("div#news_scrollable").scrollable({ size: 3,nextPage: 'a.nextNews', prevPage: 'a.prevNews'});
});
jQuery(document).ready(function(){

jQuery("a#prevpromo").css({left: jQuery('div#main_scrollable').offset().left + 838, top:jQuery('div#main_scrollable').offset().top + 160});
jQuery("a#playmain").css({left: jQuery('div#main_scrollable').offset().left + 861, top:jQuery('div#main_scrollable').offset().top + 160});
jQuery("a#nextpromo").css({left: jQuery('div#main_scrollable').offset().left + 883, top:jQuery('div#main_scrollable').offset().top + 160});

jQuery("a#playmain").click(function(){
if(jQuery(this).attr('class') == 'play'){
jQuery(this).removeClass('play');
jQuery(this).addClass('pause');							
} else {
jQuery(this).removeClass('pause');
jQuery(this).addClass('play');
}
return false;
})
});	
jQuery(window).resize(function(){
jQuery("a#prevpromo").css({left: jQuery('div#main_scrollable').offset().left + 838, top:jQuery('div#main_scrollable').offset().top + 24});
jQuery("a#playmain").css({left: jQuery('div#main_scrollable').offset().left + 861, top:jQuery('div#main_scrollable').offset().top + 24});
jQuery("a#nextpromo").css({left: jQuery('div#main_scrollable').offset().left + 883, top:jQuery('div#main_scrollable').offset().top + 24});
});
</script>
<?


$content .= "<div id=\"content\">
<a id=\"prevpromo\" class=\"prevPromo\"></a>			 				 
<div id=\"main_scrollable\">
<div class=\"items\">";

$res = sql_query("SELECT * FROM torrents WHERE sticky = 'yes' ORDER BY added DESC LIMIT 13", $cache = array("type" => "disk", "file" => "block_sticky", "time" => 60*60*24)) or sqlerr(__FILE__,__LINE__);
$num = 0;
while ($row = mysql_fetch_assoc_($res)) {


if (!empty($num))
$content .= "<div class=\"item white\">";
else
$content .= "<div class=\"item white2\">";


$combody_f = htmlspecialchars(preg_replace("/\[((\s|.)+?)\]/is", "", ($row["descr"])));

$pos1 = strpos($combody_f, '.', 400); // $pos = 7, not 0
if (strlen($combody_f) >= 455)
$combody_f = substr($combody_f,0, ($pos1+1));


$image1 = $row["image1"];

if (empty($image1))
$image1 = 'default_torrent.png';

$content .= "<div class=\"image\">";
if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image1))
$content.="<a href=details.php?id=".$row["id"]."><img src=\"".$image1."\" width=\"250px\" alt=\"".htmlspecialchars($row["name"])."\" /></a>";
else
$content.="<a href=details.php?id=".$row["id"]."><img src='thumbnail.php?image=".$image1."&amp;for=block' width=\"250px\" alt=\"".htmlspecialchars($row["name"])."\" /></a>";
$content .= "</div>";

$content .= "<div class=\"context\"><b>".htmlspecialchars_uni($row["name"])."</b><div class=\"text\">".$combody_f."</div>";
$content .= "<div class=\"detail\"><a href=\"download.php?id=".$row["id"]."\">".$tracker_lang['download']." ".$tracker_lang['name_torrent']."</a> / <a href=\"details.php?id=".$row["id"]."\">".$tracker_lang['show_data']."</a></div>";

$content .= "</div></div>";

++$num;
}


$content .= "</div></div>
<a id=\"playmain\" class=\"play\" style=\"left: 906px; top: 157px;\"></a>
<a id=\"nextpromo\" class=\"nextPromo disabled\" style=\"left: 928px; top: 157px;\"></a>
<div class=\"clearboth\">
</div></div>";



?>