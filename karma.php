<?
require_once("include/bittorrent.php");
dbconn();
header("Content-Type: text/html; charset=" . $tracker_lang['language_charset']);


if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && $_SERVER["REQUEST_METHOD"] == 'POST' && $CURUSER) {

$id = (int) $_POST['id'];
$act = (string) $_POST["act"];
$type = (string) $_POST["type"];

if (empty($id) || empty($act) || empty($type) || !is_valid_id($id) || !in_array($type, array("torrent", "comment", "user","humor")))
die($tracker_lang['invalid_id_value']);

$canrate = get_row_count("karma", "WHERE type = ".sqlesc($type)." AND value = ".sqlesc($id)." AND user = ".sqlesc($CURUSER['id']));

if ($canrate > 0){
unset($act);
$show = true;
}

if ($type == "torrent")
$table = "torrents";
elseif ($type == "comment")
$table = "comments";
elseif ($type == "humor")
$table = "humor";
else
$table = "users";

if ($act == 'plus'){

sql_query("UPDATE ".$table." SET karma = karma + 1 WHERE id = ".sqlesc($id));
sql_query("INSERT INTO karma (type, value, user, added) VALUES (".sqlesc($type).", ".sqlesc($id).", ".sqlesc($CURUSER['id']).", ".time().")");

if ($table == "humor")
sql_query("UPDATE users SET uploaded=uploaded+26214400 WHERE uploaded / downloaded >= 1 AND id = (SELECT uid FROM humor WHERE id = ".sqlesc($id).")") or sqlerr(__FILE__,__LINE__);

$show = true;

} elseif ($act == 'minus') {

sql_query("UPDATE ".$table." SET karma = karma - 1 WHERE id = ".sqlesc($id));
sql_query("INSERT INTO karma (type, value, user, added) VALUES (".sqlesc($type).", ".sqlesc($id).", ".sqlesc($CURUSER['id']).", ".time().")");

if ($table == "humor")
sql_query("UPDATE users SET uploaded=uploaded-13107200 WHERE uploaded / downloaded >= 1 AND id = (SELECT uid FROM humor WHERE id = ".sqlesc($id).")") or sqlerr(__FILE__,__LINE__);

$show = true;
}

if (!empty($show)) {
$res = sql_query("SELECT karma FROM ".$table." WHERE id = ".sqlesc($id));
$row = mysql_fetch_array($res);

unsql_cache("arrid_".$id);

if (isset($_POST['b']) && $_POST['b'] == "yes")
echo "<script>
setTimeout(\"hum_online();\", 2000);
</script>";


die("<img src=\"pic/".($act == "plus" ? "minus-dis.png":"minus.png")."\" title=\"".$tracker_lang['is_nolike']."\" alt=\"\" /> ".karma($row["karma"])." <img src=\"pic/".($act == "plus" ? "plus.png":"plus-dis.png")."\" title=\"".$tracker_lang['is_like']."\" alt=\"\" />");
}

}
?>