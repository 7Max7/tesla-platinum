<?php

require_once("include/bittorrent.php");
dbconn(false);

/**
* @author 7Max7
* @copyright Tesla Tracker (TT) � 2011 v.Platinum
*/

loggedinorreturn();

if (get_user_class() < UC_SYSOP){
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

accessadministration();

$tid = (isset($_GET["tid"]) ? (int) $_GET["tid"]:0);
$action = (string) $_GET["action"];

$tid_post = (isset($_POST["tid"]) ? (int) $_POST["tid"]:0);
$action_post = (string) $_POST["action"];


if (!empty($tid) && $action <> 'delete_tag'){
$cat_sql = array();

$recat = sql_query("SELECT id, name, image FROM categories", $cache = array("type" => "disk", "file" => "browse_cat_array", "time" => 24*7200)) or sqlerr(__FILE__, __LINE__);

while ($art = mysql_fetch_assoc_($recat)){
$cat_sql[$art["id"]] = array("name" => $art["name"], "image" => $art["image"]);
}

}



/// �������� ��������� ///
if (!empty($tid) && $action == 'delete') {

sql_query("DELETE FROM categories WHERE id = " .sqlesc($tid)) or sqlerr(__FILE__, __LINE__);

unsql_cache("category_php_all");
unsql_cache("block-category");
unsql_cache("browse_cat_array");
unsql_cache("browse_genrelist");
unsql_cache("count_tags_".$tid);
unsql_cache("tags_countcat");
unsql_cache("taggenrelist_".$tid); /// ������� ��� ����� �������������� �������

if (!headers_sent())
header("Location: category.php");
else
die("<script>setTimeout('document.location.href=\"category.php\"', 10);</script>");

die;
}
/// �������� ��������� ///



/// �������� ���� ///
if (!empty($tid) && $action == 'delete_tag') {

$sqro = sql_query("SELECT category FROM tags WHERE id = ".sqlesc($tid)) or sqlerr(__FILE__, __LINE__);
$ctaid = mysql_fetch_row($sqro);

sql_query("DELETE FROM tags WHERE id = " .sqlesc($tid)) or sqlerr(__FILE__, __LINE__);

unsql_cache("tags_countcat");
unsql_cache("taggenrelist_".$ctaid[0]); /// ������� ��� ����� �������������� �������

if (!headers_sent())
header("Location: category.php?tid=".$ctaid[0]."&action=viewtags");
else
die("<script>setTimeout('document.location.href=\"category.php?tid=".$ctaid[0]."&action=viewtags\"', 10);</script>");

die;
}
/// �������� ���� ///


/// ������� ����� � ��������� ///
if (!empty($tid) && $action == 'truntags') {

sql_query("DELETE FROM tags WHERE category = " .sqlesc($tid)) or sqlerr(__FILE__, __LINE__);

unsql_cache("tags_countcat");
unsql_cache("taggenrelist_".$tid); /// ������� ��� ����� �������������� �������

if (!headers_sent())
header("Location: category.php");
else
die("<script>setTimeout('document.location.href=\"category.php\"', 10);</script>");

die;
}
/// ������� ����� � ��������� ///




/// �������������� ��������� ///
if (!empty($tid_post) && $action_post == 'edit') {

$array_upd = array();
$array_upd[] = "name = ".sqlesc(htmlspecialchars_uni($_POST["name"]));

$image = htmlentities($_POST["img"]);
if (file_exists(ROOT_PATH."/pic/cats/".$image) || empty($image))
$array_upd[] = "image = ".sqlesc($image);

$array_upd[] = "sort = ".sqlesc(intval($_POST["sort"]));

sql_query("UPDATE categories SET ".implode(", ", $array_upd)." WHERE id = ".sqlesc($tid_post)) or sqlerr(__FILE__, __LINE__);

unsql_cache("category_php_all");

if (!headers_sent())
header("Location: category.php");
else
die("<script>setTimeout('document.location.href=\"category.php\"', 10);</script>");

die;


}
/// �������������� ��������� ///



stdhead($tracker_lang['category_tags']);


/// �������������� ��������� ���� ///
if (!empty($tid) && $action == 'edit') {

$sqro = sql_query("SELECT * FROM categories WHERE id = ".sqlesc($tid)) or sqlerr(__FILE__, __LINE__);
$ctaid = mysql_fetch_assoc($sqro);

begin_frame($tracker_lang['editing'].": ".$cat_sql[$tid]["name"], true);

echo "<form name=\"formed\" method=\"post\" action=\"category.php\">";
echo "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"80%\">";
echo "<input type=\"hidden\" name=\"action\" value=\"edit\">";
echo "<input type=\"hidden\" name=\"tid\" value='".$tid."'>";
echo "<tr><td class=\"b\">".$tracker_lang['name'].": </td><td align='right'><input type='text' size=\"50\" name='name' value='".htmlspecialchars($ctaid['name'])."'></td></tr>";
echo "<tr><td class=\"b\">".$tracker_lang['image_is'].": </td><td align='right'><input type='text' size=\"50\" name='img' value='".htmlentities($ctaid['image'])."'></td></tr>";
echo "<tr><td class=\"b\">".$tracker_lang['sorting'].": </td><td align='right'><input type='text' size=\"50\" name='sort' value='".$ctaid['sort']."'></td></tr>";
echo "<tr><td colspan=\"2\" class=\"a\"><input class=\"btn\" type='submit' value='".$tracker_lang['edit']."'></td></tr>";
echo "</table></form>";

end_frame();
stdfoot();
die();

}
/// �������������� ��������� ���� ///



/// �������� ����� ��������� ///
if ($action == 'viewtags' && !empty($tid)){

$sql_tags = sql_query("SELECT name, id, howmuch FROM tags WHERE category = ".sqlesc($tid)." ORDER BY howmuch DESC") or sqlerr(__FILE__, __LINE__);
begin_frame($tracker_lang['tags_forcat'].": ".$cat_sql[$tid]["name"], true);

echo "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"80%\">";

echo "<tr><td align=\"center\" class=\"b\" width=\"50%\" colspan=\"4\">".sprintf($tracker_lang['tags_backmain'], "category.php")."</td></tr>";

echo "<tr>
<td align=\"center\" class=\"a\" width=\"5%\">#</td>
<td align=\"center\" class=\"a\" width=\"5%\">".$tracker_lang['name']."</td>
<td align=\"center\" class=\"a\">".$tracker_lang['number_all']."</td>
<td align=\"center\" class=\"a\">".$tracker_lang['action']."</td>
</tr>";

while ($row_tags = mysql_fetch_array($sql_tags)) {

echo "<tr>
<td class=\"b\" width=\"5%\">".$row_tags["id"]."</td>
<td class=\"b\" width=\"80%\"><a title=\"".$tracker_lang['search']." ".htmlspecialchars($row_tags["name"])."\" style=\"font-weight:normal;\" href=\"browse.php?tag=".urlencode($row_tags["name"])."&cat=".$tid."\">".$row_tags["name"]."</a></td>
<td align=\"center\"width=\"10%\" class=\"b\">".number_format($row_tags["howmuch"])."</td>
<td align=\"center\"width=\"10%\" class=\"b\">
<a title=\"".$tracker_lang['delete']."\" href=category.php?tid=".$row_tags["id"]."&action=delete_tag>".$tracker_lang['delete']."</a></td>
</tr>";

}

echo "<tr><td align=\"center\" class=\"b\" width=\"50%\" colspan=\"4\">".sprintf($tracker_lang['cat_edit_link'], $cat_sql[$tid]["name"], "category.php?tid=".$tid."&action=edit")."</td></tr>";

echo "<tr><td align=\"center\" class=\"b\" width=\"50%\" colspan=\"4\">".sprintf($tracker_lang['tags_truncat'], $cat_sql[$tid]["name"], "category.php?tid=".$tid."&action=truntags")."</td></tr>";

echo "</table>";
end_frame();


stdfoot();
die;

}
/// �������� ����� ��������� ///



/// ���������� ���� ///
if ($action_post == "add_tagsnews"){

$name = htmlspecialchars(strip_tags($_POST['name']));
$name = tolower($name);

$category = (int) $_POST['category'];

$ex_cat = get_row_count("categories", "WHERE id = ".sqlesc($category));

if (!empty($ex_cat))
$ex_tags = get_row_count("tags", "WHERE name = ".sqlesc($name)." AND category = ".sqlesc($category));

if (empty($ex_tags))
sql_query("INSERT INTO tags (name, added, category) VALUES (" .implode(",", array_map("sqlesc", array($name, get_date_time(), $category))).")");

$next_id = mysql_insert_id();

unsql_cache("taggenrelist_".$category); /// ������� ��� ����� �������������� �������

if (!headers_sent() && !empty($next_id))
header("Location: category.php?tid=".$category."&action=viewtags");
elseif (headers_sent() && !empty($next_id))
die("<script>setTimeout('document.location.href=\"category.php?tid=".$category."&action=viewtags\"', 10);</script>");
elseif (!headers_sent() && empty($next_id))
header("Location: category.php?action=addaction");
elseif (headers_sent() && empty($next_id))
die("<script>setTimeout('document.location.href=\"category.php?action=addaction\"', 10);</script>");

die;

}
/// ���������� ���� ///





/// ���������� ��������� ///
if ($action_post == "add_catnews"){

$name = htmlspecialchars_uni(strip_tags($_POST['name']));
$sort = intval($_POST["sort"]);

if (empty($name))
stderr($tracker_lang['arror'], $tracker_lang['invalid_id_value']);

$image = htmlentities($_POST["img"]);
if (!file_exists(ROOT_PATH."/pic/cats/".$image) || empty($image))
stderr($tracker_lang['error'], sprintf($tracker_lang['img_noexes_pic'], $image, "<strong>/pic/cats/</strong>"));

$ex_cat = get_row_count("categories", "WHERE name = ".sqlesc($name));

if (empty($ex_cat))
sql_query("INSERT INTO categories (sort, name, image) VALUES (" .implode(",", array_map("sqlesc", array($sort, $name, $image))).")");

$next_id = mysql_insert_id();

if (!empty($next_id)){
unsql_cache("category_php_all");
unsql_cache("block-category");
unsql_cache("browse_cat_array");
unsql_cache("browse_genrelist");
unsql_cache("count_tags_".$next_id);
unsql_cache("taggenrelist_".$next_id); /// ������� ��� ����� �������������� �������
}

if (!headers_sent() && !empty($next_id))
header("Location: category.php");
elseif (headers_sent() && !empty($next_id))
die("<script>setTimeout('document.location.href=\"category.php\"', 10);</script>");
elseif (!headers_sent() && empty($next_id))
header("Location: category.php?action=addaction");
elseif (headers_sent() && empty($next_id))
die("<script>setTimeout('document.location.href=\"category.php?action=addaction\"', 10);</script>");

die;

}
/// ���������� ��������� ///




/// ��������� ����� id ��������� � ������� ///
if ($action_post == "patch_cat"){

$enoexcat = (isset($_POST["noexcat"]) ? (int) $_POST["noexcat"]:false);
$ecategory = (isset($_POST["category"]) ? (int) $_POST["category"]:false);

if ($enoexcat <> false && $ecategory <> false){
sql_query("UPDATE torrents SET category = ".sqlesc($ecategory)." WHERE category = ".sqlesc($enoexcat)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE tags SET category = ".sqlesc($ecategory)." WHERE category = ".sqlesc($enoexcat));
sql_query("UPDATE my_search SET cats = REPLACE(cats , ',".$enoexcat.",', ',".$ecategory.",')");
}

unsql_cache("category_php_all");
unsql_cache("block-category");
unsql_cache("browse_cat_array");
unsql_cache("browse_genrelist");

if ($enoexcat <> false)
unsql_cache("count_tags_".$enoexcat);
if ($ecategory <> false)
unsql_cache("count_tags_".$ecategory);

if (!headers_sent())
header("Location: category.php?action=addaction");
elseif (headers_sent())
die("<script>setTimeout('document.location.href=\"category.php?action=addaction\"', 10);</script>");

die;
}
/// ��������� ����� id ��������� � ������� ///




/// ��������� id ��������� � ������� ///
if ($action_post == "update_cat"){

$enomy_noexcat = (isset($_POST["my_noexcat"]) ? (int) $_POST["my_noexcat"]:false);
$emy_cat = (isset($_POST["my_category"]) ? (int) $_POST["my_category"]:false);

$enoexcat = (isset($_POST["noexcat"]) ? (int) $_POST["noexcat"]:false);
$ecategory = (isset($_POST["category"]) ? (int) $_POST["category"]:false);

if ($enomy_noexcat <> false && $emy_cat <> false){
sql_query("UPDATE torrents SET category = ".sqlesc($emy_cat)." WHERE category = ".sqlesc($enomy_noexcat)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE tags SET category = ".sqlesc($emy_cat)." WHERE category = ".sqlesc($enomy_noexcat));
sql_query("UPDATE my_search SET cats = REPLACE(cats , ',".$enoexcat.",', ',".$ecategory.",')");
} elseif ($enoexcat <> false && $ecategory <> false){
sql_query("UPDATE torrents SET category = ".sqlesc($ecategory)." WHERE category = ".sqlesc($enoexcat)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE tags SET category = ".sqlesc($ecategory)." WHERE category = ".sqlesc($enoexcat));
sql_query("UPDATE my_search SET cats = REPLACE(cats , ',".$enoexcat.",', ',".$ecategory.",')");
}

unsql_cache("taggenrelist_".$ecategory); /// ������� ��� ����� �������������� �������
unsql_cache("taggenrelist_".$enoexcat); /// ������� ��� ����� �������������� �������

unsql_cache("category_php_all");
unsql_cache("block-category");
unsql_cache("browse_cat_array");
unsql_cache("browse_genrelist");

if ($enomy_noexcat <> false)
unsql_cache("count_tags_".$enomy_noexcat);
if ($emy_cat <> false)
unsql_cache("count_tags_".$emy_cat);

if ($ecategory <> false)
unsql_cache("count_tags_".$enoexcat);
if ($emy_cat <> false)
unsql_cache("count_tags_".$ecategory);

if (!headers_sent())
header("Location: category.php?action=addaction");
elseif (headers_sent())
die("<script>setTimeout('document.location.href=\"category.php?action=addaction\"', 10);</script>");

die;
}
/// ��������� id ��������� � ������� ///



if ($action == "addaction"){

$cats = genrelist();

echo "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";
echo "<tr><td class='a' class=\"center\">".sprintf($tracker_lang['tags_backmain'], "category.php")."</td></tr>";
echo "<tr><td class='b' class=\"center\">".$tracker_lang['is_addpanel_tags']."</td></tr>";
echo "</table>";

$notif = '[pm][email]';
$arrat_id = $ar_exname = array();
$rat = sql_query("SELECT id, name FROM categories") or sqlerr(__FILE__, __LINE__);

while ($artae = mysql_fetch_assoc($rat)){
$arrat_id[] = $artae["id"];
$notif.= '[cat'.$artae["id"].']';
}


/// �������� ����� ���� ��������� � ������� ������������ ///
$sql_field = sql_query("SHOW FULL FIELDS FROM users LIKE 'notifs'") or sqlerr(__FILE__,__LINE__);
$r_field = mysql_fetch_array($sql_field);

if (!empty($r_field["Type"]) && !empty($notif)){

preg_match("/varchar\((.*?)\)/is", $r_field["Type"], $cid);

if (is_valid_id($cid[1]) && strlen($notif) > $cid[1] && !empty($cid[1])){
sql_query("ALTER TABLE users CHANGE notifs notifs VARCHAR( ".round(strlen($notif)+100)." ) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL DEFAULT ''") or sqlerr(__FILE__,__LINE__);
sql_query("ALTER TABLE users CHANGE catedit catedit VARCHAR( ".round(strlen($notif)+100)." ) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL DEFAULT ''") or sqlerr(__FILE__,__LINE__);
sql_query("ALTER TABLE my_search CHANGE cats cats VARCHAR( ".round(strlen($notif)+100)." ) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL DEFAULT ''") or sqlerr(__FILE__,__LINE__);
}
}
/// �������� ����� ���� ��������� � ������� ������������ ///






/// ��������� ����� id ��������� � ������� ///
$sql = sql_query("SELECT tor.category, (SELECT COUNT(*) FROM torrents WHERE tor.category = torrents.category) AS num FROM torrents AS tor WHERE tor.category NOT IN (".implode(",", $arrat_id).") GROUP BY tor.category") or sqlerr(__FILE__, __LINE__);

while ($row = mysql_fetch_assoc($sql)){
$ar_exname[] = array("num" => $row["num"], "id" => $row["category"]);
}


if (count($ar_exname)){
begin_frame($tracker_lang['rechange_main'], true);
echo "<form name='formadex' method='post' action='category.php'>";
echo "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"50%\">";

echo "<tr><td class='b' colspan=\"2\">".$tracker_lang['warning'].": ".sprintf($tracker_lang['rechange_idcat'], count($ar_exname))."</td></tr>";
echo "<tr><td class='a'>".$tracker_lang['cat_fake'].": </td><td>";

echo "<select name=\"noexcat\" style='width:100%'>";

foreach ($ar_exname as $rown)
echo "<option value=\"".$rown["id"]."\">".$rown["id"]." (".number_format($rown["num"])." ".$tracker_lang['time_s'].")</option>\n";

echo "</select>\n";
echo "</td></tr>";

echo "<tr><td class='a'>".$tracker_lang['cat_new_is'].": </td><td>";
echo "<select name=\"category\" style='width:100%'>\n<option value=\"0\">".$tracker_lang['choose']."</option>\n";

foreach ($cats as $row)
echo "<option value=\"".$row["id"]."\">".htmlspecialchars($row["name"])."</option>\n";

echo "</select>\n";
echo "</td></tr>";

echo "<tr><td colspan=2 class='a'><input type='hidden' name='action' value='patch_cat'><input type='submit' style=\"height: 25px; width:250px\" class=\"btn\" value='".$tracker_lang['change_act']."'></td></tr>";
echo "</table>";
echo "</form>";

end_frame();
}
/// ��������� ����� id ��������� � ������� ///




/// ��������� id ��������� � ������� ///
begin_frame($tracker_lang['change_cat_intor'], true);
echo "<form name='formadre' method='post' action='category.php'>";
echo "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"50%\">";

echo "<tr><td class='b' colspan=\"2\">".$tracker_lang['warning'].": ".$tracker_lang['enter_checkcat']."</td></tr>";

echo "<tr><td class='a'>".$tracker_lang['cat_old'].": </td><td>";
echo "<select name=\"noexcat\" style='width:100%'>\n<option value=\"0\">".$tracker_lang['choose']."</option>\n";
foreach ($cats as $row)
echo "<option value=\"".$row["id"]."\">".htmlspecialchars($row["name"])."</option>\n";
echo "</select>\n ".$tracker_lang['enter_cat_name']." <br /> <input type='text' size=8 name='my_noexcat'> ".$tracker_lang['enter_cat_id']."";
echo "</td></tr>";

echo "<tr><td class='a'>".$tracker_lang['cat_new_is'].": </td><td>";
echo "<select name=\"category\" style='width:100%'>\n<option value=\"0\">".$tracker_lang['choose']."</option>\n";
foreach ($cats as $row)
echo "<option value=\"".$row["id"]."\">".htmlspecialchars($row["name"])."</option>\n";
echo "</select>\n ".$tracker_lang['enter_cat_name']." <br /> <input type='text' size=8 name='my_category'> ".$tracker_lang['enter_cat_id']."";
echo "</td></tr>";

echo "<tr><td colspan=2 class='b'><input type='hidden' name='action' value='update_cat'><input type='submit' style=\"height: 25px; width:250px\" class=\"btn\" value='".$tracker_lang['change_cat_intor']."'></td></tr>";
echo "</table>";
echo "</form>";

end_frame();
/// ��������� id ��������� � ������� ///






/// ���������� ���� ���� ///
begin_frame($tracker_lang['add_tags_incat'], true);
echo "<form name='formadt' method='post' action='category.php'>";
echo "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"50%\">";

echo "<tr><td class='a'>".$tracker_lang['name'].": </td> <td><input type='text' size=50 name='name'></td></tr>";

$cats = genrelist();

echo "<tr><td class='a'>".$tracker_lang['category'].": </td><td>";
echo "<select name=\"category\" style='width:100%'>\n<option value=\"0\">".$tracker_lang['choose']."</option>\n";

foreach ($cats as $row)
echo "<option value=\"".$row["id"]."\">".htmlspecialchars($row["name"])."</option>\n";

echo "</select>\n";
echo "</td></tr>";

echo "<tr><td colspan=2 class='b'><input type='hidden' name='action' value='add_tagsnews'><input type='submit' style=\"height: 25px; width:200px\" class=\"btn\" value='".$tracker_lang['add_tags_incat']."'></td></tr>";
echo "</table>";
echo "</form>";

end_frame();
/// ���������� ���� ���� ///




/// ���������� ��������� ���� ///
begin_frame($tracker_lang['cat_add'], true);
echo "<form name='formadc' method='post' action='category.php'>";
echo "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"50%\">";
echo "<tr><td class='a'>".$tracker_lang['name'].": </td><td><input type='text' size=50 name='name'></td></tr>";
echo "<tr><td class='a'>".$tracker_lang['image_is'].": </td><td><input type='text' size=50 name='img'></td></tr>";
echo "<tr><td class='a'>".$tracker_lang['sorting'].": </td><td><input type='text' size=50 name='sort'></td></tr>";
echo "<tr><td colspan=2 class='b'><input type='hidden' name='action' value='add_catnews'><input type='submit' style=\"height: 25px; width:200px\" class=\"btn\" value='".$tracker_lang['cat_add']."'></td></tr>";
echo "</table>";
echo "</form>";
end_frame();
/// ���������� ��������� ���� ///

stdfoot();
die;
}


echo "<table cellspacing='0' cellpadding='2' width=\"100%\"><tr><td align='center'>";

begin_frame($tracker_lang['category_tags'], true);
echo "<table class=\"main\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td align=\"center\" class=\"a\" width=\"50%\" colspan=\"5\">".sprintf($tracker_lang['aoru_tagcat'], "category.php?action=addaction")."</td></tr>";

echo "<tr>
<td align=\"center\" class=\"a\" width=\"5%\">#</td>
<td align=\"center\" class=\"a\" width=\"5%\">".$tracker_lang['sorting']."</td>
<td align=\"center\" class=\"a\">".$tracker_lang['image_is']."</td>
<td align=\"center\" class=\"a\">".$tracker_lang['name']."</td>
<td align=\"center\" class=\"a\">".$tracker_lang['action']."</td>
</tr>";

$retag = sql_query("SELECT COUNT(*) AS num, category FROM tags GROUP BY category", $cache = array("type" => "disk", "file" => "tags_countcat", "time" => 60*60)) or sqlerr(__FILE__, __LINE__);

while ($artag = mysql_fetch_assoc_($retag)){
$tags[$artag["category"]] = $artag["num"];
}

$sql = sql_query("SELECT id, sort, name, image, (SELECT COUNT(*) FROM torrents WHERE categories.id = torrents.category) AS num_torrent FROM categories GROUP BY id", $cache = array("type" => "disk", "file" => "category_php_all", "time" => 60*60*5)) or sqlerr(__FILE__, __LINE__);

while ($row = mysql_fetch_assoc_($sql)) {

echo "<tr>
<td align=\"center\" class=\"b\">".$row['id']."</td>
<td align=\"center\" class=\"b\">".$row['sort']."</td>
<td align=\"center\" class=\"b\"><img src='".$DEFAULTBASEURL."/pic/cats/".htmlentities($row['image'])."' border='0' /></td>
<td align=\"center\" class=\"b\">".htmlspecialchars($row['name'])."<br />".$tracker_lang['torrents'].": <a title='".$tracker_lang['search_btn']."' href='browse.php?cat=".$row['id']."'>".number_format($row['num_torrent'])." ".$tracker_lang['time_s']."</a><br />".$tracker_lang['tags'].": <a title=\"".$tracker_lang['number_all']." / ".$tracker_lang['edit']."\" href='category.php?tid=".$row['id']."&action=viewtags'>".number_format($tags[$row['id']])." ".$tracker_lang['time_s']."</a></td>
<td align=\"center\" class=\"b\"><a title='".$tracker_lang['trun_tagofcat']."' href='category.php?tid=".$row['id']."&action=truntags'>".$tracker_lang['trun_tagofcat']."</a><br /><a title='".$tracker_lang['edit']."' href='category.php?tid=".$row['id']."&action=edit'>".$tracker_lang['edit']."</a><br /><a title='".$tracker_lang['delete_user']."' href='category.php?tid=".$row['id']."&action=delete'>".$tracker_lang['delete']."</a>
</td>
</tr>";

}
echo "</table>";
end_frame();

echo "</td></tr></table><br />";

stdfoot();

?>