<?
require_once("include/bittorrent.php");
dbconn(false,true);

header("Content-Type: text/html; charset=" .$tracker_lang['language_charset']);
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$do = (isset($_POST["action"]) ? (string) $_POST["action"] : "load");
$choice = (isset($_POST["choice"]) ? (int) $_POST["choice"] : 0);
$pollId = (isset($_POST["pollId"]) ? (int) $_POST["pollId"] : 0);

$results = $votes = array();

if (!in_array($do, array("load", "vote")) || !$CURUSER)
die($tracker_lang['loginid']);

if ($do == "load"){

$r_check = sql_query("SELECT p.id, p.added, p.sort, p.question, pa.selection, pa.userid 
FROM polls AS p
LEFT JOIN pollanswers AS pa ON p.id = pa.pollid AND pa.userid = ".sqlesc($CURUSER["id"])." AND pa.forum = '0'
WHERE p.forum = '0' ORDER BY p.id DESC LIMIT 1") or sqlerr(__FILE__, __LINE__);
$ar_check = mysql_fetch_assoc($r_check);

if (mysql_num_rows($r_check) == 1) {

$r_op = sql_query("SELECT * FROM polls WHERE id = ".$ar_check["id"], $cache = array("type" => "disk", "file" => "polls_".$ar_check["id"], "time" => 180))  or sqlerr(__FILE__, __LINE__);
$a_op = mysql_fetch_assoc_($r_op);

for ($i=0; $i < 20; $i++) {
if (!empty($a_op['option'.$i]))
$options[$i] = format_comment($a_op['option'.$i]);
}

if (empty($ar_check["userid"])){

echo ("<div id=\"poll_title\">".format_comment($ar_check["question"])."</div>\n");

foreach($options as $op_id => $op_val) {
echo ("<div align=\"left\"><input type=\"radio\" onclick=\"addvote(".$op_id.")\" name=\"choices\" value=\"".$op_id."\" id=\"opt_".$op_id."\" /><label for=\"opt_".$op_id."\">&nbsp;".$op_val."</label></div>\n");
}

echo("<div  align=\"left\"><input type=\"radio\" onclick=\"addvote(255)\" name=\"choices\" value=\"255\" id=\"opt_255\" /><label for=\"opt_255\">&nbsp;".$tracker_lang['blank_vote']."</label></div>\n");

echo("<input type=\"hidden\" value=\"\" name=\"choice\" id=\"choice\"/>");
echo("<input type=\"hidden\" value=\"".$ar_check["id"]."\" name=\"pollId\" id=\"pollId\"/>");

echo("<div align=\"center\"><input type=\"button\" value=\"".$tracker_lang['vote']."\" class=\"btn\" style=\"display:none; width: 200px\" id=\"vote_b\" onclick=\"vote();\"/></div>");

} else {

$r = sql_query("SELECT count(*) as count, selection  FROM pollanswers WHERE pollid = ".sqlesc($ar_check["id"])." AND selection < 20 AND forum = '0' GROUP BY selection", $cache = array("type" => "disk", "file" => "pollid".$ar_check["id"], "time" => 60)) or sqlerr(__FILE__, __LINE__);

$total = 0;
while($a = mysql_fetch_assoc_($r)) {
$total += $a["count"];
$votes[$a["selection"]] = $a["count"];
}

if ($total==0) $total = 1;

foreach($options as $k => $op) {
$results[] = array((isset($votes[$k])?$votes[$k]:0), $op);
}

function srt($a,$b) {
if ($a[0] > $b[0]) return -1;
if ($a[0] < $b[0]) return 1;
return 0;
}

if ($ar_check["sort"] == "yes") usort($results, srt);

echo ("<div id=\"poll_title\">".format_comment($ar_check["question"])."</div>\n");

echo ("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border:none\" id=\"results\" class=\"results\">");

$i = 0;

$maxid = @max($votes);
foreach ($results as $result) {

echo("<tr>
<td align=\"left\" width=\"40%\" style=\"border:none;\">".$result[1]."</td>
<td style=\"border:none;\" align=\"left\" width=\"60%\" valing=\"middle\"><div class=\"bar".(($i==0 && $ar_check["sort"] == "yes") ? "max" : "")."\" name=\"".($result[0] / $total * 100)."\" id=\"poll_result\">&nbsp;</div></td>
<td style=\"border:none;\">&nbsp;<b>".@number_format(($result[0] / $total * 100),2)."%</b></td>
</tr>\n");
++$i;

}
echo("</table>");


if ($a_op["comment"] <> "no"){

$sqrow = sql_query("SELECT COUNT(*) FROM comments WHERE poll = ".$ar_check["id"], $cache = array("type" => "disk", "file" => "poll_comment_".$ar_check["id"], "time" => 60*60*2)) or sqlerr(__FILE__, __LINE__); // 2 ����
$comments = mysql_fetch_row_($sqrow);
$comm_num = $comments[0];

}

echo ("<div align=\"center\"><b>".$tracker_lang['votes']."</b>: ".$total." ".($a_op["comment"] <> "no" && !empty($comm_num) ? "<b>".$tracker_lang['commens']."</b>: ".$comm_num : "")."</div><br /><div align=\"right\">".(get_user_class() > UC_MODERATOR ? "<b>[</b><a href=\"makepoll.php?action=edit&pollid=".$ar_check["id"]."&returnto=main\">".$tracker_lang['edit']."</a><b>]</b> - <b>[</b><a  href=\"polls.php?action=delete&pollid=".$ar_check["id"]."&returnto=main\">".$tracker_lang['delete']."</a><b>]</b> - " : "")." ".($a_op["comment"] == "no" ? "<b>[</b><s>".$tracker_lang['commenting']."</s><b>]</b>" : "<b>[</b><a href=\"polloverview.php?id=".$ar_check["id"]."\">".$tracker_lang['commenting']."</a><b>]</b>")."</div>");
}

} else
echo $tracker_lang['no_polls'];

} elseif ($do == "vote" && !empty($pollId)) {

$check = mysql_result(sql_query("SELECT count(id) FROM pollanswers WHERE pollid = ".$pollId." AND forum = '0' AND userid = ".sqlesc($CURUSER["id"])));

if ($check == 0)
sql_query("INSERT INTO pollanswers VALUES(0, ".sqlesc($pollId).", ".sqlesc($CURUSER["id"]).", ".sqlesc($choice).", 0)") or sqlerr(__FILE__, __LINE__);

if (mysql_affected_rows() == 1)
echo json_encode(array("status" => 1));

}

?>