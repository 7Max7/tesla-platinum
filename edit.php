<?
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

if (!mkglobal("id") || !is_valid_id($id) || empty($id))
stderr($tracker_lang['error_data'], $tracker_lang['invalid_id_value'].": id");


///////// ��� ����������� ������� ////////
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {

header("Content-Type: text/html; charset=" .$tracker_lang['language_charset']);

$id = (int) $_POST['id'];

if (empty($id))
die;

$res_s = sql_query("SELECT DISTINCT uid, username, class FROM sessions WHERE time > ".sqlesc(get_date_time(gmtime() - 300))." AND url=".sqlesc("/edit.php?id=".$id)." ORDER BY time DESC") or sqlerr(__FILE__,__LINE__);

$title_who = array();
while ($ar_r = mysql_fetch_assoc($res_s)) {
$title_who[] = "<b><a href=\"userdetails.php?id=".$ar_r['uid']."\">".get_user_class_color($ar_r["class"], $ar_r["username"])."</a></b>";
}

if (count($title_who))
echo implode(", ", $title_who);
else 
echo $tracker_lang['no_data'];

die;
}
///////// ��� ����������� ������� ////////


$res = sql_query("SELECT *, b.class AS classname, b.username AS classusername FROM torrents AS t
LEFT JOIN users AS b ON t.moderatedby = b.id
WHERE t.id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_assoc($res);

if (!$row)
stderr($tracker_lang['error_data'], $tracker_lang['torrent_del_or_move']);

if (!empty($row["owner"])){
$res2 = sql_query("SELECT id, username, class FROM users WHERE id = ".sqlesc($row["owner"])) or sqlerr(__FILE__, __LINE__);
$row2 = mysql_fetch_assoc($res2);
}

if (get_user_class() <= UC_UPLOADER && $row["moderated"] == "yes")
stderr($tracker_lang['error_data'], $tracker_lang['torrent_verified']);

if (!empty($CURUSER["catedit"]) && !preg_match("/\[cat".$row["category"]."\]/is", $CURUSER["catedit"]) && get_user_class() == UC_MODERATOR && $CURUSER["id"] <> $row["owner"])
stderr($tracker_lang['error'], $tracker_lang['no_modcat']); 

if (!isset($CURUSER) || ($CURUSER["id"] <> $row["owner"] && get_user_class() < UC_MODERATOR))
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

stdhead($tracker_lang['edit_of_torrent']." - \"".$row["name"]."\"");

if ($row["stop_time"] <> "0000-00-00 00:00:00" && get_user_class() < UC_SYSOP) {
$subres = sql_query("SELECT id FROM snatched WHERE startdat < ".sqlesc($row["stop_time"])." and torrent = ".sqlesc($id)." and userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);
$snatch = mysql_fetch_array($subres);
$snatched = $snatch["id"];
}

echo ("<form name=\"edit\" method=\"post\" action=\"takeedit.php\" enctype=\"multipart/form-data\">\n");
echo ("<input type=\"hidden\" name=\"id\" value=\"".$id."\">\n");

echo ("<table class=\"main\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n");
echo ("<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['edit_of_torrent']."</td></tr>");

$all_psize = file_upload_max_size();
if ($all_psize < (($Torrent_Config["max_image_size"]*5)+$Torrent_Config["max_torrent_size"]))
print "<tr><td align=\"center\" class=\"a\" width=\"99%\" colspan=\"2\" style=\"padding: 15px; font-weight: bold;\"><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['warn_limit_size'].": ".mksize($all_psize)."</td></tr>";

?>
<script>
function edit_online(id) {
jQuery.post("edit.php",{"id":id}, function(response) {
jQuery("#edit_online").html(response);
}, "html");
setTimeout("edit_online('<?=$id;?>');", 30000);
}
edit_online('<?=$id;?>');
</script>
<?

echo "<tr><td align=\"right\"><b>".$tracker_lang['now_editting']."</b></td><td align=\"left\"><span align=\"center\" id=\"edit_online\">".$tracker_lang['loading']." ".$tracker_lang['now_editting']."</span></td></tr>";

if (get_user_class() == UC_SYSOP)
tr($tracker_lang['download'], "<a class=\"index\" href=\"download.php?id=".$id."\"><b>".$row["name"]."</b></a>", 1);
 
if (get_user_class() >= UC_MODERATOR){

echo '<script>
function adjective_ax() {
jQuery.post("block-details_ajax.php" , {action:"simular", id:"'.$id.'", crc:"'.crc32($row["info_hash"].$id).'"}, function(response) {
jQuery("#adjective_ax").html(response);
}, "html");
setTimeout("adjective_ax();", 240666);
}
setTimeout("adjective_ax();", 3000);
</script>';

tr($tracker_lang['match_files'], "<div id=\"adjective_ax\">".$tracker_lang['loading']." ".$tracker_lang['match_files']."</div>", 1);  
}


tr($tracker_lang['torrent_file'], "<input type='file' name='tfile' size='80'> ".$tracker_lang['max_file_size'].": <b>".mksize($Torrent_Config["max_torrent_size"])."</b>".($Functs_Patch["multitracker"] == true ? "<br /><i>".$tracker_lang['max_speed_seed']."</i>":""), 1);

if ($Functs_Patch["multitracker"] == true)
tr($tracker_lang['privat_torrent'], ($row["multitracker"] == "no" ? $tracker_lang['yes']." <b>[</b>".$tracker_lang['disable_funcs'].": ".$tracker_lang['local_dht']."<b>]</b>" : $tracker_lang['no']." <b>[</b>".$tracker_lang['enable_funcs'].": ".$tracker_lang['local_dht']."<b>]</b>"), 1);

tr($tracker_lang['name'], "<input type=\"text\" name=\"name\" value=\"".htmlspecialchars($row["name"])."\" size=\"100\" /> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i>", 1);

$image0_inet = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", $row["image1"])); /// ����� �� ������ [] � ��
$image0_inet = htmlentities($image0_inet); ///������� ������ �� ������

if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image0_inet))
unset($image0_inet);

tr($tracker_lang['image'], "<table cellspacing=\"3\" cellpadding=\"0\" width=\"100%\" border=\"0\">
<tr>
<td align=\"center\" class=\"b\" colspan='2'>
<label><input type=\"radio\" name=\"img1action\" value=\"keep\" checked />".$tracker_lang['edit_noact']."&nbsp</label>
<label><input type=\"radio\" name=\"img1action\" value=\"delete\" />".$tracker_lang['edit_actdel']."&nbsp</label>
<label><input type=\"radio\" name=\"img1action\" value=\"update\" />".$tracker_lang['edit_actupd']."&nbsp</label>
</td>
</tr>

<tr><td align=\"left\" class=\"a\" colspan=\"2\">".$tracker_lang['load_from_local']." ".(empty($image0_inet) && !empty($row["image1"]) ? "<b>".$tracker_lang['active_select']."</b>":"")."</td></tr>
<tr><td align=\"left\" class=\"b\"><input type=\"file\" name=\"image0\" size=\"80\" /></td></tr>
<tr><td align=\"left\" class=\"a\" colspan=\"2\">".$tracker_lang['load_from_ethernet']." ".(!empty($image0_inet) && !empty($row["image1"]) ? "<b>".$tracker_lang['active_select']."</b>":"")."</td></tr>

<tr><td align=\"left\" class=\"b\"><input type=\"text\" maxLength=\"128\" size=\"80\" name=\"image0_inet\" value=\"".$image0_inet."\" /> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>
<tr><td align=\"center\" class=\"a\" colspan=\"2\">".$tracker_lang['max_file_size'].": <b>".mksize($Torrent_Config["max_image_size"])."</b></td></tr>

<tr>
<td align=\"left\" class=\"b\" colspan=\"2\">
<label><input type='checkbox' name='truncacheimg' value='yes' />".$tracker_lang['truncacheimg']."</label>
".(empty($image0_inet) ? "<br />
<label><input type='checkbox' name='imgimove' value='yes' />".$tracker_lang['imgimove']."</label>":"")."
</td>
</tr>
</table>", 1);


for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

$picture1 = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", $row["picture".$xpi_s])); /// ����� �� ������ [] � ��
$picture1 = htmlentities($picture1); ///������� ������ �� ������

if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $picture1))
unset($picture1);

tr($tracker_lang['scrinshot']." #".$xpi_s, "<table cellspacing=\"3\" cellpadding=\"0\" width=\"100%\" border=\"0\">
<tr>
<td align=\"center\" class=\"b\" colspan=\"2\">
<label><input type=\"radio\" name=\"pic".$xpi_s."action\" value=\"keep\" checked>".$tracker_lang['edit_noact']."&nbsp</label>
<label><input type=\"radio\" name=\"pic".$xpi_s."action\" value=\"delete\">".$tracker_lang['edit_actdel']."&nbsp</label>
<label><input type=\"radio\" name=\"pic".$xpi_s."action\" value=\"update\">".$tracker_lang['edit_actupd']."&nbsp</label>
</td>
</tr>
".($Functs_Patch["take_pic"] <> 1 ? "
<tr><td align=\"left\" class=\"a\"colspan=\"2\">".$tracker_lang['load_from_local']." ".(empty($picture1) && !empty($row["picture".$xpi_s]) ? "<b>".$tracker_lang['active_select']."</b>":"")."</td></tr>
<tr><td align=\"left\" class=\"b\"><input type=\"file\" name=\"picture_".$xpi_s."\" size=\"80\"> ".$tracker_lang['max_file_size'].": <b>".mksize($Torrent_Config["max_image_size"])."</b></td></tr>
":"")."
".($Functs_Patch["take_pic"] <> 2 ? "
<tr><td align=\"left\" class=\"a\"colspan=\"2\">".$tracker_lang['load_from_ethernet']." ".(!empty($picture1) && !empty($row["picture".$xpi_s]) ? "<b>".$tracker_lang['active_select']."</b>":"")."</td></tr>
<tr><td align=\"left\" class=\"b\"><input name='picture".$xpi_s."' value=\"".htmlentities($picture1)."\" size='80'\"> <i>".sprintf($tracker_lang['max_simp_of'], 256)."</i></td></tr>
":"")."
</table>", 1);
}

if ($Functs_Patch["take_pic"] <> 1)
tr ($tracker_lang['scrinshots'], "<textarea name=\"array_picture\" cols=\"77\" rows=\"3\"></textarea><br />".$tracker_lang['arr_pictures'], 1);

echo ("<tr><td align=\"right\" valign=\"top\"><b>".$tracker_lang['description']."".(get_user_class() >= UC_MODERATOR ? "<br />[".strlen($row["descr"])."]":"")."</b></td><td>");
textbbcode("edit", "descr",htmlspecialchars($row["descr"]));
echo ("</td></tr>\n");

$s = "<select name=\"type\">\n";
$cats = genrelist();
foreach ($cats as $subrow) {
$s .= "<option value=\"" . $subrow["id"] . "\"";
if ($subrow["id"] == $row["category"]) $s .= " selected=\"selected\"";
$s .= ">" . htmlspecialchars($subrow["name"]) . "</option>\n";
}

$s .= "</select>\n";

tr($tracker_lang['category'], $s, 1);


if (get_user_class() >= UC_MODERATOR) {

///// ��� ����� /////
?>
<script type="text/javascript" src="js/tagto.js"></script>
<script type="text/javascript">
(function(jQuery){
jQuery(document).ready(function(){
jQuery("#from").tagTo("#tags");
});
})(jQuery);
</script>

<?
if (!$row["tags"])
$ta = $tracker_lang['no_tag_oncat'];

$tags = taggenrelist($row["category"]);

$s = '<input type="hidden" name="oldtags" value="'.tolower($row["tags"]).'"><input type="text" id="tags" name="tags" value="'.tolower($row["tags"]).'"><br />'.$ta.'';
$s .= '<div id="from" '.(count($tags) > 50 ? 'style=\'height: 150px; overflow: auto;\'':"").'>';

foreach ($tags as $tag)
$s .= "<a class=\"alink\" title=\"".$tracker_lang['click_on_tag']."\" href='#'>".htmlspecialchars(tolower($tag["name"]))."</a>\n";

$s .= "</div>".$tracker_lang['notag_user']."\n";

tr($tracker_lang['all_tags'], $s, 1);
///// ��� ����� /////

}

tr($tracker_lang['visible'], "<input type='radio' name='visible' value='yes' ".($row["visible"] == "yes" ? "checked" : "").">".$tracker_lang['yes']." <input type='radio' name='visible' value='no' ".($row["visible"] == "no" ? "checked" : "").">".$tracker_lang['no']." <i>".$tracker_lang['visible_is']."</i>.", 1);
	
if (get_user_class() >= UC_ADMINISTRATOR){

if ($row["viponly"] <> "0000-00-00 00:00:00") {
$day_selec_2 = " <b>".$tracker_lang['clock']."</b>: ".$row["viponly"];
} else {
$day_selec = "<select name='day_s'><option value=\"0\">".$tracker_lang['signup_not_selected']."</option>\n";
$i = "1";

while ($i <= 62) {
$day_selec .= "<option value=".$i.">".$tracker_lang['number_all']." ".$tracker_lang['day_all'].": ".$i."</option>\n";
++$i;
}

$day_selec .= "</select>\n";
}

tr($tracker_lang['only_vip_users'], "<label><input type='radio' name='vip_only' value='yes' ".($row["viponly"] <> "0000-00-00 00:00:00" ? "checked" : "").">".$tracker_lang['yes']." </label><label><input type='radio' name='vip_only' value='no' ".($row["viponly"] == "0000-00-00 00:00:00" ? "checked" : "").">".$tracker_lang['no']." </label> ".$day_selec." <br /><i>".$tracker_lang['svip_days']."</i>".$day_selec_2, 1);
}
	
	
if (get_user_class() >= UC_ADMINISTRATOR)
tr($tracker_lang['banned'], "<label><input type='radio' name='banned' value='yes' ".($row["banned"] == "yes" ? "checked" : "").">".$tracker_lang['yes']." </label><label><input type='radio' name='banned' value='no' " .($row["banned"] == "no" ? "checked" : "").">".$tracker_lang['no']." </label> <i>".$tracker_lang['tor_banstopped']."</i><br /><input type=\"text\" size=\"73\" name=\"banned_reason\" ".($row["banned"] == "yes" && !empty($row["banned_reason"]) ? "value=\"".htmlspecialchars($row["banned_reason"])."\"":"")."> <i>".$tracker_lang['reason_main']."</i>", 1);

if (get_user_class() >= UC_MODERATOR)
tr($tracker_lang['golden'], "<label><input type=\"checkbox\" name=\"free\" ".($row["free"] == "yes" ? "checked=\"checked\"" : "")." value=\"1\" /> <i>".$tracker_lang['golden_descr']."</i></label>", 1);

if (get_user_class() >= UC_ADMINISTRATOR)
tr($tracker_lang['sticky'], "<label><input type='radio' name='sticky' value='yes' ".($row["sticky"] == "yes" ? "checked" : "").">".$tracker_lang['yes']." </label><label><input type='radio' name='sticky' value='no' ".($row["sticky"] == "no" ? "checked" : "").">".$tracker_lang['no']." </label> <i>".$tracker_lang['sticky_info']."</i>", 1);

if (get_user_class() >= UC_ADMINISTRATOR)
tr($tracker_lang['stop_time'], "<label><input type='radio' name='stopped' value='yes' ".($row["stop_time"] <> "0000-00-00 00:00:00" ? "checked" : "").">".$tracker_lang['yes']." </label><label><input type='radio' name='stopped' value='no' ".($row["stop_time"] == "0000-00-00 00:00:00" ? "checked" : "").">".$tracker_lang['no']." </label> ".($row["stop_time"] <> "0000-00-00 00:00:00" ? "<b>".$tracker_lang['clock'].": ".$row["stop_time"]."</b><br />":"")." <i>".sprintf($tracker_lang['stop_tor_of'], $row["stop_time"])."</i>", 1);

if (get_user_class() >= UC_MODERATOR)
tr($tracker_lang['http_link'], "<input type=\"text\" name=\"webseed\" size=\"73\" value=\"".strip_tags($row["webseed"])."\"/><br />".$tracker_lang['web_seed_info'], 1);

if (get_user_class() >= UC_MODERATOR)
tr($tracker_lang['new_datanow'], "<label><input type=\"checkbox\" name=\"up_date\" value=\"yes\"> <i>".$tracker_lang['new_timeupd']."</i></label>", 1);  

if (get_user_class() >= UC_ADMINISTRATOR)
echo ("<tr><td align=\"right\" valign=\"top\" class=\"heading\">".$tracker_lang['user_tor_anonim']."</td><td colspan=\"2\" align=\"left\"><label><input type='checkbox' name='user_reliases_anonim' value='1' /> <i>".$tracker_lang['user_tor_aninfo']."</i></label></td></tr>\n");

if (get_user_class() >= UC_ADMINISTRATOR && $row2["owner"] >= UC_VIP && $row2["owner"] <= UC_UPLOADER)
echo ("<tr><td align=\"right\" valign=\"top\" class=\"heading\">".$tracker_lang['tabs_mdbyme']."</td><td colspan=\"2\" align=\"left\"><label><input type='checkbox' name='mdbyme' value='1' ".($row["moderatedby"] == $row["owner"] ? "checked" : "")." /><i>".$tracker_lang['tabs_imdbyme']."</i></label><br />".$tracker_lang['warning'].": ".$tracker_lang['mdbyme_edinfo']."</td></tr>\n");

if (get_user_class() >= UC_ADMINISTRATOR){

echo ("<tr><td align=\"right\" valign=\"top\" class=\"heading\">".$tracker_lang['assign_author']."</td><td colspan=\"2\" align=\"left\">".$tracker_lang['enter_iduser'].": <input type=\"text\" size=\"8\" name=\"release_set_id\"></tr></td>");

echo ("<tr><td align=\"right\" valign=\"top\" class=\"heading\">".$tracker_lang['newid_tor']."</td><td colspan=\"2\" align=\"left\">".$tracker_lang['enter_new_id'].": <input type=\"text\" size=\"8\" name=\"newid\"></tr></td>");

tr($tracker_lang['del_comm'], "<label><input type=\"checkbox\" name=\"delete_comment\" value=\"yes\"> <i>".$tracker_lang['delete_user']."</i></label>", 1);  

}

if (get_user_class() >= UC_MODERATOR)
tr($tracker_lang['recheck_files'], "<label><input type=\"checkbox\" name=\"checks_files\" value=\"yes\"> <i>".$tracker_lang['recheck_utorrent']."</i></label>", 1);  

if (get_user_class() >= UC_MODERATOR)
tr($tracker_lang['block_comments'], "<label><input type='radio' name='lock_comments' value='yes' ".($row["comment_lock"] == "yes" ? "checked" : "").">".$tracker_lang['yes']." </label> <label><input type='radio' name='lock_comments' value='no' ".($row["comment_lock"] == "no" ? " checked" : "").">".$tracker_lang['no']." </label>", 1);


if (get_user_class() >= UC_MODERATOR){

$torrent_com = htmlspecialchars($row["torrent_com"]);

echo ("<tr><td align=\"right\" valign=\"top\" class=\"heading\">".$tracker_lang['history_torrent']." <br />[".strlen($torrent_com)."]</td><td colspan='2' align='left'><textarea cols='75' rows='6' ".(get_user_class() < UC_SYSOP ? " readonly" : "name='torrent_com'").">".$torrent_com."</textarea></td></tr>\n");

echo ("<tr><td align=\"right\" valign=\"top\" class=\"heading\">".$tracker_lang['add_note']."</td><td colspan='2' align='left'><textarea cols='75' rows='3' name='torrent_com_zam'></textarea></td></tr>\n");
}


if (get_user_class() >= UC_SYSOP){
tr($tracker_lang['deleting_torrent'], "
<label><input type=\"checkbox\" name=\"reacon\" value=\"1\"/>".$tracker_lang['del_torr_reason'].":</label><br />
<label><input name=\"reasontype\" type=\"radio\" value=\"1\">".$tracker_lang['torrent_ghouls']."</label><br />
<input name=\"reasontype\" type=\"radio\" value=\"2\"><b>".$tracker_lang['torrent_dybl']."</b>: <input type=\"text\" size=\"40\" name=\"dup\">&nbsp;<i>".$tracker_lang['specify_torrent']."</i><br />
<input name=\"reasontype\" type=\"radio\" value=\"3\">&nbsp;<b>".$tracker_lang['torrent_banrules']."</b>: <input type=\"text\" size=\"40\" name=\"rule\">&nbsp;<i>".$tracker_lang['reason_main']."</i><br />
<input name=\"reasontype\" type=\"radio\" value=\"4\">&nbsp;<b>".$tracker_lang['torrent_license']."</b>: &nbsp;<i>".$tracker_lang['file_license']."</i>
", 1);
}

echo ("<tr><td class=\"b\" colspan=\"2\" align=\"center\">
<input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['edit']."\" class=\"btn\" value=\"".$tracker_lang['edit']."\" style=\"height: 25px; width: 120px\" /> <input type='reset' class=\"btn\" value=\"".$tracker_lang['reset']."\" style=\"height: 25px; width: 120px\" /></form>
<form method=\"post\" action=\"details.php?id=".$id."\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['back_inlink']."\" style='height: 25px; width: 120px' /></form>
</td></tr>\n");

echo ("</table>\n");

stdfoot();
?>