<?
if (!defined('BLOCK_FILE')) {
 Header("Location: ../index.php");
 exit;
}

global $tracker_lang;
$blocktitle = $tracker_lang['my_friends'];

?>
<script>
function friends_online() {
jQuery.post("block-online_jquery.php", {"action":"friends"}, function(response) {
jQuery("#friends_online").html(response);
}, "html");
setTimeout("friends_online();", 60000);
}
friends_online();
</script>
<?
$content = "<span align=\"center\" id=\"friends_online\">".$tracker_lang['loading']."</span>";
?>