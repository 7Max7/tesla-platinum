<?
require ("include/bittorrent.php");
dbconn();
loggedinorreturn();

if (get_user_class() < UC_SYSOP) {
attacks_log($_SERVER["SCRIPT_FILENAME"]); 
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

accessadministration();

$redir = false;

if (isset($_POST["remove"]) && is_array($_POST["remove"])) {

sql_query("DELETE FROM cheaters WHERE userid IN (".implode(",", array_map('intval', $_POST['remove'])).")") or sqlerr(__FILE__, __LINE__);
$redir = true;
}

if (isset($_GET["truncate"])){

sql_query("TRUNCATE TABLE cheaters") or sqlerr(__FILE__, __LINE__);
$redir = true;
}

if (isset($_POST["desact"]) && is_array($_POST["desact"])) {

foreach ($_POST["desact"] as $tid){

if (is_valid_id($tid)){

$res1 = sql_query("SELECT timediff, rate, upthis FROM cheaters WHERE userid = ".sqlesc($tid)." ORDER BY max(upthis) DESC LIMIT 1") or sqlerr(__FILE__, __LINE__);
$arr1 = mysql_fetch_array($res1);

$rate = $arr1["rate"];
$timediff = $arr1["timediff"];

$modcomment = date("Y-m-d") . " - ������� ��� ����� (�����: ".mksize($arr1["upthis"])." ��������: ".$rate." /��� �����: ".$timediff." ���).\n";

sql_query("UPDATE users SET enabled = 'no', modcomment = CONCAT_WS('', ".sqlesc($modcomment).", modcomment) WHERE class <= ".sqlesc(UC_ADMINISTRATOR)." AND id = ".sqlesc($tid)) or sqlerr(__FILE__, __LINE__); 

unsql_cache("arrid_".$tid);

}

}

$redir = true;
}

if ($redir == true){
header("Refresh: 0; url=\"cheaters.php\"");
die;
}

$count = get_row_count("cheaters");

if (empty($count))
stderr($tracker_lang['error'], $tracker_lang['no_data_now']);

$count = get_row_count("cheaters", "GROUP BY cheaters.userid");

stdhead($tracker_lang['cheating'], true);

echo "<form action=\"cheaters.php\" method=\"post\"><table class=\"main\" border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"colhead\" colspan=\"6\" width=\"100%\">".$tracker_lang['tab_cheaters_info']."</td></tr>";

list ($pagertop, $pagerbottom, $limit) = pager(50, $count, "cheaters.php?");

echo "<tr><td align=\"center\" colspan=\"6\" width=\"100%\">".$pagertop."</td></tr>";

?>
<script language="Javascript" type="text/javascript">
jQuery(document).ready(function() {

jQuery("#remove").click(function () {
if (!jQuery("#remove").is(":checked"))
jQuery(".removeo").removeAttr("checked");
else
jQuery(".removeo").attr("checked","checked");
});

jQuery("#desact").click(function () {
if (!jQuery("#desact").is(":checked"))
jQuery(".desacto").removeAttr("checked");
else
jQuery(".desacto").attr("checked","checked");
});

}); 
</script>
<?

echo "<tr>
<td class=\"colhead\" width=\"1%\" align=\"center\">#</td>
<td class=\"colhead\">".$tracker_lang['username']." / ".$tracker_lang['description']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['number_all']."</td>
<td class=\"colhead\" align=\"center\"><label><input type=\"checkbox\" id=\"remove\" />".$tracker_lang['disselect']."</label> / <label>".$tracker_lang['disable']." <input type=\"checkbox\" id=\"desact\" /></label></td>
</tr>";

$res = sql_query("SELECT c.*, u.username, u.class, u.downloaded, u.uploaded, u.enabled, u.warned
FROM cheaters AS c 
LEFT JOIN users AS u ON c.userid = u.id 
GROUP BY c.userid ORDER BY c.added DESC ".$limit) or sqlerr(__FILE__, __LINE__);

while ($arr = mysql_fetch_assoc($res)) {

$rat_u = @number_format($arr["uploaded"] / $arr["downloaded"], 3);
$ratio = "<font color=\"".get_ratio_color($rat_u)."\">".($arr["downloaded"] > 0 ? $rat_u : "<a title=\"".$tracker_lang['ratio']."\">---</a>")."</font>";

echo "<tr><td width=\"1%\" align=\"center\" class=\"a\">".$arr["id"]."</td>";

echo "<td align=\"left\" class=\"a\"><a target='_blank' href=\"userdetails.php?id=".$arr["userid"]."\"><b>".get_user_class_color($arr["class"], $arr["username"])."</b></a> (".$ratio."), <u>U</u>: ".mksize($arr["uploaded"]).", <u>D</u>: ".mksize($arr["downloaded"]).", ".($arr["enabled"] == "no" ? "<img src=\"/pic/warned2.gif\" title=\"".$tracker_lang['disabled']."\">" : "")." ".($arr["warned"] == "yes" ? "<img src=\"/pic/warned9.gif\" title=\"".$tracker_lang['warned']."\">" : "")."  <a target='_blank' title=\"".$tracker_lang['user_ip']."\" href=\"usersearch.php?ip=".$arr["userip"]."\">".$arr["userip"]."</a>, ".htmlspecialchars($arr["client"])."
</td>";


$rarr = sql_query("SELECT t.name, t.id, t.free, u.upthis, t.size, u.timediff, u.rate, u.numpeers, u.added, u.client, u.id AS uid, s.uploaded, s.downloaded, s.to_go 
FROM torrents AS t
LEFT JOIN snatched AS s ON s.torrent = t.id AND s.userid = ".sqlesc($arr["userid"])."
LEFT JOIN cheaters AS u ON u.torrentid = t.id
WHERE u.userid = ".sqlesc($arr["userid"])." ORDER BY u.added DESC") or sqlerr(__FILE__, __LINE__);

echo "<td align=\"center\" class=\"a\">".mysql_num_rows($rarr)." ".$tracker_lang['times']."</td>";

echo "<td align=\"center\" class=\"a\" width=\"10%\">

<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td align=\"center\" class=\"a\"><input type=\"checkbox\" class=\"removeo\" name=\"remove[]\" value=\"".$arr["userid"]."\" /></td><td align=\"center\" class=\"b\"><input type=\"checkbox\" name=\"desact[]\" class=\"desacto\" value=\"".$arr["userid"]."\" /></td></tr>
</table>

</td>";

echo "</tr>";

echo "<td align=\"center\" width=\"100%\" colspan=\"6\" style=\"margin: 0px; padding: 0px;\">";
echo "<table class=\"main\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellspacing=\"5\">";

$array_upthis = $array_rate = array();
$num = 1;

while ($row = mysql_fetch_assoc($rarr)) {

$array_upthis[] = $row["upthis"];
$array_rate[] = $row["rate"];

if ($num%2 == 0){
$cl1 = "class=\"b\"";
$cl2 = "class=\"a\"";
} else {
$cl2 = "class=\"b\"";
$cl1 = "class=\"a\"";
}

echo "<tr>
<td ".$cl1." width=\"1%\" align=\"left\">".$row["uid"]."</td>
<td ".$cl2.">".$num.". <a href=\"details.php?id=".$row["id"]."\" title=\"".$tracker_lang['description']."\">".htmlspecialchars_uni($row["name"])."</a> (".number_format($row["numpeers"]).")<br />
<b>".$tracker_lang['size']."</b>: ".mksize($row["size"]).", <b>U</b>: ".mksize($row["uploaded"]).", <b>D</b>: ".mksize($row["downloaded"]).", <b>".$tracker_lang['completed']."</b>: " . sprintf("%.2f%%", 100 * (1 - ($row["to_go"] / $row["size"])))."
</td>
<td ".$cl1." align=\"center\">".mksize($row["upthis"])." ".$tracker_lang['in']." ".mksize($row["rate"])." /".$tracker_lang['sec']." (".$row["timediff"]." ".$tracker_lang['sec'].")<br />
".$row["added"]."
</td>
</tr>";

++$num;
}

echo "</table></td>";


$i1 = $i2 = 0;
foreach ($array_upthis as $v1) {
$i1 += $v1;
}
foreach ($array_rate as $v2) {
$i2 += $v2;
}

echo "<tr><td class=\"a\" align=\"center\" colspan=\"6\">

<table class=\"main\" border=\"1\" width=\"100%\" cellspacing=\"0\" cellspacing=\"0\">
<tr>
<td class=\"a\" align=\"center\" width=\"50%\" valign=\"top\"><b>".$tracker_lang['flooded']."</b> -> <u>".$tracker_lang['min']."</u>: ".@mksize(min($array_upthis)).", <u>".$tracker_lang['average']."</u>: ".@mksize($i1/count($array_upthis)).", <u>".$tracker_lang['max']."</u>: ".@mksize(max($array_upthis))."</td>

<td class=\"a\" align=\"center\" width=\"50%\" valign=\"top\"><b>".$tracker_lang['speed']."</b> -> <u>".$tracker_lang['min']."</u>: ".@mksize(min($array_rate))." / ".$tracker_lang['sec'].", <u>".$tracker_lang['average']."</u>: ".@mksize($i2/count($array_rate))." / ".$tracker_lang['sec'].", <u>".$tracker_lang['max']."</u>: ".@mksize(max($array_rate))." / ".$tracker_lang['sec']."</td>
</tr>
</table>

</td></tr>";

echo "<tr><td align=\"center\" class=\"b\" colspan=\"6\"></td></tr>";
}

if (get_user_class() >= UC_ADMINISTRATOR)
echo "<tr><td colspan=\"6\" align=\"right\"><input type=\"submit\" name=\"submit\" class=\"btn\" value=\"".$tracker_lang['b_action']."\" /></td></tr>";

echo "<tr><td align=\"center\" class=\"a\" colspan=\"6\"><a href=\"cheaters.php?truncate\">".$tracker_lang['trunc_monit']."</a></td></tr>";

echo "<tr><td align=\"center\" colspan=\"6\">".$pagerbottom."</td></tr>";

echo "</table>";
echo "</form>";

stdfoot(true);
?>