<?
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();

if (get_user_class() < UC_SYSOP) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}


global $CURUSER, $tracker_lang;

function forum_select($currentforum = 0) {

global $tracker_lang;

echo ("<form method='post' action='forummanage.php?action=takedelete&id=".$currentforum."' name='jump'>\n");
echo "<table width=100% border=2 cellspacing=5 cellpadding=5>";

echo "<tr><td class=\"a\" align=\"center\"><a class=\"alink\" href='forummanage.php'>".$tracker_lang['admining_forum']."</a></td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";

echo ("<input type='hidden' name='deleteall' value='true' />\n");
echo ($tracker_lang['move_cat_forum'].": <br />");
echo ("<select name='forumid'>\n");

$res = sql_query("SELECT id, name FROM forums ORDER BY name") or sqlerr(__FILE__, __LINE__);

while ($arr = mysql_fetch_assoc($res)) {
if ($arr["id"] == $currentforum)
continue;
echo ("<option value='".$arr["id"]."' ".($currentforum == $arr["id"] ? "selected='selected'" : "").">". $arr["name"]."</option>\n");
}

echo ("</select><br />");
echo ("<input type='submit' value='".$tracker_lang['act_movcat']."' class='btn' />\n");
echo "</td></tr>";
echo ("</table>");
echo ("</form>");

}




$action = isset($_GET['action']) ? (string) $_GET['action'] : 'main';

switch($action) {

case 'edit': {

stdhead($tracker_lang['edit']);

$id = isset($_GET["id"]) ? (int) $_GET["id"] : stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$result = sql_query ("SELECT * FROM forums where id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($result) > 0) {

while ($row = mysql_fetch_assoc($result)){

echo "<form method=\"post\" action=\"forummanage.php?action=takeedit\">";
echo "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\" align=\"center\">";

echo "<tr><td colspan=\"2\" class=\"a\" align=\"center\"><a class=\"alink\" href='forummanage.php'>".$tracker_lang['admining_forum']."</a></td></tr>";
echo "<tr><td colspan=\"2\" class='colhead'>".htmlspecialchars($row["name"])."</td></tr>";
echo "<tr><td class=\"b\"><b>".$tracker_lang['name']."</b></td><td class=\"a\"><input name=\"name\" type=\"text\" size=\"60\" maxlength=\"60\" value=\"".htmlspecialchars($row["name"])."\" /> ".sprintf($tracker_lang['max_simp_of'], 60)."</td></tr>";
echo "<tr><td class=\"b\"><b>".$tracker_lang['description']."</b></td><td class=\"a\"><input name=\"desc\" type=\"text\" size=\"100\" maxlength=\"200\" value=\"".htmlspecialchars($row["description"])."\" /> ".sprintf($tracker_lang['max_simp_of'], 200)."</td></tr>";
echo "<tr><td class=\"b\"><b>".$tracker_lang['is_read_post'].": </b></td><td class=\"a\"><select name='readclass'>";

$maxclass = get_user_class();
for ($i = 0; $i <= $maxclass; ++$i)
if (get_user_class_name($i) != "" )
echo ("<option value='$i'".($row["minclassread"] == $i ? " selected='selected'" : "").">".get_user_class_name($i)."</option>");

echo "</select>";
echo "</td></tr>";

echo "<tr>";
echo "<td class=\"b\"><b>".$tracker_lang['is_write_post'].": </b></td>";
echo "<td class=\"a\"><select name='writeclass'>";

$maxclass = get_user_class();
for ($i = 0; $i <= $maxclass; ++$i)
if (get_user_class_name($i) != "" )
echo ("<option value='$i'".($row["minclasswrite"] == $i ? " selected='selected'" : "").">".get_user_class_name($i)."</option>");

echo "</select></td></tr>";

echo "<tr>";
echo "<td class=\"b\"><b>".$tracker_lang['is_creat_post'].": </b></td>";
echo "<td class=\"a\"><select name='createclass'>";
$maxclass = get_user_class();
for ($i = 0; $i <= $maxclass; ++$i)
if (get_user_class_name($i) != "" )
echo ("<option value='$i'".($row["minclasscreate"] == $i ? " selected='selected'" : "").">".get_user_class_name($i)."</option>");
echo "</select></td>";
echo "</tr>";


echo "<tr>";
echo "<td class=\"b\"><b>".$tracker_lang['sorting']."</b></td>";
echo "<td class=\"a\"><select name='sort'>";
$res = sql_query("SELECT sort FROM forums") or sqlerr(__FILE__, __LINE__);
$nr = mysql_num_rows($res);
$maxclass = $nr + 1;
for ($i = 0; $i <= $maxclass; ++$i)
echo ("<option value='$i'".($row["sort"] == $i ? " selected='selected'" : "").">$i</option>");
echo "</select>";
echo "</td>";
echo "</tr>";

echo "<tr>";
echo "<td class=\"b\"><b>".$tracker_lang['forum_hidden']."</b></td>";
echo "<td class=\"a\"><input type=radio name=visible value='yes' ".($row["visible"] == "no" ? "checked":"")."> ".$tracker_lang['yes']." <input type=radio name=visible value='no' ".($row["visible"] == "yes" ? "checked":"")."> ".$tracker_lang['no']."</td>";
echo "</tr>";


echo "<tr><td class=a colspan=\"2\">";
echo "<input type=\"hidden\" name=\"id\" value=\"".$id."\" />";
echo "<input type=\"submit\" name=\"submit\" value=\"".$tracker_lang['edit']."\" class=\"btn\" />";
echo "</td></tr>";

echo "</table>";
echo "</form>";

}
} else
echo $tracker_lang['sum_nodata'];

stdfoot();
die;
}

break;

case 'takeedit': {

$id = (int) $_POST["id"];

if (!$_POST['name'] && !$_POST['desc'] && !$_POST['id']) {
header("Location: forummanage.php");
die();
}

sql_query("UPDATE forums SET sort = ".sqlesc(intval($_POST['sort'])).", name = ".sqlesc(htmlspecialchars_uni($_POST['name'])). ", description = ".sqlesc(htmlspecialchars($_POST['desc'])). ", minclassread = ".sqlesc(intval($_POST['readclass'])).", minclasswrite = ".sqlesc(intval($_POST['writeclass'])).",
minclasscreate = ".sqlesc(intval($_POST['createclass'])).", visible = ".sqlesc(($_POST['visible'] == "yes" ? "yes":"no"))."
WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("forums_cat");
unsql_cache("forums.main");
unsql_cache("forums.id_name");

if (mysql_affected_rows() === 1){
header("Refresh: 3; url=forummanage.php?action=edit&id=".$id);
stderr($tracker_lang['success'], $tracker_lang['edited']." <a href='forummanage.php'>".$tracker_lang['back_inlink']."</a>");
} else {
header("Refresh: 3; url=forummanage.php?action=edit&id=".$id);
stderr($tracker_lang['error'], $tracker_lang['is_done'].". <a href='forummanage.php'>".$tracker_lang['back_inlink']."</a>");
}

die();
}
break;

 
case 'delete': {

$id = (int) $_GET["id"];

if (empty($id) || !is_valid_id($id))
die($tracker_lang['invalid_id_value'].": ".$id);

$res = sql_query("SELECT id, subject FROM topics WHERE forumid = $id") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) >= 1) {

stdhead($tracker_lang['warning']);
forum_select($id);
stdfoot();
die;

} else {
$row = mysql_fetch_assoc($res);
stderr($tracker_lang['warning'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $row["subject"], "forummanage.php?action=takedelete&id=$id"));
}

}
break;


 
case 'takedelete': {

$id = isset($_GET['id']) ? (int) $_GET['id'] : stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

if (!isset($_POST['deleteall'])) {
$res = sql_query("SELECT id FROM topics WHERE forumid=$id")or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
sql_query("DELETE FROM forums WHERE id=$id") or sqlerr(__FILE__, __LINE__);

sql_query("DELETE FROM topics WHERE forumid=$id") or sqlerr(__FILE__, __LINE__);

unsql_cache("forums.id_name");
unsql_cache("forums_cat");
unsql_cache("forums.main");

if (mysql_affected_rows() > 0)
stderr($tracker_lang['success'], "
".$tracker_lang['warning'].": ".$tracker_lang['forum_deleted']."<br />
".$tracker_lang['warning'].": ".$tracker_lang['topic_deleted']."<br />
<a href='forummanage.php'>".$tracker_lang['back_inlink']."</a>");

} else {

$forumid = (isset($_POST['forumid']) && ctype_digit($_POST['forumid'])) ? intval($_POST['forumid']) : 	stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$res = sql_query("SELECT id FROM topics WHERE forumid = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) <> 0){

$tid = array();
while($row = mysql_fetch_assoc($res))
$tid[] = $row['id'];

sql_query("UPDATE topics SET forumid = ".sqlesc($forumid)." WHERE id IN (".implode(',' , $tid).")") or sqlerr(__FILE__, __LINE__);

if (mysql_affected_rows() > 0)
sql_query("DELETE FROM forums WHERE id = ".sqlesc($id))or sqlerr(__FILE__, __LINE__);

unsql_cache("forums_cat");
unsql_cache("forums.main");
unsql_cache("forums.id_name");

}


if (mysql_affected_rows() > 0)
stderr($tracker_lang['success'], "
".$tracker_lang['warning'].": ".$tracker_lang['forum_deleted']."<br /><a href='forummanage.php'>".$tracker_lang['back_inlink']."</a>");

}
}

break;



case 'add': {


stdhead($tracker_lang['creating_category']);

begin_main_frame();

echo "<form method='post' action=\"forummanage.php?action=takeadd\">";
echo "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\" align=\"center\">";

echo "<tr><td class=\"a\" align=\"center\" colspan=\"2\"><a class=\"alink\" href='forummanage.php'>".$tracker_lang['admining_forum']."</a></td></tr>";

echo "<tr><td colspan=\"2\" class='colhead'>".$tracker_lang['creating_category']."</td></tr>";
echo "<tr><td class=\"b\"><b>".$tracker_lang['name']."</b></td><td class=\"a\"><input name=\"name\" type=\"text\" size=\"20\" maxlength=\"60\" /></td></tr>";

echo "<tr><td class=\"b\"><b>".$tracker_lang['description']."</b></td><td class=\"a\"><input name=\"desc\" type=\"text\" size=\"30\" maxlength=\"200\" /></td></tr>";
echo "<tr><td class=\"b\"><b>".$tracker_lang['is_read_post'].": </b></td><td class=\"a\"><select name=\"readclass\">";

$maxclass = get_user_class();
for ($i = 0; $i <= $maxclass; ++$i)
echo ("<option value='$i'".(get_user_class() == $i ? " selected='selected'" : "").">".get_user_class_name($i)."</option>\n");

echo "</select>";
echo "</td>";
echo "</tr>";

echo "<tr><td class=\"b\"><bb>".$tracker_lang['is_write_post'].": </b></td><td class=\"a\"><select name='writeclass'>";

$maxclass = get_user_class();
for ($i = 0; $i <= $maxclass; ++$i)
echo ("<option value='$i'".(get_user_class() == $i ? " selected='selected'" : "").">".get_user_class_name($i)."</option>\n");

echo "</select></td></tr>";

echo "<tr><td class=\"b\"><b>".$tracker_lang['is_creat_post'].": </b></td><td class=\"a\"><select name='createclass'>";

$maxclass = get_user_class();
for ($i = 0; $i <= $maxclass; ++$i)
echo ("<option value='$i'".(get_user_class() == $i ? " selected='selected'" : "").">".get_user_class_name($i)."</option>\n");

echo "</select></td></tr>";

echo "<tr><td class=\"b\"><bb>".$tracker_lang['sorting']."</b></td><td class=\"a\"><select name=\"sort\">";

$res = sql_query ("SELECT sort FROM forums");
$nr = mysql_num_rows($res);
$maxclass = $nr + 1;
for ($i = 0; $i <= $maxclass; ++$i)
echo ("<option value='$i'>$i </option>\n");

echo "</select></td></tr>";

echo "<tr><td class=\"b\"><b>".$tracker_lang['hide_topic']."</b></td><td class=\"a\"><input type=radio name=visible value='yes' checked> ".$tracker_lang['no']."<input type=radio name=visible value='no'/> ".$tracker_lang['yes']." </td></tr>";

echo "<tr><td class=a align=\"center\" colspan=\"2\"><input type=\"submit\" name=\"submit\" value=\"".$tracker_lang['is_creat_post']."\" class=\"btn\" /></td></tr>";
echo "</table>";
echo "</form>";

end_main_frame();
stdfoot();
die;
}
break;



case 'takeadd': {

if (!$_POST['name'] && !$_POST['desc']) {
header("Location: forummanage.php");
die();
}

sql_query("INSERT INTO forums (sort, name,  description,  minclassread,  minclasswrite, minclasscreate,visible) VALUES (".sqlesc(intval($_POST['sort'])).", ".sqlesc(htmlspecialchars_uni($_POST['name'])).", ".sqlesc(htmlspecialchars_uni($_POST['desc'])). ", ".sqlesc(intval($_POST['readclass'])). ", ".sqlesc(intval($_POST['writeclass'])).", ".sqlesc(intval($_POST['createclass'])).", ".sqlesc(($_POST['visible'] == "yes" ? "yes" : "no")).")");

unsql_cache("forums_cat");
unsql_cache("forums.main");
unsql_cache("forums.id_name");

if(mysql_affected_rows() === 1) {
header("Refresh: 3; url=forummanage.php");
stderr($tracker_lang['success'], $tracker_lang['news_added']." <a href='forummanage.php'>".$tracker_lang['back_inlink']."</a>");
} else {
header("Refresh: 5; url=forummanage.php");
stderr($tracker_lang['error'], $tracker_lang['unknown']." <a href='forummanage.php'>".$tracker_lang['back_inlink']."</a>");
}

die;
}
break;


default: {

stdhead($tracker_lang['admining_forum']);


begin_main_frame();

echo '<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">';


echo "<tr><td class='a' align='center' colspan='5'>";
echo "<form method=\"get\" action=\"forummanage.php?action=add\">
<input type=\"hidden\" name=\"action\" value=\"add\">
<input type=\"submit\" title=\"".$tracker_lang['creating_category']."\" value=\"".$tracker_lang['creating_category']."\" class=\"btn\" />
</form>";
echo "</tr></td>";

echo "<tr>
<td class='colhead' align='left'>".$tracker_lang['name']."</td>
<td class='colhead' align='center'>".$tracker_lang['topics_many']." / ".$tracker_lang['message_many']."</td>
<td class='colhead' align='center'>".$tracker_lang['min_class']."</td>
<td class='colhead' align='center'>".$tracker_lang['action']."</td>
</tr>";

$result = sql_query("SELECT * FROM forums ORDER BY sort ASC") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($result) > 0) {
$n = 0;
while ($row = mysql_fetch_assoc($result)){


if ($n%2==0){
$cl1 = "class=a";
$cl2 = "class=b";
} else {
$cl2 = "class=a";
$cl1 = "class=b";
}

$forums = sql_query("SELECT t.forumid, count(DISTINCT p.topicid) AS topics, count(*) AS posts 
FROM posts p 
LEFT JOIN topics t ON t.id = p.topicid 
LEFT JOIN forums f ON f.id = t.forumid 
WHERE t.forumid = ".sqlesc($row["id"])." GROUP BY t.forumid") or sqlerr(__FILE__, __LINE__);

while ($forum = mysql_fetch_assoc($forums)) {
$topiccount = number_format($forum['topics']);
$postcount = number_format($forum['posts']);
}

if (empty($postcount))
$postcount=0;
if (empty($topiccount))
$topiccount=0;



echo "<tr>
<td ".$cl1."><a class=\"alink\" href='forums.php?action=viewforum&forumid=".$row["id"]."'><b>".htmlspecialchars($row["name"])."</b></a><br /><small>".htmlspecialchars($row["description"])."</small></td>
<td ".$cl2." align=\"center\">$topiccount / $postcount</td>
<td ".$cl1.">
<b>".$tracker_lang['is_read_post']."</b>: <font color=\"#".get_user_rgbcolor($row["minclassread"], $user_name)."\">".get_user_class_name($row["minclassread"])."</font><br />
<b>".$tracker_lang['is_write_post']."</b>: <font color=\"#".get_user_rgbcolor($row["minclasswrite"], $user_name)."\">".get_user_class_name($row["minclasswrite"])."</font><br />
<b>".$tracker_lang['is_creat_post']."</b>: <font color=\"#".get_user_rgbcolor($row["minclasscreate"], $user_name)."\">".get_user_class_name($row["minclasscreate"])."</font></td>
<td ".$cl2." align='center'><b><a class=\"alink\" href=\"forummanage.php?action=edit&id=".$row["id"]."\">".$tracker_lang['edit']."</a><br /><a class=\"alink\" href=\"forummanage.php?action=delete&id=".$row["id"]."\"><font color='red'>".$tracker_lang['delete']."</font></a></b></td></tr>";

unset($postcount);
unset($topiccount);
++$n;
}

echo "<tr><td class=\"b\" align=\"center\" colspan=\"4\"><a class=\"alink\" href=\"forums.php\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

} else
echo "<tr><td>".$tracker_lang['sum_nodata']."</td></tr>";

echo "</table>";

end_main_frame();
stdfoot();
die;
}


}


?>