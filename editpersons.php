<?

/**
 * @author 7Max7
 * @copyright 2011
 */

require_once("include/bittorrent.php");
dbconn(false);

if (get_user_class() < UC_MODERATOR) {
stderr($tracker_lang['persons_movie'], $tracker_lang['access_denied']);
die();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$update = array();

$id = (isset($_POST["id"]) ? intval($_POST["id"]):0);

if (empty($id) || !is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$res = sql_query("SELECT aname FROM actors WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$arr = mysql_fetch_assoc($res);

$bio = (isset($_POST["bio"]) ? trim($_POST["bio"]):'');
$aname = (isset($_POST["aname"]) ? trim($_POST["aname"]):'');

$images = (isset($_POST["images"]) ? trim($_POST["images"]):'');

$gender = (isset($_POST["gender"]) ? (string) $_POST["gender"]:'m');
$lang = (isset($_POST["lang"]) ? (string) $_POST["lang"]:'rus');

$year = (isset($_POST["year"]) ? (int) $_POST["year"]:'0000');
$month = (isset($_POST["month"]) ? (int) $_POST["month"]:'00');
$day = (isset($_POST["day"]) ? (int) $_POST["day"]:'00');
$alldate = $year."-".$month."-".$day;

if (!preg_match("#^([0-9\+]{4})-([0-9\+]{2})-([0-9\+]{2})$#", $alldate))
$alldate = '0000-00-00';

if (in_array($lang, array("rus", "eng")))
$update[] = "lang = ".sqlesc($lang);

if (in_array($gender, array("f", "m")))
$update[] = "gender = ".sqlesc($gender);

if (!empty($images)){
$expl = explode("\n", $images);

$array_ima = array();
if (count($expl)){
foreach ($expl AS $ima){
if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $ima))
$array_ima[] = $ima;
}
}

if (count($array_ima))
$update[] = "images = ".sqlesc(implode("\n", $array_ima));
else
$update[] = "images = ''";

}

if (empty($aname) || empty($bio))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": 
".(empty($aname) ? $tracker_lang['person_aname']:"")."
".(empty($bio) ? $tracker_lang['description']:"")."
");

$update[] = "aname = ".sqlesc(htmlspecialchars_uni($aname));
$update[] = "bio = ".sqlesc(htmlspecialchars_uni(addslashes($bio)));

sql_query("UPDATE actors SET ".implode(", ", $update)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

header("Location: persons.php?n=".$arr["aname"]);
die;

}


$id = (isset($_GET["id"]) ? intval($_GET["id"]):0);
if (empty($id) || !is_valid_id($id))
stderr($tracker_lang['persons_movie'], $tracker_lang['invalid_id_value']);


stdhead($tracker_lang['persons_movie']);

$res = sql_query("SELECT * FROM actors WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

echo "<form method=\"post\" action=\"editpersons.php\"><table class=\"embedded\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=\"colhead\" align=\"center\" colspan=\"12\">".$tracker_lang['persons_movie']." / ".$tracker_lang['editing']."</td></tr>";

if (mysql_num_rows($res) == 0) {

echo "<tr><td class=\"b\" align=\"center\" colspan=\"12\">".$tracker_lang['no_data_now']."</td></tr>";

echo "</table></form>";

stdfoot();
die;
}

$arr = mysql_fetch_assoc($res);

list ($year1, $month1, $day1) = explode('-', $arr["birthday"]);

echo "<tr><td class=\"b\">".$tracker_lang['person_aname']."</td><td class=\"a\" align=\"left\"><input type=\"text\" size=\"52\" name=\"aname\" value=\"".htmlspecialchars($arr["aname"])."\"> max 256</td></tr>";


$year .= "<select name='year'><option value=\"0000\">".$tracker_lang['choose'].": ".$tracker_lang['my_year']."</option>\n";

$i = date("Y")-200;

while($i <= (date('Y',time())-1)) {
$year .= "<option value='".$i."' ".((!empty($year1) && $year1 == $i) ? "selected":"").">".$i." (".(date("Y")-$i).")</option>\n";
++$i;
}
$year .= "</select>\n";

$birthmonths = array(
"01" => $tracker_lang['my_months_january'],
"02" => $tracker_lang['my_months_february'],
"03" => $tracker_lang['my_months_march'],
"04" => $tracker_lang['my_months_april'],
"05" => $tracker_lang['my_months_may'],
"06" => $tracker_lang['my_months_june'],
"07" => $tracker_lang['my_months_jule'],
"08" => $tracker_lang['my_months_august'],
"09" => $tracker_lang['my_months_september'],
"10" => $tracker_lang['my_months_october'],
"11" => $tracker_lang['my_months_november'],
"12" => $tracker_lang['my_months_december'],
);

$month = "<select name=\"month\"><option value=\"00\">".$tracker_lang['choose'].": ".$tracker_lang['my_month']."</option>\n";
foreach ($birthmonths as $month_no => $show_month) {
$month .= "<option value='$month_no' ".((!empty($month1) && $month1 == $month_no) ? "selected":"").">$show_month</option>\n";
}
$month .= "</select>\n";

$day .= "<select name='day'><option value=\"00\">".$tracker_lang['choose'].": ".$tracker_lang['my_day']."</option>\n";
$i = 1;
while ($i <= 31) {

if ($i < 10)
$day .= "<option value='0".$i. "' ".((!empty($day1) && $day1 == "0".$i) ? "selected":"").">0".$i."</option>\n";
else 
$day .= "<option value='".$i."' ".((!empty($day1) && $day1 == $i) ? "selected":"").">".$i."</option>\n";

++$i;
}
$day .="</select>\n";


echo "<tr><td class=\"b\">".$tracker_lang['my_birthday']."</td><td class=\"a\" align=\"left\">".$year.$month.$day." ".($arr["birthday"] <> '0000-00-00' ? "(".$arr["birthday"].")":"")."</td></tr>";

echo "<tr><td class=\"b\">".$tracker_lang['images']."</td><td class=\"a\" align=\"left\"><textarea cols='75' rows='6' name='images'>".htmlspecialchars($arr["images"])."</textarea></td></tr>";

echo "<tr><td class=\"b\">".$tracker_lang['gender_person']."</td><td class=\"a\" align=\"left\"><label><input type='radio' name='gender' value='m' ".($arr["gender"] == "m" ? "checked" : "")." />".$tracker_lang['person_male']."</label> <label><input type='radio' name='gender' value='f' ".($arr["gender"] == "f" ? "checked" : "")." /> ".$tracker_lang['person_female']."</label></td></tr>";
echo "<tr><td class=\"b\">".$tracker_lang['type_person']."</td><td class=\"a\" align=\"left\"><label><input type='radio' name='lang' value='eng' ".($arr["lang"] == "eng" ? "checked" : "")." />".$tracker_lang['person_rbroad']." </label> <label><input type='radio' name='lang' value='rus' ".($arr["lang"] == "rus" ? "checked" : "")." /> ".$tracker_lang['person_russian']."</label></td></tr>";


echo "<tr><td align=\"left\" colspan=\"2\" valign=\"top\">";
textbbcode("edit", "bio", htmlspecialchars($arr["bio"]));
echo "</td></tr>";

echo "<tr><td align=\"right\" colspan=\"2\" �lass=\"b\"><input type=\"hidden\" name=\"action\" value=\"edit\" /><input type=\"hidden\" name=\"id\" value='".$id."' /> <input value=\"".$tracker_lang['edit']."\" class=\"btn\" type='submit'></td></tr>";

echo "</table></form>";

stdfoot();
?>