<?
if (!defined('BLOCK_FILE')) {
 Header("Location: ../index.php"); 
 exit;
}


global $CURUSER, $DEFAULTBASEURL, $tracker_lang, $SITE_Config;

$blocktitle = $tracker_lang['welcome_back'].($CURUSER ? "<a class=\"copyright\" href=\"".$DEFAULTBASEURL."/userdetails.php?id=".$CURUSER["id"]."\">".$CURUSER["username"]."</a>" : $tracker_lang['guest']);

$ip = getip();

if ($CURUSER) {

$uped = mksize($CURUSER['uploaded']);
$downed = mksize($CURUSER['downloaded']);

if ($CURUSER["downloaded"] > 0) {
$ratio = $CURUSER['uploaded'] / $CURUSER['downloaded'];
$ratio = number_format($ratio, 3);
$color = get_ratio_color($ratio);
if ($color)
$ratio = "<font color=$color>$ratio</font>";
}
else
if ($CURUSER["uploaded"] > 0)
$ratio = "Inf.";
else
$ratio = "---";

$content .= "<center><a href=\"".$DEFAULTBASEURL."/my.php\"><img class=\"avatars\" src=\"".$DEFAULTBASEURL."/pic/avatar/".($CURUSER["avatar"] ? $CURUSER["avatar"] : "default_avatar.gif")."\" width=\"100\" alt=\"".$tracker_lang['click_on_avatar']."\" title=\"".$tracker_lang['click_on_avatar']."\" border=\"0\" /></a></center>";

if ($CURUSER && $CURUSER["override_class"] <> "255")
$content .="<center><nobr><b>[</b>".$tracker_lang['class_default']."<b>]</b><br /><b>".get_user_class_color($CURUSER["override_class"], get_user_class_name($CURUSER['override_class']))."</b></nobr></center><br />";

$unread = $CURUSER["unread"];
$newmessage1 = ($unread > 1 ? $tracker_lang['message_news'] : $tracker_lang['message_new']); 
$newmessage2 = ($unread > 1 ? $tracker_lang['message_many'] : $tracker_lang['message_one']); 
$newmessage = $newmessage1."<br />".$newmessage2; 

if (!empty($unread))
$list = "<span style=\" ".($CURUSER["stylesheet"]=="black_night" ? "":"BACKGROUND-COLOR: #efedef;")." BORDER:silver 1px solid;  DISPLAY:block;  COLOR:#00f;  MARGIN:2px 1px;  PADDING:2px 2px 2px 6px;  TEXT-DECORATION:none;\"><b><a href=\"message.php?action=new\">".sprintf($tracker_lang['new_pms'], $unread, $newmessage)."</a></b></span>";
else
$list = "<span style=\" style=\" ".($CURUSER["stylesheet"]=="black_night" ? "":"BACKGROUND-COLOR: #efedef;")." BORDER:silver 1px solid; DISPLAY:block; COLOR:#00f;  MARGIN:1px 1px; PADDING:2px 2px 2px 6px; TEXT-DECORATION:none;\"><a href=\"message.php?action=viewmailbox&box=1\"><img style=\"border:none\" title=\"".$tracker_lang['inbox']." ".$tracker_lang['messages']."\" src=\"pic/pn_inbox.gif\"></a>&nbsp;&nbsp; <a href=\"message.php?action=viewmailbox&box=-1\"><img style=\"border:none\" title=\"".$tracker_lang['outbox']." ".$tracker_lang['messages']."\" src=\"pic/pn_sentbox.gif\"></a></span>";


?>
<script>
function user_online() {
jQuery.post("block-user_jquery.php" ,  {"action":"message"} , function(response) {
jQuery("#user_online").html(response);
}, "html");
setTimeout("user_online();", 60000);
}
user_online();
</script>
<?

$content.= '<nobr><div align="center" id="user_online">'.$list.'</div></nobr>';

$content.= $tracker_lang['class'].": &nbsp;".get_user_class_color($CURUSER['class'], get_user_class_name($CURUSER['class']));

if (get_user_class() >= UC_MODERATOR && ($CURUSER['override_class'] == 255))
$content.= "<a href=\"".$DEFAULTBASEURL."/setclass.php\"><img style=\"border:none\" alt=\"".$tracker_lang['test_class']."\" src=\"pic/forum.gif\"></a>";

if ($CURUSER['override_class'] <> 255 && $CURUSER)
$content.= "<a href=\"".$DEFAULTBASEURL."/restoreclass.php\"><img style=\"border:none\" title=\"".$tracker_lang['testing_class']."\" src=\"pic/megs.gif\"></a>";
 
if ($CURUSER["hiderating"] == "yes") {

$content.= "<br /><font color=\"1900D1\">".$tracker_lang['ratio'].": </font><font color=\"ff1ac6\"><b>+100%</b></font><br />";
$hideratinguntil = $CURUSER['hideratinguntil'];

}

else

$content .= "<br />
<font color=\"1900D1\">".$tracker_lang['ratio'].": ".$ratio."</font><br />
<font color=\"green\">".$tracker_lang['uploaded'].": ".$uped."</font><br />
<font color=\"darkred\">".$tracker_lang['downloaded'].": ".$downed."</font><br />";

$content.= ($CURUSER["bonus"] == "0.00" ? "<font color=\"#0670ca\">".$tracker_lang['my_bonus'].": ".$CURUSER["bonus"]."</font>" : "<font color=\"#0670ca\">".$tracker_lang['my_bonus'].": <a title=\"".$tracker_lang['change_bonus']."\" href=\"".$DEFAULTBASEURL."/mybonus.php\">".$CURUSER["bonus"]."</a></font><br/>");

?>
<script>
function usersen_online() {
jQuery.post("block-user_jquery.php" , {"action":"thanks"} , function(response) {
jQuery("#usersen_online").html(response);
}, "html");
setTimeout("usersen_online();", 180000);
}
usersen_online();
</script>
<?

$content.= '<div id="usersen_online"></div>';

if (!empty($CURUSER["invites"]))
$content.= "<font color=\"DarkCyan\">".$tracker_lang['my_invite'].": <a href=\"".$DEFAULTBASEURL."/invite.php\">" .$CURUSER["invites"]. "</a> ".$tracker_lang['pcs']."</font><br />\n";


if (!empty($CURUSER["unmark"]))
$content.= "<font title=\"".$tracker_lang['click_on_votes']."\" color=\"#A0522D\">".$tracker_lang['estimate'].": <a href=\"".$DEFAULTBASEURL."/rating.php\">" .$CURUSER["unmark"]. "</a> ".$tracker_lang['pcs']."</font><br />\n";

if (!empty($CURUSER["unseed"]))
$content.= "<font title=\"".$tracker_lang['click_on_seeding']."\" color=\"#FF552A\">".$tracker_lang['my_seeding'].": <a href=\"".$DEFAULTBASEURL."/upload.php\">" .$CURUSER["unseed"]. "</a> ".$tracker_lang['pcs']."</font><br />\n";


#if (!empty($CURUSER["unmatch"]))
$content.= "<font title=\"".$tracker_lang['mematch']."\" color=\"#F81515\">".$tracker_lang['mematch'].": <a href=\"".$DEFAULTBASEURL."/mematch.php\">" .$CURUSER["unmatch"]. "</a> ".$tracker_lang['pcs']."</font><br />\n";


if (empty($CURUSER["question"]) && empty($CURUSER["rejoin"]))
$content.= "<font title=\"".$tracker_lang['click_on_secret']."\" color=\"purple\">".$tracker_lang['sec_question'].": <a href=\"".$DEFAULTBASEURL."/my.php#question\">".$tracker_lang['no']."</a></font><br />";

if (!empty($CURUSER["num_warned"])) {
$img = "";

for ($i = 1; $i <= $CURUSER["num_warned"]; $i++) {

$img.= "<a href=\"mybonus.php\" target=\"_blank\"><img src=\"".$DEFAULTBASEURL."/pic/warned.gif\" style=\"border:none\" alt=\"".$tracker_lang['num_warned']." - ".$i."\" title=\"".$tracker_lang['num_warned']." - ".$i."\"></a>"; 
}

$num_warned_view= "<center>".$img."</center>";
}

?>
<script>
function userseed_online() {
jQuery.post("block-user_jquery.php" , {"action":"peers"} , function(response) {
jQuery("#userseed_online").html(response);
}, "html");
setTimeout("userseed_online();", 180000);
}
userseed_online();
</script>
<?

$content.= '<div id="userseed_online"></div>';



$ss_r = sql_query("SELECT * FROM stylesheets ORDER by id ASC", $cache = array("type" => "disk", "file" => "my_style", "time" => 86400)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows_($ss_r) > 1){
$content.= "<center><form method=\"post\" action=\"\" name=\"jump\"><select name=id_styles onchange=\"if(this.options[this.selectedIndex].value != -1){forms['jump'].submit()}\">\n";

while ($arr = mysql_fetch_assoc_($ss_r))
$content.= "<option value=".$arr["id"].($CURUSER["stylesheet"] == $arr["name"] ? " selected>" : ">").$arr["name"]."</option>\n";

$content.= "</select>\n";
$content.= "</form></center>";
}




$content.= $num_warned_view."<center><form method=\"post\" action=\"logout.php\"><input onClick=\"return confirm('".$tracker_lang['confirm_logout']."')\" type=\"submit\" style=\"width: 100px\" class=\"btn\" value=\"".$tracker_lang['logout']."\"></form></center>";

} else {

if (!empty($_GET["returnto"]))
$returnto = htmlspecialchars($_GET["returnto"]);

if (isset($returnto))
$return_inp = "<input type=\"hidden\" name=\"returnto\" value=\"".htmlentities($returnto)."\"/>";




$agent = $_SERVER["HTTP_USER_AGENT"];
$hache = crc32(htmlentities($agent));

if (!empty($ip) && !isset($_GET["no_mylogins"]) && !empty($hache)){

$dti = get_date_time(gmtime() - 1209600);//// 2 ������

$du = sql_query("SELECT username FROM users WHERE crc = ".sqlesc($hache)." AND ip = ".sqlesc($ip)." AND last_access >= ".sqlesc($dti)." LIMIT 1") or sqlerr(__FILE__,__LINE__);
$rodu = mysql_fetch_array($du);

$value = " value=\"".$rodu['username']."\"";
}


$content.= "<noindex><form method=\"post\" action=\"takelogin.php\"><table border=\"0\" width=\"100%\">";


if ($SITE_Config["maxlogin"] == true) {

$remaining = remaining();
if (!empty($remaining))
$rema = "<b>[".$remaining."]</b> ".$tracker_lang['from']." <b>[".$SITE_Config["maxlogin_limit"]."]</b>";
else
$rema = sprintf($tracker_lang['banned_ip_maxlogin'], getip());

$content.= "<tr><td align=\"center\" class=\"b\">".$rema."</td></tr>";

}


if (!empty($remaining)){

$content.= "<tr><td align=\"left\" class=\"a\"><b>".$tracker_lang['enter']." ".$tracker_lang['username']."</b>:</td></tr>";
$content.= "<tr><td class=\"b\"><input id=\"nickname\" type=\"text\" size=\"18\" name=\"username\" ".$value." class=\"login\"/></td></tr>";


$content.= "<tr><td align=\"left\" class=\"a\"><b>".$tracker_lang['enter']." ".$tracker_lang['password']."</b>:</td></tr>";
$content.= "<tr><td class=\"b\"><input id=\"password\" type=\"password\" size=\"18\" name=\"password\" class=\"pass\"/></td></tr>";

$content.= "<tr><td class=\"b\" align=\"center\">".(isset($return_inp) ? $return_inp:"")."<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['enter_me']."\"></td></tr>";

}

$content.= "<tr><td class=\"a\"><a class=\"menu\" href=\"signup.php\">".$tracker_lang['signup']."</a></td></tr>";
$content.= "<tr><td class=\"a\"><a class=\"menu\" href=\"recover.php\">".$tracker_lang['recover']."</a></td></tr>";

$content.= "</table></form></noindex>";

}



?>