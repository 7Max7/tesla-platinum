<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $CURUSER, $tracker_lang;

$blocktitle = ".:: <a title=\"".$tracker_lang['forum_main']."\" class=\"altlink_white\" href='forums.php'>".$tracker_lang['forum_main']."</a> :: <a title=\"".$tracker_lang['search']."\" class=\"altlink_white\" href='forums.php?action=search'>".$tracker_lang['search']."</a> :: <a title=\"".$tracker_lang['mail_unread_desc']."\" class=\"altlink_white\" href='forums.php?action=viewunread'>".$tracker_lang['mail_unread_desc']."</a> :: <a title=\"".$tracker_lang['forum_uniread']."\" class=\"altlink_white\" href='forums.php?action=catchup'>".$tracker_lang['forum_uniread']."</a> ::.";


if ($CURUSER && !empty($CURUSER["class"]))
$curuserclass = get_user_class();
else
$curuserclass = 1;

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-forum_light_".$curuserclass, "time" => 60*60, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

$content = "<table align=center cellpadding=0 cellspacing=0 width=100%>";
$content.= "<tr>";

$content.= "
<td class = \"colhead\" style=\"padding-right: 5px;\" align=\"center\" width=\"70%\">".$tracker_lang['subject']."</td>
<td class = \"colhead\" align=\"left\" width=\"10%\">&nbsp;".$tracker_lang['answers']."</td>
<td class = \"colhead\" align=\"left\" width=\"10%\">&nbsp;".$tracker_lang['views']."</td>
";

$content.= "</tr>";

if ($CURUSER && !empty($CURUSER["class"]))
$curuserclass = get_user_class();
else
$curuserclass = 1;

$for = sql_query("SELECT ft.*, ff.name as forumname, ff.description, ff.minclassread, (SELECT COUNT(*) FROM posts WHERE topicid = ft.id) AS post_num FROM topics as ft, forums as ff WHERE ff.id = ft.forumid AND ft.visible = 'yes' AND ff.visible = 'yes' AND ff.minclassread <= ".sqlesc($curuserclass)." ORDER BY lastpost DESC LIMIT 10")or sqlerr(__FILE__, __LINE__);

while ($topicarr = mysql_fetch_assoc($for)) {

$polls_view = ($topicarr["polls"] == "yes" ? " <img width='13' title=\"".$tracker_lang['poll']."\" src=\"pic/forumicons/polls.gif\">":"");
    	 
$posts = $topicarr["post_num"];
$postsperpage=20;
$tpages = floor($posts / $postsperpage);

if ($tpages * $postsperpage <> $posts)
$tpages++;

if ($tpages > 1) {
$topicpages = " [";
for ($i = 1; $i <= $tpages; ++$i){
$topicpages.= "".($i==1 ? "":" ")."<a href='forums.php?action=viewtopic&topicid=".$topicarr["id"]."&page=".$i."'>".$i."</a>".($i<> $tpages ? "":"")."";
}
$topicpages.= "]";
}  else
$topicpages = "";
    	
$topicid = $topicarr["id"];

$topic_userid = $topicarr["userid"];
$views = $topicarr["views"];
$sticky = $topicarr["sticky"];
$lastpost = $topicarr["lastpost"];
$posts = $topicarr["post_num"]; /// 
$replies = max(0, $posts - 1);


$subject = "<a href=\"forums.php?action=viewtopic&topicid=$topicid&page=last#$lastpost\"><font color=\"#000000\">".htmlspecialchars($topicarr["subject"])."</font></a> -> <a title=\"".$topicarr["description"]."\" href=\"forums.php?action=viewforum&forumid=$topicarr[forumid]\"><font color=\"#808080\">".htmlspecialchars($topicarr["forumname"])."</font></a>";

$content.= "<tr>
<td class=\"b\" style=\"padding-right: 5px;\" align = \"left\">".($sticky=="yes" ? "<b>".$tracker_lang['stickyf']."</b>: ":"").$subject.$topicpages.$polls_view."</td>
<td class=\"b\" align = \"left\">".$replies."</td>
<td class=\"b\" align = \"left\">".$views."</td>
</tr>";

}

$content.= "</td></tr></tr>";
$content.= "</table>\n";


file_query($content, $cache = array("type" => "disk", "file" => "block-forum_light_".$curuserclass, "time" => 60*60, "action" => "set"));
}

?>