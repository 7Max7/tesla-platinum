<? 
require "include/bittorrent.php";
dbconn(false, true);

header("Content-Type: text/html; charset=" . $tracker_lang['language_charset']);

global $CURUSER, $Torrent_Config;

if (empty($CURUSER)) die;

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && $_SERVER["REQUEST_METHOD"] == 'POST') {

$act = (string) $_POST["act"]; /// ��������
$user = (int) $_POST["user"]; /// ��������

if (empty($user) || empty($act))
die($tracker_lang['error_data'].", ".$tracker_lang['invalid_id_value']);


if ($act == "info") { /// ��, ��� �� ���������

$csql_ino = sql_query("SELECT info, signature FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__); 
$user = mysql_fetch_array($csql_ino);

if (!empty($user["info"]))
echo format_comment($user["info"],true);

if ($user["info"] && $user["signature"])
echo "<hr>";

if (!empty($user["signature"]))
echo format_comment($user["signature"]);

if (empty($user["info"]) && empty($user["signature"]))
echo ($tracker_lang['no_data_now']." (info)");

} /// ���������, ��������.


elseif ($act == "maxlogin" && ($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR)) { /// ��, ��� �� ���������

$arru = sql_query("SELECT ip, class FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_assoc($arru);

if (mysql_num_rows($arru) == 0)
die($tracker_lang['no_user_intable']);

$sql_max = sql_query("SELECT id, ip, added, comment FROM loginattempts", $cache = array("type" => "disk", "file" => "maxlogin_all", "time" => 60*60)) or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows_($sql_max) == 0)
die($tracker_lang['no_data_now']." (".$act.")");

echo "<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"a\" colspan=\"4\">".$tracker_lang['tabs_imax']."</td></tr>";

echo "<tr>
<td class=\"colhead\" width=\"5%\">#</td>
<td class=\"colhead\">".$tracker_lang['user_ip']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['clock']."</td>
<td class=\"colhead\" width=\"10%\" align=\"center\">".$tracker_lang['number_all']."</td>
</tr>\n";

while ($login = mysql_fetch_assoc_($sql_max)){

$exp = explode(",", $login["comment"]);

$user_keys = array_keys($exp, $user);

if ($login["ip"] == $row["ip"] || count($user_keys))
echo "<tr>
<td class=\"b\">".$login["id"]."</td>
<td class=\"a\">".$login["ip"]."</td>
<td class=\"b\" align=\"center\">".$login["added"]." <br /> ".get_elapsed_time(sql_timestamp_to_unix_timestamp($login["added"]), false, true)." ".$tracker_lang['ago']."</td>
<td class=\"a\" align=\"center\">".(count($user_keys) ? count($user_keys):count($exp))." ".$tracker_lang['times']."</td>
</tr>\n";

}

echo "</table>";

} /// ���������, ��������.


elseif ($act=="uploaded") {

$csql_ino = sql_query("SELECT username, class FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__); 
$user_a = mysql_fetch_assoc($csql_ino);

$r = sql_query("SELECT torrents.id, torrents.name, torrents.size, (torrents.leechers+torrents.f_leechers) AS leechers, (torrents.seeders+torrents.f_seeders) AS seeders, torrents.added, torrents.category, categories.name AS catname, categories.image AS catimage, categories.id AS catid FROM torrents LEFT JOIN categories ON torrents.category = categories.id WHERE owner = ".sqlesc($user)." ORDER BY added DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);

$d0=0;
if (mysql_num_rows($r) > 0) {

$d0=1;
echo "<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">

<tr><td class=\"a\" colspan=\"6\" align=\"left\"><a title=\"".$tracker_lang['arr_searched']."\" href=\"browse.php?userid=".$user."\">".$tracker_lang['userid_searched'].": ".get_user_class_color($user_a["class"], $user_a["username"])."</a></td></tr>

<tr>
<td class=\"colhead\">".$tracker_lang['type']."</td>
<td class=\"colhead\">".$tracker_lang['name']."</td>
".($Torrent_Config["use_ttl"] == true ? "<td class=\"colhead\" align=\"center\">".$tracker_lang['ttl']."</td>" : "")."
<td class=\"colhead\" align=\"center\">".$tracker_lang['tracker_leechers']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['tracker_seeders']."</td>
<td class=\"colhead\">".$tracker_lang['size']."</td>
</tr>
\n";

while ($a = mysql_fetch_assoc($r)) {
$size = mksize($a["size"]);

$ttl = ($Torrent_Config["ttl_days"]*24) - floor((gmtime() - sql_timestamp_to_unix_timestamp($a["added"])) / 3600);
if ($ttl == 1) $ttl .= "&nbsp;".$tracker_lang['hour']; else $ttl .= "&nbsp;".$tracker_lang['hours'];

echo "<tr>
<td><a href=\"browse.php?cat=".$a["catid"]."\"><img class=effect onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" src=\"pic/cats/".$a["catimage"]."\" alt=\"".$a["catname"]."\" border=\"0\" /></a></td>
<td><a href=\"details.php?id=" . $a["id"] . "\"><b>" . $a["name"] . "</b></a></td>
".($Torrent_Config["use_ttl"] == true ? "<td align=\"center\">".$ttl."</td>" : "")."
<td align=\"center\">".$a["seeders"]."</td>
<td align=\"center\">".$a["leechers"]."</td>
<td align=\"center\">".$size."</td>
</tr>";

}
echo "</table>";
}

if (empty($d0))
die($tracker_lang['no_data_now']." (".$act.")");

}///// ����� uploaded


elseif ($act=="mdbyme") {

$r = sql_query("SELECT torrents.id, torrents.name, torrents.size, (torrents.leechers+torrents.f_leechers) AS leechers, (torrents.seeders+torrents.f_seeders) AS seeders, torrents.added, torrents.category, categories.name AS catname, categories.image AS catimage, categories.id AS catid FROM torrents LEFT JOIN categories ON torrents.category = categories.id WHERE owner = ".sqlesc($user)." AND moderatedby = ".sqlesc($user)." ORDER BY added DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);
$d0=0;
if (mysql_num_rows($r) > 0) {
$d0=1;
echo "<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr>
<td class=\"colhead\">".$tracker_lang['type']."</td>
<td class=\"colhead\">".$tracker_lang['name']."</td>
".($Torrent_Config["use_ttl"] == true ? "<td class=\"colhead\" align=\"center\">".$tracker_lang['ttl']."</td>" : "")."
<td class=\"colhead\" align=\"center\">".$tracker_lang['tracker_leechers']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['tracker_seeders']."</td>
<td class=\"colhead\">".$tracker_lang['size']."</td>
</tr>\n";

echo "<tr><td class=a colspan=\"5\">".$tracker_lang['tabs_imdbyme']." ".sprintf($tracker_lang['limit_ofn'], 500)."</td></tr>\n";

while ($a = mysql_fetch_assoc($r)) {
$size = mksize($a["size"]);

$ttl = ($Torrent_Config["ttl_days"]*24) - floor((gmtime() - sql_timestamp_to_unix_timestamp($a["added"])) / 3600);
if ($ttl == 1) $ttl .= "&nbsp;".$tracker_lang['hour']; else $ttl .= "&nbsp;".$tracker_lang['hours'];

echo "<tr>
<td><a href=\"browse.php?cat=".$a["catid"]."\"><img class=effect onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" src=\"pic/cats/".$a["catimage"]."\" alt=\"".$a["catname"]."\" border=\"0\" /></a></td>
<td><a href=\"details.php?id=" . $a["id"] . "\"><b>" . $a["name"] . "</b></a></td>
".($Torrent_Config["use_ttl"] == true ? "<td align=\"center\">".$ttl."</td>" : "")."
<td align=\"center\">".$a["seeders"]."</td>
<td align=\"center\">".$a["leechers"]."</td>
<td align=\"center\">".$size."</td>
</tr>\n";

}
  echo "</table>";
}

if (empty($d0))
die($tracker_lang['no_data_now']." (".$act.")");

}///// ����� mdbyme



elseif ($act=="snatched") {
	

$r = sql_query("SELECT snatched.torrent AS id, snatched.uploaded, snatched.seeder, snatched.downloaded, snatched.startdat, snatched.completedat, snatched.last_action, categories.name AS catname, categories.image AS catimage, categories.id AS catid, torrents.name, (torrents.leechers+torrents.f_leechers) AS leechers, (torrents.seeders+torrents.f_seeders) AS seeders 
FROM snatched
LEFT JOIN torrents ON torrents.id = snatched.torrent JOIN categories ON torrents.category = categories.id 
WHERE snatched.finished='no' AND userid = ".sqlesc($user)." ORDER BY last_action DESC LIMIT 200") or sqlerr(__FILE__,__LINE__);
$d4=0;

if (mysql_num_rows($r) > 0) {

$d4=1;

echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr>
  <td class=\"colhead\">".$tracker_lang['type']."</td>
  <td class=\"colhead\">".$tracker_lang['name']."</td>
  <td class=\"colhead\">".$tracker_lang['tracker_seeders']." / ".$tracker_lang['tracker_leechers']."</td>
  <td class=\"colhead\">".$tracker_lang['uploaded']."</td>
  <td class=\"colhead\">".$tracker_lang['downloaded']."</td>
  <td class=\"colhead\">".$tracker_lang['ratio']."</td>
  <td class=\"colhead\">".$tracker_lang['begin']." / ".$tracker_lang['completed']."</td>
  <td class=\"colhead\">".$tracker_lang['action']."</td>
  <td class=\"colhead\">".$tracker_lang['uploadeder']."</td>
</tr>";

while ($a = mysql_fetch_array($r)) {

if ($a["downloaded"] > 0) {
      $ratio = number_format($a["uploaded"] / $a["downloaded"], 3);
      $ratio = "<font color=\"" . get_ratio_color($ratio) . "\">".$ratio."</font>";
   } else
if ($a["uploaded"] > 0)
$ratio = "Infinity";
	else
$ratio = "---";

$uploaded = mksize($a["uploaded"]);
$downloaded = mksize($a["downloaded"]);

if ($a["seeder"] == 'yes')
$seeder = "<font color=\"green\">".$tracker_lang['yes']."</font>";
else
$seeder = "<font color=\"red\">".$tracker_lang['no']."</font>";


echo "<tr>
<td><a href=\"browse.php?cat=".$a["catid"]."\"><img class=effect onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" src=\"pic/cats/".$a["catimage"]."\" alt=\"".$a["catname"]."\" border=\"0\" /></a></td>
<td><a href=\"details.php?id=".$a["id"]."\"><b>".$a["name"]."</b></a></td>
<td align=\"center\">".$a["leechers"]." / ".$a["seeders"]."</td>
<td align=\"center\">".$uploaded."</td>
<td align=\"center\">".$downloaded."</td>
<td align=\"center\">".$ratio."</td><td align=\"center\">".$a["startdat"]."<br />".$a["completedat"]."</td>
<td align=\"center\">".$a["last_action"]."</td>
<td align=\"center\">".$seeder."</td>
</tr>";

}
echo "</table>";
}

if ($d4==0)
die($tracker_lang['no_data_now']." (".$act.")");


} /// ����� snatched

elseif ($act=="completed") {

$r = sql_query("SELECT snatched.torrent AS id, snatched.uploaded, snatched.seeder, snatched.downloaded, snatched.startdat, snatched.completedat, snatched.last_action, categories.name AS catname, categories.image AS catimage, categories.id AS catid, torrents.name, (torrents.leechers+torrents.f_leechers) AS leechers, (torrents.seeders+torrents.f_seeders) AS seeders FROM snatched JOIN torrents ON torrents.id = snatched.torrent JOIN categories ON torrents.category = categories.id WHERE snatched.finished='yes' AND userid = ".sqlesc($user)." ORDER BY torrent DESC LIMIT 200") or sqlerr(__FILE__,__LINE__);
	$d5=0;
 
if (mysql_num_rows($r) > 0) {

	$d5=1;
echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr>
  <td class=\"colhead\">".$tracker_lang['type']."</td>
  <td class=\"colhead\">".$tracker_lang['name']."</td>
  <td class=\"colhead\">".$tracker_lang['tracker_leechers']." / ".$tracker_lang['tracker_seeders']."</td>
  <td class=\"colhead\">".$tracker_lang['uploaded']."</td>
  <td class=\"colhead\">".$tracker_lang['downloaded']."</td>
  <td class=\"colhead\">".$tracker_lang['ratio']."</td>
  <td class=\"colhead\">".$tracker_lang['begin']." / ".$tracker_lang['completed']."</td>
  <td class=\"colhead\">".$tracker_lang['action']."</td>
  <td class=\"colhead\">".$tracker_lang['uploadeder']."</td>
</tr>";

$d=0;

while ($a = mysql_fetch_assoc($r)) {

$d=1;
if ($a["downloaded"] > 0) {
      $ratio = number_format($a["uploaded"] / $a["downloaded"], 3);
      $ratio = "<font color=\"" . get_ratio_color($ratio) . "\">".$ratio."</font>";
   } else
if ($a["uploaded"] > 0)
$ratio = "Infinity";
	else
$ratio = "---";

$uploaded = mksize($a["uploaded"]);
$downloaded = mksize($a["downloaded"]);

if ($a["seeder"] == 'yes')
$seeder = "<font color=\"green\">".$tracker_lang['yes']."</font>";
else
$seeder = "<font color=\"red\">".$tracker_lang['no']."</font>";


echo "<tr>
<td><a href=\"browse.php?cat=".$a["catid"]."\"><img class=effect onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" src=\"pic/cats/".$a["catimage"]."\" alt=\"".$a["catname"]."\" border=\"0\" /></a></td>
<td><a href=\"details.php?id=" . $a["id"] . "\"><b>" . $a["name"] . "</b></a></td>
<td align=\"center\">".$a["seeders"]." / ".$a["leechers"]."</td>
<td align=\"center\">".$uploaded."</td>
<td align=\"center\">".$downloaded."</td>
<td align=\"center\">".$ratio."</td><td align=\"center\">".$a["startdat"]."<br />".$a["completedat"]."</td>
<td align=\"center\">".$a["last_action"]."</td>
<td align=\"center\">".$seeder."</td>
</tr>";

}
echo "</table>";
}

if ($d5==0)
die($tracker_lang['no_data_now']." (".$act.")");
	
} /// ����� complete

elseif ($act=="leeching") {

$res = sql_query("SELECT torrent, added, uploaded, downloaded, torrents.name AS torrentname, categories.name AS catname, categories.id AS catid, size, image, category, seeders, leechers FROM peers LEFT JOIN torrents ON peers.torrent = torrents.id LEFT JOIN categories ON torrents.category = categories.id WHERE userid = ".sqlesc($user)." AND seeder='no' ORDER BY added DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);

  $d1=0;
  while ($arr = mysql_fetch_assoc($res)) {
  $d1=1;
    if ($arr["downloaded"] > 0)
    {
      $ratio = number_format($arr["uploaded"] / $arr["downloaded"], 3);
      $ratio = "<font color=" . get_ratio_color($ratio) . ">".$ratio."</font>";
    }
    else
      if ($arr["uploaded"] > 0)
        $ratio = "Infinity";
      else
        $ratio = "---";
    

	$catimage = htmlspecialchars($arr["image"]);
	$catname = htmlspecialchars($arr["catname"]);
    
	$ttl = ($Torrent_Config["ttl_days"]*24) - floor((gmtime() - sql_timestamp_to_unix_timestamp($arr["added"])) / 3600);
	if ($ttl == 1) $ttl .= "&nbsp;".$tracker_lang['hour']; else $ttl .= "&nbsp;".$tracker_lang['hours'];
    
	$size = mksize($arr["size"]);
	$uploaded = mksize($arr["uploaded"]);
	$downloaded = mksize($arr["downloaded"]);
	$seeders = number_format($arr["seeders"]);
	$leechers = number_format($arr["leechers"]);
    $leeching_array.="<tr>
	<td style='padding: 0px'><a href=\"browse.php?cat=".$arr["catid"]."\"><img class=effect onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" src=\"pic/cats/".$catimage."\" alt=\"".$catname."\" border=\"0\" /></a></td>
	<td><a href=details.php?id=".$arr["torrent"]."><b>" . $arr["torrentname"] ."</b></a></td>
	".($Torrent_Config["use_ttl"] == true ? "<td align=\"center\">".$ttl."</td>" : "")."
	<td align=\"center\">".$size."</td>
	<td align=\"right\">".$seeders."</td><td align=\"right\">".$leechers."</td>
	<td align=\"center\">".$uploaded."</td>
	<td align=\"center\">".$downloaded."</td>
	<td align=\"center\">".$ratio."</td>
    </tr>\n";
  }
  if ($d1==1){
  	
  echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
  <tr>
  <td class=\"colhead\" align=left>".$tracker_lang['type']."</td>
  <td class=\"colhead\">".$tracker_lang['name']."</td>
  ".($Torrent_Config["use_ttl"] == true ? "<td class=\"colhead\" align=\"center\">".$tracker_lang['ttl']."</td>" : "")."
  <td class=\"colhead\" align=\"center\">".$tracker_lang['size']."</td>
  <td class=\"colhead\" align=\"right\">".$tracker_lang['tracker_leechers']."</td>
  <td class=\"colhead\" align=\"right\">".$tracker_lang['tracker_seeders']."</td>
  <td class=\"colhead\" align=\"center\">".$tracker_lang['uploaded']."</td>
  <td class=\"colhead\" align=\"center\">".$tracker_lang['downloaded']."</td>
  <td class=\"colhead\" align=\"center\">".$tracker_lang['ratio']."</td>
  </tr>\n";
  echo $leeching_array;
  echo"</table>\n";
   }  else
  die($tracker_lang['no_data_now']." (".$act.")");
}/// ����� leeching

elseif ($act=="seeding") {

$res = sql_query("SELECT torrent, added, uploaded, downloaded, torrents.name AS torrentname, categories.name AS catname, categories.id AS catid, size, image, category, seeders, leechers FROM peers LEFT JOIN torrents ON peers.torrent = torrents.id LEFT JOIN categories ON torrents.category = categories.id WHERE userid = ".sqlesc($user)." AND seeder='yes' ORDER BY added DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);

  $d2=0;
  while ($arr = mysql_fetch_assoc($res)) {
  
  $d2=1;
    if ($arr["downloaded"] > 0)
    {
      $ratio = number_format($arr["uploaded"] / $arr["downloaded"], 3);
      $ratio = "<font color=" . get_ratio_color($ratio) . ">".$ratio."</font>";
    }
    else
      if ($arr["uploaded"] > 0)
        $ratio = "Infinity";
      else
        $ratio = "---";

	$catimage = htmlspecialchars($arr["image"]);
	$catname = htmlspecialchars($arr["catname"]);
    
	$ttl = ($Torrent_Config["ttl_days"]*24) - floor((gmtime() - sql_timestamp_to_unix_timestamp($arr["added"])) / 3600);
	if ($ttl == 1) $ttl .= "&nbsp;".$tracker_lang['hour']; else $ttl .= "&nbsp;".$tracker_lang['hours'];
    
	$size = mksize($arr["size"]);
	$uploaded = mksize($arr["uploaded"]);
	$downloaded = mksize($arr["downloaded"]);
	$seeders = number_format($arr["seeders"]);
	$leechers = number_format($arr["leechers"]);
    $leeching_array.="<tr>
	<td style='padding: 0px'><a href=\"browse.php?cat=".$arr["catid"]."\"><img class=effect onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" src=\"pic/cats/".$catimage."\" alt=\"".$catname."\" border=\"0\" /></a></td>
	<td><a href=details.php?id=".$arr["torrent"]."><b>" . $arr["torrentname"] ."</b></a></td>
	".($Torrent_Config["use_ttl"] == true ? "<td align=\"center\">".$ttl."</td>" : "")."
	<td align=\"center\">".$size."</td>
	<td align=\"right\">".$seeders."</td><td align=\"right\">".$leechers."</td>
	<td align=\"center\">".$uploaded."</td>
	<td align=\"center\">".$downloaded."</td>
	<td align=\"center\">".$ratio."</td>
    </tr>\n";
  }
  if ($d2==1){
  	
  echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
  <tr>
  <td class=\"colhead\" align=left>".$tracker_lang['type']."</td>
  <td class=\"colhead\">".$tracker_lang['name']."</td>
  ".($Torrent_Config["use_ttl"] == true ? "<td class=\"colhead\" align=\"center\">".$tracker_lang['ttl']."</td>" : "")."
  <td class=\"colhead\" align=\"center\">".$tracker_lang['size']."</td>
  <td class=\"colhead\" align=\"center\">".$tracker_lang['tracker_leechers']."</td>
  <td class=\"colhead\" align=\"center\">".$tracker_lang['tracker_seeders']."</td>
  <td class=\"colhead\" align=\"center\">".$tracker_lang['uploaded']."</td>
  <td class=\"colhead\" align=\"center\">".$tracker_lang['downloaded']."</td>
  <td class=\"colhead\" align=\"center\">".$tracker_lang['ratio']."</td>
  </tr>\n";
  echo $leeching_array;
  echo"</table>\n";
   }  else
  die($tracker_lang['no_data_now']." (".$act.")");
}/// ����� seeding

elseif ($act=="checked") {

$r = sql_query("SELECT torrents.id, torrents.name, torrents.size, (torrents.leechers+torrents.f_leechers) AS leechers, (torrents.seeders+torrents.f_seeders) AS seeders, torrents.added, torrents.category, categories.name AS catname, categories.image AS catimage, categories.id AS catid FROM torrents LEFT JOIN categories ON torrents.category = categories.id WHERE moderatedby = ".sqlesc($user)." ORDER BY added DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);

$d3=0;
if (mysql_num_rows($r) > 0) {

$d3=1;
echo "<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr>
<td class=\"colhead\">".$tracker_lang['type']."</td>
<td class=\"colhead\">".$tracker_lang['name']."</td>
".($Torrent_Config["use_ttl"] == true ? "<td class=\"colhead\" align=\"center\">".$tracker_lang['ttl']."</td>" : "")."
<td class=\"colhead\">".$tracker_lang['tracker_leechers']."</td>
<td class=\"colhead\">".$tracker_lang['tracker_seeders']."</td>
<td class=\"colhead\">".$tracker_lang['size']."</td>
</tr>\n";

while ($a = mysql_fetch_assoc($r)) {
$size = mksize($a["size"]);
$ttl = ($Torrent_Config["ttl_days"]*24) - floor((gmtime() - sql_timestamp_to_unix_timestamp($a["added"])) / 3600);
if ($ttl == 1) $ttl .= "&nbsp;".$tracker_lang['hour']; else $ttl .= "&nbsp;".$tracker_lang['hours'];

$cat = "<a href=\"browse.php?cat=".$a["catid"]."\"><img class=effect onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" src=\"pic/cats/".$a["catimage"]."\" alt=\"".$a["catname"]."\" border=\"0\" /></a>";

echo "<tr>
<td>$cat</td>
<td><a href=\"details.php?id=".$a["id"]."\"><b>".$a["name"]."</b></a></td>
".($Torrent_Config["use_ttl"] == true ? "<td align=\"center\">".$ttl."</td>" : "")."
<td align=\"center\">".$a["seeders"]."</td>
<td align=\"center\">".$a["leechers"]."</td>
<td align=\"center\">".$size."</td>
</tr>\n";
  }
  echo "</table>";
}
if (empty($d3))
die($tracker_lang['no_data_now']." (".$act.")");

}/// ����� checked

elseif ($act=="inviter") {

$it = sql_query("SELECT id, username, class, added, email FROM users AS u WHERE invitedby = ".sqlesc($user)." ORDER BY invitedby LIMIT 200") or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($it) >= 1) {

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=a colspan=\"3\" width=\"100%\">".$tracker_lang['tabs_i_inviter']."</td></tr>";

echo "<tr>
<td class=\"colhead\">".$tracker_lang['signup_username']."</td>
".(($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR) ? "<td class=\"colhead\">".$tracker_lang['email']."</td>":"")."
<td class=\"colhead\">".$tracker_lang['signup']."</td>
</tr>";

$invi_num=0;

while ($inviter = mysql_fetch_assoc($it)) {

echo "<tr>
<td class=\"b\"><a href=\"userdetails.php?id=".$inviter["id"]."\">".get_user_class_color($inviter["class"], $inviter["username"])."</a></td>
".(($user==$CURUSER["id"] || get_user_class() >= UC_MODERATOR) ? "<td class=\"b\">".htmlentities($inviter["email"])."</td>":"")."
<td class=\"b\">".($inviter["added"])."</td>
</tr>";
++$invi_num;
}    

} else die($tracker_lang['no_data_now']." (".$act.")");
}
///////////////////
elseif ($act=="out_re" && ($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR)) {

$it = sql_query("SELECT u.torrentid,u.userid,u.touid,users.username,users.class, torrents.name
FROM thanks AS u 
LEFT JOIN torrents ON torrents.id=u.torrentid
LEFT JOIN users ON users.id=u.touid
WHERE userid = ".sqlesc($user)." ORDER BY u.id DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($it) >= 1) {

echo "<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">

<tr>
<td class=\"colhead\">".$tracker_lang['signup_username']."</td>
<td class=\"colhead\">".$tracker_lang['torrent']." (".$tracker_lang['name'].")</td>
</tr>";

$invi_num=0;
$del=0;

while ($inviter = mysql_fetch_assoc($it)) {

if (!empty($inviter["username"])){
echo "<tr>
<td class=\"b\"><a href=\"userdetails.php?id=".$inviter["touid"]."\">".get_user_class_color($inviter["class"], $inviter["username"])."</a></td>
<td class=\"b\">".(!empty($inviter["name"]) ? "<a href=\"details.php?id=".$inviter["torrentid"]."\">".$inviter["name"]."</a>":$tracker_lang['torrent']." ".$tracker_lang['deleted'])."</td>
</tr>";

++$invi_num;
} else {
++$del;
//	unset($inviter["touid"],$inviter["username"],$inviter["class"]);
}
}
echo "</table>";

echo "<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
<tr><td align=\"center\" class=\"b\"><b>".$tracker_lang['all_thanks']."</b>: ".number_format($invi_num+$del)." <b>".$tracker_lang['where_disabled_user']."</b>: ".number_format($del)."</td></tr>
</table>";
  
} else die($tracker_lang['no_data_now']." (".$act.")");

} elseif ($act == "in_re" && ($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR)) {

$it = sql_query("SELECT u.torrentid, u.userid, u.touid, users.username, users.class, torrents.name
FROM thanks AS u 
LEFT JOIN torrents ON torrents.id = u.torrentid
LEFT JOIN users ON users.id = u.userid
WHERE touid = ".sqlesc($user)." ORDER BY u.id DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($it) >= 1) {

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
<tr>
<td class=\"colhead\">".$tracker_lang['signup_username']."</td>
<td class=\"colhead\">".$tracker_lang['torrent']."</td>
</tr>";

$invi_num=0;
$del=0;

while ($inviter = mysql_fetch_assoc($it)) {

if (!empty($inviter["username"])){
echo "<tr>
<td class=\"b\"><a href=\"userdetails.php?id=".$inviter["userid"]."\">".get_user_class_color($inviter["class"], $inviter["username"])."</a>
</td>
<td class=\"b\">".(!empty($inviter["name"]) ? "<a href=\"details.php?id=".$inviter["torrentid"]."\">".$inviter["name"]."</a>":$tracker_lang['torrent']." ".$tracker_lang['deleted'])."</td>
</tr>";

++$invi_num;
} else {
++$del;
}
}
echo "</table>";

echo "<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\"><tr><td align=\"center\" class=\"b\"><b>".$tracker_lang['all_thanks']."</b>: ".number_format($invi_num+$del)." <b>".$tracker_lang['where_disabled_user']."</b>: ".number_format($del)."</td></tr></table>";

} else die($tracker_lang['no_data_now']." (".$act.")");
}

elseif ($act=="agent" && ($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR)) {

$rsi = sql_query("SELECT crc, idagent FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__, __LINE__); 
$user = mysql_fetch_assoc($rsi);

if (!empty($user["idagent"]))
$idagent = array_unique(explode(",",$user["idagent"]));

if (empty($user["idagent"]) || !count($idagent))
die($tracker_lang['no_data_now']." (".$act.")");

$rs = sql_query("SELECT id, added, agent, crc32 FROM useragent WHERE id IN (".implode(",", $idagent).") ORDER BY FIELD(id, ".implode(",", $idagent).") DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($rs) >= 1) {

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=a colspan=\"4\" width=\"100%\">".$tracker_lang['tabs_iagent']."</td></tr>";

echo "<tr>
<td class=\"colhead\">#</td>
<td class=\"colhead\">".$tracker_lang['tabs_agent']."</td>
".(get_user_class() > UC_MODERATOR ? "<td align=\"center\" class=\"colhead\">".$tracker_lang['hash_sum']."</td>":"")."
<td align=\"center\" class=\"colhead\">".$tracker_lang["added"]."</td>
</tr>";

$invi_num=1;

while ($inviter = mysql_fetch_assoc($rs)) {

echo "<tr>
<td ".($inviter["crc32"]==$user["crc"] ? "class=\"a\"":"class=\"b\"").">".$invi_num."</td>
<td ".($inviter["crc32"]==$user["crc"] ? "class=\"a\"":"class=\"b\"").">".htmlentities($inviter["agent"])."</td>
".(get_user_class() > UC_MODERATOR ? "<td ".($inviter["crc32"]==$user["crc"] ? "class=\"a\"":"class=\"b\"")." align=\"center\"><a target=\"_blank\" href=\"useragent.php?id=".$inviter["id"]."\">".$inviter["crc32"]."</a></td>":"")."
<td ".($inviter["crc32"]==$user["crc"] ? "class=\"a\"":"class=\"b\"")." align=\"center\">".$inviter["added"]."</td>
</tr>";
++$invi_num;
}

echo "</table>";

} else die($tracker_lang['no_data_now']." (".$act.")");
}


elseif ($act == "ref" && ($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR)) {

if ($refer_parse == "1") {

if (empty($refer_day)) $refer_day = 2;

$secs = $refer_day * (86400 * 31); // ������� ������ ������ ������ N �������
$dt = get_date_time(gmtime() - $secs);
sql_query("DELETE FROM referrers WHERE date < ".sqlesc($dt)) or sqlerr(__FILE__,__LINE__);
}
else
die($tracker_lang['func_disabled']);

$csdql_ino = sql_query("SELECT ip FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__); 
$user_t = mysql_fetch_assoc($csdql_ino);

$rs = sql_query("SELECT id, parse_url, parse_ref, numb, date, lastdate FROM referrers WHERE uid = ".sqlesc($user)." GROUP BY parse_ref ORDER BY date DESC LIMIT 200") or sqlerr(__FILE__, __LINE__); 

if (mysql_num_rows($rs) >= 1) {

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">";
echo "<tr><td class=a colspan=\"3\" width=\"100%\">".$tracker_lang['tabs_i_ref']."<br />".sprintf($tracker_lang['tabs_help'], $refer_day, 200)."</td></tr>";
echo "</table>";

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
<tr>
<td class=\"colhead\">#</td>
<td class=\"colhead\">".$tracker_lang['refer_site']."</td>
<td align=\"center\" class=\"colhead\">".$inviter["clock"]." / ".$tracker_lang['update']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['number_all']."</td>
</tr>";

$invi_num=1;

while ($inviter = mysql_fetch_assoc($rs)) {

$inviter["parse_ref"] = str_replace($inviter["parse_url"],"<b>".$inviter["parse_url"]."</b>",$inviter["parse_ref"]);
$words_search = parse_url_query($inviter['parse_ref']);
echo "<tr>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"").">".$invi_num."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"").">".($inviter["parse_ref"])."".($words_search <> false && !empty($words_search['query']) ? "<br />
".$tracker_lang['search'].": ".str_replace("+", " ", $words_search['query'])."":"")."</td>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"")." align=\"center\">".$inviter["date"]."<br />".($inviter["lastdate"]=="0000-00-00 00:00:00" ? $tracker_lang['no']:$inviter["lastdate"])."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"")." align=\"center\">".$inviter["numb"]."<br /></td>
</tr>";
++$invi_num;
}

echo "</table>";

} else die($tracker_lang['no_data_now']." (".$act.")");
}



elseif ($act == "refurl" && ($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR)) {

if ($refer_parse == "1") {

if (empty($refer_day))
$refer_day = 2;

$secs = $refer_day * (86400 * 31); // ������� ������ ������ ������ N �������
$dt = get_date_time(gmtime() - $secs);
sql_query("DELETE FROM referrers WHERE date < ".sqlesc($dt)) or sqlerr(__FILE__,__LINE__);
}
else
die($tracker_lang['func_disabled']);

$urld = "/userdetails.php?id=".$user;

$rs = sql_query("SELECT id, parse_url, parse_ref, numb, date, lastdate FROM referrers WHERE LEFT(req_url, ".strlen($urld).") = ".sqlesc($urld)." GROUP BY parse_ref ORDER BY date DESC LIMIT 200") or sqlerr(__FILE__, __LINE__); 

if (mysql_num_rows($rs) >= 1) {

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">";
echo "<tr><td class=a colspan=\"3\" width=\"100%\">".$tracker_lang['tabs_i_refurl']."<br />".sprintf($tracker_lang['tabs_help'], $refer_day, 500)."</td></tr>";
echo "</table>";

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
<tr>
<td class=\"colhead\">#</td>
<td class=\"colhead\">".$tracker_lang['refer_site']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['clock']." / ".$tracker_lang['update']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['number_all']."</td>
</tr>";

$invi_num=1;

while ($inviter = mysql_fetch_assoc($rs)) {

$inviter["parse_ref"] = str_replace($inviter["parse_url"],"<b>".$inviter["parse_url"]."</b>",$inviter["parse_ref"]);
$words_search = parse_url_query($inviter['parse_ref']);

echo "<tr>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"").">".$invi_num."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"").">".($inviter["parse_ref"])."".($words_search <> false && !empty($words_search['query']) ? "<br />
".$tracker_lang['search'].": ".str_replace("+", " ", $words_search['query'])."":"")."</td>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"")." align=\"center\">".$inviter["date"]."<br />".($inviter["lastdate"]=="0000-00-00 00:00:00" ? $tracker_lang['no']:$inviter["lastdate"])."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"")." align=\"center\">".$inviter["numb"]."<br /></td>
</tr>";

++$invi_num;
}


if (!empty($invi_num))
echo "<tr><td class=a colspan=\"3\" width=\"100%\">".$tracker_lang['my_backlink'].": ".$DEFAULTBASEURL."<a href=\"".$DEFAULTBASEURL.$urld."\">".$urld."</a></td></tr>";

echo "</table>";

} else die($tracker_lang['no_data_now']." (".$act.")");
}




elseif ($act == "red" && ($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR)) {

if ($redir_parse == "1") {

if (empty($redir_day))
$redir_day = 2;

$secs = $redir_day * (86400 * 31); // ������� ������ ������ ������ N �������
$dt = get_date_time(gmtime() - $secs);
sql_query("DELETE FROM reaway WHERE date < ".sqlesc($dt)) or sqlerr(__FILE__,__LINE__);
}
else
die($tracker_lang['func_disabled']);

$csdql_ino = sql_query("SELECT ip FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__); 
$user_t = mysql_fetch_assoc($csdql_ino);

$rs = sql_query("SELECT id, parse_url, parse_ref, numb, date, lastdate FROM reaway WHERE uid = ".sqlesc($user)." GROUP BY parse_ref ORDER BY date DESC LIMIT 200") or sqlerr(__FILE__, __LINE__); 

if (mysql_num_rows($rs) >= 1) {

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">";
echo "<tr><td class=a colspan=\"3\" width=\"100%\">".$tracker_lang['tabs_ired']."<br />".sprintf($tracker_lang['tabs_help'], $redir_day, 200)."</td></tr>";
echo "</table>";

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr>
<td class=\"colhead\">#</td>
<td class=\"colhead\">".$tracker_lang['refer_site']." (".$tracker_lang['from_to'].")</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['clock']." / ".$tracker_lang['update']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['number_all']."</td>
</tr>";

$invi_num=1;

while ($inviter = mysql_fetch_assoc($rs)) {

$inviter["parse_ref"] = str_replace($inviter["parse_url"],"<b>".$inviter["parse_url"]."</b>",$inviter["parse_ref"]);

$exf = explode("/", $tracker_lang['from_to']);

echo "<tr>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"").">".$invi_num."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"").">".trim(current($exf)).": ".($inviter["parse_ref"])."<br />".trim(end($exf)).": ".($inviter["parse_url"])."</td>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"")." align=\"center\">".$inviter["date"]."<br />".($inviter["lastdate"]=="0000-00-00 00:00:00" ? $tracker_lang['no']:$inviter["lastdate"])."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"")." align=\"center\">".$inviter["numb"]."<br /></td>
</tr>";

++$invi_num;
}

echo "</table>";

} else die($tracker_lang['no_data_now']." (".$act.")");
}


elseif ($act == "session" && get_user_class() >= UC_ADMINISTRATOR) {

$csdql_ino = sql_query("SELECT ip, class, username FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__); 
$user_t = mysql_fetch_assoc($csdql_ino);

$rs = sql_query("SELECT * FROM sessions WHERE uid = ".sqlesc($user)." ORDER BY time DESC LIMIT 200") or sqlerr(__FILE__, __LINE__); 

if (mysql_num_rows($rs) >= 1) {

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">";
echo "<tr><td class=\"a\" colspan=\"3\" width=\"100%\">".$tracker_lang['data_session']."</td></tr>";
echo "</table>";

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
<tr>
<td class=\"colhead\">".$tracker_lang['my_info']."</td>
<td class=\"colhead\" width=\"150px\">".$tracker_lang['clock']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['who_see']." / ".$tracker_lang['domen']."</td>
</tr>";

$invi_num=1;

while ($inviter = mysql_fetch_assoc($rs)) {

$slep = "<div class=\"spoiler-wrap\" id=\"".round($invi_num+crc32($inviter["sid"])*($invi_num/4*77))."\"><div class=\"spoiler-head folded clickable\">��������� � ������: ".$inviter["ip"]."</div>
<div class=\"spoiler-body\" style=\"display: none;\">
<b>".$tracker_lang['user_ip']."</b>: <a target='_blank' href=\"".$DEFAULTBASEURL."/usersearch.php?ip=".$inviter["ip"]."\">".$inviter["ip"]."</a> (".$tracker_lang['search_ip'].")<br />
<b>������</b>: ".$inviter["sid"]."<br />
<b>".$tracker_lang['tabs_agent']."</b>: ".$inviter['useragent']."<br />
</div>
</div>";

$inviter["host"] = str_replace("www.muz-tracker.net", "<font color=\"blue\">www.muz-tracker.net</font>", $inviter["host"]);
$inviter["host"] = str_replace("www.muz-trackers.ru", "<font color=\"red\">www.muz-trackers.ru</font>", $inviter["host"]);

$url = basename($inviter["url"]);
$list = explode(".php", $url);

echo "<tr>

<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"").">".$slep."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"").">".$inviter["time"]."</td>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"")." align=\"center\">

<a target='_blank' href=\"".$inviter["url"]."\">".($list[0].".php")."</a><br /><div align=\"right\">".$inviter["host"]."</div></td>

</tr>";

++$invi_num;
unset($inviter);
}

echo "</table>";

} else die($tracker_lang['no_data_now']." (".$act.")");
}

elseif ($act == "tfile" && ($user == $CURUSER["id"] || get_user_class() >= UC_MODERATOR)) {

$csdql_ino = sql_query("SELECT class FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__); 
$user_t = mysql_fetch_assoc($csdql_ino);

$rss = sql_query("SELECT * FROM attachments WHERE uploadby = ".sqlesc($user)." ORDER BY id DESC LIMIT 200") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($rss) >= 1) {

/// ������ �������� ������������� ///
$rs = sql_query("SELECT sum(size) AS my_size FROM attachments WHERE uploadby = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__);
$as = mysql_fetch_assoc($rs);
$my_size = $as["my_size"];
/// ������ �������� ������������� ///

$limit_size = (!empty($Tfiles_Config[$user_t["class"]]) ? ($Tfiles_Config[$user_t["class"]]*1024*1024):0);

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">";

echo "<tr><td class=\"a\" colspan=\"2\" width=\"100%\">".$tracker_lang['tabs_i_tfile']." (<a title=\"".$tracker_lang['dox']."\" href=\"tfiles.php\">".$tracker_lang['dox']."</a>)</td></tr>";

if (isset($limit_size) && !empty($limit_size)){

$end = explode("/", $tracker_lang['rem_space_busy']);
echo "<tr><td align=\"left\" class=\"b\">".trim($end[1])." / ".trim($end[0])." / ".$tracker_lang['total']."</td><td align=\"left\" class=\"b\">".mksize($my_size)." / ".(($limit_size-$my_size) > 0 ? mksize($limit_size-$my_size):mksize(0))." / ".mksize($limit_size)."</td></tr>";
}

echo "</table>";

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr>
<td width=\"3%\" align=\"center\" class=\"colhead\">#</td>
<td width=\"50%\" class=\"colhead\" align=\"left\">".$tracker_lang['name']." / ".$tracker_lang['size']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['added']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['details_leeched']."</td>
<td width=\"20%\" align=\"center\" class=\"colhead\">".$tracker_lang['type']."</td>
</tr>";

$num = 0;
while ($arr = mysql_fetch_assoc($rss)) {

if ($num%2 == 0){
$cl1 = "class=\"b\"";
$cl2 = "class=\"a\"";
} else {
$cl2 = "class=\"b\"";
$cl1 = "class=\"a\"";
}

echo "<tr>";

echo "<td ".$cl1." align=\"center\">".$arr['id']."</td>";

echo "<td ".$cl2." align=\"left\"><a title=\"".$tracker_lang['download']."\" href=\"tfiles.php?download=".$arr["id"]."\"><b>".htmlspecialchars($arr["title"])."</b></a> (".mksize($arr['size']).")".(get_user_class() > UC_MODERATOR ? "<br />".$arr['filename']."":"")."</td>";
echo "<td ".$cl1." align=\"center\">".$arr["added"]."</td>";
echo "<td ".$cl2." align=\"center\">".number_format($arr['hits'])." ".$tracker_lang['time_s']."</td>";
echo "<td ".$cl1." align=\"left\">".$arr['type']."</td>";

echo "</tr>";

++$num;
}

echo "<tr><td class=\"a\" colspan=\"4\" width=\"100%\" align=\"center\">".$tracker_lang['sort_numfile'].": ".mysql_num_rows($rss)."</td></tr>";


echo "</table>";

} else die($tracker_lang['no_data_now']." (".$act.")");
}


elseif ($act == "download" && ($user == $CURUSER["id"] || get_user_class() >= UC_ADMINISTRATOR)) {

global $Download_Config;

$csdql_ino = sql_query("SELECT class FROM users WHERE id = ".sqlesc($user)) or sqlerr(__FILE__,__LINE__); 
$user_t = mysql_fetch_array($csdql_ino);

$down_limit = $Download_Config[($user_t["class"]+1)];

$rsd = sql_query("SELECT torrents FROM download WHERE userid = ".sqlesc($user)." AND date = ".sqlesc(date("Y-m-d"))) or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($rsd)){
$arrd = mysql_fetch_assoc($rsd);

$exp = explode(',', $arrd["torrents"]);

if (count($exp))
$rss = sql_query("SELECT id, name, size FROM torrents WHERE id IN (".implode(",", $exp).") ORDER BY FIELD(id, ".implode(",", $exp).") DESC") or sqlerr(__FILE__,__LINE__);

}

if (count($exp) && mysql_num_rows($rss) >= 1) {

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">";
echo "<tr><td class=\"a\" colspan=\"2\" width=\"100%\">".$tracker_lang['tabs_i_download']." ".sprintf($tracker_lang['download_limiter'], "<ins>".get_user_class_name(($CURUSER ? $CURUSER["class"] : "-1"))."</ins>", "<strong>".$down_limit."</strong>")."</td></tr>";
echo "</table>";

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr>
<td width=\"3%\" align=\"center\" class=\"colhead\">#</td>
<td width=\"50%\" class=\"colhead\" align=\"left\">".$tracker_lang['name']." / ".$tracker_lang['size']."</td>
</tr>";

$num = 0;
while ($arr = mysql_fetch_assoc($rss)) {

if ($num%2 == 0){
$cl1 = "class=\"b\"";
$cl2 = "class=\"a\"";
} else {
$cl2 = "class=\"b\"";
$cl1 = "class=\"a\"";
}

echo "<tr>";
echo "<td ".$cl1." align=\"center\">".$arr['id']."</td>";
echo "<td ".$cl2." align=\"left\"><a title=\"".$tracker_lang['description']."\" href=\"details.php?id=".$arr["id"]."\"><b>".htmlspecialchars($arr["name"])."</b></a> (".mksize($arr['size']).")</td>";
echo "</tr>";

++$num;
}

echo "<tr><td class=\"b\" colspan=\"2\" width=\"100%\" align=\"center\">".$tracker_lang['rem_space_busy'].": <strong>".number_format($down_limit-mysql_num_rows($rss))."</strong> (<ins>".mysql_num_rows($rss)."</ins>)</td></tr>";


echo "</table>";

} else die($tracker_lang['no_data_now']." (".$act.")");
}



elseif ($act == "mematch" && ($user == $CURUSER["id"] || get_user_class() >= UC_ADMINISTRATOR)) { /// ��, ��� �� ���������

$count_ = get_row_count("my_search", "WHERE userid = ".sqlesc($CURUSER['id'])); /// ������ ������!!

if ($count_ == 0)
die($tracker_lang['no_data_now']." (".$act.")");


$res_cat = sql_query("SELECT id, name, image FROM categories", $cache = array("type" => "disk", "file" => "browse_cat_array", "time" => 24*7200)) or sqlerr(__FILE__, __LINE__);

while ($arr_cat = mysql_fetch_assoc_($res_cat)){
$cat_sql[$arr_cat["id"]] = array("name" => $arr_cat["name"], "image" => $arr_cat["image"]);
}

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";

echo "<tr><td class=\"a\" colspan=\"4\" width=\"100%\">".$tracker_lang['mem_help']."</td></tr>";

$res = sql_query("SELECT my_search.*, (SELECT COUNT(*) FROM my_match WHERE my_match.mysearch = my_search.id) AS number FROM my_search WHERE userid = ".sqlesc($user)." ORDER BY id DESC LIMIT 100") or sqlerr(__FILE__, __LINE__);

echo "<tr>
<td class=\"colhead\" width=\"3%\" align=\"center\">#</td>
<td class=\"colhead\" width=\"20%\">".$tracker_lang['active_category']."</td>
<td class=\"colhead\">".$tracker_lang['mem_phrone']." / ".$tracker_lang['arr_searched']."</td>
<td class=\"colhead\" width=\"10%\" align=\"center\">".$tracker_lang['sort_added']."</td>
</tr>";

$i = 0;
while ($arr = mysql_fetch_assoc($res)) {

$number = $arr['number'];

$list = "<i title=\"".$tracker_lang['mem_catselno']."\">".$tracker_lang['signup_not_selected']."</i>";

$expp = array();
if (!empty($arr['cats'])) $expp = explode(',', $arr['cats']);

if (count($expp)){

$list = '<select size="1" name="cat">';
foreach ($expp AS $idcat){
if (isset($cat_sql[$idcat])) $list.= "<option value=\"".$cat["id"]."\">".htmlspecialchars($cat_sql[$idcat]["name"])."</option>";
}
$list.= '</select>';

}

echo "<tr>";
echo "<td width=\"3%\" ".($i%2==0 ? "class=\"b\"":"class=\"a\"")." align=\"center\">".$arr['id']."</td>";
echo "<td width=\"20%\" ".($i%2==0 ? "class=\"a\"":"class=\"b\"").">".$list."</td>";
echo "<td ".($i%2==0 ? "class=\"a\"":"class=\"b\"").">".$arr['words']." ".(!empty($number) ? "(".$number.")":"")."</td>";
echo "<td width=\"10%\" ".($i%2==0 ? "class=\"a\"":"class=\"b\"")." align=\"center\"><nobr>".$arr['time']."</nobr></td>";
echo "</tr>";

++$i;
}


echo "</table>";

} /// ���������, ��������.












}
/// ����� ����������� 
?>