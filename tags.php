<?
require "include/bittorrent.php";
dbconn();
loggedinorreturn();

global $DEFAULTBASEURL;




function insert_tag($name, $description, $syntax, $example, $remarks = false) {

global $tracker_lang;

echo ("<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">");

echo ("<tr><td class=\"colhead\" colspan=\"2\">".$name."</td></tr>");

echo ("<tr valign=\"top\"><td class=\"a\" width=\"25%\">".$tracker_lang['description']."</td><td class=\"b\">".$description."</td></tr>");
echo ("<tr valign=\"top\"><td class=\"b\">".$tracker_lang['tags_sintax']."</td><td class=\"a\"><tt>".$syntax."</tt>\n");
echo ("<tr valign=\"top\"><td class=\"a\">".$tracker_lang['help']."</td><td class=\"b\"><tt>".$example."</tt></td></tr>");

if (preg_match("/\[php\]/is", $syntax))
echo ("<tr valign=\"top\"><td colspan=\"2\" align=\"center\">".format_comment(regex_php_tag($syntax), true)."</td></tr>");
else
echo ("<tr valign=\"top\"><td colspan=\"2\" align=\"center\">".format_comment($example, true)."</td></tr>");

if (!empty($remarks))
echo ("<tr><td class=\"b\">".$tracker_lang['warning']."</td><td class=\"a\">".$remarks."</td></tr>");

echo ("</table><br />");
}




stdhead($tracker_lang['tags_bb_code']);


$test = $testext = htmlspecialchars_uni(isset($_POST["test"]) ? trim($_POST["test"]):"");




if (get_user_class() >= UC_SYSOP && !empty($test)){

$array = array();


if (substr($DEFAULTBASEURL, 0, 11) <> "http://www."){ /// ����� #1 ��� ������ c www

preg_match_all("/".addcslashes(str_ireplace('http://', 'http://www.', $DEFAULTBASEURL), "/")."\/details.php\?id=([0-9]{2,20})+(\n|\r|\s|\&|$)/is", $test, $dtid1);
if (count($dtid1[1])) $array = array_merge($dtid1[1]);

} else {

preg_match_all("/".addcslashes($DEFAULTBASEURL)."\/details.php\?id=([0-9]{2,20})+(\n|\r|\s|\&|$)/is", $test, $dtid1);
if (count($dtid1[1])) $array = array_merge($dtid1[1]);

}

if (substr($DEFAULTBASEURL, 0, 7) <> "http://"){ /// ����� #2 ��� ������ ��� http (������� ������)
preg_match_all("/".addcslashes("http://".$DEFAULTBASEURL, "/")."\/details.php\?id=([0-9]{2,20})+(\n|\r|\s|\&|$)/is", $test, $dtid2);
if (count($dtid2[1])) $array = array_merge($array, $dtid2[1]);
}

/// ����� #3 ��� ������ ��� www
preg_match_all("/".str_ireplace('www.', '', addcslashes($DEFAULTBASEURL, "/"))."\/details.php\?id=([0-9]{2,20})+(\n|\r|\s|\&|$)/is", $test, $dtid3);
if (count($dtid3[1])) $array = array_merge($array, $dtid3[1]);

/// /// ����� #4 ��� ������ localhost
preg_match_all("/http:\/\/localhost\/details.php\?id=([0-9]{2,20})+(\n|\r|\s|\&|$)/is", $test, $dtid4);
if (count($dtid4[1])) $array = array_merge($array, $dtid4[1]);

$con_arr = (count($array) ? array_unique(array_map("intval", $array)):false);

if (count($con_arr) && $con_arr <> false) array_splice($con_arr, 100); /// �������� ����� ��������� ��� ������ �� 100 (����� �� ���� ������)

/// ��������� � ������ ��� �� ����� ���� ������� �� �������� �� ����
if (count($con_arr) && count($array) && !preg_match("/\[(.*?)\]/is", $testext)){
foreach ($con_arr AS $lnk){
$testext = str_ireplace('details.php?id='.$lnk, 'details.php?id='.$lnk." (+)", $testext);
}
}
/// ��������� � ������ ��� �� ����� ���� ������� �� �������� �� ����


}




echo '<table class="main" width="100%" border="0" cellspacing="0" cellpadding="5">';
echo "<tr><td class=\"colhead\" align=\"center\">".$tracker_lang['tags_test']."</td></tr>";
echo '<tr><td class="b" align="center"><form method="post" action="tags.php"><textarea name="test" cols="150" rows="7">'.(!empty($test) ? htmlspecialchars($test) : "").'</textarea></td></tr>';
echo "<tr><td class=\"a\" align=\"center\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['tags_test']."\" style='height: 23px; margin-left: 5px' /></td></tr>";

if (!empty($testext))
echo ("<tr><td class=\"b\" align=\"center\">".format_comment($testext)."</td></tr>");

echo "</form></table><br />";


if (count($con_arr) && count($array)){

$res = sql_query("SELECT torrents.id, torrents.moderated, torrents.multitracker, torrents.moderatedby, torrents.moderatordate, torrents.viponly, torrents.category, torrents.tags, (torrents.leechers+torrents.f_leechers) AS leechers, (torrents.seeders+torrents.f_seeders) AS seeders, torrents.free,torrents.banned, torrents.banned_reason, torrents.name, torrents.size, torrents.added, torrents.comments, torrents.numfiles, torrents.sticky, torrents.owner, users.username, users.class FROM torrents
LEFT JOIN users ON torrents.owner = users.id
WHERE torrents.id IN (".implode(', ', $con_arr).")") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res) >= 1){

echo "<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\" width=\"100%\">";
echo "<tr><td class=\"a\" align=\"center\">".$tracker_lang['admincp'].": ".$tracker_lang['b_action']." ".(count($con_arr) < 100 ? "(".count($con_arr).")":"")."</td></tr>";
echo "</table>";

echo "<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\" width=\"100%\">";

torrenttable($res, "index");

echo "<tr><td class=\"a\" align=\"center\" colspan=\"12\">".sprintf($tracker_lang['limit_ofn'], 100)."</td></tr>";

echo "</table><br />";

} else {

echo "<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\" width=\"100%\">";
echo "<tr><td class=\"a\" align=\"center\">".$tracker_lang['admincp'].": ".$tracker_lang['b_action']." ".(count($con_arr) < 100 ? "(".count($con_arr).")":"")."</td></tr>";
echo "</table>";

echo "<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\" width=\"100%\">";
echo "<tr><td class=\"a\" align=\"center\" colspan=\"12\"><h3>".$tracker_lang['nothing_found']."</h3</td></tr>";
echo "</table><br />";


}



}






if (empty($test)){

begin_frame($tracker_lang['tags_bb_code']);


insert_tag(
"������",
"������ ����� ������.",
"[b]�����[/b]",
"[b]���� ����� ������.[/b]",
""
);

insert_tag(
"������",
"������ ����� ���������.",
"[i]�����[/i]",
"[i]���� ����� ���������.[/i]",
""
);

insert_tag(
"�������������",
"������ ����� ������������.",
"[u]�����[/u]",
"[u]���� ����� ������������.[/u]",
""
);

insert_tag(
"������������",
"������ ����� �����������.",
"[s]�����[/s]",
"[s]���� ����� �����������.[/s]",
""
);

insert_tag(
"���� (� 1)",
"������ ���� ������.",
"[color=<i>Color</i>]<i>�����</i>[/color]",
"[color=red]���� ����� �������.[/color]",
"����� ������ ���� �� ���� �����, � ������� blue red green pink black white"
);

insert_tag(
"���� (� 2)",
"������ ���� ������.",
"[color=#<i>RGB</i>]<i>�����</i>[/color]",
"[color=#ff0000]���� ����� �������.[/color]",
"RGB ���� ������ ���� 6-�� ������� ������� ����������������� ������� ���������."
);

insert_tag(
"������",
"��������� ������ ������.",
"[size=<i>n</i>]<i>�����</i>[/size]",
"[size=18]��� 18 ������.[/size]",
"����� ������ ���� � ��������� �� 1 (���������) �� 100 (�������). ������ �� ��������� - 8."
);

insert_tag(
"�����",
"��������� ����� ��� ������.",
"[font=<i>Font</i>]<i>�����</i>[/font]",
"[font=Impact]��������[/font]",
""
);

insert_tag(
"������ (� 1)",
"������� ������.",
"[url]<i>������</i>[/url]",
"[url]".$DEFAULTBASEURL."[/url]",
""
);

insert_tag(
"������ (� 2)",
"������� ������ � ���������.",
"[url=<i>URL</i>]<i>����� �����������</i>[/url]",
"[url=".$DEFAULTBASEURL."]".$tracker_lang['help']."[/url]",
""
);

insert_tag(
"������ (� 3)",
"������� <ins>����������</ins> ������ � ���������.",
"[url=<i>URL</i>]<i>����� �����������</i>[/url]",
"[url=index.php?test]�� ������� �������� �����[/url]",
"���������� ������ ����������� ������ ����� � ���� .php ����������"
);


insert_tag(
"�������� (� 1)",
"������� ��������.",
"[img]<i>������]</i>[/img]",
"[img]http://www.rambler.ru/i/logos/friends.gif[/img]",
"������ ������ ������������� �� .gif, .jpg ��� .png </b>"
);

insert_tag(
"�������� (� ������� �� ����)",
"������� ��������.",
"[url=<i>������</i>][img]<i>������ �� ����</i>[/img][/url]",
"[url=http://www.rambler.ru/][img]http://www.rambler.ru/i/logos/friends.gif[/img][/url]",
"������ ������ ������������� �� .gif, .jpg ��� .png </b>"
);



insert_tag(
"������� (������� �����)",
"������� ��������",
"[spoiler]<i>��� �����</i>[/spoiler]",
"[spoiler]������ ��� �����, � ���� ���?![/spoiler]",
"�� N ��������� ������ ������������, ��� ���������, ��� �� ��������. </b>"
);


insert_tag(
"������� (������� �����) � ������������",
"������� ��������",
"[spoiler=<i>��� ����� 1</i>]<i>��� ����� 2</i>[/spoiler]",
"[spoiler=������ ����� ��]����� ��������������������� �����, ������� � ������.[/spoiler]",
"�� N ��������� ������ ������������, ��� ���������, ��� �� ��������. </b>"
);

insert_tag(
"������ (� 1)",
"������� ������.",
"[quote]<i>������������ �����</i>[/quote]",
"[quote]���� ������ ����� ������ ����.[/quote]",
""
);

insert_tag(
"������ (� 2)",
"������� ������.",
"[quote=<i>Author</i>]<i>������������ �����</i>[/quote]",
"[quote=7Max7]���� ������ ����� ������ ����.[/quote]",
""
);

insert_tag(
"������",
"������� ������.",
"[li]<i>�����</i>",
"[li] ����������� 1\n[li] ����������� 2",
""
);

insert_tag(
"������",
"������� �������������� �����.",
"[hr]",
"[hr] ����� [hr]",
""
);

insert_tag(
"�� ���� (me)",
"������� �� ����.",
"[me]<i>�����</i>",
"[me] ���� �� ���� (me)",
""
);

insert_tag(
"����� �������",
"�����",
"[pi]��� � ���� �����",
"[pi]��� � ���� �����",
""
);

insert_tag(
"������ bb ��� (�������������������)",
"�����",
"[bb]��� � ������ <b>bb ���</b>[/bb]",
"[bb]��� � ������ [b]bb ���[/b] �� �� �������������� �������� bb ����� � [pre]������������ ��� ����, �������� �����[/pre].[/bb]",
""
);

insert_tag(
"� ������",
"����� ��� �������� � ������.",
"[align=center]<i>�����</i>[/align]",
"[align=center] ����������� [/align]",
""
);


insert_tag(
"�� ������ ����",
"����� ��� �������� �� ������ ������.",
"[align=justify]<i>�����</i>[/align]",
"[align=justify] �� ���� ������ ����� [/align]",
""
);


insert_tag(
"�����",
"����� ��� �������� �����.",
"[align=left]<i>�����</i>[/align]",
"[align=left] ����� �����[/align]",
""
);

insert_tag(
"������",
"����� ��� �������� ������.",
"[align=right]<i>�����</i>[/align]",
"[align=right] ������ ����� [/align]",
""
);

insert_tag(
"������������ �����",
"�����",
"[pre]��� ����� �� ������� ����������, ���������� (enter) �� ������ ������, �������� � ���� ��������.
[/pre]",
"[pre]��� ����� �� ������� ����������, ���������� (enter)
�� ������ ������,    �������� � ���� ��������
;-)
[/pre]",
"����� ��� ��������� �����,  ����� ��� � word'e."
);

insert_tag(
"��������� ����������",
"�����",
"[highlight]��� �����[/highlight]",
"[highlight]��� �����[/highlight]",
"��� ������ ��������� ����������� �����"
);

insert_tag(
"��������� php ���� (���������� php ����)",
"����� ������������ php ������",
"[php] echo '�������� php ����'; \$numb = 0;
include_once (\"Tesla/source.img\");
cache_creat (\"my_config\", true);
++\$numb;
die;
[/php]",
"��������� ������ � ������� �������������� ���������� �������� � �������� ���� � detail.php � message.php ��������: ������� ".htmlentities("<?")." � � ����� ".htmlentities("?>")." �������� � �������� ���� �������������!"
);


insert_tag(
"����� ��������� ������",
"�����",
"[mcom=#FFD42A:#002AFF]��� ��� ������� <b>����</b> �� ����� ������� <b>����</b>[/mcom]",
"[mcom=#FFD42A:#002AFF]��� ��� ������� ����� �� ����� ������� ����[/mcom]",
"����� ����� ��� ������� ����"
);

insert_tag(
"������� ������ ��� ��������� �����",
"�����",
"[hideback]������� ������ ��� ��������� �����[/hideback]",
"[hideback]������� ������ ��� ��������� �����[/hideback]",
"�������� ����� ��� ��������� ����� ����������"
);

insert_tag(
"����� ������ ������",
"����� ��� �������� ",
"[legend]��������� ������ �����[/legend]",
"[legend]��������� ������ �����[/legend]",
"��������� ����� ������ ������."
);

insert_tag(
"����� ������ ������ � �������",
"����� ��� �������� ",
"[legend=������ ����]��������� ������ ����� � �������[/legend]",
"[legend=������ ����]��������� ������ ����� � �������[/legend]",
"��������� ����� ������ ������."
);

insert_tag(
"�������� ������",
"����� ��� �������� ",
"[marquee]�������� ������[/marquee]",
"[marquee]�������� ������[/marquee]",
"��� ����� ������"
);


insert_tag(
"�����",
"������������� ����� ������.",
"[audio]������ �� mp3 �����[/audio]",
"[audio]http://music.torrent-xzona.ru/audio/79546822/44111745/dj_titon-dj_titon_-_.mp3[/audio]",
"����������� ������ ������ ������������� �� .mp3"
);


insert_tag(
"���� ��������",
"������������� ���� ������",
"[flash]������ �� flash ����.swf[/flash]",
"[flash]".$DEFAULTBASEURL."/swf/snow_internal2.swf[/flash]",
"����������� ������ ������ ������������� �� .swf �������� ��������� �������, � ������ �������� ����: [flash=300:50]��� ������[/flash]"
);

insert_tag(
"����� � youtube.com",
"������������� ������ � ����� youtube.com",
"[video=������ �� ���� youtube]",
"[video=http://www.youtube.com/watch?v=593KCbGNl9s]",
"������������ ��� ������� ������ (http://www.youtube.com/watch?v=593KCbGNl9s), ��� � �������������� (http://www.youtube-nocookie.com/v/593KCbGNl9s) � �������������� ��������� HD �����."
);


insert_tag(
"����� � youtu.be (�������������� ������)",
"������������� ������ � ����� youtu.be (�������: ��������� � �������� � ������� �����)",
"[video=������ �� ���� youtu.be]",
"[video=http://youtu.be/j1vT7esXE0A]",
"������������ ������ (http://youtu.be/j1vT7esXE0A) � �������������� ��������� HD �����."
);


end_frame();

}




stdfoot();
?>