<?
if(!defined('IN_TRACKER') && !defined('IN_ANNOUNCE'))
die ("Hacking attempt init!");

/*
 * 11.08.2018
 * �������� � ������ �������� ����� �� ����������� ������.
 * */

// SET PHP ENVIRONMENT
@error_reporting (E_ALL & ~E_NOTICE & ~E_DEPRECATED);
@ini_set ('error_reporting', E_ALL & ~E_NOTICE);///  & ~E_NOTICE
@ini_set ('display_errors', false);
@ini_set ('display_startup_errors', '0');
@ini_set ('ignore_repeated_errors', '1');
@ignore_user_abort (1);


if (empty($_SERVER["HTTP_HOST"]))
$_SERVER["HTTP_HOST"] = $_SERVER["SERVER_NAME"];  

require_once (ROOT_PATH.'/include/passwords.php');

@set_time_limit (0);

@ini_set ('magic_quotes_runtime', 0);
@date_default_timezone_set ("Europe/Kiev"); /// ������� ����

if ($Cache_Config["memcache_on"] == true && (empty($Cache_Config["type"]) || $Cache_Config["type"] == "memcache"))
$memcache_server = memcache_connect($Cache_Config["memcache_host"], $Cache_Config["memcache_port"]); /// ����������� ����

$url = explode('/', htmlspecialchars_uni($_SERVER['PHP_SELF']));
array_pop($url);
$DEFAULTBASEURL = ($_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://".htmlspecialchars_uni($_SERVER['HTTP_HOST']).implode('/', $url);


$announce_urls = array();

if (in_array(basename($_SERVER['SCRIPT_FILENAME']), array("download.php", "details.php"))){

$announce_urls[] = $DEFAULTBASEURL."/announce.php"; /// �� ��������

$announce_urls[] = "http://retracker.local/announce"; /// ��������� ��������

$announce_urls[] = "udp://tracker.openbittorrent.com:80";
$announce_urls[] = "udp://tracker.publicbt.com:80";

}


$announce_urls[] = "http://i.bandito.org/announce.php";

$announce_urls[] = "http://bt.rutor.org:2710/announce";
$announce_urls[] = "http://announce.torrentsmd.eu:6969/announce.php?passkey=5e5be76d37b099ad5f6fbe3fdb3c7e1d";



// DEFINE ������ ������
define ("UC_USER", 0);
define ("UC_POWER_USER", 1);
define ("UC_VIP", 2);
define ("UC_UPLOADER", 3);
define ("UC_MODERATOR", 4);
define ("UC_ADMINISTRATOR", 5);
define ("UC_SYSOP", 6);

/// ����� ������� core_annouce.php � functions.php

function htmlspecialchars_uni ($message) {

$message = stripslashes($message);

///	$message = preg_replace("#&(?!\#[0-9]+;)#si", "", $message); // Fix & but allow unicode
$message = preg_replace(array("'<script[^>]*?>.*?</script>'si","'&(quot|#34|#034|#x22);'i","'&(amp|#38|#038|#x26);'i"), array("","\""," "), $message);
$message = str_replace(array("&quot;", "&amp;", "-gt;", "&nbsp; "), "", $message);
	    
return $message;
}


function dbconn($autoclean = false, $lightmode = false) {
//  $lightmode = true - ���� ������ � ���� (����)

global $Mysql_Config, $tracker_lang;


if (!mysql_connect($Mysql_Config['host'], $Mysql_Config['user'], $Mysql_Config['pass'])) {

global $use_lang, $default_language, $CURUSER;

if ($use_lang == true)
include_once(ROOT_PATH.'/languages/lang_'.(!empty($CURUSER["language"]) ? $CURUSER["language"]:$default_language).'/lang_main.php');


switch (mysql_errno()) {

case 2003: case 1146: case 1049:  {

if (defined("IN_ANNOUNCE"))
err ('','dbconn: mysql_connect: '.mysql_error(), 2);
else
die ($tracker_lang['mysql_nocon'].": ".mysql_error());

}

case 1040: case 2002: case 1203: 

if ($_GET && !defined("IN_ANNOUNCE"))
die("<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=".$tracker_lang['language_charset']."\">
<meta http-equiv=\"Content-Style-Type\" content=\"text/css\">
<meta http-equiv='refresh' content=\"30 ".$_SERVER["REQUEST_URI"]."\">
<title>".$tracker_lang['mysql_timeout']."</title>
<style type=\"text/css\">body { min-width: 800px; color: #000000; background: #E3E3E3; font: 16px Verdana; }
.msg { margin: 20%; text-align: center; background: #EFEFEF; border: 1px solid #B7C0C5; }
</style></head><body>
<div class=\"msg\"><p style=\"margin: 1em 0;\">".$tracker_lang['mysql_nocon']."</p><p style=\"margin: 1em 0;\">".$tracker_lang['mysql_pageupd']."</p>
</div></body>");
elseif (!$_GET && !defined("IN_ANNOUNCE"))
die("<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=".$tracker_lang['language_charset']."\">
<meta http-equiv=\"Content-Style-Type\" content=\"text/css\">
<title>".$tracker_lang['mysql_timeout']."</title>
<style type=\"text/css\">body { min-width: 800px; color: #000000; background: #E3E3E3; font: 16px Verdana; }
.msg { margin: 20%; text-align: center; background: #EFEFEF; border: 1px solid #B7C0C5; }
</style></head><body>
<div class=\"msg\"><p style=\"margin: 1em 0;\">".$tracker_lang['mysql_nocon']."</p>	<p style=\"margin: 1em 0;\">".$tracker_lang['mysql_pageupd']."</p>
</div></body>");

default: (defined("IN_ANNOUNCE") ? err('','dbconn: mysql_connect: ' . mysql_error(), 2) : die("[" . mysql_errno() . "] dbconn: mysql_connect: " . mysql_error()));
}

}


if (defined("IN_ANNOUNCE"))
mysql_select_db($Mysql_Config['db']) or err('','dbconn: mysql_select_db: ' + mysql_error(), 2);
else
mysql_select_db($Mysql_Config['db']) or die("dbconn: mysql_select_db: " + mysql_error());

mysql_query("SET NAMES ".$Mysql_Config['charset']);


if (!defined("IN_ANNOUNCE")){

userlogin($lightmode);
parse_referer($lightmode);

if (in_array(basename($_SERVER['SCRIPT_FILENAME']), array('index.php', 'persons.php')))
register_shutdown_function("autoclean");

}

register_shutdown_function("mysql_close"); /// � ����� ������ ������� - ����������� �� ���� ������.

/// ������� ���������� ����������, � ������ ���������� ������� (include) ����� ������
unset($Mysql_Config);

}


function file_query ($content = false, $cache = false) {

/** ������ ���� ������ ��� Tesla (Tesla Tracker) v. Platinum �� 2013 ���.
 ** $cache = array("type" => "disk", "file" => "", "time" => 60, "action" = "get or set")
**/
global $Cache_Config, $memcache_server, $CURUSER, $default_language, $SITENAME;

/// �������������� ��� ��������, ��� ��� ����� ��� (�������� ������ ��� ����!)
if (!empty($Cache_Config["type"]) && !empty($cache["type"])) $cache["type"] = $Cache_Config["type"];

/// ����� ����� ����
$expire = $cache["time"]; if (empty($expire)) $expire = 60;

/// ��������� ����������� ��������
if (empty($cache["file"]) || empty($cache["action"])) return false;

$md5_querie = $cache["file"];

if (!empty($CURUSER["language"])) $md5_querie = substr($CURUSER["language"], 0, 3)."_".$md5_querie;
else $md5_querie = substr($default_language, 0, 3)."_".$md5_querie;

if (isset($Cache_Config["gzip_cache"]) && $Cache_Config["gzip_cache"] == true) $md5_querie.= "_gz";

if (isset($_GET["cache_deleting"]) && $CURUSER["class"] >= UC_SYSOP) {
unsql_cache($md5_querie);
if ($cache["action"] == "get") return false;
}

/// ���� ������ ��� ���� memcache - ����������
if ($cache["type"] == "memcache"){

if ($cache["action"] == "get"){

$result_cache = memcache_get($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie);

if ($result_cache == FALSE || !extension_loaded("memcache")) return false;
else {

if ($Cache_Config["gzip_cache"] == true) return gzinflate($result_cache); ///gzdeflate
else return $result_cache;

}


} elseif ($cache["action"] == "set"){

if ($Cache_Config["gzip_cache"] == true) $content = gzdeflate($content); ///gzinflate

memcache_set($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie, $content, 0, $expire);
}
/// ���� ������ ��� ���� xcache - ����������
} elseif ($cache["type"] == "xcache"){

if ($cache["action"] == "get"){
$result_cache = xcache_get("STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie);

if ($result_cache == FALSE || !extension_loaded("xcache")) return false;
else {

if ($Cache_Config["gzip_cache"] == true) return gzinflate($result_cache); ///gzdeflate
else return $result_cache;

}


} elseif ($cache["action"] == "set"){

if ($Cache_Config["gzip_cache"] == true) $content = gzdeflate($content); ///gzinflate

xcache_set("STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie, $content, $expire);
}


/// ���� ������ ��� ���� �� ���� - ����������
} elseif ($cache["type"] == "disk"){

$file = ROOT_PATH."/cache/".$md5_querie.".txt";

if ($cache["action"] == "get"){

if (file_exists($file) && (time() - filemtime($file)) < $expire) $temp_cache = file_get_contents($file);

if (empty($temp_cache)) return false;
else {

if ($Cache_Config["gzip_cache"] == true) return gzinflate($temp_cache); ///gzdeflate
else return $temp_cache;

}

} elseif($cache["action"] == "set"){

$fp = fopen($file, "w");
if ($Cache_Config["gzip_cache"] == true) fwrite($fp, gzdeflate($content)); ///gzinflate
else fwrite($fp, $content);

fclose($fp);

}

}

return false;
}


function unsql_cache ($fget_file, $adcache = false) {

/** 
 * ������ ���� ������ ��� Tesla (Tesla Tracker) v. Platinum �� 2013 ���.
**/

if (is_array($fget_file) && count($fget_file)){

foreach ($fget_file AS $fget_one){
unsql_cache ($fget_one);
}

return true;
}

global $Cache_Config, $memcache_server, $CURUSER, $default_language, $SITENAME;

/// �������������� ��� ��������, ��� ��� ����� ��� (�������� ������ ��� ����!)
if (!empty($Cache_Config["type"])) $cache_type = $Cache_Config["type"];

$md5_querie = $fget_file;

if (!empty($CURUSER["language"]) && $adcache == true) $md5_querie = substr($CURUSER["language"], 0, 3)."_".$md5_querie;
elseif ($adcache == true) $md5_querie = substr($default_language, 0, 3)."_".$md5_querie;

if ($Cache_Config["gzip_cache"] == true) $md5_querie.= "_gz";

/// ���� ������ ��� ���� memcache - ����������
if (($cache_type == "memcache" && $Cache_Config["memcache_on"] == true) || $Cache_Config["memcache_on"] == true){

memcache_delete ($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie);

/// ���� ������ ��� ���� xcache - ����������
} elseif ($cache_type == "xcache"){

if (extension_loaded("xcache"))
xcache_unset ("STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie);

/// ���� ������ ��� ���� �� ���� - ����������
} elseif ($cache_type == "disk" || empty($Cache_Config["type"])){

if (file_exists(ROOT_PATH."/cache/".$md5_querie.".txt")) unlink (ROOT_PATH."/cache/".$md5_querie.".txt");

}

if ($adcache == false) unsql_cache($fget_file, true);

return true;
}

function get_date_time($timestamp = 0) {

if ($timestamp) return date("Y-m-d H:i:s", $timestamp);
else
return date("Y-m-d H:i:s");

}

function gmtime() {

return strtotime(get_date_time());

}

function strip_magic_quotes($arr) {

foreach ($arr as $k => $v)	{

if (is_array($v))
$arr[$k] = strip_magic_quotes($v);
else
$arr[$k] = stripslashes($v);

}

return $arr;
}


function mksize($bytes) {
	if ($bytes < 1000 * 1024)
		return number_format($bytes / 1024, 2) . " kB";
	elseif ($bytes < 1000 * 1048576)
		return number_format($bytes / 1048576, 2) . " MB";
	elseif ($bytes < 1000 * 1073741824)
		return number_format($bytes / 1073741824, 2) . " GB";
	else
		return number_format($bytes / 1099511627776, 2) . " TB";
}


// IP ���������� ������ ��� ������� �������� ������������ ip �������
function validip($ip) {

if (!empty($ip) && $ip == long2ip(ip2long($ip)))
return true;
else 
return false;

}

function getip() {

global $Functs_Patch;

if ($Functs_Patch["ip"] == true){

// Code commented due to possible hackers/banned users to fake their ip with http headers
if (isset($_SERVER)) {

if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && validip($_SERVER['HTTP_X_FORWARDED_FOR']))
$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
elseif (isset($_SERVER['HTTP_CLIENT_IP']) && validip($_SERVER['HTTP_CLIENT_IP']))
$ip = $_SERVER['HTTP_CLIENT_IP'];
else
$ip = $_SERVER['REMOTE_ADDR'];

} else {

if (getenv('HTTP_X_FORWARDED_FOR') && validip(getenv('HTTP_X_FORWARDED_FOR')))
$ip = getenv('HTTP_X_FORWARDED_FOR');
elseif (getenv('HTTP_CLIENT_IP') && validip(getenv('HTTP_CLIENT_IP')))
$ip = getenv('HTTP_CLIENT_IP');
else
$ip = getenv('REMOTE_ADDR');

}
} else $ip = getenv('REMOTE_ADDR');

return $ip;
}

function validip_pmr($ip = false) {

if (empty($ip))	$ip = getip();

if (!empty($ip) && $ip == long2ip(ip2long($ip))) {

$reserved_ips = array (
array('80.94.240.0','80.94.255.255'), /// IDC
);

foreach ($reserved_ips as $r) {
$min = ip2long($r[0]);
$max = ip2long($r[1]);
if ((ip2long($ip) >= $min) && (ip2long($ip) <= $max)) return false;
}

return true;
} else return false;
}

function sqlesc($value) {

if (strlen($value) >= 10)
$value = str_ireplace(array("CREATE", "UPDATE", "DROP", "INSERT", "DATABASE", "javascript", "SUBSTRING", "FIND_IN_SET"), "", $value);

// Stripslashes
/*if (get_magic_quotes_gpc()) {
$value = stripslashes($value);
}*/

if (!is_numeric($value))
$value = "'".mysql_real_escape_string($value)."'"; /// Quote if not a number or a numeric string

return $value;
}


function hash_pad($hash) {
return str_pad($hash, 20);
}

function hash_where($name, $hash) {

$shhash = preg_replace('/ *$/s', "", $hash);

return "(".$name." = " . sqlesc($hash) . " OR ".$name." = " . sqlesc($shhash) . ")";
}

function gzip() {

if (!preg_match("/login/i", $_SERVER["SCRIPT_FILENAME"]) && !preg_match("/signup/i", $_SERVER["SCRIPT_FILENAME"]) && !preg_match("/recover/i", $_SERVER["SCRIPT_FILENAME"])){
    	
global $use_gzip;
static $already_loaded; /// ����� ������ ��� ��������
   
if (extension_loaded('zlib') && ini_get('zlib.output_compression') <>'1' && ini_get('output_handler') <> 'ob_gzhandler' && $use_gzip=='yes' && !$already_loaded) {
@ob_start('ob_gzhandler');

$already_loaded = true; /// �������� ��� ������������

} else {

/// ��������, ������� �� ��������� �����������, �� ����� ���� ��������, ��� ��� ��� ���������� �������� ��� ���������������� ����� ����������.
@ob_start(); 

}
}
}


/// ������� ����������� ������ Tesla TT v.Platinum


function sql_query($query, $cache = false) {
/** ������ ���� ������ ��� Tesla (Tesla Tracker) v. Platinum �� 2011 ���.
 ** $cache = array("type" => "disk", "file" => "", "time" => 60, "gzip_cache" => false)
**/
global $queries, $query_stat, $querytime, $Cache_Config, $memcache_server, $SITENAME;

$query_start_time = timer(); // ����� ������ �������


/// �������������� ��� ��������, ��� ��� ����� ��� (�������� ������ ��� ����!)
if (!empty($Cache_Config["type"]) && !empty($cache["type"]))
$cache["type"] = $Cache_Config["type"];

if ($Cache_Config["visible_cache"] == false && empty($cache["type"]))
++$queries; // ����� ������� ��������
elseif ($Cache_Config["visible_cache"] == true)
++$queries; // ����� ������� ��������

/// ����� ����� ����
$expire = $cache["time"];
if (empty($expire))
$expire = 60;

/// ���� ���������� ���� �������� - ���������� ���
if (empty($cache["file"]))
$md5_querie = md5($query);
else
$md5_querie = $cache["file"];

if (isset($Cache_Config["gzip_cache"]) && $Cache_Config["gzip_cache"] == true)
$md5_querie.= "_gz";

if (!empty($cache["type"])) $type = array();

/// ���� ������ ��� ���� memcache - ����������
if ($cache["type"] == "memcache" && $Cache_Config["memcache_on"] == true){

if (isset($_GET["cache_deleting"])) {
memcache_delete($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie,1);
$type[] = "D";
}

$result_cache = memcache_get($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie);

if ($result_cache == false || !extension_loaded("memcache")){

$temp_query = mysql_query($query); /// �������� ������

if (!$temp_query && DEBUG_MODE) {
$type[] = "E";
$mysql_error = mysql_error();
}

$temp_cache = array();

for ($i=0; $i < mysql_num_fields($temp_query); $i++){
$temp_cache['fields'][$i] = mysql_fetch_field($temp_query, $i);
}

for ($i=0; $i < mysql_num_rows($temp_query); $i++){
$temp_cache['data'][$i] = mysql_fetch_row($temp_query);
}

if ($Cache_Config["gzip_cache"] == true)
$true = memcache_set($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie, gzdeflate(serialize($temp_cache)), 0, $expire);
else
$true = memcache_set($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie, serialize($temp_cache), 0, $expire);

$type[] = "W";

//if ($true == false)
//$true = memcache_replace($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie, serialize($temp_cache), false, $expire);
} elseif ($result_cache == 's:0:"";'){

unset($result, $temp_cache);
/// ������� �������� �� �������
memcache_delete($memcache_server, "STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie,1);
$type[] = "D";

} else {

if ($Cache_Config["gzip_cache"] == true)
$result = unserialize(gzinflate($result_cache));
else
$result = unserialize($result_cache);

$type[] = "R";
}

//$type[] = "MemCache";
/// ���� ������ ��� ���� xcache - ����������
} elseif ($cache["type"] == "xcache"){

if (isset($_GET["cache_deleting"])) {
xcache_unset("STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie);
$type[] = "D";
}

$result_cache = xcache_get("STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie);

if ($result_cache == false || !extension_loaded("XCache")){

$temp_query = mysql_query($query); /// �������� ������

if (!$temp_query && DEBUG_MODE) {
$type[] = "E";
$mysql_error = mysql_error();
}


$temp_cache = array();

for ($i=0; $i < mysql_num_fields($temp_query); $i++){
$temp_cache['fields'][$i] = mysql_fetch_field($temp_query, $i);
}

for ($i=0; $i < mysql_num_rows($temp_query); $i++){
$temp_cache['data'][$i] = mysql_fetch_row($temp_query);
}

if ($Cache_Config["gzip_cache"] == true)
xcache_set("STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie, gzdeflate(serialize($temp_cache)), $expire);
else
xcache_set("STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie, serialize($temp_cache), $expire);

$type[] = "W";

} else {

if ($Cache_Config["gzip_cache"] == true)
$temp_cache = unserialize(gzinflate($result_cache));
else
$temp_cache = unserialize($result_cache);

$type[] = "R";
}

if (empty($temp_cache) || $temp_cache == 's:0:"";'){
unset($result,$temp_cache);
xcache_unset("STesla_".substr(crc32($SITENAME), 1, 5).":".$md5_querie);
$type[] = "D";
}

//$type[] = "Xcache";
/// ���� ������ ��� ���� �� ���� - ����������
} elseif ($cache["type"] == "disk"){

$file = ROOT_PATH."/cache/".$md5_querie.".txt";

if (isset($_GET["cache_deleting"])) {
unsql_cache($md5_querie);
$type[] = "D";
}

if (file_exists($file) && (time() - filemtime($file)) < $expire){

if ($Cache_Config["gzip_cache"] == true)
$temp_cache = unserialize(gzinflate(file_get_contents($file)));
else
$temp_cache = unserialize(file_get_contents($file));

$type[] = "R";
} else {

$temp_query = mysql_query($query); /// �������� ������

if (!$temp_query && DEBUG_MODE) {
$type[] = "E";
$mysql_error = mysql_error();
}

$temp_cache = array();

for ($i=0; $i < mysql_num_fields($temp_query); $i++){
$temp_cache['fields'][$i] = mysql_fetch_field($temp_query, $i);
}

for ($i=0; $i < mysql_num_rows($temp_query); $i++){
$temp_cache['data'][$i] = mysql_fetch_row($temp_query);
}

$fp = fopen($file, "w");
//flock($file, LOCK_SH);

if ($Cache_Config["gzip_cache"] == true)
fwrite($fp, gzdeflate(serialize($temp_cache)));
else
fwrite($fp, serialize($temp_cache));

fclose($fp);

$type[] = "W";
}

if (empty($temp_cache) || $temp_cache == 's:0:"";'){
unset($result, $temp_cache);
if ($file) unlink($file);
$type[] = "D"; 
}

} else {
$result = mysql_query($query); /// �������� ������

if (!$result && DEBUG_MODE) $mysql_error = mysql_error();

}

if (isset($temp_cache) && !isset($result))
$result = $temp_cache;
elseif (!isset($result) && !isset($temp_cache)){
$result = mysql_query($query); /// �������� ������

if (!$result && DEBUG_MODE) $mysql_error = mysql_error();

}

$query_end_time = timer(); // ����� ��������� �������
$query_time = ($query_end_time - $query_start_time);
$querytime = $querytime + $query_time;
$query_time = substr($query_time, 0, 8);

if (DEBUG_MODE && !isset($type)){
if (preg_match('/^DELETE FROM/i', $query))
$query.= " /*D: ".mysql_affected_rows()."*/";
elseif (preg_match('/^UPDATE/i', $query))
$query.= " /*U: ".mysql_modified_rows()."*/";
elseif (preg_match('/^INSERT/i', $query))
$query.= " /*I: ".mysql_insert_id()."*/";
}

if (isset($type)) $query.= " /*".$md5_querie."*/";
if (isset($mysql_error)) $query.= " E: ".$mysql_error;

if ($Cache_Config["visible_cache"] == false && !isset($type))
$query_stat[] = array("seconds" => $query_time, "query" => $query);
elseif ($Cache_Config["visible_cache"] == true)
$query_stat[] = array("seconds" => $query_time, "query" => $query, "cache" => (isset($cache["type"]) ? $cache["type"]." (".implode(",", $type).")":"")."");

return $result;
}


/* ���������� ����� ���������� ������� */
function mysql_num_rows_($row){
if (!isset($row['data']))
return @mysql_num_rows($row);
else
return count($row['data']);
}

/* ������������ ��� ���������� ������� � ���������� ��������������� ������ */
function mysql_fetch_row_($row){

global $NextRof;

$crc = crc32($row['fields'][0]->name.$row['fields'][0]->table.$row['fields'][0]->max_length);

if (empty($NextRof[$crc]))
$NextRof[$crc] = 0;

if (!isset($row['data']))
return mysql_fetch_row($row);

if ($NextRof[$crc] >= mysql_num_rows_($row)){
unset($NextRof[$crc]);
return false;
}

++$NextRof[$crc];
return $row['data'][$NextRof[$crc]-1];
}

/* ������������ ��� ���������� ������� � ���������� ������������� ������ */
function mysql_fetch_assoc_($row){

global $NextRow;

$crc = crc32($row['fields'][0]->name.$row['fields'][0]->table.$row['fields'][0]->max_length);

if (empty($NextRow[$crc]))
$NextRow[$crc] = 0;

if (!count($row["fields"]))
return mysql_fetch_assoc($row);
   	
if ($NextRow[$crc] >= count($row['data'])){
unset($NextRow[$crc]);
return false;
}

for ($i=0; $i < count($row['fields']); $i++){
$result[$row['fields'][$i]->name] = $row['data'][$NextRow[$crc]][$i];
}

++$NextRow[$crc]; 
return $result;
}

/// ����� ������� core_annouce.php � functions.php
?>