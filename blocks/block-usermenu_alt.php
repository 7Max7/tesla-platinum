<?php


global $CURUSER,  $tracker_lang;


echo "<div class=\"spoiler-wrap\" id=\"".date("Ymd21")."\"><div title=\"".$tracker_lang['open_spoiler']."\" class=\"spoiler-head folded clickable ClickEnD\">".$tracker_lang['user_menu']."</div><div class=\"spoiler-body\" style=\"display: none;\">";


echo "
<a class=\"menu\" href=\"my.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'my.php' ? "<b>".$tracker_lang['my']."</b>":"".$tracker_lang['my']."")."</a>"

."<a class=\"menu\" href=\"userdetails.php?id=".$CURUSER["id"]."\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'userdetails.php' ? "<b>".$tracker_lang['profile']."</b>":"".$tracker_lang['profile']."")."</a>"

."<a class=\"menu\" href=\"bookmarks.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'bookmarks.php' ? "<b>".$tracker_lang['bookmarks']."</b>":"".$tracker_lang['bookmarks']."")."</a>"

."<a class=\"menu\" href=\"checkcomm.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'checkcomm.php' ? "<b>".$tracker_lang['monitor']."</b>":$tracker_lang['monitor'])."</a>"

."<a class=\"menu\" href=\"mematch.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'mematch.php' ? "<b>".$tracker_lang['mematch']."</b>":$tracker_lang['mematch'])."</a>"

."<a class=\"menu\" href=\"mybonus.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'mybonus.php' ? "<b>".$tracker_lang['my_bonus']."</b>":$tracker_lang['my_bonus'])."</a>"

."<a class=\"menu\" href=\"mysimpaty.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'mysimpaty.php' ? "<b>".$tracker_lang['respect_my']."</b>":$tracker_lang['respect_my'])."</a>"

."<a class=\"menu\" href=\"invite.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'invite.php' ? "<b>".$tracker_lang['invite']."</b>":"".$tracker_lang['invite']."")."</a>"

."<a class=\"menu\" href=\"users.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'users.php' ? "<b>".$tracker_lang['users']."</b>":"".$tracker_lang['users']."")."</a>"

."<a class=\"menu\" href=\"smilies.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'smilies.php' ? "<b>".$tracker_lang['smilies']."</b>":"".$tracker_lang['smilies']."")."</a>"

."<a class=\"menu\" href=\"tfiles.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'tfiles.php' ? "<b>".$tracker_lang['dox']."</b>":$tracker_lang['dox'])."</a>"

."<a class=\"menu\" href=\"friends.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'friends.php' ? "<b>".$tracker_lang['personal_lists']."</b>":"".$tracker_lang['personal_lists']."")."</a>"

."<a class=\"menu\" href=\"subnet.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'subnet.php' ? "<b>".$tracker_lang['neighbours']."</b>":"".$tracker_lang['neighbours']."")."</a>"

."<a class=\"menu\" href=\"mytorrents.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'mytorrents.php' ? "<b>".$tracker_lang['torrent_my']."</b>":"".$tracker_lang['torrent_my']."")."</a>"

."<a class=\"menu\" href=\"getrss.php\">".(basename($_SERVER['SCRIPT_FILENAME']) == 'getrss.php' ? "<b>".$tracker_lang['rss_news']."</b>":$tracker_lang['rss_news'])."</a>"
;


echo "<div class=\"spoiler-foot ClickEnD\"><span style=\"line-height: normal;\">".$tracker_lang['close_spoiler'].": ".$tracker_lang['user_menu']."</span></div></div></div>";


?>