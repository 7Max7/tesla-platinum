<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $tracker_lang;

$blocktitle = ".:: <a title=\"".$tracker_lang['forum_main']."\" class=\"altlink_white\" href='forums.php'>".$tracker_lang['forum_main']."</a> :: <a title=\"".$tracker_lang['search']."\" class=\"altlink_white\" href='forums.php?action=search'>".$tracker_lang['search']."</a> :: <a title=\"".$tracker_lang['mail_unread_desc']."\" class=\"altlink_white\" href='forums.php?action=viewunread'>".$tracker_lang['mail_unread_desc']."</a> :: <a title=\"".$tracker_lang['forum_uniread']."\" class=\"altlink_white\" href='forums.php?action=catchup'>".$tracker_lang['forum_uniread']."</a> ::.";


?>
<style type="text/css">
<!--
#tabs_f {text-align: left;}
#tabs_f .tab_f { border: 1px solid #cecece;padding: 5px 10px 5px 10px;font-weight:bold;
/*background:#ededed; */
margin-right:5px;line-height: 23px;cursor: pointer;
/*font-weight: bold;*/
}
#tabs_f.active {border-bottom: none;padding-bottom: 5px;background: #FAFAFA;cursor: default;}
#tabs_f #body_f {border: 1px solid #cecece;padding: 5px;margin-bottom: 10px;background: #FAFAFA;}
#tabs_f .tab_f_error {background:url(../pic/error.gif) repeat-y;height: 34px;line-height: 34px;padding-left: 40px;}
#tabs_f{padding-top: 7px}
#tabs_f span{position: relative;border-bottom: 1px solid #FAFAFA !important;top: -1px;-webkit-border-top-left-radius: 4px;-webkit-border-top-right-radius: 4px;-moz-border-radius-topleft: 4px;-moz-border-radius-topright: 4px;border-top-left-radius: 4px;border-top-right-radius: 4px;}
#tabs_f span:hover{background: #FAFAFA;}
.active{color: #C60000; font-weight: bold;}
table.tt {width: 100%;}
table.tt td {padding: 5px;}
table.tt td.tt {background-color: #777;padding: 7px;}
-->
</style>

<script type="text/javascript">

var loading = "<img src=\"pic/loading.gif\" alt=\"��������..\" title=\"��������..\"/>";
jQuery(function() {
    jQuery(".tab_f").click ( function(){
        if(jQuery(this).hasClass("active"))
            return;
        else
        {
            jQuery("#loading").html(loading);
            var act = jQuery(this).attr("id");
            jQuery(this).toggleClass("active");
            jQuery(this).siblings("span").removeClass("active");
            jQuery.post("block-forums_jquery.php",{"act":act},function (response) {
                jQuery("#body_f").empty();
                jQuery("#body_f").append(response);
                jQuery("#loading").empty();
            });
        }
    });
    jQuery('.zebra:even').css({backgroundColor: '#EEEEEE'});
    if(jQuery.browser.msie) {
        width = jQuery('#profile_right h2').width();
        if (width > 422)
            jQuery('#profile_right').width(width);
        else
        {
            jQuery('#profile_right').width("422");
            jQuery('#profile_container').width("686");
        }
    }
});
</script>

<?

global $CURUSER;

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-forum_".$CURUSER["class"], "time" => 60*60, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

$content.= ("<div id=\"tabs_f\">\n");
$content.= ("<span class=\"tab_f active\" id=\"0\">".$tracker_lang['forum_last_comments']."</span>\n");

$content.= ("<span title=\"".$tracker_lang['forum_import_topics']."\" class=\"tab_f\" id=\"5\">".$tracker_lang['forum_important']."</span>\n");

$content.= ("<span title=\"".$tracker_lang['forum_active_threads']."\" class=\"tab_f\" id=\"1\">".$tracker_lang['active']."</span>\n");

$content.= ("<span title=\"".$tracker_lang['forum_hidden_topic']."\" class=\"tab_f\" id=\"2\">".$tracker_lang['forum_hidden']."</span>\n");

$content.= ("<span title=\"".$tracker_lang['forum_most_viewed']."\" class=\"tab_f\" id=\"3\">".$tracker_lang['forum_viewed']."</span>\n");

if ($CURUSER) // ���� ��� ��������
$content.= ("<span title=\"".$tracker_lang['forum_my_themes']."\" class=\"tab_f\" id=\"4\">".$tracker_lang['forum_my']."</span>\n");

$content.= ("<span id=\"loading\"></span>\n");
$content.= ("<div id=\"body_f\">\n");

///////// ����� ��������

$content.= "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
<tr> 
<td class=\"colhead\" align=\"left\" width=\"70%\">&nbsp;".$tracker_lang['subject']."</td>
<td class=\"colhead\" align=\"left\" width=\"30%\">&nbsp;".$tracker_lang['category']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['answers']." / ".$tracker_lang['views']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['news_poster']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['subscribe_last_comment']."</td>
</tr>";

if ($CURUSER && !empty($CURUSER["class"]))
$curuserclass = get_user_class();
else
$curuserclass = 1;

$for = sql_query("SELECT ft.*, ff.name as forumname, ff.description, ff.minclassread, (SELECT COUNT(*) FROM posts WHERE topicid = ft.id) AS post_num 
FROM topics as ft, forums as ff
WHERE ff.id = ft.forumid AND ft.visible = 'yes' AND ff.visible = 'yes' AND ff.minclassread <= ".sqlesc($curuserclass)." ORDER BY lastpost DESC LIMIT 10") or sqlerr(__FILE__, __LINE__);

$arraytopic = array();

while ($topicarr = mysql_fetch_assoc($for)) {

$polls_view = ($topicarr["polls"] == "yes" ? " <img width='13' title=\"".$tracker_lang['poll']."\" src=\"pic/forumicons/polls.gif\">":"");
    	 
$posts = $topicarr["post_num"];
$postsperpage=20;
$tpages = floor($posts / $postsperpage);

if ($tpages * $postsperpage != $posts)
++$tpages;

if ($tpages > 1){
$topicpages = " [";

for ($i = 1; $i <= $tpages; ++$i){
$topicpages .= "".($i==1 ? "":" ")."<a href='forums.php?action=viewtopic&topicid=".$topicarr["id"]."&page=".$i."'>".$i."</a>".($i<> $tpages ? "":"")."";
}
$topicpages .= "]";
}
else
$topicpages = "";
    	
$forumname = "<a title=\"".htmlspecialchars($topicarr["description"])."\" href=\"forums.php?action=viewforum&forumid=".$topicarr["forumid"]."\">".htmlspecialchars($topicarr["forumname"])."</a>";

$topicid = $topicarr["id"];
$arraytopic[] = $topicid;
$topic_userid = $topicarr["userid"];
$views = $topicarr["views"];
$sticky = $topicarr["sticky"];
$lastpost = $topicarr["lastpost"];
$posts = $topicarr["post_num"]; /// 
$replies = max(0, $posts - 1);

$res = sql_query("SELECT p.*, la.class AS la_class,la.username AS la_username, ow.class AS owner_class,ow.username AS owner_username
FROM posts AS p 
LEFT JOIN users AS la ON la.id = p.userid	
LEFT JOIN users AS ow ON ow.id = ".sqlesc($topic_userid)."	
WHERE topicid = ".sqlesc($topicid)." ORDER BY id DESC LIMIT 1") or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_assoc($res);

$postid = $arr["id"];
$userid = $arr["userid"];
$added = $arr["added"];
         
if ($arr["la_username"])
$username = "<a href='userdetails.php?id=".$userid."'>".get_user_class_color($arr["la_class"], $arr["la_username"])."</a>";
else 
$username = "id: ".$userid;

if ($arr["owner_username"])
$author = "<a href='userdetails.php?id=".$topic_userid."'>".get_user_class_color($arr["owner_class"], $arr["owner_username"])."</a>";
else 
$author = "id: ".$topic_userid;

$subject = "<a title=\"".$added."\" href=\"forums.php?action=viewtopic&topicid=".$topicid."&page=last#".$lastpost."\">".htmlspecialchars($topicarr["subject"])."</a>";


$content.= "<tr>
<td class=\"b\" align=\"left\">".($sticky == "yes" ? "<b>".$tracker_lang['stickyf']."</b>: ":"").$subject.$topicpages.$polls_view."</td>
<td class=\"b\" align=\"left\">".$forumname."</td>
<td class=\"b\" align=\"center\"><small>".$replies." / ".$views."</small></td>
<td class=\"b\" align=\"center\">".$author."</td>
<td class=\"b\" align=\"right\">".$username." <small>".$added."</small></td>
</tr>";

}


$content.= "<tr><td align=\"center\" colspan=\"5\" class=\"b\">";
$content.= "<small>".$tracker_lang['forum_hidupdate'].": ";

$for = sql_query("SELECT tp.id AS topid, tp.subject, tp.lastpost, tp.visible, tp.forumid, ft.minclassread 
FROM topics AS tp
LEFT JOIN forums AS ft ON ft.id = tp.forumid
WHERE ft.minclassread <= ".sqlesc($curuserclass)." AND (tp.visible = 'no' OR ft.visible = 'no') AND tp.lastdate > ".sqlesc(get_date_time(gmtime() - 86400*7))." ORDER BY tp.lastdate DESC LIMIT 5") or sqlerr(__FILE__, __LINE__); 
 
$first = array();
while ($farr = mysql_fetch_assoc($for)) {
$first[] = "<a href=\"forums.php?action=viewtopic&topicid=".$farr["topid"]."&page=last#".$farr["lastpost"]."\"><b>".htmlspecialchars($farr["subject"])."</b></a>";
}
    
if (count($first))
$content.= implode(",", $first);
else
$content.= "���.";
  
$content.= "</small></td></tr></tr>";
$content.= "</table>";

///////// ����� ��������
$content.= "</div>";
$content.= "</div>";

$filecache = file_query($content, $cache = array("type" => "disk", "file" => "block-forum_".$CURUSER["class"], "time" => 60*60, "action" => "set"));
}

?>