var tid = 0, x = 0, y = 0;
var obj;

document.onmousemove=track;

function track(e)
{
    x = (document.all) ? window.event.x + document.body.scrollLeft : e.pageX;
    y = (document.all) ? window.event.y + document.body.scrollTop : e.pageY;
}

function show(id)
{
    obj = document.getElementById(id);
    obj.style.left = x - 120;
    obj.style.top = y + 25;
    obj.style.display = "block";
    tid = window.setTimeout("show("+id+")",10);
}

function hide(id)
{
    obj = document.getElementById(id);
    window.clearTimeout(tid);
    obj.style.display = "none";
}
function show_hide(id)
{
        var klappText = document.getElementById('s' + id);
        var klappBild = document.getElementById('pic' + id);

        if (klappText.style.display == 'none') {
                  klappText.style.display = 'block';
                  klappBild.src = 'pic/minus.gif';
                  klappBild.title = '������';
        } else {
                  klappText.style.display = 'none';
                  klappBild.src = 'pic/plus.gif';
                  klappBild.title = '��������';
        }
}

jQuery(document).ready(function() {
    
	jQuery('div.spoiler-body img').each(function() {
		jQuery(this).attr({
			alt: jQuery(this).attr('src'),
			src: 'pic/loading_by_strong.gif'
		});
	});
	jQuery('div.spoiler-head').live("click", function() {
		jQuery(this).toggleClass('unfolded');
		var c = jQuery(this).parent().children('div.spoiler-body');
		c.find('img').each(function() {
			jQuery(this).attr('src', jQuery(this).attr('alt'));
		});
		c.is(':visible') ? c.hide('fast') : c.show('fast');
	}).addClass('ClickEnD');
 
    jQuery('div.spoiler-foot').click(function () {
		var d = jQuery(this).parents('div.spoiler-wrap')[0];
		jQuery('html, body').animate({scrollTop:jQuery(d).offset().top-1}, 80);
		jQuery(d).find('div.spoiler-head:first').click();
	}).addClass('ClickEnD');

});


function changeText(text, id){
document.getElementById(id).value = text;
}



var dhtmlgoodies_slideSpeed = 10;	// Higher value = faster
var dhtmlgoodies_timer = 10;	// Lower value = faster

var objectIdToSlideDown = false;
var dhtmlgoodies_activeId = false;
var dhtmlgoodies_slideInProgress = false;
function showHideContent(e,inputId) {
	if(dhtmlgoodies_slideInProgress)return;
	dhtmlgoodies_slideInProgress = true;
	if(!inputId)inputId = this.id;
	inputId = inputId + '';
	var numericId = inputId.replace(/[^0-9]/g,'');
	var answerDiv = document.getElementById('dhtmlgoodies_a' + numericId);

	objectIdToSlideDown = false;
	
	if(!answerDiv.style.display || answerDiv.style.display=='none'){		
		if(dhtmlgoodies_activeId &&  dhtmlgoodies_activeId!=numericId){			
			objectIdToSlideDown = numericId;
			slideContent(dhtmlgoodies_activeId,(dhtmlgoodies_slideSpeed*-1));
		}else{
			
			answerDiv.style.display='block';
			answerDiv.style.visibility = 'visible';
			
			slideContent(numericId,dhtmlgoodies_slideSpeed);
		}
	}else{
		slideContent(numericId,(dhtmlgoodies_slideSpeed*-1));
		dhtmlgoodies_activeId = false;
	}	
}

function slideContent(inputId,direction) {
	var obj =document.getElementById('dhtmlgoodies_a' + inputId);
	var contentObj = document.getElementById('dhtmlgoodies_ac' + inputId);
	height = obj.clientHeight;
	if(height==0)height = obj.offsetHeight;
	height = height + direction;
	rerunFunction = true;
	if(height>contentObj.offsetHeight){
		height = contentObj.offsetHeight;
		rerunFunction = false;
	}
	if(height<=1){		height = 1;		rerunFunction = false;	}

	obj.style.height = height + 'px';
	var topPos = height - contentObj.offsetHeight;
	if(topPos>0)topPos=0;
	contentObj.style.top = topPos + 'px';
	if(rerunFunction){
		setTimeout('slideContent(' + inputId + ',' + direction + ')',dhtmlgoodies_timer);
	}else{
		if(height<=1){
			obj.style.display='none'; 
			if(objectIdToSlideDown && objectIdToSlideDown!=inputId){
				document.getElementById('dhtmlgoodies_a' + objectIdToSlideDown).style.display='block';
				document.getElementById('dhtmlgoodies_a' + objectIdToSlideDown).style.visibility='visible';
				slideContent(objectIdToSlideDown,dhtmlgoodies_slideSpeed);				
			}else{
				dhtmlgoodies_slideInProgress = false;
			}
		}else{
			dhtmlgoodies_activeId = inputId;
			dhtmlgoodies_slideInProgress = false;
		}
	}
}



function initShowHideDivs() {
	var divs = document.getElementsByTagName('DIV');
	var divCounter = 1;
	for(var no=0;no<divs.length;no++){
		if(divs[no].className=='dhtmlgoodies_question'){
			divs[no].onclick = showHideContent;
			divs[no].id = 'dhtmlgoodies_q'+divCounter;
			var answer = divs[no].nextSibling;
			while(answer && answer.tagName!='DIV'){
				answer = answer.nextSibling;
			}
			answer.id = 'dhtmlgoodies_a'+divCounter;	
			contentDiv = answer.getElementsByTagName('DIV')[0];
			contentDiv.style.top = 0 - contentDiv.offsetHeight + 'px'; 	
			contentDiv.className='dhtmlgoodies_answer_content';
			contentDiv.id = 'dhtmlgoodies_ac' + divCounter;
			answer.style.display='none';
			answer.style.height='1px';
			divCounter++;
		}		
	}	
}
window.onload = initShowHideDivs;


var Paginator = function(paginatorHolderId, pagesTotal, pagesSpan, pageCurrent, baseUrl){
	if(!document.getElementById(paginatorHolderId) || !pagesTotal || !pagesSpan) return false;

	this.inputData = {
		paginatorHolderId: paginatorHolderId,
		pagesTotal: pagesTotal,
		pagesSpan: pagesSpan < pagesTotal ? pagesSpan : pagesTotal,
		pageCurrent: pageCurrent,
		baseUrl: baseUrl ? baseUrl : '/pages/'
	};

	this.html = {
		holder: null,

		table: null,
		trPages: null, 
		trScrollBar: null,
		tdsPages: null,

		scrollBar: null,
		scrollThumb: null,
			
		pageCurrentMark: null
	};


	this.prepareHtml();

	this.initScrollThumb();
	this.initPageCurrentMark();
	this.initEvents();

	this.scrollToPageCurrent();
} 
Paginator.prototype.prepareHtml = function(){

	this.html.holder = document.getElementById(this.inputData.paginatorHolderId);
	this.html.holder.innerHTML = this.makePagesTableHtml();

	this.html.table = this.html.holder.getElementsByTagName('table')[0];

	var trPages = this.html.table.getElementsByTagName('tr')[0]; 
	this.html.tdsPages = trPages.getElementsByTagName('td');

	this.html.scrollBar = getElementsByClassName(this.html.table, 'div', 'scroll_bar')[0];
	this.html.scrollThumb = getElementsByClassName(this.html.table, 'div', 'scroll_thumb')[0];
	this.html.pageCurrentMark = getElementsByClassName(this.html.table, 'div', 'current_page_mark')[0];
	if(this.inputData.pagesSpan == this.inputData.pagesTotal){
		addClass(this.html.holder, 'fullsize');
	}
}

Paginator.prototype.makePagesTableHtml = function(){
	var tdWidth = (100 / this.inputData.pagesSpan) + '%';

	var html = '' +
	'<table width="100%">' +
		'<tr>' 
			for (var i=1; i<=this.inputData.pagesSpan; i++){
				html += '<td width="' + tdWidth + '"></td>';
			}
			html += '' + 
		'</tr>' +
		'<tr>' +
			'<td colspan="' + this.inputData.pagesSpan + '">' +
				'<div class="scroll_bar">' + 
					'<div title="������� � ����� ������� �������" class="scroll_trough"></div>' + 
					'<div class="scroll_thumb">' + 
						'<div title="������� � ����������� ���� ���������" class="scroll_knob"></div>' + 
					'</div>' + 
					'<div class="current_page_mark"></div>' + 
				'</div>' +
			'</td>' +
		'</tr>' +
	'</table>';

	return html;
}
Paginator.prototype.initScrollThumb = function(){
	this.html.scrollThumb.widthMin = '8'; // minimum width of the scrollThumb (px)
	this.html.scrollThumb.widthPercent = this.inputData.pagesSpan/this.inputData.pagesTotal * 100;

	this.html.scrollThumb.xPosPageCurrent = (this.inputData.pageCurrent - Math.round(this.inputData.pagesSpan/2))/this.inputData.pagesTotal * this.html.table.offsetWidth;
	this.html.scrollThumb.xPos = this.html.scrollThumb.xPosPageCurrent;

	this.html.scrollThumb.xPosMin = 0;
	this.html.scrollThumb.xPosMax;

	this.html.scrollThumb.widthActual;

	this.setScrollThumbWidth();
	
}

Paginator.prototype.setScrollThumbWidth = function(){
	this.html.scrollThumb.style.width = this.html.scrollThumb.widthPercent + "%";
	this.html.scrollThumb.widthActual = this.html.scrollThumb.offsetWidth;
	if(this.html.scrollThumb.widthActual < this.html.scrollThumb.widthMin){
		this.html.scrollThumb.style.width = this.html.scrollThumb.widthMin + 'px';
	}
	this.html.scrollThumb.xPosMax = this.html.table.offsetWidth - this.html.scrollThumb.widthActual;
}

Paginator.prototype.moveScrollThumb = function(){
	this.html.scrollThumb.style.left = this.html.scrollThumb.xPos + "px";
}
Paginator.prototype.initPageCurrentMark = function(){
	this.html.pageCurrentMark.widthMin = '3';
	this.html.pageCurrentMark.widthPercent = 100 / this.inputData.pagesTotal;
	this.html.pageCurrentMark.widthActual;

	this.setPageCurrentPointWidth();
	this.movePageCurrentPoint();
}

Paginator.prototype.setPageCurrentPointWidth = function(){
	this.html.pageCurrentMark.style.width = this.html.pageCurrentMark.widthPercent + '%';
	this.html.pageCurrentMark.widthActual = this.html.pageCurrentMark.offsetWidth;
	if(this.html.pageCurrentMark.widthActual < this.html.pageCurrentMark.widthMin){
		this.html.pageCurrentMark.style.width = this.html.pageCurrentMark.widthMin + 'px';
	}
}

Paginator.prototype.movePageCurrentPoint = function(){
	if(this.html.pageCurrentMark.widthActual < this.html.pageCurrentMark.offsetWidth){
		this.html.pageCurrentMark.style.left = (this.inputData.pageCurrent - 1)/this.inputData.pagesTotal * this.html.table.offsetWidth - this.html.pageCurrentMark.offsetWidth/2 + "px";
	} else {
		this.html.pageCurrentMark.style.left = (this.inputData.pageCurrent - 1)/this.inputData.pagesTotal * this.html.table.offsetWidth + "px";
	}
}

Paginator.prototype.initEvents = function(){
	var _this = this;

	this.html.scrollThumb.onmousedown = function(e){
		if (!e) var e = window.event;
		e.cancelBubble = true;
		if (e.stopPropagation) e.stopPropagation();

		var dx = getMousePosition(e).x - this.xPos;
		document.onmousemove = function(e){
			if (!e) var e = window.event;
			_this.html.scrollThumb.xPos = getMousePosition(e).x - dx;
			_this.moveScrollThumb();
			_this.drawPages();
			
			
		}
		document.onmouseup = function(){
			document.onmousemove = null;
			_this.enableSelection();
		}
		_this.disableSelection();
	}

	this.html.scrollBar.onmousedown = function(e){
		if (!e) var e = window.event;
		if(matchClass(_this.paginatorBox, 'fullsize')) return;
		
		_this.html.scrollThumb.xPos = getMousePosition(e).x - getPageX(_this.html.scrollBar) - _this.html.scrollThumb.offsetWidth/2;
		
		_this.moveScrollThumb();
		_this.drawPages();
		
		
	}

	addEvent(window, 'resize', function(){Paginator.resizePaginator(_this)});
}

Paginator.prototype.drawPages = function(){
	var percentFromLeft = this.html.scrollThumb.xPos/(this.html.table.offsetWidth);
	var cellFirstValue = Math.round(percentFromLeft * this.inputData.pagesTotal);
	
	var html = "";
	if(cellFirstValue < 1){
		cellFirstValue = 1;
		this.html.scrollThumb.xPos = 0;
		this.moveScrollThumb();
	} else if(cellFirstValue >= this.inputData.pagesTotal - this.inputData.pagesSpan) {
		cellFirstValue = this.inputData.pagesTotal - this.inputData.pagesSpan + 1;
		this.html.scrollThumb.xPos = this.html.table.offsetWidth - this.html.scrollThumb.offsetWidth;
		this.moveScrollThumb();
	}

	

	for(var i=0; i<this.html.tdsPages.length; i++){
		var cellCurrentValue = cellFirstValue + i;
		if(cellCurrentValue == this.inputData.pageCurrent){
			html = "<span title='�� �� ������ ���������'>" + "<strong>" + cellCurrentValue + "</strong>" + "</span>";
		} else {
			html = "<span title='������� � ���� ���������'>" + "<a href='" + this.inputData.baseUrl + cellCurrentValue + "'>" + cellCurrentValue + "</a>" + "</span>";
		}
		this.html.tdsPages[i].innerHTML = html;
	}
}

Paginator.prototype.scrollToPageCurrent = function(){
	this.html.scrollThumb.xPosPageCurrent = (this.inputData.pageCurrent - Math.round(this.inputData.pagesSpan/2))/this.inputData.pagesTotal * this.html.table.offsetWidth;
	this.html.scrollThumb.xPos = this.html.scrollThumb.xPosPageCurrent;
		this.moveScrollThumb();
	this.drawPages();
	}



Paginator.prototype.disableSelection = function(){
	document.onselectstart = function(){
		return false;
	}
	this.html.scrollThumb.focus();	
}

Paginator.prototype.enableSelection = function(){
	document.onselectstart = function(){
		return true;
	}
}

Paginator.resizePaginator = function (paginatorObj){

	paginatorObj.setPageCurrentPointWidth();
	paginatorObj.movePageCurrentPoint();

	paginatorObj.setScrollThumbWidth();
	paginatorObj.scrollToPageCurrent();
}

function getElementsByClassName(objParentNode, strNodeName, strClassName){
	var nodes = objParentNode.getElementsByTagName(strNodeName);
	if(!strClassName){
		return nodes;	
	}
	var nodesWithClassName = [];
	for(var i=0; i<nodes.length; i++){
		if(matchClass( nodes[i], strClassName )){
			nodesWithClassName[nodesWithClassName.length] = nodes[i];
		}	
	}
	return nodesWithClassName;
}


function addClass( objNode, strNewClass ) {
	replaceClass( objNode, strNewClass, '' );
}

function removeClass( objNode, strCurrClass ) {
	replaceClass( objNode, '', strCurrClass );
}

function replaceClass( objNode, strNewClass, strCurrClass ) {
	var strOldClass = strNewClass;
	if ( strCurrClass && strCurrClass.length ){
		strCurrClass = strCurrClass.replace( /\s+(\S)/g, '|$1' );
		if ( strOldClass.length ) strOldClass += '|';
		strOldClass += strCurrClass;
	}
	objNode.className = objNode.className.replace( new RegExp('(^|\\s+)(' + strOldClass + ')($|\\s+)', 'g'), '$1' );
	objNode.className += ( (objNode.className.length)? ' ' : '' ) + strNewClass;
}

function matchClass( objNode, strCurrClass ) {
	return ( objNode && objNode.className.length && objNode.className.match( new RegExp('(^|\\s+)(' + strCurrClass + ')($|\\s+)') ) );
}


function addEvent(objElement, strEventType, ptrEventFunc) {
	if (objElement.addEventListener)
		objElement.addEventListener(strEventType, ptrEventFunc, false);
	else if (objElement.attachEvent)
		objElement.attachEvent('on' + strEventType, ptrEventFunc);
}
function removeEvent(objElement, strEventType, ptrEventFunc) {
	if (objElement.removeEventListener) objElement.removeEventListener(strEventType, ptrEventFunc, false);
		else if (objElement.detachEvent) objElement.detachEvent('on' + strEventType, ptrEventFunc);
}


function getPageY( oElement ) {
	var iPosY = oElement.offsetTop;
	while ( oElement.offsetParent != null ) {
		oElement = oElement.offsetParent;
		iPosY += oElement.offsetTop;
		if (oElement.tagName == 'BODY') break;
	}
	return iPosY;
}

function getPageX( oElement ) {
	var iPosX = oElement.offsetLeft;
	while ( oElement.offsetParent != null ) {
		oElement = oElement.offsetParent;
		iPosX += oElement.offsetLeft;
		if (oElement.tagName == 'BODY') break;
	}
	return iPosX;
}

function getMousePosition(e) {
	if (e.pageX || e.pageY){
		var posX = e.pageX;
		var posY = e.pageY;
	}else if (e.clientX || e.clientY) 	{
		var posX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		var posY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	return {x:posX, y:posY}	
}











var scrolltotop={
	setting: {startline:600, scrollduration:500, fadeduration:[500, 100]},
	controlHTML: '<img src="./pic/up.png" style="width:35px; height:35px" />',
	controlattrs: {offsetx:5, offsety:10},
	anchorkeyword: '#atop',
	state: {isvisible:false, shouldvisible:false},
	scrollup:function(){
		if (!this.cssfixedsupport)
			this.$control.css({opacity:0})
		this.$body.animate({scrollTop: 0}, this.setting.scrollduration);
	},
	keepfixed:function(){
		var $window=jQuery(window)
		var controlx=$window.scrollLeft() + $window.width() - this.$control.width() - this.controlattrs.offsetx
		var controly=$window.scrollTop() + $window.height() - this.$control.height() - this.controlattrs.offsety
		this.$control.css({left:controlx+'px', top:controly+'px'})
	},
	togglecontrol:function(){
		var scrolltop=jQuery(window).scrollTop()
		if (!this.cssfixedsupport) this.keepfixed()
		this.state.shouldvisible=(scrolltop>=this.setting.startline)? true : false
		if (this.state.shouldvisible && !this.state.isvisible){
			this.$control.stop().animate({opacity:1}, this.setting.fadeduration[0])
			this.state.isvisible=true
		}
		else if (this.state.shouldvisible==false && this.state.isvisible){
			this.$control.stop().animate({opacity:0}, this.setting.fadeduration[1])
			this.state.isvisible=false
		}
	},
	init:function(){
		jQuery(document).ready(function($){
			var mainobj=scrolltotop
			var iebrws=document.all
			mainobj.cssfixedsupport=!iebrws || iebrws && document.compatMode=="CSS1Compat" && window.XMLHttpRequest
			mainobj.$body=$('html,body')
			mainobj.$control=$('<div id="topcontrol">'+mainobj.controlHTML+'</div>')
				.css({position:mainobj.cssfixedsupport? 'fixed' : 'absolute', bottom:mainobj.controlattrs.offsety, right:mainobj.controlattrs.offsetx, opacity:0, cursor:'pointer'})
			///	.attr({title:'������ ���������!'})
				.click(function(){mainobj.scrollup(); return false})
				.appendTo('body')
			if (document.all && !window.XMLHttpRequest && mainobj.$control.text()!='')
				mainobj.$control.css({width:mainobj.$control.width()})
			mainobj.togglecontrol()
			$('a[href="' + mainobj.anchorkeyword +'"]').click(function(){
				mainobj.scrollup()
				return false
			})
			$(window).bind('scroll resize', function(e){
				mainobj.togglecontrol()
			})
		})
	}
}
scrolltotop.init()



function textbb_udesck(id){
var txt = document.getElementById(id).value;
txt = txt.replace('���������� � ������: ', '[u]���������� � ������[/u]: ')
txt = txt.replace('���������� � ������ :', '[u]���������� � ������[/u]: ')

txt = txt.replace('����� ���������:', '[b]����� ���������[/b]: ')
txt = txt.replace('����� ��������� :', '[b]����� ���������[/b]: ')

txt = txt.replace('����� �������� :', '[b]����� ��������[/b]: ')
txt = txt.replace('����� ��������:', '[b]����� ��������[/b]: ')

txt = txt.replace('�������, ��������: ', '[b]�������, ��������[/b]: ')
txt = txt.replace('�������, �������� :', '[b]�������, ��������[/b]: ')

txt = txt.replace('��������� :', '[b]���������[/b]: ')
txt = txt.replace('���������:', '[b]���������[/b]: ')

txt = txt.replace('���� ����� :', '[b]���� �����[/b]: ')
txt = txt.replace('���� �����:', '[b]���� �����[/b]: ')

txt = txt.replace('����������� :', '[b]�����������[/b]: ')
txt = txt.replace('�����������:', '[b]�����������[/b]: ')

txt = txt.replace('���� ������������ :', '[b]���� ������������[/b]: ')
txt = txt.replace('���� ������������:', '[b]���� ������������[/b]: ')

txt = txt.replace('����������� :', '[b]�����������[/b]: ')
txt = txt.replace('�����������:', '[b]�����������[/b]: ')

txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('��������:', '[b]��������[/b]: ')

txt = txt.replace('���. ���������� :', '[b]���. ����������[/b]: ')
txt = txt.replace('���. ����������:', '[b]���. ����������[/b]: ')

txt = txt.replace('����� :', '[b]�����[/b]: ')
txt = txt.replace('�����:', '[b]�����[/b]: ')

txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')

txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')

txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')

txt = txt.replace('��� :', '[b]���[/b]: ')
txt = txt.replace('���:', '[b]���[/b]: ')

txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')

txt = txt.replace('�������:', '[b]�������[/b]: ')
txt = txt.replace('������� :', '[b]�������[/b]: ')

txt = txt.replace('������������ �������� :', '[b]������������ ��������[/b]: ')
txt = txt.replace('������������ ��������:', '[b]������������ ��������[/b]: ')

txt = txt.replace('���������� �������� :', '[b]���������� ��������[/b]: ')
txt = txt.replace('���������� ��������:', '[b]���������� ��������[/b]: ')

txt = txt.replace('������� ��������:', '[b]������� ��������[/b]: ')
txt = txt.replace('������� �������� :', '[b]������� ��������[/b]: ')

txt = txt.replace('������� :', '[b]�������[/b]: ')
txt = txt.replace('�������:', '[b]�������[/b]: ')

txt = txt.replace('� ������ :', '[b]� ������[/b]: ')
txt = txt.replace('� ������:', '[b]� ������[/b]: ')


txt = txt.replace('����������� :', '[b]�����������[/b]: ')
txt = txt.replace('�����������:', '[b]�����������[/b]: ')

txt = txt.replace('�������:', '[b]��������[/b]: ')
txt = txt.replace('������� :', '[b]��������[/b]: ')

txt = txt.replace('���������� �������: ', '[b]���������� �������[/b]: ')
txt = txt.replace('���������� ������� :', '[b]���������� �������[/b]: ')

txt = txt.replace('� ����:', '[b]� ����[/b]: ')
txt = txt.replace('� ���� :', '[b]� ����[/b]: ')

txt = txt.replace('����������� ��������� ����������:', '[b]����������� ��������� ����������[/b]: ')
txt = txt.replace('����������� ��������� ���������� :', '[b]����������� ��������� ����������[/b]: ')

txt = txt.replace('����������� :', '[b]�����������[/b]: ')
txt = txt.replace('�����������:', '[b]�����������[/b]: ')

txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')

txt = txt.replace('�������:', '[b]�������[/b]: ')
txt = txt.replace('������� :', '[b]�������[/b]: ')

txt = txt.replace('Trailer:', '[b]�������[/b]: ')
txt = txt.replace('Trailer :', '[b]�������[/b]: ')

txt = txt.replace('System:', '[b]System[/b]: ')
txt = txt.replace('System :', '[b]System[/b]: ')

txt = txt.replace('CPU:', '[b]CPU[/b]: ')
txt = txt.replace('CPU :', '[b]CPU[/b]: ')


txt = txt.replace('Memory:', '[b]Memory[/b]: ')
txt = txt.replace('Memory :', '[b]Memory[/b]: ')

txt = txt.replace('HDD:', '[b]HDD[/b]: ')
txt = txt.replace('HDD :', '[b]HDD[/b]: ')

txt = txt.replace('Graphics:', '[b]Graphics[/b]: ')
txt = txt.replace('Graphics :', '[b]Graphics[/b]: ')


txt = txt.replace('����� ������ :', '[b]����� ������[/b]: ')
txt = txt.replace('����� ������:', '[b]����� ������[/b]: ')

txt = txt.replace('����� ������������ :', '[b]����� ������������[/b]: ')
txt = txt.replace('����� ������������:', '[b]����� ������������[/b]: ')

txt = txt.replace('��������:', '[b]��������[/b]: ')
txt = txt.replace('������������ ��������:', '[b]������������ ��������[/b]: ')
txt = txt.replace('��� ������:', '[b]��� ������[/b]: ')
txt = txt.replace('����:', '[b]����[/b]: ')
txt = txt.replace('�����:', '[b]����[/b]: ')

txt = txt.replace('��������:', '[b]��������[/b]: ')

txt = txt.replace('� �����:', '[b]� �����[/b]: ')
txt = txt.replace('� ������:', '[b]� ������[/b]: ')
txt = txt.replace('��������:', '[b]��������[/b]: ')
txt = txt.replace('�����������������:', '[b]�����������������[/b]: ')
txt = txt.replace('�������:', '[b]�������[/b]: ')
txt = txt.replace('��������:', '[b]��������[/b]: ')
txt = txt.replace('�������������:', '[b]�������������[/b]: ')
txt = txt.replace('����\n', '[u]����[/u] \n')
txt = txt.replace('����: ', '[b]����[/b]: ')
txt = txt.replace('���� :', '[b]����[/b]: ')
txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('������(�):', '[b]������(�)[/b]: ')
txt = txt.replace('��������:', '[b]��������[/b]: ')
txt = txt.replace('�����:', '[b]�����[/b]: ')
txt = txt.replace('����:', '[b]����[/b]: ')
txt = txt.replace('�����������:', '[b]�����������[/b]: ')
txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('���������:', '[b]���������[/b]: ')
txt = txt.replace('���� ����������:', '[b]���� ����������[/b]: ')
txt = txt.replace('���������:', '[b]���������[/b]: ')
txt = txt.replace('��������:', '[b]��������[/b]: ')
txt = txt.replace('���. ����������:', '[b]���. ����������[/b]: ')
txt = txt.replace('������������:', '[b]������������[/b]: ')
txt = txt.replace('�������:', '[b]�������[/b]: ')
txt = txt.replace('����� ��� ������:', '[b]����� ��� ������[/b]: ')
txt = txt.replace('����:', '[b]����[/b]: ')
txt = txt.replace('� �����:', '[b]� �����[/b]: ')
txt = txt.replace('�� ����:', '[b]�� ����[/b]: ')
txt = txt.replace('����������� ����:', '[b]����������� ����[/b]: ')
txt = txt.replace('��������� ����������:', '[b]��������� ����������[/b]: ')
txt = txt.replace('��������:', '[b]��������[/b]: ')
txt = txt.replace('����������:', '[b]����������[/b]: ')
txt = txt.replace('����������� ����������:', '[b]����������� ����������[/b]: ')
txt = txt.replace('������������ ����������:', '[b]������������ ����������[/b]: ')
txt = txt.replace('��������:', '[b]��������[/b]: ')
txt = txt.replace('�� �������� ', '[b]�� ��������[/b]: ')
txt = txt.replace('�������� �������:', '[b]�������� �������[/b]: ')
txt = txt.replace('������������ ���:', '[b]������������ ���[/b]: ')
txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('������:', '[b]������[/b]: ')
txt = txt.replace('��� �������:', '[b]��� �������[/b]: ')
txt = txt.replace('��������:', '[u]��������[/u]: ')
txt = txt.replace('����� �����:', '[b]����� �����[/b]: ')
txt = txt.replace('����� �����:', '[b]����� �����[/b]: ')
txt = txt.replace('�����:', '[b]�����[/b]: ')
txt = txt.replace('������ :', '[u]������[/u]: ')
txt = txt.replace('������:', '[u]������[/u]: ')

txt = txt.replace('������ ������ :', '[b]������ ������[/b]: ')
txt = txt.replace('������ ������: ', '[b]������ ������[/b]: ')

txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('������������ �������� :', '[b]������������ ��������[/b]: ')
txt = txt.replace('��� ������ :', '[b]��� ������[/b]: ')
txt = txt.replace('���� :', '[b]����[/b]: ')
txt = txt.replace('����� :', '[b]����[/b]: ')
txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('� ����� :', '[b]� �����[/b]: ')
txt = txt.replace('� ������ :', '[b]� ������[/b]: ')
txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('����������������� :', '[b]�����������������[/b]: ')
txt = txt.replace('������� :', '[b]�������[/b]: ')
txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('������������� :', '[b]�������������[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')
txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('����� :', '[b]�����[/b]: ')
txt = txt.replace('���� :', '[b]����[/b]: ')
txt = txt.replace('����������� :', '[b]�����������[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')
txt = txt.replace('��������� :', '[b]���������[/b]: ')
txt = txt.replace('���� ���������� :', '[b]���� ����������[/b]: ')
txt = txt.replace('��������� :', '[b]���������[/b]: ')
txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('���. ���������� :', '[b]���. ����������: [/b]')
txt = txt.replace('������������ :', '[b]������������[/b]: ')
txt = txt.replace('������� :', '[b]�������[/b]: ')
txt = txt.replace('����� ��� ������ :', '[b]����� ��� ������[/b]: ')
txt = txt.replace('���� :', '[b]����[/b]: ')
txt = txt.replace('� ����� :', '[b]� �����[/b]: ')
txt = txt.replace('�� ���� :', '[b]�� ����[/b]: ')
txt = txt.replace('����������� ���� :', '[b]����������� ����[/b]: ')
txt = txt.replace('��������� ���������� :', '[b]��������� ����������: [/b]')
txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('������(�) :', '[b]������(�)[/b]: ')
txt = txt.replace('���������� :', '[b]����������[/b]: ')
txt = txt.replace('����������� ���������� :', '[b]����������� ����������[/b]: ')
txt = txt.replace('������������ ���������� :', '[b]������������ ����������[/b]: ')
txt = txt.replace('�������� :', '[b]��������[/b]: ')
txt = txt.replace('�������� ������� :', '[b]�������� �������[/b]: ')
txt = txt.replace('������������ ��� :', '[b]������������ ���[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')
txt = txt.replace('������ :', '[b]������[/b]: ')
txt = txt.replace('��� ������� :', '[b]��� �������[/b]: ')
txt = txt.replace('�������� :', '[u]��������[/u]: ')
txt = txt.replace('����� ����� :', '[b]����� �����[/b]: ')
txt = txt.replace('����� ����� :', '[b]����� �����[/b]: ')
txt = txt.replace('����� :', '[b]�����[/b]: ')

txt = txt.replace(/http:\/\/www."(.+)">/gi, '<a href="$1"><img src="$1"></a>')


document.getElementById(id).value = txt;
}
