<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

global $CURUSER;

echo "<style type=\"text/css\">
table.tabs { background-color: transporent; border-collapse: collapse; border: 0px none; }
table.tabs td { border: 0px none; font: bold 11px Arial; color: #57533C; white-space: nowrap; }
table.tabs td a, table.tabs td a:visited { font-weight: bold; color: #57533C; text-decoration: none; }
table.tabs td a:hover { color: #000000; }
table.tabs td.active { background-image: url('../../pic/tabs/bg_active.gif'); background-color: #000000; color: #FFFFFF; }
table.tabs td.notactive { background-image: url('../../pic/tabs/bg.gif'); color: #57533C; }
table.tabs td.space { width: 100%; background-image: none; text-align: right; }
table.ustats { border-collapse: separate; border: 1px solid #000000; }
table.ustats td { white-space: nowrap; }
table.ustats td.head { background-color: #000000; border: 1px solid #000000; font-weight: bold; font-size: 7pt; color: #FFFFFF; }
table.ustats td.cell   { border: 0px none; border-bottom: 1px solid #DDDDDD; }
table.ustats td.hhcell { background-color: #EEEEF8; border-top: 1px solid #6FDF1B; border-bottom: 1px solid #6FDF1B; border-left: 0px none; border-right: 0px none; color: #000000; }
table.ustats td.hvcell { background-color: #EEEEF8; border-top: 0px none; border-bottom: 1px solid #DDDDDD; border-left: 1px solid #6FDF1B; border-right: 1px solid #6FDF1B; }
table.ustats td.hccell { background-color: #6FDF1B; border-top: 1px solid #F8C71E; border-bottom: 1px solid #F8C71E; border-left: 1px solid #F8C71E; border-right: 1px solid #F8C71E; color: #000000; }
table.ustats td.hhcell a, table.ustats td.hhcell a:visited, table.ustats td.hccell a, table.ustats td.hccell a:visited { font-weight: bold; color: #000000; text-decoration: none; }
table.ustats td.hhcell a:hover, table.ustats td.hccell a:hover { color: #FF0000; }
table.ustats td.foot { background-color: #F5F4EA; border: 1px solid #F5F4EA; text-align: right; color: #000000; }
table.ustats td.foot a, table.ustats td.foot a:visited { font-weight: normal; color: #000000; text-decoration: underline; }
table.ustats td.foot a:hover { color: #FF0000; }
</style>";

function createtabs($tabs, $activetab, $title = '', $width = '100%') {

global $SITENAME ;

$result = '';
$count = count($tabs);
$num = 0;

if ($count){
$width = preg_match('/^(\d{1,4})(%|px)*$/', $width) ? ' width="'.$width.'"' : ' widht="100%"';
$result = "<table class=\"tabs\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"".$width."><tr>";

foreach($tabs as $tabname => $tabtitle) {

++$num;

$curactivetab = ($tabname == $activetab);

if ($num == 1)
$result .= ($curactivetab ? "<td><img src=\"./pic/tabs/begin_active.gif\" border=\"0\"></td>" : "<td><img src=\"./pic/tabs/begin.gif\" border=\"0\"></td>");
elseif($curactivetab)
$result .= "<td><img src=\"./pic/tabs/div_active_in.gif\" border=\"0\"></td>";
elseif (!$prevactive)
$result .= "<td><img src=\"./pic/tabs/div.gif\" border=\"0\"></td>";

$result .= ($curactivetab ? "<td class=\"active\">&nbsp;".strip_tags($tabtitle)."&nbsp;</td>" : "<td class=\"notactive\">&nbsp;".$tabtitle."&nbsp;</td>");

if ($num == $count)
$result .= ($curactivetab ? "<td><img src=\"./pic/tabs/end_active.gif\" border=\"0\"></td>" : "<td><img src=\"./pic/tabs/end.gif\" border=\"0\"></td>");
elseif ($curactivetab)
$result .= "<td><img src=\"./pic/tabs/div_active_out.gif\" border=\"0\"></td>";

$prevactive = $curactivetab;
}

$result .= "<td class=\"space\">".$title."</td></tr>\t</table>";
}

echo $result;
}

function usertable($res, $hcol = '', $width = '100%') {

global $CURUSER, $ss_uri, $tracker_lang;

$width  = preg_match('/^(\d{1,4})(%|px)*$/', $width) ? ' width="'.$width.'"' : '';

echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" class=\"ustats\" ".$width.">";

echo "<tr>
<td class=\"head\">#</td>
<td class=\"head\" align=\"left\">".$tracker_lang['signup_username']."</td>
<td class=\"head\" align=\"right\">".$tracker_lang['uploaded']."</td>
<td class=\"head\" align=\"right\">".$tracker_lang['speed']." U</td>
<td class=\"head\" align=\"right\">".$tracker_lang['downloaded']."</td>
<td class=\"head\" align=\"right\">".$tracker_lang['speed']." D</td>
<td class=\"head\" align=\"right\">".$tracker_lang['ratio']."</td>
<td class=\"head\" align=\"right\">".$tracker_lang['bonuses']."</td>
<td class=\"head\" align=\"right\">".$tracker_lang['respects']."</td>
<td class=\"head\" align=\"right\">".$tracker_lang['commens']."</td>
<td class=\"head\" align=\"left\">".$tracker_lang['added']."</td>
</tr>";

$num = 0;
while($a = mysql_fetch_assoc($res)){

if ($hcol == 'comments' && !$a['commentsnum'])
continue;

++$num;
$highlightrow = ($CURUSER["id"] == $a["userid"]);

foreach (explode(':', 'uploaded:upspeed:downloaded:downspeed:ratio:bonus:thanks:simpaty:comments:none') as $col) {

if ($highlightrow && $hcol == $col && $hcol != 'none')
$styleclass[$col] = 'hccell';
elseif (!$highlightrow && $hcol == $col && $hcol != 'none')
$styleclass[$col] = 'hvcell';
elseif ($highlightrow && $hcol != $col)
$styleclass[$col] = 'hhcell';
else
$styleclass[$col] = 'cell';
}

if ($a["downloaded"]) {
$ratio = $a["uploaded"] / $a["downloaded"];
$color = get_ratio_color($ratio);
$ratio = number_format($ratio, 2);

if ($color)
$ratio = "<font color=\"".$color."\">".$ratio."</font>";
} else
$ratio = "Inf.";

$simpaty = $a['simpaty'] < 0 ? '<FONT color="#FF0000">'.$a['simpaty'].'</FONT>' : $a['simpaty'];

if ($a['userid'] == $CURUSER['id']){
$bonus = '<A href="mybonus.php" class="online">'.$a["bonus"].'</A>';
$thanks = $a['thanks'] ? '<A href="mythanks.php" class="online">'.$a['thanks'].'</A>' : $a['thanks'];
$simpaty  = '<A href="mysimpaty.php" class="online">'.$simpaty.'</A>';
$comments = $a['commentsnum'] ? '<A href="userhistory.php?action=viewcomments&id='.$a['userid'].'" class="online">'.$a['commentsnum'].'</A>' : $a['commentsnum'];
} elseif(get_user_class() >= UC_MODERATOR) {
$bonus = $a["bonus"];
$thanks = $a['thanks'] ? '<A href="mythanks.php?id='.$a['userid'].'" class="online">'.$a['thanks'].'</A>' : $a['thanks'];
$simpaty = '<A href="mysimpaty.php?id='.$a['userid'].'" class="online">'.$simpaty.'</A>';
$comments = $a['commentsnum'] ? '<A href="userhistory.php?action=viewcomments&id='.$a['userid'].'" class="online">'.$a['commentsnum'].'</A>' : $a['commentsnum'];
} else {
$bonus = $a["bonus"];
$thanks = $a['thanks'];
$simpaty = $a['simpaty'];
$comments = $a['commentsnum'];
}

if ($CURUSER)
$username = '<a href="userdetails.php?id='.$a["userid"].'">'.get_user_class_color($a["class"], $a["username"]).'</a>';
else
$username = '<b>'.get_user_class_color($a["class"], $a["username"]).'</b>';

echo "<tr>";
echo "<td class=\"".$styleclass['none']."\" align=\"center\">".$num."</td>";
echo "<td class=\"".$styleclass['none']."\" align=\"left\">".$username."</td>";
echo "<td class=\"".$styleclass['uploaded']."\" align=\"right\">".mksize($a["uploaded"])."</td>";
echo "<td class=\"".$styleclass['upspeed']."\" align=\"right\">".mksize($a["upspeed"])."/s</td>";
echo "<td class=\"".$styleclass['downloaded']."\" align=\"right\">".mksize($a["downloaded"])."</td>";
echo "<td class=\"".$styleclass['downspeed']."\" align=\"right\">".mksize($a["downspeed"])."/s</td>";
echo "<td class=\"".$styleclass['ratio']."\" align=\"right\">".$ratio."</td>";
echo "<td class=\"".$styleclass['bonus']."\" align=\"right\">".$bonus."</td>";
echo "<td class=\"".$styleclass['simpaty']."\" align=\"right\">".$simpaty."</td>";
echo "<td class=\"".$styleclass['comments']."\" align=\"right\">".$comments."</td>";
echo "<td class=\"".$styleclass['none']."\" align=\"left\">".$a["added"]." (".get_elapsed_time(sql_timestamp_to_unix_timestamp($a["added"]))." ".$tracker_lang['ago'].")</td>";
echo "</tr>";

}

if (!$num)
echo '<tr><td colspan="12" align="center">'.$tracker_lang['no_data_now'].'</td></tr>';

echo '<tr><td colspan="12" class="foot"><A href="#top" onclick="blur();"><DIV>&#65085;</DIV></A></td></tr>';

echo '</TABLE><BR />';
}

$pu = (get_user_class() >= UC_POWER_USER);
$lim = isset($_GET["lim"]) ? (int) $_GET["lim"] : false;
$type = isset($_GET["type"]) ? $_GET["type"] : false;

if(!$pu || !($lim == 10 || $lim == 100 || $lim == 250))
$lim = 10;

stdhead("TOP 10", true);

$mainquery = "SELECT users.id as userid, users.username, users.class, users.added, users.uploaded, users.downloaded, users.bonus, users.simpaty, users.uploaded / (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(users.added)) AS upspeed, users.downloaded / (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(users.added)) AS downspeed, COUNT(DISTINCT comments.id) AS commentsnum FROM users LEFT JOIN comments ON comments.user = users.id WHERE enabled = 'yes'";


$exp = explode(",", $tracker_lang['top_info']);

$tops = array();

$tops['bonus'] = array('query' => $mainquery.' AND bonus > 0 GROUP BY users.id ORDER BY bonus DESC, username ASC LIMIT ', 'highlightcol' => 'bonus', 'title_rightpart' => trim($exp[0]));
$tops['simpaty'] = array('query' => $mainquery.' AND simpaty != 0 GROUP BY users.id ORDER BY simpaty DESC, username ASC LIMIT ', 'highlightcol' => 'simpaty', 'title_rightpart' => trim($exp[1]));
$tops['comments'] = array('query' => $mainquery.' GROUP BY users.id ORDER BY commentsnum DESC, username ASC LIMIT ', 'highlightcol' => 'comments', 'title_rightpart' => trim($exp[2]));
$tops['uploaded'] = array('query' => $mainquery.' AND uploaded > 0 GROUP BY users.id ORDER BY uploaded DESC, username ASC LIMIT ', 'highlightcol' => 'uploaded', 'title_rightpart' => trim($exp[3]));
$tops['downloaded'] = array('query' => $mainquery.' AND downloaded > 0 GROUP BY users.id ORDER BY downloaded DESC, username ASC LIMIT ', 'highlightcol' => 'downloaded', 'title_rightpart' => trim($exp[4]));
$tops['upspeed'] = array('query' => $mainquery.' AND uploaded > 0 GROUP BY users.id ORDER BY upspeed DESC, username ASC LIMIT ', 'highlightcol' => 'upspeed', 'title_rightpart' => trim($exp[5]));
$tops['downspeed'] = array('query' => $mainquery.' AND downloaded > 0 GROUP BY users.id ORDER BY downspeed DESC, username ASC LIMIT ', 'highlightcol' => 'downspeed', 'title_rightpart' => trim($exp[6]));
$tops['bestshare'] = array('query' => $mainquery.' AND downloaded > 1073741824 AND uploaded > 0 GROUP BY users.id ORDER BY uploaded / downloaded DESC, uploaded DESC LIMIT ', 'highlightcol' => 'ratio', 'title_rightpart' => trim($exp[7]));
$tops['worstshare'] = array('query' => $mainquery.' AND downloaded > 1073741824 AND uploaded / downloaded < 1 GROUP BY users.id ORDER BY uploaded / downloaded ASC, downloaded DESC LIMIT ', 'highlightcol' => 'ratio', 'title_rightpart' => trim($exp[8]));

echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
<tbody>
<tr>
<td class=\"bottom\">";

foreach ($tops as $toptype => $top) {

if ($toptype != $type)
$limit = 10;
else
$limit = $lim;

$sqlresult = sql_query($top['query'].$limit) or sqlerr(__FILE__, __LINE__);

$tabs['10'] = '<A href="topten.php#'.$toptype.'">Top 10</A>';
if($pu) {
$tabs['100'] = '<A href="topten.php?type='.$toptype.'&lim=100#'.$toptype.'">Top 100</A>';
$tabs['250'] = '<A href="topten.php?type='.$toptype.'&lim=250#'.$toptype.'">Top 250</A>';
}

echo '<A name="'.$toptype.'"></A>';
createtabs($tabs, $limit, '<DIV style="padding-left: 150px; text-align: left;">Top '.$limit.' '.$top['title_rightpart'].'</DIV>');
usertable($sqlresult, $top['highlightcol']);

}

echo "</td></tr></tbody></table>";

stdfoot(true);

?>