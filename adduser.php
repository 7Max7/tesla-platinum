<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();


if (get_user_class() < UC_ADMINISTRATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]); 
stderr($tracker_lang['error'], $tracker_lang['access_denied']); 
die();
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {

$username = htmlspecialchars($_POST["username"]);
$email = htmlspecialchars($_POST["email"]);
$gender = ((isset($_POST["gender"]) && $_POST["gender"] == "2") ? "2":"1");
$password = htmlspecialchars($_POST["password"]);
$password2 = htmlspecialchars($_POST["password2"]);

if (!empty($username) && (!validusername($username) || strlen($username) > 12))
stderr($tracker_lang['error'], $tracker_lang['validusername']);

if (empty($username) || empty($password) || empty($email))
stderr($tracker_lang['error'], $tracker_lang['missing_form_data']);

if (!validemail($email))
stderr($tracker_lang['error'], $tracker_lang['validemail']);

if ($password <> $password2)
stderr($tracker_lang['error'], $tracker_lang['password_mismatch']);

$class = (!empty($_POST["class"]) ? (int) $_POST["class"]:0); // �����

if ($class >= get_user_class())
$class = 0;

$secret = mksecret();
$passhash = md5($secret.$password.$secret);

$modcomment = date("Y-m-d") . " - ��� �������� ����� ������� ".$CURUSER["username"].".";
    
$res3 = sql_query("SELECT id, class, username FROM users WHERE username = ".sqlesc($username)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res3) <> 0){
$arr3 = mysql_fetch_array($res3);

stderr($tracker_lang['error'], sprintf($tracker_lang['user_is_signup'], "<a href=\"userdetails.php?id=".$arr3["id"]."\">".get_user_class_color($arr3["class"], $arr3["username"])."</a>"));
}


sql_query("INSERT INTO users (added, last_access, secret, username, class, passhash, status, email, modcomment, gender) VALUES (".sqlesc(get_date_time()).", ".sqlesc(get_date_time()).", ".sqlesc($secret).", ".sqlesc($username).", ".sqlesc($class).", ".sqlesc($passhash).", 'confirmed', ".sqlesc($email).", ".sqlesc($modcomment).", ".sqlesc($gender).")");
	
if (mysql_errno() == 1062){
stderr($tracker_lang['error'], sprintf($tracker_lang['user_is_signup'], $username)); 
die;
}

$id = mysql_insert_id();

write_log("������������ ".$username." ��� �������� ����� ������� - " . $CURUSER["username"], "000000", "tracker");

header("Location: ".$DEFAULTBASEURL."/userdetails.php?id=".$id);
die;
}

stdhead($tracker_lang['add_user']);

echo "<form method=\"post\" action=\"adduser.php\">";
echo "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['add_user']."</td></tr>";

echo "<tr><td class=\"rowhead\">".$tracker_lang['username']."</td><td><input type=\"text\" name=\"username\" size=\"40\"/> <i>".sprintf($tracker_lang['max_simp_of'], 40)."</i></td></tr>";

echo "<tr><td class=\"rowhead\">".$tracker_lang['signup_password']."</td><td><input type=\"password\" name=\"password\" size=\"40\"/></td></tr>";
echo "<tr><td class=\"rowhead\">".$tracker_lang['signup_password_again']."</td><td><input type=\"password\" name=\"password2\" size=\"40\"/></td></tr>";


if (get_user_class() == UC_SYSOP)
$maxclass = UC_ADMINISTRATOR;
elseif (get_user_class() == UC_MODERATOR)
$maxclass = UC_UPLOADER; // �������� 
elseif (get_user_class() == UC_ADMINISTRATOR)
$maxclass = UC_MODERATOR; // �������� 
else
$maxclass = get_user_class() - 1;

echo "<tr><td class=\"rowhead\">".$tracker_lang['class']."</td><td><select name=\"class\">";

for ($i = 0; $i <= $maxclass; ++$i)
echo ("<option value=\"".$i."\" ".($user["class"] == $i ? "selected" : "").">".get_user_class_name($i)."</option>\n");

echo "</select></td></tr>";

echo "<tr><td class=\"rowhead\">".$tracker_lang['gender']."</td><td><label><input type=\"radio\" checked name=\"gender\" value=\"1\">".$tracker_lang['my_gender_male']." <img src=\"pic/male.gif\"></label><label><input type=\"radio\" name=\"gender\" value=\"2\">".$tracker_lang['my_gender_female']." <img src=\"pic/female.gif\"></label></td></tr>";

echo "<tr><td class=\"rowhead\">".$tracker_lang['email']."</td><td><input type=\"text\" name=\"email\" size=\"40\"/> <i>".sprintf($tracker_lang['max_simp_of'], 80)."</i></td></tr>";

echo "<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" value=\"".$tracker_lang['add_user']."\" class=\"btn\"></td></tr>";

echo "</table>";
echo "</form>";

stdfoot(); 

?>