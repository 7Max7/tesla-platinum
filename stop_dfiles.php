<?
require "include/bittorrent.php";
dbconn();
loggedinorreturn();


/**
 * @author 7Max7
 * @copyright ��������� ��� ������ Tesla TT (Tesla Tracker) v.Platinum IV 2014 ����
 * ������� ������ ���� � ��������� ���������� ������, ����� ��������������� ��������� �� ������ �������, ������ ������ ��������� - ��������� �������.
 * ��������: �������  LOCATE() �� ������������� � �������� ������� � MySQL 4.0.0, �� ���� ������������� � ���� � ����� ������ �������.
**/


if (get_user_class() < UC_SYSOP || get_user_class() <> UC_SYSOP){
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

accessadministration(); 

/*
DROP TABLE IF EXISTS `stop_dfiles`;
CREATE TABLE IF NOT EXISTS `stop_dfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_in` varchar(128) NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `number` int(10) NOT NULL DEFAULT '0' COMMENT '�������',
  `lastime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '��������� ���������������',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dybl` (`key_in`),
  KEY `added` (`added`),
  KEY `lastime` (`lastime`),
  FULLTEXT KEY `full` (`key_in`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;
*/


if ($_SERVER["REQUEST_METHOD"] == "POST") {

$action = (string) $_POST["action"];
$id = (int) $_POST["id"];

$key_in = trim(htmlspecialchars_uni($_POST["key_in"]));

if (empty($_POST["key_in"]) || empty($key_in))
stderr($tracker_lang['error'], $tracker_lang['missing_form_data']."<br /><a href=\"stop_dfiles.php\"><b>".$tracker_lang['back_inlink']."</b></a>");

if (!empty($key_in) && strlen($key_in) < 3)
stderr($tracker_lang['error'], sprintf($tracker_lang['min_simp_of'], 3)."<br /><a href=\"stop_dfiles.php\"><b>".$tracker_lang['back_inlink']."</b></a>");
elseif (!empty($key_in) && strlen($key_in) > 64)
stderr($tracker_lang['error'], sprintf($tracker_lang['max_simp_of'], 64)."<br /><a href=\"stop_dfiles.php\"><b>".$tracker_lang['back_inlink']."</b></a>");

if ($action == "new"){

sql_query("INSERT INTO stop_dfiles (key_in) VALUES (".sqlesc($key_in).")");

$id = mysql_insert_id();
if (!empty($id))
write_log("� ������� '���� �����', ��� �������� ������ - $key_in (# $id), ������������� ".$CURUSER['username'], "36943C", "other");

} elseif ($action == "edit" && is_valid_id($id)){

$udp = array();
$res = sql_query("SELECT key_in FROM stop_dfiles WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc($res);
$oldname = $row['key_in'];

$udp[] = "key_in = ".sqlesc($key_in);

$proc = @similar_text($oldname, $key_in); /// ���������� �����, ���� ����� 50% �������� - �������� ����������
if (!empty($proc) && $proc < 5){
$udp[] = "number = 0";
$udp[] = "lastime = '0000-00-00 00:00:00'";
}

if (count($udp)){
sql_query("UPDATE stop_dfiles SET ".implode(", ", $udp)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (mysql_modified_rows())
write_log("� �������� '���� �����', �������������� ������ � $key_in (# $id), ������������� ".$CURUSER['username'], "8C8C8C", "other");
}

}

unsql_cache("stop_dfiles");

header("Location: stop_dfiles.php");
die;

} elseif ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["action"]) && $_GET["action"] == "delete"){

$id = (int) $_GET["id"];

if (is_valid_id($id)){

$res = sql_query("SELECT key_in FROM stop_dfiles WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc($res);
$oldname = $row['key_in'];

sql_query("DELETE FROM stop_dfiles WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

write_log("�� ������ '���� �����', ��� ������ ������ - $oldname (# $id), ������������� ".$CURUSER['username'], "D96969", "other");
}
unsql_cache("stop_dfiles");

header("Location: stop_dfiles.php");
die;

}

$count = get_row_count("stop_dfiles");

stdhead($tracker_lang['stop_dfiles']);


echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";
echo "<form method=\"post\" action=\"stop_dfiles.php\"><input type=\"hidden\" name=\"action\" value=\"new\">";

echo "<tr><td colspan=\"2\" class=\"colhead\" align=\"center\">".$tracker_lang['stop_dfiles']."</td></tr>";

echo "<tr><td class=\"b\" align=\"center\"><input type=\"text\" required=\"required\" name=\"key_in\" size=\"40\"> <input type=\"submit\" value=\"".$tracker_lang['added_newrule']."\" class=\"btn\" /></td></tr>";

echo "</form>";
echo "</table><br />";



if (!empty($count)){

echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

$postpage = $CURUSER["postsperpage"];
if (empty($postpage)) $postpage = 50;

list ($pagertop, $pagerbottom, $limit) = pager($postpage, $count, "stop_dfiles.php?");

//echo "<tr><td class=\"b\" align=\"center\" colspan=\"4\">������� ������� (�������)</td></tr>";

echo "<tr>
<td class=\"colhead\" align=\"center\">#</td>
<td class=\"colhead\">".$tracker_lang['mem_phrone']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['added']." / ".$tracker_lang['last_actpos']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['action']."</td>
</tr>";


echo "<tr><td colspan=\"4\">".$pagertop."</td></tr>";

$res = sql_query("SELECT * FROM stop_dfiles ORDER BY id DESC ".$limit) or sqlerr(__FILE__, __LINE__);

$num = 0;
while ($row = mysql_fetch_assoc($res)){

if ($num%2 == 0){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

echo "<tr>
<form method=\"post\" action=\"stop_dfiles.php\">

<td ".$cl1." align=\"center\">".$row["id"]."</td>

<td ".$cl2." width=\"70%\"><input required=\"required\" type=\"text\" name=\"key_in\" value=\"".htmlspecialchars($row["key_in"])."\" size=\"35\" /> (<a title=\"".$tracker_lang['persons_torsearch']."\" class=\"alink\" target=\"_blank\" href=\"browse.php?search=".htmlspecialchars($row["key_in"])."\">".$tracker_lang['search']."</a>) (<a ".(!empty($row["number"]) ? "onClick=\"return confirm('".$tracker_lang['warn_sure_action']."')\"":"")." class=\"alink\" title=\"".$tracker_lang['delete']."\" href=\"stop_dfiles.php?id=".$row["id"]."&action=delete\">".$tracker_lang['delete']."</a>)</td>

<td ".$cl2." width=\"15%\" align=\"center\">".($row["added"] >= date("Y-m-d 00:00:00") ? "<b>".$row["added"]."</b>":$row["added"])."<br />".($row["lastime"] >= date("Y-m-d 00:00:00") ? "<b>".$row["lastime"]."</b>":$row["lastime"])."</td>

<td ".$cl1." align=\"center\" width=\"10%\">
".(!empty($row["number"]) ? $tracker_lang['taken'].": ".$row["number"]." ".$tracker_lang['time_s']."<br />":"")."
<input type=\"hidden\" name=\"action\" value=\"edit\">
<input type=\"hidden\" name=\"id\" value=\"".$row["id"]."\">
<input type=\"submit\" value=\"".$tracker_lang['edit']."\" title=\"".$tracker_lang['edit']."\" class=\"btn\">
</td>

</form>
</tr>";

++$num;
}

echo "<tr><td colspan=\"4\">".$pagerbottom."</td></tr>";

echo "</table>";

}

stdfoot(); 

?>