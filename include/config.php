<?
if(!defined('IN_TRACKER') && !defined('IN_ANNOUNCE'))
  die('Hacking attempt!');

/**
** �������� ��������� ������ Tesla (Tesla Tracker) v.Platinum IV 2014 ����
**/

define ("DEBUG_MODE", 1); // ����� ���� (���������� ������������� ���������� ����� ������ ��������)


$SITE_Config = array(
/// ��������� �����
"site_online" => true, /// false - true
/// ��������� ������� �����
"announce_online" => true, /// false - true
/// c����� �������� � ��������� �������, ��� 1, 0 - ����
"scrape_online" => true, /// false - true

/// ������� ���������� �����
"reason_off" => "", /// "������ ����� ���������� �� �������:" - ��� ������� ���������� �����

/// �������� POST ������ �� ����� ����������
"post_ref_check" => false, /// true - false

/// �������� ������������ �������� � download � .utorrent ����� ���������������
"orig_name" => true, /// true - false

/// ������� ��������� ������� ���� �� ��������
"try_repair_table" => true, /// true - false

/// ������� ������� ������ � ������������ ������� ��� �������� (���������� ������)
"maxlogin" => true, /// true - false
/// ���������� ��� ������������� ����� ������ ����� ����� ip
"maxlogin_limit" => 15, /// ����� ������� (���������� ������)


/// �������� ���������� http ����� �� ������� ������ (http-flood)
"anti_httpdoss" => false, /// true - false

/// �������� �������� ����������� ������ � ����� blocks (���������� ������ �������)
"integrity" => false, /// true - false

/// �������� �������������� �������� � ����� -> ���� ������ (��������� ������ �������)
"accessadmin" => false, /// true - false

/// �������� �������� �� ���� ��� ����� � ����������� ��������
"captcha" => true /// true - false

);



$use_lang = true; // �������� �������� �������. ��������� ���� �� ������ ��������� ������� � ������ ����� - ����� ��� ����� �� ������� ������ ������ ������.
$use_blocks = true; // ������������ ������� ������. 1 - ��, 0 - ���. ���� �� ��������� �� �����-������ � �� ������� ������ �� ������ ��������� �������� ��� ������ � �������.

$use_gzip = "yes"; // ������������ ������ GZip �� ���������.

$use_ipbans = 1; // ������������ ������� ������������ IP-�������. 0 - ���, 1 - ��.
$use_sessions = 1; // ������������ ������. 0 - ���, 1 - ��.

$h = date('H'); // ��������� ���
if ($h >= 00 && $h <= 06) // ����������
$announce_interval = 60 * 90; // 90 ����� �� ���������� - ����
else 
$announce_interval = 60 * 45; // 45 ����� �� ���������� - ����


// ������������ �� �����
$maxusers = 6000; // ���� ������ �� ����� �����

$SITEEMAIL = ""; // ����� �����

$SITENAME = "Tesla TT (Tesla Tracker) v.Platinum IV"; //��� ����� ��� �����. ������

$default_language = "russian"; // ���� ������� �� ���������.


// ��������� ��������
$Ava_config = array(

'size' => 96200, /// ������������ ������ �������
'width' => 120, /// ������������ ������
'height' => 120, /// ������������ ������

);
// ��������� ��������



// ������������� ���� ����� ������� �� ���� ������ ��������� � �������. �� ������ ������������ - �� ����� ��� ����.
$ctracker = array(
/// ����������� ������ �� xss ����
"on" => true, /// true - false
/// ������� ��������� ������, ������ ���� ����
"replace" => true /// true - false
);


$points_per_hour = 1; // ������� ��������� ������� � ���, ���� ������������ ��������.
$fixed_bonus = true; // ������������� ���������� ������� ��� �������� ������� ���� 1 ��. (�� cleanup.php)

$autoclean_interval = 600; // �� ��������� ���!
$points_per_cleanup = $points_per_hour*($autoclean_interval/3600); // �� ��������� ���!


$default_theme = "sky blue"; // ���� �� ��������� ��� ������.


$Signup_Config = array(

///��������� �����������
"deny_signup" => false, /// true = ����������� ���������, false = ����������� ��������.

/// ��������� ����������� �� ������������
"allow_invite_signup" => true, /// true = ���������, false = �� ���������.

/// �������� ���������������� �������������
"signup_timeout" => 86400 * 3, /// �����

/// ����� �� ����������� �� ����� ������ ����������� � ������� check_banned_emails
"email_rebans" => false,/// true = ��� �����, false = ���� �����.

/// �������� ������������� ��������� �� �����, ���� ����� ip ������� ������ N ������������ (�� ����).
"dubl_ip_deny" => false,
/// � ��� � ���� N ������������.
"dubl_ip_num" => 3, /// ����� �� 0

/// ������������ ��������� �� �����, ����� - �������������� ��������� ��� �����������.
"use_email_act" => false, /// true - false

);



$Torrent_Config = array(

// �������� �� ������ ����������� ���������
"guest_hidecom" => false, /// true - false

// ������������ TTL.
"use_ttl" => false, /// true - false

// ������� ���� ������� ����� ���� �� TTL.
"ttl_days" => 28, /// �����

// C����� ������ �������� ���� 3 ����� �����
"free_from_3GB" => true, /// true - false

// ��� ������������� (�������� 0 ���� ������ ��������� ������� �������� � ����������� �� ������ �������� �����)
"multihours" => 0, /// ���������� ����� ��� �������������� ������� �����

// ���� a.k.a �����
"auto_tags" => true, /// ��������������� ����(��) �� �������� ������ ���� 100 ���������� 
//// �������� ��� �������, � �������� ���� ������ � ����: ���1, ���2...

// ������� ����� 
"procents" => 10, /// ���� ����� ������ ������ �� ����� �������� � �����

// ������������ �������� ��������� ������� ������ N ������� � ����� 0
"use_dead_torrent" => false, /// true - false
// � ��� � ������ N ����
"dead_torrent_time" => 128, /// N - ���� � ���� ����� ����

// ����������� ��� ��������� ����� � ���
"on_search_log" => false, /// true - false


// max ������ �� ���������� ������� (.torrent)
"max_torrent_size" => 666000, /// ����� (����) ������

// max ������ �� ���������� ������ (�����������, �������)
"max_image_size" => 512000, /// ����� (����) ������


// �� �������
"max_dead_torrent_time" => 6 * 3600 /// �����

);


/// �������� ���������
$Forum_Config = array(

/// ��������� ���������� ������
"on" => true, /// true - ���, false - ����
/// ������� ���������� ������
"off_reason" => "", /// ������ �������

/// ����� �������� ������
"guest" => true, /// true - ���, false - ����

/// ��������� ��������� ��� ����� �������������
"anti_spam" => false, /// true - ���, false - ����
/// ���������� ���� ��� �������� ��� ����������� �������� �� ���� ��� ������� �����
"readpost_expiry" => 14*86400 /// �����

);


/// ���������� ������ ������ ���������� � ���
$error_bit = 0; /// ������ -> 1 ���, ������ -> 2 ������ ���������, ������ -> 0 ��������� ���.

/// �������� ������ ������, ��������� ��������������
$announce_net = 1; /// 0 ���������


/// ������������ ���������� 10% �� ������� ������������� (�� �������)
$use_10proc = true; // false - ���, true - ��.

//// ��������
$refer_parse = 1; // �������� ����� ������� (����� � ����� block-top_refer)
$refer_day = 6; // ������� �������� ����� ������ ������ N ���

$parser_deny = array(
"www.muz-trackers.ru",
"www.muz-tracker.net",
); /// ��� ����� �� ����� ������������ � ������� ���������.


/// �������, ���� �� ��� ���� -> �� ������� ������
$redir_parse = 1; // �������� ������� �������
$redir_day = 6; // ������� �������� ������� ������������� ������ N ���


/// ������������ ������� -> ������������ �������
$�System = array(
/// ��������� ������������� �������: ���� ����� �������.
"aggressive" => true,
/// true - ��� ���������� ��������, ����� ������� ����� � �������.
"monitoring" => false,
/// true - ��������� �� ������������ (���� ����) ��� ������� ����� � �������.
"drop_class" => true,
/// true - �������� ������������������ ��������� ������������ ��� ������� ����� � �������.
"fake_msg_user" => true,

/// true - �������� ������������� ��������� �� ������.
"real_admin_msg" => true,
/// true - �������� ������ �� ����� ��������� � $war_email �� ������.
"sendmsg_on_war_email" => true,

/// true - ��� �� ip ������������ ��� ������� ����� � �������.
"ban_ip_user" => false,
/// true - ��������� ������� ����� ������� ����� ������������ � �������.
"off_user" => false
);


/// �������� ��������� ���� � ����������� memcache
$Cache_Config = array(

/// ��������� memcache
"memcache_on" => false, /// true - false
/// ����� ����������� memcache
"memcache_host" => "localhost",
/// ���� memcache
"memcache_port" => 11211, /// �� ��������� 11211

/// �������������� ���������� ������ ���� �� ���� �������� � ����� ������ ���.
"type" => "", /// ����� - ����� ���� ���� �� ������ �������, ��������: disk - apc - memcache
//// ������, ����� ���� ������� ���� ����������.!

/// ���������� ��� �������� � ����� ������ �������� (�����)
"visible_cache" => true, /// true - false

/// ����������� ������ (������������ ������) ��� �������� ������
"gzip_cache" => false /// true - false !!!! ��������: ���� �������� �� ������ + max .02 � �������
/// ������ ������ ������ ��������� � ��������, ��� ������������ ��������.
);

$Functs_Patch = array(

/// �������������� �������� � ip ���������� (��� ������ ����������� ip ������ - ��������� true)
"ip" => false, /// false �� ���������

/// ����� ��������� ������ �� ������� �������� (�������� ���������� �����) ��� ������� ��� �������
"remote_peers" => 10, /// 10 (� ��������) �� ���������

// ��������: ������� ����� �� ������� ����� ����������� � ������ ����� ���������� download.php (������ ��� '������' ���������!!!)
// ��� ���������� ����� (�/� �� �������) �������� ��������� off (��� �����) � ������� ��������. ������������ � ������� �������������, ����� ������ ������� �� �� ��������. PS ����� ��������� �������� ����� (�������� -> �������).
"remote_off" => false, /// false �� ��������� (��������� ���������)

/// ��������� ������ ���������� ����� ����� download
"deny_refer" => true, /// true �� ���������

/// ������� ������� ��� ���������� download.php
"auto_thanks" => true, /// false �� ���������

/// ��������: ��. ���� ������� �� ��. ��������, �� ������������� � �������� ������ ��� �������.
/// ������� ������� (���������� �/� download) - � ������� ����� 0 ��� ����� ������� false (���� �� timeout).
"kill_cold_announce" => false, /// false �� ���������

/// ������� ������� ���� �� ������� ��������� watermark_dark.png � watermark_light.png
"watermark" => false, /// false ���������

/// ��������� ������������� (���������� ��� ������ ���������)
"multitracker" => true, /// false ���������

/// ������� ���������� (������ �������, ���� � ���)
"take_pic" => 3 /// 1 - ������ �������; 2 - ������ ���������; 3 - ���������, � � ��� ��������

);



$Cleanup_Config = array(

/// ��� ����� ���������� (����� �� ���� �� �������� ��������)
"ban_after_off" => 180, /// ���������� ���� (0 - ��������� �������)

/// �������� ���������� ������������� (�������� ��������)
"user_noaccess_del" => 182, /// ���������� ���� (0 - ��������� �������)

/// �������� �������������� ������������� (�������� ��������)
"users_parked" => 365, /// ���������� ���� (0 - ��������� �������)

/// ���������� ����������� ������������ 
"user_notif_access" => 96, /// ���������� ���� (0 - ��������� �������)



/// ����������� ������������� � ���� ��������
"bday_present" => true, /// true �� ��������� (false - ���������)

/// ������� ������� ����� �����������
"bday_present_cl" => array(
"0" => 20, /// ������������ aka UC_USER
"1" => 30, /// ������� ������������ aka UC_POWER_USER
"2" => 40, /// VIP aka UC_VIP
"3" => 50, /// ���������� aka UC_UPLOADER
"4" => 60, /// ��������� aka UC_MODERATOR
"5" => 70, /// ������������� aka UC_ADMINISTRATOR
"6" => 80 /// ���� aka UC_SYSOP
),


/// ��������� ������� (������) ������������ ��� N ����
"rand_price" => 21, /// ���������� ���� (0 - ��������� �������)

/// ������� ������� ����� � ���� ������� (������� min � max �������� ��� ���������� ���������)
"rand_price_rand" => array(
"min" => 594967296, /// ����������� ����� �������
"max" => 10294967296  /// ������������ ����� ������
),




/// ������� ��� ���������� ��������� ��������� ������ N ���� 
"messages_unread" => 180, /// ���������� ���� (0 - ��������� �������)

/// ������� ��� ���������� ��������� ������ N ����
"messages_read" => 360, /// ���������� ���� (0 - ��������� �������)

/// ������� ������ ������� ������ ������ N ����
"maxlogin_timeout" => 7, /// ���������� ����

/// �������� ��� ����������, ������� �� �������� ������ � ������� N ����, �� � ���������� � ������ �� ����� 14 ����
"auto_duploader" => 62, /// ���������� ���� (0 - ��������� �������)
"auto_duploader_pm" => false, /// ��������� � ��������� ������������ (false - ���������)

/// ��������� ������� - ������� �����
"sessions_delete" => 20, /// ���������� ����� (����������� �� ������ 20)

/// ������� ���� �� 31 ����
"shoutbox_truncate" => 0, /// ���������� ���� (0 - ��������� �������)

/// ������� ��� ������ � ����� cache (��� � N ����)
"cleancache" => 21, /// ���������� ���� (0 - ��������� �������)

/// ����������� � ������ ������ � ���� ������ (��� � N ����)
"sql_optimize" => 0 /// ���������� ���� (0 - ��������� �������)

);




if ($SITE_Config["captcha"] == true)
$Captcha_Config = array(

///��������� ������: 'standart', 'univers', 'big', 'colossal', 'roman', 'nancyj', 'slant', 'rounded', 'pebbles', 'o8', 'dotmatrix'

"font" => '', /// ����� - ����� ���������, ������� ������� ������ �� ������ ����, ������: 'roman'

"strlen" => 4, /// ����� ������. �� ��������� 4 ��� 6 - ����� ������ �������

"allow_chars" => 3 /// 1 - ������ �����, 2 - ������ �����, - 3 - ������������ ��� ��������.

);

$Seo_Config = array(

'browse_name' => '������� �����',

'browse_desc' => '������ ������� ������� �������������� �� ������� ����������. ������� ����� ������� ��������� � ��� �����������.',

'index_name' => '�������� ������, tracker',

'index_desc' => '������� ����� ����� �������',

);

/// ���������� max ������� ������� ��� ��������� ��� ������ �����
$Tfiles_Config = array(

"0" => 0, /// ������������, 0 - ����������
"1" => 0, /// �������, 0 - ����������
"2" => 0, /// vip, 0 - ����������
"3" => 0, /// ��������, 0 - ����������
"4" => 0, /// ���������, 0 - ����������
"5" => 0, /// ���������������, 0 - ����������
"6" => 0, /// ����, 0 - ����������

);

/// ���������� max ���������� ��������� � ���� ��� ������ �����
$Download_Config = array(

"0" => 100, /// �����, 0 - ����������
"1" => 0, /// ������������, 0 - ����������
"2" => 0, /// �������, 0 - ����������
"3" => 0, /// vip, 0 - ����������
"4" => 0, /// ��������, 0 - ����������
"5" => 0, /// ���������, 0 - ����������
"6" => 0, /// ���������������, 0 - ����������
"7" => 0, /// ����, 0 - ����������

);


$ICE = 0;  /// �������� ���� ����� ���� (���� �����)

?>