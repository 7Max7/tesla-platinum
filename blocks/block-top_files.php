<?
if (!defined('BLOCK_FILE')) {
Header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright 2012
 */
 
global $tracker_lang;
$blocktitle = $tracker_lang['week_top'];

?>
<style type="text/css">
<!--
#tabstwho {text-align: left;}
#tabstwho{padding-top: 7px;} 
#tabstwho span{position: relative;border-bottom: 1px solid #FAFAFA !important;top: -1px;-webkit-border-top-left-radius: 4px;-webkit-border-top-right-radius: 4px;-moz-border-radius-topleft: 4px;-moz-border-radius-topright: 4px;border-top-left-radius: 4px;border-top-right-radius: 4px;}
#tabstwho span:hover{background: #FAFAFA;}
.activey{color: #C60000;}
#tabstwho .tabstwho {border: 1px solid #cecece;padding: 5px 10px 5px 10px;
/* background:#ededed;*/
margin-right:5px;line-height: 23px;cursor: pointer;font-weight: bold;}
#tabstwho.activey {border-bottom: none;padding-bottom: 5px;background: #FAFAFA;cursor: default;}
#tabstwho #bodyy {border: 1px solid #cecece;padding: 5px;margin-bottom: 10px;background: #FAFAFA;}
#tabstwho .tabstwho_error {background:url(../pic/error.gif) repeat-y;height: 34px;line-height: 34px;padding-left: 40px;}
table.tt { width: 100%;}
table.tt td {padding: 5px;}
table.tt td.tt {background-color: #777;padding: 7px;}
.example {float:left; margin:20px; border-bottom:#ccc 1px solid; cursor:pointer}
.pics {height: 232px;width: 232px;padding: 0;margin: 0;}
.pics img {padding: 15px;  border:  1px solid #ccc;  background-color: #eee;  width:  200px; height: 200px; top:  0; left: 0}
-->
</style>

<script type="text/javascript">
var loading = "<img src=\"pic/loading.gif\" alt=\"Загрузка..\" />";
jQuery(function() {
    jQuery(".tabstwho").click ( function(){
        if(jQuery(this).hasClass("activey"))
            return;
        else
        {
            jQuery("#loading").html(loading);
            var user = jQuery("#bodyy").attr("user");
            var act = jQuery(this).attr("id");
            jQuery(this).toggleClass("activey");
            jQuery(this).siblings("span").removeClass("activey");
            jQuery.post("block_top_jquery.php",{"user":user,"act":act},function (response) {
                jQuery("#bodyy").empty();
                jQuery("#bodyy").append(response);
                jQuery("#loading").empty();
            });
        }
    });
    jQuery('.zebra:even').css({backgroundColor: '#EEEEEE'});
    if(jQuery.browser.msie)
    {
        width = jQuery('#profile_right h2').width();
        if (width > 422)
            jQuery('#profile_right').width(width);
        else
        {
            jQuery('#profile_right').width("422");
            jQuery('#profile_container').width("686");
        }
    }
});
</script>
<?

$content.= "<div id=\"tabstwho\">\n";

$content.= "<span title=\"".$tracker_lang['top_view']."\" class=\"tabstwho activey\" id=\"view\">".$tracker_lang['top_view']."</span>\n";
$content.= "<span title=\"".$tracker_lang['top_intop']."\" class=\"tabstwho\" id=\"intop\">".$tracker_lang['top_intop']."</span>\n";
$content.= "<span title=\"".$tracker_lang['top_outtop']."\" class=\"tabstwho\" id=\"outtop\">".$tracker_lang['top_outtop']."</span>\n";
$content.= "<span title=\"".$tracker_lang['top_snat']."\"  class=\"tabstwho\" id=\"snat\">".$tracker_lang['top_snat']."</span>\n";
$content.= "<span title=\"".$tracker_lang['top_mark']."\"  class=\"tabstwho\" id=\"mark\">".$tracker_lang['top_mark']."</span>\n";

$content.= "<span id=\"loading\"></span>\n";
$content.= "<div id=\"bodyy\" user=\"by 7Max7 for Pro Tesla TT (III) (2012)\">\n";

$res = sql_query("SELECT times_completed, f_seeders, f_leechers, name, seeders, webseed, leechers, image1, id, hits, views
FROM torrents
WHERE added > DATE_SUB(NOW(), INTERVAL 7 DAY) ORDER BY views DESC LIMIT 0,8", $cache = array("type" => "disk", "file" => "block-top_files", "time" => 60*60*12)) or sqlerr(__FILE__, __LINE__);

$num=1;
$numi=0;

$content.= "<table border=\"1\" cellspacing=\"0\" style=\"border-collapse: collapse\" width=\"100%\" class=\"main\" id=\"table\">";
$nc=1;
    
for ($i = 0; $i < $num; ++$i) {
	
while ($row = mysql_fetch_assoc_($res)){

$num=1;

if ($nc == 1) { $content.="<tr>"; }
$content.="<td>";

$image1 = htmlentities($row["image1"]);
if(empty($image1))
$image1="default_torrent.png";  


$content.="<td align=\"center\" valign=\"top\" width=\"25%\">"; 

if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image1))
$content.="<a href=details.php?id=".$row["id"]."><img class=\"effect2\" onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect2'\" src=\"".$image1."\" width=\"140\"/><br />".format_comment($row["name"])."</a>";
else
$content.="<a href=details.php?id=".$row["id"]."><img class=\"effect2\" onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect2'\" src=\"thumbnail.php?image=".$image1."&amp;for=block\" width=\"140\"/><br />".format_comment($row["name"])."</a>";


if (!empty($row["webseed"]))
++$row['seeders'];

$content.= '<div align="center" style=\'font-weight:bold;\'><font color="blue">'.$tracker_lang['views'].': '. round($row['views']) .'</font><br />
'.sprintf($tracker_lang['new_torrents_stats'], '<font color="red">'. ($row['seeders']+$row['f_seeders']).'</font>', '<font color="green">'. ($row['leechers']+$row['f_leechers']).'</font>').'</div>';
       
$content.= "</td>";
++$nc;

if ($nc == 5) { $nc=1; $content.="</tr>"; }
++$numi;
	  //  if ($nc == 9) { $nc=1; echo"</tr>"; }
    }
}
$content.="</tr></table>";



if ($numi==0)
$content.="<center>".$tracker_lang['no_data']."</center>\n";

/////////
$content.= ("</div>\n");
$content.= ("</div>\n");

?>