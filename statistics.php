<?php
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

if (get_user_class() < UC_ADMINISTRATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]); 
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

stdhead($tracker_lang['statistics'], true);
$base_url = $DEFAULTBASEURL."/statistics.php";

$array_stat = array(
'reg' => $tracker_lang['stat_reg_i'],
'rate' => $tracker_lang['stat_rate_i'],
'offers' => $tracker_lang['stat_offers_i'],
'msg' => $tracker_lang['stat_msg_i'],
'log' => $tracker_lang['stat_log_i'],
'bans' => $tracker_lang['stat_bans_i'],
'bansmail' => $tracker_lang['stat_bansmail_i'],
'comm' => $tracker_lang['stat_comm_i'],
'new' => $tracker_lang['stat_new_i'],
'poll' => $tracker_lang['stat_poll_i'],
'humor' => $tracker_lang['stat_humor_i'],
'login' => $tracker_lang['stat_login_i'],
'simpaty' => $tracker_lang['stat_simpaty_i'],
'refer' => $tracker_lang['stat_refer_i'],
'away' => $tracker_lang['stat_away_i'],
'tags' => $tracker_lang['stat_tags_i'],
'cloud' => $tracker_lang['stat_cloud_i'],
'useragent' => $tracker_lang['stat_useragent_i'],
'tfiles' => $tracker_lang['stat_tfiles_i'],
'snatched' => $tracker_lang['stat_snatched_i'],
'topics' => $tracker_lang['stat_topics_i'],
'post' => $tracker_lang['stat_post_i'],
'editpost' => $tracker_lang['stat_editpost_i'],
'multi_time' => $tracker_lang['stat_multi_time_i'],
'checkpeers' => $tracker_lang['stat_checkpeers_i'],
'last_action' => $tracker_lang['stat_last_action_i'],
'last_reseed' => $tracker_lang['stat_last_reseed_i'],
'stop_time' => $tracker_lang['stat_stop_time_i'],
'torr' => $tracker_lang['stat_torr_i'],
'unmatch' => $tracker_lang['ustat_unmatch'],
);


echo "<table width=\"100%\" align=\"center\">";
echo "<tr><td class='b' valign=\"top\">";

echo "<div id=\"tabs\">\n";

foreach ($array_stat AS $lnk => $lang){

echo "<span onClick=\"document.location.href='statistics.php?act=stats&code=".$lnk."'\" title=\"".$lang."\" class=\"tab ".(($_GET["code"] == $lnk || $_POST["code"] == "show_".$lnk) ? "active":"")."\"><nobr>".$lang."</nobr></span>\n";
}

echo "</div>";
echo "</td></tr>";

if (!isset($_GET["act"]) && !$_POST)
echo "<tr><td class='a' valign=\"top\" align=\"center\">".$tracker_lang['click_on_choose']."</td></tr>";

echo "</table>";

function start_form($hiddens = false, $name='theAdminForm', $js = false) {

global $base_url;

$form = "<form action='{$base_url}' method='post' name='$name' $js>";

if (is_array($hiddens)) {
foreach ($hiddens as $k => $v) {
$form.= "\n<input type='hidden' name='{$v[0]}' value='{$v[1]}'>";
}
}

return $form;
}


function form_dropdown($name, $list = array(), $default_val = false, $js = false, $css = false) {

if (!empty($js))
$js = ' '.$js.' ';

if (!empty($css))
$css = ' class="'.$css.'" ';

$html = "<select name='$name'".$js." $css class='dropdown'>\n";

foreach ($list as $k => $v) {

$selected = "";

if (!empty($default_val) && $v[0] == $default_val)
$selected = ' selected';

$html .= "<option value='".$v[0]."'".$selected.">".$v[1]."</option>\n";
}

$html .= "</select>\n\n";

return $html;
}

function end_form($text = false, $js = false, $extra = false) {

$html = "";
$colspan = "";
$td_colspan = 0;

if (!empty($text)) {
if ($td_colspan > 0)
$colspan = " colspan='".$td_colspan."' ";
$html .= "<tr><td align='center' class='b'".$colspan."><input type='submit' class='btn' value='$text'".$js." id='button' accesskey='s'>{$extra}</td></tr>\n";
}

$html .= "</form>";

return $html;
}

$month_names = array();

$tmp_in = array_merge($_GET, $_POST);

foreach ($tmp_in as $k => $v) {
unset($$k);
}

$month_names = array(1 => $tracker_lang['my_months_january'], $tracker_lang['my_months_february'], $tracker_lang['my_months_march'], $tracker_lang['my_months_april'], $tracker_lang['my_months_may'], $tracker_lang['my_months_jule'], $tracker_lang['my_months_july'], $tracker_lang['my_months_august'], $tracker_lang['my_months_september'], $tracker_lang['my_months_october'], $tracker_lang['my_months_november'], $tracker_lang['my_months_december']);

if (isset($tmp_in['code']) && !empty($tmp_in['code'])) {

switch($tmp_in['code']) {

case 'show_reg':result_screen('reg');break;
case 'show_rate':result_screen('rate');break;
case 'rate':main_screen('rate');break;
case 'show_post':result_screen('post');break;
case 'post':main_screen('post');break;
case 'show_editpost':result_screen('editpost');break;
case 'editpost':main_screen('editpost');break;
case 'show_away':result_screen('away');break;
case 'away':main_screen('away');break;
case 'show_topics':result_screen('topics');break;
case 'topics':main_screen('topics');break;
case 'show_post':result_screen('post');break;
case 'post':main_screen('post');break;
case 'show_bansmail':result_screen('bansmail');break;
case 'bansmail':main_screen('bansmail');break;
case 'show_snatched':result_screen('snatched');break;
case 'snatched':main_screen('snatched');break;
case 'show_refer':result_screen('refer');break;
case 'refer':main_screen('refer');break;
case 'show_useragent':result_screen('useragent');break;
case 'useragent':main_screen('useragent');break;
case 'show_cloud':result_screen('cloud');break;
case 'cloud':main_screen('cloud');break;
case 'show_tags':result_screen('tags');break;
case 'tags':main_screen('tags');break;
case 'show_log':result_screen('log');break;
case 'log':main_screen('log');break;
case 'show_msg':result_screen('msg');break;
case 'msg':main_screen('msg');break;
case 'show_torr':result_screen('torr');break;
case 'torr':main_screen('torr');break;
case 'show_unmatch':result_screen('unmatch');break;
case 'unmatch':main_screen('unmatch');break;
case 'show_bans':result_screen('bans');break;
case 'bans':main_screen('bans');break;
case 'show_comm':result_screen('comm');break;
case 'comm':main_screen('comm');break;
case 'show_new':result_screen('new');break;
case 'new':main_screen('new');break;
case 'show_poll':result_screen('poll');break;
case 'poll':main_screen('poll');break;
case 'show_tfiles':result_screen('tfiles');break;
case 'tfiles':main_screen('tfiles');break;
case 'show_humor':result_screen('humor');break;
case 'humor':main_screen('humor');break;
case 'show_login':result_screen('login');break;
case 'login':main_screen('login');break;
case 'show_simpaty':result_screen('simpaty');break;
case 'simpaty':main_screen('simpaty');break;
case 'show_offers':result_screen('offers');break;
case 'offers': main_screen('offers'); break;
case 'show_multi_time': result_screen('multi_time'); break;
case 'multi_time': main_screen('multi_time'); break;
case 'show_checkpeers': result_screen('checkpeers'); break;
case 'checkpeers': main_screen('checkpeers'); break;
case 'show_last_action': result_screen('last_action'); break;
case 'last_action': main_screen('last_action'); break;
case 'show_last_reseed': result_screen('last_reseed');break;
case 'last_reseed':main_screen('last_reseed');break;
case 'show_stop_time':result_screen('stop_time');break;
case 'stop_time':main_screen('stop_time');break;

default:main_screen('reg');break;
}
}



function result_screen($mode='reg') {

global $month_names, $CURUSER, $tracker_lang;

$page_title = "<h2>".$tracker_lang['result_stats']."</h2>";

if (!checkdate($_POST['to_month'],$_POST['to_day'],$_POST['to_year']))
die($tracker_lang['date_from'].": ".$tracker_lang['invalid_id_value']);

if (!checkdate($_POST['from_month'], $_POST['from_day'], $_POST['from_year']))
die($tracker_lang['date_to'].": ".$tracker_lang['invalid_id_value']);

$to_time = mktime(12, 0, 0, $_POST['to_month'], $_POST['to_day'], $_POST['to_year']);
$from_time = mktime(12, 0, 0, $_POST['from_month'], $_POST['from_day'], $_POST['from_year']);
$human_to_date = getdate($to_time);
$human_from_date = getdate($from_time);

if ($mode == 'reg') {
$sql_table = 'users';
$sql_field = 'added';
} else if ($mode == 'rate'){
$sql_table = 'ratings';
$sql_field = 'added';
} else if ($mode == 'post'){
$sql_table = 'posts';
$sql_field = 'added';
} else if ($mode == 'editpost'){
$sql_table = 'posts';
$sql_field = 'editedat';
} else if ($mode == 'away'){
$sql_table = 'reaway';
$sql_field = 'date';
} else if ($mode == 'msg'){
$sql_table = 'messages';
$sql_field = 'added';
} else if ($mode == 'bansmail'){
$sql_table = 'bannedemails';
$sql_field = 'added';
} else if ($mode == 'snatched'){
$sql_table = 'snatched';
$sql_field = 'startdat';
} else if ($mode == 'useragent'){
$sql_table = 'useragent';
$sql_field = 'added';
} else if ($mode == 'topics'){
$sql_table = 'topics';
$sql_field = 'lastdate';
} else if ($mode == 'cloud'){
$sql_table = 'searchcloud';
$sql_field = 'added';
} else if ($mode == 'refer'){
$sql_table = 'referrers';
$sql_field = 'date';
} else if ($mode == 'log'){
$sql_table = 'sitelog';
$sql_field = 'added';
} else if ($mode == 'torr'){
$sql_table = 'torrents';
$sql_field = 'added';
} else if ($mode == 'unmatch'){
$sql_table = 'my_search';
$sql_field = 'time';
} else if ($mode == 'multi_time'){
$sql_table = 'torrents';
$sql_field = 'multi_time';
} else if ($mode == 'checkpeers'){
$sql_table = 'torrents';
$sql_field = 'checkpeers';
} else if ($mode == 'last_action'){
$sql_table = 'torrents';
$sql_field = 'last_action';
} else if ($mode == 'last_reseed'){
$sql_table = 'torrents';
$sql_field = 'last_reseed';
} else if ($mode == 'stop_time'){
$sql_table = 'torrents';
$sql_field = 'stop_time';
} else if ($mode == 'tags'){
$sql_table = 'tags';
$sql_field = 'added';
} else if ($mode == 'bans'){
$sql_table = 'bans';
$sql_field = 'added';
} else if ($mode == 'comm'){
$sql_table = 'comments';
$sql_field = 'added';
} else if ($mode == 'new'){
$sql_table = 'news';
$sql_field = 'added';
} else if ($mode == 'poll'){
$sql_table = 'polls';
$sql_field = 'added';
} else if ($mode == 'simpaty'){
$sql_table = 'simpaty';
$sql_field = 'respect_time';
} else if ($mode == 'offers'){
$sql_table = 'off_reqs';
$sql_field = 'added';
} else if ($mode == 'tfiles'){
$sql_table = 'attachments';
$sql_field = 'added';
} else if ($mode == 'humor'){
$sql_table = 'humor';
$sql_field = 'date';
} else if ($mode == 'login'){
$sql_table = 'loginattempts';
$sql_field = 'added';
}

switch ($_POST['timescale']) {
case 'daily':
$sql_date = "%w %U %m %Y";
$php_date = "F jS - Y";
break;

case 'monthly':
$sql_date = "%m %Y";
$php_date = "F Y";
break;

default:
$sql_date = "%U %Y";
$php_date = " [F Y]";
break;
}

$sortby = isset($_POST['sortby']) ? mysql_real_escape_string($_POST['sortby']) : "";

$sqlq = "SELECT UNIX_TIMESTAMP(MAX({$sql_field})) as result_maxdate, COUNT(*) as result_count,
DATE_FORMAT({$sql_field},'{$sql_date}') AS result_time
FROM {$sql_table}
WHERE UNIX_TIMESTAMP({$sql_field}) > '{$from_time}' AND UNIX_TIMESTAMP({$sql_field}) < '{$to_time}'
GROUP BY result_time ORDER BY {$sql_field} {$sortby}";

$res = sql_query($sqlq) or sqlerr(__FILE__,__LINE__);

$running_total = 0;
$max_result = 0;
$results = array();

if ($_POST['timescale'] == "daily")
$view = $tracker_lang['daily'];
if ($_POST['timescale'] == "weekly")
$view = $tracker_lang['weekly'];
if ($_POST['timescale'] == "monthly")
$view = $tracker_lang['monthly'];

$html = $page_title."<br />
<table width=\"100%\" align=\"center\" id=torrenttable border=1>
<tr><td colspan=3 class='b' align=\"center\">".$view." ({$human_from_date['mday']} {$month_names[$human_from_date['mon']]} {$human_from_date['year']} - {$human_to_date['mday']} {$month_names[$human_to_date['mon']]} {$human_to_date['year']})</td></tr>\n";

if (mysql_num_rows($res)) {

while ($row = mysql_fetch_assoc($res)) {

if ($row['result_count'] > $max_result)
$max_result = $row['result_count'];

$running_total += $row['result_count'];
$results[] = array('result_maxdate' => $row['result_maxdate'],'result_count'=> $row['result_count'],'result_time'=> $row['result_time']);

}
$srednee = number_format ($running_total / mysql_num_rows($res),0);

foreach($results as $pOOp => $data) {

$img_width = intval(($data['result_count'] / $max_result) * 100 - 20);

if ($img_width < 1)
$img_width = 1;

$img_width .= '%';

if ($_POST['timescale'] == 'weekly') 
$date = $data['week_all']." #".strftime("%W", $data['result_maxdate'])."<br />" . date($php_date, $data['result_maxdate']);
else 
$date = get_date_time($data['result_maxdate']); 

$html .= "<tr>
<td class=b width=30%>" .$date . "</td>
<td class=a width=70%><img src=\"./themes/".$CURUSER["stylesheet"]."/images/bar_left.gif\" border='0' height=\"12\" width='4' align='middle' alt=''><img src=\"./themes/".$CURUSER["stylesheet"]."/images/bar.gif\" border='0' height=\"12\" width='$img_width' align='middle' alt=''><img src=\"./themes/".$CURUSER["stylesheet"]."/images/bar_right.gif\" border='0' height=\"12\" width='4' align='middle' alt=''>
</td>
<td align=right class=b width=5%>".$data['result_count']."</td>
</tr>\n";
}
$html .= '<tr><td colspan=3 class=b>&nbsp;'. "<div align='center'>".$tracker_lang['all'].": <b>".$running_total."</b> ".$tracker_lang['average'].": <b>".$srednee."</b></div></td></tr>\n";
} else
$html .= "<tr><td class=\"b\" align=\"center\"><h3>".$tracker_lang['no_data']."</h3></td></tr>\n" ;

print $html."</table>\n<br />";
}

function main_screen($mode='reg') {

global $month_names, $tracker_lang;

$form_code = 'show_'.$mode;

$old_date = getdate(time() - (3600 * 24 * 90));
$new_date = getdate(time() + (3600 * 24));

$html =  "<table width=\"100%\" align=\"center\" id=torrenttable border=1>";
$html .= start_form(array(1 => array('code' ,  $form_code ),2 => array('act',  'stats' ),));
$html .= "<tr><td class=\"a\"><b>".$tracker_lang['date_from']."</b> " .form_dropdown("from_month",  make_month(), $old_date['mon'] ).'&nbsp;&nbsp;'.form_dropdown("from_day",  make_day() ,  $old_date['mday']).'&nbsp;&nbsp;'.form_dropdown("from_year" ,  make_year(),  $old_date['year'])."<br /></td></tr>";
$html .= "<tr><td class=\"a\"><b>".$tracker_lang['date_to']."</b> " .form_dropdown("to_month",  make_month(), $new_date['mon'] ).'&nbsp;&nbsp;'.form_dropdown("to_day",  make_day() ,  $new_date['mday']).'&nbsp;&nbsp;'.form_dropdown("to_year" ,  make_year(),  $new_date['year']) ."<br /></td></tr>";

if ($mode != 'views')
$html .= "<tr><td class=\"a\"><b>".$tracker_lang['timescale']."</b> " .form_dropdown("timescale",  array(0 => array('daily', $tracker_lang['daily']), 1 => array('weekly', $tracker_lang['weekly']), 2 => array('monthly', $tracker_lang['monthly']))) ."<br /></td></tr>";

$html .= "<tr><td class=\"a\"><b>".$tracker_lang['sorting']."</b> " .form_dropdown("sortby",  array(0 => array('asc', $tracker_lang['sort_asc']), 1 => array('desc', $tracker_lang['sort_desc'])), 'desc') ."<br /></td></tr>";
$html .= end_form($tracker_lang['stat_view'])."</table>";

print $html;
}

function make_year() {

$time_now = getdate();
$return = array();
$start_year = 2006; 
$latest_year = intval($time_now['year']);
if ($latest_year == $start_year) $start_year -= 1;

for ($y = $start_year; $y <= $latest_year; $y++) {
$return[] = array($y, $y);
}

return $return;
}

//-----------------------------------------

function make_month() {

global $month_names;
reset($month_names);
$return = array();

for ($m = 1 ; $m <= 12; $m++){
$return[] = array($m, $month_names[$m]);
}

return $return;
}


function make_day() {
$return = array();

for ($d = 1 ; $d <= 31; $d++){
$return[] = array($d, $d);
}

return $return;
}

stdfoot(true);
?>