<?
require "include/bittorrent.php";
dbconn();
loggedinorreturn();

if (get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

stdhead($tracker_lang['statistic']);

$res = sql_query("SELECT COUNT(*) FROM torrents") or sqlerr(__FILE__, __LINE__);
$n = mysql_fetch_row($res);
$n_tor = $n[0];

$res = sql_query("SELECT COUNT(*) FROM peers") or sqlerr(__FILE__, __LINE__);
$n = mysql_fetch_row($res);
$n_peers = $n[0];

$uporder = urlencode(isset($_GET['uporder']) ? $_GET['uporder']:0);
$catorder = urlencode(isset($_GET["catorder"]) ? $_GET["catorder"]:0);

if ($uporder == "lastul")
$orderby = "last DESC, name";
elseif ($uporder == "torrents")
$orderby = "n_t DESC, name";
elseif ($uporder == "peers")
$orderby = "n_p DESC, name";
else
$orderby = "name";

$res = sql_query("SELECT u.class, u.username AS name, MAX(t.added) AS last, COUNT(DISTINCT t.id) AS n_t, COUNT(p.id) as n_p
FROM users as u
LEFT JOIN torrents as t ON u.id = t.owner
LEFT JOIN peers as p ON t.id = p.torrent WHERE u.class = 3
GROUP BY u.id UNION SELECT u.id, u.username AS name, MAX(t.added) AS last, COUNT(DISTINCT t.id) AS n_t, COUNT(p.id) as n_p
FROM users as u
LEFT JOIN torrents as t ON u.id = t.owner
LEFT JOIN peers as p ON t.id = p.torrent WHERE u.class > 3
GROUP BY u.id ORDER BY ".$orderby) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stdmsg($tracker_lang['error'], $tracker_lang['no_data_now']);
else {

echo ("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">

<tr><td class=\"a\" colspan=\"6\">".$tracker_lang['statistic'].": ".$tracker_lang['bl_uploader']."</td></tr>

<tr>\n
<td class=\"colhead\"><a href=\"stats.php?uporder=uploader&catorder=".$catorder."\" class=\"altlink_white\">".$tracker_lang['username']."</a></td>\n
<td class=\"colhead\"><a href=\"stats.php?uporder=lastul&catorder=".$catorder."\" class=\"altlink_white\">".$tracker_lang['clock']."</a></td>\n
<td class=\"colhead\" align=\"center\"><a href=\"stats.php?uporder=torrents&catorder=".$catorder."\" class=\"altlink_white\">".$tracker_lang['torrents']."</a></td>\n
<td class=\"colhead\" align=\"center\">".$tracker_lang['finish_peer']."</td>\n
<td class=\"colhead\" align=\"center\"><a href=\"stats.php?uporder=peers&catorder=".$catorder."\" class=\"altlink_white\">".$tracker_lang['seeding_now']."</a></td>\n
<td class=\"colhead\" align=\"center\">".$tracker_lang['finish_peer']."</td>\n
</tr>\n");

while ($uper = mysql_fetch_array($res)) {

$cl1 = 'class = "a"'; $cl2 = 'class = "b"';
if ($na%2 == 0){
$cl1 = 'class = "b"'; $cl2 = 'class = "a"';
}

echo ("<tr><td ".$cl2.">".get_user_class_color($uper['class'],$uper['name'])."</td>\n");
echo ("<td ".$cl1." ".(!empty($uper['last'])? ("><a title=\"".$tracker_lang['search']."\" href=\"browse.php?date=".(current(explode(" ", $uper['last'])))."\">".($uper['last'])."</a>
<small>(".get_elapsed_time(sql_timestamp_to_unix_timestamp($uper['last']))." ".$tracker_lang['ago'].")</small>") : "align=\"left\"><i>".$tracker_lang['no_data']."</i>")."</td>\n");
echo ("<td ".$cl2." align=\"center\">".$uper['n_t']."</td>\n");
echo ("<td ".$cl1." align=\"center\">".($n_tor > 0? number_format(100 * $uper['n_t']/$n_tor,1)."%":"<i>".$tracker_lang['no_data']."</i>")."</td>\n");
echo ("<td ".$cl2." align=\"center\">".$uper['n_p']."</td>\n");
echo ("<td ".$cl1." align=\"center\">".($n_peers > 0? number_format(100 * $uper['n_p']/$n_peers,1)."%":"<i>".$tracker_lang['no_data']."</i>")."</td></tr>\n");
++$na;
}
echo ('</table><br />');


}

if ($n_tor == 0)
stdmsg($tracker_lang['error'], $tracker_lang['no_data_now']);
else {
if ($catorder == "lastul")
$orderby = "last DESC, c.name";
elseif ($catorder == "torrents")
$orderby = "n_t DESC, c.name";
elseif ($catorder == "peers")
$orderby = "n_p DESC,c.name";
else
$orderby = "c.name";

$res = sql_query("SELECT c.id as catid, c.name as catname, MAX(t.added) AS last, COUNT(DISTINCT t.id) AS n_t, COUNT(p.id) AS n_p, (t.f_leechers+t.f_seeders) AS f_peers
FROM categories as c
LEFT JOIN torrents as t ON t.category = c.id
LEFT JOIN peers as p
ON t.id = p.torrent GROUP BY c.id ORDER BY ".$orderby) or sqlerr(__FILE__, __LINE__);

echo ("<table width=\"100%\" border=\"0\">

<tr><td class=\"a\" colspan=\"6\">".$tracker_lang['statistic'].": ".$tracker_lang['category']."</td></tr>

<tr><td class=\"colhead\"><a href=\"stats.php?uporder=".$uporder."&catorder=category\" class=\"altlink_white\">".$tracker_lang['category']."</a></td>
<td class=\"colhead\"><a href=\"stats.php?uporder=".$uporder."&catorder=lastul\" class=\"altlink_white\">".$tracker_lang['clock']."</a></td>
<td class=\"colhead\" align=\"center\"><a href=\"stats.php?uporder=".$uporder."&catorder=torrents\" class=\"altlink_white\">".$tracker_lang['torrents']."</a></td>
<td class=\"colhead\">".$tracker_lang['finish_peer']."</td>
<td class=\"colhead\"><a href=\"stats.php?uporder=".$uporder."&catorder=peers\" class=\"altlink_white\">".$tracker_lang['seeding_now']."</a></td>
<td class=\"colhead\">".$tracker_lang['finish_peer']."</td></tr>\n");

while ($cat = mysql_fetch_array($res)) {

$cl1 = 'class = "a"'; $cl2 = 'class = "b"';
if ($na%2 == 0){
$cl1 = 'class = "b"'; $cl2 = 'class = "a"';
}

echo ("<tr><td ".$cl1.">".$cat['catname']. "</b></a></td>");
echo ("<td ".$cl2." ".(!empty($cat['last'])? ("><a title=\"".$tracker_lang['search']."\" href=\"browse.php?date=".(current(explode(" ", $cat['last'])))."\">".($cat['last'])."</a> <small>(".get_elapsed_time(sql_timestamp_to_unix_timestamp($cat['last']))." ".$tracker_lang['ago'].")</small>
"): "align=\"left\">".$tracker_lang['no_data'])."</td>");
echo ("<td ".$cl1." align=\"center\"><a href=\"browse.php?cat=".$cat['catid']."\">".$cat['n_t']."</td>");
echo ("<td ".$cl2." align=\"center\">".number_format(100 * $cat['n_t']/$n_tor, 1)."%</td>");
echo ("<td ".$cl1." align=\"center\">".number_format($cat['n_p']+$cat['f_peers'])."</td>");
echo ("<td ".$cl2." align=\"center\">".($n_peers > 0 ? number_format(100 * $cat['n_p']/$n_peers,1)."%" : "---")."</td>\n");
++$na;
}
echo  ('</table><br />');

}

$res2 = sql_query("SELECT sum(s.uploaded) AS uploaded, s.startdat, s.completedat, s.torrent, s.userid, u.class, u.username, t.name,t.size
FROM snatched AS s
INNER JOIN users AS u ON u.id = s.userid AND u.class < 4
INNER JOIN torrents AS t ON t.id = s.torrent
WHERE t.size > '4684115968' AND (s.uploaded / t.size) > 3
GROUP BY s.torrent ORDER BY uploaded DESC LIMIT 250") or sqlerr(__FILE__, __LINE__);

echo ("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">

<tr><td class=\"a\" colspan=\"6\">".$tracker_lang['statistic'].": ".$tracker_lang['class_uploader']." (".$tracker_lang['size']." > ".$tracker_lang['class_uploader']."*3)</td></tr>

<tr>\n
<td class=\"colhead\">".$tracker_lang['class_uploader']."</td>\n
<td class=\"colhead\">".$tracker_lang['clock']." ".$tracker_lang['begin']." / ".$tracker_lang['completed']."</td>\n
<td class=\"colhead\" align=\"center\">".$tracker_lang['torrent']."</td>\n
<td class=\"colhead\" align=\"center\">".$tracker_lang['size']." (%)/".$tracker_lang['torrent']."</td>\n
</tr>\n");

$aray = array();
while ($uper2 = mysql_fetch_array($res2)) {

$cl1 = 'class = "a"'; $cl2 = 'class = "b"';
if ($na%2 == 0){
$cl1 = 'class = "b"'; $cl2 = 'class = "a"';
}

$aray[] = $uper2['userid'];
$coun = count(array_keys($aray, $uper2['userid']));

echo ("<tr>");
echo ("<td ".$cl1." align=\"center\"><a href=\"userdetails.php?id=".$uper2['userid']."\">".get_user_class_color($uper2['class'], $uper2['username'])."</a> ".($coun > 1 ? " (<a title=\"".$tracker_lang['number_all']."\">".$coun."</a>)":"")."</td>\n");
echo ("<td ".$cl1." align=\"center\">".$uper2['startdat']."<br />".$uper2['completedat']."</td>\n");
echo ("<td ".$cl1." align=\"left\"><a href=\"details.php?id=".$uper2['torrent']."\">".$uper2['name']."</a></td>\n");
echo ("<td ".$cl1." align=\"center\">".mksize($uper2['uploaded'])." (".number_format(100 * $uper2['uploaded']/$uper2['size'],1)."%) / ".mksize($uper2['size'])."</td>
</tr>\n");
++$na;
}

if (!count($aray))
echo ("<tr><td colspan=\"5\" align=\"center\" class=\"b\">".$tracker_lang['no_data_now']."</td></tr>");


echo ('</table><br />');

stdfoot();

?>