<?
require "include/bittorrent.php";
gzip();
dbconn(false);
loggedinorreturn();


if ($CURUSER["shoutbox"] <> '0000-00-00 00:00:00'){
stdhead($tracker_lang['chat_banof']);
stdmsg($tracker_lang['error'], sprintf($tracker_lang['chat_banof'], $CURUSER["shoutbox"]));
stdfoot();
die; 
}


if (get_user_class() < UC_MODERATOR) {
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}



if (get_user_class() > UC_MODERATOR && isset($_POST["delmp"]) && is_array($_POST["delmp"])) {

$ptablep = ($_POST["php"] == 'victorina' ? "victorina":"vquestions");
$link = "words.php?page=".intval($_POST["page"])."&php=".$ptablep;

if (!isset($_POST["php"]))
stderr($tracker_lang['error'], $tracker_lang['missing_form_data']);

if (isset($_POST['actions']) && $_POST['actions'] == 'check')
sql_query("UPDATE ".$ptablep." SET work = 'on' WHERE id IN (" . implode(", ", array_map("intval", $_POST["delmp"])).")") or sqlerr(__FILE__, __LINE__);
else
sql_query("DELETE FROM ".$ptablep." WHERE id IN (" . implode(", ", array_map("intval", $_POST["delmp"])).")") or sqlerr(__FILE__, __LINE__);

@header("Location: ".$link) or die("<script>setTimeout('document.location.href=\"".$link."\"', 10);</script>");
}

$ttablet = ($_GET["php"] == 'victorina' ? "victorina":"vquestions");

stdhead(($ttablet == 'victorina' ? $tracker_lang['table_vika']:$tracker_lang['table_victorina']), true);


if (!isset($_GET["php"]) || empty($_GET["php"]) || !in_array($_GET["php"], array("victorina", "vquestions"))){

echo ("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">\n");
echo "<tr><td class=\"b\" align=\"center\" colspan=\"5\"><strong>".$tracker_lang['choose']."</strong>: <a title=\"".$tracker_lang['table_vika']."\" href=\"words.php?php=victorina\">".$tracker_lang['table_vika']."</a> / <a title=\"".$tracker_lang['table_victorina']."\" href=\"words.php?php=vquestions\">".$tracker_lang['table_victorina']."</a></td></tr>";
echo "</table>";

stdfoot(true);
die;
}


$count = get_row_count($ttablet);

list ($pagertop, $pagerbottom, $limit) = pager(60, $count, "words.php?php=".$ttablet."&");


$res = sql_query("SELECT v.*, u.class, u.username 
FROM ".$ttablet." AS v 
LEFT JOIN users AS u ON v.owner = u.id ORDER BY work, id DESC ".$limit) or sqlerr(__FILE__, __LINE__);


echo ("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">\n");

echo "<tr><td class=\"a\" align=\"center\" colspan=\"5\"><a title=\"".$tracker_lang['history_list']."\" class=\"alink\" href=\"tracker-chat.php?action=history\">".$tracker_lang['history_list']."</a> / <a title=\"".$tracker_lang['chat_panadd']."\" class=\"alink\" href=\"tracker-chat.php?action\">".$tracker_lang['chat_panadd']."</a> / <a title=\"".$tracker_lang['table_vika']."\" class=\"alink\" href=\"words.php?php=victorina\">".$tracker_lang['table_vika']."</a> / <a title=\"".$tracker_lang['table_victorina']."\" class=\"alink\" href=\"words.php?php=vquestions\">".$tracker_lang['table_victorina']."</a></td></tr>";

if (get_user_class() > UC_MODERATOR && file_exists(ROOT_PATH."torrents/txt/vika_nowords.txt") && $ttablet == 'victorina')
echo "<tr><td class=\"b\" align=\"center\" colspan=\"5\"><a title=\"".$tracker_lang['chat_exisques']."\" class=\"alink\" href=\"tracker-chat.php?action=wordsffile\">".$tracker_lang['chat_exisques']."</a></td></tr>";

echo "<tr><td class=\"b\" align=\"center\" colspan=\"5\">".$pagertop."</td></tr>";

if (get_user_class() > UC_MODERATOR)
echo "<form method=\"post\" action=\"words.php\" name=\"form1\">";

echo ("<tr>
".(get_user_class() > UC_MODERATOR ? "<td class=\"a\" align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['mark_all']."\" value=\"".$tracker_lang['mark_all']."\" onclick=\"this.value=check(document.form1.elements);\"></td>":"<td class=\"a\">#</td>")."
<td class=\"a\" align=\"left\">".$tracker_lang['question']."</td>
<td class=\"a\" align=\"left\">".($ttablet <> 'victorina' ? $tracker_lang['answer']:$tracker_lang['answ_action'])."</td>
<td class=\"a\" align=\"center\">".$tracker_lang['moderated']."</td>
<td class=\"a\" align=\"center\">".$tracker_lang['added']."</td>
</tr>\n");


if (get_user_class() > UC_MODERATOR)
echo '<script language="Javascript" type="text/javascript">
var checkflag = "false";
var marked_row = new Array;
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
} else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
}
}
</script>';


while ($arr = mysql_fetch_assoc($res)) {

if (empty($arr["username"]))
$sender = "<font color=\"red\">[<b>id ".$arr["owner"]."</b>]</font> <br />".$tracker_lang['anonymous']."";
else
$sender = "<a href=\"userdetails.php?id=".$arr["owner"]."\"><b>".get_user_class_color($arr["class"], $arr["username"])."</b></a>";

echo "<tr>";

if (get_user_class() > UC_MODERATOR)
echo ("<td class=\"a\" align=\"center\"><INPUT type=\"checkbox\" name=\"delmp[]\" value=\"".$arr['id']."\"><br />".$arr['id']."</td>\n");
else
echo ("<td class=\"b\" align=\"center\">".$arr['id']."</td>");



echo "<td class=\"b\">".htmlspecialchars_uni($arr["question"])."</td>";

if ($ttablet == 'victorina'){
$answers = unserialize($arr['answer']);
echo "<td class=\"b\" align=\"left\">";
$num = 1;
foreach ($answers AS $ans){
echo $num.": ".format_comment($ans, true)."<br />";
++$num;
}
echo "</td>";
} else echo "<td class=\"b\" align=\"left\">".htmlspecialchars_uni($arr['answer'])."</td>";

echo "<td class=\"b\" align=\"center\">".($arr["work"] == 'on' ? $tracker_lang['yes']:$tracker_lang['no'])."</td>";

echo "<td class=\"b\" align=\"center\" valign=\"top\">".$sender."<br />".(!empty($arr["last_check"]) ? '':$arr["time"])."</td>";

}


echo ("</tr>");

if (get_user_class() > UC_MODERATOR){

echo "<tr><td class=\"b\" align=\"left\" colspan=\"5\">
<input type=\"hidden\" name=\"page\" value=\"".intval(isset($_GET['page']) ? $_GET['page']:0)."\" />
<input type=\"hidden\" name=\"php\" value=\"".$ttablet."\" />
<select name=\"actions\">
<option value=\"check\" selected=\"selected\">".$tracker_lang['approve']."</option>\n
<option value=\"delete\">".$tracker_lang['delete']."</option>\n
</select>

<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['b_action']."\" />


</td></tr>";

}

echo "<tr><td class=\"b\" align=\"center\" colspan=\"5\">".$pagerbottom."</td></tr>";

echo "</table>";

if (get_user_class() > UC_MODERATOR)
echo "</form>";

echo "<br />";

stdfoot(true);

?>