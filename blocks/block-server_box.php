<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

global $tracker_lang;

$blocktitle = $tracker_lang['server_loading']." ROM";


$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-server_box", "time" => 60*60*24*4, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {


function lnk_size ($file_o = 'cache'){

$size = 0;

if (strtoupper(substr(PHP_OS,0,3) == 'WIN')) $file = $_SERVER["DOCUMENT_ROOT"].'/'.$file_o;
else $file = ROOT_PATH.$file_o.'/';

if (strtoupper(substr(PHP_OS, 0, 3) == 'WIN')){

$obj = new COM ('scripting.filesystemobject');

if (is_object($obj)){
$ref = $obj-> getfolder ($file);
$size = $ref->size;
$obj = null;
}

} else {
    
$handle = @popen('du '.escapeshellcmd($file).' -b -s -S', 'r');
$read = @fread($handle, 4096);
@pclose($handle);

if (!empty($read)){

preg_match("/^([0-9\+]{1,20})\s.*?/is", $read, $view);
$size = trim($view[1]);

if (!empty($size)){
preg_match('/\d+/', $read, $view); // Parse result
$size = trim($view[0]);
}

}

}

if (!empty($size)) return $size;
else return false;

}


$now = time();
$dttime = get_date_time();
$res = sql_query("SELECT arg, value_i, value_u FROM avps") or sqlerr(__FILE__,__LINE__);
while ($row = mysql_fetch_array($res)){
$arra[$row["arg"]]["value_u"]=$row["value_u"];
$arra[$row["arg"]]["value_i"]=$row["value_i"];
}




$csize_now = lnk_size('cache');
$csize_now_2 = lnk_size('torrents/thumbnail');
$csize_now_3 = lnk_size('torrents/txt');
$csize_now_4 = lnk_size('torrents/tfiles');
$csize_now_5 = lnk_size('torrents/images');
$csize_now_6 = lnk_size('torrents');
$csize_now_7 = lnk_size('backup/backup');


$sziae = intval($csize_now+$csize_now_2+$csize_now_3+$csize_now_4+$csize_now_5+$csize_now_6+$csize_now_7+lnk_size('pic/avatar'));

$content = "<table class=\"main\" border=\"0\" width=\"100%\">";








///////////////// backup /////////////////
if ($arra["cache_backup"]["value_i"] <= $csize_now_7) {

sql_query("UPDATE avps SET value_u = ".sqlesc($now).", value_i = ".sqlesc($csize_now_7).", value_s = ".sqlesc($dttime)." WHERE arg='cache_backup'") or sqlerr(__FILE__,__LINE__);

if (mysql_modified_rows()==0)
sql_query("INSERT INTO avps (arg, value_u, value_i, value_s) VALUES ('cache_backup', ".sqlesc($now).", ".sqlesc($csize_now_7).",".sqlesc($dttime).")");

$topsize_cache_7 = "Max: <b>".mksize($csize_now_7)."</b> ".$tracker_lang['in']." ".get_date_time($now)."";
$arra["cache_backup"]["value_i"] = $csize_now_7;
$topsize_max_7 = $csize_now_7;
}
elseif (!empty($arra["cache_backup"]["value_i"])) {
$topsize_max_7 = $arra["cache_backup"]["value_i"];
$topsize_cache_7 = "Max: <b>".mksize($arra["cache_backup"]["value_i"])."</b> (".mksize($csize_now_7).") ".$tracker_lang['in']." ".get_date_time($arra["cache_backup"]["value_u"])."";
}

if (!empty($csize_now_7)){

$fix_cacheall = round(($csize_now_7/$topsize_max_7)*100);

if ($fix_cacheall <= 10)
$pic_6 = "loadbarbg.gif";
elseif ($fix_cacheall <= 80)
$pic_6 = "loadbargreen.gif";
elseif ($fix_cacheall <= 95)
$pic_6 = "loadbaryellow.gif";
else 
$pic_6 = "loadbarred.gif";

$width_6 = intval($fix_cacheall * 4);

$content.= "<tr>";

$content.= "<td class=\"a\" align=\"left\">";
$content.= $tracker_lang['size']." /<strong>backup</strong>";
$content.= "</td>";

$content.= "<td class=\"b\" align=\"center\">";
$content.= "<table border=\"0\" width=\"400\"><tr>
<td style=\"padding: 0px; background-repeat: repeat-x\" title=\"".$tracker_lang['size']." /backup\">
<img height=\"15\" width=\"".$width_6."\" src=\"/pic/".$pic_6."\">
</td>
</tr></table>";
$content.= (!empty($csize_now_7) ? $topsize_cache_7:"");
$content.= "</td>";

$content.= "</tr>";
}
///////////////// backup /////////////////






///////////////// cache /////////////////
if ($arra["cache_size"]["value_i"] <= $csize_now) {

sql_query("UPDATE avps SET value_u = ".sqlesc($now).", value_i = ".sqlesc($csize_now).", value_s = ".sqlesc($dttime)." WHERE arg='cache_size'") or sqlerr(__FILE__,__LINE__);

if (mysql_modified_rows()==0)
sql_query("INSERT INTO avps (arg, value_u, value_i, value_s) VALUES ('cache_size', ".sqlesc($now).", ".sqlesc($csize_now).",".sqlesc($dttime).")");

$topsize_cache = "Max: <b>".mksize($csize_now)."</b> ".$tracker_lang['in']." ".get_date_time($now)."";
$arra["cache_size"]["value_i"] = $csize_now;
$topsize_max = $csize_now;
}
elseif (!empty($arra["cache_size"]["value_i"])) {
$topsize_max = $arra["cache_size"]["value_i"];
$topsize_cache = "Max: <b>".mksize($arra["cache_size"]["value_i"])."</b> (".mksize($csize_now).") ".$tracker_lang['in']." ".get_date_time($arra["cache_size"]["value_u"])."";
}

if (!empty($csize_now)){

$fix_cacheall = round(($csize_now/$topsize_max)*100);

if ($fix_cacheall <= 10)
$pic_6 = "loadbarbg.gif";
elseif ($fix_cacheall <= 80)
$pic_6 = "loadbargreen.gif";
elseif ($fix_cacheall <= 95)
$pic_6 = "loadbaryellow.gif";
else 
$pic_6 = "loadbarred.gif";

$width_6 = intval($fix_cacheall * 4);

$content.= "<tr>";

$content.= "<td class=\"b\" align=\"left\">";
$content.= $tracker_lang['size']." /<strong>cache</strong>";
$content.= "</td>";

$content.= "<td class=\"a\" align=\"center\">";
$content.= "<table border=\"0\" width=\"400\"><tr>
<td style=\"padding: 0px; background-repeat: repeat-x\" title=\"".$tracker_lang['size']." /cache\">
<img height=\"15\" width=\"".$width_6."\" src=\"/pic/".$pic_6."\">
</td>
</tr></table>";
$content.= (!empty($csize_now) ? $topsize_cache:"");
$content.= "</td>";

$content.= "</tr>";
}
///////////////// cache /////////////////




///////////////// torrents /////////////////
if ($arra["cache_torrents"]["value_i"] <= $csize_now_6) {

sql_query("UPDATE avps SET value_u = ".sqlesc($now).", value_i = ".sqlesc($csize_now_6).", value_s = ".sqlesc($dttime)." WHERE arg='cache_torrents'") or sqlerr(__FILE__,__LINE__);

if (mysql_modified_rows()==0)
sql_query("INSERT INTO avps (arg, value_u, value_i, value_s) VALUES ('cache_torrents', ".sqlesc($now).", ".sqlesc($csize_now_6).",".sqlesc($dttime).")");

$topsize_cache_6 = "Max: <b>".mksize($csize_now_6)."</b> ".$tracker_lang['in']." ".get_date_time($now)."";
$arra["cache_torrents"]["value_i"] = $csize_now_6;
$topsize_max_6 = $csize_now_6;
}
elseif (!empty($arra["cache_torrents"]["value_i"])) {
$topsize_max_6 = $arra["cache_torrents"]["value_i"];
$topsize_cache_6 = "Max: <b>".mksize($arra["cache_torrents"]["value_i"])."</b> (".mksize($csize_now_6).") ".$tracker_lang['in']." ".get_date_time($arra["cache_torrents"]["value_u"])."";
}

if (!empty($csize_now_6)){

$fix_cacheall = round(($csize_now_6/$topsize_max_6)*100);

if ($fix_cacheall <= 10)
$pic_6 = "loadbarbg.gif";
elseif ($fix_cacheall <= 80)
$pic_6 = "loadbargreen.gif";
elseif ($fix_cacheall <= 95)
$pic_6 = "loadbaryellow.gif";
else 
$pic_6 = "loadbarred.gif";

$width_6 = intval($fix_cacheall * 4);

$content.= "<tr>";

$content.= "<td class=\"a\" align=\"left\">";
$content.= $tracker_lang['size']." /<strong>torrents</strong>";
$content.= "</td>";

$content.= "<td class=\"b\" align=\"center\">";
$content.= "<table border=\"0\" width=\"400\"><tr>
<td style=\"padding: 0px; background-repeat: repeat-x\" title=\"".$tracker_lang['size']." /torrents\">
<img height=\"15\" width=\"".$width_6."\" src=\"/pic/".$pic_6."\">
</td>
</tr></table>";
$content.= (!empty($csize_now_6) ? $topsize_cache_6:"");
$content.= "</td>";

$content.= "</tr>";
}
///////////////// torrents /////////////////









///////////////// images /////////////////
if ($arra["cache_images"]["value_i"] <= $csize_now_5) {

sql_query("UPDATE avps SET value_u = ".sqlesc($now).", value_i = ".sqlesc($csize_now_5).", value_s = ".sqlesc($dttime)." WHERE arg='cache_images'") or sqlerr(__FILE__,__LINE__);

if (mysql_modified_rows()==0)
sql_query("INSERT INTO avps (arg, value_u, value_i, value_s) VALUES ('cache_images', ".sqlesc($now).", ".sqlesc($csize_now_5).",".sqlesc($dttime).")");

$topsize_cache_5 = "Max: <b>".mksize($csize_now_5)."</b> ".$tracker_lang['in']." ".get_date_time($now)."";
$arra["cache_images"]["value_i"] = $csize_now_5;
$topsize_max_5 = $csize_now_5;
}
elseif (!empty($arra["cache_images"]["value_i"])) {
$topsize_max_5 = $arra["cache_images"]["value_i"];
$topsize_cache_5 = "Max: <b>".mksize($arra["cache_images"]["value_i"])."</b> (".mksize($csize_now_5).") ".$tracker_lang['in']." ".get_date_time($arra["cache_images"]["value_u"])."";
}

if (!empty($csize_now_5)){

$fix_cacheall = round(($csize_now_5/$topsize_max_5)*100);

if ($fix_cacheall <= 10)
$pic_6 = "loadbarbg.gif";
elseif ($fix_cacheall <= 80)
$pic_6 = "loadbargreen.gif";
elseif ($fix_cacheall <= 95)
$pic_6 = "loadbaryellow.gif";
else 
$pic_6 = "loadbarred.gif";

$width_6 = intval($fix_cacheall * 4);

$content.= "<tr>";

$content.= "<td class=\"b\" align=\"left\">";
$content.= $tracker_lang['size']." /<strong>images</strong>";
$content.= "</td>";

$content.= "<td class=\"a\" align=\"center\">";
$content.= "<table border=\"0\" width=\"400\"><tr>
<td style=\"padding: 0px; background-repeat: repeat-x\" title=\"".$tracker_lang['size']." torrents/images\">
<img height=\"15\" width=\"".$width_6."\" src=\"/pic/".$pic_6."\">
</td>
</tr></table>";
$content.= (!empty($csize_now_5) ? $topsize_cache_5:"");
$content.= "</td>";

$content.= "</tr>";
}
///////////////// images /////////////////






///////////////// tfiles /////////////////
if ($arra["cache_tfiles"]["value_i"] <= $csize_now_4) {

sql_query("UPDATE avps SET value_u = ".sqlesc($now).", value_i = ".sqlesc($csize_now_4).", value_s = ".sqlesc($dttime)." WHERE arg='cache_tfiles'") or sqlerr(__FILE__,__LINE__);

if (mysql_modified_rows()==0)
sql_query("INSERT INTO avps (arg, value_u, value_i, value_s) VALUES ('cache_tfiles', ".sqlesc($now).", ".sqlesc($csize_now_4).",".sqlesc($dttime).")");

$topsize_cache_4 = "Max: <b>".mksize($csize_now_4)."</b> ".$tracker_lang['in']." ".get_date_time($now)."";
$arra["cache_tfiles"]["value_i"] = $csize_now_4;
$topsize_max_4 = $csize_now_4;
}
elseif (!empty($arra["cache_tfiles"]["value_i"])) {
$topsize_max_4 = $arra["cache_tfiles"]["value_i"];
$topsize_cache_4 = "Max: <b>".mksize($arra["cache_tfiles"]["value_i"])."</b> (".mksize($csize_now_4).") ".$tracker_lang['in']." ".get_date_time($arra["cache_tfiles"]["value_u"])."";
}

if (!empty($csize_now_4)){

$fix_cacheall = round(($csize_now_4/$topsize_max_4)*100);

if ($fix_cacheall <= 10)
$pic_6 = "loadbarbg.gif";
elseif ($fix_cacheall <= 80)
$pic_6 = "loadbargreen.gif";
elseif ($fix_cacheall <= 95)
$pic_6 = "loadbaryellow.gif";
else 
$pic_6 = "loadbarred.gif";

$width_6 = intval($fix_cacheall * 4);

$content.= "<tr>";

$content.= "<td class=\"a\" align=\"left\">";
$content.= $tracker_lang['size']." /<strong>tfiles</strong>";
$content.= "</td>";

$content.= "<td class=\"b\" align=\"center\">";
$content.= "<table border=\"0\" width=\"400\"><tr>
<td style=\"padding: 0px; background-repeat: repeat-x\" title=\"".$tracker_lang['size']." torrents/tfiles\">
<img height=\"15\" width=\"".$width_6."\" src=\"/pic/".$pic_6."\">
</td>
</tr></table>";
$content.= (!empty($csize_now_4) ? $topsize_cache_4:"");
$content.= "</td>";

$content.= "</tr>";
}
///////////////// tfiles /////////////////










///////////////// thumbnail /////////////////
if ($arra["cache_thumb"]["value_i"] <= $csize_now_2) {

sql_query("UPDATE avps SET value_u = ".sqlesc($now).", value_i = ".sqlesc($csize_now_2).", value_s = ".sqlesc($dttime)." WHERE arg='cache_thumb'") or sqlerr(__FILE__,__LINE__);

if (mysql_modified_rows()==0)
sql_query("INSERT INTO avps (arg, value_u, value_i, value_s) VALUES ('cache_thumb', ".sqlesc($now).", ".sqlesc($csize_now_2).",".sqlesc($dttime).")");

$topsize_cache_2 = "Max: <b>".mksize($csize_now_2)."</b> ".$tracker_lang['in']." ".get_date_time($now)."";
$arra["cache_thumb"]["value_i"] = $csize_now_2;
$topsize_max_2 = $csize_now_2;
}
elseif (!empty($arra["cache_thumb"]["value_i"])) {
$topsize_max_2 = $arra["cache_thumb"]["value_i"];
$topsize_cache_2 = "Max: <b>".mksize($arra["cache_thumb"]["value_i"])."</b> (".mksize($csize_now_2).") ".$tracker_lang['in']." ".get_date_time($arra["cache_thumb"]["value_u"])."";
}

if (!empty($csize_now_2)){

$fix_cacheall = round(($csize_now_2/$topsize_max_2)*100);

if ($fix_cacheall <= 10)
$pic_6 = "loadbarbg.gif";
elseif ($fix_cacheall <= 80)
$pic_6 = "loadbargreen.gif";
elseif ($fix_cacheall <= 95)
$pic_6 = "loadbaryellow.gif";
else 
$pic_6 = "loadbarred.gif";

$width_6 = intval($fix_cacheall * 4);

$content.= "<tr>";

$content.= "<td class=\"b\" align=\"left\">";
$content.= $tracker_lang['size']." /<strong>thumbnail</strong>";
$content.= "</td>";

$content.= "<td class=\"a\" align=\"center\">";
$content.= "<table border=\"0\" width=\"400\"><tr>
<td style=\"padding: 0px; background-repeat: repeat-x\" title=\"".$tracker_lang['size']." torrents/thumbnail\">
<img height=\"15\" width=\"".$width_6."\" src=\"/pic/".$pic_6."\">
</td>
</tr></table>";
$content.= (!empty($csize_now_2) ? $topsize_cache_2:"");
$content.= "</td>";

$content.= "</tr>";
}
///////////////// thumbnail /////////////////



///////////////// txt /////////////////
if ($arra["cache_txt"]["value_i"] <= $csize_now_3) {

sql_query("UPDATE avps SET value_u = ".sqlesc($now).", value_i = ".sqlesc($csize_now_3).", value_s = ".sqlesc($dttime)." WHERE arg='cache_txt'") or sqlerr(__FILE__,__LINE__);

if (mysql_modified_rows()==0)
sql_query("INSERT INTO avps (arg, value_u, value_i, value_s) VALUES ('cache_txt', ".sqlesc($now).", ".sqlesc($csize_now_3).",".sqlesc($dttime).")");

$topsize_cache_3 = "Max: <b>".mksize($csize_now_3)."</b> ".$tracker_lang['in']." ".get_date_time($now)."";
$arra["cache_txt"]["value_i"] = $csize_now_3;
$topsize_max_3 = $csize_now_3;
}
elseif (!empty($arra["cache_txt"]["value_i"])) {
$topsize_max_3 = $arra["cache_txt"]["value_i"];
$topsize_cache_3 = "Max: <b>".mksize($arra["cache_txt"]["value_i"])."</b> (".mksize($csize_now_3).") ".$tracker_lang['in']." ".get_date_time($arra["cache_txt"]["value_u"])."";
}

if (!empty($csize_now_3)){

$fix_cacheall = round(($csize_now_3/$topsize_max_3)*100);

if ($fix_cacheall <= 10)
$pic_6 = "loadbarbg.gif";
elseif ($fix_cacheall <= 80)
$pic_6 = "loadbargreen.gif";
elseif ($fix_cacheall <= 95)
$pic_6 = "loadbaryellow.gif";
else 
$pic_6 = "loadbarred.gif";

$width_6 = intval($fix_cacheall * 4);

$content.= "<tr>";

$content.= "<td class=\"a\" align=\"left\">";
$content.= $tracker_lang['size']." /<strong>txt</strong>";
$content.= "</td>";

$content.= "<td class=\"b\" align=\"center\">";
$content.= "<table border=\"0\" width=\"400\"><tr>
<td style=\"padding: 0px; background-repeat: repeat-x\" title=\"".$tracker_lang['size']." torrents/txt\">
<img height=\"15\" width=\"".$width_6."\" src=\"/pic/".$pic_6."\">
</td>
</tr></table>";
$content.= (!empty($csize_now_3) ? $topsize_cache_3:"");
$content.= "</td>";

$content.= "</tr>";
}
///////////////// txt /////////////////





///////////////// ROM /////////////////
$dfree = @disk_free_space(ROOT_PATH);
$dtotal = @disk_total_space(ROOT_PATH);

$permem_p = max(1, (100 - round(($dfree/$dtotal)*100)));
$permem_p_2 = max(1, (100 - round((($dfree+$sziae)/($dtotal-$dfre))*100)));


if ($permem_p <= 25) $pic = "loadbarbg.gif";
elseif ($permem_p <= 80) $pic = "loadbargreen.gif";
else $pic = "loadbaryellow.gif";


$width_m = max(1, $permem_p * 4);
$width_m_2 = max(1, ($permem_p-$permem_p_2)*4);


if (!empty($permem_p) && !empty($dtotal)){

$content.= "<tr>";

$content.= "<td class=\"b\" align=\"left\">";
$content.= "ROM";
$content.= "</td>";

$content.= "<td class=\"a\" align=\"center\">";
$content.= "<table border=\"0\" width=\"402\"><tr>
<td style=\"padding: 0px; background-repeat: repeat-x\">
<img height=\"15\" width=\"".($width_m-$width_m_2)."\" src=\"pic/".$pic."\" title=\"ROM: ".($permem_p_2)."%, ".$tracker_lang['rem_space_busy'].": ".mksize($dfree)." / ".mksize($dtotal-$dfree-$sziae)." (".mksize($dtotal).")\"><img height=\"15\" width=\"".$width_m_2."\" src=\"pic/loadbarred.gif\" title=\"ROM: ".max(1, ($permem_p-$permem_p_2))."%, ".$tracker_lang['size'].": ".mksize($sziae)."\">
</td>
</tr></table>";
$content.= "ROM: <b>".($permem_p)."%</b>, ".$tracker_lang['rem_space_busy'].": <b>".mksize($dfree)."</b> / <b>".mksize($dtotal-$dfree)."</b> (<u>".mksize($dtotal)."</u>)";
$content.= "</td>";

$content.= "</tr>";

}
///////////////// ROM /////////////////







$content.= "</table>";

file_query($content, $cache = array("type" => "disk", "file" => "block-server_box", "time" => 60*60*24*4, "action" => "set"));
}

?>