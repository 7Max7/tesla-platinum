<?
require_once("include/bittorrent.php");
dbconn();

global $SITE_Config, $default_theme, $CURUSER;

if (!headers_sent() && $CURUSER) {
header("Location: index.php");
die;
} elseif ($CURUSER)
die("<script>setTimeout('document.location.href=\"index.php\"', 10);</script>");

if (!mkglobal("username:password"))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);


if ($SITE_Config["captcha"] == true){

$p_captcha = trim(isset($_POST["captcha"]) ? (string) $_POST["captcha"] : false);
$p_hash = trim(isset($_POST["hash"]) ? (string) $_POST["hash"] : false);
$returnto = htmlentities($_GET["returnto"]);

include_once("include/functions_captcha.php");

if (!empty($p_captcha) && valid_captcha($p_captcha, $p_hash) == false){
stderr($tracker_lang['error'], $tracker_lang['captcha_invalid']."<br /><a href=\"login.php\"><b>".$tracker_lang['back_inlink']."</b></a>");

} elseif (empty($p_captcha)){

stdhead($tracker_lang['takelogin']);

echo "<table border=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<form method=\"post\" action=\"takelogin.php\">";

echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"><fieldset class=\"fieldset\"><legend><b>".$tracker_lang['warning']."</b></legend><h3>".$tracker_lang['signup_use_cookies']."</h3></fieldset></td></tr>";

if (!empty($_GET["returnto"])) {

$site_own = (($_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://").htmlspecialchars_uni($_SERVER['HTTP_HOST']);

if (!isset($_GET["nowarn"]))
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"><div class=\"error2\">".$tracker_lang['no_inlogin']."<br />
<input type=\"text\" size=\"100\" name=\"fJf\" value=\"".$site_own."/".$returnto."\" readonly style=\"width: 80%;\"/>
</div></td></tr>";
}

$acaptcha = creating_captcha();

echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['captcha_confirm'].":</td><td class=\"a\" align=\"left\">

<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">

<tr><td class=\"b\" align=\"center\" style=\"margin: 0px; padding: 0px;\">
<div id=\"result\">".$acaptcha["echo"]."</div>
<input type=\"hidden\" name=\"hash\" value=\"".$acaptcha["hash"]."\" />
</td>
<td class=\"a\" width=\"50px\"><a href=\"#\" id=\"status\" class=\"button button-blue\"><span>".$tracker_lang['captcha_button']."</span></a></td>
</tr>

<tr><td class=\"a\" colspan=\"2\"><input type=\"text\" name=\"captcha\" size=\"20\" value=\"\" /> ".$tracker_lang['captcha_take']."</td></tr>
</table>

</td></tr>";

echo '<script type="text/javascript">
jQuery("#status").click(function() {
var onput = jQuery("#old").attr("value");
jQuery.post("md5.php",{"update": "true", "hash": "'.$acaptcha["hash"].'", "old": onput},
function(response) {jQuery("#result").fadeIn("slow"); jQuery("#result").html(response);}, "html");
});
</script>';

echo("
<input type=\"hidden\" name=\"returnto\" value=\"".$returnto."\" />\n
<input type=\"hidden\" name=\"username\" value=\"".$username."\" />\n
<input type=\"hidden\" name=\"password\" value=\"".$password."\" />\n
");

echo "<tr><td class=\"b\" colspan=\"2\" align=\"center\"><input type=\"submit\" class=\"btn\" style=\"width: 200px;\" value=\"".$tracker_lang['enter_me']."\"/></td></tr>";
echo "</form>";

echo "</table>";
echo "<br />";

stdfoot();

die;

}

}



if ($SITE_Config["maxlogin"] == true && !$CURUSER)
failedloginscheck(); /// �������� ������� �� ip ������

$ip = getip();

if (empty($username) || empty($password))
stderr($tracker_lang['error'], $tracker_lang['data_noempty'].": <br /> <strong>".(empty($username)? $tracker_lang['signup_username']."<br />":"")."".(empty($password)? $tracker_lang['signup_password']:"")."</strong>");
elseif (strlen($username) > 12)
stderr($tracker_lang['error'], $tracker_lang['signup_username'].": ".sprintf($tracker_lang['max_simp_of'], 12));
elseif (!validusername($username))
stderr($tracker_lang['error'], $tracker_lang['validusername']);

else {

$res = sql_query("SELECT id, passhash, secret, unseed, stylesheet, shelter, class, enabled, email, modcomment, status, monitoring, ip, usercomment, passkey FROM users WHERE username = ".sqlesc($username)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($res);

}

if (!$row)
stderr($tracker_lang['error'], $tracker_lang['user_noexis_in']);

if (!validemail($row["email"]))
stderr($tracker_lang['error'], $tracker_lang['validemail']);

////////////////////
if (!file_exists(ROOT_PATH."/themes/".$row["stylesheet"]."/template.php"))
sql_query("UPDATE users SET stylesheet='$default_theme' WHERE username = ".sqlesc($username)) or sqlerr(__FILE__, __LINE__);
////////////////////

if ($SITE_Config["maxlogin"] == true && !$CURUSER && ($row["passhash"] <> md5($row["secret"].$password.$row["secret"])))
failedlogins($username, $row["id"]); /// �������� ���������� �������� ������ + ����� ���.

$usercomment = $row["usercomment"];

if ($row["status"] == 'pending')
stderr($tracker_lang['error'], $tracker_lang['account_not_activated']);

if ($row["passhash"] <> md5($row["secret"].$password.$row["secret"]))
stderr($tracker_lang['error'], $tracker_lang['access_denied'].": ".$row["id"]." (passhash)");

if ($row["enabled"] == "no")
stderr($tracker_lang['error'], $tracker_lang['user_off'].": ".$username);


/////////////////// ��� �����
if ($row["class"] < UC_ADMINISTRATOR){

$rmail = sql_query("SELECT id, added, comment FROM bannedemails WHERE email = ".sqlesc($row["email"])) or sqlerr(__FILE__, __LINE__);
$rbans_mail = mysql_fetch_array($rmail);

if ($rbans_mail["added"] && $rbans_mail["id"]) {
$modcom = date("Y-m-d")." $row[email] - �������� (".htmlspecialchars_uni($rbans_mail["comment"]).").\n";
sql_query("UPDATE users SET enabled = 'no', modcomment = CONCAT_WS('', ".sqlesc($modcom).", modcomment) WHERE email = ".sqlesc($row["email"])) or sqlerr(__FILE__, __LINE__);

stderr($tracker_lang['error'], $tracker_lang['emailbanned'].", (".$row["email"]."): ".htmlspecialchars_uni($rbans_mail["comment"]));
}
}
/////////////////// ��� �����


/*
/// ��� � �� ������ - 92
if ($row["id"] == "92"){
$ip_real = getip();
$com_end=$usercomment;
$update = get_date_time() . " ����� � ������� $ip_real.\n". $com_end;
sql_query("UPDATE users SET modcomment='$update' WHERE id = ".$row["id"]) or sqlerr(__FILE__, __LINE__);
}
/// ������ � ������� ������������ � ����� � ������� ����
*/


$agent_msg = htmlentities($_SERVER["HTTP_USER_AGENT"]);

$peers = sql_query("SELECT ip, useragent FROM sessions WHERE uid = ".sqlesc($row["id"])." AND username = ".sqlesc($username)." AND time > ".sqlesc(get_date_time(gmtime() - 300))." LIMIT 1")or sqlerr(__FILE__, __LINE__);
$num = mysql_fetch_array($peers);
$ip = getip();

if ($ip==$num["ip"])
$enter = false;
elseif (!empty($num["ip"]) && $ip <> $num["ip"] && $num["useragent"] <> $agent_msg)
$enter = true;
else
$enter = false;

if ($enter == true){

$crc32_now=crc32(htmlentities($_SERVER["HTTP_USER_AGENT"]));

write_log("������� �������� ����� � ������� ������� ��� ��� � ����. ������������ � ip : <strong>".$ip."</strong> ($crc32_now) ������� ����� � �����: <strong>".$username."</strong> (".$num["ip"].").","#BCD2E6","error");


if ($row['monitoring']=='yes') {
$sf = ROOT_PATH."/cache/monitoring_".$row["id"].".txt";
$fpsf = @fopen($sf,"a+");
@fputs($fpsf, "������� ����� � ������� ������� ��� � ���� $row[ip] (crc-".$crc32_now.")#".htmlspecialchars_uni($_SERVER["REQUEST_URI"])."#".getip()."#".htmlentities($_SERVER["HTTP_USER_AGENT"])."#".get_date_time()."#\n\r");
@fclose($fpsf);
}

//// ��������� ������������� � ����� � �� �������
if ($row["class"] >= UC_MODERATOR){

$msg = sqlesc("������ ���, ���� �������������� ������� ����� � ��� ������� (� ����� �������), ���� �� ������ �� ������ ���� ������ � �����, �� ����������, ������� � [url=$DEFAULTBASEURL/my.php]������ ��������[/url] � ��������� ������� �������� ����� [b]������� ������[/b] � [b]����������� ���������[/b]. ����� ��������� �����, ������ ����� - � ��� ����� [b][url=$DEFAULTBASEURL/my.php]����� ������[/url][/b] �� ����� �����. [hr] [b]IP �����[/b]: $ip \n[b]�������[/b]: $agent_msg");

$subject = sqlesc("������� �����");
$maws = sql_query("SELECT COUNT(*) FROM messages WHERE subject = ".$subject." AND added < ".sqlesc(get_date_time()-500)) or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($maws)>0)
sql_query("INSERT INTO messages (sender, receiver, added, msg, poster, subject) VALUES (0, $row[id], ".sqlesc(get_date_time()).", $msg, 0, $subject)")  or sqlerr(__FILE__,__LINE__);

}
//// ��������� ������������� � ����� � �� �������

$ip_real=getip();
$poput = "������� ����� � $ip_real";
if (!stristr($usercomment, $poput)!==false)
sql_query("UPDATE users SET usercomment = CONCAT_WS('', ".sqlesc(get_date_time() . " - ������� ����� � $ip_real.\n").", usercomment) WHERE id = ".sqlesc($row["id"])) or sqlerr(__FILE__, __LINE__);

stderr($tracker_lang['error'], $tracker_lang['inlogin_session']);
}

logincookie($row["id"], $row["passhash"], $row["shelter"],1);


/// ������ �� ����� ����� aka 7Max7
$ip = getip();
$updateset[] = 'ip = '.sqlesc($ip); /// add .$set_checked

//// �������� ���������� �� ������������ ���������
$unseed = get_row_count("torrents", "WHERE owner = ".sqlesc($row["id"])." AND (leechers / seeders >= 4) AND multitracker = 'no' LIMIT 500");
$updateset[] = "unseed = ".sqlesc($unseed);
//// �������� ���������� �� ������������ ���������

//// �������� ���������� �� ���������� ���������
$unmark = get_row_count("snatched", "WHERE (SELECT COUNT(*) FROM ratings WHERE torrent = snatched.torrent AND user = ".sqlesc($row["id"]).") < '1' AND snatched.finished = 'yes' AND userid = ".sqlesc($row["id"]));
$updateset[] = "unmark = ".sqlesc($unmark);
//// �������� ���������� �� ���������� ���������

//// �������� ���������� ������������� ���������
$res_un = sql_query("SELECT COUNT(*) FROM messages WHERE receiver = ".$row["id"]." AND location = '1' AND unread = 'yes'") or sqlerr(__FILE__,__LINE__);
$arr_un = @mysql_fetch_row($res_un);
$updateset[] = "unread = ".sqlesc($arr_un[0]);
//// �������� ���������� ������������� ���������

$updateset[] = "simpaty = ".sqlesc(get_row_count("simpaty", "WHERE touserid = ".sqlesc($row["id"])));

if ($updateset)
sql_query("UPDATE users SET ".implode(", ", $updateset)." WHERE id = ".$row["id"])  or sqlerr(__FILE__, __LINE__);
/// ������ �� ����� ����� aka 7Max7

if ($row['monitoring'] == 'yes') {
$sf = ROOT_PATH."/cache/monitoring_".$row["id"].".txt";
$fpsf = @fopen($sf,"a+");
@fputs($fpsf,"���� � ������##".getip()."#".htmlentities($_SERVER["HTTP_USER_AGENT"])."#".get_date_time()."#\n\r");
@fclose($fpsf);
}

sql_query("UPDATE referrers SET uid = ".sqlesc($row["id"])." WHERE ip = ".sqlesc($ip)." AND uid = '0'");

sql_query("DELETE FROM loginattempts WHERE ip = ".sqlesc($ip)." AND attempts < '3'");


unsql_cache("arrid_".$row["id"]);
unsql_cache("an_".$row["passkey"]); // passkey �� ������� ������� ���

if (!empty($_POST["returnto"]))
header("Location: ".$DEFAULTBASEURL."/".$_GET["returnto"]);
else
@header("Location: ".$DEFAULTBASEURL."/") or die("<script>setTimeout('document.location.href=\"".$DEFAULTBASEURL."\"', 10);</script>");
die;

?>