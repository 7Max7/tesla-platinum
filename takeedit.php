<?
require_once("include/bittorrent.php");
require_once("include/benc.php");

// Call to undefined function mb_convert_case() - ������� ���������� posix.so � mbstring.so

function cache_img($id, $name, $where = false) {

if (empty($name))
return false;

$ary = array("details", "browse", "block", "beta", "getdetals");
foreach ($ary AS $ima){

$fname = ROOT_PATH."/torrents/thumbnail/".$id.(!empty($where) ? $where:"").$ima.".".strtolower(end(explode('.', $name)));
if (file_exists($fname)) @unlink($fname);
}

}

if (!mkglobal("id"))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

if (empty($id) || !is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

dbconn();
loggedinorreturn();



/// ������������� ��������
if (!empty($_POST["reacon"])) {

$res = sql_query("SELECT name, owner, seeders, image1, tags, moderated, moderatedby, info_hash, picture1, picture2, picture3, picture4, category FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_array($res);

if (!$row)
stderr($tracker_lang['error'], $tracker_lang['torrent_del_or_move']);

if ($CURUSER["id"] != $row["owner"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if (!empty($CURUSER["catedit"]) && !preg_match("/\[cat".$row["category"]."\]/is", $CURUSER["catedit"]) && ($CURUSER["id"] <> $row["owner"]) && get_user_class() == UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['no_modcat']);

$tname = htmlspecialchars_uni($row["name"]);

$rt = (int) $_POST["reasontype"];
$dup = htmlspecialchars_uni($_POST["dup"]);
$rule = htmlspecialchars_uni($_POST["rule"]);

if (!is_int($rt) || $rt < 1 || $rt > 4)
stderr($tracker_lang['deleting_torrent'], $tracker_lang['noreason_act'].": ".$tracker_lang['reason']);

if ($rt == 1)
$reasonstr = "������� (��� ��������)";
elseif ($rt == 2) {
if (!$dup)
stderr($tracker_lang['deleting_torrent'], $tracker_lang['noreason_act'].": ".$tracker_lang['torrent_dybl']);

$reasonstr = "��������".($dup ? (" ��: ".$dup) : "!");
} elseif ($rt == 3) {
if (!$rule) 
stderr($tracker_lang['deleting_torrent'], $tracker_lang['noreason_act'].": ".$tracker_lang['torrent_banrules']);

$reasonstr = "��������� ������: ".$rule;
} elseif ($rt == 4) {

$reasonstr = "��������� ������: ������� ���������� �������.";
$info_hash = $row["info_hash"];

$descki = array();

$resf = sql_query("SELECT * FROM files WHERE torrent = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
if (mysql_num_rows($resf) <> 0){
while ($rowf = mysql_fetch_array($resf))
$descki[] = $rowf["filename"].":".mksize($rowf["size"])."\n";
} else {

$ifilename = ROOT_PATH."/torrents/".$id.".torrent";

if (@file_exists($ifilename)){

$dict = bdec_file($ifilename);
list($info) = dict_check_t($dict, "info");
list($dname, $plen, $pieces) = @dict_check_t($info, "name(string):piece length(integer):pieces(string)");
$filelist = array();
$totallen = @dict_get_t($info, "length", "integer");
if (isset($totallen))
$descki[] = utf8_to_win($dname).":".mksize($totallen);
else {
$flist = @dict_get_t($info, "files", "list");
$totallen = 0;

if (count($flist)){
foreach ($flist as $sf) {
list($ll, $ff) = @dict_check_t($sf, "length(integer):path(list)");
$totallen += $ll;
$ffa = array();

foreach ($ff as $ffe) {
$ffa[] = $ffe["value"];
}
$descki[] = utf8_to_win(implode("/", $ffa)).":".mksize($ll);
}
}
}
}

}

$count = count($descki);
if ($count > 1000) {
array_splice($descki, 1000);
$descki[] = '...';
}

$desc = implode("\n", $descki)."\n\n[b]���������� ������[/b]: ".(count($descki) <> $count ? count($descki)." / ".$count:$count)." ��.\n\n[b]����� ������[/b]: ".mksize((!empty($totallen) ? $totallen:$row["size"]));

$desc = "[spoiler=������ ������ ($id)]\n".$desc."[/spoiler]";


sql_query("INSERT INTO license VALUES (0, ".sqlesc($tname).", ".sqlesc($info_hash).", ".sqlesc(get_date_time()).", ".sqlesc(($desc)).")");
}

deletetorrent($id);

if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["image1"]) && !empty($row["image1"]))
@unlink(ROOT_PATH."/torrents/images/".$row["image1"]);

for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["picture".$xpi_s]) && !empty($row["picture".$xpi_s]))
@unlink (ROOT_PATH."/torrents/images/".$row["picture".$xpi_s]);

}

$tags = explode(",", $row["tags"]);
foreach ($tags as $tag) {
@sql_query("UPDATE tags SET howmuch = howmuch-1 WHERE name = ".sqlesc($tag)) or sqlerr(__FILE__, __LINE__);
}

if ($row["owner"] <> $CURUSER["id"] && !empty($row["owner"])) {

$res = sql_query("SELECT username, enabled FROM users WHERE id = ".sqlesc($row["owner"]));
$row2 = mysql_fetch_array($res);

if (!empty($row2["username"]) && $row2["enabled"] = 'yes'){

$msg = ("���� ������� $tname ($id) ���� ������� �������������  $CURUSER[username] ($reasonstr)");
$subject = ("������� $id ($tname) ��� ������");
sql_query("INSERT INTO messages (sender, receiver, added, msg, poster, subject) VALUES (0, ".sqlesc($row["owner"]).", ".sqlesc(get_date_time()).", ".sqlesc($msg).", 0, ".sqlesc($subject).")") or sqlerr(__FILE__,__LINE__);

}

}

write_log(($row["owner"] == $CURUSER["id"] ? "������ ":"")."������� $id ($tname) ��� ������ $CURUSER[username]: $reasonstr", "", "torrent");

unsql_cache("block-comment");
unsql_cache("block-last_files");
unsql_cache("anet_".$row["info_hash"]); // ������ �������
unsql_cache("anto_".$row["info_hash"]); // ������

unsql_cache("block-last_files"); /// ��������� �������
unsql_cache("block_showrealese_mulcache"); /// ��������� �������
unsql_cache("block_showrealese_topc"); /// ��� ������
unsql_cache("showRealese_search"); /// ��� ���������
unsql_cache("showRealese_last"); /// ��� ���������
unsql_cache("block-showRealese"); /// �������
unsql_cache("block_3dview_0"); /// ������� ��� ���� 3view

if (!empty($CURUSER["groups"])){
unsql_cache("group_data_".$CURUSER["groups"]);
unsql_cache("traff_".$CURUSER["groups"]);
}

stderr($tracker_lang['success'], $tracker_lang['torrent']." ".$tracker_lang['deleted'].": ".$row["name"]." <br />".$tracker_lang['reason'].": ".$reasonstr);
}
/// ����� �������� ��������


if (!mkglobal("id:name:descr:type"))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": <strong>
".(empty($_POST["id"]) ? $tracker_lang['invalid_id']:"")."
".(empty($_POST["descr"]) ? $tracker_lang['description']:"")."
".(empty($_POST["type"]) ? $tracker_lang['sort_cat']:"")."
".(empty($_POST["name"]) ? $tracker_lang['name']:"")."
</strong>");



$res = sql_query("SELECT torrents.*, u.username, u.class
FROM torrents
LEFT JOIN users AS u ON u.id = torrents.moderatedby
WHERE torrents.id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

$row = mysql_fetch_array($res);

if (!$row)
stderr($tracker_lang['error'], $tracker_lang['torrent_del_or_move']);

if ($CURUSER["id"] <> $row["owner"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if (!empty($CURUSER["catedit"]) && !preg_match("/\[cat".$row["category"]."\]/is", $CURUSER["catedit"]) && ($CURUSER["id"] <> $row["owner"]) && get_user_class() == UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['no_modcat']);

$row_name = htmlspecialchars_uni($row["name"]);



/////////////////////////// ������� ���� �����, ������ � ������� ������ � ����������� � ��������
if ($row_name <> $name){
$stop_dfiles = stop_dfiles($name);
if ($stop_dfiles <> false)
stderr($tracker_lang['error']." / <a href=\"edit.php?id=".$id."\"><b>".$tracker_lang['back_inlink']."</b></a>", sprintf($tracker_lang['stop_dfiles_warn'], "<strong>".$name."</strong>", "<strong>#".$stop_dfiles['id']."</strong>, <strong>".$stop_dfiles['key_in']."</strong>", $stop_dfiles['number']));
}
/////////////////////////// ������� ���� �����, ������ � ������� ������ � ����������� � �������� 




$row_image = htmlspecialchars($row["image1"]);
$row_descr = htmlspecialchars_uni(addslashes($row["descr"]));

if (get_user_class() == UC_SYSOP)
$torrent_com = $_POST["torrent_com"];
else
$torrent_com = $row["torrent_com"];

if ($_POST["banned"] == "yes" && empty($_POST["banned_reason"]) && $row["banned"] == "no")
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['reason']);

if (empty($_POST["name"]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['name']);

$updateset = array();

/// picturemod array for Tesla TT





if (isset($_POST['array_picture']) && !empty($_POST['array_picture'])) {

$arr_del = array();
$explo = explode("\n", $_POST['array_picture']);

$numxpi = 0;
foreach ($explo AS $xpi_s){

if (empty($xpi_s) || $numxpi > 4) break;

$pic = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", $xpi_s)); /// ����� �� ������ [] � ��
$pic = trim(htmlentities($pic)); ///������� ������ �� ������

if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $pic) && !in_array(crc32($pic), $arr_del)){
$_POST['picture'.$numxpi] = $pic;
$_POST['pic'.$numxpi.'action'] = "update";
$arr_del[] = crc32($pic);
++$numxpi;
}

}

}




if ($Functs_Patch["take_pic"] <> 1){

for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

$pic1action = $_POST['pic'.$xpi_s.'action'];

if ($pic1action <> "keep"){

$picture_1 = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", $row["picture".$xpi_s])); /// ����� �� ������ [] � ��
$picture_1 = htmlentities($picture_1); ///������� ������ �� ������

if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $picture_1) && !empty($picture_1))
unset($picture_1);

if ($pic1action == "update"){

///��������� �������� �������
if (!empty($_FILES['picture_'.$xpi_s]['name'])) {

$updateset[] = "picture".$xpi_s." = " .sqlesc(uploadimage("picture_".$xpi_s, $picture_1, $id, "_".$xpi_s));
cache_img($id, $row["picture".$xpi_s], "_".$xpi_s);

unset($_POST["picture".$xpi_s]);
}

} elseif ($pic1action == "delete") {

if (!empty($row["picture".$xpi_s])) {
@unlink(ROOT_PATH."/torrents/images/".$picture_1);

$updateset[] = "picture".$xpi_s." = ''";
cache_img($id, $row["picture".$xpi_s], "_".$xpi_s);
}

}

}

}
}

// picturemod
$img1action = $_POST['img1action'];

$image0_inet = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", $_POST['image0_inet'])); /// ����� �� ������ [] � ��
$image0_inet = htmlentities($image0_inet); ///������� ������ �� ������

if ($img1action == "update"){

///��������� �������� �������
if (!empty($_FILES['image0']['name']))
$updateset[] = "image1 = ".sqlesc(uploadimage("image0", $row["image1"], $id));

elseif (!empty($image0_inet) && empty($_FILES['image0']['name']) && preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image0_inet)){

list ($width, $height) = getimagesize($image0_inet);

/// �������� ���������� http ������
if (!empty($width) && !empty($height)){

$image = @file_get_contents($image0_inet); /// �������� ��� �������� ��� �������� ��

if (empty($image))
stderr($tracker_lang['error'], $tracker_lang['image_empty'].": ".$image0_inet);

if (strlen($image) > $Torrent_Config["max_image_size"])
stderr($tracker_lang['error'], sprintf($tracker_lang['image_isbig'], mksize($Torrent_Config["max_image_size"])));

/// ������� ���� ��� �� http ������
if (!empty($row["image1"]) && !preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["image1"]))
@unlink(ROOT_PATH."/torrents/images/".$row["image1"]); /// ������� ������ ������ � �������

$caimage = imageshack($image0_inet);

if ($caimage == false || empty($caimage)) $caimage = $image0_inet;

if ($caimage <> false)
$updateset[] = "image1 = ".sqlesc($caimage);

}
}

cache_img($id, $row["image1"]);

} elseif ($img1action == "delete") {

if (!empty($row["image1"])) {

@unlink(ROOT_PATH."/torrents/images/".htmlspecialchars($row["image1"]));
$updateset[] = "image1 = ''";

cache_img($id, $row["image1"]);
}

}

if (!empty($_POST["imgimove"]) && !empty($row["image1"]) && !preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["image1"]) && file_exists(ROOT_PATH."/torrents/images/".htmlspecialchars($row["image1"]))){

$caimage = imageshack(ROOT_PATH."/torrents/images/".htmlspecialchars($row["image1"]));

if ($caimage <> false){
$updateset[] = "image1 = ".sqlesc($caimage);
unlink(ROOT_PATH."/torrents/images/".htmlspecialchars($row["image1"]));
cache_img($id, $row["image1"]);
}

}


if ($Functs_Patch["take_pic"] <> 2){

for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

$pic1action = $_POST['pic'.$xpi_s.'action'];

if ($pic1action <> "keep"){

$pic = $_POST['picture'.$xpi_s];

$pic = htmlentities($pic);

/// sendpic � funkyimg.com
if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $pic) && !stristr($pic,"sendpic.ru") && !stristr($pic,"funkyimg.com")){

if ($pic1action == "update" && empty($_FILES['picture_'.$xpi_s]['name'])){

if (list($width, $height) = @getimagesize($pic))
$updateset[] = "picture".$xpi_s." = ".sqlesc($pic);

} elseif ($pic1action == "delete" && empty($_FILES['picture_'.$xpi_s]['name']))
$updateset[] = "picture".$xpi_s." = ''";

cache_img($id, $row["picture".$xpi_s], "_".$xpi_s);

}
}

}
}
/// picturemod array for Tesla TT


if (isset($_FILES["tfile"]) && !empty($_FILES["tfile"]["name"]))
$update_torrent = true;

if (!empty($update_torrent)) {

$f = $_FILES["tfile"];
$fname = trim($f["name"]);

if (empty($fname) || !validfilename($fname) || !preg_match('/^(.+)\.torrent$/si', $fname, $matches))
stderr($tracker_lang['error'], $tracker_lang['no_upload_torrent']);

$tmpname = $f["tmp_name"];

if (!is_uploaded_file($tmpname) || !filesize($tmpname))
stderr($tracker_lang['error'], $tracker_lang['dox_cannot_move']);

$dict = bdec_file($tmpname, $Torrent_Config["max_torrent_size"]);
if (!isset($dict))
stderr($tracker_lang['error'], $tracker_lang['no_binary_torrent']);

list ($info) = dict_check($dict, "info");
list ($dname, $plen, $pieces) = dict_check($info, "name(string):piece length(integer):pieces(string)");

if ($dict['value']['created by']['strlen'] > 128)
stderr($tracker_lang['error'], "created by field too long");

$filelist = array();
$totallen = dict_get($info, "length", "integer");
if (isset($totallen))
$filelist[] = array($dname, $totallen);
else {

$flist = dict_get($info, "files", "list");
if (!isset($flist))
stderr($tracker_lang['error'], "missing both length and files");

if (!count($flist))
stderr($tracker_lang['error'], "no files");
$totallen = 0;

foreach ($flist as $fn) {
list($ll, $ff) = dict_check($fn, "length(integer):path(list)");
$totallen += $ll;
$ffa = array();

foreach ($ff as $ffe) {
if ($ffe["type"] != "string")
stderr($tracker_lang['error'], "filename error");
$ffa[] = $ffe["value"];
}

if (!count($ffa))
stderr($tracker_lang['error'], "filename error 2");

$ffe = implode("/", $ffa);
$filelist[] = array($ffe, $ll);

/*
if ($ffe == 'Thumbs.db'){
stderr($tracker_lang['error'], "� ��������� ��������� ������� ����� Thumbs.db!");
die;
}
*/

}

}



/// ���� ��� ���������� - ������ ������ � �������� �����
if (!empty($dict['value']['info']['value']['private']['value'])){

$dict['value']['announce'] = bdec(benc_str($announce_urls[0])); /// change announce url to local

$dict['value']['info']['value']['source'] = bdec(benc_str("[".$DEFAULTBASEURL."] ".$SITENAME)); // add link for bitcomet users

unset($dict['value']['announce-list']); /// remove multi-tracker capability
unset($dict['value']['nodes']); /// remove cached peers (Bitcomet & Azareus)
unset($dict['value']['info']['value']['crc32']); /// remove crc32
unset($dict['value']['info']['value']['ed2k']); /// remove ed2k
unset($dict['value']['info']['value']['md5sum']); /// remove md5sum
unset($dict['value']['info']['value']['sha1']); /// remove sha1
unset($dict['value']['info']['value']['tiger']); /// remove tiger
unset($dict['value']['azureus_properties']); /// remove azureus properties

}


if (!empty($dict['value']['info']['value']['private']['value']))
$dict['value']['info']['value']['private'] = bdec('i1e'); /// Privat
//else
//$dict['value']['info']['value']['private'] = bdec('i0e'); /// ������ dht


$dict = bdec(benc($dict)); // double up on the becoding solves the occassional misgenerated infohash

/// ���� ��� ���������� - ������ ������ � �������� �����
if (!empty($dict['value']['info']['value']['private']['value'])){
$dict['value']['comment'] = bdec(benc_str($DEFAULTBASEURL."/details.php?id=".$id)); // change torrent comment to URL
$dict['value']['created by'] = bdec(benc_str($CURUSER["username"])); // change created by
$dict['value']['publisher'] = bdec(benc_str($CURUSER["username"])); // change publisher
$dict['value']['publisher.utf-8'] = bdec(benc_str($CURUSER["username"])); // change publisher.utf-8
$dict['value']['publisher-url'] = bdec(benc_str($DEFAULTBASEURL."/userdetails.php?id=".$CURUSER["id"])); // change publisher-url
$dict['value']['publisher-url.utf-8'] = bdec(benc_str($DEFAULTBASEURL."/userdetails.php?id=".$CURUSER["id"])); // change publisher-url.utf-8
}

list ($info) = dict_check($dict, "info");
$infohash = sha1($info["string"]);


//////////////////////////// ��������� ������� - ������ �������
$num_license = get_row_count("license","WHERE info_hash = ".sqlesc($infohash));
if (!empty($num_license))
stderr($tracker_lang['access_denied'], $tracker_lang['file_copyright']);
//////////////////////////// ��������� ������� - ������ �������


@unlink(ROOT_PATH."/torrents/".$id.".torrent");

move_uploaded_file($tmpname, ROOT_PATH."/torrents/".$id.".torrent");

$fp = fopen(ROOT_PATH."/torrents/".$id.".torrent", "w");
if ($fp) {
@fwrite($fp, benc($dict), strlen(benc($dict)));
fclose($fp);
}

$updateset[] = "info_hash = ".sqlesc($infohash);
$updateset[] = "numfiles = ".sqlesc(count($filelist));

@sql_query("DELETE FROM files WHERE torrent = ".$id);
foreach ($filelist as $file) {
@sql_query("INSERT INTO files (torrent, filename, size) VALUES (".sqlesc($id).", ".sqlesc(utf8_to_win($file[0])).",".$file[1].")");
}


sql_query("UPDATE torrents SET multi_time = '0000-00-00 00:00:00' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

}

$name = preg_replace("/\&/", "and", $name);
$name = trim(htmlspecialchars_decode($name));


if (get_user_class() >= UC_MODERATOR) {

///// ��� ����� /////
if ($_POST["oldtags"] <> $_POST["tags"]){

$replace = array(", ", " , ", " ,");
$ptype = (int) $_POST["type"];

$_POST["tags"] = str_ireplace("/", ",", $_POST["tags"]);
$_POST["tags"] = str_ireplace(".", " ", $_POST["tags"]);

$tags = trim(str_replace($replace, ",", tolower(trim(htmlspecialchars_uni($_POST["tags"])))));
$oldtags = tolower(trim(htmlspecialchars_uni($_POST["oldtags"])));

$un = array_diff(explode(",", $tags), explode(",", $oldtags));
$un2 = array_diff(explode(",", $oldtags), explode(",", $tags));

$ret = array();
$rest = sql_query("SELECT name FROM tags WHERE category = ".sqlesc($ptype));
while ($rowt = mysql_fetch_array($rest))
$ret[] = tolower($rowt["name"]);

$union = array_intersect($ret, $un);
$ununion = array_diff($un, $ret);

/// ����������� � ������� ������� ������ ������ - ��� ����������� ����������� ����� (����� �����)
$tag = tolower($tag);
$ptype = tolower($ptype);

foreach ($union as $tag) {
sql_query("UPDATE tags SET howmuch = howmuch+1 WHERE name = ".sqlesc($tag)) or sqlerr(__FILE__, __LINE__);
}

foreach ($un2 as $tag) {
sql_query("UPDATE tags SET howmuch = howmuch-1 WHERE name = ".sqlesc($tag)) or sqlerr(__FILE__, __LINE__);
}

foreach ($ununion as $tag) {
sql_query("INSERT INTO tags (category, name, howmuch, added) VALUES (".sqlesc($ptype).", ".sqlesc($tag).", 1, ".sqlesc(get_date_time()).")") or sqlerr(__FILE__, __LINE__);
}

unsql_cache("block_cloudstags");
unsql_cache("taggenrelist_".$type); /// ������� ��� ����� �������������� �������

}

///// ��� ����� /////
}


$descr = htmlspecialchars_uni(addslashes($_POST["descr"]));

if (empty($descr))
stderr($tracker_lang['error'], $tracker_lang['no_upload_desc']);


if (get_user_class() >= UC_MODERATOR && $_POST["oldtags"] <> $_POST["tags"])
$updateset[] = "tags = ".sqlesc($tags);

if (is_valid_id($type))
$updateset[] = "category = ".sqlesc($type);


if (get_user_class() >= UC_ADMINISTRATOR) {


if ($_POST["banned"] == "yes" && $row["banned"] == "no") {

$banned_reason = htmlspecialchars($_POST["banned_reason"]);

$updateset[] = "banned_reason = ".sqlesc($banned_reason);
$updateset[] = "banned = 'yes'";

$torrent_com = get_date_time()." ".$CURUSER["username"]." ������� ������� (".$banned_reason.").\n". $torrent_com;

} elseif ($_POST["banned"] == "no" && $row["banned"] == "yes") {

$updateset[] = "banned_reason = ''";
$updateset[] = "banned = 'no'";

$torrent_com = get_date_time()." ".$CURUSER["username"]." �������� �������.\n". $torrent_com;

}


/// ������� ����
if (!empty($_POST["truncacheimg"])){

$ima_type = end(explode('.', $row_image));
$feimg = 0;

foreach (explode(".", "details.block.beta.getdetals.browse") as $x){

$feurl = ROOT_PATH."/torrents/thumbnail/".$id.$x.".".$ima_type;
if (file_exists($feurl)){
$feimg += filesize($feurl);
@unlink($feurl);
}

if ($x == "details"){
for ($xpi_s=1; $xpi_s<5; $xpi_s++) {
$ima_type = end(explode('.', $row["picture".$xpi_s]));
$feurl = ROOT_PATH."/torrents/thumbnail/".current(explode('.', $row["picture".$xpi_s]))."_".$xpi_s.".".$ima_type;
if (file_exists($feurl))
$feimg += filesize($feurl);
cache_img($id, $row["picture".$xpi_s], "_".$xpi_s);
}
}

}

if (!empty($feimg))
$torrent_com = get_date_time()." ".$CURUSER["username"]." ������� ��� (".mksize($feimg).").\n". $torrent_com;
}


if ($_POST["sticky"] == "yes" && $row["sticky"] == "no") {
$updateset[] = "sticky = 'yes'";
$torrent_com = get_date_time()." ".$CURUSER["username"]." �������� ��������.\n". $torrent_com;
} elseif ($_POST["sticky"] == "no" && $row["sticky"] == "yes") {
$updateset[] = "sticky = 'no'";
$torrent_com = get_date_time()." ".$CURUSER["username"]." ���� ��������.\n". $torrent_com;
}


if ($_POST["stopped"] == "no" && $row["stop_time"] <> "0000-00-00 00:00:00") {
$updateset[] = "stop_time = ".sqlesc("0000-00-00 00:00:00");
$torrent_com = get_date_time()." ".$CURUSER["username"]." ���������� �������.\n". $torrent_com;
} elseif ($_POST["stopped"] == "yes" && $row["stop_time"] == "0000-00-00 00:00:00") {
$updateset[] = "stop_time = ".sqlesc(get_date_time());
$torrent_com = get_date_time()." ".$CURUSER["username"]." ������������ ������� ����� (�� ����� �������).\n". $torrent_com;
}



}


if (isset($_POST["up_date"]) && $_POST["up_date"] == "yes" && get_user_class() >= UC_MODERATOR)
$updateset[] = "added = ".sqlesc(get_date_time()); /// ��������� ���� ���� ���������


if (get_user_class() >= UC_MODERATOR) {

if (isset($_POST["lock_comments"]) && $_POST["lock_comments"] == "yes" && $row["comment_lock"] == 'no')
$updateset[] = "comment_lock = 'yes'";

if (isset($_POST["lock_comments"]) && $_POST["lock_comments"] == "no" && $row["comment_lock"] == 'yes')
$updateset[] = "comment_lock = 'no'";

}

if (get_user_class() >= UC_MODERATOR){


$updateset[] = "free = '".(isset($_POST["free"]) && $_POST["free"] == 1 ? 'yes' : 'no')."'";


if (isset($_POST["checks_files"]) && $_POST["checks_files"] == "yes"){

$file_url = ROOT_PATH."/torrents/".$id.".torrent";

if (@file_exists($file_url)){

$dict = bdec_file($file_url, 1024000);
list($info) = dict_check_t($dict, "info");
list($dname, $plen, $pieces) = @dict_check_t($info, "name(string):piece length(integer):pieces(string)");

$filelist = array();
$totallen = @dict_get_t($info, "length", "integer");
if (isset($totallen))
$filelist[] = array($dname, $totallen);
else {

$flist = @dict_get_t($info, "files", "list");
if (!isset($flist))
$fileerror=true;
if (!@count($flist))
$fileerror=true;
$totallen = 0;

foreach ($flist as $file_url) {

list($ll, $ff) = @dict_check_t($file_url, "length(integer):path(list)");
$totallen += $ll;
$ffa = array();

foreach ($ff as $ffe) {
if ($ffe["type"] != "string")
$fileerror=true;
$ffa[] = $ffe["value"];
}

if (!count($ffa))
$fileerror=true;
$ffe = implode("/", $ffa);
$filelist[] = array($ffe, $ll);
}

}

$dict = @bdec(@benc($dict));
@list($info) = @dict_check_t($dict, "info");
$infohash = sha1($info["string"]);

if (!empty($totallen)){
sql_query("DELETE FROM files WHERE torrent = ".sqlesc($id));

foreach ($filelist as $file) {
sql_query("INSERT INTO files (torrent, filename, size) VALUES (".sqlesc($id).", ".sqlesc(utf8_to_win($file[0])).",".sqlesc($file[1]).")") or sqlerr(__FILE__, __LINE__);
}

}

$updateset[] = "numfiles = ".sqlesc(@count($filelist));

$torrent_com = get_date_time()." $CURUSER[username] �����. ���������� ������ (+).\n". $torrent_com;
} else {
$torrent_com = get_date_time()." $CURUSER[username] �����. ���������� ������ (����������� .torrent).\n". $torrent_com;
}

}


}



if (get_user_class() >= UC_ADMINISTRATOR){

//// ��� �������
if ($_POST["vip_only"] == "yes" && $row["viponly"] == "0000-00-00 00:00:00"){

$day = (int) $_POST["day_s"]; /// ���
$s_day_sql = get_date_time(gmtime() + $day * 86400);

if (!empty($day)){
$updateset[] = "viponly = ".sqlesc($s_day_sql);
$torrent_com = get_date_time()." ".$CURUSER["username"]." ������� vip ������� ($day ��).\n". $torrent_com;
}

} elseif ($_POST["vip_only"] == "no" && $row["viponly"] <> "0000-00-00 00:00:00"){

$updateset[] = "viponly = '0000-00-00 00:00:00'";
$torrent_com = get_date_time()." ".$CURUSER["username"]." �������� vip �������.\n". $torrent_com;

}
//// ��� �������


if (isset($_POST["delete_comment"]) && $_POST["delete_comment"] == 'yes'){

sql_query("DELETE FROM comments WHERE torrent = ".sqlesc($id));
write_log($CURUSER["username"]." ������ ��� ����������� � �������� $name ($id)", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "comment");

unsql_cache("block-comment");

$torrent_com = get_date_time()." ".$CURUSER["username"]." ������ ��� ����������� ��������.\n". $torrent_com;
}



/// ������� ����� ���������
if (!empty($_POST["user_reliases_anonim"])){

$updateset[] = "owner = '0'";
$torrent_com = get_date_time()." ".$CURUSER["username"]." �������� ������ �����������.\n". $torrent_com;

}


/// ������� ����� ������� ������
if (!empty($_POST["release_set_id"])){

$setid = (int) $_POST["release_set_id"];

$updateset[] = "owner = ".sqlesc($setid);

$res = sql_query("SELECT username FROM users WHERE id = ".sqlesc($setid)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($res);

if (!empty($row["username"]))
$owner_t = $row["username"];
else
$owner_t = "[".$setid."]";

if (!empty($setid))
$torrent_com = get_date_time()." ".$CURUSER["username"]." �������� ���������� - $owner_t � ������.\n". $torrent_com;

}

/// ��������� ����������
if (!empty($_POST["mdbyme"]) && $row["moderatedby"] <> $row["owner"]){
$updateset[] = "moderatedby = ".sqlesc($row["owner"]);
$torrent_com = get_date_time()." ".$CURUSER["username"]." ��� ��������� ���������� (�.�.) �����.\n". $torrent_com;
}
/// ��������� ����������




}

$updateset[] = "visible = '".(isset($_POST["visible"]) && $_POST["visible"] == 'yes' && $row["banned"] == "no" ? "yes" : "no")."'";


$newid = (isset($_POST["newid"]) ? intval($_POST["newid"]):0);

if (!empty($newid) && get_user_class() >= UC_ADMINISTRATOR){

$count = get_row_count("torrents", "WHERE id = ".sqlesc($newid));

if (empty($count)){

$true = rename(ROOT_PATH."/torrents/".$id.".torrent", ROOT_PATH."/torrents/".$newid.".torrent");

if ($true == false)
stderr($tracker_lang['newid_tor'], $tracker_lang['dox_cannot_move'].": ".$newid.".torrent");

if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["image1"]) && !empty($row["image1"])){

$new_name = str_replace($id, $newid, $row["image1"]);
$true = rename(ROOT_PATH."/torrents/images/".$row["image1"], ROOT_PATH."/torrents/images/".$new_name);

if ($true == false)
$updateset[] = "image1 = ".sqlesc($row["image1"]);
else
$updateset[] = "image1 = ".sqlesc($new_name);

}

for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["picture".$xpi_s]) && !empty($row["picture".$xpi_s])){

$new_name = str_replace($id, $newid, $row["picture".$xpi_s]);
$true = rename(ROOT_PATH."/torrents/images/".$row["picture".$xpi_s], ROOT_PATH."/torrents/images/".$new_name);

if ($true == false)
$updateset[] = "picture".$xpi_s." = ".sqlesc($row["picture".$xpi_s]);
else
$updateset[] = "picture".$xpi_s." = ".sqlesc($new_name);

}

}

sql_query("UPDATE my_match SET torrentid = ".sqlesc($newid)." WHERE torrentid = ".sqlesc($id));
sql_query("UPDATE torrents SET id = ".sqlesc($newid)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE bookmarks SET torrentid = ".sqlesc($newid)." WHERE torrentid = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE cheaters SET torrentid = ".sqlesc($newid)."  WHERE torrentid = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE checkcomm SET checkid = ".sqlesc($newid)." WHERE checkid = ".sqlesc($id)." AND torrent = '1'") or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE report SET torrentid = ".sqlesc($newid)." WHERE torrentid = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE simpaty SET type = ".sqlesc($newid)." WHERE type = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE thanks SET torrentid = ".sqlesc($newid)." WHERE torrentid = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

foreach (explode(".","peers.files.comments.ratings.snatched") as $x)
sql_query("UPDATE ".$x." SET torrent = ".sqlesc($newid)." WHERE torrent = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

$torrent_com = get_date_time()." ".$CURUSER["username"]." �������� ������������� $newid ($id).\n". $torrent_com;

$id = $newid;

unsql_cache("block-last_files"); /// ��������� �������
unsql_cache("block_showrealese_mulcache"); /// ��������� �������
unsql_cache("block_showrealese_topc"); /// ��� ������
unsql_cache("showRealese_search"); /// ��� ���������
unsql_cache("showRealese_last"); /// ��� ���������
unsql_cache("block-showRealese"); /// �������
unsql_cache("block_3dview_0"); /// ������� ��� ���� 3view

}

}



if (get_user_class() >= UC_MODERATOR && $_POST["torrent_com_zam"])
$torrent_com = get_date_time()." ������� �� ".$CURUSER["username"].": ".htmlspecialchars($_POST["torrent_com_zam"])."\n". $torrent_com;


if ($row_descr <> $descr || $row_name <> $name){

$torrent_com = get_date_time()." ��� �������������� ".$CURUSER["username"].".\n".$torrent_com;

$updateset[] = "name = ".sqlesc($name);
$updateset[] = "descr = ".sqlesc(trim($descr));

unsql_cache("simtorid_".$id); /// �������� ���� ������� ������
unsql_cache("simtorid_alt_".$id); /// �������� ���� ������� ������
unsql_cache("traffp_".$id);

}



/// ������������� ������� ��������� ���� ������������� ����������������� �������
if ($row["class"] < get_user_class() && get_user_class() > UC_MODERATOR && ($row_descr <> $descr || $row_name <> $name) && $row["moderatedby"] <> $row["owner"]){
$updateset[] = "moderated = 'yes'";
$updateset[] = "moderatedby = ".sqlesc($CURUSER["id"]);
$updateset[] = "moderatordate = ".sqlesc(get_date_time());

unsql_cache("usermod_1");
}
/// ������������� ������� ��������� ���� ������������� ����������������� �������


/// ����������� � ������� �����
if (isset($totallen))
$updateset[] = "size = ".sqlesc($totallen);

if (!empty($torrent_com))
$updateset[] = "torrent_com = ".sqlesc($torrent_com);


$ori_web = strip_tags($_POST["webseed"]);
if (get_user_class() >= UC_MODERATOR && $row["webseed"] <> $ori_web) {

$parse_owner = parse_url($ori_web, PHP_URL_HOST);
if (empty($parse_owner))
$updateset[] = "webseed = ''";
else
$updateset[] = "webseed = ".sqlesc($ori_web);

}

if ($update_torrent)
$updateset[] = "multitracker = ".sqlesc(!empty($dict['value']['info']['value']['private']['value']) ? "no":"yes");

if (empty($dict['value']['info']['value']['private']['value']) && $update_torrent && !empty($row["info_hash"]) && !empty($Torrent_Config["multihours"]))
$updateset[] = "multi_time = '0000-00-00 00:00:00'";


sql_query("UPDATE torrents SET ".implode(", ", $updateset)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);


unsql_cache("multi_viewid_".$id);

header("Location: ".$DEFAULTBASEURL."/details.php?id=".$id);
die;
?>