<?
require_once("include/bittorrent.php");

dbconn(false);
loggedinorreturn();

if ($CURUSER['override_class'] == "255")
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

sql_query("UPDATE users SET class = ".sqlesc($CURUSER['override_class']).", override_class = '255' WHERE id = ".$CURUSER['id']) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$CURUSER["id"]);

$returlink = $_SERVER["HTTP_REFERER"];
$site = parse_url($returlink, PHP_URL_HOST);

if (empty($site) && empty($returlink))
$returlink = $DEFAULTBASEURL;

header("Location: ".$returlink);
die;
?>