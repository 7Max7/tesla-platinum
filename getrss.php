<? 
require "include/bittorrent.php";
dbconn();
loggedinorreturn();

$catoptions = '';

///////// cache
$res_ = sql_query("SELECT id, name FROM categories ORDER BY sort ASC", $cache = array("type" => "disk", "file" => "browse_genrelist_rss", "time" => 86400*7));
while ($cat_ = mysql_fetch_assoc_($res_)){
$catoptions.= "<label><input type=\"checkbox\" name=\"cat[]\" value=\"".$cat_["id"]."\" ".(preg_match('/\[cat'.$cat_["id"].'\]/i', $CURUSER["notifs"]) ? " checked" : "")."/>".$cat_["name"]."</label><br />";
$category[$cat_['id']] = $cat_['name'];
}
///////// cache


if (empty($CURUSER["passkey"]) || strlen($CURUSER["passkey"]) <> 32) {
$CURUSER["passkey"] = md5($CURUSER["username"].get_date_time().$CURUSER["passhash"]);
sql_query("UPDATE users SET passkey = ".sqlesc($CURUSER["passkey"])." WHERE id = ".sqlesc($CURUSER["id"]));
unsql_cache("arrid_".$CURUSER["id"]);
}

stdhead($tracker_lang['rss']);

if ($_SERVER['REQUEST_METHOD'] == "POST") {

$query = array();
$link = $DEFAULTBASEURL."/rss.php";

if ($_POST['feed'] == "dl")
$query[] = "feed=dl";

if (isset($_POST['cat']) && is_array($_POST['cat'])){

$exp = @array_unique(array_map('intval', $_POST['cat']));
$query[] = htmlspecialchars("cat=".implode(',', $exp));

}

if (isset($_POST['login']) && $_POST['login'] == "passkey")
$query[] = "passkey=".$CURUSER["passkey"];

if (count($query))
$link.= "?".implode("&", $query);

echo "<script type=\"text/javascript\">function highlight(field) {field.focus();field.select();}</script>";
echo "<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\" width=\"100%\">

<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['rss']."</td></tr>

<tr><td class=\"b\" width=\"50%\">".$tracker_lang['rss_newslink'].":</td><td class=\"a\"><a title=\"".$tracker_lang['rss']."\" href=\"".$link."\">".$link."</a></td></tr>

<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input onclick='highlight(this)' type=\"text\" size=\"100\" name=\"url\" value=\"".$link."\" .></td></tr>

</table><br />";

}

echo "<form method=\"post\" action=\"getrss.php\">
<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">

<tr>
<td class=\"a\">".$tracker_lang['my_default_browse']."</td>
<td class=\"b\">".$catoptions."</td>
</tr>

<tr>
<td class=\"b\">".$tracker_lang['rss_typelink']."</td>
<td class=\"a\">
<input type=\"radio\" name=\"feed\" value=\"web\" checked />".$tracker_lang['preview']."<br />
<input type=\"radio\" name=\"feed\" value=\"dl\" />".$tracker_lang['download_alt']."
</td>
</tr>

<tr>
<td class=\"a\">".$tracker_lang['rss_login']."</td>
<td class=\"b\">
<input type=\"radio\" name=\"login\" value=\"cookie\" />".$tracker_lang['rss_standart']."<br />
<input type=\"radio\" name=\"login\" value=\"passkey\" checked />".$tracker_lang['rss_altpass']."
</td>
</tr>

<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input class=\"btn\" title=\"".$tracker_lang['click_on_rss']."\" value=\"".$tracker_lang['click_on_rss']."\" type=\"submit\" /></td></tr>

</table></form>";

stdfoot();

?>