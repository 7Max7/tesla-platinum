<?php

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

if (!defined('BLOCK_FILE')) {
Header("Location: ../index.php");
exit;
}

global $tracker_lang;

$blocktitle = $tracker_lang['sticky_torrent']." (<a title=\"".$tracker_lang['click_on_sticky_list']."\" class=\"altlink_white\" href='browse.php?incldead=4'>".$tracker_lang['list']." ".$tracker_lang['tracker_torrents']."</a>)";

?>
<script src="/js/jquery.tools.min.js" type="text/javascript"></script>

<style>
fieldset,legend,ul,ol,li,img,fieldset,blockquote { margin: 0; padding: 0; }
h1,h2,h3,h4,h5,h6 { font-weight: normal; }
img,fieldset { border: 0; }
blockquote a { color:#2C7CC5 !Important }
blockquote a:hover { color:#FF8400 !Important  }

ul.css-tabs {
	margin:0 !important; 
	padding:0;
	height:30px;
	text-align:right;	 	
}
ul.css-tabs li {
 float:left;
	padding:0;
	margin:0;
	list-style-type:none;

}
#actions a:hover, #menu-numbers a:hover {
    background: #FAFAFA;
    color: #fff;
	border: solid 1px #AAE;
}
#actions a.current {
    background: #26B;
    color: #fff;
	border: solid 1px #AAE;
}
#actions a.tab1, #actions a.tab2 {
	color:#999;
	border-color:#999;
	background:#fff;
	float:left;
}
ul.css-tabs a:hover {
	background-color:#F7F7F7;
	color:#333;background-position:left bottom;
}
ul.css-tabs a.current {
	color:#000;	
	cursor:default;background-position:0px 108px;
}
div.css-panes div {
	display:none;
	width:930px;
}
#actions, #menu-numbers {
            font-size: 80%;
}
#actions a, #menu-numbers a {
    text-decoration: none;
	border: solid 1px #88B8E2;
	color: #15B;
	background-color:#F6F6F6}

#actions a {
    display: block;
    float: right;
    padding: 4px 8px;
    margin-right: 5px;
	margin-bottom: 5px;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
}
#menu-numbers a {
    display: block;
    float: left;
    padding: 4px 8px;
    margin-right: 5px;
	margin-bottom: 5px;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
}
#menu-numbers {
	float:right;
}
.div-tab-pic {
	float:left; margin-right:20px; 
}
.tabs-text-inside {
	float:left; width:510px; padding-left:10px; line-height:1.4em;
}
#panes div {
	width:953px;
	font-family:verdana;
	float:left;
	height: 300px;
	color:#000000;
}
.vertical {
	position:relative;
	overflow:hidden;	
	/* vertical scrollers have typically larger height than width */	
	height: 340px;	 
	width: auto;
	border-top:1px solid #ddd;	
}
.items {
	position:absolute;
	height:20000em;	
	margin: 0px;
}
#actions {
	width:960px;
	margin:0px 0 10px 0;
	position: relative;
	height: 25px;
}
#actions a {
	font-size:11px;		
	cursor:pointer;
	color:#666;
}
#actions a:hover {
	text-decoration:underline;
	color:#EE7702;
	background-color:#2266BB;
}
.disabled {
	visibility:hidden;		
}
.nextPage {
	float:right;
}
#content_wrap {
	width:100%;
	clear: both;
	color: #FEFEFE;
	height: auto;
	background: url(../pic/tabs/content-home-backg.jpg) repeat-x;
}
#content_wrap2 {
	width:100%;
	clear: both;
	background:url(../pic/tabs/content-home2-backg2.jpg) repeat-x;
	background-color:#F2EFF0;
	height: auto;	
}
#content, #content2 {
	width:990px;
	margin:0 auto;
	text-align:left;
}
#content {
	margin-top:25px;
}
#content_main_home {
	width:960px;
	margin: 0 auto;
	padding: 0px 10px 30px 10px;
}
#content_main_pricing {
	width:980px;
	margin: 0 auto;
	padding: 0 0 40px 0;
/*	background: url(../pic/tabs/black-backg-panes.jpg) left repeat-y;*/
}
#content_main_home2 {
	width:960px;
	margin: 0 auto;
	height: 365px;
	padding: 20px 10px 10px 10px;
	text-align: left;
}
.ad-wrap {
	height:173px;
	width:960px;
	position: relative;
}
.content-ad-pic {
	width:388px;
	height:141px;
	float: left;
	margin: 0 12px 0 0;
	background:url(../pic/tabs/content-ad-backg.jpg) repeat-x;
	padding: 15px;
}
.content-ad-text  {
	width:488px;
	height:120px;
	float: left;
	background:url(../pic/tabs/content-ad-backg.jpg) repeat-x;
	padding: 20px;
}
</style>

<?

$content = "<div id=\"content_wrap2\">";
$content.= "<div style=\"height: auto;\" id=\"content_main_home2\">";

$res = sql_query("SELECT name, id, image1, descr, picture1, picture2, picture3, picture4, tags, size FROM torrents WHERE sticky='yes' ORDER BY added DESC LIMIT 40", $cache = array("type" => "disk", "file" => "block-aimagepro", "time" => 600)) or sqlerr(__FILE__,__LINE__);///

$num = mysql_num_rows_($res);
$dupl = $num/2;

$content.= "<div id=\"actions\">";
$content.= "<a class=\"next\">".$tracker_lang['page_next']."</a>&nbsp;";

$content.= "<div id=\"menu-numbers\">";

if ($dupl <= 1)
$content.= "<a class=\"current\">1</a>";
else{

for ($i=1; $i <= $dupl; $i++){
$content.= "<a class=\"".($dupl == $i ? "current":"")."\">".$i."</a>&nbsp;";
}
}

$content.= "</div>";

$content.= "<a class=\"prev\">".$tracker_lang['page_prev']."</a>";
$content.= "</div>";

$content.= "<div class=\"scrollable vertical\">";
$content.= "<div class=\"items\" style=\"top: -692px;\">";


while ($row = mysql_fetch_assoc_($res)){

$name = htmlspecialchars_uni($row["name"]);

$content.= "<div class=\"ad-wrap\">"; ///cloned

$image1 = $row["image1"];


if (empty($image1))
$image1 = "default_torrent.png";

$content.= "<blockquote class=\"content-ad-pic\">";
$content.= "<span class=\"galPicList daGallery\">";

$img = array();
for ($i=1; $i < 5; $i++){

$image0 = htmlentities($row["picture".$i.""]);
if (!empty($image0)) {

if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image0))
$img[] = "<a title=\"<a href='details.php?id=".$row["id"]."'>".$name."</a> ".$tracker_lang['or']." <a href='download.php?id=".$row["id"]."'>".$tracker_lang['download']." ".$tracker_lang['name_torrent']."</a>\" rel=\"group".$row["id"]."\" href=\"".$image0."\"><img style=\"float: right; margin: 0pt 0pt 2px 50px;\" width=\"85px\" border='0' src=\"".$image0."\"/></a>";

}
}

if (count($img) == 4)
$content.= implode(' ',$img);


if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image1))
$content.= "<a href=\"".$image1."\" rel=\"group".$row["id"]."\" title=\"<a href='details.php?id=".$row["id"]."'>".$name."</a> ".$tracker_lang['or']." <a href='download.php?id=".$row["id"]."'>".$tracker_lang['download']." ".$tracker_lang['name_torrent']."</a>\"><img height=\"120px\" title=\"".$name."\" alt=\"".$image1."\" src=\"".$image1."\"></a>";
else
$content.= "<a href=\"/torrents/images/".$image1."\" rel=\"group".$row["id"]."\"  title=\"<a href='details.php?id=".$row["id"]."'>".$name."</a> ".$tracker_lang['or']." <a href='download.php?id=".$row["id"]."'>".$tracker_lang['download']." ".$tracker_lang['name_torrent']."</a>\"><img height=\"120px\" title=\"".$name."\" alt=\"".$image1."\" src=\"/torrents/images/".$image1."\"></a>";

$content.= "</span>";
$content.= "</blockquote>";



$content.= "<blockquote class=\"content-ad-text\">";
$content.= "<blockquote style=\"position: relative;\">";

$combody_f = htmlspecialchars(preg_replace("/\[((\s|.)+?)\]/is", "", ($row["descr"])));

$list = explode("\n",$combody_f);

$array_list_1 = array();
$array_list_2 = array();

foreach ($list AS $lif){
$array_list_1[] = strlen($lif);
$array_list_2[] = $lif;
}

$newid = array_keys($array_list_1,max($array_list_1));
$newdesck = $array_list_2[$newid[0]];

if (stristr($newdesck,"/") && stristr($newdesck,":")){
unset($array_list_1[$newid[0]]);
$newid = array_keys($array_list_1,max($array_list_1));
$newdesck = $array_list_2[$newid[0]];
}

if (!empty($newdesck))
$combody_f = $newdesck;


$pos = strpos($combody_f, '.', 400); // $pos = 7, not 0
if (strlen($combody_f) >= 450)
$combody_f = substr($combody_f, 0, ($pos+1));

$content.= "<span style=\"position: absolute; bottom: 0px; left: 0px;\"><a title=\"".$tracker_lang['show_data']." ".$name."\" href=\"details.php?id=".$row["id"]."\" class=\"ad1-wordpress-link\">".$name."</a> [<a href=\"download.php?id=".$row["id"]."\" title=\"".$tracker_lang['download']." ".$name."\" class=\"ad1-wordpress-link\">".mksize($row["size"])."</a>]</span>";


$tags = array();
if (!empty($row["tags"])) {
foreach(explode(",", $row["tags"]) as $tag)
$tags[] = "<a style=\"font-weight:normal;\" href=\"browse.php?tag=".urlencode($tag)."\">".tolower($tag)."</a>";
}
$content.= "<span>".( count($tags) ? implode(",",$tags):$tracker_lang['no_choose'])."</span>";

$content.= "<p><small>".$combody_f."</small></p>";///[...]

$content.= "<br />";

$content.= "</blockquote>";
$content.= "</blockquote>";
$content.= "</div>";
}

$content.= "</div>";
$content.= "</div>";

$content.= "</div>";
	
$content.= "</div>";

$content.='<script type="text/javascript">jQuery(function() {var api = jQuery("div.scrollable").scrollable({vertical:true, size: 2}).mousewheel().circular().navigator({navi: "#menu-numbers",naviItem: \'a\',activeClass: \'current\',api : true})});</script>';

$content .= '<script type="text/javascript" src="js/daGallery.js"></script>';
$content .= '<script type="text/javascript">DaGallery.init();</script>';

if (mysql_num_rows_($res) == 0)
unset($content);

?>