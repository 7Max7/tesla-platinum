<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $tracker_lang;
$blocktitle = $tracker_lang['upload_groups'];


$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-groups", "time" => 60*60*24*3, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

$content = "";
$res = sql_query("SELECT id, name, comment FROM groups") or sqlerr(__FILE__, __LINE__);
while ($row = mysql_fetch_assoc($res)){
$content.= "<a class=\"menu\" title=\"".$tracker_lang['show_data'].": ".(!empty($row["comment"]) ? htmlspecialchars($row["comment"]):$tracker_lang['no_desck'])."\" href=\"browse.php?gr=".$row["id"]."\">".$row["name"]."</a>";
}

$filecache = file_query($content, $cache = array("type" => "disk", "file" => "block-groups", "time" => 60*60*24*3, "action" => "set"));
}

?>