<?php

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

if (!defined('BLOCK_FILE')) {
Header("Location: ../index.php");
exit;
}

global $tracker_lang;
$blocktitle = $tracker_lang['search_bots'];

?>
<script>
function bots_online() {
jQuery.post("block-online_jquery.php", {"action":"bot"}, function(response) {
jQuery("#bots_online").html(response);
}, "html");
setTimeout("bots_online();", 60000);
}
bots_online();
</script>
<?

$content = "<span align=\"center\" id=\"bots_online\">".$tracker_lang['loading']."</span>";

?>