<?
require "include/bittorrent.php";
dbconn(false);

loggedinorreturn();

if (get_user_class() < UC_SYSOP){
attacks_log($_SERVER["SCRIPT_FILENAME"]); 
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

accessadministration();

stdhead($tracker_lang['monitoring_sytem'],true);


$type = (isset($_GET["id"]) ? (int) $_GET["id"] : false);
$type_clear = (isset($_GET["type_clear"]) ? (int) $_GET["type_clear"] : false);
$del_clear = (isset($_GET["del_clear"]) ? (int) $_GET["del_clear"] : false);

if (!empty($type_clear))
$type = $type_clear;

$array_type = array();

$res2 = sql_query("SELECT id, username FROM users WHERE monitoring = 'yes' ORDER BY id") or sqlerr(__FILE__, __LINE__); 
while ($arr2 = mysql_fetch_assoc($res2)) {
$array_type[$arr2["id"]] = $arr2["username"];
}

if (!empty($del_clear) && isset($array_type[$del_clear])){

sql_query("UPDATE users SET monitoring = 'no' WHERE id = ".sqlesc($del_clear)) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$del_clear);

if (!headers_sent()){
@header ("Location: monitoring.php");
die;
} else
die ("<script>setTimeout('document.location.href=\"monitoring.php\"', 3);</script>");

}


if (!count($array_type)){
stderr($tracker_lang['error'], $tracker_lang['no_data']);
die();
}

$file = 'monitoring_'.$type.'.txt';

$root = ROOT_PATH."cache/".$file;

if (!empty($type_clear) && !empty($file))
@unlink($root);

if (!empty($file) && file_exists($root)){

$allsize = filesize($root);
$data = file_get_contents($root);

}

echo "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";

if (!empty($type) && !empty($allsize))
echo "<tr><td class=\"a\"><strong>".$tracker_lang['size']."</strong>: ".mksize($allsize)."</td><td class=\"a\"><strong>".$tracker_lang['username']."</strong>: <a href=\"userdetails.php?id=".$type."\">".$array_type[$type]."</a></td>";

echo "<tr><td class=\"b\" colspan=\"2\">";
echo "<div id=\"tabs\">\n";
foreach ($array_type AS $act => $text){
echo "<span onClick=\"document.location.href='monitoring.php?id=".$act."'\" class=\"tab ".($type == $act ? "active":"")."\"><nobr>".$text."</nobr></span>\n";
$last = $act;
}

if ((!empty($type) && !empty($type_clear)) || (!empty($type) && !empty($data)))
echo "<span onClick=\"document.location.href='monitoring.php?type_clear=".$type."'\" class=\"tab ".($type_clear == $type ? "active":"")."\"><nobr>".$tracker_lang['dropcache'].": ".$array_type[$type]."</nobr></span>";

if (!empty($type) && empty($del_clear))
echo "<span title=\"".$tracker_lang['monitoring_sytem'].": ".$tracker_lang['deny']."\" onClick=\"document.location.href='monitoring.php?del_clear=".$type."'\" class=\"tab\"><nobr>".$tracker_lang['reset'].": ".$array_type[$type]."</nobr></span>";


echo "</div>";
echo "</td></tr>";

echo "</table>";

echo  "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";

if (empty($data))
echo  "<tr><td class=\"b\" colspan=\"5\"><h3>".$tracker_lang['no_data']."</h3></td></tr>";
else {

echo "<tr>
<td class=\"a\" valign=\"top\" width=\"3%\">#</td>
<td class=\"a\" valign=\"top\">".$tracker_lang['description']."</td>
</tr>";

$exp = explode("\n\r", $data);

$line_num = 1;
foreach ($exp AS $datka){

list($text, $host, $ip, $hua, $time, $act) = explode('#', $datka);
    
$events = explode("||", $act);

$data_get = htmlspecialchars(print_r(unserialize($events[0]), true));
$data_post = htmlspecialchars(print_r(unserialize($events[1]), true));	

if (!empty($data_get))
$data_get = preg_replace('/Array\s\((.*?)\)/is', "<strong>(</strong>\\1<strong>)</strong>", $data_get);

if (!empty($data_post))
$data_post = preg_replace('/Array\s\((.*?)\)/is', "<strong>(</strong>\\1<strong>)</strong>", $data_post);

if (!empty($datka)){

echo "<td class=\"b\"># ".$line_num."</td>";

echo "<td class=\"b\">
<strong>".$tracker_lang['clock']."</strong>: ".$time."<br />
<strong>".$tracker_lang['user_ip']."</strong>: <a title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$ip."\">".$ip."</a><br />
".(!empty($text) ? "<strong>".$tracker_lang['action']."</strong>: <u>".$text."</u>: <br />":"")."
".(!empty($hua) ? "<strong>".$tracker_lang['tabs_agent']."</strong>: <a title=\"".$tracker_lang['search_added']."\" href=\"useragent.php?fme=".$hua."\">".htmlentities($hua)."</a> (".$tracker_lang['hash_sum'].": <a title=\"".$tracker_lang['search_added']."\" href=\"useragent.php?fme=".crc32($hua)."\">".crc32($hua)."</a>)<br />":"")."
<strong>".$tracker_lang['tabs_red']."</strong>: <a href=\"".$host."\">".$host."</a><br />
".(!empty($data_get) ? "<br /><b>GET</b>: ".$data_get:"")." ".(!empty($data_post) ?"<br /><b>POST</b>: ".$data_post:"")."
</td>";

echo "</tr>";

++$line_num;
}

}

}

echo  "</table>";

stdfoot(true);

die;
?>