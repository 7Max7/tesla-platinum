<?
require "include/bittorrent.php";
dbconn();
loggedinorreturn();

if (get_user_class() < UC_MODERATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]); 
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
}

if (isset($_POST["nowarned"]) && $_POST["nowarned"] == "nowarned"){

if (empty($_POST["usernw"]) && empty($_POST["desact"]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

if (isset($_POST["usernw"]) && is_array($_POST["usernw"])) {

$modcomment = date("Y-m-d")." - �������������� ���� ".$CURUSER['username'].".\n";

sql_query("UPDATE users SET warned = 'no', warneduntil = '0000-00-00 00:00:00', num_warned = '0', modcomment = CONCAT_WS('', ".sqlesc($modcomment).", modcomment) WHERE class < ".sqlesc($CURUSER["class"])." AND id IN (".implode(",", array_map('intval', $_POST['usernw'])).")") or sqlerr(__FILE__, __LINE__);
}

if (isset($_POST["desact"]) && is_array($_POST["desact"])){

$modcomment = date("Y-m-d")." - �������������� ���� � �������� ".$CURUSER['username'].".\n";

sql_query("UPDATE users SET warned = 'no', warneduntil = '0000-00-00 00:00:00', num_warned = '0', modcomment = CONCAT_WS('', ".sqlesc($modcomment).", modcomment), enabled = 'no' WHERE class < ".sqlesc($CURUSER["class"])." AND id IN (".implode(",", array_map('intval', $_POST['desact'])).")") or sqlerr(__FILE__, __LINE__);
}

header("Refresh: 0; url=warned.php");
die;

}

$warned = get_row_count("users", "WHERE warned = 'yes' AND class < ".sqlesc(get_user_class()));

if (empty($warned))
stderr($tracker_lang['error'], $tracker_lang['no_data_now']);

stdhead($tracker_lang['warned_users']);

$res = sql_query("SELECT * FROM users WHERE warned = 'yes' AND class < ".sqlesc(get_user_class())." ORDER BY warneduntil") or sqlerr(__FILE__, __LINE__);

echo "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"2\">";

if (get_user_class() >= UC_ADMINISTRATOR)
echo "<form action=\"warned.php\" method=\"post\">";

?>
<script language="Javascript" type="text/javascript">
jQuery(document).ready(function() {

jQuery("#usernw").click(function () {
if (!jQuery("#usernw").is(":checked"))
jQuery(".usernwo").removeAttr("checked");
else
jQuery(".usernwo").attr("checked","checked");
});

jQuery("#desact").click(function () {
if (!jQuery("#desact").is(":checked"))
jQuery(".desacto").removeAttr("checked");
else
jQuery(".desacto").attr("checked","checked");
});

}); 
</script>
<?

echo "<tr>
<td class=\"colhead\" align=\"left\">".$tracker_lang['signup_username']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['signup']." / ".$tracker_lang['last_login']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['uploaded']." / ".$tracker_lang['downloaded']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['clock']."</td>
".(get_user_class() >= UC_ADMINISTRATOR ? "<td class=\"colhead\" align=\"center\"><label><input type=\"checkbox\" id=\"usernw\" />".$tracker_lang['disselect']."</label> / <label>".$tracker_lang['disable']." <input type=\"checkbox\" id=\"desact\" /></label></td>":"")."
</tr>";

$num = 0;
while ($arr = mysql_fetch_assoc($res)){

$cl2 = 'class = "b"'; $cl1 = 'class = "a"';

if ($num % 2 == 1){
$cl1 = 'class = "b"';
$cl2 = 'class = "a"';
}

if ($arr['added'] == '0000-00-00 00:00:00')
$arr['added'] = '-';
if ($arr['last_access'] == '0000-00-00 00:00:00')
$arr['last_access'] = '-';

if ($arr["downloaded"] != 0)
$ratio = number_format($arr["uploaded"] / $arr["downloaded"], 3);
else
$ratio = "---";

echo "<tr>
<td align=\"left\" ".$cl1."><a href=\"userdetails.php?id=".$arr["id"]."\"><b>".get_user_class_color($arr["class"], $arr["username"])."</b></a><br />".get_user_icons($arr, true)."</td>
<td align=\"center\" ".$cl2.">".$arr['added']."<br />".$arr['last_access']."</td>
<td align=\"center\" ".$cl1.">".mksize($arr["uploaded"])." / ".mksize($arr["downloaded"])."<br /><font color=\"".get_ratio_color($ratio)."\">".$ratio."</font></td>
<td align=\"center\" ".$cl2.">".$arr["warneduntil"]."</td>
<td align=\"center\" ".$cl1."><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td align=\"center\" class=\"a\"><input type=\"checkbox\" class=\"usernwo\" name=\"usernw[]\" value=\"".$arr["id"]."\" /></td><td align=\"center\" class=\"b\"><input type=\"checkbox\" name=\"desact[]\" class=\"desacto\" value=\"".$arr["id"]."\" /></td></tr>
</table></td>

</tr>";

++$num;
}

if (get_user_class() >= UC_ADMINISTRATOR)
echo "<tr><td colspan=\"10\" align=\"right\" ".$cl2."><input type=\"submit\" class=\"btn\" name=\"submit\" value=\"".$tracker_lang['b_action']."\"></td></tr><input type=\"hidden\" name=\"nowarned\" value=\"nowarned\"></form>";

echo "</table>";

stdfoot();
?>