<?
require "include/bittorrent.php";
dbconn();
loggedinorreturn();

if (get_user_class() < UC_ADMINISTRATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$hiderating = get_row_count("users", "WHERE hiderating = 'yes'");

if (empty($hiderating))
stderr($tracker_lang['error'], $tracker_lang['no_data_now']);
else {

stdhead($tracker_lang['delete_rating'], true);

begin_frame($tracker_lang['delete_rating'].": ".$hiderating, true);
begin_table();

$res = sql_query("SELECT * FROM users WHERE hiderating='yes' ORDER BY id") or sqlerr(__FILE__, __LINE__);

$num = mysql_num_rows($res);
echo ("<table cellpadding=\"4\" cellspacing=\"1\" border=\"0\" width=\"100%\">\n");

echo ("<tr>
<td class=\"colhead\" align=\"center\" width=\"15%\">".$tracker_lang['username']."</td>
<td class=\"colhead\" align=\"center\" width=\"10%\">".$tracker_lang['registered']."</td>
<td class=\"colhead\" align=\"center\" width=\"15%\">".$tracker_lang['last_login']."</td>
<td class=\"colhead\" align=\"center\" width=\"15%\">".$tracker_lang['class']."</td>
<td class=\"colhead\" align=\"center\" width=\"10%\">".$tracker_lang['downloaded']."</td>
<td class=\"colhead\" align=\"center\" width=\"10%\">".$tracker_lang['uploaded']."</td>
<td class=\"colhead\" align=\"center\" width=\"10%\">".$tracker_lang['ratio']."</td>
<td class=\"colhead\" align=\"center\" width=\"15%\">".$tracker_lang['clock']."</td>
</tr>\n");

while ($arr = mysql_fetch_assoc($res)){

if ($arr['added'] == '0000-00-00 00:00:00')
$arr['added'] = '-';
if ($arr['last_access'] == '0000-00-00 00:00:00')
$arr['last_access'] = '-';

if ($arr["downloaded"] != 0)
$ratio = number_format($arr["uploaded"] / $arr["downloaded"], 3);
else
$ratio = "---";

echo ("<tr>
<td align=\"center\" class=\"b\"><a href=\"userdetails.php?id=".$arr["id"]."\"><b>".get_user_class_color($arr["class"], $arr["username"])."</b></a> ".($arr["donor"] =="yes" ? "<img src=\"/pic/star.gif\" border=\"0\" title=\"".$tracker_lang['donor']."\" alt=\"".$tracker_lang['donor']."\" />" : "")."</td>
<td align=\"center\" class=\"b\">".$arr['added']."</td>
<td align=\"center\" class=\"b\">".$arr['last_access']."</td>
<td align=\"center\" class=\"b\">".get_user_class_name($arr["class"])."</td>
<td align=\"center\" class=\"b\">".mksize($arr["downloaded"])."</td>
<td align=\"center\" class=\"b\">".mksize($arr["uploaded"])."</td>
<td align=\"center\" class=\"b\"><font color=\"".get_ratio_color($ratio)."\">".$ratio."</font></td>");

if ($arr['hideratinguntil'] == '0000-00-00 00:00:00')
echo ("<td align=\"center\" class=\"b\">".$tracker_lang['unlimited_time']."</td>\n");
else
echo ("<td align=\"center\" class=\"b\">".$arr['hideratinguntil']."</td></tr>\n");

}
echo ("</table>\n");

}

stdfoot(true);
?>