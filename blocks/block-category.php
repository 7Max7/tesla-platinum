<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $tracker_lang;
$blocktitle = $tracker_lang['fast_category'];


$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-category", "time" => 60*60*24*3, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

$content = "";

$cats = genrelist();

foreach ($cats as $row)
$content.= "<a class=\"menu\" title=\"".$tracker_lang['show_data']."\" href=\"browse.php?cat=".$row["id"]."\">".$row["name"]." </a>";

$filecache = file_query($content, $cache = array("type" => "disk", "file" => "block-category", "time" => 60*60*24*3, "action" => "set"));
}

?>