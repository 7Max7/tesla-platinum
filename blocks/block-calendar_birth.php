<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $tracker_lang;
$blocktitle = $tracker_lang['calendar_birthday'];

?>
<style>
.cl1 {border:solid 1px #00CCFF; text-align:center}
.cl3hov {background-color:#FF6633; border-color:#FF531A; text-align:center}
.cl2 {background-color:#CCFFCC; color:#00B32D; text-align:center}
</style>
<?

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-calendar_birth", "time" => 60*60*24*7, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

global $tracker_lang; 

$array = array();
$timeday = date("-m-d");
$res_ = sql_query("SELECT DATE_FORMAT(birthday, '-%m-%d') AS date FROM users WHERE birthday <> '0000-00-00' GROUP BY date") or sqlerr(__FILE__, __LINE__);

while ($arr_ = mysql_fetch_assoc($res_)){

$res = sql_query("SELECT COUNT(*) AS xount FROM users WHERE birthday LIKE '%".$arr_["date"]."%'") or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res);

$array[$arr_["date"]] = $arr["xount"];

}

list($today, $mounth, $year) = explode("-", date("j-m-Y")); 

$day = mktime(0, 0, 0, $mounth, 1, $year); 
$dayofweek = date("w", $day); 
$back = ($dayofweek + 6) % 7; 
$day -= 86400 * $back; 
$day_tmp = $day; 

$day_name = array("Mon" => $tracker_lang['Monday'], "Tue" => $tracker_lang['Tuesday'], "Wed" => $tracker_lang['Wednesday'], "Thu" => $tracker_lang['Thursday'], "Fri" => $tracker_lang['Friday'], "Sat" => $tracker_lang['Saturday'], "Sun" => $tracker_lang['Sunday']);

$content = '<table align="center" border="0" cellpadding="3" cellspacing="0" width="100%"> 
<tr>'; 

for ($i=0; $i<7; $i++) {
$content.= '<td class="colhead">'.$day_name[date('D', $day_tmp)].'</td>'; 
$day_tmp += 86400; 
}

$content.= '</tr>'; 

while (true) {

$content.= '<tr>';

for ($i=0; $i<7; $i++) {

$date = date('j', $day);
$mounth_tmp = date("m", $day);

$mounfor = date('-m-d', $day);

$content.= '<td';

if (isset($array[date('-m-d', $day)]))
$content.= ' class="cl3hov" title="'.$tracker_lang['birth_onday'].': '.$array[date('-m-d', $day)].'"';

elseif ($mounth <> $mounth_tmp)
$content.= ' class="cl2"';
else
$content.= ' class="cl1"';

$content.= '>'.$date.'</td>';

$day += 86400;

}

$content.= '</tr>'; 

if ($mounth <> $mounth_tmp) break; 
}


$content.= '</table>';

//60*60*24
$filecache = file_query($content, $cache = array("type" => "disk", "file" => "block-calendar_birth", "time" => 60*60*24*7, "action" => "set"));
}

?> 