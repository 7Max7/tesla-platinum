<?

if (!defined('BLOCK_FILE')) {   
 Header("Location: ../index.php");   
 exit;   
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $CURUSER, $tracker_lang;

$blocktitle = $tracker_lang['news'].(get_user_class() >= UC_ADMINISTRATOR ? " - [<a class=\"altlink_white\" href=\"news.php\"><b>".$tracker_lang['create']."</b></a>]" : "")." ".($CURUSER ? " - [<a class=\"altlink_white\" href=\"newsarchive.php\"><b>".$tracker_lang['news_olders']."</b></a>]":"");


$filecache = file_query("", $cache = array("type" => "disk", "file" => "block_news_light", "time" => 60*60, "action" => "get"));

if ($filecache <> false) {
$content = $filecache; 
} else {

$content = "<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" class=\"main\">"; 

$res = sql_query("SELECT * FROM news ORDER BY added DESC LIMIT 6") or sqlerr(__FILE__, __LINE__); /// 48 �����

$num = 0;
while ($row = mysql_fetch_assoc($res)){

list ($data,$time) = explode(" ", $row["added"]);

$content.= "<tr>
<td><div style=\"background: none repeat scroll 0 0 #F2F2F2;border: 1px solid #B4B4B4;color: #333333;float: right;font-family: 'Courier New',Courier,monospace; padding: 1px 8px; white-space: nowrap;\">".$data."</div></td>
<td width=\"100%\" style=\"border: 0 none;\"><div style=\"background: url('./pic/gradient.jpg') no-repeat scroll left top transparent; margin-bottom: 2px; padding: 3px 8px 2px;\">
<a title=\"".$row["added"]."\" ".($row["added"] < get_date_time(gmtime() - 60*60*24*7) ? "style=\"color: #000000;text-decoration: none;\"":"style=\"color: #A52A2A;font-weight: bold;\"")." href=\"newsoverview.php?id=".$row["id"]."\">".htmlspecialchars_uni($row["subject"])."</a></div></td>
</tr>"; 

++$num;
}

if ($num == 0)
$content = "<tr><td class=\"b\" colspan=\"2\">".$tracker_lang['no_news']."</td></tr>";

$content.= "</table>";

file_query($content, $cache = array("type" => "disk", "file" => "block_news_light", "time" => 60*60, "action" => "set"));
}

?>