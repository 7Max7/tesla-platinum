<?
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

parked();

if (isset($_POST["delete"]) && $_POST["delete"] == "allchecks"){

sql_query("DELETE FROM checkcomm WHERE userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);

header("Location: checkcomm.php");
die;
}

$count = get_row_count("checkcomm", "WHERE userid = ".sqlesc($CURUSER["id"]));

stdhead($tracker_lang['monitor']." / ".$tracker_lang['monitor_comments']);

echo "<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

$perpage = 50;
list ($pagertop, $pagerbottom, $limit) = pager($perpage, $count, "checkcomm.php?");

echo "<tr><td class=\"colhead\" align=\"center\" colspan=\"12\">".$tracker_lang['monitor']." / ".$tracker_lang['monitor_comments']." - <a class=\"altlink_white\" href=\"bookmarks.php\">".$tracker_lang['bookmarks']."</a></td></tr>";

if (empty($count)){

echo "<tr><td class=\"b\" align=\"center\" colspan=\"12\">".$tracker_lang['no_data_now']."</td></tr>";
echo "</table>";

stdfoot();
die;
}

if ($count > $perpage)
echo "<tr><td class=\"index\" colspan=\"12\">".$pagertop."</td></tr>";

echo "<tr>
<td width=\"2%\" class=\"colhead\">#</td>
<td width=\"2%\" align=\"center\" class=\"colhead\">U/R</td>
<td width=\"50%\" class=\"colhead\">".$tracker_lang['name']." / ".$tracker_lang['type']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['forum_last_comments']."</td>
</tr>";

$res = sql_query("SELECT ch.*, off.name AS chname, t.name AS tname, com.user AS o_user, com.added AS o_added
FROM checkcomm AS ch
LEFT JOIN off_reqs AS off ON off.id = ch.checkid
LEFT JOIN torrents AS t ON t.id = ch.checkid
LEFT JOIN comments AS com ON (com.offer = ch.checkid AND com.torrent = '0') OR (com.offer = '0' AND com.torrent = ch.checkid)
WHERE ch.userid = ".sqlesc($CURUSER["id"])." ORDER BY ch.id DESC ".$limit) or sqlerr(__FILE__, __LINE__);

$i2 = 0;

while ($arr = mysql_fetch_assoc($res)) {

if ($i2%2==1){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

//print_r($arr); echo "<br />";

if (empty($arr["o_user"]))
$lastcommentd = "<center><i>".$tracker_lang['no_comments']."</i></center>";
else {

$res2 = sql_query("SELECT username, class FROM users WHERE id = ".sqlesc($arr["o_user"])) or sqlerr(__FILE__, __LINE__);
$row2 = mysql_fetch_assoc($res2);

$lastcommentd = $tracker_lang['news_poster'].": ".(!empty($row2["username"]) ? "<a class=\"index\" href=\"userdetails.php?id=".$arr["o_user"]."\">".get_user_class_color($row2["class"] ,$row2["username"])."</a>":"<b>".$tracker_lang['anonymous']."</b>")." &nbsp;";

if ($arr["offer"] == 1)
$lastcommentd.= "<a title=\"".$tracker_lang['subscribe_last_comment']."\" class=\"index\" href=\"detailsoff.php?id=".$arr['checkid']."&tocomm=1\"><img border=\"0\" src=\"pic/pm.gif\" /></a>";
else
$lastcommentd.= "<a title=\"".$tracker_lang['subscribe_last_comment']."\" class=\"index\" href=\"details.php?id=".$arr['checkid']."&tocomm=1\"><img border=\"0\" src=\"pic/pm.gif\" /></a>";

$lastcommentd.= "</br>".$arr["o_added"]; 

}

echo "<tr>";
echo "<td ".$cl1." width=\"2%\">".$arr['id']."</td>";

if (empty($arr["o_user"]))
echo "<td ".$cl2." width=\"2%\" align=\"center\"><img src=\"pic/pn_inbox.gif\" border=\"0\" /></td>";
else
echo "<td ".$cl2." width=\"2%\" align=\"center\"><img src=\"pic/pn_inboxnew.gif\" border=\"0\" /></td>";

echo "<td ".$cl1." width=\"50%\">";

if ($arr["offer"] == 1){

echo (!empty($arr["chname"]) ? "<a href=\"detailsoff.php?id=".$arr['checkid']."\">".$arr["chname"]."</a>":$tracker_lang['torrent_del_or_move']);
echo "<br /><a title=\"".$tracker_lang['requests_section']."\" href=\"detailsoff.php\">".$tracker_lang['requests_section']."</a> / <a title=\"".$tracker_lang['monitor_comments_disable']."\" class=\"index\" href=\"commentoff.php?action=checkoff&tid=".$arr['checkid']."\">".$tracker_lang['monitor_comments_disable']."</a>";

} elseif ($arr["torrent"] == 1){

echo (!empty($arr["tname"]) ? "<a href=\"details.php?id=".$arr['checkid']."\">".$arr["tname"]."</a>":$tracker_lang['offer_del_or_move']);
echo "<br /><a title=\"".$tracker_lang['browse']."\" href=\"browse.php\">".$tracker_lang['browse']."</a> / <a title=\"".$tracker_lang['monitor_comments_disable']."\" class=\"index\" href=\"comment.php?action=checkoff&tid=".$arr['checkid']."\">".$tracker_lang['monitor_comments_disable']."</a>";

}

echo "</td>";

echo "<td ".$cl2." align=\"center\">".$lastcommentd."</td>";
echo "</tr>";


++$i2;
}

if ($count > $perpage)
echo "<tr><td ".$cl1." class=\"index\" colspan=\"12\">".$pagerbottom."</td></tr>";

if (!empty($count)){
echo "<tr><td class=\"b\" class=\"index\" colspan=\"12\" align=\"center\">
<form method=\"post\" action=\"checkcomm.php\"><input type=\"hidden\" name=\"delete\" value=\"allchecks\" /> <input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['trunc_monit']."\" /></form>
</td></tr>";
}

echo "</table>";

stdfoot();
?>