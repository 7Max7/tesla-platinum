<?
if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

global $CURUSER, $tracker_lang;

$blocktitle = $tracker_lang['my_language'];

function lang_array ($lang = false){

global $default_language;

$array = array();


$filecache = file_query("", $cache = array("type" => "disk", "file" => "block_lang", "time" => 60*10, "action" => "get"));

if ($filecache <> false) {
$array = unserialize($filecache);
} else {

$dh = opendir(ROOT_PATH.'/languages/');
while ($file = readdir($dh)) :

if (preg_match('/^lang_([a-zA-Z]{4,25})$/is', $file, $langi)){

$file = (string) $langi[1];

if (!empty($file)) $array[] = $file;

}

endwhile;
closedir($dh);

$filecache = file_query(serialize($array), $cache = array("type" => "disk", "file" => "block_lang", "time" => 60*10, "action" => "set"));
}

if (!count($array))
$array[] = $default_language;

return $array;

}


if ($CURUSER){

$my_lng = $CURUSER["language"];

if (isset($_POST["language"]) && !empty($_POST["language"])){

$pos_lang = (string) $_POST["language"];
$array_lang = lang_array();

if (in_array($pos_lang, $array_lang)){
sql_query("UPDATE users SET language = ".sqlesc($pos_lang)." WHERE id = ".$CURUSER["id"]) or sqlerr(__FILE__,__LINE__);
echo "<script>setTimeout('document.location.href=\"index.php\"', 1);</script>";
die;
}
}

}


$content = '';
$content.= "<form method=\"post\" action=\"index.php\"><table align=\"center\" width=\"100%\" valign=\"top\" cellpadding=\"0\">";
$content.= "<tr><td align=\"center\" class=\"a\">".$tracker_lang['my_language'].":</td></tr>";

$content.= "<tr><td align=\"center\" class=\"b\">";
$content.= "<select name=\"language\">";

$content.= "<option value=\"0\">".$tracker_lang['no_choose']."</option>\n";

$array_lang = lang_array();
foreach ($array_lang AS $file){
$content.= "<option ".($my_lng == $file ? "selected":"")." value=\"".$file."\">".$file."</option>\n";
}

$content.= "</select><br />";

$content.= "<tr><td align=\"center\" class=\"a\"><input class=\"btn\" type=\"submit\" style=\"width: 100px\" value=\"".$tracker_lang["confirmation"]."\" /></td></tr>";
$content.= "</td></tr></table></form>";












?>