<?
require_once("include/bittorrent.php");
dbconn(false);

$action = htmlentities($_GET["action"]);

/// ��������������� �������� ///
if (isset($_POST['area']) && $action == 'preview') {
$area = base64_decode($_POST['area']);
header("Content-Type: text/html; charset=".$tracker_lang['language_charset']);
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

if (!empty($area)){

echo "<table cellpadding='2' width='100%' border='0' cellspacing='0'>";
echo "<tr><td class=\"colhead\"> </td></tr>";
echo "<tr><td class=\"b\">".format_comment(htmlspecialchars_uni($area), true)."</td></tr>";
echo "<tr><td class=\"colhead\"> </td></tr>";
echo "</table>";

}

die;
}
/// ��������������� �������� ///

loggedinorreturn();
parked();

if ($CURUSER["commentpos"] == 'no')
stderr($tracker_lang['error_data'], $tracker_lang['commentpos_no']);

if ($action == "add") {

$res = sql_query("SELECT COUNT(*) FROM comments WHERE user = ".sqlesc($CURUSER["id"])." AND added > ".sqlesc(get_date_time(gmtime() - 60))) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_row($res);

if ($row[0] > 2)
stderr("�������� ������", "���������� ������ 2 ��������� �� 1 ������.");


if ($_SERVER["REQUEST_METHOD"] == "POST") {

$torrentid = (int) $_POST["tid"];
if (!is_valid_id($torrentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT name, comment_lock FROM torrents WHERE id = ".sqlesc($torrentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);

if ($arr["comment_lock"] == 'yes')
stderr($tracker_lang['error'], $tracker_lang['comment_lock']);

if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['no_torrent_with_such_id']);

$name = $arr["name"];

$text = htmlspecialchars_uni($_POST["text"]);

if (empty($text))
stderr($tracker_lang['error'], $tracker_lang['comment_cant_be_empty']);

if (get_user_class() == UC_SYSOP)
$sender_id = ($_POST['sender'] == 'system' ? 0 : $CURUSER['id']);
else
$sender_id = $CURUSER['id'];

sql_query("INSERT INTO comments (user, torrent, added, text, ip) VALUES (".sqlesc($sender_id).", ".sqlesc($torrentid).", ".sqlesc(get_date_time()).", ".sqlesc($text).", ".sqlesc(getip()).")");

$newid = mysql_insert_id();
sql_query("UPDATE torrents SET comments = comments + 1 WHERE id = ".sqlesc($torrentid)) or sqlerr(__FILE__,__LINE__);


/////////////////�������� �� ����������/////////////////
$res3 = mysql_query("SELECT * FROM checkcomm WHERE checkid =".sqlesc($torrentid)." AND torrent = '1'") or sqlerr(__FILE__,__LINE__);
while ($arr3 = mysql_fetch_array($res3)) {

$name = htmlspecialchars_uni($arr["name"]);

$subject = "����� ������� � ".$name;

if (get_user_class() == UC_SYSOP){
$sender_name = ($_POST['sender'] == 'system' ? "[color=gray][[b]System[/b]][/color]" : "[color=#".get_user_rgbcolor($CURUSER["class"], $CURUSER["username"])."]".$CURUSER["username"]."[/color]");
}
else
$sender_name = "[color=#".get_user_rgbcolor($CURUSER["class"], $CURUSER["username"])."]".$CURUSER["username"]."[/color]";

$msg = "��� �������� [url=".$DEFAULTBASEURL."/details.php?id=".$torrentid."&viewcomm=".$newid."#comm".$newid."]".$name."[/url] ".$sender_name." ������� �����������:[hr] ".$text." [hr]";

if ($_POST['sender'] == 'system')
write_log("C�������� �� system - ".$CURUSER["username"]." � ".$name, "808080", "comment");

if ($CURUSER["id"] <> $arr3["userid"])
sql_query("INSERT INTO messages (sender, receiver, added, subject, msg, poster) VALUES (0, ".sqlesc($arr3["userid"]).", ".sqlesc(get_date_time()).", ".sqlesc($subject).",".sqlesc($msg).", 0)")or sqlerr(__FILE__,__LINE__);

}
/////////////////�������� �� ����������/////////////////

unsql_cache("block-comment");

header("Refresh: 0; url=details.php?id=".$torrentid."&viewcomm=".$newid."#comm".$newid);
die;
}

$torrentid = (int) $_GET["tid"];
if (!is_valid_id($torrentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT name FROM torrents WHERE id = ".sqlesc($torrentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['no_torrent_with_such_id']);

stdhead($tracker_lang['torrents'].": ".$tracker_lang['commenting']);

begin_frame($tracker_lang['commentpos'], true);

echo("<form name=\"comment\" method=\"post\" action=\"comment.php?action=add\">\n");
echo("<input type=\"hidden\" name=\"tid\" value=\"".$torrentid."\"/>\n");

echo "<table class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";
echo "<tr><td class=\"colhead\">".$tracker_lang['add_comment']."</td></tr>";
echo "<tr><td align=\"center\">
".textbbcode("comment", "text", "")."
<input type=\"submit\" name=\"post\" class=\"btn\" title=\"CTRL+ENTER ".$tracker_lang['add_comment']."\" value=\"".$tracker_lang['add_comment']."\" />
</td></tr>";
echo "</table>";

echo("</form>\n");

$res = sql_query("SELECT comments.id, text, comments.ip, comments.added, username, title, class, users.id as user, users.avatar, users.donor, users.enabled, users.warned, users.parked 
FROM comments 
LEFT JOIN users ON comments.user = users.id WHERE torrent = ".sqlesc($torrentid)." 
ORDER BY comments.id DESC LIMIT 5");

$allrows = array();
while ($row = mysql_fetch_array($res))
$allrows[] = $row;

if (count($allrows))
commenttable($allrows);

stdfoot();
die;

} elseif ($action == "quote") {

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT c.*, t.name, t.id AS tid, u.username 
FROM comments AS c 
LEFT JOIN torrents AS t ON c.torrent = t.id 
JOIN users AS u ON c.user = u.id WHERE c.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['torrents'].": ".$tracker_lang['commenting']);

begin_frame($tracker_lang['commentpos'], true);

$text = "[quote=".$arr["username"]."]".$arr["text"]."[/quote]\n";

echo("<form method=\"post\" name=\"comment\" action=\"comment.php?action=add\">\n");
echo("<input type=\"hidden\" name=\"tid\" value=\"".$arr["tid"]."\" />\n");

echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
echo "<tr><td class=\"colhead\">".$tracker_lang['add_comment']."</td></tr>";

echo "<tr><td align=\"center\">
".textbbcode("comment","text",htmlspecialchars_uni($text))."
<input type=\"submit\" class=\"btn\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['add_comment']."\" value=\"".$tracker_lang['add_comment']."\" />
</td></tr>";

echo "</table>";
echo("</form>\n");

stdfoot();
die;

} elseif ($action == "edit") {

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT c.*, t.name, t.id AS tid 
FROM comments AS c 
LEFT JOIN torrents AS t ON c.torrent = t.id WHERE c.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if ($arr["user"] <> $CURUSER["id"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$text = htmlspecialchars_uni($_POST["text"]);

$returnto = htmlspecialchars($_POST["returnto"]);

if (empty($text))
stderr($tracker_lang['error'], $tracker_lang['comment_cant_be_empty']);

sql_query("UPDATE comments SET text = ".sqlesc($text).", ".(empty($arr["ori_text"]) ? "ori_text = ".sqlesc($arr["text"]).", ":"")." editedat = ".sqlesc(get_date_time()).", editedby = ".sqlesc($CURUSER["id"])." WHERE id = ".sqlesc($commentid)) or sqlerr(__FILE__, __LINE__);

if ($returnto)
header("Location: ".$returnto);
else
header("Location: ".$DEFAULTBASEURL);
die;
}

stdhead($tracker_lang['news'].": ".$tracker_lang['editing']);

begin_frame($tracker_lang['editing'], true);

echo("<form method=\"post\" name=\"comment\" action=\"comment.php?action=edit&cid=".$commentid."\">\n");
echo("<input type=\"hidden\" name=\"returnto\" value=\"details.php?id=".$arr["tid"]."&viewcomm=".$commentid."#comm".$commentid."\" />\n");
echo("<input type=\"hidden\" name=\"cid\" value=\"".$commentid."\" />\n");

echo "<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
echo "<tr><td class=\"colhead\">".$tracker_lang['news'].": ".$tracker_lang['editing']."</td></tr>";

echo "<tr><td align=\"center\">
".textbbcode("comment","text",htmlspecialchars($arr["text"]))."
<input type=\"submit\" name=\"post\" class=\"btn\" title=\"CTRL+ENTER ".$tracker_lang['edit']."\" value=\"".$tracker_lang['edit']."\" />
</td></tr>";

echo "</table>";
echo("</form>\n");

stdfoot();
die;

} elseif ($action == "delete") {

if (get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$commentid = (int) $_GET["cid"];

if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if (!isset($_GET["sure"]))
stderr($tracker_lang['delete']." ".$tracker_lang['comment'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $tracker_lang['comment'], "comment.php?action=delete&cid=".$commentid."&sure=1"));

$res = sql_query("SELECT user, torrent, (SELECT username FROM users WHERE id = comments.user) AS classusername, (SELECT name FROM torrents WHERE id = comments.torrent) AS name_torrent FROM comments WHERE id = ".sqlesc($commentid))  or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if ($arr)
$torrentid = $arr["torrent"];

write_log($CURUSER["username"]." ������ ����������� ".$arr["classusername"]." (".$commentid.") � �������� '".$arr["name_torrent"]."' (".$torrentid.")", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "comment");

sql_query("DELETE FROM comments WHERE id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);

unsql_cache("block-comment");

if ($torrentid && mysql_affected_rows() > 0)
sql_query("UPDATE torrents SET comments = comments - 1 WHERE id = ".sqlesc($torrentid));

list($commentid) = mysql_fetch_row(sql_query("SELECT id FROM comments WHERE torrent = ".sqlesc($torrentid)." ORDER BY added DESC LIMIT 1"));

if ($torrentid && $commentid)
$returnto = "details.php?id=".$torrentid."&viewcomm=".$commentid."#comm".$commentid;
elseif ($torrentid && !$commentid) 
$returnto = "details.php?id=".$torrentid;

if ($returnto)
header("Location: ".$returnto);
else
header("Location: ".$DEFAULTBASEURL);
die;

} elseif ($action == "check" || $action == "checkoff") {

$tid = (int) $_GET["tid"];
if (!is_valid_id($tid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);;

$docheck = mysql_fetch_array(sql_query("SELECT COUNT(*) FROM checkcomm WHERE checkid = ".sqlesc($tid )." AND userid = ".sqlesc($CURUSER["id"])." AND torrent = '1'"));

$from = $_SERVER["HTTP_REFERER"];
$host = $_SERVER["REQUEST_URI"];

if ($docheck[0] > 0 && $action == "check") {

if ($from)
@header("Refresh: 3; url=".$from);
else 
@header("Refresh: 3; url=details.php?id=".$tid);

stderr($tracker_lang['error_data'], "<p>".$tracker_lang['enable_funcs']." - ".$tracker_lang['monitor_comments'].".</p><a href=\"".$from."\">".$tracker_lang['back_inlink']."</a> ".$tracker_lang['or']." <a href=\"details.php?id=".$tid."\">".$tracker_lang['torrent']."</a>");

}
if ($action == "check") {
sql_query("INSERT INTO checkcomm (checkid, userid, torrent) VALUES (".sqlesc($tid).", ".sqlesc($CURUSER["id"]).", 1)") or sqlerr(__FILE__,__LINE__);

if ($from)
@header("Refresh: 3; url=".$from);
else 
@header("Refresh: 3; url=details.php?id=".$tid);

stderr($tracker_lang['error_data'], "<p>".$tracker_lang['disable_funcs']." - ".$tracker_lang['monitor_comments_disable'].".</p><a href=\"".$DEFAULTBASEURL."/details.php?id=".$tid."\">".$tracker_lang['back_inlink']."</a>");

} else {

sql_query("DELETE FROM checkcomm WHERE checkid = ".sqlesc($tid)." AND userid = ".sqlesc($CURUSER["id"])." AND torrent = 1") or sqlerr(__FILE__,__LINE__);

if ($from)
@header("Refresh: 3; url=".$from);
else 
@header("Refresh: 3; url=details.php?id=".$tid);

stderr($tracker_lang['error_data'], "<p>".$tracker_lang['disable_funcs']." - ".$tracker_lang['monitor_comments_disable'].".</p><a href=\"".$DEFAULTBASEURL."/details.php?id=".$tid."\">".$tracker_lang['back_inlink']."</a>");

}

} elseif ($action == "vieworiginal" && get_user_class() > UC_MODERATOR) {

$commentid = (int) $_GET["cid"];

if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT c.*, t.name, t.id AS tid 
FROM comments AS c 
LEFT JOIN torrents AS t ON c.torrent = t.id WHERE c.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);

if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['news'].": ".$tracker_lang['message_view']);

if (empty($arr["ori_text"]))
$arr["ori_text"] = $arr["text"];

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"a\">".$tracker_lang['original_com'].": ".$commentid."</td></tr>";
echo "<tr><td class=\"b\">".format_comment($arr["ori_text"], true)."</td></tr>";
echo "<tr><td class=\"a\"><a href=".$DEFAULTBASEURL."/details.php?id=".$arr["tid"]."&viewcomm=".$commentid."#comm".$commentid.">".$tracker_lang['back_inlink']."</a></td></tr>";

echo "</table>";

stdfoot();
die;

} else
stderr($tracker_lang['error'], $tracker_lang['no_data']);

?>