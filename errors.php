<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

if (get_user_class() <= UC_MODERATOR){
attacks_log($_SERVER["SCRIPT_FILENAME"]); 
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

accessadministration();


$type = (isset($_GET["type"]) ? (string) $_GET["type"] : false);
$type_clear = (isset($_GET["type_clear"]) ? (string) $_GET["type_clear"] : false);



if ($type == "all" && isset($_GET['d'])){

unsql_cache(array('hacklog', 'info_cache_stat', 'sqlerror', 'error_torrent', 'fix_lang', "block-class_".$CURUSER["class"]));

write_log("������������ ".$CURUSER['username']." ������� ������ ������ ����� ������.", "6A676A", "error");

$returlink = $_SERVER["HTTP_REFERER"];
$site = parse_url($returlink, PHP_URL_HOST);

if (empty($site) && empty($returlink))
$returlink = $DEFAULTBASEURL;

if (!headers_sent()) header("Location: ".$returlink);
else echo ("<script>setTimeout('document.location.href=\"".$returlink."\"', 10);</script>");

die;
}



stdhead($tracker_lang['errors'], true);


if (!empty($type_clear)) $type = $type_clear;

$array_type = array();

$array_type['xss'] = $tracker_lang['err_xss'];
$array_type['info'] = $tracker_lang['err_info'];
$array_type['sql'] = $tracker_lang['err_sql'];
$array_type['bit'] = $tracker_lang['err_bit'];
$array_type['lang'] = $tracker_lang['err_lang'];

if ($type == 'xss')
$file = 'hacklog.txt';
elseif ($type == 'info')
$file = 'info_cache_stat.txt';
elseif ($type == 'sql')
$file = 'sqlerror.txt';
elseif ($type == 'bit')
$file = 'error_torrent.txt';
elseif ($type == 'lang')
$file = 'fix_lang.txt';


$root = ROOT_PATH."cache/".$file;

if (!empty($type_clear) && !empty($file)){

@unlink($root);

unsql_cache("block-class_".$CURUSER["class"]); // ������� ��� �� ���� ����� �������������
}

if (!empty($file) && file_exists($root)){

$allsize = filesize($root);
$data = file_get_contents($root);

}

echo "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";

if (!empty($type) && !empty($allsize))
echo "<tr><td class=\"a\"><strong>".$tracker_lang['size']."</strong>: ".mksize($allsize)." (".$type.")</td>";

echo "<tr><td class=\"b\" colspan=\"2\">";
echo "<div id=\"tabs\">\n";
foreach ($array_type AS $act => $text){
echo "<span onClick=\"document.location.href='errors.php?type=".$act."'\" class=\"tab ".($type == $act ? "active":"")."\"><nobr>".$text."</nobr></span>\n";
$last = $act;
}

if ((!empty($type) && !empty($type_clear)) || (!empty($type) && !empty($data)))
echo "<span onClick=\"document.location.href='errors.php?type_clear=".$type."'\" class=\"tab ".($type_clear == $type ? "active":"")."\"><nobr>".$tracker_lang['dropcache'].": ".$array_type[$type]."</nobr></span>";

echo "</div>";
echo "</td></tr>";

echo "</table>";

echo  "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";

if (empty($data))
echo  "<tr><td class=\"b\" colspan=\"5\"><h3>".$tracker_lang['no_data']."</h3></td></tr>";

elseif ($type == 'xss' && !empty($data)){

echo "<tr>
<td class=\"a\" valign=\"top\" width=\"3%\">#</td>
<td class=\"a\" valign=\"top\">".$tracker_lang['description']."</td>
</tr>";

$exp = explode("\n\r", $data);

$line_num = 1;
foreach ($exp AS $datka){

list($time, $ip, $hua, $from, $host, $evant) = explode('#', $datka);
    
$events = explode("||", $evant);

$data_get = htmlspecialchars(print_r(unserialize($events[0]), true));
$data_post = htmlspecialchars(print_r(unserialize($events[1]), true));	

if (!empty($data_get))
$data_get = preg_replace('/Array\s\((.*?)\)/is', "<strong>(</strong>\\1<strong>)</strong>", $data_get);

if (!empty($data_post))
$data_post = preg_replace('/Array\s\((.*?)\)/is', "<strong>(</strong>\\1<strong>)</strong>", $data_post);

if (!empty($datka)){

echo "<td class=\"b\"># ".$line_num."</td>";

echo "<td class=\"b\">
<strong>".$tracker_lang['clock']."</strong>: ".$time."<br />
<strong>".$tracker_lang['user_ip']."</strong>: <a title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$ip."\">".$ip."</a><br />
<strong>".$tracker_lang['tabs_agent']."</strong>: <a title=\"".$tracker_lang['search_added']."\" href=\"useragent.php?fme=".$hua."\">".htmlentities($hua)."</a> (".$tracker_lang['hash_sum'].": <a title=\"".$tracker_lang['search_added']."\" href=\"useragent.php?fme=".crc32($hua)."\">".crc32($hua)."</a>)<br />
<strong>".$tracker_lang['refer_site']."</strong>: ".$from."<br />
<br /><strong>".$tracker_lang['more']."</strong>: ".($host)."<br />
".(!empty($data_get) ? "<br /><b>GET</b>: ".$data_get:"")." ".(!empty($data_post) ?"<br /><b>POST</b>: ".$data_post:"")."
</td>";

echo "</tr>";

++$line_num;
}

}

} elseif ($type == 'info' && !empty($data)){

echo "<tr>
<td class=\"a\" valign=\"top\">#</td>
<td class=\"a\" valign=\"top\">".$tracker_lang['description']."</td>
</tr>";

$exp = explode("\n", $data);

$line_num = 1;
foreach ($exp AS $datka){
    
list($time, $ip, $hua, $from, $host) = explode('#', $datka);

if (!empty($datka) && !empty($hua)){

echo "<tr>";
echo "<td class=\"b\"># ".$line_num."</td>";

echo "<td class=\"b\">
<strong>".$tracker_lang['clock']."</strong>: ".$time."<br />
<strong>".$tracker_lang['user_ip']."</strong>: <a title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$ip."\">".$ip."</a><br />
<strong>".$tracker_lang['tabs_agent']."</strong>: <a title=\"".$tracker_lang['search_added']."\" href=\"useragent.php?fme=".$hua."\">".htmlentities($hua)."</a> (".$tracker_lang['hash_sum'].": <a title=\"".$tracker_lang['search_added']."\" href=\"useragent.php?fme=".crc32($hua)."\">".crc32($hua)."</a>)<br />
<strong>".$tracker_lang['refer_site']."</strong>: <a href=\"".$from."\">".$from."</a><br />
<strong>".$tracker_lang['http_link']."</strong>: <a href=\"".$host."\">".$host."</a>
</td></td>";

echo "</tr>";

++$line_num;

}

}

} elseif ($type == 'sql' && !empty($data)){

echo "<tr>
<td class=\"a\" align=\"left\">#</td>
<td class=\"a\" align=\"center\">".$tracker_lang['description']."</td>
</tr>";

$exp = explode("\n<#-/-!>\r", $data);

$line_num = 1;
foreach ($exp AS $datka){
    
list($hua, $even) = explode('#', $datka);
    
$events = explode("\r(-\-)\n", $even);

$data_get = htmlspecialchars(print_r(unserialize($events[0]), true));
$data_post = htmlspecialchars(print_r(unserialize($events[1]), true));	

if (!empty($data_get))
$data_get = preg_replace('/Array\s\((.*?)\)$/is', "<strong>(</strong>\\1<strong>)</strong>", $data_get);

if (!empty($data_post))
$data_post = preg_replace('/Array\s\((.*?)\)$/is', "<strong>(</strong>\\1<strong>)</strong>", $data_post);

if (!empty($hua) && !empty($datka)){

$hua = preg_replace('/\/([a-zA-Z]+.php(.*?)),/is', "/<a href=\"\\1\">\\1</a>,", $hua);

echo "<tr>
<td class=\"a\" align=\"left\"># ".$line_num."</td>
<td class=\"b\" align=\"left\">".$hua.(!empty($data_get) ? "<br /><b>GET</b> (".mksize(strlen($events[0]))."): ".$data_get:"")." ".(!empty($data_post) ?" <br /><b>POST</b>  (".mksize(strlen($events[1]))."): ".$data_post:"")."</td>
</tr>";

++$line_num;
}

}

} elseif ($type == 'bit' && !empty($data)){

echo "<tr>
<td class=\"a\" valign=\"top\">#</td>
<td class=\"a\" valign=\"top\">".$tracker_lang['description']."</td>
<td class=\"a\" valign=\"top\">".$tracker_lang['torrent_clients']."</td>
</tr>";

$exp = explode("\n\r", $data);

$line_num = 1;
foreach ($exp AS $datka){
    
list($passkey, $time, $ip, $data, $agent) = explode('#', $datka);

if (!empty($datka) && !empty($data)){

echo "<tr>";
echo "<td class=\"b\"># ".$line_num."</td>";

echo "<td class=\"b\">
<strong>".$tracker_lang['user_ip']."</strong>: <a title=\"".$tracker_lang['search_ip']."\" href=\"usersearch.php?ip=".$ip."\">".$ip."</a><br />
<strong>".$tracker_lang['clock']."</strong>: ".$time."<br />
<strong>".$tracker_lang['passkey']."</strong>: ".(!empty($passkey) ? "<a title=\"".$tracker_lang['search']."\" href=\"usersearch.php?passkey=".$passkey."\">".$passkey."</a>": $tracker_lang['unknown_passkey'])."<br />
<strong>".$tracker_lang['more']."</strong>: ".$data."<br />
</td>";

echo "<td class=\"b\">".$agent."</td>";

echo "</tr>";

++$line_num;
}

}

} elseif ($type == 'lang' && !empty($data)){

echo "<tr>
<td class=\"a\" valign=\"top\">#</td>
<td class=\"a\" valign=\"top\">".$tracker_lang['description']."</td>
<td class=\"a\" valign=\"top\">".$tracker_lang['fixed']."</td>
</tr>";

$exp = explode("\n", $data);

$line_num = 1;
foreach ($exp AS $datka){
    
list ($plang, $tphp) = explode('-', $datka);

if (!empty($datka) && !empty($data)){

echo "<tr>";
echo "<td class=\"b\"># ".$line_num."</td>";

echo "<td class=\"b\"><strong>".$plang."</strong> => \$tracker_lang['<ins>".trim($plang)."</ins>']</td>";

echo "<td class=\"b\">".$tphp."</td>";

echo "</tr>";

++$line_num;

}

}

}

echo  "</table>";

stdfoot(true);

?>