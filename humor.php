<?
require "include/bittorrent.php";
dbconn();


if (isset($_POST['GRI_E']) && $CURUSER){

$hum = htmlspecialchars_uni(strip_tags($_POST['GRI_E']));

if (!empty($hum) && strlen($hum) > 10)
sql_query("INSERT INTO humor (uid, txt) VALUES (".sqlesc($CURUSER["id"]).", ".sqlesc($hum).")");

$id = mysql_insert_id();
header("Location: humor.php".(!empty($id) ? "?id=".$id:"")) or die("<script>setTimeout('document.location.href=\"humor.php".(!empty($id) ? "?id=".$id:"")."\"', 100);</script>");

die;

} elseif (isset($_POST['GRI']) && get_user_class() >= UC_MODERATOR) {

$id = (int) $_POST['idi_txt'];

$hum = htmlspecialchars_uni(strip_tags($_POST['GRI']));

if (!empty($hum) && !empty($id))
sql_query("UPDATE humor SET txt = ".sqlesc($hum)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

header("Location: humor.php".(!empty($id) ? "?id=".$id:"")) or die("<script>setTimeout('document.location.href=\"humor.php".(!empty($id) ? "?id=".$id:"")."\"', 100);</script>");

die;
}



$id = (int) $_GET['id'];

if (get_user_class() >= UC_MODERATOR && isset($_GET['do'])){

if ($_GET['do'] == "delete"){

sql_query("DELETE FROM humor WHERE id = ".sqlesc($id)." LIMIT 1") or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM karma WHERE type = 'humor' AND value = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

header("Location: humor.php") or die("<script>setTimeout('document.location.href=\"humor.php\"', 100);</script>");
die;

} elseif ($_GET['do'] == "edit"){

$hu = sql_query("SELECT txt FROM humor WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (!mysql_num_rows($hu))
stderr($tracker_lang['error'], $tracker_lang['no_humor_with_such_id'].": ".$id);

stdhead($tracker_lang['jokes'].": ".$tracker_lang['edit']);

$res = mysql_fetch_assoc($hu);

$text = htmlspecialchars_uni($res["txt"]);

echo "<form action=\"humor.php\" method=\"post\"><table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tr><td align=\"center\" class=\"a\">".$tracker_lang['edit']."</td></tr>
<tr><td align=\"center\" class=\"b\">
<textarea class=\"En_J\" cols=\"100\" rows=\"6\" name=\"GRI\" onkeypress=\"EnJ_GrI.value=GRI.value.length\" onchange=\"EnJ_GrI.value=GRI.value.length\">".$text."</textarea> 
<input class=\"E_nJ\" disabled onfocus=\"GRI.focus()\" name=\"EnJ_GrI\" /> <input type=\"hidden\" name=\"idi_txt\" value=\"".$id."\" /> <input class=\"btn\" value=\"".$tracker_lang['edit']."\" type=\"submit\" />
</td></tr>
</table></form>";

stdfoot();
die;
}

} elseif (!isset($_GET['do']) && isset($_GET['id'])) {

$hu = sql_query("SELECT humor.*, users.username, users.class 
FROM humor 
LEFT JOIN users ON users.id = humor.uid 
WHERE humor.id = ".sqlesc($id)." LIMIT 1") or sqlerr(__FILE__, __LINE__);

if ( mysql_num_rows($hu) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_humor_with_such_id'].": ".$id);

stdhead($tracker_lang['view_humor']);

$res = mysql_fetch_assoc($hu);

echo "<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";

echo "<tr><td align=\"center\" class=\"a\" colspan=\"2\"><b>".$tracker_lang['view_humor']."</b>: <a title=\"".$tracker_lang['stable_link']."\" href=\"humor.php?id=".$id."\">".$id."</a> (".$res['date'].")</td></tr>";

echo "<tr><td align=\"center\" class=\"b\">".format_comment($res['txt'])."</td><tr>";

echo "<tr><td align=\"center\" class=\"a\" colspan=\"2\">
<div style=\"text-align:right;\">
".(!empty($res["username"]) ? "<b>".$tracker_lang['news_poster']."</b>: <a href=\"userdetails.php?id=".$res['uid']."\">".get_user_class_color($res['class'],$res['username'])."</a>":"".$tracker_lang['news_poster'].": <u>".$tracker_lang['anonymous']."</u>.")." ".(get_user_class() >= UC_MODERATOR ? "<b>(</b><a href=\"humor.php?id=".$id."&do=edit\" title=\"".$tracker_lang['edit']."\">".$tracker_lang['edit']."</a><b>)</b> <b>(</b><a title=\"".$tracker_lang['delete']."\" href=\"humor.php?id=".$id."&do=delete\">".$tracker_lang['delete']."</a><b>)</b>":"")."</div>
</td></tr>";

echo "</table>";
echo "<br />";

echo "<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
echo "<tr><td align=\"center\" class=\"a\" colspan=\"2\"><b>".$tracker_lang['last_uhumor']."</b></td><tr>";

$num_karma = number_format(get_row_count("karma", "WHERE value = ".sqlesc($id)));

$hstat = sql_query("SELECT karma.*, users.username, users.class
FROM karma 
LEFT JOIN users ON users.id = karma.user
WHERE karma.value = ".sqlesc($id)." ORDER BY karma.added DESC LIMIT 200") or sqlerr(__FILE__, __LINE__);

echo "<tr><td align=\"center\" class=\"b\">";

$array_date = array();

if (mysql_num_rows($hstat) == 0)
echo $tracker_lang['no_votes'];

while ($row = mysql_fetch_assoc($hstat)){

$array_date[] = $row["added"];

if ($st == true)
echo ", ";

echo "<a href=userdetails.php?id=".$row['user'].">".get_user_class_color($row['class'], $row['username'])."</a>";
$st = true;
}

echo "</td><tr>";

$min = @min($array_date);
$max = @max($array_date);

echo "<tr><td align=\"center\" class=\"a\" colspan=\"2\"><b>".$tracker_lang['total']."</b>: ".$num_karma." ".$tracker_lang['votes']." ".(!empty($num_karma) ? " <br /> ".get_date_time($min)." - ".get_date_time($max):"</b>.")."</td><tr>";

echo "</table>";
echo "<br />";

echo "<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";

echo "<tr><td align=\"center\" class=\"b\" colspan=\"2\">".($CURUSER ? "<a href=\"humor.php\" title=\"".$tracker_lang['add_humor']."\"><b>".$tracker_lang['add_humor']."</b></a> /":"")." <a href=\"humorall.php\" title=\"".$tracker_lang['jokes']."\"><b>".$tracker_lang['jokes']."</b></a> </td></tr>";

echo "</table>";
echo "<br />";

} elseif ($CURUSER) {

stdhead($tracker_lang['add_humor']);

echo "<br />
<form action=\"humor.php\" method=\"post\">
<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";

$text_array = array(
'������� (����� ����) �������� ����� ����� ������ �����������.',
'���� ������� �� ������ ��� �������� �����, )))))) � ������ �����, �� �� �� ����� ������� � � ����, ������� ��� � ������� �����.',
'���� ������� ������� � �������� �� ���� ��� ��������� ������, �� ����� ����� ��������.',
'�������� <b>1000</b> ��������, <b>BB</b> ���� ���������',
'���������� CapsLock (��� ������������� <u>�������</u>)',
'��������� ���������',
'��������� ������������ ��� � ����� ���������� ������ ������������.'
);

echo "<tr><td align=\"center\" class=\"a\" colspan=\"2\"><b>��������</b>: ������� ������ ����� ����������� �������� � ����.</td></tr>";

echo "<tr><td align=\"center\" class=\"b\" colspan=\"2\">";

foreach ($text_array AS $dinka){
echo "<li>".$dinka."</li>";
}

echo "</td></tr>";

echo "<tr><td align=\"center\" class=\"b\" colspan=\"2\"><strong>�� ������ �����</strong>: <br />
<b>����������</b> - ��� (��� ������ ������) ����������� <b>25 ��</b> � ������� ��� ������� ������� >=1<br />
<b>�� ����������</b> - � ��� ��������� <b>12,5 ��</b> �� ������� ��� �������� >=1
</td></tr>";

echo "<tr>
<td align=\"center\" class=\"b\" colspan=\"2\">
<textarea class=\"En_J\" cols=\"100\" rows=\"6\" name=\"GRI_E\" onkeypress=\"EnJ_GrI.value=GRI_E.value.length\" onchange=\"EnJ_GRI_E.value=GRI_E.value.length\"></textarea>
<br /><input class=\"E_nJ\" disabled onfocus=\"GRI_E.focus()\" name=\"EnJ_GrI\" /> <input class=\"btn\" value=\"".$tracker_lang['add_humor']."\" type=\"submit\" />
</td></tr>";

echo "<tr><td align=\"center\" class=\"b\" colspan=\"2\"><a href=\"humorall.php\" title=\"".$tracker_lang['jokes']."\"><b>".$tracker_lang['jokes']."</b></td></tr>";

echo "</table></form><br />";
} else stderr($tracker_lang['error'], $tracker_lang['no_data_now']);

stdfoot();
?>