<? if(!defined('IN_TRACKER'))  die('Hacking attempt!');

 

function get_user_class_color($class, $username, $italik = false) {

global $tracker_lang;

switch ($username) {


case '7Max7':
return "<span style=\"color:red ".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\">7</span><span style=\"color:#0f6cee".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\" title=\"�����\">Max</span><span style=\"color:red".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\">7</span>";
break;


}

switch ($class) {

case UC_SYSOP: return "<span style=\"color:blue ".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\" title=\"".$tracker_lang['class_sysop']."\">" . $username . "</span>";
break;

case UC_ADMINISTRATOR: return "<span style=\"color:green ".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\" title=\"".$tracker_lang['class_administrator']."\">" . $username . "</span>";
break;

case UC_MODERATOR: return "<span style=\"color:red ".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\" title=\"".$tracker_lang['class_moderator']."\">" . $username . "</span>";
break;

case UC_UPLOADER: return "<span style=\"color:#f59555 ".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\" title=\"".$tracker_lang['class_uploader']."\">" . $username . "</span>";
break;

case UC_VIP: return "<span style=\"color:#9C2FE0 ".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\" title=\"".$tracker_lang['class_vip']."\">" . $username . "</span>";
break;

case UC_POWER_USER: return "<span style=\"color:#D21E36 ".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\" title=\"".$tracker_lang['class_power_user']."\">" . $username . "</span>";
break;

case UC_USER: return "<span style=\"color:black ".($italik? "; border-bottom: 1px solid #0f4806; ":"")."\" title=\"".$tracker_lang['class_user']."\">" . $username . "</span>";
break;

}

return $username;
}


function get_user_rgbcolor($class, $username = false) {

global $tracker_lang;

switch ($username) {



/// ������ �����
case '��': /// ����
return "008000"; /// ��
break;




}

switch ($class) {

case UC_SYSOP: return "0000ff";
break;

case UC_ADMINISTRATOR: return "008000";
break;

case UC_MODERATOR: return "ff0000";
break;

case UC_UPLOADER: return "f59555";
break;

case UC_VIP: return "9C2FE0";
break;

case UC_POWER_USER: return "D21E36";
break;

case UC_USER: return "000000";
break;

}

return $username;

}


function get_user_class() {

global $CURUSER;

if (!$CURUSER)
return -1;
else
return $CURUSER["class"];

}

function get_user_class_name($class) {

global $tracker_lang;

switch ($class) {
case '-1': return $tracker_lang['guest'];
case UC_USER: return $tracker_lang['class_user'];
case UC_POWER_USER: return $tracker_lang['class_power_user'];
case UC_VIP: return $tracker_lang['class_vip'];
case UC_UPLOADER: return $tracker_lang['class_uploader'];
case UC_MODERATOR: return $tracker_lang['class_moderator'];
case UC_ADMINISTRATOR: return $tracker_lang['class_administrator'];
case UC_SYSOP: return $tracker_lang['class_sysop'];
}

return '';

}


function attacks_log ($file) {

global $CURUSER, $�System, $DEFAULTBASEURL;

/// ���� ������������� ��������� ����� - �� ���� ������.
if ($CURUSER['override_class'] <> '255' || !$CURUSER)
return true;

$ip = getip(); /// ��������� ����� ip

$file = end(explode('/', $file));

write_log("������� ������. ������������: <strong>".$CURUSER['username']."</strong> � Ip: <strong>".$ip."</strong> ����: <strong>".htmlentities($file)."</strong>","#BCD2E6","error");

//// ����������� ������������� ������� (�� config)
if ($�System["aggressive"] == true && $CURUSER) {

/// true - ��� ���������� ��������, ����� ������� ����� � �������.
if ($CURUSER["monitoring"] == "no" && $�System["monitoring"] == true) {
$usercomment = sqlesc(date("Y-m-d") . " �System: ��� ����������� (����� ".htmlentities($file).").\n");

sql_query("UPDATE users SET monitoring = 'yes', usercomment = CONCAT_WS('',".$usercomment.", usercomment) WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);
}

/// true - ��������� ������� ����� ������� ����� ������������ � �������.
if ($�System["off_user"] == true) {
$usercomment = sqlesc(date("Y-m-d") . " �System: ���� �������� (����� ".htmlentities($file).").\n");

sql_query("UPDATE users SET enabled = 'no', usercomment = CONCAT_WS('',".$usercomment.", usercomment) WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);
}

/// true - ��������� �� ������������ (���� ����) ��� ������� ����� � �������.
if (get_user_class() <> UC_USER && get_user_class() < UC_SYSOP  && $�System["drop_class"] == true) {
$usercomment = sqlesc(date("Y-m-d") . " �System: ��������� ���� (����� ".htmlentities($file).").\n");

sql_query("UPDATE users SET class = '0', promo_time = ".sqlesc(get_date_time()).", usercomment = CONCAT_WS('',".$usercomment.", usercomment) WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);
}

/// true - ��������� �� ������������ (���� ����) ��� ������� ����� � �������.
if ($�System["fake_msg_user"] == true && $�System["off_user"] == false) {

$subject = sqlesc("������� ������");

$msg = sqlesc("������������������� ������ ����������������� �����. [b]������������[/b]: [url=".$DEFAULTBASEURL."/userdetails.php?id=".$CURUSER['id']."]".$CURUSER['username']."[/url]. \n������ ��������� �������� ������ ������, ������������ �������������.");

sql_query("INSERT INTO messages (sender, receiver, added, msg, subject, poster) SELECT 0, id, ".sqlesc(get_date_time()).", ".$msg.", ".$subject.", 0 FROM users WHERE id = ".sqlesc($CURUSER['id'])) or sqlerr(__FILE__,__LINE__);

sql_query("UPDATE users SET unread = unread+1 WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);

$fake_msg = true;
}

/// true - ��� �� ip ������������ ��� ������� ����� � �������.
if ($�System["ban_ip_user"] == true && !empty($ip)) {

$first = ip2long($ip);
$last = ip2long($ip);

sql_query("INSERT INTO bans (added, addedby, first, last, bans_time, comment) VALUES(".sqlesc(get_date_time()).", '92', ".sqlesc($first).", ".sqlesc($last).", ".sqlesc(get_date_time(gmtime() + 7 * 604800)).", ".sqlesc("�System: ��� �� ip (����� ".htmlentities($file).").").")") or sqlerr(__FILE__, __LINE__);

write_log("IP ����� ".$ip." ��� ������� �������� (������� ����� ".htmlentities($file).").","ff3a3a","bans");
unsql_cache("bans_first_last");
}

/// true - �������� ������������� ��������� �� ������.
if ($�System["real_admin_msg"] == true) {

$subject = sqlesc("������� ������");

$msg = sqlesc("������������������� ������ ����������������� �����. [b]������������[/b]: [url=".$DEFAULTBASEURL."/userdetails.php?id=".$CURUSER['id']."]".$CURUSER['username']."[/url]. ".($fake_msg == true ? "\n������ ��������� �������� ������ ������, ������������ ������������, � ������ ������ ��� �����������.":"")."\n [b]��������[/b]: ��� ��������� ���� ����� � ������� ����, � �� ��������� ������ �����.");

sql_query("INSERT INTO messages (sender, receiver, added, msg, subject, poster) SELECT 0, id, ".sqlesc(get_date_time()).", ".$msg.", ".$subject.", 0 FROM users WHERE class = ".sqlesc(UC_SYSOP)) or sqlerr(__FILE__,__LINE__);

sql_query("UPDATE users SET unread = unread+1 WHERE class = ".sqlesc(UC_SYSOP)) or sqlerr(__FILE__,__LINE__);

}

/// true - �������� ������ �� ����� ��������� � $Mail_config['warning'] �� ������.
if ($�System["sendmsg_on_war_email"] == true) {

global $SITENAME, $Mail_config;

$message = "������������������� ������ ����������������� �����. ������������: ".$DEFAULTBASEURL."/userdetails.php?id=".$CURUSER['id']." ".$CURUSER['username'].". ".($fake_msg == true ? "\n������ ��������� �������� ������ ������, ������������ ������������, � ������ ������ ��� �����������.":"")."";

if (!empty($Mail_config['warning']))
@sent_mail($Mail_config['warning'], $SITENAME, $SITEEMAIL, "������� ������", $message, false);

}

unsql_cache("arrid_".$CURUSER["id"]);
}

}

function display_date_time($timestamp = 0 , $tzoffset = 0){

return date("Y-m-d H:i:s", $timestamp + ($tzoffset * 60));

}

function textbbcode($form, $name, $content = false, $id_area = 'area') {

global $tracker_lang;

if (preg_match("/upload/i", $_SERVER["SCRIPT_FILENAME"]))
$col = "18";
elseif (preg_match("/edit/i", $_SERVER["SCRIPT_FILENAME"]))
$col = "38";
else
$col = "11";

echo '<script language="javascript" type="text/javascript" src="js/ajax.js"></script>';
echo '<script language="javascript" type="text/javascript" src="js/bbcode.js"></script>';

echo "<div id=\"loading-layer\" style=\"display:none;font-family: Verdana;font-size:11px;width:200px;height:50px;background:#FFF;padding:10px;text-align:center;border:1px solid #000\"><div style=\"font-weight:bold\" id=\"loading-layer-text\">".$tracker_lang['loading']."</div><br /><img src=\"../pic/loading.gif\" border=\"0\" /></div>";

echo "<style>
.editbutton {cursor: pointer; padding: 2px 1px 0px 5px;}
div.grippie {background:#EEEEEE url(\"/pic/grippie.png\") no-repeat scroll center 2px;border-color:#DDDDDD;border-style:solid;border-width:0pt 1px 1px;cursor:s-resize;height:9px;overflow:hidden;}
</style>";

echo "<table cellpadding=\"0\" cellspacing=\"0\" align=\"�enter\">";

echo "<tr>";
echo "<td class=\"b\">";

echo "<div>";

echo "<div align=\"center\">";

echo "<select name=\"fontFace\" class=\"editbutton\">
<option style=\"font-family: Verdana;\" value=\"Verdana\" selected=\"selected\">".$tracker_lang['bb_fonts'].":</option>
<option style=\"font-family: Courier;\" value=\"Courier\">&nbsp;Courier</option>
<option style=\"font-family: Courier New;\" value=\"Courier New\">&nbsp;Courier New</option>
<option style=\"font-family: monospace;\" value=\"monospace\">&nbsp;monospace</option>
<option style=\"font-family: Fixedsys;\" value=\"Fixedsys\">&nbsp;Fixedsys</option>
<option style=\"font-family: Arial;\" value=\"Arial\">&nbsp;Arial</option>
<option style=\"font-family: Comic Sans MS;\" value=\"Comic Sans MS\">&nbsp;Comic Sans</option>
<option style=\"font-family: Georgia;\" value=\"Georgia\">&nbsp;Georgia</option>
<option style=\"font-family: Tahoma;\" value=\"Tahoma\">&nbsp;Tahoma</option>
<option style=\"font-family: Times New Roman;\" value=\"Times New Roman\">&nbsp;Times</option>
<option style=\"font-family: serif;\" value=\"serif\">&nbsp;serif</option>
<option style=\"font-family: sans-serif;\" value=\"sans-serif\">&nbsp;sans-serif</option>
<option style=\"font-family: cursive;\" value=\"cursive\">&nbsp;cursive</option>
<option style=\"font-family: fantasy;\" value=\"fantasy\">&nbsp;fantasy</option>
<option style=\"font-family: Book Antiqua;\" value=\"Book Antiqua\">&nbsp;Antiqua</option>
<option style=\"font-family: Century Gothic;\" value=\"Century Gothic\">&nbsp;Century Gothic</option>
<option style=\"font-family: Franklin Gothic Medium;\" value=\"Franklin Gothic Medium\">&nbsp;Franklin</option>
<option style=\"font-family: Garamond;\" value=\"Garamond\">&nbsp;Garamond</option>
<option style=\"font-family: Impact;\" value=\"Impact\">&nbsp;Impact</option>
<option style=\"font-family: Lucida Console;\" value=\"Lucida Console\">&nbsp;Lucida</option>
<option style=\"font-family: Palatino Linotype;\" value=\"Palatino Linotype\">&nbsp;Palatino</option>
<option style=\"font-family: Trebuchet MS;\" value=\"Trebuchet MS\">&nbsp;Trebuchet</option>
</select>";

echo "&nbsp;";

echo "<select name=\"codeColor\" class=\"editbutton\">
<option style=\"color: black; background: #fff;\" value=\"black\" selected=\"selected\">".$tracker_lang['bb_colorfont'].":</option>
<option style=\"color: black;\" value=\"Black\">&nbsp;������</option>
<option style=\"color: sienna;\" value=\"Sienna\">&nbsp;����</option>
<option style=\"color: Beige;\" value=\"Beige\">&nbsp;�������</option>
<option style=\"color: darkolivegreen;\" value=\"DarkOliveGreen\">&nbsp;����. �������</option>
<option style=\"color: darkgreen;\" value=\"DarkGreen\">&nbsp;�. �������</option>
<option style=\"color: Cornflower;\" value=\"Cornflower\">&nbsp;�����������</option>
<option style=\"color: darkslateblue;\" value=\"DarkSlateBlue\">&nbsp;����.-�����</option>
<option style=\"color: navy;\" value=\"Navy\">&nbsp;�����-�����</option>
<option style=\"color: MidnightBlue;\" value=\"MidnightBlue\">&nbsp;����.-�����</option>
<option style=\"color: indigo;\" value=\"Indigo\">&nbsp;������</option>
<option style=\"color: darkslategray;\" value=\"DarkSlateGray\">&nbsp;��������-�����</option>
<option style=\"color: darkred;\" value=\"DarkRed\">&nbsp;�. �������</option>
<option style=\"color: darkorange;\" value=\"DarkOrange\">&nbsp;�. ���������</option>
<option style=\"color: olive;\" value=\"Olive\">&nbsp;���������</option>
<option style=\"color: green;\" value=\"Green\">&nbsp;�������</option>
<option style=\"color: DarkCyan;\" value=\"DarkCyan\">&nbsp;������ ����</option>
<option style=\"color: CadetBlue;\" value=\"CadetBlue\">&nbsp;����-�����</option>
<option style=\"color: Aquamarine;\" value=\"Aquamarine\">&nbsp;���������</option>
<option style=\"color: teal;\" value=\"Teal\">&nbsp;������� �����</option>
<option style=\"color: blue;\" value=\"Blue\">&nbsp;�������</option>
<option style=\"color: slategray;\" value=\"SlateGray\">&nbsp;��������-�����</option>
<option style=\"color: dimgray;\" value=\"DimGray\">&nbsp;������-�����</option>
<option style=\"color: red;\" value=\"Red\">&nbsp;�������</option>
<option style=\"color: Chocolate;\" value=\"Chocolate\">&nbsp;����������</option>
<option style=\"color: Firebrick;\" value=\"Firebrick\">&nbsp;���������</option>
<option style=\"color: Saddlebrown;\" value=\"SaddleBrown\">&nbsp;���.����������</option>
<option style=\"color: yellowgreen;\" value=\"YellowGreen\">&nbsp;����-�������</option>
<option style=\"color: seagreen;\" value=\"SeaGreen\">&nbsp;�����. �������</option>
<option style=\"color: mediumturquoise;\" value=\"MediumTurquoise\">&nbsp;���������</option>
<option style=\"color: royalblue;\" value=\"RoyalBlue\">&nbsp;������� �����.</option>
<option style=\"color: purple;\" value=\"Purple\">&nbsp;�������</option>
<option style=\"color: gray;\" value=\"Gray\">&nbsp;�����</option>
<option style=\"color: magenta;\" value=\"Magenta\">&nbsp;���������</option>
<option style=\"color: orange;\" value=\"Orange\">&nbsp;���������</option>
<option style=\"color: yellow;\" value=\"Yellow\">&nbsp;������</option>
<option style=\"color: Gold;\" value=\"Gold\">&nbsp;�������</option>
<option style=\"color: Goldenrod;\" value=\"Goldenrod\">&nbsp;����������</option>
<option style=\"color: lime;\" value=\"Lime\">&nbsp;��������</option>
<option style=\"color: cyan;\" value=\"Cyan\">&nbsp;���.-�������</option>
<option style=\"color: deepskyblue;\" value=\"DeepSkyBlue\">&nbsp;�.���.-�������</option>
<option style=\"color: darkorchid;\" value=\"DarkOrchid\">&nbsp;�������</option>
<option style=\"color: silver;\" value=\"Silver\">&nbsp;�����������</option>
<option style=\"color: pink;\" value=\"Pink\">&nbsp;�������</option>
<option style=\"color: wheat;\" value=\"Wheat\">&nbsp;Wheat</option>
<option style=\"color: lemonchiffon;\" value=\"LemonChiffon\">&nbsp;��������</option>
<option style=\"color: palegreen;\" value=\"PaleGreen\">&nbsp;��. �������</option>
<option style=\"color: paleturquoise;\" value=\"PaleTurquoise\">&nbsp;��. ���������</option>
<option style=\"color: lightblue;\" value=\"LightBlue\">&nbsp;��. �������</option>
<option style=\"color: plum;\" value=\"Plum\">&nbsp;��. �������</option>
<option style=\"color: white;\" value=\"White\">&nbsp;�����</option>
</select>";

echo "&nbsp;";

echo "<select name=\"codeSize\" class=\"editbutton\">
<option value=\"12\" selected=\"selected\">".$tracker_lang['bb_sizefont']." - 12:</option>
<option value=\"9\" class=\"em\">".$tracker_lang['bb_sizefont']." - 9</option>
<option value=\"10\">&nbsp;".$tracker_lang['bb_sizefont']." - 10</option>
<option value=\"11\">&nbsp;".$tracker_lang['bb_sizefont']." - 11</option>
<option value=\"12\" class=\"em\">".$tracker_lang['bb_sizefont']." - 12</option>
<option value=\"14\">&nbsp;".$tracker_lang['bb_sizefont']." - 14</option>
<option value=\"16\">&nbsp;".$tracker_lang['bb_sizefont']." - 16</option>
<option value=\"18\" class=\"em\">".$tracker_lang['bb_sizefont']." - 18</option>
<option value=\"20\">&nbsp;".$tracker_lang['bb_sizefont']." - 20</option>
<option value=\"22\">&nbsp;".$tracker_lang['bb_sizefont']." - 22</option>
<option value=\"24\" class=\"em\">".$tracker_lang['bb_sizefont']." - 24</option>
</select>";

echo "&nbsp;";

$exp = array_map('trim', explode(", ", $tracker_lang['bb_valign']));

echo "<select name=\"codeAlign\" class=\"editbutton\">
<option value=\"\" selected=\"selected\">".$tracker_lang['bb_align'].": </option>
<option style=\"text-align: left;\" value=\"left\">&nbsp;".$exp[0]."</option>
<option style=\"text-align: right;\" value=\"right\">&nbsp;".$exp[1]."</option>
<option style=\"text-align: center;\" value=\"center\">&nbsp;".$exp[2]."</option>
<option style=\"text-align: justify;\" value=\"justify\">&nbsp;".$exp[3]."</option>
</select>";

echo "</div>";

$ex_1 = array_map('trim', explode(", ", $tracker_lang['bb_bcode']));
$ex_2 = array_map('trim', explode(", ", $tracker_lang['bb_brepl']));
//".$ex_1[0]."

echo "<div align=\"center\">";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[0]."\" name=\"codeHR\" title=\"".$ex_2[0]." (Ctrl+8)\" style=\"font-weight: bold; width: 26px;\" /> ";
echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[1]."\" name=\"codeBR\" title=\"".$ex_2[1]."\" style=\"width: 26px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[2]."\" name=\"codeSpoiler\" title=\"".$ex_2[2]." (Ctrl+S)\" style=\"width: 70px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\" ".$ex_1[3]." \" name=\"codeB\" title=\"".$ex_2[3]." (Ctrl+B)\" style=\"font-weight: bold; width: 30px;\" /> ";
echo "<input class=\"btn\" type=\"button\" value=\" ".$ex_1[4]." \" name=\"codeI\" title=\"".$ex_2[4]." (Ctrl+I)\" style=\"width: 30px; font-style: italic;\" /> ";
echo "<input class=\"btn\" type=\"button\" value=\" ".$ex_1[5]." \" name=\"codeU\" title=\"".$ex_2[5]." (Ctrl+U)\" style=\"width: 30px; text-decoration: underline;\" /> ";
echo "<input class=\"btn\" type=\"button\" value=\" ".$ex_1[6]." \" name=\"codeS\" title=\"".$ex_2[6]."\" style=\"width: 30px; text-decoration: line-through;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\" ".$ex_1[7]." \" name=\"codeBB\" title=\"".$ex_2[7]." (Ctrl+N)\" style=\"font-weight: bold; width: 30px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\" ".$ex_1[8]." \" name=\"codePRE\" title=\"".$ex_2[8]." (Ctrl+P)\" style=\"width: 40px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\" ".$ex_1[9]." \" name=\"codeHT\" title=\"".$ex_2[9]." (Ctrl+H)\" style=\"width: 60px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\" ".$ex_1[10]." \" name=\"codeMG\" title=\"".$ex_2[10]." (Ctrl+M)\" style=\"width: 100px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[11]."\" name=\"codeQuote\" title=\"".$ex_2[11]." (Ctrl+Q)\" style=\"width: 60px;\" /> ";
echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[12]."\" name=\"codeImg\" title=\"".$ex_2[12]." (Ctrl+R)\" style=\"width: 40px;\" /> ";

echo "<input class=\"btn\" type=\"button\" ".(in_array(basename($_SERVER['SCRIPT_FILENAME']), array("edit.php", "uploadnext.php")) ? "disabled=\"disabled\"":"")."  value=\"".$ex_1[13]."\" name=\"quoteselected\" title=\"".$ex_2[13]."\" style=\"width: 165px;\" onmouseout=\"bbcode.refreshSelection(false);\" onmouseover=\"bbcode.refreshSelection(true);\" onclick=\"bbcode.onclickQuoteSel();\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[14]."\" ".(!in_array(basename($_SERVER['SCRIPT_FILENAME']), array("edit.php", "uploadnext.php")) ? "disabled=\"disabled\"":"")." name=\"codeHIDE\" title=\"".$ex_2[14]."\" style=\"width: 70px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[15]."\" name=\"codeUri\" title=\"".$ex_2[15]."\" style=\"width: 40px; text-decoration: underline;\" /> ";
echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[16]."\" name=\"codeUr\" title=\"".$ex_2[16]."\" style=\"width: 40px; text-decoration: underline;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[17]."\" name=\"codeCode\" title=\"".$ex_2[17]." (Ctrl+K)\" style=\"width: 46px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[18]."\" name=\"codeFlash\" title=\"".$ex_2[18]." (Ctrl+F)\" style=\"width: 50px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[19]."\" name=\"codeYT\" title=\"".$ex_2[19]." (Ctrl+F)\" style=\"width: 50px;\" /> ";

//echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[20]."\" name=\"codeMR\" title=\"".$ex_2[20]."\" style=\"width: 100px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[20]."\" name=\"codeMM\" title=\"".$ex_2[20]."\" style=\"width: 50px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[21]."\" name=\"codeOpt\" title=\"".$ex_2[21]." (Ctrl+0)\" style=\"width: 30px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[22]."\" name=\"codeLG1\" title=\"".$ex_2[22]." (Ctrl+1)\" style=\"width: 65px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[23]."\" name=\"codeLG2\" title=\"".$ex_2[23]." (Ctrl+2)\" style=\"width: 65px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[24]."\" name=\"codeHIG\" title=\"".$ex_2[24]."\" style=\"width: 60px;\" /> ";

if (in_array(basename($_SERVER['SCRIPT_FILENAME']), array("edit.php", "uploadnext.php")))
echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[25]."\" onclick='textbb_udesck(\"".$id_area."\")' title=\"".$ex_2[25]."\" style=\"width: 60px;\" /> ";

echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[26]."\" name=\"Smailes\" title=\"".$ex_2[26]."\" style=\"width: 60px;\" onclick=\"window.open('smilies.php?form=".$form."&text=".$name."', 'height=500,width=450,resizable=no,scrollbars=yes'); return false;\"/> ";

if (in_array(basename($_SERVER['SCRIPT_FILENAME']), array("message.php", "tracker-chat.php", "forums.php")))
echo "<input class=\"btn\" type=\"button\" value=\"".$ex_1[26]." (".$tracker_lang['list'].")\" name=\"Smailes_View\" title=\"".$ex_2[26]." (".$tracker_lang['list'].")\" style=\"width: 120px;\" /> ";

echo "</div>";

echo "<textarea class=\"resizable\" id=\"".$id_area."\" name=\"".$name."\" style=\"width:100%;\" rows=\"".$col."\" onfocus=\"storeCaret(this);\" onselect=\"storeCaret(this);\" onclick=\"storeCaret(this);\" onkeyup=\"storeCaret(this);\">".$content."</textarea>";

?>
<script type="text/javascript">
var bbcode = new BBCode(document.<?=$form;?>.<?=$name;?>);
var ctrl = "ctrl";
bbcode.addTag("codeB", "b", null, "B", ctrl);
bbcode.addTag("codeBB", "bb", null, "N", ctrl);
bbcode.addTag("codePRE", "pre", null, "P", ctrl);
bbcode.addTag("codeHT", "hideback", null, "H", ctrl);
bbcode.addTag("codeMG", "marquee", null, "M", ctrl);
bbcode.addTag("codeLG1", "legend", null, "1", ctrl);
bbcode.addTag("codeLG2", function(e) { var v=e.value; e.selectedIndex=0; return "legend=" }, "/legend", "2", ctrl);
bbcode.addTag("codeHIDE", "hide", null, "", ctrl);
bbcode.addTag("codeHIG", "highlight", null, "", ctrl);
bbcode.addTag("codeI", "i", null, "I", ctrl);
bbcode.addTag("codeU", "u", null, "U", ctrl);
bbcode.addTag("codeS", "s", null, "", ctrl);
bbcode.addTag("codeQuote", "quote", null, "Q", ctrl);
bbcode.addTag("codeImg", "img", null, "R", ctrl);
bbcode.addTag("codeUri", "url", "/url", "", ctrl);
bbcode.addTag("codeMM", "[mcom=#529EDC:#F5F5F5]", "/mcom", "", ctrl);
bbcode.addTag("codeUr", "url=", "/url", "", ctrl);
bbcode.addTag("codeCode", "php", null, "K", ctrl);
bbcode.addTag("codeFlash", "flash", null, "F", ctrl);
bbcode.addTag("codeOpt", "li", "", "0", ctrl);
bbcode.addTag("codeHR","hr", "", "8", ctrl);
bbcode.addTag("codeYT","video=", "", "", ctrl);
bbcode.addTag("codeBR","br", "", "", ctrl);
bbcode.addTag("codeSpoiler", "spoiler", null, "S",  ctrl);
bbcode.addTag("fontFace", function(e) { var v=e.value; e.selectedIndex=0; return "font="+v+"" }, "/font");
bbcode.addTag("codeColor", function(e) { var v=e.value; e.selectedIndex=0; return "color="+v }, "/color");
bbcode.addTag("codeSize", function(e) { var v=e.value; e.selectedIndex=0; return "size="+v }, "/size");
bbcode.addTag("codeAlign", function(e) { var v=e.value; e.selectedIndex=0; return "align="+v }, "/align");
</script>
<?
echo "</div><div id=\"prevsmalie\" align=\"center\" name=\"".$form.":".$id_area."\"></div>";

echo "</td>";
echo "</tr>";

if (!in_array(basename($_SERVER['SCRIPT_FILENAME']), array("details.php")))
echo "<tr><td style=\"margin: 0px; padding: 0px;\" align=\"center\" class=\"b\"><input type=\"button\" name=\"preview\" class=\"btn\" title=\"ALT+ENTER ".$tracker_lang['preview']."\" value=\"".$tracker_lang['preview']."\" onclick=\"javascript:ajaxpreview('".$id_area."');\"/> <input type=\"reset\" class=\"btn\" value=\"".$tracker_lang['reset']."\" /></td></tr><tr><td id=\"preview\" style=\"margin: 0px; padding: 0px;\" class=\"a\"></td></tr>";

echo "</table>";

}

function get_row_count($table, $suffix = false) {

$r = sql_query("SELECT COUNT(*) FROM ".$table." ".(!empty($suffix) ? $suffix:"")) or sqlerr(__FILE__,__LINE__);
$a = mysql_fetch_row($r);

return $a[0];
}

function stdmsg($heading = '', $text = '', $div = 'success', $htmlstrip = false) {

if ($htmlstrip) {
$heading = htmlspecialchars_uni(trim($heading));
$text = htmlspecialchars_uni(trim($text));
}

echo ("<table class=\"main\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"b\">\n");
echo ("<div class=\"".$div."\">".($heading ? "<b>".$heading."</b><br />" : "")."".$text."</div></td></tr></table>\n");

}


function stderr($heading = '', $text = '', $header_url = false) {

/**
* ��������: ����� ������� ������ �������� stderr() ������� �� ������ ���� ������� echo die ��� stdhead!!!
* �������������:
* stderr("������ ������", "��� ������� �����. ��������� �������� ������ � id �������.");
* stderr("", "", $header_url = array("url" => "index.php?id=N", "code" => 301));
* stderr("", "", $header_url = array("code" => 404)); /// ��������� ��: ����� ���.
**/

if (!empty($header_url["url"]) && $header_url["code"] <> 404){

header('Location: '.$header_url["url"], true, (isset($header_url["code"]) ? $header_url["code"]:301)); // header: 301 - ������������, 302 - ��������

} elseif (isset($header_url["code"]) && $header_url["code"] == 404) {

httperr(); /// ��������� �� ������: ���� �� ������ (header 404)

} else {

stdhead();
stdmsg($heading, $text, 'error');
stdfoot();

}
die;
}

function sqlerr($file = false, $line = false) {

global $CURUSER, $queries, $Mail_config, $tracker_lang;

$repair = false;
/// ��������� ������ ����� �� �������� ��� �����
$fp = @fopen(ROOT_PATH."/cache/sqlerror.txt", "a+");

$time = get_date_time();
$file = $_SERVER["REQUEST_URI"];
$getiing = serialize($_GET ? $_GET:"")."\r(-\-)\n".serialize($_POST ? $_POST:"");

if (!empty($file))
$file_view="� ����� $file,";
if (!empty($line))
$line_view="����� $line";

$mysql_error = mysql_error();

$error = $time." " . $mysql_error." ".$file_view." ".$line_view." ".($CURUSER["username"] ? get_user_class_name($CURUSER["class"]).": ".$CURUSER["username"].". IP: ".$CURUSER["ip"]:"");

if (@file_exists(ROOT_PATH."/cache/sqlerror.txt") && !stristr($mysql_error, "doesn't exist")){
$open = file_get_contents(ROOT_PATH."/cache/sqlerror.txt");

if (!stristr($mysql_error, "syntax") || filesize(ROOT_PATH."/cache/sqlerror.txt") >= '10386330')
unset($Mail_config['warning']);

if (!stristr($open, $mysql_error." ".$file_view." ".$line_view)){
$all = $error."\r\n ������ � SQL <b>����� �� ������� MySQL</b>: " . $mysql_error . ((!empty($file) && !empty($line)) ? " � <b>".$file."</b>, ����� <b>".$line."</b> " : "") . " ������ ����� ".$queries;

if ($_SERVER["SERVER_ADDR"] <> $_SERVER["REMOTE_ADDR"] && !empty($Mail_config['warning']))
sent_mail($Mail_config['warning'], $SITENAME, $SITEEMAIL, "������ ������� �� ����� ".$SITENAME, $all, false);

@fputs($fp,"\r".$error."#".$getiing."\n<#-/-!>"); /// ���������� ���� ����� ������ ��� ��� � �����.
}
}

@fclose($fp);

if (stristr($mysql_error,"repair") || stristr($mysql_error,"MYI")){

$repair = true;

/// ��������� �������� ������� ������� �� ������ � MYI �����������
preg_match("/'(.*?).MYI (USE_FRM)'/i", $mysql_error, $tabler);
$table = end(explode('/', $tabler[1]));

/// ��������� �������� ������� �������
if (empty($table)){
preg_match("/Table '.\/.*?\/(.*?)'/is", $mysql_error, $tabled);
$table = end(explode('/', $tabled[1]));
}

/// ������������� �������� ������� ������� �� ������ � � MYI �����������
if (empty($table)){
preg_match("/\\'(([a-zA-Z0-9]{3,20}).MYI|([a-zA-Z0-9]{3,20}))\\'/is", $mysql_error, $tablep);
$table = $tablep[1];
}

repair_table($table);

}

if (get_user_class() > UC_MODERATOR || stristr($mysql_error, "doesn't exist"))
stdmsg($tracker_lang['mysql_error'], "<b>".$tracker_lang['mysql_answer']."</b>: ".$mysql_error.((!empty($file) && !empty($line)) ? " F: <b>".$file."</b> L: <b>".$line."</b>" : "")." SQL #: <b>".$queries."</b>".(!empty($all) ? "<br />".$tracker_lang['mysql_ewrite']:"")."",(($repair==true || empty($all)) ? "success":"error"));
else
stdmsg($tracker_lang['mysql_error'], $tracker_lang['mysql_ewrite'], (($repair==true || empty($all)) ? "success":"error"));

}


function format_quotes($s) {

global $tracker_lang;

preg_match_all('/\\[quote.*?\\]/', $s, $result, PREG_PATTERN_ORDER);
$openquotecount = count($openquote = $result[0]);
preg_match_all('/\\[\/quote\\]/', $s, $result, PREG_PATTERN_ORDER);
$closequotecount = count($closequote = $result[0]);

if ($openquotecount != $closequotecount)
return $s;

$openval = array();
$pos = -1;

foreach($openquote as $val)
$openval[] = $pos = strpos($s,$val,$pos+1);
$closeval = array();
$pos = -1;

foreach($closequote as $val)
$closeval[] = $pos = strpos($s,$val,$pos+1);

for ($i=0; $i < count($openval); $i++)
if ($openval[$i] > $closeval[$i]) return $s;

$s = str_replace("[quote]", "<fieldset><legend><font class=\"editorinput\">".$tracker_lang['quote_data']."</font></legend><br />",$s);
$s = preg_replace("/\\[quote=(.+?)\\]/","<fieldset><legend><font class=\"editorinput\">\\1</font></legend>", $s);
$s = str_replace("[/quote]", "</fieldset>",$s);

return $s;

}

function regex_php_tag($php) {

$o = array("&#036;","&quot;","&#092;","&lt;","&gt;","&#39;","&#33;","&amp;","[php]","[/php]");
$w = array("$","\"","\\\\","<",">","'","!","&","","");

$php = str_replace($o,$w,$php);
//$php = str_replace("\n\r","",$php);

$code = stripslashes($php);
$md5 = md5($code);

$code = "<?php\n".trim($code)."\n?>";
$code = highlight_string($code,true);
$code = "<div style=\"margin: 3px 15px 15px;\"><div style=\"margin: 0px; padding: 3px; border: 1px inset; width: 500px; height: 150px; overflow: auto;\"><code style=\"white-space: nowrap;\">
".substr($code,79);
$code = substr($code,0,-70)."</code></div></div>";

return "<!--php md5: $md5-->{$code}<!--php md5: $md5-->";
}

function agetostr($age) {

global $tracker_lang;

$age = abs($age);
$t1 = $age % 10;
$t2 = $age % 100;

$exp = array_map('trim', explode(',', $tracker_lang['year_array']));

if ($t1 == 1) $str = $exp[0];
elseif ($t1 >= 2 && $t1 <= 4) $str = $exp[1];
if (($t1 >= 5) && ($t1 <= 9) || ($t1 == 0) || ($t2 >= 11) && ($t2 <= 19)) $str = $exp[2];

return $age." ".$str;
}

function get_user_id() {
global $CURUSER;
return $CURUSER["id"];
}

function comment_hide($text) {

global $tracker_lang, $CURUSER;

$html = "<div align=\"left\" style=\"width: 100%; overflow: auto\">
<table width=\"100%\" cellspacing=\"1\" cellpadding=\"3\" border=\"0\" align=\"center\">
<tr bgcolor=\"red\"><td><b>".$tracker_lang['hide_text']."</b></td></tr>
<tr><td align=\"center\"><color=\"red\">.:: ".$tracker_lang['hide_text_i']." ::.</color></td></tr>
</table></div>";

$start_html = "<div align=\"left\" style=\"width: 100%; overflow: auto\">
<table width=\"100%\" cellspacing=\"1\" cellpadding=\"3\" border=\"0\" align=\"center\">
<tr bgcolor=\"pink\"><td class=\"rowhead\">".$tracker_lang['hide_text']."</td></tr>
<tr class=\"bgcolor1\"><td>";

$end_html = "</td></tr></table></div>";

if (basename($_SERVER['SCRIPT_FILENAME']) == 'details.php'){
$id = (!empty($_GET["id"]) ? intval($_GET["id"]):0);
$res = sql_query("SELECT COUNT(*) FROM comments WHERE torrent = ".sqlesc($id)." AND user = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($res);
}

if (get_user_class() >= UC_MODERATOR || $row[0])
return preg_replace("#\[hide\](.*?)\[/hide\]#si", $start_html."\\1".$end_html, $text);
else
return preg_replace("#\[hide\](.*?)\[/hide\]#si", $html, $text);

}

function format_urls($s) {

return preg_replace("/(\A|[^=\|^]'\"a-zA-Z0-9])((http|ftp|https|ftps):\/\/[^()<>\s]+)/is", "\\1<a rel=\"nofollow\" href=\"redir.php?url=\\2\">\\2</a>", $s);
}


function format_comment($s, $connect_smilies = false) {

global $CURUSER, $DEFAULTBASEURL, $tracker_lang;




$host = basename($_SERVER['SCRIPT_FILENAME']);



///// ���� ����, ����������� �������� ��������
if (in_array($host, array("details.php", "gettorrentdetails.php", "index.php")) && strlen($s) > 5000 && !preg_match("#\[hide\]#si", $s)){

$md5_exist = md5($s);

$filecache = file_query("", $cache = array("type" => "disk", "file" => "descr_".$md5_exist, "time" => 86400*7, "action" => "get"));

if ($filecache <> false)
return $filecache;
}
///// ���� ����, ����������� �������� ��������





$site = parse_url($DEFAULTBASEURL, PHP_URL_HOST);

$s = str_replace(";)", ";-)", $s);

$counter = 0;
$match_count = preg_match_all("#\[bb\](.*?)\[/bb\]#si", $s, $matches);

if ($match_count) {

for ($mout = 0; $mout < $match_count; ++$mout){

$s_html = "<div style=\"width: 100%; overflow: auto\" align=\"center\"><table width=\"100%\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\" align=\"center\">
<tr><td colspan=\"2\" class=\"a\"><font class=\"block-title\">".$tracker_lang['bb_sourseb']."</font></td></tr>
<tr><td class=\"b\">";
$e_html = "</td></tr></table></div>";

$t_b = str_replace("\n", "<br />", $matches[1][$mout]);
$add_text[] = $s_html.$t_b.$e_html;
++$counter;
}
}

if (in_array($host, array("message.php", "details.php", "comment.php")) && preg_match("#\[php\](.*?)\[/php\]#si", $s))
$s = regex_php_tag($s);

$s = preg_replace("/\[b\](.*?)\[\/b\]/is", "<strong>\\1</strong>", $s); /// [b]������[/b]
$s = preg_replace("/\[i\](.*?)\[\/i\]/is", "<i>\\1</i>", $s); /// [i]������[/i]
$s = preg_replace("/\[h\](.*?)\[\/h\]/is", "<h3>\\1</h3>", $s); /// [h]�������[/h]
$s = preg_replace("/\[u\](.*?)\[\/u\]/is", "<u>\\1</u>", $s); /// [u]������������[/u]
$s = preg_replace("#\[s\](.*?)\[/s\]#si", "<s>\\1</s>", $s); /// [s]�����������[/s]
$s = preg_replace("#\[li\]#si", "<li>", $s); /// [li]
$s = preg_replace("#\[hr\]#si", "<hr>", $s); /// [hr]
$s = preg_replace("#\[br\]#si", "<br />", $s); /// [br]
$s = preg_replace("#\[align=(left|right|center|justify)\](.*?)\[/align\]#is", "<div align=\"\\1\">\\2</div>", $s);
$s = preg_replace("#\[size=([0-9]+)\](.*?)\[/size\]#si", "<span style=\"font-size: \\1\">\\2</span>", $s); /// [size=4]Text[/size]
$s = preg_replace("/\[font=([a-zA-Z ,]+)\](.*?)\[\/font\]/is","<font face=\"\\1\">\\2</font>", $s); /// [font=Arial]Text[/font]

$s = str_replace ("[pi]", "<div align=\"center\" style=\"font-size: 25px; width:auto; position:relative; float:center;\">&#8604; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &#9986; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &ndash; &#8605;</div>", $s); /// [pi]
$s = str_replace ("[me]", "<script type=\"text/javascript\" src=\"/js/blink.js\"></script><blink><font color=\"red\">IMHO</font></blink>&nbsp;", $s); /// [me]

$s = preg_replace("/\[audio\]([^()<>\s]+?)\[\/audio\]/is", "<embed autostart=\"false\" loop=\"false\" controller=\"true\" width=\"220\" height=\"42\" src=\"\\1\"></embed>", $s); /// [audio]http://allsiemens.com/mp3/files/1.mp3[/audio]

$s = preg_replace("/\[img\](http:\/\/[^\s'\"<>]+(\.(jpg|jpeg|gif|png)))\[\/img\]/is", "<img border=\"0\" src=\"\\1\" alt=\"\\1\" />", $s); /// [img]http://www/image.gif[/img]

$s = preg_replace("/\[img=(http:\/\/[^\s'\"<>]+(\.(gif|jpeg|jpg|png)))\]/is", "<img border=\"0\" src=\"\\1\" alt=\"\\1\" />", $s); /// [img=http://www/image.gif]

$s = preg_replace("/\[color=([a-zA-Z]+)\](.*?)\[\/color\]/is", "<font color=\"\\1\">\\2</font>", $s); /// [color=blue]�����[/color]

$s = preg_replace("/\[color=(#[a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9])\](.*?)\[\/color\]/is", "<font color=\"\\1\">\\2</font>", $s); /// [color=#ffcc99]�����[/color]


$lnk = array_map('trim', explode(',', $tracker_lang['lnk_array']));

if ($host == "shoutbox.php"){

$s = preg_replace("/\[url=(http:\/\/[^()<>\s]+?)\](.*?)\[\/url\]/is", "<a href=\"redir.php?url=\\1\" rel=\"nofollow\" target=\"_blank\">\\2</a>", $s); /// [url=http://www.example.com]������[/url]
$s = preg_replace("/\[url\](http:\/\/[^()<>\s]+?)\[\/url\]/is", "<a href=\"redir.php?url=\\1\" rel=\"nofollow\" target=\"_blank\">\\1</a>", $s); /// [url]http://www.example.com[/url]

} else {

$s = preg_replace("/\[url=(http:\/\/".preg_quote($site)."+(\/|.*?))\](.*?)\[\/url\]/is", "<a title=\"".$lnk[0]."\" href=\"\\1\" rel=\"nofollow\">\\3</a>", $s); /// ��� ������ ����: http://localhost
$s = preg_replace("/\[url\](http:\/\/".preg_quote($site)."+(\/|.*?))\[\/url\]/is", "<a href=\"\\1\" title=\"".$lnk[0]."\" rel=\"nofollow\">\\1</a>", $s); /// ��� ������ ����: http://localhost

$s = preg_replace("/\[url\](http:\/\/[^()<>\s]+?)\[\/url\]/is", "<a title=\"".$lnk[1]."\" href=\"redir.php?url=\\1\" rel=\"nofollow\" target=\"_self\">\\1</a>", $s);
$s = preg_replace("/\[url=(http:\/\/[^()<>\s]+?)\](.*?)\[\/url\]/is", "<a title=\"".$lnk[1]."\" href=\"redir.php?url=\\1\" rel=\"nofollow\" target=\"_self\">\\2</a>", $s);

}

$s = preg_replace("/\[url=([a-zA-Z]+.php(.*?))\](.*?)\[\/url\]/is", "<a title=\"".$lnk[0]."\" href=\"\\1\" rel=\"nofollow\">\\3</a>", $s); /// [url=browse.php?search=�������]�������[/url]
$s = preg_replace("/(\\n|\\s|^)((http|ftp|https|ftps):\/\/[^()<>\s]+)/is", "\\1<a title=\"".$lnk[2]."\" rel=\"nofollow\" href=\"redir.php?url=\\2\">\\2</a>", $s);

// ������ �������� � ����� �����������
//	$s = preg_replace( "#<(\s+?)?s(\s+?)?c(\s+?)?r(\s+?)?i(\s+?)?p(\s+?)?t#is", "&lt;script", $s );
//	$s = preg_replace( "#<(\s+?)?/(\s+?)?s(\s+?)?c(\s+?)?r(\s+?)?i(\s+?)?p(\s+?)?t#is", "&lt;/script", $s );

$s = str_replace(array("javascript", "alert", "<body", "<html"), "", $s);

$s = format_quotes($s);

if ($host == 'details.php' && preg_match("#\[hide\](.*?)\[/hide\]#si", $s))
$s = comment_hide($s);

$s = nl2br($s);

$s = preg_replace("/\[pre\](.*?)\[\/pre\]/is", "<pre>".htmlspecialchars('\\1')."</pre>", $s); /// [pre]Preformatted[/pre]

$s = preg_replace("/\[highlight\](.*?)\[\/highlight\]/is", "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\"><tr><td bgcolor=\"white\"><b>\\1</b></td></tr></table>", $s); /// [highlight]Highlight text[/highlight]

$s = preg_replace("/\[hideback\](.*?)\[\/hideback\]/is", "<span onmouseout=\"this.style.color='#DDDDDD';\" onmouseover=\"this.style.color='#002AFF';\" style=\"background: #DDDDDD none repeat scroll 0% 0%;font-weight: bold; font-size: small;  color: #DDDDDD; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous; cursor: help;\">\\1</span>", $s);

$s = preg_replace("/\[legend=(.*?)\](.*?)\[\/legend\]/is", "<fieldset><legend>\\1</legend>\\2</fieldset>", $s);
$s = preg_replace("/\[legend\](.*?)\[\/legend\]/is", "<fieldset>\\1</fieldset>", $s);  //<legend></legend>
$s = preg_replace("/\[marquee\](.*?)\[\/marquee\]/is", "<marquee behavior=\"alternate\">\\1</marquee>", $s); /// [marquee]Marquee[/marquee]

$s = preg_replace("/\[flash=(\d{1,3}):(\d{1,3})\]((www.|http:\/\/|https:\/\/)[^\s]+(\.swf))\[\/flash\]/is", "<param name=\"movie\" value=\\3/><embed width=\"\\1\" height=\"\\2\" src=\"\\3\"></embed>", $s, 3); ///[flash=320x240]http://somesite.com/test.swf[/flash]

$s = preg_replace("/\[flash]((www.|http:\/\/|https:\/\/)[^\s'\"<>&]+(\.swf))\[\/flash\]/is", "<param name=\"movie\" value=\"\\1\" /><embed width=\"470\" height=\"310\" src=\\1></embed>", $s, 3); ///[flash]http://somesite.com/test.swf[/flash]

$s = preg_replace("/\[mcom=(#[a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9]):(#[a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9])\](.*?)\[\/mcom\]/is", "<div style=\"background-color: \\1; color: \\2; font-weight: bold; font-size: small;\">\\3</div>", $s); /// [mcom=#FFD42A:#002AFF]Text[/mcom]

$s = wordwrap($s, 150, "\n", 1); //linebreak - �� ��������� 75 - ���� 100

$s = preg_replace("/\[video=(https?:\\/\\/(?:[a-z\\d-]+\\.)?youtu(?:be(?:-nocookie)?\\.com\\/.*v=|\\.be\\/|.*)([-\\w]{11})(?:.*[\\?&#](?:star)?t=([\\dhms]+))?)\]/is", "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/\\2\" frameborder=\"0\" allowfullscreen></iframe>", $s);

$s = preg_replace("/\[video=(.*?(youtube-nocookie.com|youtube.com).*?(\/v\/([-\\w]{11})|watch\?v=([-\\w]{11})|\/embed\/([-\\w]{11})).*?)\]/is", "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/\\4\" frameborder=\"0\" allowfullscreen></iframe>", $s);



global $nummatch; /// [spoiler]��� �������[/spoiler]

while (preg_match("/\[spoiler=(.*?)\](.*?)\[\/spoiler\]/is", $s)) {
++$nummatch;
$s = preg_replace("/\[spoiler=(.*?)\](.*?)\[\/spoiler\]/is", "<div class=\"spoiler-wrap\" id=\"".($nummatch)."\"><div title=\"".$tracker_lang['open_spoiler']."\" class=\"spoiler-head folded clickable ClickEnD\">".trim('\\1')."</div><div class=\"spoiler-body\" style=\"display: none;\">".trim('\\2')."<div class=\"spoiler-foot ClickEnD\"><span style=\"line-height: normal;\">".$tracker_lang['close_spoiler'].": ".trim('\\1')."</span></div></div></div>", $s, 1);
if ($nummatch > 200) break;
}

while (preg_match("/\[spoiler\](.*?)\[\/spoiler\]/is", $s)) {
++$nummatch;
$s = preg_replace("/\[spoiler\](.*?)\[\/spoiler\]/is", "<div class=\"spoiler-wrap\" id=\"".($nummatch)."\"><div title=\"".$tracker_lang['open_spoiler']."\" class=\"spoiler-head folded clickable ClickEnD\">".$tracker_lang['open_spoiler']."</div><div class=\"spoiler-body\" style=\"display: none;\">".trim('\\1')."<div class=\"spoiler-foot ClickEnD\"><span style=\"line-height: normal;\">".$tracker_lang['close_spoiler']."</span></div></div></div>", $s,1);

if ($nummatch > 200) break;
}


if ($connect_smilies == true) {

global $smilies;
$num = 0;

reset($smilies);
while (list($code, $url) = each($smilies))

if ($host == "shoutbox.php")
$s = str_replace($code, "<img title=\"Click me!\" border=\"0\" onclick=\"parent.document.tesla.chatText.focus();parent.document.tesla.chatText.value=parent.document.tesla.chatText.value+'".addcslashes($code, "'")." ';return false;\" src=\"/pic/smilies/".$url."\" alt=\"".htmlspecialchars_uni($code)."\" />", $s);
else
$s = str_replace($code, "<img border=\"0\" src=\"/pic/smilies/".$url."\">", $s);

unset($smilies);
}

if (!empty($counter)) {
$is = 0;
while ($is < $counter) {
$s = preg_replace("#\[bb\](.*?)\[/bb\]#si", $add_text[$is], $s, 1);
++$is;
}
}

///// ���� ����, ����������� �������� ��������
if (isset($md5_exist))
file_query($s, $cache = array("type" => "disk", "file" => "descr_".$md5_exist, "time" => 86400*7, "action" => "set"));
///// ���� ����, ����������� �������� ��������


return $s;

}



function is_valid_user_class($class) {
return is_numeric($class) && floor($class) == $class && $class >= UC_USER && $class <= UC_SYSOP;
}

function is_valid_id($id) {
return is_numeric($id) && ($id > 0) && (floor($id) == $id);
}

function sql_timestamp_to_unix_timestamp($s) {
return mktime(substr($s, 11, 2), substr($s, 14, 2), substr($s, 17, 2), substr($s, 5, 2), substr($s, 8, 2), substr($s, 0, 4));
}

function get_ratio_color($ratio) {

if ($ratio < 0.1) return "#ff0000";
if ($ratio < 0.2) return "#ee0000";
if ($ratio < 0.3) return "#dd0000";
if ($ratio < 0.4) return "#cc0000";
if ($ratio < 0.5) return "#bb0000";
if ($ratio < 0.6) return "#aa0000";
if ($ratio < 0.7) return "#990000";
if ($ratio < 0.8) return "#880000";
if ($ratio < 0.9) return "#770000";
if ($ratio < 1) return "#660000";

return "#000000";

}



function get_slr_color($ratio) {

if ($ratio < 0.025) return "#ff0000";
if ($ratio < 0.05) return "#ee0000";
if ($ratio < 0.075) return "#dd0000";
if ($ratio < 0.1) return "#cc0000";
if ($ratio < 0.125) return "#bb0000";
if ($ratio < 0.15) return "#aa0000";
if ($ratio < 0.175) return "#990000";
if ($ratio < 0.2) return "#880000";
if ($ratio < 0.225) return "#770000";
if ($ratio < 0.25) return "#660000";
if ($ratio < 0.275) return "#550000";
if ($ratio < 0.3) return "#440000";
if ($ratio < 0.325) return "#330000";
if ($ratio < 0.35) return "#220000";
if ($ratio < 0.375) return "#110000";

return "#000000";

}

function get_comm_color($stepen) {

if ($stepen < 5) return "#2e0101";
if ($stepen < 10) return "#610000";
if ($stepen < 15) return "#960505";
if ($stepen < 20) return "#930000";
if ($stepen < 25) return "#460088";
if ($stepen < 25) return "#460088";
if ($stepen < 25) return "#460088";
if ($stepen < 30) return "#000388";
if ($stepen < 35) return "#0005c5";
if ($stepen < 40) return "#0208ff";
if ($stepen < 50) return "#3802ff";
if ($stepen < 60) return "#7902ff";
if ($stepen < 70) return "#af02ff";
if ($stepen < 80) return "#ff02f0";
if ($stepen < 90) return "#ff029d";
if ($stepen <> 100) return "#ff023e";

return "#000000";
}

function write_log($text, $color = "transparent", $type = "tracker") {

if (empty($color)){
global $CURUSER;
$color = get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]);
}

sql_query("INSERT INTO sitelog (added, color, txt, type) VALUES (".sqlesc(get_date_time()).", ".sqlesc($color).", ".sqlesc(trim($text)).", ".sqlesc($type).")");

}

function check_banned_emails ($email, $return = false) {

$allowed_emails = array("rambler.ru","gmail.com","live.ru","mail.ru","idknet.com","yandex.ru");

$exp = explode("@", $email);

if (in_array($exp[1], $allowed_emails))
return false;

if ($return == true)
return true;
else {

global $tracker_lang;

stderr($tracker_lang['error'], $tracker_lang['emailbanned']."<br /><strong>".$tracker_lang['signup_banemails']."</strong>: ".implode(", ", $allowed_emails), false);

}

}

function random_color() {

$allsymb = "0123456789ABCDEF";
$color = "#";

for ($i = 0; $i < 6; $i = $i) {
$color.= $allsymb[rand(0,15)];
++$i;
}

return $color;

}

function mtime($time, $ntime, $end1, $end2, $end3) {

$mtime = $time;
$time = $mtime [strlen ( $mtime ) - 1];
$time2 = $mtime [strlen ( $mtime ) - 2] . $mtime [strlen ( $mtime ) - 1];

return $mtime . " " . $ntime . ($time > 1 && $time < 5 && ! ($time2 >= 10 && $time2 < 20) ? $end1 : ($time >= 5 || $time == 0 || $time2 >= 10 && $time2 < 20 ? $end2 : ($time == 1 ? $end3 : "")));
}


function get_elapsed_time($U, $showseconds = true, $ligth = false){

global $tracker_lang;

$ye_a = array_map('trim', explode(', ', $tracker_lang['year_array']));
$mo_a = array_map('trim', explode(', ', $tracker_lang['month_array']));
$we_a = array_map('trim', explode(', ', $tracker_lang['week_array']));
$da_a = array_map('trim', explode(', ', $tracker_lang['day_array']));
$ho_a = array_map('trim', explode(', ', $tracker_lang['hour_array']));
$mi_a = array_map('trim', explode(', ', $tracker_lang['minute_array']));
$se_a = array_map('trim', explode(', ', $tracker_lang['second_array']));

if ($ligth == true){

$mins = floor((gmtime() - $U) / 60);
$hours = floor($mins / 60);
$mins -= $hours * 60;
$days = floor($hours / 24);
$hours -= $days * 24;
$weeks = floor($days / 7);
$days -= $weeks * 7;

if ($weeks > 0)
return $weeks." ".tolower($we_a[1]);
if ($days > 0)
return $days." ".tolower($da_a[2]);
if ($hours > 0)
return $hours." ".tolower($ho_a[2]);
if ($mins > 0)
return $mins." ".tolower($mi_a[0]);

return "< 1 ".tolower($mi_a[2]);

} else {

if (empty($U))
return "N/A";

$N = time();

if ($N>=$U)
$diff = $N-$U;
else
$diff = $U-$N;

if($diff>=31536000){
$Iyear = floor($diff/31536000);
$diff = $diff-($Iyear*31536000);
}
if ($diff>=2629800){    //2592000 seconds in month with 30 days
$Imonth = floor($diff/2629800);
$diff = $diff-($Imonth*2629800);
}
if ($diff>=604800){
$Iweek = floor($diff/604800);
$diff = $diff-($Iweek*604800);
}
if ($diff>=86400){
$Iday = floor($diff/86400);
$diff = $diff-($Iday*86400);
}
if ($diff>=3600){
$Ihour = floor($diff/3600);
$diff = $diff-($Ihour*3600);
}
if ($diff>=60){
$Iminute = floor($diff/60);
$diff = $diff-($Iminute*60);
}
if ($diff>0)
$Isecond = floor($diff);

$j = " ";
$ret = "";

if (isset($Iyear)) $ret .= $Iyear." ".rusdate($Iyear, 'year', $ye_a).$j;
if (isset($Imonth)) $ret .= $Imonth." ".rusdate($Imonth, 'month', $mo_a).$j;
if (isset($Iweek)) $ret .= $Iweek." ".rusdate($Iweek, 'week', $we_a).$j;
if (isset($Iday)) $ret .= $Iday." ".rusdate($Iday, 'day', $da_a).$j;

if ((empty($Imonth) && empty($Iyear))){

if (isset($Ihour)) $ret .= $Ihour." ".rusdate($Ihour, 'hour', $ho_a).$j;
if (isset($Iminute)) $ret .= $Iminute." ".rusdate($Iminute, 'minute', $mi_a).$j;

if ($showseconds == false && $Iminute<1 && $Ihour<1 && $Iday<1 && $Iweek<1 && $Imonth<1 && $Iyear<1)
return rusdate(0, 'minute', $mi_a);

if ((!empty($Isecond) OR !empty($ret)) AND $showseconds==true){
if(!empty($ret) AND !isset($Isecond)) $Isecond = 0;
$ret .= $Isecond ." ".rusdate($Isecond, 'second', $se_a).$j;
}
}

if (empty($ret))
$ret = "1 ".rusdate($Isecond, 'second', $se_a).$j;

return $ret;
}

}

function rusdate($num, $type, $array) {

$array = array_map('tolower', $array);

$rus = array(
"year" => array($array[2], $array[0], $array[1], $array[1], $array[1], $array[2], $array[2], $array[2], $array[2], $array[2]),
"month" => array($array[2], $array[0], $array[1], $array[1], $array[1], $array[2], $array[2], $array[2], $array[2], $array[2]),
"week" => array($array[2], $array[0], $array[1], $array[1], $array[1], $array[2], $array[2], $array[2], $array[2], $array[2]),
"day" => array($array[2], $array[0], $array[1], $array[1], $array[1], $array[2], $array[2], $array[2], $array[2], $array[2]),
"hour" => array($array[2], $array[0], $array[1], $array[1], $array[1], $array[2], $array[2], $array[2], $array[2], $array[2]),
"minute" => array($array[0], $array[1], $array[2], $array[2], $array[2], $array[0], $array[0], $array[0], $array[0], $array[0]),
"second" => array($array[1], $array[1], $array[2], $array[2], $array[2], $array[1], $array[1], $array[1], $array[1], $array[1])
);

$num = intval($num);

if (10 < $num && $num < 20)
return $rus[$type][0];

return $rus[$type][$num % 10];

}



function toupper($content) {

return strtoupper(strtr($content, "��������������������������������","�����Ũ��������������������������"));

}


function tolower($content) {

return mb_strtolower(strtr($content, "�����Ũ��������������������������","��������������������������������"), 'cp1251');

}

/// �������� ������ �� ������������
function validusername($username) {

if (empty($username))
return false;

$allowedchars = "@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_ �������������������������������������Ũ��������������������������";
for ($i = 0; $i < strlen($username); ++$i)
if (strpos($allowedchars, $username[$i]) === false)
return false;

return true;
}

/// �������� url
function valid_url($url_im, $return_var = false) {

if (empty($url_im)) return ($return_var == true ? '':false);

if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $url_im)) 
return ($return_var == true ? $url_im: true);

return ($return_var == true ? '':false);
}

function imageshack($link) {
# ������� ��������� ��������, � ������ ������������� �� imagehost
return false; 

}



/// ���������� ����� ����� �� ������ (��������)
function tags_f_descr($descr, $and_years = false) {

if (empty($descr)) return false;
$descr = htmlspecialchars_uni($descr);

if (strlen($descr) >= 1048) $descr =  substr($descr, 0, 1048); /// �������� ������ 2048 ������� ��� �������� ������
$arr_n = explode("\n", $descr); /// ������ ������������ � ������

$oldtag = "";
foreach ($arr_n AS $arr_sea) { /// ���������� �� �������, � � ����� �������� �� ����������
if (preg_match("/(\[b\]����:\s\[\/b\]|\[b\]����\[\/b\]:|\[b\]�����\[\/b\]:|\[b\]����:\[\/b\]|\[b\]����\[\/b\]|\[b\]�����:\[\/b\]|\[b\]�����:\s\[\/b\])(.*?)$/i", $arr_sea, $arr_view)){
$oldtag = $arr_view[2]; break;
}
}

$my_year = false;
if ($and_years == true AND preg_match("/(��� ������|��� �������|���).*?([0-9]{4}).*?\n/is", $descr, $arr_year)){
if (is_numeric($arr_year[2])) $my_year = trim($arr_year[2]);
}

unset($arr_n, $descr);

if (empty($oldtag)) return false;

unset($arr_view);

$oldtag = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", strip_tags($oldtag))); /// �������� ��� [] � () ����
$oldtag = preg_replace('/\.(.*)$/is', "\\1", $oldtag);

$newtags = explode(",", $oldtag); /// ������ � ������ ������


$preg_punct = trim(preg_replace('/([\x7F-\xFFA-Za-z�-��-�0-9\s])/i', "", $oldtag));
if (!empty($preg_punct)){

function count_chars_unicode($str, $x = false) {
    
    $tmp = preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY);
    foreach ($tmp as $c) {
        $chr[$c] = isset($chr[$c]) ? $chr[$c] + 1 : 1;
    }
    
    return is_bool($x) ? ($x ? $chr : count($chr)) : $chr[$x];
}

$my_variabl = '';
$exp_mas = str_split($preg_punct); $exp_mas = array_unique($exp_mas);
foreach ($exp_mas AS $bite){

if (empty($bite)) continue;
$counted = count_chars_unicode($oldtag, $bite);
if ($counted > $num){
$my_variabl = $bite;
$num = $counted;
}

}

if (!empty($my_variabl) AND $my_variabl != ','){

$newtags_duo = explode($my_variabl, $oldtag); /// ������ � ������ ������

foreach ($newtags_duo AS $keys => $vakue){
if (preg_match("/,/is", $vakue)){

$newtags_duo[$keys] = false;
$newtags_duo = array_merge_recursive($newtags_duo, array_map("trim", explode(",", $vakue)));

}
}

}

if (count($newtags) != count($newtags_duo) AND count($newtags_duo) > count($newtags) )
$newtags = $newtags_duo;

}

$tagarray = array(); /// ������� ������ ������
unset($oldtag);

if (is_array($newtags) && count($newtags)){
foreach ($newtags AS $tagtrim){ /// ������� ������� � ������� �������� �� ������
$tagtrim = trim(preg_replace("/\.$/i", "", $tagtrim));
$tagtrim = trim(str_replace(array("+",":",";","/","[","]","|","*","--","%"), "", $tagtrim));
if (!empty($tagtrim) && !preg_match('/^([0-9]+)$/is', $tagtrim) AND count(explode(" ", $tagtrim)) < 5 ) $tagarray[] = $tagtrim;
}
} else return false;

$tagarray = array_map('tolower', $tagarray);

if ($my_year <> false) $tagarray[] = $my_year;

if (is_array($tagarray) && count($tagarray))
return array_unique($tagarray);
else
return false;

}  
    

function parse_referer ($light = false) {

if ($light == true) return false;

global $Signup_Config, $CURUSER, $DEFAULTBASEURL;

/// ����� ������
$referer = (isset($_SERVER["HTTP_REFERER"]) ? htmlspecialchars_uni($_SERVER["HTTP_REFERER"], false, true):"");
if (!empty($referer)) $parse_site = parse_url($referer, PHP_URL_HOST);

if (empty($parse_site) || empty($referer) || count(parse_url($parse_site)) == 0) return false;

if (preg_match("/(CHARACTER\_MAXIMUM\_LENGTH|INFORMATION\_SCHEMA\.COLUMNS|\+TABLE_NAME\+)/is", $parse_site)) return false;

/// ����������� ������ � �����
$parse_owner = parse_url($DEFAULTBASEURL, PHP_URL_HOST);

/// ���������� ������
if ($Signup_Config['refer_parse'] == false || ($parse_owner == $parse_site) || in_array(str_replace("www.", "", $parse_site), $Signup_Config['refer_stop_url'])) return false;
if (stristr($parse_site, $parse_owner) || str_replace("www.", "", $parse_site) == str_replace("www.", "", $parse_owner)) return false;

$requrl = htmlspecialchars_uni($_SERVER["REQUEST_URI"]);
$ip = getip();

$uid = (!empty($CURUSER["id"]) ? $CURUSER["id"]:0);

$exicount = get_row_count("referrers", "WHERE uid = ".sqlesc($uid)." AND ip = ".sqlesc($ip)." AND parse_url = ".sqlesc($parse_site)." AND parse_ref = ".sqlesc($referer));

if (empty($exicount)){
//// ����� ������ �� ����� �� ������, ������ �����
sql_query("INSERT INTO referrers (parse_url, parse_ref, req_url, uid, ip, date, numb) VALUES (".sqlesc($parse_site).", ".sqlesc($referer).", ".sqlesc($requrl).", ".sqlesc($uid).", ".sqlesc($ip).", ".sqlesc(get_date_time()).", '1')") or sqlerr(__FILE__,__LINE__);

/// �������� ��� ���� ����������
//$parse_ref = urldecode($referer);
$words_search = parse_url_query($referer);
if (!in_array(basename($_SERVER["SCRIPT_FILENAME"]), array("404.php")) && !empty($words_search['query']) && !stristr($referer, $parse_owner)) {

sql_query("UPDATE meta SET numb = numb+1 WHERE crcfile = ".sqlesc(crc32($requrl))." AND metakey = ".sqlesc($words_search['query'])) or sqlerr(__FILE__,__LINE__);
if (!mysql_modified_rows())
sql_query("INSERT IGNORE INTO meta (numb, crcfile, metakey, req_url) VALUES (1, ".sqlesc(crc32($requrl)).", ".sqlesc($words_search['query']).", ".sqlesc($requrl).")"); # or sqlerr(__FILE__,__LINE__); //// ��� ������� ����� ������

unsql_cache("meta_".crc32($requrl));

}
/// �������� ��� ���� ����������

} else 
sql_query("UPDATE referrers SET numb = numb+1, lastdate = ".sqlesc(get_date_time()).", req_url = ".sqlesc($requrl)." WHERE uid = ".sqlesc($uid)." AND ip = ".sqlesc($ip)." AND parse_url = ".sqlesc($parse_site)." AND parse_ref = ".sqlesc($referer)) or sqlerr(__FILE__,__LINE__);


/// ������ ���������� �� ����������� �������
if (in_array(date('H'), array("10", "18", "24"))) {

$res = sql_query("SELECT value_u FROM avps WHERE arg = 'referrers'", $cache = array("file" => "check_trefs", "time" => 60*60));
$row = mysql_fetch_assoc_($res);

if ($row["value_u"] < time()){

global $Signup_Config;

$ferrers = array();

/// �������� �������� ������� (������)
if (empty($Signup_Config['refer_day'])) $Signup_Config['refer_day'] = 2; /// ������� ������ ������ ������ N �������
sql_query("DELETE FROM referrers WHERE date < ".sqlesc(get_date_time(gmtime() - $Signup_Config['refer_day'] * (86400 * 31)))) or sqlerr(__FILE__,__LINE__);
$ferrers[] = "Del_oldtime: ".mysql_modified_rows();

/// ��������� ���������
sql_query("UPDATE IGNORE referrers SET uid = '0' WHERE referrers.uid <> '0' AND (SELECT COUNT(*) FROM users WHERE users.id = referrers.uid) < '1'") or sqlerr(__FILE__,__LINE__);
$ferrers[] = "Old_uid: ".mysql_modified_rows();

/// ����������� id �� users ������� ��� ���������
sql_query("UPDATE IGNORE referrers SET referrers.uid = (SELECT id FROM users WHERE users.ip = referrers.ip LIMIT 1) WHERE referrers.uid = '0' AND (SELECT COUNT(*) FROM users WHERE users.ip = referrers.ip) = '1'") or sqlerr(__FILE__,__LINE__);
$ferrers[] = "New_uid: ".mysql_modified_rows();

sql_query("DELETE FROM referrers WHERE parse_ref = '' OR parse_url = ''") or sqlerr(__FILE__,__LINE__);
$ferrers[] = "Del_uid: ".mysql_modified_rows();

if (empty($row["value_u"]))
sql_query("INSERT INTO avps (arg, value_u, value_i, value_s) VALUES ('referrers', ".sqlesc((time()+86400)).", '_', ".sqlesc(serialize($ferrers)).")");
elseif (!empty($row["value_u"]))
sql_query("UPDATE avps SET value_u = ".sqlesc((time()+86400)).", value_i = '_', value_s = ".sqlesc(serialize($ferrers))." WHERE arg = 'referrers'") or sqlerr(__FILE__,__LINE__);

unsql_cache("block-top_refer");
//unsql_cache("check_trefs");
}


}

/// ����� ���������� �� ����������� �������

return true;

}


//// ��������� ������, ���� ���� �� ����� ����������
function parse_url_query($ref_data) {

if (empty($ref_data)) return false;

$tmp = parse_url($ref_data);

if (preg_match('#(msn|live|aport|gogel|google|altavista|gotorrent.net|utorrent.inspsearch.com|hi.ru|ask|yahoo|aol|yandex|mail.ru|rambler.ru|findsmarter.ru|bing.com|nigma.ru|twitter.com|search.tut.by|qip.ru\/search)#i', $ref_data)) {

if (preg_match("#[\?\&](q|p|r|query|keywords|text|s)=(([^&]+){2,})#i", $ref_data, $match))
$me_search = urldecode($match[2]);

}

$me_search = preg_replace("/(.*?)\&(.*?)=.*?/ui", "\\1", $me_search);
$me_search = preg_replace("/\"(.*?)\"/ui", "\\1", $me_search); /// ������� "�� �����"

if (!empty($me_search) && !is_valid_id($me_search))
return array("host" => $tmp["host"], "query" => trim($me_search));
else
return false;

}




function incldead ($type = 1, $light = false){

global $tracker_lang;

$incl = array();

if ($type == 1){

$incl[1] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_active']):$tracker_lang['torrent_active']);
$incl[2] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_noactive']):$tracker_lang['torrent_noactive']);
$incl[3] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_golden']):$tracker_lang['torrent_golden']);
$incl[4] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_sticky']):$tracker_lang['torrent_sticky']);
$incl[5] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_nomoderate']):$tracker_lang['torrent_nomoderate']);
$incl[6] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_banned_is']):$tracker_lang['torrent_banned_is']);
$incl[7] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_notags']):$tracker_lang['torrent_notags']);

if (get_user_class() > UC_USER)
$incl[8] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_my']):$tracker_lang['torrent_my']);

if (get_user_class() > UC_USER)
$incl[9] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_noimage']):$tracker_lang['torrent_noimage']);

$incl[10] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_multi']):$tracker_lang['torrent_multi']);
$incl[11] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_scrins']):$tracker_lang['torrent_scrins']);
$incl[14] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['torrent_privat']):$tracker_lang['torrent_privat']);

} elseif ($type == 2){

$incl[0] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_no']):$tracker_lang['sort_no']);
$incl[1] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_name']):$tracker_lang['sort_name']);
$incl[2] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_numfile']):$tracker_lang['sort_numfile']);
$incl[3] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_numcomm']):$tracker_lang['sort_numcomm']);
$incl[4] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_added']):$tracker_lang['sort_added']);
$incl[5] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_size']):$tracker_lang['sort_size']);
$incl[6] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_cat']):$tracker_lang['sort_cat']);
$incl[7] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_peers']):$tracker_lang['sort_peers']);
$incl[8] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_leech']):$tracker_lang['sort_leech']);
$incl[9] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_owner']):$tracker_lang['sort_owner']);
$incl[11] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_moderdate']):$tracker_lang['sort_moderdate']);
$incl[12] = ($light == true ? preg_replace("/\(((\s|.)+?)\)/is", "", $tracker_lang['sort_multitime']):$tracker_lang['sort_multitime']);

} elseif ($type == 3){

$incl[0] = ($light == true ? end(explode(" ", $tracker_lang['for_name_is'])):$tracker_lang['for_name_is']);
$incl[1] = ($light == true ? end(explode(" ", $tracker_lang['for_desck_is'])):$tracker_lang['for_desck_is']);
$incl[2] = ($light == true ? end(explode(" ", $tracker_lang['for_file_is'])):$tracker_lang['for_file_is']);

if (get_user_class() >= UC_MODERATOR)
$incl[3] = ($light == true ? end(explode(" ", $tracker_lang['for_history_is'])):$tracker_lang['for_history_is']);
$incl[4] = ($light == true ? end(explode(" ", $tracker_lang['for_infohash_is'])):$tracker_lang['for_infohash_is']);

}

if (count($incl))
return $incl;
else
return false;

}


if (!function_exists("is_spamer")) {
function is_spamer ($username, $email){

/**
 * ������� ������ � ����������� ���� ��� ����� (�� �����, ������). ����� - ������, �� ������� max 100%.
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

$exp = explode("@", $email); /// ��������� �� ����� � �����
$proc = similar_text($username, $exp[0]); /// ��������

if ($proc <= 5) $proc = 0; /// ���� ������� ����� ������ 50% (�� 100% �����������) �� �������� ������

if (strlen($exp[1]) >= 10) $proc += strlen($exp[1]); /// ���� ����� ��������� ������ >= 10 ������ ������ � ������
if (strlen($exp[0]) >= 12) $proc += +strlen($exp[0]); /// ���� ����� ������ >= 12 ������ ������ � ������

$text1 = preg_replace("/s(w+s)1/i", "$1", $email);  /// �������� ������������� ���� (�� ������������ � ��������)
$text2 = preg_replace("/.+/i", ".", $email); /// �������� ������������� ����������

if ($text1 <> $email) /// ����������� ��� ���������� � ������������ �������� - �� ������ ����������
$proc += (strlen($email) - strlen($text1));

if ($text2 <> $email) /// ����������� ��� ���������� � ������������ �������� - �� ������ ����������
$proc += (strlen($email) - strlen($text2));

$et = count(explode(".", $exp[0])); /// ������ ���������� ����� � ������ �����

if ($et >= 10) $proc = $proc*4; /// ���� ���������� ����� � ������ ��������� 10 �������� �� ����������� �����
elseif ($et >= 3) $proc = $proc*3; /// ���� ���������� ����� � ������ ��������� 4 �������� �� ����������� (�������) ����� 

foreach (explode(".", $exp[1]) AS $dag){
if ($dag == "co" || $dag == "cc" || $dag == "pl" || $dag == "jp") $proc += 25; /// ���� � ������ ���� ���� �����, ����������� �� ���������� ����� - 25%
}

return min(100, $proc); /// ������� ������, ������������� max �������� 100.

}

}


function uploadimage ($x, $imgname = false, $tid, $where = false) {

global $Torrent_Config, $Functs_Patch, $tracker_lang;

$allowed_types = array("image/gif" => "gif", "image/png" => "png", "image/jpeg" => "jpeg", "image/jpg" => "jpg");

if (isset($_FILES[$x]['name']) && !empty($_FILES[$x]['name'])) {

if (!array_key_exists($_FILES[$x]['type'], $allowed_types) || !preg_match('/^(.+)\.(jpg|jpeg|png|gif)$/si', $_FILES[$x]['name']))
stderr($tracker_lang['tfile_notype'], $tracker_lang['invalid_typeimage']." ".$tracker_lang['avialable_formats'].": ".implode(", ", $allowed_types));

if ($_FILES[$x]['size'] > $Torrent_Config["max_image_size"] || $_FILES[$x]['size'] < 1024)
stderr($tracker_lang['error'], $tracker_lang['image_is'].": ".$x.". ".sprintf($tracker_lang['image_isbig'], mksize($Torrent_Config["max_image_size"])));

$uploaddir = ROOT_PATH."/torrents/images/"; /// Make sure is same as on takeupload.php
$ifile = $_FILES[$x]['tmp_name']; /// What is the temporary file name?

$image = file_get_contents($_FILES[$x]['tmp_name']);
if ($image == false)
stderr($tracker_lang['error'], $tracker_lang['dox_cannot_move']);

validimage($_FILES[$x]['tmp_name'], "takeedit");

if (!empty($imgname))
@unlink(ROOT_PATH."/torrents/images/".$imgname);

$ifilename = $tid.(!empty($where) ? $where:"").'.'.end(explode('.', $_FILES[$x]['name']));

$copy = copy($ifile, $uploaddir.$ifilename);
if ($copy == false)
stderr($tracker_lang['error'], $tracker_lang['dox_cannot_move']." ".$tracker_lang['image_is'].":".$x);


////// ������� ���� //////
if ($Functs_Patch["watermark"] == true){

$margin = 7;
$ifn = $uploaddir.$ifilename;

$watermark_image_light = ROOT_PATH.'/pic/watermark_light.png';
$watermark_image_dark =  ROOT_PATH.'/pic/watermark_dark.png';


list($image_width, $image_height) = getimagesize($ifn);
list($watermark_width, $watermark_height) = getimagesize($watermark_image_light);

$watermark_x = $image_width - $margin - $watermark_width;
$watermark_y = $image_height - $margin - $watermark_height;
$watermark_x2 = $watermark_x + $watermark_width;
$watermark_y2 = $watermark_y + $watermark_height;

if ($watermark_x < 0 OR $watermark_y < 0 OR $watermark_x2 > $image_width OR $watermark_y2 > $image_height OR $image_width < $min_image OR $image_height < $min_image)
return;

$test123 = imagecreatetruecolor(1, 1);

if ($_FILES[$x]['type']=="image/gif") $creimg=imagecreatefromgif($ifn);
elseif ($_FILES[$x]['type']=="image/png") $creimg=imagecreatefrompng($ifn);
elseif ($_FILES[$x]['type']=="image/jpg" || $_FILES[$x]['type']=="image/jpeg") $creimg=imagecreatefromjpeg($ifn);

imagecopyresampled($test123, $creimg, 0, 0, $watermark_x, $watermark_y, 1, 1, $watermark_width, $watermark_height);
$rgb = imagecolorat($test123, 0, 0);

$r = ($rgb >> 16) & 0xFF;
$g = ($rgb >> 8) & 0xFF;
$b = $rgb & 0xFF;

$lightness = (double) ((min($r, $g, $b) + max($r, $g, $b)) / 510.0);
imagedestroy($test123);

$watermark_image = ($lightness < 0.5) ? $watermark_image_light : $watermark_image_dark;
$watermark = imagecreatefrompng($watermark_image);

imagealphablending($creimg, TRUE);
imagealphablending($watermark, TRUE);
imagesavealpha($creimg, true); /// ���� ����, �� Oliwer
imagesavealpha($watermark, true); /// ���� ����, �� Oliwer
imagecopy($creimg, $watermark, $watermark_x, $watermark_y, 0, 0,$watermark_width, $watermark_height);
imagedestroy($watermark);

if ($_FILES[$x]['type'] == "image/png") imagepng($creimg, $ifn);
elseif ($_FILES[$x]['type'] == "image/jpg" || $_FILES[$x]['type'] == "image/jpeg") imagejpeg($creimg, $ifn);
elseif ($_FILES[$x]['type'] == "image/gif") imagegif($creimg, $ifn);

}
////// ������� ���� //////

return $ifilename;

}

}
////



function file_upload_max_size() {

/**
** ������� ������ ���������� ����� ��� POST �������
*/

$sufs = array('' => 1, 'k' => 1024, 'm' => 1048576, 'g' => 1073741824);

static $max_size = -1;

if ($max_size < 0) {

if (preg_match('/([0-9]+)\s*(k|m|g)?(b?(ytes?)?)/i', ini_get('post_max_size'), $match_1))
$max_size = ($match_1[1] * $sufs[mb_strtolower($match_1[2])]);

if (preg_match('/([0-9]+)\s*(k|m|g)?(b?(ytes?)?)/i', ini_get('upload_max_filesize'), $match_2))
$upload_max = ($match_2[1] * $sufs[mb_strtolower($match_2[2])]);

if ($upload_max > 0 && $upload_max < $max_size)
$max_size = $upload_max;
}

return $max_size;
}


define ("VERSION", "<b>.:".$SITENAME." <noindex><a href=\"http://www.muz-tracker.net/\" class=\"copyright\" title=\"������ �����: Tesla (TT) Tesla Tracker � 2014 �. ���������� ������ (��������� ���������) �������� 7Max7.\">�</a></noindex> ".date("Y")." v.Platinum (IV):.</b>");
define ("TBVERSION", "");

?>