<?

require_once("include/bittorrent.php");
dbconn(false);

if ($CURUSER) {
if (!headers_sent())
@header("Location: ".$DEFAULTBASEURL);
else
die("<script>setTimeout('document.location.href=\"".$DEFAULTBASEURL."\"', 10);</script>");
}

$ip = getip();

if ($Signup_Config["deny_signup"] == true && $Signup_Config["allow_invite_signup"] == false)
stderr($tracker_lang['error'], $tracker_lang['sorry_signup_no']);


$users = get_row_count("users");
if ($users >= $maxusers)
stderr($tracker_lang['arror'], sprintf($tracker_lang['signup_error_invite'], "<b>".$maxusers."</b>"));


if (!isset($_POST["agree"]) && $SITE_Config["captcha"] == false) {

stdhead($tracker_lang['rules']);

echo "<fieldset class=\"fieldset\">";

echo "<legend><b>".$tracker_lang['rules']."</b></legend>";
echo "<form method=\"post\" action=\"signup.php\">";

echo "<table cellpadding=\"4\" cellspacing=\"0\" border=\"0\" style=\"width:100%\" class=\"table\">
<tr><td class=\"a\">".$tracker_lang['signup_nextup'].":</td></tr>

<tr><td class=\"b\" style=\"font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; font-family: verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif\">
<div class=\"page\" style=\"border-right: thin inset; padding-right: 6px; border-top: thin inset; padding-left: 6px; padding-bottom: 6px; overflow: auto; border-left: thin inset; padding-top: 1px; border-bottom: thin inset; height: 200px\">

<p>���������� � ��������������, ������������� ������, ��������� ������� ��� �������������� � ������������ ��������� �� �������, ��� ����� ��� ��������� ����������� ����������. ��������� �������� ����� ������ ������ ������, �� �� ������������� �������, �������������� ������ ����� ����� ��������������� �� ���������� ���������.</p>

<p>���������� � ������ ���������, �� ���������� ��������� ���������� ������� � �����, � ����� ���������� ���������������� ��.</p>

<p>������������� ������� ��������� �� ����� ����� �������, ��������, ���������� ��� ��������� ����� ���� ��� ��������� �� ������ ����������.</p>

</div>
</td></tr>
<tr><td class=\"a\">

<label><input type=\"checkbox\" name=\"rulesverify\" value=\"yes\"> ".$tracker_lang['signup_i_have_read_rules']."</label><br />
<label><input type=\"checkbox\" name=\"faqverify\" value=\"yes\"> ".$tracker_lang['signup_i_will_read_faq']."</label><br />
<label><input type=\"checkbox\" name=\"ageverify\" value=\"yes\"> ".$tracker_lang['signup_i_am_13_years_old_or_more']."</label><br />
<label><input class=\"tablea\" type=\"checkbox\" name=\"agree\" value=\"yes\"><strong>".$tracker_lang['signup_i_am_rulesex']." - ".$SITENAME."</strong>
</label>

<input type=\"hidden\" name=\"do\" value=\"register\">
</td></tr>

<tr><td class=\"b\"><input class=\"btn\" name=\"post\" type=\"submit\" value=\"".$tracker_lang['signup']."\"/></td></tr>

</table>
</fieldset>

</form><br />";

stdfoot();
die;
}

stdhead($tracker_lang['signup']);

if (isset($_POST["agree"]) && $_POST["agree"] == "yes"){
if (empty($_POST["rulesverify"]) || empty($_POST["faqverify"]) || empty($_POST["ageverify"]))
stderr($tracker_lang['error'], $tracker_lang['signup_norules'].": <br /> <strong>
".(empty($_POST["rulesverify"]) ? $tracker_lang['signup_i_have_read_rules']."<br />":"")."
".(empty($_POST["faqverify"]) ? $tracker_lang['signup_i_will_read_faq']."<br />":"")."
".(empty($_POST["ageverify"]) ? $tracker_lang['signup_i_am_13_years_old_or_more']:"")."
</strong>");
}

$countries = "<option value=\"0\">".$tracker_lang['signup_not_selected']."</option>\n";

$ct_r = sql_query("SELECT id, name FROM countries ORDER BY name") or sqlerr(__FILE__, __LINE__);
while ($ct_a = mysql_fetch_array($ct_r))
$countries .= "<option value=\"".$ct_a["id"]."\" ".(!validip_pmr($ip) && $ct_a["id"]=="103" ? "selected":"").">".$ct_a["name"]."</option>\n";

echo "<script language=\"JavaScript\" src=\"js/ajax.js\" type=\"text/javascript\"></script>";

echo "<form name=\"mainForm\" method=\"post\" action=\"takesignup.php\">";
echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"10\">";

$array_warn = array();

if ($Signup_Config["deny_signup"] == true && $Signup_Config["allow_invite_signup"] == true)
$array_warn[] = $tracker_lang['signup_only_invite'];

$array_warn[] = $tracker_lang['free_signup_lim'].": ".number_format($maxusers-$users);
$array_warn[] = $tracker_lang['signup_use_cookies'];

if (count($array_warn))
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"><fieldset class=\"fieldset\"><legend><b>".$tracker_lang['warning']."</b></legend><h3>".implode("<br />", $array_warn)."</h3></fieldset></td></tr>";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['signup_username']."</td><td class=\"a\" align=\"left\"><input type=\"text\" size=\"60\" class=\"login\" name=\"wantusername\" id=\"wantusername\" onblur=\"signup_check('username'); return false;\"/> <i>".sprintf($tracker_lang['max_simp_of'], 40)."</i> <div id=\"check_username\"></div></td></tr>";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['signup_password']."</td><td class=\"a\" align=\"left\">
<input type=\"password\" size=\"60\" class=\"pass\" name=\"wantpassword\" id=\"wantpassword\"/>
</td></tr>";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['signup_password_again']."</td><td class=\"a\" align=\"left\"><input type=\"password\" class=\"pass\" size=\"60\" name=\"passagain\" id=\"passagain\" onblur=\"signup_check('password'); return false;\"/><div id=\"check_password\"></div></td></tr>";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['email']."</td><td class=\"a\" align=\"left\"><input type=\"text\" size=\"60\" name=\"email\" class=\"mail\" id=\"email\" onblur=\"signup_check('email'); return false;\"/> <i>".sprintf($tracker_lang['max_simp_of'], 80)."</i> <div id=\"check_email\"></div> 
".($Signup_Config["email_rebans"] == true ? "<br />
<fieldset class=\"fieldset\"><legend><b>".$tracker_lang['signup_banemails'].": </b></legend>
<li><b>... @rambler.ru</b></li>
<li><b>... @gmail.com</b></li>
<li><b>... @live.ru</b></li>
<li><b>... @mail.ru</b></li>
<li><b>... @idknet.com</b></li>
</fieldset>":"")."
</td></tr>";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['gender']."</td><td class=\"a\" align='left'><label><input type=\"radio\" name=\"gender\" value=\"1\">".$tracker_lang['my_gender_male']." <img src=\"/pic/male.gif\"></label><label><input type=\"radio\" name=\"gender\" value=\"2\">".$tracker_lang['my_gender_female']." <img src=\"/pic/female.gif\"></label></td></tr>";

$year = "<select name='year'><option value=\"0000\">".$tracker_lang['choose'].": ".$tracker_lang['my_year']."</option>\n";
$i = date("Y")-60;

while ($i <= (date('Y',time())-13)) {
$year .= "<option value='".$i."'>".$i." (".(date("Y")-$i).")</option>\n";
++$i;
}

$year .= "</select>\n";

$birthmonths = array("01" => $tracker_lang['my_months_january'],"02" => $tracker_lang['my_months_february'],"03" => $tracker_lang['my_months_march'],"04" => $tracker_lang['my_months_april'],"05" => $tracker_lang['my_months_may'],"06" => $tracker_lang['my_months_june'],"07" => $tracker_lang['my_months_jule'],"08" => $tracker_lang['my_months_august'],"09" => $tracker_lang['my_months_september'],"10" => $tracker_lang['my_months_october'],"11" => $tracker_lang['my_months_november'],"12" => $tracker_lang['my_months_december']);

$month = "<select name=\"month\"><option value=\"00\">".$tracker_lang['choose'].": ".$tracker_lang['my_month']."</option>\n";

foreach ($birthmonths as $month_no => $show_month) {
$month.= "<option value=\"".$month_no."\" ".(date('m')==$month_no ? "selected":"").">".$show_month."</option>\n";
}

$month.= "</select>\n";
$day = "<select name='day'><option value=\"00\">".$tracker_lang['choose'].": ".$tracker_lang['my_day']."</option>\n";
$i = 1;

while ($i <= 31) {

if ($i < 10)
$day.= "<option value='0".$i."' ".(date('j')==$i ? "selected":"").">0".$i."</option>\n";
else
$day.= "<option value='".$i."' ".(date('j')==$i ? "selected":"").">".$i."</option>\n";

++$i;
}

$day.= "</select> <i>".$tracker_lang['yes_optional']."</i>\n";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['my_birthdate']."</td><td class=\"a\" align=\"left\">".$year.$month.$day."</td></tr>";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['my_country']."</td><td class=\"a\" align=\"left\"><select name=\"country\">\n".$countries."\n</select> <i>".$tracker_lang['yes_optional']."</i></td></tr>";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['protect_of_cookie']."</td><td class=\"a\" align=left><label><input type=\"radio\" name=\"tesla_guard\" value=\"yes\">".$tracker_lang['protect_agent']."</label><label><input type=\"radio\" name=\"tesla_guard\" checked value=\"no\">".$tracker_lang['protect_ip']."</label><br /><br /><font class=\"small_text\"><b>".$tracker_lang['help']."</b>: ".$tracker_lang['help_of_protect']."</font></td></tr>";

if ($Signup_Config["allow_invite_signup"] == true) 
echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['signup_invite']."</td><td align='left' class=\"a\"><input type=\"text\" size=\"60\" class=\"invite\" name=\"invite\" maxlength=\"32\" size=\"32\" id=\"invite\" onblur=\"signup_check('invite'); return false;\"/> ".(($Signup_Config["deny_signup"] == true && $Signup_Config["allow_invite_signup"] == true) ? "<i>".$tracker_lang['yes_optional']."</i> ":"<i>".$tracker_lang['no_optional']."</i>")." <div id=\"check_invite\"></div></td></tr>";

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['sec_question']."</td><td class=\"a\" align='left'><i>".$tracker_lang['sec_question']."</i> <br /><input title=\"".$tracker_lang['sec_question']."\" type=\"text\" name=\"ques\" size='70' /> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i> <br /><i>".$tracker_lang['sec_answer']."</i><br /><input title=\"".$tracker_lang['sec_answer']."\" type=\"text\" name=\"answ\" size='70' /> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i><br />
<i>".$tracker_lang['sec_warndata']."</i></td></tr>";


if ($SITE_Config["captcha"] == true){

include_once("include/functions_captcha.php");
$acaptcha = creating_captcha();

echo "<tr><td align=\"right\" class=\"b\">".$tracker_lang['captcha_confirm']."</td><td class=\"a\" align=\"left\">

<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">

<tr><td class=\"b\" align=\"center\" style=\"margin: 0px; padding: 0px;\">
<div id=\"result\">".$acaptcha["echo"]."</div>
<input type=\"hidden\" name=\"hash\" value=\"".$acaptcha["hash"]."\" />
</td>
<td class=\"a\" width=\"50px\"><a href=\"#\" id=\"status\" class=\"button button-blue\"><span>".$tracker_lang['captcha_button']."</span></a></td>
</tr>

<tr><td class=\"a\" colspan=\"2\"><input type=\"text\" name=\"captcha\" size=\"20\" value=\"\" /> ".$tracker_lang['captcha_take']."</td></tr>

</table>
</td></tr>";


echo '<script type="text/javascript">
jQuery("#status").click(function() {
var onput = jQuery("#old").attr("value");
jQuery.post("md5.php",{"update": "true", "hash": "'.$acaptcha["hash"].'", "old": onput},
function(response) {jQuery("#result").fadeIn("slow"); jQuery("#result").html(response);}, "html");
});
</script>';
}



echo "<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input type=\"submit\" name=\"post\" value=\"".$tracker_lang['signup']."\" style='height: 25px;width: 200px' class=\"btn\"></td></tr>";

echo "</table></form><br />";
echo "<div id='loading-layer'></div>";

stdfoot();

?>