<?
require_once 'include/bittorrent.php';
dbconn(false, true);

header("Content-Type: text/html; charset=".$tracker_lang['language_charset']);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] <> 'XMLHttpRequest')
die ('XMLHttpRequest');

global $CURUSER, $tracker_lang;

if (empty($CURUSER)) die;

?>
<script language="JavaScript" type="text/javascript">
function karma(id, type, act) {
jQuery.post("karma.php",{"id":id,"act":act,"type":type,"b":"yes"}, function (response) {
jQuery("#karma").empty();
jQuery("#karma").append(response);
});
}
</script>
<?

$hu = sql_query("SELECT * FROM humor WHERE (SELECT COUNT(*) FROM karma WHERE type = 'humor' AND value = humor.id AND user = ".$CURUSER["id"].") IS NULL LIMIT 1") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($hu) == 0)
$hu = sql_query("SELECT h.*, (SELECT COUNT(*) FROM karma WHERE type = 'humor' AND value = h.id AND user = ".$CURUSER["id"].") AS canrate
FROM humor AS h
ORDER BY RAND() LIMIT 1") or sqlerr(__FILE__, __LINE__);
  
if (mysql_num_rows($hu) > 0){

$res = mysql_fetch_assoc($hu); 

$humor = strip_tags($res['txt']);
$humor = str_replace('script', '', $humor);
$humor = str_replace('js', '', $humor);
$humor = str_replace('src=', '', $humor); 
$humor = trim($humor);



$pos1 = @strpos($humor, '.', 450); // $pos = 7, not 0
$pos2 = @strpos($humor, '.', 600); // $pos = 7, not 0
$pos3 = strlen($humor); // $pos = 7, not 0

if (strlen($humor) >= 450 && !empty($pos1))
$humor = substr($humor, 0, ($pos1+1));
elseif (strlen($humor) >= 600 && !empty($pos2))
$humor = substr($humor, 0, ($pos2+1));


$humor = strlen($pos3) > 600 ? format_comment($humor)." <a title=\"".$tracker_lang['more']."\" href=\"humor.php?id=".$res['id']."\">&#8658;&#8658;&#8658;</a>" : format_comment($humor);

echo "<table border=\"0\" width=\"100%\">";

echo "<tr><td align=\"center\" class=\"b\">".$humor."</td></tr>";

if (!$CURUSER || $res["canrate"] > 0 || $CURUSER['id'] == $res['uid'])
echo "<td class=\"a\" align=\"center\"><img src=\"pic/minus-dis.png\" title=\"".$tracker_lang['is_nolike']."\" alt=\"\" /> ".karma($res["karma"])." <img src=\"pic/plus-dis.png\" title=\"".$tracker_lang['is_like']."\" alt=\"\" /><br />
<small><a title=\"".$tracker_lang['stable_link']."\" href=\"humor.php?id=".$res['id']."\">".$tracker_lang['stable_link']."</a> : <a title=\"".$tracker_lang['add_humor']."\" href=\"humor.php\">".$tracker_lang['add_humor']."</a> : ".(get_user_class() < UC_MODERATOR ? "":" <a title=\"".$tracker_lang['edit']."\" href=\"humor.php?id=".$res['id']."&do=edit\">".$tracker_lang['edit']."</a> : <a title=\"".$tracker_lang['delete']."\" href=\"humor.php?id=".$res['id']."&do=delete\">".$tracker_lang['delete']."</a> : ")." <a title=\"".$tracker_lang['jokes']."\" href=\"humorall.php\">".$tracker_lang['jokes']."</a></small></td>\n";
else
echo "<td align=\"center\" class=\"a\" id=\"karma\"><img src=\"pic/minus.png\" style=\"cursor:pointer;\" title=\"".$tracker_lang['is_nolike']."\" alt=\"\" onclick=\"javascript: karma('".$res["id"]."', 'humor', 'minus');\" /> ".karma($res["karma"])." <img src=\"pic/plus.png\" style=\"cursor:pointer;\" onclick=\"javascript: karma('".$res["id"]."', 'humor', 'plus');\" title=\"".$tracker_lang['is_like']."\" alt=\"\" /><br />
<small><a title=\"".$tracker_lang['stable_link']."\" href=\"humor.php?id=".$res['id']."\">".$tracker_lang['stable_link']."</a> : <a title=\"".$tracker_lang['add_humor']."\" href=\"humor.php\">".$tracker_lang['add_humor']."</a> : ".(get_user_class() < UC_MODERATOR ? "":" <a title=\"".$tracker_lang['edit']."\" href=\"humor.php?id=".$res['id']."&do=edit\">".$tracker_lang['edit']."</a> : <a title=\"".$tracker_lang['delete']."\" href=\"humor.php?id=".$res['id']."&do=delete\">".$tracker_lang['delete']."</a> : ")." <a title=\"".$tracker_lang['jokes']."\" href=\"humorall.php\">".$tracker_lang['jokes']."</a></small></td>\n";

echo "</table>";

} else
echo "<center>".$tracker_lang['no_data_now']."<br /><br /><b>(</b><a href=humor.php>".$tracker_lang['add_humor']."</a><b>)</b></center>"; 

?>