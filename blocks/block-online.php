<?php

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

if (!defined('BLOCK_FILE')) {
Header("Location: ../index.php");
exit;
}

global $tracker_lang;
$blocktitle = $tracker_lang['online_now'];

?>
<script>
function index_online() {
jQuery.post("block-online_jquery.php" , {"action":"all"} , function(response) {
jQuery("#index_online").html(response);
}, "html");
setTimeout("index_online();", 90000);
}
index_online();
</script>
<?
$content= '<div align="center" id="index_online">'.$tracker_lang['loading'].'</div>';
?>