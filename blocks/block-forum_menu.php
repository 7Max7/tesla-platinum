<?

if (!defined('BLOCK_FILE')) {
header("Location: ../index.php");
exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $CURUSER, $tracker_lang;

$blocktitle = $tracker_lang['forum_last_threads'];


if ($CURUSER && !empty($CURUSER["class"]))
$curuserclass = get_user_class();
else
$curuserclass = 1;

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-forum_menu_".$curuserclass, "time" => 60*60, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

$content = "";

$for = sql_query("SELECT ft.id, ft.subject, ft.lastpost, ft.lastdate, ft.lastpost FROM topics as ft, forums as ff WHERE ff.id = ft.forumid AND ft.visible = 'yes' AND ff.visible = 'yes' AND ff.minclassread <= ".sqlesc($curuserclass)." ORDER BY lastpost DESC LIMIT 10") or sqlerr(__FILE__, __LINE__);

while ($arr = mysql_fetch_assoc($for)) {

$content.= "<a class=\"menu\" title=\"".$arr["lastdate"]."\" href=\"forums.php?action=viewtopic&topicid=".$arr["id"]."&page=last#".$arr["lastpost"]."\">".htmlspecialchars($arr["subject"])."</a>";

}

file_query($content, $cache = array("type" => "disk", "file" => "block-forum_menu_".$curuserclass, "time" => 60*60, "action" => "set"));
}

?>