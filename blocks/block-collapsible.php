<?php

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

if (!defined('BLOCK_FILE')) {
Header("Location: ../index.php");
exit;
}

$blocktitle = "�������";
?>

<script type="text/javascript" src="js/bl_jquery.js"></script>
<script type="text/javascript" src="js/bl_jquery.main.js"></script>
<script type="text/javascript" src="js/bl_prototype.js"></script>
<script type="text/javascript" src="js/bl_scriptaculous.js?load=effects,builder,controls"></script>
<script language="JavaScript" type="text/javascript" src="js/bl_contentflow.js"></script>
<link rel="stylesheet" href="js/contentflow.css" type="text/css" media="screen"/>

<?
$content = '<noscript><h2 class="center">��� ������ � ������ ���������� �������� JavaScript � ���������� ������ ��������-������������.</h2></noscript>';
?>

<script type="text/javascript">
var cfr  = null;
var cfl  = null;
var cfn  = null;
var cfld = null;

var indexPg = {
switchViewWay : function (way) {
setCookie('d_bl_old', way, 365);
window.location.reload();
},

switchAttachedSection : function (t) {

        var tsecid = jQuery(t).attr('id');
        var tselsc = getCookie('tselsc');

        if (tsecid == tselsc) {
            if (tsecid != 'rcm-arrow')
            {
                jQuery(t).children().attr('src', '/pic/browse/arrow_down.png');
                jQuery('#rcm-arrow').children().attr('src', '/pic/browse/arrow_right.png');
            }

            setCookie('tselsc', '', 365);
        }
        else {
            jQuery('#rcm-arrow').children().attr('src', '/pic/browse/arrow_down.png');
            jQuery('#lst-arrow').children().attr('src', '/pic/browse/arrow_down.png');
            jQuery('#rnd-arrow').children().attr('src', '/pic/browse/arrow_down.png');
            jQuery(t).children().attr('src', '/pic/browse/arrow_right.png');

            setCookie('tselsc', tsecid, 365);

            switch (tsecid) {
                case 'lst-arrow':
                    jQuery('[lang=last-distrs]').click();
                    break
                case 'rnd-arrow':
                    jQuery('[lang=random-distrs]').click();
                    break
                case 'rcm-arrow':
                default:
                    jQuery('[lang=recommended-distrs]').click();
                    break
            }
        }
    },

    cfStart : function () {

        collapse('distributions');

        if (cfr === null && cfl === null && cfn === null) {
            jQuery('[lang=' + cfld + ']').click();
        }
    }
};
</script>

<script type="text/javascript">
jQuery(function() {
    jQuery(".bl_distrs").click ( function(){
        if (jQuery(this).hasClass("bl_distrs_active"))
            return;
        else
        {
            var item = jQuery(this).attr("lang");

            jQuery(this).toggleClass("bl_distrs_active");
            jQuery(this).siblings("li").removeClass("bl_distrs_active");

            jQuery('#recommended-distrs').hide();
            jQuery('#last-distrs').hide();
            jQuery('#random-distrs').hide();
            jQuery('#' + item).show();

            switch (item) {
                case 'recommended-distrs':
                    if (cfr === null) {
                        cfr = new ContentFlow('recommended-distrs');
                        cfr.init();
                    }
                    break
                case 'last-distrs':
                    if (cfl === null) {
                        cfl = new ContentFlow('last-distrs');
                        cfl.init();
                    }
                    break
                case 'random-distrs':
                    if (cfn === null) {
                        cfn = new ContentFlow('random-distrs');
                        cfn.init();
                    }
                    break
            }
        }
    });
});

jQuery(document).ready(function() {
    var tselsc = getCookie('tselsc');

    if (tselsc !== null && tselsc != '') {
        jQuery('#' + tselsc).children().attr('src', '/pic/browse/arrow_right.png');

        switch (tselsc) {
            case 'lst-arrow':
                cfld = 'last-distrs';
                break
            case 'rnd-arrow':
                cfld = 'random-distrs';
                break
            case 'rcm-arrow':
            default:
                cfld = 'recommended-distrs';
        }
    }
    else {
        cfld = 'recommended-distrs';
    }

    var cookie = explode("|", getCookie('collapsed'));

    if (!in_array('distributions', cookie)) {
        jQuery('[lang=' + cfld + ']').click();
    }
});

</script>

<?
$content.= '<div id="distributions">';
$content.= '<div class="collapsed_content bordered">';
$content.= '<div style="float: left;">';

if (isset($_COOKIE["d_bl_old"]) && $_COOKIE["d_bl_old"] == 'no')
$content.= '<ins class="d_bl_select" title="���������� ����� ��������"></ins>
<ins class="d_bl_select d_bl_select_old_ns" onclick="indexPg.switchViewWay(\'yes\');" title="���������� ������ ��������"></ins>';
else
$content.= '<ins class="d_bl_select d_bl_select_ns" onclick="indexPg.switchViewWay(\'no\');" tooltip="���������� ����� ��������"></ins>
<ins class="d_bl_select d_bl_select_old" tooltip="���������� ������ ��������"></ins>';

$content.= '</div>';






if (isset($_COOKIE["d_bl_old"]) && $_COOKIE["d_bl_old"] == 'yes'){

$content.= '<div align="center"><table width="100%" cellspacing="0">';

$dybl_ = 0;

$res_0 = sql_query("SELECT id, name, image1, free, (f_seeders + seeders) AS seeders, (f_leechers + leechers) AS leechers, times_completed FROM torrents ORDER BY id DESC LIMIT 12", $cache = array("type" => "disk", "file" => "block_collapsible_0", "time" => 60*10)) or sqlerr(__FILE__, __LINE__);

while ($row_0 = mysql_fetch_assoc_($res_0)){

if ($dybl_ == 0){
$content.= '<tr>';
$num = 0;
} else {
++$num;
}

if (empty($row_0["image1"]))
$row_0["image1"] = 'default_torrent.png';

$content.= "<td width=\"25%\" valign=\"top\" align=\"center\">
<div style=\"margin-top: 3px;\" title=\"".htmlspecialchars($row_0["name"])."\"><b class=\"bloki\"><a class=\"bloki\" href=\"details.php?id=".$row_0["id"]."\">".htmlspecialchars($row_["name"])."</a></b><br /><a class=\"bloki\" href=\"details.php?id=".$row_0["id"]."\"><img class=\"glossy\" src='thumbnail.php?image=".htmlspecialchars_uni($row_0["image1"])."' width=\"140px\" height=\"200px\" /></a></div>
<div style=\"background-color: #ffffcc;\">".($row_0["free"] == "yes" ? "<img alt=\"�������\" title=\"�������\" src=\"/pic/freedownload.gif\">":"")." <img src=\"/pic/upload.gif\" title=\"�������\" alt=\"�������\"/>".$row_0["leechers"]." <img src=\"/pic/download.gif\"title=\"������\" alt=\"������\"/>".$row_0["seeders"]." <img src=\"/pic/ok.gif\" title=\"�������\" alt=\"�������\"/>".$row_0["times_completed"]."</div>
</td>";

if ($num == 3 ){
$content.= '</tr>';
$dybl_ = 0;
$num = 0;
} else {
++$dybl_;
}




}

$content.= '</div>';
$content.= '</div>';


$content.= '</table></div>';

}

else {


$content.= '<div align="center">
            <ul style="height: 4px;">
                <a onclick="indexPg.switchAttachedSection(this);" style="cursor: pointer;" id="rcm-arrow">
                <img border="0" src="/pic/browse/arrow_down.png" align="absmiddle" title="���������� ��-���������">
                </a>
                <li class="bl_distrs bordered" lang="recommended-distrs">���������������</li>

                <a onclick="indexPg.switchAttachedSection(this);" style="cursor: pointer;" id="lst-arrow">
                <img border="0" src="/pic/browse/arrow_down.png" align="absmiddle" title="���������� ��-���������">
                </a>
                <li class="bl_distrs bordered" lang="last-distrs">���������</li>
                <a onclick="indexPg.switchAttachedSection(this);" style="cursor: pointer;" id="rnd-arrow">
                <img border="0" src="/pic/browse/arrow_down.png" align="absmiddle" title="���������� ��-���������">
                </a>
                <li class="bl_distrs bordered" lang="random-distrs">���������</li>

            </ul>
</div>';

$content.= '<br />';




$content.= '<div id="recommended-distrs" class="ContentFlow" style="height: 420px;" oncontextmenu="return false;">';
$content.= '<div class="loadIndicator"><div class="indicator"></div></div>';
$content.= '<div class="flow">';


$res_1 = sql_query("SELECT id, name, image1, free, (f_seeders + seeders) AS seeders, (f_leechers + leechers) AS leechers, times_completed FROM torrents ORDER BY times_completed DESC LIMIT 12", $cache = array("type" => "disk", "file" => "block_collapsible_1", "time" => 60*10)) or sqlerr(__FILE__, __LINE__);

while ($row_1 = mysql_fetch_assoc_($res_1)){

$image1 = htmlspecialchars_uni($row_1["image1"]);

$content.= '<div class="item">
<img class="content" src="thumbnail.php?image='.$image1.'" title="'.htmlspecialchars($row_1["name"]).'" href="details.php?id='.$row_1["id"].'">
<div class="caption">
'.($row_1["free"] == 'yes' ? '<img border="0" src="/pic/freedownload.gif" title="������� �������">':'').' '.htmlspecialchars($row_1["name"]).'<br />
<img border="0" alt="�������" title="�������" src="/pic/upload.gif"> '.number_format($row_1["seeders"]).' <img border="0" alt="���������" title="���������" src="/pic/download.gif"> '.number_format($row_1["leechers"]).' <img border="0" alt="�������" title="�������" src="/pic/ok.png"> '.number_format($row_1["times_completed"]).'</div>
</div>';

}

$content.= '</div>';
$content.= '<div class="globalCaption"></div>';
$content.= '</div>';



$content.= '<div id="last-distrs" class="ContentFlow" style="height: 420px; display: none;">';

$content.= '<div class="loadIndicator"><div class="indicator"></div></div>';

$content.= '<div class="flow">';
        

$res_2 = sql_query("SELECT id, name, image1, free, (f_seeders + seeders) AS seeders, (f_leechers + leechers) AS leechers, times_completed FROM torrents ORDER BY id DESC LIMIT 12", $cache = array("type" => "disk", "file" => "block_collapsible_2", "time" => 60*10)) or sqlerr(__FILE__, __LINE__);

while ($row_2 = mysql_fetch_assoc_($res_2)){

$image2 = htmlspecialchars_uni($row_2["image1"]);

$content.= '<div class="item">
<img class="content" src="thumbnail.php?image='.$image2.'" title="'.htmlspecialchars($row_2["name"]).'" href="details.php?id='.$row_2["id"].'">
<div class="caption">
'.($row_2["free"] == 'yes' ? '<img border="0" src="/pic/freedownload.gif" title="������� �������">':'').' '.htmlspecialchars($row_2["name"]).'<br />
<img border="0" alt="�������" title="�������" src="/pic/upload.gif"> '.number_format($row_2["seeders"]).' <img border="0" alt="���������" title="���������" src="/pic/download.gif"> '.number_format($row_2["leechers"]).' <img border="0" alt="�������" title="�������" src="/pic/ok.png"> '.number_format($row_2["times_completed"]).'</div>
</div>';

}
$content.= '</div>';


$content.= '<div class="globalCaption"></div>';
$content.= '</div>';




$content.= '<div id="random-distrs" class="ContentFlow" style="height: 420px; display: none;">';
$content.= '<div class="loadIndicator"><div class="indicator"></div></div>';
$content.= '<div class="flow">';


$res_3 = sql_query("SELECT id, name, image1, free, (f_seeders + seeders) AS seeders, (f_leechers + leechers) AS leechers, times_completed FROM torrents ORDER BY RAND() DESC LIMIT 12", $cache = array("type" => "disk", "file" => "block_collapsible_3", "time" => 60*10)) or sqlerr(__FILE__, __LINE__);

while ($row_3 = mysql_fetch_assoc_($res_3)){

$image3 = htmlspecialchars_uni($row_3["image1"]);

$content.= '<div class="item">
<img class="content" src="thumbnail.php?image='.$image3.'" title="'.htmlspecialchars($row_3["name"]).'" href="details.php?id='.$row_3["id"].'">
<div class="caption">
'.($row_3["free"] == 'yes' ? '<img border="0" src="/pic/freedownload.gif" title="������� �������">':'').' '.htmlspecialchars($row_3["name"]).'<br />
<img border="0" alt="�������" title="�������" src="/pic/upload.gif"> '.number_format($row_3["seeders"]).' <img border="0" alt="���������" title="���������" src="/pic/download.gif"> '.number_format($row_3["leechers"]).' <img border="0" alt="�������" title="�������" src="/pic/ok.png"> '.number_format($row_3["times_completed"]).'</div>
</div>';

}
$content.= '</div>';

$content.= '<div class="globalCaption"></div>';
$content.= '</div>';

$content.= '</div>';
$content.= '</div>';


$content.= '<br />';
$content.= '<div align="center" id="DIV_DA_26224"></div><br />';
}
 
?>