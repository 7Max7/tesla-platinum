<?php
if (!defined('BLOCK_FILE')) {
Header("Location: ../index.php");
exit;
}

global $tracker_lang;

$blocktitle = $tracker_lang['main_menu'];

$res = sql_query("SELECT COUNT(*) AS num_off FROM off_reqs WHERE perform = 'no'", $cache = array("type" => "disk", "file" => "block_menu_perform", "time" => 60*30)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_assoc_($res);

$num_off = number_format($arr["num_off"]);


$content = "
<a class=\"menu\" href=\"index.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'index.php' ? "<b>".$tracker_lang['homepage']."</b>":"".$tracker_lang['homepage']."")."</a>"

."<a class=\"menu\" href=\"browse.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'browse.php' ? "<b>".$tracker_lang['torrents']."</b>":$tracker_lang['torrents'])."</a>"

."<a class=\"menu\" href=\"browse.php?date=".date("Y-m-d")."\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'browse.php' ? "<b>".$tracker_lang['torrent_today']."</b>":$tracker_lang['torrent_today'])."</a>"

."<a class=\"menu\" href=\"persons.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'persons.php' ? "<b>".$tracker_lang['persons_movie']."</b>":$tracker_lang['persons_movie'])."</a>"

."<a class=\"menu\" ".(empty($num_off)? "title=\"".$tracker_lang['requests_section']."\"":"title=\"".$tracker_lang['new_offers']."\"")." href=\"detailsoff.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'detailsoff.php' ? "<b>".$tracker_lang['requests_section']."</b>":$tracker_lang['requests_section'])." ".(empty($num_off)? "":"(<b>".$num_off."</b>)")."</a>"

."<a class=\"menu\" href=\"rules.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'rules.php' ? "<b>".$tracker_lang['rules']."</b>":"".$tracker_lang['rules']."")."</a>"

."<a class=\"menu\" href=\"faq.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'faq.php' ? "<b>".$tracker_lang['faq']."</b>":"".$tracker_lang['faq']."")."</a>"

."<a class=\"menu\" href=\"tags.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'tags.php' ? "<b>".$tracker_lang['tags_bb_code']."</b>":$tracker_lang['tags_bb_code'])."</a>"

."<a class=\"menu\" href=\"topten.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'topten.php' ? "<b>".$tracker_lang['topten']."</b>":"".$tracker_lang['topten']."")."</a>"

."<a class=\"menu\" href=\"newsarchive.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'newsarchive.php' ? "<b>".$tracker_lang['news_olders']."</b>":$tracker_lang['news_olders'])."</a>"

."<a class=\"menu\" href=\"polls.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'polls.php' ? "<b>".$tracker_lang['polls_olders']."</b>":$tracker_lang['polls_olders'])."</a>"

."<a class=\"menu\" href=\"formats.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'formats.php' ? "<b>".$tracker_lang['formats']."</b>":"".$tracker_lang['formats']."")."</a>"

."<a class=\"menu\" href=\"groups.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'groups.php' ? "<b>".$tracker_lang['upload_groups']."</b>":$tracker_lang['upload_groups'])."</a>"

."<a class=\"menu\" href=\"comments_last.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'comments_last.php' ? "<b>".$tracker_lang['forum_last_comments']."</b>":$tracker_lang['forum_last_comments'])."</a>" ;

if (get_user_class() >= UC_MODERATOR)
$content.= "<a class=\"menu\" href=\"log.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'log.php' ? "<b>".$tracker_lang['log']."</b>":"".$tracker_lang['log']."")."</a>";

$content.= "<a class=\"menu\" href=\"support.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'support.php' ? "<b>".$tracker_lang['support']."</b>":$tracker_lang['support'])."</a>";

$content.= "<a class=\"menu\" href=\"useragreement.php\">&nbsp;".(basename($_SERVER['SCRIPT_FILENAME']) == 'useragreement.php' ? "<b>".$tracker_lang['sourse_tesla']."</b>":$tracker_lang['sourse_tesla'])."</a>";


?>