<?
require "include/bittorrent.php";
gzip();
dbconn(false);
loggedinorreturn();

$userid = (int) $_GET["id"];

if (!is_valid_id($userid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

if ($CURUSER["id"] <> $userid && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$page = (int) $_GET["page"];
$action = (string) $_GET["action"];
$perpage = 25;

$ar_text = array("viewcomments" => $tracker_lang['browse'],"viewnewscomment" => $tracker_lang['news'],"viewpollscomment" => $tracker_lang['polls'], "viewoffercomment" => $tracker_lang['requests_section']);

$res = sql_query("SELECT * FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 1){
$arr = mysql_fetch_assoc($res);
$subject = "<a href=\"userdetails.php?id=".$userid."\">".get_user_class_color($arr["class"], $arr["username"])."</a> ".get_user_icons($arr, true);
}
else
$subject = $tracker_lang['anonymous']."(".$userid.")";


if ($action == "viewcomments") {

$res = sql_query("SELECT COUNT(*) FROM comments AS c LEFT JOIN torrents as t ON c.torrent = t.id WHERE c.torrent <> '0' AND c.user = ".sqlesc($userid)." ORDER BY c.id DESC") or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_row($res) or stderr($tracker_lang['error'], $tracker_lang['no_comments']);

$commentcount = $arr[0];
list($pagertop, $pagerbottom, $limit) = pager($perpage, $commentcount, "userhistory.php?action=viewcomments&id=".$userid."&");

$res = sql_query("SELECT t.name, c.torrent AS t_id, c.id, c.added, c.text FROM comments AS c LEFT JOIN torrents as t ON c.torrent = t.id WHERE c.torrent <> '0' AND c.user = ".sqlesc($userid)." ORDER BY c.id DESC ".$limit) or sqlerr(__FILE__, __LINE__);

stdhead($tracker_lang['userhistory']);

echo "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
echo "<tr><td class=\"a\"><strong>".$tracker_lang['userhistory']."</strong>: ".$ar_text[$action]." (".$commentcount.")</td>";
echo "<td class=\"a\"><strong>".$tracker_lang['username']."</strong>: ".$subject."</td></tr>";
echo "<tr><td class=\"b\" colspan=\"2\">";
echo "<div id=\"tabs\">\n";
foreach ($ar_text AS $act => $text){
echo "<span onClick=\"document.location.href='userhistory.php?action=".$act."&id=".$userid."'\" title=\"".$tracker_lang['userhistory']." - ".$text."\" class=\"tab ".($action == $act ? "active":"")."\"><nobr>".$text."</nobr></span>\n";
}
echo "</div>";
echo "</td></tr>";
echo "</table>";


if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_comments']);

if ($commentcount > $perpage)
echo $pagertop;

begin_main_frame();
begin_frame();

while ($arr = mysql_fetch_assoc($res)){

$commentid = $arr["id"];
$torrent = $arr["name"];
$torrentid = $arr["t_id"];

/*
$subres = sql_query("SELECT COUNT(*) FROM comments WHERE torrent = ".sqlesc($torrentid)." AND id < ".sqlesc($commentid)) or sqlerr(__FILE__, __LINE__);
$subrow = mysql_fetch_row($subres);
$count = $subrow[0];
$comm_page = floor($count/20);
$page_url = $comm_page?"&page=".$comm_page:"";
*/

echo ("<table border='0' cellspacing='0' cellpadding='0' width='100%'>
<tr><td class='a'># <a href='details.php?id=".$torrentid."&viewcomm=".$commentid."#comm".$commentid."'>".$commentid."</a> ".(!empty($torrent) ? ("<a href='details.php?id=".$torrentid."'>".htmlspecialchars($torrent)."</a>"):$tracker_lang['user_tor_anonim']). " (".$arr["added"].")</td></tr>
<tr><td class='b'>".format_comment($arr["text"], true)."</td></tr>
</table>");

}

end_frame();
end_main_frame();

if ($commentcount > $perpage) 
echo $pagerbottom;

stdfoot();
die;

} elseif ($action == "viewnewscomment"){

$res = sql_query("SELECT COUNT(*) FROM comments WHERE user = ".sqlesc($userid)." AND torrent = '0' AND poll = '0' AND offer = '0' ORDER BY id DESC") or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_row($res) or stderr($tracker_lang['error'], $tracker_lang['no_comments']);

$commentcount = $arr[0];
list($pagertop, $pagerbottom, $limit) = pager($perpage, $commentcount, "userhistory.php?action=viewnewscomment&id=".$userid."&");

$res = sql_query("SELECT *, (SELECT subject FROM news WHERE news=comments.news LIMIT 1) AS news_subject, (SELECT id FROM news WHERE news=comments.news LIMIT 1) AS news_id
FROM comments WHERE user = ".sqlesc($userid)." AND torrent = '0' AND poll = '0' AND offer = '0'ORDER BY id ".$limit) or sqlerr(__FILE__, __LINE__);


stdhead($tracker_lang['userhistory']);

echo "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
echo "<tr><td class=\"a\"><strong>".$tracker_lang['userhistory']."</strong>: ".$ar_text[$action]." (".$commentcount.")</td>";
echo "<td class=\"a\"><strong>".$tracker_lang['username']."</strong>: ".$subject."</td></tr>";
echo "<tr><td class=\"b\" colspan=\"2\">";
echo "<div id=\"tabs\">\n";
foreach ($ar_text AS $act => $text){
echo "<span onClick=\"document.location.href='userhistory.php?action=".$act."&id=".$userid."'\" title=\"".$tracker_lang['userhistory']." - ".$text."\" class=\"tab ".($action == $act ? "active":"")."\"><nobr>".$text."</nobr></span>\n";
}
echo "</div>";
echo "</td></tr>";
echo "</table>";

if (mysql_num_rows($res) == 0) 
stderr($tracker_lang['error'], $tracker_lang['no_comments']);

if ($commentcount > $perpage) 
echo $pagertop;

begin_main_frame();
begin_frame();

while ($arr = mysql_fetch_assoc($res)){

$commentid = $arr["id"];
$torrent = $arr["name"];

$torrentid = $arr["t_id"];

/*
$subres = sql_query("SELECT COUNT(*) FROM comments WHERE id < ".sqlesc($commentid)." AND torrent = '0' AND poll = '0' AND offer = '0'") or sqlerr(__FILE__, __LINE__);
$subrow = mysql_fetch_row($subres);
$count = $subrow[0];
$comm_page = floor($count/20);
$page_url = $comm_page?"&page=".$comm_page:"";
*/

echo ("<table border='0' cellspacing='0' cellpadding='0' width='100%'>
<tr><td class='a'># ".$commentid." <a href=\"newsoverview.php?id=".$arr["news_id"]."\">".htmlspecialchars($arr["news_subject"])."</a> (".$arr["added"].")</td></tr>
<tr><td class='b'>".format_comment($arr["text"], true)."</td></tr>
</table>");

}
end_frame();
end_main_frame();

if ($commentcount > $perpage) 
echo $pagerbottom;

stdfoot();
die;

} elseif ($action == "viewpollscomment") {

$res = sql_query("SELECT COUNT(*) FROM comments WHERE torrent = '0' AND news = '0' AND offer = '0' AND user = ".sqlesc($userid)." ORDER BY id DESC") or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_row($res) or stderr($tracker_lang['error'], $tracker_lang['no_comments']);
$commentcount = $arr[0];
list($pagertop, $pagerbottom, $limit) = pager($perpage, $commentcount, "userhistory.php?action=viewpollscomment&id=".$userid."&");

$res = sql_query("SELECT *, (SELECT question FROM polls WHERE forum = '0' AND id = comments.poll LIMIT 1) AS polls_subject, (SELECT id FROM polls WHERE forum = '0' AND id = comments.poll LIMIT 1) AS polls_id
FROM comments WHERE torrent = '0' and news = '0' AND offer = '0' AND user = ".sqlesc($userid)." ORDER BY id ".$limit) or sqlerr(__FILE__, __LINE__);

stdhead($tracker_lang['userhistory']);

echo "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
echo "<tr><td class=\"a\"><strong>".$tracker_lang['userhistory']."</strong>: ".$ar_text[$action]." (".$commentcount.")</td>";
echo "<td class=\"a\"><strong>".$tracker_lang['username']."</strong>: ".$subject."</td></tr>";
echo "<tr><td class=\"b\" colspan=\"2\">";
echo "<div id=\"tabs\">\n";
foreach ($ar_text AS $act => $text){
echo "<span onClick=\"document.location.href='userhistory.php?action=".$act."&id=".$userid."'\" title=\"".$tracker_lang['userhistory']." - ".$text."\" class=\"tab ".($action == $act ? "active":"")."\"><nobr>".$text."</nobr></span>\n";
}
echo "</div>";
echo "</td></tr>";
echo "</table>";

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_comments']);

if ($commentcount > $perpage) 
echo $pagertop;

begin_main_frame();
begin_frame();

while ($arr = mysql_fetch_assoc($res)){

$commentid = $arr["id"];
$torrent = $arr["name"];
$torrentid = $arr["t_id"];

/*
$subres = sql_query("SELECT COUNT(*) FROM comments WHERE torrent = '0' AND news = '0' AND offer = '0' AND id < ".sqlesc($commentid)) or sqlerr(__FILE__, __LINE__);
$subrow = mysql_fetch_row($subres);
$count = $subrow[0];
$comm_page = floor($count/20);
$page_url = $comm_page?"&page=".$comm_page:"";
*/

echo ("<table border='0' cellspacing='0' cellpadding='0' width='100%'>
<tr><td class='b'>
# ".$commentid." <a href=\"polloverview.php?id=".$arr["polls_id"]."\">".format_comment($arr["polls_subject"])."</a> (".$arr["added"].")
</td></tr>
<tr><td class='b'>".format_comment($arr["text"], true)."</td></tr>
</table>");

}

end_frame();
end_main_frame();

if ($commentcount > $perpage) 
echo $pagerbottom;

stdfoot();
die;

} elseif ($action == "viewoffercomment"){

$res = sql_query("SELECT COUNT(*) FROM comments WHERE user = ".sqlesc($userid)." AND torrent = '0' AND poll = '0' AND news = '0' ORDER BY id DESC") or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_row($res) or stderr($tracker_lang['error'], $tracker_lang['no_comments']);

$commentcount = $arr[0];
list($pagertop, $pagerbottom, $limit) = pager($perpage, $commentcount, "detailsoff.php?action=viewoffercomment&id=".$userid."&");

$query = "SELECT *, (SELECT name FROM off_reqs WHERE off_reqs.id=comments.offer LIMIT 1) AS news_subject, (SELECT id FROM off_reqs WHERE off_reqs.id=comments.offer LIMIT 1) AS news_id
FROM comments WHERE user = ".sqlesc($userid)." AND torrent = '0' AND poll = '0' AND news = '0'ORDER BY id ".$limit;
$res = sql_query($query) or sqlerr(__FILE__, __LINE__);

stdhead($tracker_lang['userhistory']);

echo "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
echo "<tr><td class=\"a\"><strong>".$tracker_lang['userhistory']."</strong>: ".$ar_text[$action]." (".$commentcount.")</td>";
echo "<td class=\"a\"><strong>".$tracker_lang['username']."</strong>: ".$subject."</td></tr>";
echo "<tr><td class=\"b\" colspan=\"2\">";
echo "<div id=\"tabs\">\n";
foreach ($ar_text AS $act => $text){
echo "<span onClick=\"document.location.href='userhistory.php?action=".$act."&id=".$userid."'\" title=\"".$tracker_lang['userhistory']." - ".$text."\" class=\"tab ".($action == $act ? "active":"")."\"><nobr>".$text."</nobr></span>\n";
}
echo "</div>";
echo "</td></tr>";
echo "</table>";

if (mysql_num_rows($res) == 0) 
stderr($tracker_lang['error'], $tracker_lang['no_comments']);

if ($commentcount > $perpage) 
echo $pagertop;

begin_main_frame();
begin_frame();

while ($arr = mysql_fetch_assoc($res)){

$commentid = $arr["id"];
$torrent = $arr["name"];

$torrentid = $arr["t_id"];

/*
$subres = sql_query("SELECT COUNT(*) FROM comments WHERE id < ".sqlesc($commentid)." AND torrent = '0' AND poll = '0' AND offer = '0'") or sqlerr(__FILE__, __LINE__);
$subrow = mysql_fetch_row($subres);
$count = $subrow[0];
$comm_page = floor($count/20);
$page_url = $comm_page?"&page=".$comm_page:"";
*/

echo ("<table border='0' cellspacing='0' cellpadding='0' width='100%'>
<tr><td class='a'># ".$commentid." ".(empty($arr["news_subject"]) ? "<s>".$tracker_lang['unknown']."</s>":"<a href=\"detailsoff.php?id=".$arr["news_id"]."\">".htmlspecialchars($arr["news_subject"])."</a>")." (".$arr["added"].")</td></tr>
<tr><td class='b'>".format_comment($arr["text"], true)."</td></tr>
</table>");

}
end_frame();
end_main_frame();

if ($commentcount > $perpage) 
echo $pagerbottom;

stdfoot();
die;
}

else {

stdhead($tracker_lang['userhistory']);

echo "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
echo "<tr><td class=\"a\"><strong>".$tracker_lang['userhistory']."</strong>: ".$tracker_lang['no_select_cat']."</td>";
echo "<td class=\"a\"><strong>".$tracker_lang['username']."</strong>: ".$subject."</td></tr>";
echo "<tr><td class=\"b\" colspan=\"2\">";
echo "<div id=\"tabs\">\n";
foreach ($ar_text AS $act => $text){
echo "<span onClick=\"document.location.href='userhistory.php?action=".$act."&id=".$userid."'\" title=\"".$tracker_lang['userhistory']." - ".$text."\" class=\"tab ".($action == $act ? "active":"")."\"><nobr>".$text."</nobr></span>\n";
}
echo "</div>";
echo "</td></tr>";

echo "<tr><td class=\"a\" colspan=\"2\">";
echo $tracker_lang['click_on_choose'];
echo "</td></tr>";

echo "</table>";

stdfoot();

}


?>