<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

if (get_user_class() <= UC_MODERATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['clases']) && is_array($_POST['clases'])){

$sender_id = ((isset($_POST['sender']) && $_POST['sender'] == 'system') ? 0 : $CURUSER['id']);

$msg = htmlspecialchars_uni((isset($_POST['message']) ? trim($_POST['message']):""));
$subject = htmlspecialchars_uni((isset($_POST['subject']) ? trim($_POST['subject']):""));

if (empty($msg) || empty($subject)){
header("Refresh: 5; url=staffmess.php");
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);
}

$clases = (count($_POST['clases']) ? $_POST['clases']:"");

if (count($clases))
sql_query("INSERT INTO messages (sender, receiver, added, msg, subject) SELECT ".sqlesc($sender_id).", id, ".sqlesc(get_date_time()).", ".sqlesc($msg).", ".sqlesc($subject)." FROM users WHERE id <> ".sqlesc($CURUSER["id"])." AND class IN (".implode(", ", array_map("intval", $clases)).")") or sqlerr(__FILE__,__LINE__);

$counter = mysql_affected_rows();

if (!empty($counter)){
write_log("�������� ��������� �� ������������ ".$CURUSER["username"]." (���������� ".$counter." ���������)", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "other");

header("Refresh: 5; url=staffmess.php");

stderr($tracker_lang['success'], $tracker_lang['mass_pm'].": ".$counter);
die;
}

}

stdhead($tracker_lang['mass_pm']);

echo "<form method=\"post\" action=\"staffmess.php\" name=\"msg\">";

echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
echo "<tr><td class=\"b\">";

echo "<table align=\"center\" cellspacing=\"0\" cellpadding=\"0\">";

echo "<tr><td class=\"a\" colspan=\"2\">".$tracker_lang['mass_pm']."</td></tr>";

echo "<tr><td class=\"b\">".$tracker_lang['class'].": </td>";

echo "<td class=\"a\">"; 
$res1 = sql_query("SELECT class FROM users GROUP BY class") or sqlerr(__FILE__, __LINE__);

echo "<select multiple=\"multiple\" size=\"".(mysql_num_rows($res1)+1)."\" name=\"clases[]\">";
echo "<option value=\"0\" selected>".$tracker_lang['signup_not_selected']."</option>\n";
while ($arr1 = mysql_fetch_array($res1)){
echo "<option value=\"".$arr1["class"]."\" style=\"color: #".get_user_rgbcolor($arr1["class"]).";\">".get_user_class_name($arr1["class"])."</option>\n";
}
echo "</select></td></tr>";

echo "</td></tr>";

echo "<td colspan=\"2\" class=\"a\"><b>".$tracker_lang['subject']."</b>: <input name=\"subject\" type=\"text\" size=\"90\"></td></tr>";

echo "<tr><td align=\"center\" colspan=\"2\">";
echo textbbcode("msg", "message");
echo "</td></tr>";

echo "<tr><td class=\"a\" colspan=\"2\" align=\"center\">";

echo "<b>".$tracker_lang['sender'].":&nbsp;&nbsp;</b><label><b>".get_user_class_color($CURUSER['class'], $CURUSER['username'])."</b> <input name=\"sender\" type=\"radio\" value=\"0\" checked>&nbsp;&nbsp;</label>";

if (get_user_class() > UC_MODERATOR)
echo "<label><font color=\"gray\">[<b>".$tracker_lang['from_system']."</b>]</font><input name=\"sender\" type=\"radio\" value=\"system\"></label>";

echo "</td></tr>";

echo "<tr><td colspan=\"2\" align=\"center\"><input type=\"hidden\" name=\"receiver\" value=\"".$receiver."\"><br /><input type=\"submit\" style=\"height: 25px; width:200px\" value=\"".$tracker_lang['sendmessage']."\" class=\"btn\">";

echo "</td></tr>";
echo "</table>";

echo "</td></tr>";
echo "</table></form>";

stdfoot();
?>