<?
require "include/bittorrent.php";
dbconn(true);
loggedinorreturn();

if (get_user_class() < UC_ADMINISTRATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

if (!empty($_POST["book"]) && is_array($_POST["book"])) {

$res = sql_query("SELECT * FROM users WHERE id IN (".implode(", ", array_map("intval", $_POST["book"])).")") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res)) {

while ($user = mysql_fetch_array($res)){

$class = $user["class"];
$userid = $user["id"];

if (!empty($user["username"]) && get_user_class() <> $class && get_user_class() > $class && $user["override_class"] == '255')
continue;

sql_query("DELETE FROM cheaters WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM messages WHERE receiver = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM karma WHERE user = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE friendid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM bookmarks WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM invites WHERE inviter = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM peers WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM readposts WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM report WHERE userid = ".sqlesc($userid)." OR usertid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM simpaty WHERE fromuserid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM pollanswers WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM shoutbox WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM ratings WHERE user = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM snatched WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM thanks WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM checkcomm WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM sessions WHERE uid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);

if (!empty($user["avatar"]))
@unlink(ROOT_PATH."/pic/avatar/".$user["avatar"]);

@unlink(ROOT_PATH."/cache/monitoring_".$userid.".txt");

write_log("������������ ".$user["username"]." ��� ������ ������������� ".$CURUSER["username"].". �������: ��� � ����������.", "590000", "bans");

}

}

header("Refresh: 0; url=adminbookmark.php");
die;
}

$count = get_row_count("users WHERE addbookmark = 'yes'");

list ($pagertop, $pagerbottom, $limit) = pager(30, $count, "adminbookmark.php?");

if (empty($count))
stderr($tracker_lang['admin_bookmark'], $tracker_lang['no_data_now']);

stdhead($tracker_lang['admin_bookmark']);

echo ("<form method=\"post\" action=\"adminbookmark.php\"><table cellpadding=\"2\" cellspacing=\"1\" border=\"0\" width=\"100%\">");

if (!empty($count))
echo ("<tr><td colspan=\"7\">".$pagertop."</td></tr>");

echo ("<tr>
<td class=\"colhead\" align=\"center\"><b>".$tracker_lang['signup_username']."</b></td>
<td class=\"colhead\" align=\"center\"><b>".$tracker_lang['reason']."</b></td>
<td class=\"colhead\" align=\"center\"><b>".$tracker_lang['uploaded']."</b></td>
<td class=\"colhead\" align=\"center\"><b>".$tracker_lang['downloaded']."</b></td>
<td class=\"colhead\" align=\"center\"><b>".$tracker_lang['ratio']."</b></td>
<td class=\"colhead\" align=\"center\"><b>".$tracker_lang['delete']."</b></td>
</tr>");

$res = sql_query("SELECT id, username, class, bookmcomment, added, uploaded, downloaded FROM users WHERE addbookmark = 'yes' ORDER BY id DESC ".$limit) or sqlerr(__FILE__,__LINE__);

while ($arr = mysql_fetch_assoc($res)) {

if (!empty($arr["downloaded"]))
$ratio = number_format($arr["uploaded"] / $arr["downloaded"], 3);
else
$ratio = "---";

echo "<tr>
<td class=\"b\" align=\"center\"><b><a href=\"userdetails.php?id=".$arr["id"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a></b></td>
<td class=\"b\">".htmlspecialchars_uni($arr["bookmcomment"])."</a></td>
<td class=\"b\" align=\"center\">". mksize($arr["uploaded"])."</td></a></td>
<td class=\"b\" align=\"center\">".mksize($arr["downloaded"])."</td>
<td class=\"b\" align=\"center\"><font color=\"".get_ratio_color($ratio)."\">".$ratio."</font></td>
<td class=\"b\" align=\"center\">".(get_user_class() <> $arr["class"] && get_user_class() > $arr["class"] ? "<input type=\"checkbox\" name=\"book[]\" value=\"".$arr["id"]."\" />":"")."</td>
</tr>";

}

if (!empty($count))
echo ("<tr><td colspan=\"7\" align=\"right\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['delete_user']."\" /></td></tr>");

if (!empty($count))
echo ("<tr><td colspan=\"7\">".$pagerbottom."</td></tr>");

echo ("</table></form>");

stdfoot();
?>