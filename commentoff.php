<?
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

if ($CURUSER["commentpos"] == 'no')
stderr($tracker_lang['error_data'], $tracker_lang['commentpos_no']);

$action = (string) $_GET["action"];

if ($action == "add") {

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$offid = (int) $_POST["tid"];
if (!is_valid_id($offid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT name FROM off_reqs WHERE id = ".sqlesc($offid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$name = $arr[0];
$text = htmlspecialchars($_POST["msg"]);
if (!$text)
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": msg");


if (get_user_class() == UC_SYSOP)
$sender_id = ($_POST['sender'] == 'system' ? 0 : $CURUSER['id']);
else
$sender_id = $CURUSER['id'];

sql_query("INSERT INTO comments (user, offer, added, text, ip) VALUES (".sqlesc($sender_id).", ".sqlesc($offid).", ".sqlesc(get_date_time()).", ".sqlesc($text).", ".sqlesc(getip()).")");

$newid = mysql_insert_id();

sql_query("UPDATE off_reqs SET comments = comments+1 WHERE id = ".sqlesc($offid)) or sqlerr(__FILE__,__LINE__);

/////////////////�������� �� ����������/////////////////
$res3 = sql_query("SELECT * FROM checkcomm WHERE checkid = ".sqlesc($offid)." AND offer = '1'") or sqlerr(__FILE__,__LINE__);
$subject = "����� �����������";
while ($arr3 = mysql_fetch_array($res3)) {

$msg = "� ������� [url=".$DEFAULTBASEURL."/detailsoff.php?id=".$offid."&viewcomm=".$newid."#comm".$newid."]".$name."[/url] ��� �������� ����� �����������.";

if ($CURUSER["id"] <> $arr3["userid"])
sql_query("INSERT INTO messages (sender, receiver, added, msg, location, subject) VALUES ('0', ".sqlesc($arr3["userid"]).", ".sqlesc(get_date_time()).", ".sqlesc($msg).", '1', ".sqlesc($subject).")");
}
/////////////////�������� �� ����������/////////////////

header("Location: detailsoff.php?id=".$offid."&viewcomm=".$newid."#comm".$newid);
die;
}

$offid = (int) $_GET["tid"];
if (!is_valid_id($offid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT name FROM off_reqs WHERE id = ".sqlesc($offid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['requests_section'].": ".$tracker_lang['add_comment']);

begin_frame($tracker_lang['requests_section'].": ".$tracker_lang['add_comment'], true);

$name = $arr["name"];

echo ("<form name=\"form\" method=\"post\" action=\"commentoff.php?action=add\">\n");
echo ("<input type=\"hidden\" name=\"tid\" value=\"".$offid."\" />\n");

echo ("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">\n");
echo ("<tr><td class=\"colhead\" align=\"left\">".$tracker_lang['add_comment']."</td><tr>\n");

echo ("<tr><td align=\"center\">".textbbcode("form","msg","")."
<input type=\"submit\" value=\"".$tracker_lang['add_comment']."\" class=\"btn\" />
</td></tr>");

echo "</table></form>";

$res = sql_query("SELECT comments.id, text, comments.added, username, users.id as user, users.avatar, users.uploaded, users.downloaded, users.class, users.enabled, users.parked, users.warned, users.donor
FROM comments 
LEFT JOIN users ON comments.user = users.id WHERE offer = ".sqlesc($offid)." ORDER BY comments.id DESC LIMIT 5");

$allrows = array();
while ($row = mysql_fetch_array($res))
$allrows[] = $row;

if (count($allrows))
commenttable($allrows, "commentoff");

stdfoot();
die;

} elseif ($action == "quote") {

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT c.*, o.name, o.id AS oid, u.username
FROM comments AS c
LEFT JOIN off_reqs AS o ON c.offer = o.id
LEFT JOIN users AS u ON c.user = u.id
WHERE c.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);

$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$text = "[quote=".$arr["username"]."]".$arr["text"]."[/quote]\n";
$offid = $arr["oid"];

stdhead($tracker_lang['requests_section'].": ".$tracker_lang['add_comment']);

begin_frame($tracker_lang['requests_section'].": ".$tracker_lang['add_comment'], true);

$name = $arr["name"];

echo ("<form name=\"form\" method=\"post\" action=\"commentoff.php?action=add\">\n");
echo ("<input type=\"hidden\" name=\"tid\" value=\"".$offid."\" />\n");

echo ("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n");
echo ("<tr><td class=\"colhead\" align=\"left\">".$tracker_lang['add_comment']."</td><tr>\n");
echo ("<tr><td align=\"center\">".textbbcode("form", "msg", htmlspecialchars_uni($text))."
<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['add_comment']."\" />
</td></tr>");

echo ("</table></form>\n");

stdfoot();
die;

} elseif ($action == "edit") {

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT c.*, o.name
FROM comments AS c
LEFT JOIN off_reqs AS o ON c.offer = o.id
WHERE c.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);

if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if ($arr["user"] <> $CURUSER["id"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$text = htmlspecialchars_uni($_POST["msg"]);
$returnto = htmlentities($_POST["returnto"]);

if (empty($text))
stderr($tracker_lang['error'], $tracker_lang['no_fields_blank']);

sql_query("UPDATE comments SET text = ".sqlesc($text).", ".(empty($arr["ori_text"]) ? "ori_text = ".sqlesc($arr["text"]).", ":"")." editedat = ".sqlesc(get_date_time()).", editedby = ".sqlesc($CURUSER["id"])." WHERE id = ".sqlesc($commentid)) or sqlerr(__FILE__, __LINE__);

if ($returnto)
header("Location: ".$returnto);
else
header("Location: ".$DEFAULTBASEURL);
die;

}

stdhead($tracker_lang['requests_section'].": ".$tracker_lang['editing']);

begin_frame($tracker_lang['requests_section'].": ".$tracker_lang['editing'], true);

echo ("<form name=\"form\" method=\"post\" action=\"commentoff.php?action=edit&cid=".$commentid."\">\n");
echo ("<input type=\"hidden\" name=\"returnto\" value=\"".$_SERVER["HTTP_REFERER"]."\" />\n");
echo ("<input type=\"hidden\" name=\"cid\" value=\"".$commentid."\" />\n");
echo ("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">\n");

echo ("<tr><td class=\"colhead\" align=\"left\">".$tracker_lang['editing']."</td><tr>\n");

echo ("<tr><td align=\"center\">
".textbbcode("form","msg", htmlspecialchars($arr["text"]))."
<input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['edit']."\" />
</td></tr>");

echo ("</form></table>\n");

stdfoot();
die;

} elseif ($action == "check" || $action == "checkoff") {

$from = $_SERVER["HTTP_REFERER"];

$tid = (int) $_GET["tid"];
if (!is_valid_id($tid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$docheck = mysql_fetch_array(sql_query("SELECT COUNT(*) FROM checkcomm WHERE checkid = ".sqlesc($tid)." AND userid = ".sqlesc($CURUSER["id"])." AND offer = '1'"));

if ($docheck[0] > 0 && $action == "check"){

if ($from)
@header("Refresh: 3; url=".$from);
else
@header("Refresh: 3; url=detailsoff.php?id=".$tid."#startcomments") or die("<script>setTimeout('document.location.href=\"detailsoff.php?id=".$tid."#startcomments\"', 10);</script>");

}

if ($action == "check") {
sql_query("INSERT INTO checkcomm (checkid, userid, offer) VALUES (".sqlesc($tid).", ".sqlesc($CURUSER["id"]).", '1')") or sqlerr(__FILE__,__LINE__);

if ($from)
@header("Refresh: 3; url=".$from);
else 
@header("Refresh: 3; url=detailsoff.php?id=".$tid."#startcomments");

stderr($tracker_lang['error_data'], "<p>".$tracker_lang['enable_funcs']." - ".$tracker_lang['monitor_comments'].".</p><a href=\"".$from."\">".$tracker_lang['back_inlink']."</a> ".$tracker_lang['or']." <a href=\"detailsoff.php?id=".$tid."#startcomments\">".$tracker_lang['request']."</a>");

} else {

sql_query("DELETE FROM checkcomm WHERE checkid = ".sqlesc($tid)." AND userid = ".sqlesc($CURUSER["id"])." AND offer = '1'") or sqlerr(__FILE__,__LINE__);

if ($from)
@header("Refresh: 3; url=".$from);
else
@header("Refresh: 3; url=detailsoff.php?id=".$tid."#startcomments") or die("<script>setTimeout('document.location.href=\"detailsoff.php?id=".$tid."#startcomments\"', 10);</script>");

stderr($tracker_lang['error_data'], "<p>".$tracker_lang['disable_funcs']." - ".$tracker_lang['monitor_comments_disable'].".</p><a href=".$DEFAULTBASEURL."/detailsoff.php?id=".$tid."#startcomments>".$tracker_lang['back_inlink']."</a>");

}

} elseif ($action == "delete") {

if (get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if (!isset($_GET["sure"])) {
$referer = htmlspecialchars($_SERVER["HTTP_REFERER"]);
stderr($tracker_lang['delete']." ".$tracker_lang['comment'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $tracker_lang['comment'], "commentoff.php?action=delete&cid=".$commentid."&sure=1".($referer ? "&returnto=".($referer): "")));
}

$res = sql_query("SELECT offer FROM comments WHERE id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if ($arr)
$offid = $arr["offer"];

sql_query("DELETE FROM comments WHERE id = ".sqlesc($commentid));

if ($offid && mysql_affected_rows() > 0)
sql_query("UPDATE off_reqs SET comments = comments - 1 WHERE id = ".sqlesc($offid)) or sqlerr(__FILE__,__LINE__);

$returnto = htmlspecialchars($_GET["returnto"]);
if ($returnto)
header("Location: ".$returnto);
else
header("Location: ".$DEFAULTBASEURL);
die;


} elseif ($action == "vieworiginal" && get_user_class() > UC_MODERATOR) {

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT c.*, t.name 
FROM comments AS c 
LEFT JOIN off_reqs AS t ON c.offer = t.id WHERE c.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['requests_section'].": ".$tracker_lang['message_view']);

if (empty($arr["ori_text"]))
$arr["ori_text"] = $arr["text"];

echo ("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">");
echo ("<tr><td class=\"colhead\">".$tracker_lang['original_com'].": ".$commentid."</td></tr>");
echo ("<tr><td class=\"b\">".format_comment($arr["ori_text"], true)."</td></tr>");

$returnto = htmlentities($_SERVER["HTTP_REFERER"]);
if (!empty($returnto))
echo ("<tr><td class=\"a\"><a href=\"".$returnto."\">".$tracker_lang['ago']."</a></td></tr>");

echo ("</table>\n");
stdfoot();
die;

} else 

stderr($tracker_lang['error'], $tracker_lang['no_data']);

?>