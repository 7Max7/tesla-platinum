<?
require_once("include/bittorrent.php");
dbconn(false);
//loggedinorreturn();


$del = (!empty($_GET['del']) ? intval($_GET['del']):"");

if (get_user_class() == UC_SYSOP){

if (isset($_GET['sure']) && $_GET['sure'] == "yes" && !empty($del)) {

sql_query("DELETE FROM groups WHERE id = ".sqlesc($del)) or sqlerr(__FILE__, __LINE__);

unsql_cache("browse_gr_array");
unsql_cache("details_groups-".$del);
unsql_cache("group_data_".$del);
unsql_cache("traff_".$del);
unsql_cache("block-groups"); /// ���� ������

header("Location: groups.php");
die;

} elseif (!empty($del) && !isset($_GET['sure'])){

$query = sql_query("SELECT * FROM groups WHERE id = ".sqlesc($del)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($query);

if (mysql_num_rows($query))
stderr($tracker_lang['error'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], "<strong>".$row["name"]."</strong> (".$tracker_lang['group_realese'].")", "groups.php?del=".$del."&sure=yes"));
}

if (isset($_GET['edited']) && isset($_POST['group_name']) && isset($_POST['group_image'])) {

$id = (!empty($_GET['id']) ? intval($_GET['id']):"");

$group_name = htmlspecialchars_uni(!empty($_POST['group_name']) ? $_POST['group_name']:"");
$group_image = htmlspecialchars_uni(!empty($_POST['group_image']) ? $_POST['group_image']:"");
$comment = htmlspecialchars_uni(!empty($_POST['comment']) ? $_POST['comment']:"");

if (empty($group_name) || empty($group_image) || empty($comment))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": <strong>
".(empty($id) ? $tracker_lang['invalid_id']:"")."
".(empty($group_name) ? $tracker_lang['name']:"")."
".(empty($group_image) ? $tracker_lang['image_is']:"")."
".(empty($comment) ? $tracker_lang['description']:"")."
</strong>");

if (!preg_match('/^(.+)\.(jpg|jpeg|png|gif)$/si', $group_image))
stderr($tracker_lang['add_relgroup'], $tracker_lang['invalid_typeimage']." ".$tracker_lang['avialable_formats'].": ".implode(", ", array("image/gif" => "gif", "image/png" => "png", "image/jpeg" => "jpeg", "image/jpg" => "jpg")));

sql_query("UPDATE groups SET name = ".sqlesc($group_name).", image = ".sqlesc($group_image).", comment = ".sqlesc($comment)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("traff_".$id);
unsql_cache("group_data_".$id);
unsql_cache("browse_gr_array");
unsql_cache("details_groups-".$id);

header("Location: groups.php");
die;

}


/// ����������� ������
$editid = (!empty($_GET['editid']) ? intval($_GET['editid']):"");
if (!empty($editid)) {

$query = sql_query("SELECT * FROM groups WHERE id = ".sqlesc($editid)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($query);

if (!mysql_num_rows($query))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['upload_groups']);


$array_pic = array();
$dh = opendir(ROOT_PATH.'/pic/groups/');

while ($file = readdir($dh)) : if (preg_match('/^(.+)\.$/si', $file, $matches))
$file = $matches[1];

if (preg_match('/^(.+)\.(jpg|jpeg|png|gif)$/si', $file))
$array_pic[] = "<option ".($file == $row["image"] ? "selected":"")." value=\"".$file."\">".$file."</option>";

endwhile; closedir($dh);


echo("<form method=\"post\" action=\"groups.php?id=".$editid."&edited=1\">");
echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">";
echo("<input type=\"hidden\" name=\"id\" value=\"".$editid."\" />");
echo("<tr><td class=\"a\">#</td><td class=\"b\" align=\"left\">".$editid."</td></tr>");

echo("<tr><td class=\"b\">".$tracker_lang['name']."</td><td class=\"a\" align=\"left\"><input type=\"text\" size=\"50\" name=\"group_name\" value=\"".$row["name"]."\" /> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>");

if (count($array_pic)){
echo "<tr>
<td class=\"a\">".$tracker_lang['friend_urlban']."</td>
<td class=\"b\">".$tracker_lang['enter_cat_name'].": <select name=\"group_image\">".implode("\n", $array_pic)."</select>
</td>
</tr>";
} else
echo("<tr><td class=\"a\">".$tracker_lang['friend_urlban']."</td><td class=\"b\" align=\"left\"><input type=\"text\" size=\"50\" name=\"group_image\" value=\"".$row["image"]."\" /> <i>".sprintf($tracker_lang['max_simp_of'], 128)."</i></td></tr>");


echo("<tr><td class=\"b\">".$tracker_lang['description']."</td><td class=\"a\" align=\"left\"><textarea cols=\"75\" rows=\"3\" name=\"comment\" value=\"".$row["comment"]."\">".$row["comment"]."</textarea> <i>".sprintf($tracker_lang['max_simp_of'], 1024)."</i></td></tr>");

echo("<tr><td align=\"right\" class=\"b\" colspan=\"2\"><input class=\"btn\" type=\"submit\" value=\"".$tracker_lang['edit']."\"></td></tr>");
echo("</table></form>");

stdfoot();
die;
}

if (isset($_GET['add']) && $_GET['add'] == "true") {

$group_name = htmlspecialchars_uni(!empty($_POST['group_name']) ? $_POST['group_name']:"");
$group_image = htmlspecialchars_uni(!empty($_POST['group_image']) ? $_POST['group_image']:"");

$comment = htmlspecialchars_uni(!empty($_POST['comment']) ? $_POST['comment']:"");

if (empty($group_name) || empty($group_image) || empty($comment))
stderr($tracker_lang['add_relgroup'], $tracker_lang['invalid_id_value'].": <strong>
".(empty($id) ? $tracker_lang['invalid_id']:"")."
".(empty($group_name) ? $tracker_lang['name']:"")."
".(empty($group_image) ? $tracker_lang['friend_urlban']:"")."
".(empty($comment) ? $tracker_lang['description']:"")."
</strong>");


if (!preg_match('/^(.+)\.(jpg|jpeg|png|gif)$/si', $group_image))
stderr($tracker_lang['add_relgroup'], $tracker_lang['invalid_typeimage']." ".$tracker_lang['avialable_formats'].": ".implode(", ", array("image/gif" => "gif", "image/png" => "png", "image/jpeg" => "jpeg", "image/jpg" => "jpg")));

sql_query("INSERT INTO groups SET name = ".sqlesc($group_name).", comment = ".sqlesc($comment).", image = ".sqlesc($group_image)) or sqlerr(__FILE__, __LINE__);

unsql_cache("block-groups"); /// ���� ������
unsql_cache("browse_gr_array");

header("Location: groups.php");
die;

} elseif (isset($_GET['add']) && $_GET['add'] == "yes"){


$array_pic = array();
$dh = opendir(ROOT_PATH.'/pic/groups/');

while ($file = readdir($dh)) : if (preg_match('/^(.+)\.$/si', $file, $matches))
$file = $matches[1];

if (preg_match('/^(.+)\.(jpg|jpeg|png|gif)$/si', $file))
$array_pic[] = "<option value=\"".$file."\">".$file."</option>";

endwhile; closedir($dh);

stdhead($tracker_lang['add_relgroup']);

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">";
echo("<form method=\"post\" action=\"groups.php?add=true\">");

echo("<tr><td align=\"center\" class=\"colhead\" colspan=\"2\">".$tracker_lang['add_relgroup']."</td></tr>");

echo("<tr><td class=\"a\"><b>".$tracker_lang['name']."</b>: </td><td align=\"left\"><input type=\"text\" size=\"50\" name=\"group_name\"> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>");


if (count($array_pic)){
echo "<tr>
<td class=\"a\">".$tracker_lang['friend_urlban']."</td>
<td class=\"b\">".$tracker_lang['enter_cat_name'].": <select name=\"group_image\">".implode("\n", $array_pic)."</select> <input type=\"hidden\" name=\"add\" value=\"true\" />
</td>
</tr>";
} else
echo("<tr><td class=\"a\"><b>".$tracker_lang['friend_urlban']."</b>: </td><td align=\"left\"><input type=\"text\" size=\"50\" name=\"group_image\"> <i>".sprintf($tracker_lang['max_simp_of'], 128)."</i> <input type=\"hidden\" name=\"add\" value=\"true\" /></td></tr>");


echo("<tr><td class=\"a\"><b>".$tracker_lang['description']."</b>: </td><td align=\"left\"><textarea cols=\"50\" rows=\"3\" name=\"comment\"></textarea> <i>".sprintf($tracker_lang['max_simp_of'], 1024)."</i></td></tr>");

echo("<tr><td align=\"center\" class=\"b\" colspan=\"2\"><br />".sprintf($tracker_lang['add_relimage'], "<u>/pic/groups/</u>", "<u>/pic/groups/</u><b>rtesla.gif</b>", "<b>rtesla.gif</b>")."</td></tr>");

echo("<tr><td colspan=\"2\" class=\"a\" align=\"right\"><input class=\"btn\" value=\"".$tracker_lang['add_relgroup']."\" type=\"submit\"></td></tr>");

echo("</form>");
echo("</table>");

stdfoot();
die;
}


}


stdhead($tracker_lang['upload_groups'], true);

$perpage = 25;
$count = get_row_count("groups");
list ($pagertop, $pagerbottom, $limit) = pager($perpage, $count, "groups.php?");


echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">";

if (get_user_class() == UC_SYSOP)
echo "<tr><td colspan=\"6\" class=\"b\">".$tracker_lang['admincp'].": <a href=\"groups.php?add=yes\">".$tracker_lang['add_relgroup']."</a></td></tr>";

if (empty($count)){
echo "<tr><td colspan=\"6\" align=\"center\" class=\"b\">".$tracker_lang['sum_nodata']."</td></tr>";
echo "</table>";
stdfoot(true);
die;
}

if ($count > $perpage)
echo "<tr><td colspan=\"6\">".$pagertop."</td></tr>";

echo("<tr>
<td class=\"colhead\" width=\"10\" align=\"center\">#</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['friend_urlban']." / ".$tracker_lang['name']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['description']." / ".$tracker_lang['traffic']."</td>
".(get_user_class() == UC_SYSOP ? "<td class=\"colhead\" align=\"center\">".$tracker_lang['action']."</td>":"")."
</tr>");

$sql = sql_query("SELECT * FROM groups ORDER BY id ".$limit) or sqlerr(__FILE__, __LINE__);

$num = 0;
while ($row = mysql_fetch_assoc($sql)) {

if ($num%2==0){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

$id = $row['id'];

$sql2 = sql_query("SELECT username, class, id AS t, (SELECT COUNT(*) FROM torrents WHERE owner=t) AS num_torrent, (SELECT SUM(size) FROM torrents WHERE owner=t) AS num_size FROM users WHERE groups = ".$id, $cache = array("type" => "disk", "file" => "group_data_".$id, "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);

$num_torrent = 0;
$users = $userid = array();

while ($row2 = mysql_fetch_assoc_($sql2)) {

$num_size = 0;
$num_torrent +=$row2["num_torrent"];
$num_size += $row2["num_size"];

$users[] = "<a href=\"userdetails.php?id=".$row2["t"]."\">".get_user_class_color($row2["class"], $row2["username"])."</a>".(!empty($row2["num_torrent"]) ? " (<a title=\"".$tracker_lang['torrents']."\" href=\"browse.php?userid=".$row2["t"]."\">".$row2["num_torrent"]."</a>)":"");

$userid[] = $row2["t"];

}

$traff_up = $traff_down = 0;

if (count($userid)){

$data = sql_query("SELECT sum(uploaded) AS datau, sum(downloaded) AS datad FROM snatched WHERE torrent IN (SELECT id FROM torrents WHERE owner IN (".implode(",", $userid)."))", $cache = array("type" => "disk", "file" => "traff_".$id, "time" => 60*60*48)) or sqlerr(__FILE__, __LINE__);
$a = mysql_fetch_assoc_($data);


$traff_down += $a["datad"];
$traff_up += $a["datau"];

}

echo("<tr>
<td ".$cl2." width=\"2%\" rowspan=\"2\" align=\"center\">".$id."</td>
<td ".$cl1." width=\"10%\" rowspan=\"2\" align=\"center\"><a title=\"".$tracker_lang['torrents']."\" href=\"browse.php?gr=".$id."\"><img src=\"/pic/groups/".$row['image']."\" border=\"0\" /><br />".$row['name']."</a></td>

<td ".$cl2." align=\"center\">

<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"100%\">
".(count($users) ? "<tr><td class=\"b\" align=\"left\" colspan=\"4\">".$tracker_lang['reliusers']." (".count($users)."): ".implode(", ", $users)."</td></tr>":"")."
<tr>
<td class=\"a\" width=\"25%\" align=\"center\"><b>".$tracker_lang['torrents']."</b>: ".$num_torrent."</td>
<td class=\"a\" width=\"25%\" align=\"center\"><b>".$tracker_lang['all_size']."</b>: ".mksize($num_torrent)."</td>
<td class=\"a\" width=\"25%\" align=\"center\"><b>".$tracker_lang['downloaden']."</b>: ".mksize($traff_down)." (".$tracker_lang['traffic'].")</td>
<td class=\"a\" width=\"25%\" align=\"center\"><b>".$tracker_lang['uploaden']."</b>: ".mksize($traff_up)." (".$tracker_lang['traffic'].")</td>
</tr>
</table>

</td>

".(get_user_class() == UC_SYSOP ? "<td ".$cl1." rowspan=\"2\" align=\"center\" width=\"15%\"><a title=\"".$tracker_lang['edit']."\" href=\"groups.php?editid=".$id."\">".$tracker_lang['edit']."</a><br /><br /><a  title=\"".$tracker_lang['delete']."\" href=\"groups.php?del=".$id."\">".$tracker_lang['delete']."</a></td>":"")."

</tr>");

echo("<tr><td ".$cl1." width=\"100%\" align=\"left\"><strong>".$tracker_lang['description']."</strong>: ".(!empty($row['comment']) ? format_comment($row['comment']):"---")."</td></tr>");

++$num;
}

if ($count > $perpage)
echo "<tr><td colspan=\"6\">".$pagerbottom."</td></tr>";

echo "</table>";

stdfoot(true);

?>