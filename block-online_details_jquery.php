<?
require_once 'include/bittorrent.php';

dbconn(false,true);
header("Content-Type: text/html; charset=".$tracker_lang['language_charset']);
  
if ($_SERVER['HTTP_X_REQUESTED_WITH'] <> 'XMLHttpRequest')
die ('XMLHttpRequest');

$id = (int) $_POST["id"];

if (empty($id)) die($tracker_lang['error_data']);

if (!$CURUSER) die;

$url = "/userdetails.php?id=".$id;

$res_s = sql_query("SELECT DISTINCT uid, username, class,ip FROM sessions WHERE time > ".sqlesc(get_date_time(gmtime() - 180))." AND (LEFT(url, ".strlen($url).") = ".sqlesc($url)." OR url = ".sqlesc($url).") ORDER BY time DESC") or sqlerr(__FILE__,__LINE__);

$title_who_s = array();
while ($ar_r = mysql_fetch_assoc($res_s)) {

$username = $ar_r['username'];

if (!empty($username))
$title_who_s[] = "<a href=\"userdetails.php?id=".$ar_r['uid']."\">".get_user_class_color($ar_r["class"], $ar_r["username"]) . "</a>";
else 
$title_who_s[] = $ar_r['ip'];

}

if (count($title_who_s))
echo implode(", ", $title_who_s);
else
echo $tracker_lang['no_data'];

?>