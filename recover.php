<?
require "include/bittorrent.php";
dbconn(false);

global $CURUSER, $SITE_Config;

if (!empty($CURUSER)){
@header("Location: ../index.php");
die();
}

if ($SITE_Config["maxlogin"] == true) failedloginscheck();

$ip = getip();


if ($_SERVER["REQUEST_METHOD"] == "POST") {

$email = (isset($_POST["email"]) ? htmlentities($_POST["email"]):"");

if (!empty($email)){


/// ������ '������' ������������ �� �������� ������
if ($SITE_Config["captcha"] == true){

include_once("include/functions_captcha.php");
$acaptcha = creating_captcha();

$p_captcha = trim(isset($_POST["captcha"]) ? (string) $_POST["captcha"] : false);
$p_hash = trim(isset($_POST["hash"]) ? (string) $_POST["hash"] : false);

if (valid_captcha($p_captcha, $p_hash) == false){

stdhead(isset($_POST['act']) ? $tracker_lang['recover_reconfirm']:$tracker_lang['recover_email']);

echo "<form method=\"post\" action=\"recover.php\">";
echo "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";

echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['email'].":</td><td class=\"a\" align=\"left\">".$email."</td></tr>";

if ($p_hash <> false || $p_captcha <> false)
echo "<tr><td class=\"a\" colspan=\"2\" align=\"center\" style=\"font-weight: bold;\">".$tracker_lang['captcha_repeat']."</td></tr>";


echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['captcha_confirm'].":</td><td class=\"a\" align=\"left\">

<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">

<tr><td class=\"b\" align=\"center\" style=\"margin: 0px; padding: 0px;\">
<div id=\"result\">".$acaptcha["echo"]."</div>
<input type=\"hidden\" name=\"hash\" value=\"".$acaptcha["hash"]."\" />
</td>
<td class=\"a\" width=\"50px\"><a href=\"#\" id=\"status\" class=\"button button-blue\"><span>".$tracker_lang['captcha_button']."</span></a></td>
</tr>

<tr><td class=\"a\" colspan=\"2\"><input type=\"text\" name=\"captcha\" size=\"20\" value=\"\" /> ".$tracker_lang['captcha_take']."</td></tr>

</table>
</td></tr>";

echo '<script type="text/javascript">
jQuery("#status").click(function() {
var onput = jQuery("#old").attr("value");
jQuery.post("md5.php",{"update": "true", "hash": "'.$acaptcha["hash"].'", "old": onput},
function(response) {jQuery("#result").fadeIn("slow"); jQuery("#result").html(response);}, "html");
});
</script>';

echo "<tr><td class=\"b\" colspan=\"2\" align=\"center\">".((isset($_POST['act']) && $_POST['act'] == 'reconfirm') ? "<input type=\"hidden\" name=\"act\" value=\"reconfirm\" /> ":"")."<input type=\"hidden\" name=\"email\" value=\"".$email."\" /><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['recover_email']."\" /></td></tr>";

echo "</table>";
echo "</form><br />";

stdfoot();
die;

}

}
/// ������ '������' ������������ �� �������� ������


if (!validemail($email)) // �����������
stderr($tracker_lang['error'], $tracker_lang['validemail']);

$res = sql_query("SELECT id, status, enabled, passhash, email, editsecret FROM users WHERE email = ".sqlesc($email)." LIMIT 1") or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res) or stderr((isset($_POST['act']) ? $tracker_lang['recover_reconfirm']:$tracker_lang['recover_email']), $tracker_lang['nouser_email']);


if (isset($_POST['act']) && $_POST['act'] == 'reconfirm'){

if ($arr["status"] <> "pending")
stderr($tracker_lang['recover_reconfirm'], $tracker_lang['activated']);

if ($row["enabled"] == "no")
stderr($tracker_lang['recover_reconfirm'], $tracker_lang['recover_nosel']);

$id = $arr['id'];
$psecret = md5($arr['editsecret']);

$body = <<<EOD
�� ��� ���-�� ������, �������� ��������� ������ ��� ������������� �������� �������� � ���� ������� ($email).

���� ��� ���� �� ��, �������������� ��� ������. �������, ������� ����� ��� E-Mail ����� ����� IP ����� {$ip}.

����� ����������� ���� ������ �����������, ������ �� ��������� ������:
$DEFAULTBASEURL/confirm.php?id=$id&secret=$psecret

�������, ���������� �� ���� E-Mail, ��������� �� ��������� ������:
$DEFAULTBASEURL/userdetails.php?id=$id

����� ���� ��� ��� ��������, ������� ����� ������� ����������� � ������� ����� �� ���� �� ����� ����:
$DEFAULTBASEURL

� ����� ������������ ������ ��������, ������� ���������� ��������� ������ � ����� � ����, ����� � ������ �����, �� ������ ��� ������� � ������ ������������ ��� ��� ������.
--
$SITENAME
EOD;

$subject = <<<EOD
������������� ����������� (��������� ������) �� $SITENAME
EOD;


@sent_mail($arr["email"], $SITENAME, $SITEEMAIL, $subject, $body) or stderr($tracker_lang['error'], $tracker_lang['enabled_sendmail']);

} else {

if ($arr["status"] == "pending" || $arr["enabled"] == "no")
stderr($tracker_lang['recover_email'], $tracker_lang['recover_nosel']);

$sec = mksecret();
sql_query("UPDATE users SET editsecret = ".sqlesc($sec)." WHERE id = ".$arr["id"]) or sqlerr(__FILE__, __LINE__);

if (!mysql_affected_rows())
stderr($tracker_lang['error'], $tracker_lang['error_unknown']);

unsql_cache("arrid_".$arr["id"]);

$hash = md5($sec.$email.$arr["passhash"].$sec);
$dybl_secret = sha1($sec.$email.$hash); # �������� �������

$body = <<<EOD
�� ��� ���-�� ������, �������� ����� ������ � �������� ��������� � ���� ������� ($email).

������ ��� ������ ��������� � IP ������� {$ip}.
���� ��� ���� �� ��, �������������� ��� ������.

����� ����������� ���� ������, ������ �� ��������� ������:
$DEFAULTBASEURL/recover.php?id={$arr["id"]}&secret=$hash&dybl_secret=$dybl_secret

�������, ���������� �� ���� E-Mail, ��������� �� ��������� ������:
$DEFAULTBASEURL/userdetails.php?id={$arr["id"]}

����� ���� ��� ��� ��������, ��� ������ ����� ������� � ����� ������ ����� ��������� ��� �� E-Mail.

� ����� ������������ ������ ��������, ������� ���������� ��������� ������ � ����� � ����, ����� � ������ �����, �� ������ ��� ������� � ������ ������������ ��� ��� ������.
--
$SITENAME
EOD;

@sent_mail($arr["email"], $SITENAME, $SITEEMAIL, "������������� �������������� ������ �� ".$SITENAME, $body) or stderr($tracker_lang['error'], $tracker_lang['enabled_sendmail']);
}

stderr($tracker_lang['success'], $tracker_lang['recover_sendemail']);
die;
}

$id_username = (isset($_POST["id_username"]) ? (int) $_POST["id_username"]:""); /// id ������������
$username_id = (isset($_POST["username_id"]) ? htmlspecialchars($_POST["username_id"]):""); /// ��� ������������

//if (empty($id_username) && empty($username_id) && empty(userid))
//stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].print_r($_POST, 1));

if (!empty($username_id) && !validusername($username_id))
stderr($tracker_lang['error'], $tracker_lang['validusername']);

if (!empty($username_id) && strlen($username_id) > 12)
stderr($tracker_lang['error'], $tracker_lang['signup_username'].": ".sprintf($tracker_lang['max_simp_of'], 12));

if (!empty($id_username) && !is_valid_id($id_username))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);


if (!empty($id_username) || !empty($username_id)){

if ($SITE_Config["maxlogin"] == true && !$CURUSER)
failedloginscheck(); /// �������� ������� �� ip ������

if (!empty($username_id)){

$res = sql_query("SELECT id, enabled, question, status, rejoin FROM users WHERE username = ".sqlesc($username_id)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], sprintf($tracker_lang['no_user_isname'], htmlspecialchars($username_id)));

} else {

$res = sql_query("SELECT id, enabled, question, status, rejoin FROM users WHERE id = ".sqlesc($id_username)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], sprintf($tracker_lang['no_user_isid'], $id_username));

}

$row = mysql_fetch_array($res);

if ($row["status"] == "pending" || $row["enabled"] == "no" || empty($row["question"]) || empty($row["rejoin"]))
stderr($tracker_lang['recover_secansv'], $tracker_lang['recover_nosel']);

$question = htmlspecialchars_uni(strip_tags($row["question"]));

if (!empty($question)){

logoutcookie();

stdhead($tracker_lang['recover']);


echo "<form method=\"post\" action=\"recover.php\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['sec_question']."</td></tr>";

echo "<tr><td class=\"a\" width=\"20%\" align=\"left\"><b>".$tracker_lang['sec_question']."</b>: </td><td class=\"a\"><b>".$question."</b></td></tr>";
echo "<tr><td class=\"a\" width=\"20%\" align=\"left\"><b>".$tracker_lang['sec_answer']."</b>: </td><td class=\"a\"><input type=\"password\" size=\"40\" name=\"do_you_wanna\" /></td></tr>
";



/// ������ '������' ������������ �� �������� ������
if ($SITE_Config["captcha"] == true){

include_once("include/functions_captcha.php");
$acaptcha = creating_captcha();

$p_captcha = trim(isset($_POST["captcha"]) ? (string) $_POST["captcha"] : false);
$p_hash = trim(isset($_POST["hash"]) ? (string) $_POST["hash"] : false);

if (valid_captcha($p_captcha, $p_hash) == false){

if ($p_hash <> false || $p_captcha <> false)
echo "<tr><td class=\"a\" colspan=\"2\" align=\"center\" style=\"font-weight: bold;\">".$tracker_lang['captcha_repeat']."</td></tr>";

echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['captcha_confirm'].":</td><td class=\"a\" align=\"left\">

<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">

<tr><td class=\"b\" align=\"center\" style=\"margin: 0px; padding: 0px;\">
<div id=\"result\">".$acaptcha["echo"]."</div>
<input type=\"hidden\" name=\"hash\" value=\"".$acaptcha["hash"]."\" />
</td>
<td class=\"a\" width=\"50px\"><a href=\"#\" id=\"status\" class=\"button button-blue\"><span>".$tracker_lang['captcha_button']."</span></a></td>
</tr>

<tr><td class=\"a\" colspan=\"2\"><input type=\"text\" name=\"captcha\" size=\"20\" value=\"\" /> ".$tracker_lang['captcha_take']."</td></tr>

</table>
</td></tr>";

echo '<script type="text/javascript">
jQuery("#status").click(function() {
var onput = jQuery("#old").attr("value");
jQuery.post("md5.php",{"update": "true", "hash": "'.$acaptcha["hash"].'", "old": onput},
function(response) {jQuery("#result").fadeIn("slow"); jQuery("#result").html(response);}, "html");
});
</script>';

}

}
/// ������ '������' ������������ �� �������� ������



echo "<tr><td colspan=\"2\" align=\"center\"><input type=\"hidden\" name=\"userid\" value=\"".$row["id"]."\" /> <input type=\"submit\" name=\"post\" class=\"btn\" value=\"".$tracker_lang['selected']."\" /></td></tr>";

echo "</table></form><br />";

stdfoot();
die;
}

}

$do_you_wanna = (isset($_POST["do_you_wanna"]) ? htmlspecialchars_uni(strip_tags($_POST['do_you_wanna'])):""); // ��� ������������

if (!empty($do_you_wanna)) {

if ($SITE_Config["maxlogin"] == true && !$CURUSER)
failedloginscheck(); /// �������� ������� �� ip ������

$userid = (int) $_POST["userid"]; // id ������������

if (empty($userid) || !is_valid_id($userid) || !empty($row_do))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);




/// ������ '������' ������������ �� �������� ������
if ($SITE_Config["captcha"] == true){

include_once("include/functions_captcha.php");

$p_captcha = trim(isset($_POST["captcha"]) ? (string) $_POST["captcha"] : false);
$p_hash = trim(isset($_POST["hash"]) ? (string) $_POST["hash"] : false);

}
/// ������ '������' ������������ �� �������� ������






$res_do = sql_query("SELECT id, shelter, rejoin, added, username, class, question, email, enabled, status FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res_do) == 0)
stderr($tracker_lang['error'], sprintf($tracker_lang['no_user_isid'], $userid));
else {

$row_do = mysql_fetch_array($res_do);

if (empty($row_do["question"]) || empty($row_do["rejoin"]) || $row_do["enabled"] == 'no' || $row_do["status"] <> 'confirmed')
stderr($tracker_lang['recover_secansv'], $tracker_lang['recover_nosel']);

}

$shelter = $row_do["shelter"];
$questio_n = htmlspecialchars_uni(strip_tags($row_do['question']));
$rejoin = htmlspecialchars_uni(strip_tags($row_do['rejoin']));

$update_new = md5($row_do["added"].$do_you_wanna.$row_do["added"]);

if ($rejoin == $update_new){




/// ������ '������' ������������ �� �������� ������
if ($SITE_Config["captcha"] == true){

include_once("include/functions_captcha.php");

$p_captcha = trim(isset($_POST["captcha"]) ? (string) $_POST["captcha"] : false);
$p_hash = trim(isset($_POST["hash"]) ? (string) $_POST["hash"] : false);

if (valid_captcha($p_captcha, $p_hash) == false){

$acaptcha = creating_captcha();

stdhead($tracker_lang['recover_secansv']);

echo "<form method=\"post\" action=\"recover.php\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"b\" colspan=\"2\">".$tracker_lang['captcha_repeat']."</td></tr>";

echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['captcha_confirm'].":</td><td class=\"a\" align=\"left\">

<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">

<tr><td class=\"b\" align=\"center\" style=\"margin: 0px; padding: 0px;\">
<div id=\"result\">".$acaptcha["echo"]."</div>
<input type=\"hidden\" name=\"hash\" value=\"".$acaptcha["hash"]."\" />
</td>
<td class=\"a\" width=\"50px\"><a href=\"#\" id=\"status\" class=\"button button-blue\"><span>".$tracker_lang['captcha_button']."</span></a></td>
</tr>

<tr><td class=\"a\" colspan=\"2\"><input type=\"text\" name=\"captcha\" size=\"20\" value=\"\" /> ".$tracker_lang['captcha_take']."</td></tr>

</table>
</td></tr>";

echo '<script type="text/javascript">
jQuery("#status").click(function() {
var onput = jQuery("#old").attr("value");
jQuery.post("md5.php",{"update": "true", "hash": "'.$acaptcha["hash"].'", "old": onput},
function(response) {jQuery("#result").fadeIn("slow"); jQuery("#result").html(response);}, "html");
});
</script>';


echo "<tr><td colspan=\"2\" align=\"center\">
<input type=\"hidden\" name=\"userid\" value=\"".$userid."\" /> <input type=\"hidden\" name=\"do_you_wanna\" value=\"".$do_you_wanna."\" /> 
<input type=\"submit\" name=\"post\" class=\"btn\" value=\"".$tracker_lang['selected']."\" />
</td></tr>";

echo "</table></form><br />";

stdfoot();
die;
}

}
/// ������ '������' ������������ �� �������� ������



$chpassword = generatePassword(); /// ���������� � ������� ����� ������ ������������
$sec = mksecret();
$passhash = md5($sec.$chpassword.$sec);

$updateset = array();
$updateset[] = "secret = ".sqlesc($sec);
$updateset[] = "passhash = ".sqlesc($passhash);
$usercomment = get_date_time()." - ����� ������ ����� ������. ������ (".$ip.").\n";
$updateset[] = "usercomment = CONCAT_WS('', ".sqlesc($usercomment).", usercomment)";

sql_query("UPDATE users SET ".implode(",", $updateset)." WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);

unsql_cache("arrid_".$userid);

logincookie($userid, $passhash, $shelter, 1); // �������� �� ����

$body = <<<EOD
�� ��� ���-�� ������, �������� ����� ������ � ��������: $row_do[username].

������ ��� ������ ��������� � IP �������: {$ip}.
����� ������ ��� ������������ (�������������): $chpassword

�������, ���������� �� ���� E-Mail, ��������� �� ��������� ������:
$DEFAULTBASEURL/userdetails.php?id=$userid

���� ��� ���� �� ��, ����������, ��������� �� ��������� ������:
$DEFAULTBASEURL/recover.php
� ������������ ������ � ������ ��������, ����� �� � ������������ ������� ������� ��������� ������ (���� ���� ����� ���������� �������)

����� ���� ��� �� ��� ��������, ���� ������������ �� ����� ����� �����������. ��� ������ ����������, ��� ��������� ������ ����� ��������� ������ ����������� � ������������� �����.
--
$SITENAME
EOD;

if (validemail($row_do["email"]) && !empty($SITEEMAIL))
@sent_mail($row_do["email"], $SITENAME, $SITEEMAIL, "������������ ������ �� ".$SITENAME, $body);
else {

sql_query("INSERT INTO messages (sender, receiver, added, msg, subject, poster) VALUES (0, ".sqlesc($userid).", ".sqlesc(get_date_time()).", ".sqlesc("������ ��� ������� ������������ ����� ��������� ������ � ����� ���������� �����, ���� ��� �� �� ��� ������ ������ �� ������ - ���������� �������� �� ���� ������������� � �������� ������ ��� ����� (��������� ������ � ������) ������.").", ".sqlesc("������ ��� ������� �������.").", 0)");

sql_query("UPDATE users SET unread = unread+1 WHERE id = ".sqlesc($userid));
}

stdhead($tracker_lang['recover']);

echo "<script type=\"text/javascript\">function highlight(field) {field.focus();field.select();}</script>
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">

<tr><td class=\"b\" colspan=\"2\">".$tracker_lang['recover_secansv']."</td></tr>

<tr><td class=\"a\"><b>".$tracker_lang['signup_username']."</b>: </td><td class=\"b\" align=\"left\"><b>".get_user_class_color($row_do["class"], $row_do["username"])."</b></td></tr>
<tr><td class=\"a\"><b>".$tracker_lang['new_password']."</b>: </td><td class=\"b\" align=\"left\">".$chpassword."</td></tr>

<tr><td colspan=\"2\" class=\"a\" align=\"center\"><h3><a href=\"userdetails.php?id=".$userid."\" title=\"".$tracker_lang['login']."\">".$tracker_lang['login']."</a></h3></td></tr>

</table><br />";

stdfoot();
die;

} else {

stdhead($tracker_lang['recover']);


echo "<form method=\"post\" action=\"recover.php\">";
echo "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"b\" colspan=\"2\">".$tracker_lang['validrecover']."</td></tr>";
echo "<tr><td class=\"a\" width=\"20%\" align=\"left\"><b>".$tracker_lang['sec_question']."</b>: </td><td class=\"a\"><b>".$questio_n."</b></td></tr>";
echo "<tr><td class=\"a\" width=\"20%\" align=\"left\"><b>".$tracker_lang['sec_answer']."</b>: </td><td class=\"a\"><input type=\"password\" size=\"40\" name=\"do_you_wanna\" /></td></tr>
";

/// ������ '������' ������������ �� �������� ������
if ($SITE_Config["captcha"] == true){

$acaptcha = creating_captcha();

//if ($p_hash <> false || $p_captcha <> false)
echo "<tr><td class=\"b\" colspan=\"2\" align=\"center\" style=\"font-weight: bold;\">".$tracker_lang['captcha_repeat']."</td></tr>";

echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['captcha_confirm'].":</td><td class=\"a\" align=\"left\">

<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">

<tr><td class=\"b\" align=\"center\" style=\"margin: 0px; padding: 0px;\">
<div id=\"result\">".$acaptcha["echo"]."</div>
<input type=\"hidden\" name=\"hash\" value=\"".$acaptcha["hash"]."\" />
</td>
<td class=\"a\" width=\"50px\"><a href=\"#\" id=\"status\" class=\"button button-blue\"><span>".$tracker_lang['captcha_button']."</span></a></td>
</tr>

<tr><td class=\"a\" colspan=\"2\"><input type=\"text\" name=\"captcha\" size=\"20\" value=\"\" /> ".$tracker_lang['captcha_take']."</td></tr>

</table>
</td></tr>";

echo '<script type="text/javascript">
jQuery("#status").click(function() {
var onput = jQuery("#old").attr("value");
jQuery.post("md5.php",{"update": "true", "hash": "'.$acaptcha["hash"].'", "old": onput},
function(response) {jQuery("#result").fadeIn("slow"); jQuery("#result").html(response);}, "html");
});
</script>';

}
/// ������ '������' ������������ �� �������� ������



echo "<tr><td colspan=\"2\" align=\"center\"><input type=\"hidden\" name=\"userid\" value=\"".$userid."\" /> <input type=\"submit\" name=\"post\" class=\"btn\" value=\"".$tracker_lang['selected']."\" /></td></tr>
</form>
</table><br />";

$update = array();

$update[] = "attempts = attempts + 1";
$update[] = "comment = CONCAT_WS('', ".sqlesc($userid.",").", comment)";

sql_query("UPDATE loginattempts SET ".implode(",", $update)." WHERE ip = ".sqlesc($ip)) or sqlerr(__FILE__, __LINE__);

if (!mysql_affected_rows())
sql_query("INSERT INTO loginattempts (ip, added, attempts, comment) VALUES (".sqlesc($ip).", ".sqlesc(get_date_time()).", 'yes', ".sqlesc($userid).")") or sqlerr(__FILE__, __LINE__);

stdfoot();
die;
}

}

} elseif (isset($_GET["secret"]) && isset($_GET["id"])) {


if ($SITE_Config["maxlogin"] == true && !$CURUSER)
failedloginscheck(); /// �������� ������� �� ip ������

$id = (!empty($_GET["id"]) ? (int) $_GET["id"]: false);
$md5 = (!empty($_GET["secret"]) ? (string) $_GET["secret"]: false);
$sha1 = (!empty($_GET["dybl_secret"]) ? (string) $_GET["dybl_secret"]: false);

$md5 = trim(preg_replace("#\s#is", "", $md5)); # ������� �������
$sha1 = trim(preg_replace("#\s#is", "", $sha1)); # ������� �������

if (empty($id) || $id == false || !is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": id");

if (empty($md5) || $md5 == false)
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": secret");

$res = sql_query("SELECT username, email, passhash, editsecret, status, enabled FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res);

$email = $arr["email"];

if (!validemail($email))
stderr($tracker_lang['error'], $tracker_lang['validemail']);

if ($arr["status"] == "pending" || $arr["enabled"] == "no")
stderr($tracker_lang['recover_email'], $tracker_lang['recover_nosel']);

$sec = hash_pad($arr["editsecret"]);
if (preg_match('/^ *$/s', $sec) || empty($arr["editsecret"]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": editsecret");

if ($md5 <> md5($sec.$email.$arr["passhash"].$sec)){
if ($sha1 <> sha1($sec.$email.$arr["passhash"]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": secret / md5");
}

$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; /// generate new password;

$newpassword = "";
for ($i = 0; $i < 10; $i++)
$newpassword .= $chars[mt_rand(0, strlen($chars) - 1)];

$sec = mksecret();
$newpasshash = md5($sec.$newpassword.$sec);
$usercomment = get_date_time()." - ����� ������ �� ����� (".$ip.").\n";

sql_query("UPDATE users SET secret = ".sqlesc($sec).", editsecret = '', passhash = ".sqlesc($newpasshash).", usercomment = CONCAT_WS('', ".sqlesc($usercomment).", usercomment) WHERE id = ".sqlesc($id)." AND editsecret = ".sqlesc($arr["editsecret"])) or sqlerr(__FILE__, __LINE__);

if (!mysql_affected_rows())
stderr($tracker_lang['error'], $tracker_lang['error_unknown']);

unsql_cache("arrid_".$id);

$body = <<<EOD
�� ������ ������� �� �������������� ������, �� ������������� ��� ����� ������.

������������: {$arr["username"]}
������:       $newpassword

�� ������ ����� �� ���� ���: $DEFAULTBASEURL/login.php

�������, ��������������� �� ���� E-Mail, ��������� �� ��������� ������:
$DEFAULTBASEURL/userdetails.php?id=$id

� ����� ������������ ������ ��������, ������� ���������� ��������� ������ � ����� � ����, ����� � ������ �����, �� ������ ��� ������� � ������ ������������ ��� ��� ������.
--
$SITENAME
EOD;

@sent_mail($email, $SITENAME, $SITEEMAIL, "������ �������� �� ".$SITENAME, $body) or stderr($tracker_lang['error'], $tracker_lang['enabled_sendmail']);

if (empty($SITEEMAIL)){
sql_query("INSERT INTO messages (sender, receiver, added, msg, subject, poster) VALUES (0, ".sqlesc($id).", ".sqlesc(get_date_time()).", ".sqlesc("������ ��� ������� ������������ ����� ����� (������������ ������ ��������� email), ���� ��� �� �� ��� ������ ������ �� ������ - ���������� �������� �� ���� �������������.").", ".sqlesc("������ ��� ������� �������.").", 0)");
sql_query("UPDATE users SET unread = unread+1 WHERE id = ".sqlesc($id));
}

stderr($tracker_lang['success'], $tracker_lang['recover_sendemail']);

} else {

stdhead($tracker_lang['recover']);

$uid = (isset($_COOKIE["uid"]) ? (int) $_COOKIE["uid"]: '');

echo "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td align=\"center\" colspan=\"2\" class=\"b\">".$tracker_lang['recover_info']."</td></tr>
</table><br />";


echo "<form method=\"post\" action=\"recover.php\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['recover_email']."</td></tr>
<tr><td class=\"a\">".$tracker_lang['email']."</td><td class=\"a\"><input type=\"text\" size=\"30\" name=\"email\" class=\"mail\"> <i>".sprintf($tracker_lang['max_n_limit'], 80)."</i></td></tr>
<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['recover_email']."\"></td></tr>
</form>
</table><br />";


echo "<form method=\"post\" action=\"recover.php\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['recover_secansv']."</td></tr>
<tr><td class=\"a\">".$tracker_lang['enter_login_or_empty']."</td><td class=\"a\"><input type=\"text\" size=\"20\" name=\"username_id\" /> <i>".sprintf($tracker_lang['max_n_limit'], 40)."</i></td></tr>
<tr><td class=\"a\">".$tracker_lang['enter_id_or_up']."</td><td class=\"a\"><input type=\"text\" size=\"10\" name=\"id_username\" value=\"".$uid."\" /></td></tr>
<tr><td class=\"b\" colspan=\"2\"><strong>".$tracker_lang['warning']."</strong>: ".$tracker_lang['first_main_second']."</td></tr>
<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['recover_secansv']."\" /></td></tr>
</form>
</table><br />";


echo "<form method=\"post\" action=\"recover.php\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['recover_reconfirm']."</td></tr>
<tr><td class=\"a\">".$tracker_lang['email']."</td><td class=\"a\"><input type=\"text\" size=\"30\" name=\"email\" class=\"mail\"> <i>".sprintf($tracker_lang['max_n_limit'], 80)."</i></td></tr>
<tr><td colspan=\"2\" align=\"center\"><input type=\"hidden\" name=\"act\" value=\"reconfirm\" /> <input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['recover_reconfirm']."\"></td></tr>
</form>
</table><br />";


stdfoot();

}

?>