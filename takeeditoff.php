<?
require_once("include/bittorrent.php");

dbconn();
loggedinorreturn();


if (isset($_POST['ful_id']) && isset($_POST['idi'])){

$idi = (!empty($_POST['idi']) ? intval($_POST['idi']):"");
$work = (!empty($_POST['ful_id']) ? intval($_POST['ful_id']):"");

if (!empty($idi) && !empty($work)) {

$res = sql_query("SELECT perform, owner, name FROM off_reqs WHERE id = ".sqlesc($idi)) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_assoc($res);
if (!$row)
stderr($tracker_lang['error'], $tracker_lang['offer_del_or_move']);

$res_u = sql_query("SELECT name FROM torrents WHERE id = ".sqlesc($work)) or sqlerr(__FILE__,__LINE__);
$row_u = mysql_fetch_assoc($res_u);
if (!$row_u)
stderr($tracker_lang['error'], $tracker_lang['torrent_del_or_move']);

sql_query("UPDATE off_reqs SET ful_id = ".sqlesc($work).", perform = 'yes', fulfilled = ".sqlesc($CURUSER["id"]).", data_perf = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($idi)) or sqlerr(__FILE__,__LINE__);

unsql_cache("multi_viewid_block-detailsoff");

if (!empty($row['owner']) && $row['owner'] <> $CURUSER['id']){

$subject = ("��� ������ / ����������� (#$idi) ��������.");
$msg = ("��� ������ / ����������� ��� ������� #$idi ([url=detailsoff.php?id=".$idi."]".$row['name']."[/url]) ��� �������� ������������� [url=userdetails.php?id=".$CURUSER["id"]."]".$CURUSER["username"]."[/url], �� ������ ������ �� �������: [url=details.php?id=".$work."]".$row_u['name']."[/url].");

sql_query("UPDATE users SET unread = unread+1 WHERE id = ".sqlesc($row['owner'])) or sqlerr(__FILE__,__LINE__);
unsql_cache("arrid_".$row['owner']);

sql_query("INSERT INTO messages (sender, receiver, added, msg, poster, subject) VALUES (0, ".sqlesc($row["owner"]).", ".sqlesc(get_date_time()).", ".sqlesc($msg).", 0, ".sqlesc($subject).")") or sqlerr(__FILE__,__LINE__);

}

unsql_cache("block_menu_off");
header("Location: ".$DEFAULTBASEURL."/detailsoff.php?id=".$idi);
die;

}

}

/// ��������������

if (!mkglobal("id:name:descr:type"))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": <strong>
".(empty($_POST["id"]) ? $tracker_lang['invalid_id']:"")."
".(empty($_POST["descr"]) ? $tracker_lang['description']:"")."
".(empty($_POST["type"]) ? $tracker_lang['sort_cat']:"")."
</strong>");

if (empty($id) || !is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

if (isset($_POST["reacon"])){

$res = sql_query("SELECT name, owner FROM off_reqs WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_assoc($res);
if (!$row)
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

if ($CURUSER["id"] <> $row["owner"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if (!empty($CURUSER["catedit"]) && !preg_match("/\[cat".$row["category"]."\]/is", $CURUSER["catedit"]) && ($CURUSER["id"] <> $row["owner"]) && get_user_class() == UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['no_modcat']);

$rt = (int) $_POST["reasontype"];
$dup = htmlspecialchars_uni($_POST["dup"]);
$rule = htmlspecialchars_uni($_POST["rule"]);

if (!is_int($rt) || $rt < 1 || $rt > 3)
stderr($tracker_lang['error'], $tracker_lang['noreason_act'].": ".$tracker_lang['reason']);

if ($rt == 1)
$reasonstr = "�������, ��������";
elseif ($rt == 2) {
if (!$dup)
stderr($tracker_lang['error'], $tracker_lang['noreason_act'].": ".$tracker_lang['torrent_dybl']);
$reasonstr = "��������".($dup ? (" ��: ".$dup) : "!");
} elseif ($rt == 3) {
if (!$rule) 
stderr($tracker_lang['error'], $tracker_lang['noreason_act'].": ".$tracker_lang['torrent_banrules']);
$reasonstr = "�������: ".$rule;
}

sql_query("DELETE FROM checkcomm WHERE checkid = ".sqlesc($id)." AND offer = '1'") or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM off_reqs WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM comments WHERE offer = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

write_log(($row["owner"] == $CURUSER["id"] ? "������":"")." ������ $id ($row[name]) ��� ������ $CURUSER[username]: $reasonstr\n","52A77C","torrent");

stderr($tracker_lang['success'], $tracker_lang['requests_section']." ".$tracker_lang['deleted'].": ".$row["name"]." <br />".$tracker_lang['reason'].": ".$reasonstr);
}
/// ����� �������� ��������


$res = sql_query("SELECT * FROM off_reqs WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_assoc($res);
if (!$row)
stderr($tracker_lang['error'], $tracker_lang['offer_del_or_move']);

if (get_user_class() == UC_SYSOP)
$torrent_com = $_POST["torrent_com"];
else
$torrent_com = $row["torrent_com"];

if ($CURUSER["id"] != $row["owner"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if (!empty($CURUSER["catedit"]) && !preg_match("/\[cat".$row["category"]."\]/is", $CURUSER["catedit"]) && ($CURUSER["id"] <> $row["owner"]) && get_user_class() == UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['no_modcat']);

$name = preg_replace("/\&/", "and", $_POST["name"]);
$name = htmlspecialchars($name);

if (empty($name))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['name']);

$updateset = array();

$formatup = (string) $_POST["formatdown"]; // mb gb

$numfiles = (isset($_POST["numfiles"]) ? intval($_POST["numfiles"]):"");

if (!empty($numfiles))
$updateset[] = "numfiles = ".sqlesc($numfiles);

$size = (isset($_POST["size"]) ? intval($_POST["size"]):"");

if (!empty($size))
$updateset[] = "size = ".sqlesc($formatup == 'mb' ? ($size * 1048576) : ($size * 1073741824));

$descr = htmlspecialchars_uni(addslashes($row["descr"]));
if (empty($descr))
stderr($tracker_lang['error'], $tracker_lang['no_upload_desc']);

if (isset($_POST["user_no_perf"])){
$updateset[] = "perform = 'no'";
$updateset[] = "fulfilled = ''";
$updateset[] = "ful_id = ''";
$torrent_com = get_date_time()." ".$CURUSER["username"]." ������� ����������.\n". $torrent_com;
}

$type = (isset($_POST["type"]) ? intval($_POST["type"]):"");
if (!empty($type))
$updateset[] = "category = ".sqlesc($type);

$updateset[] = "name = ".sqlesc($name);
$updateset[] = "descr = ".sqlesc($descr);

if (isset($_POST["up_date"]) && $_POST["up_date"] == "yes")
$updateset[] = "added = ".sqlesc(get_date_time()); /// ��������� ���� ���� ���������

if (get_user_class() >= UC_ADMINISTRATOR){

if (isset($_POST["delete_comment"]) && $_POST["delete_comment"] == 'yes'){

sql_query("DELETE FROM comments WHERE offer = ".sqlesc($id));
$torrent_com = get_date_time()." ".$CURUSER["username"]." ������ ��� ����������� �������.\n". $torrent_com;
}

/// ������� ����� ���������
if (isset($_POST["user_reliases_anonim"]) && $_POST["user_reliases_anonim"] == '1'){
$updateset[] = "owner = '0'";
$torrent_com = get_date_time()." ".$CURUSER["username"]." �������� ������� ������������.\n". $torrent_com;
}


/// ������� ����� ������� ������
if (isset($_POST["release_set_id"])){

$setid = (!empty($_POST["release_set_id"]) ? intval($_POST["release_set_id"]):"");

if (!empty($setid)){

$res = sql_query("SELECT username FROM users WHERE id = ".sqlesc($setid)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc($res);

$torrent_com = get_date_time()." ".$CURUSER["username"]." �������� ���������� - ".(!empty($row["username"]) ? $row["username"]:"id: [".$setid."]")." � �������.\n". $torrent_com;
$updateset[] = "owner = ".sqlesc($setid);

}

}

}


if (get_user_class() >= UC_MODERATOR && isset($_POST["torrent_com_zam"])) {
$torrent_com1 = htmlspecialchars($_POST["torrent_com_zam"]);
if (!empty($torrent_com1))
$torrent_com = get_date_time()." ������� �� ".$CURUSER["username"].": ".trim($torrent_com1)."\n". $torrent_com;
}

if ($row["descr"] <> $_POST["descr"] || $row["name"] <> $_POST["name"])
$torrent_com = get_date_time()." ��� �������������� ".$CURUSER["username"].".\n". $torrent_com;

$updateset[] = "torrent_com = ".sqlesc($torrent_com);

sql_query("UPDATE off_reqs SET ".implode(",", $updateset)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("multi_viewid_block-detailsoff");

header("Location: ".$DEFAULTBASEURL."/detailsoff.php?id=".$id);
die;

?>