<?
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();

if (get_user_class() < UC_ADMINISTRATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

$arrBrowser = array('Lynx' => 'Lynx', 'libwww-perl' => 'Lynx', 'ia_archiver' => $tracker_lang['my_website'], 'ArchitextSpider' => $tracker_lang['my_website'], 'Lycos_Spider_(T-Rex)' => $tracker_lang['my_website'], 'Scooter' => $tracker_lang['my_website'], 'InfoSeek' => $tracker_lang['my_website'], 'AltaVista' => $tracker_lang['my_website'], 'Eule-Robot' => $tracker_lang['my_website'], 'SwissSearch' => $tracker_lang['my_website'], 'Checkbot' => $tracker_lang['my_website'], 'Crescent Internet ToolPak' => $tracker_lang['my_website'], 'Slurp' => $tracker_lang['my_website'], 'WiseWire-Widow' => $tracker_lang['my_website'], 'NetAttache' => $tracker_lang['my_website'], 'Web21 CustomCrawl' => $tracker_lang['my_website'], 'BTWebClient' => 'uTorrent RSS', 'CheckUrl' => $tracker_lang['my_website'], 'LinkLint-checkonly' => $tracker_lang['my_website'], 'Namecrawler' => $tracker_lang['my_website'], 'ZyBorg' => $tracker_lang['my_website'], 'Googlebot' => 'Google Bot', 'Yandex' => 'Yandex Bot', 'WebCrawler' => $tracker_lang['my_website'], 'WebCopier' => $tracker_lang['my_website'], 'JBH Agent 2.0' => $tracker_lang['my_website']);

$arrbot = array('Apple-PubSub' => 'MacOS RSS', 'BTWebClient' => 'uTorrent (web) RSS', 'BitTorrent' => 'uTorrent (BitTorrent) RSS', 'Transmission' => 'Transmission', 'uTorrent' => 'uTorrent RSS', 'BTSP' => 'uTorrent (BTSP) RSS', 'Googlebot' => 'Google Bot', 'Yandex' => 'Yandex Bot', 'Yahoo' => 'Yahoo Bot', 'search.msn.com/msnbot.htm' => 'MSN Bot', 'Rambler' => 'Rambler Bot', 'Mail.Ru/1.0' => 'Mail.Ru', 'bot@linguee.com' => 'Linguee Bot', 'Purebot/1.1' => 'Puritysearch.net', 'SetLinks' => 'SetLinks Bot', 'IEMB3; mlbot' => 'MainLink Bot', 'mlbot' => 'MainLink Bot', 'DotBot/1.1' => 'Dotnetdotcom.org', 'Feedfetcher-Google' => 'Google Feed', 'Subscribe.Ru/1.0' => 'Subscribe.Ru', 'Nigma.ru/3.0' => 'Nigma Bot', 'lmspider/Nutch' => 'Nuance.com', 'MJ12bot/v' => 'Majestic12.co.uk', 'Sogou web spider/4.' => 'Sogou.com', 'FeedDemon/3.0' => 'Feeddemon.com', 'AhrefsBot/1.0' => 'Ahrefs.com', 'ahrefs.com/robot/' => 'Ahrefs.com', 'AdsBot-Google' => 'AdsBot-Google', 'AdsBot-Google-Mobile' => 'AdsBot-Google-Mobile', 'Twiceler-0.' => 'Cuil.com', 'R6_FeedFetcher' => 'Radian6.com', 'AportWorm' => 'Aport.ru', 'bingbot/2.0' => 'Bing.com', 'magpie-crawler' => 'Brandwatch.net', 'Mail.RU_Bot' => 'Mail.RU', 'rogerbot' => 'seomoz.org (SEO)', 'Android' => 'Android', 'Mikrotik' => 'Mikrotik');

$arrSystem = array('Windows 3.1' => 'Windows 3.1', 'Win16' => 'Windows 3.1', '16bit' => 'Windows 3.1', 'Win32' => 'Windows 95', '32bit' => 'Windows 95', 'Win 32' => 'Windows 95', 'Win95' => 'Windows 95', 'Windows 95/NT' => 'Windows 95', 'Win98' => 'Windows 98', 'Windows 95' => 'Windows 95', 'Windows 98' => 'Windows 98', 'Windows NT 5.0' => 'Windows 2000', 'Windows NT 5.1' => 'Windows XP', 'Windows NT 5.2' => 'Windows XP (64 bit)', 'Windows NT 6.0' => 'Windows Vista', 'WinVI' => 'Windows Vista', 'Windows NT 6.1' => 'Windows Seven', 'Windows NT' => 'Windows NT', 'WinNT' => 'Windows NT', 'Windows ME' => 'Windows ME', 'Windows CE' => 'Windows CE', 'Windows' => 'Windows 95', 'Mac_68000' => 'Macintosh', 'Mac_PowerPC' => 'Macintosh', 'Mac_68K' => 'Macintosh', 'Mac_PPC' => 'Macintosh', 'Macintosh' => 'Macintosh', 'IRIX' => 'Unix', 'SunOS' => 'Unix', 'AIX' => 'Unix', 'Linux' => 'Unix', 'HP-UX' => 'Unix', 'SCO_SV' => 'Unix', 'FreeBSD' => 'Unix', 'BSD/OS' => 'Unix', 'OS/2' => 'OS/2', 'WebTV/1.0' => 'WebTV/1.0', 'WebTV/1.2' => 'WebTV/1.2', 'iMac' => 'iMac', 'Mikrotik' => 'Mikrotik');


function getbot($arrbot,$botagent) {

global $tracker_lang;

$default = $tracker_lang['guest'];

foreach($arrbot as $key => $value) {
if (strpos($botagent, $key) !== false) {
$default = $value;
break;
}
}

$botagent['default'] = $default;

if ($default == $tracker_lang['guest'])
$default = "<b>".$default."</b>";
else
$default = "<u>".$default."</u>";

return $default;
}

function getSystem($arrSystem,$userAgent) {

global $tracker_lang;

$system = $tracker_lang['unknown'];
foreach($arrSystem as $key => $value) {
if (strpos($userAgent, $key) !== false) {
$system = $value;
break;
}
}
return $system;
}

function getBrowser($arrBrowser,$userAgent) {

global $tracker_lang;

$version = "";
$browser = $tracker_lang['unknown'];
if (($pos = strpos($userAgent, 'Opera')) !== false) {
$browser = 'Opera';
$pos += 6;
if ((($posEnd = strpos($userAgent, ';', $pos)) !== false) || (($posEnd = strpos($userAgent, ' ', $pos)) !== false))
$version = trim(substr($userAgent, $pos, $posEnd - $pos));
} elseif (($pos = strpos($userAgent, 'MSIE')) !== false) {
$browser = 'Internet Explorer';
$posEnd = strpos($userAgent, ';', $pos);
if ($posEnd !== false) {
$pos += 4;
$version = trim(substr($userAgent, $pos, $posEnd - $pos));
}
} elseif (((strpos($userAgent, 'Gecko')) !== false) && ((strpos($userAgent, 'Netscape')) === false)) {
$browser = 'Mozilla';
if (($pos = strpos($userAgent, 'rv:')) !== false) {
$posEnd = strpos($userAgent, ')', $pos);
if ($posEnd !== false) {
$pos += 3;
$version = trim(substr($userAgent, $pos, $posEnd - $pos));
}
}
} elseif ((strpos($userAgent, ' I;') !== false) || (strpos($userAgent, ' U;') !== false) || (strpos($userAgent, ' U ;') !== false) || (strpos($userAgent, ' I)') !== false) || (strpos($userAgent, ' U)') !== false)){
$browser = 'Netscape Navigator';
if (($pos = strpos($userAgent, 'Netscape6')) !== false) {
$pos += 10;
$version = trim(substr($userAgent, $pos, strlen($userAgent) - $pos));
} else {
if (($pos = strpos($userAgent, 'Mozilla/')) !== false) {
if (($posEnd = strpos($userAgent, ' ', $pos)) !== false) {
$pos += 8;
$version = trim(substr($userAgent, $pos, $posEnd - $pos));
}
}
}
} else {
foreach($arrBrowser as $key => $value) {
if (strpos($userAgent, $key) !== false) {
$browser = $value;
break;
}
}
}
$userAgentArr['browser'] = $browser;
$userAgentArr['version'] = $version;
return $userAgentArr;
}

$where = $paglist = array();

$ip = ip2long(isset($_GET["ip"]) ? trim($_GET["ip"]) : '');
$uid = intval(isset($_GET["uid"]) ? trim($_GET["uid"]) : 0);

$only = ((isset($_GET["users"]) && $_GET["users"] == 'yes') ? 'yes' : 'no');

if (is_numeric($ip)) {
$paglist[] = "ip=".long2ip($ip);
$where[] = "s.ip = ".sqlesc(long2ip($ip));
}
if (!empty($uid) || $uid == '-1') {
$paglist[] = "uid=".$uid;
$where[] = "s.uid = ".sqlesc($uid);
}
if (!empty($only) && $only == 'yes') {
$paglist[] = "users=yes";
$where[] = "s.uid > '-1'";
}


stdhead($tracker_lang['tabs_session']);

if (count($where)){
$count = get_row_count("sessions AS s", "WHERE ".implode(" AND ", $where));
} else
$count = get_row_count("sessions");


list ($pagertop, $pagerbottom, $limit) = pager(100, $count, (count($paglist) ? "0nline.php?".implode('&', $paglist)."&":"0nline.php?"));


if (count($where))
$res = sql_query("SELECT s.url, s.host, s.sid, s.uid ,s.username, s.class, s.ip, s.useragent, s.time FROM sessions AS s ".(count($where) ? "WHERE ".implode(" AND ", $where):"")." ORDER BY s.time ".$limit) or sqlerr(__FILE__, __LINE__);
else
$res = sql_query("SELECT s.url, s.host, s.sid, s.uid ,s.username, s.class, s.ip, s.useragent, s.time, 
(SELECT COUNT(*) FROM sessions WHERE useragent=s.useragent AND ip=s.ip) AS col
FROM sessions AS s ".(count($where) ? "WHERE ".implode(" AND ", $where):"")."
ORDER BY s.time ".$limit) or sqlerr(__FILE__, __LINE__);



echo "<table class=\"embedded\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">

<tr><td class=\"colhead\" align=\"left\" colspan=\"4\">".$tracker_lang['tabs_session']."</td></tr>";



echo "<tr><td class=\"b\" colspan=\"3\" ".(count($where) ? "align=\"center\"":"").">";
echo "<form method=\"get\" action=\"0nline.php\">".$tracker_lang['user_ip'].": <input title=\"".$tracker_lang['user_ip']."\" type=\"text\" value=\"".(is_numeric($ip) ? long2ip($ip) : '')."\" name=\"ip\" size=\"15\" /> ".$tracker_lang['or']." ".$tracker_lang['account_id'].": <input type=\"text\" value=\"".(!empty($uid) ? $uid : '')."\" name=\"uid\" size=\"15\" /> <input title=\"".$tracker_lang['users']."\" type=\"checkbox\" name=\"users\" ".($only == 'yes' ? "checked":"")." value=\"yes\" /> <input type=\"submit\" value=\"".$tracker_lang['fast_search']."\" class=\"btn\" /></form>";

if (count($where))
echo "<form method=\"get\" action=\"0nline.php\"><input type=\"submit\" value=\"".$tracker_lang['admincp_back']."\" class=\"btn\" /></form>";
echo "</td></tr>";



echo "<tr><td align=\"center\" colspan=\"4\">".$pagertop."</td></tr>

<tr>
<td class=\"a\" align=\"center\">".$tracker_lang['username']." / ".$tracker_lang['description']."</td>
<td class=\"a\" width=\"25%\" align=\"center\">".$tracker_lang['clock']."</td>
<td class=\"a\" width=\"25%\" align=\"center\">".$tracker_lang['page']." / ".$tracker_lang['domen']."</td>
</tr>";

if (empty($count))
echo "<tr><td class=\"index\" colspan=\"3\">".$tracker_lang['nothing_found']."</td></tr>\n";


$i = 0;

while ($row = mysql_fetch_array($res)) {


$res_list = explode(".php", basename($row["url"]));
if (stristr($row["url"], "redir.php") && substr($row["url"], 0,  10) == '/redir.php')
$res_list[0] = 'redir';

$brawser = getBrowser($arrBrowser, $row["useragent"]);
$bot = getbot($arrbot, $row["useragent"]);

if (empty($row["ip"])) continue;

$slep = "<div class=\"spoiler-wrap\" id=\"".md5($row["sid"])."\"><div class=\"spoiler-head folded clickable\">".$tracker_lang['more']."</div>
<div class=\"spoiler-body\" style=\"display: none;\">
<b>".$tracker_lang['class']."</b>: ".($row["class"] <>-1 ? "<a title=\"".$row["ip"]."\"><b><font color=\"#".get_user_rgbcolor($row["class"], $row["username"])."\">".get_user_class_name($row["class"])."</b></font></a>":$bot)."<br />
<b>OS</b>: ".getSystem($arrSystem, $row["useragent"])."<br />
<b>".$tracker_lang['user_ip']."</b>: <a target='_blank' href=\"".$DEFAULTBASEURL."/usersearch.php?ip=".$row["ip"]."\">". $row["ip"]."</a><br />
<b>Session</b>: ".$row["sid"]."<br />
<b>".$tracker_lang['tabs_agent']."</b>: <ins>".$brawser['browser']."</ins>, ".format_urls($row["useragent"])."<br />
<b>".$tracker_lang['page']."</b>: ".($row["url"])."
</div>
</div>";

if ($row["class"] <> -1)
echo "<tr><td ".($i%2==0 ? "class=\"b\"":"class=\"a\"")."><a target='_blank' href=\"userdetails.php?id=".$row["uid"]."\">".get_user_class_color($row["class"], $row["username"])."</a>".$slep."</td>";
else
echo "<tr><td ".($i%2==0 ? "class=\"b\"":"class=\"a\"")."><a href=\"".$DEFAULTBASEURL."/0nline.php?uid=-1\">".$bot."</a>,  <a href=\"0nline.php?ip=".$row["ip"]."\">".$row["ip"]."</a> ".(!empty($row["col"]) ? "(".$row["col"].") ":"")."".$slep."</td>";

echo "<td ".($i%2==0 ? "class=\"a\"":"class=\"b\"")." align=\"center\">".($row["time"])."</td>";

$row["host"] = str_replace("www.muz-tracker.net", "<font color=\"blue\">www.muz-tracker.net</font>", $row["host"]);
$row["host"] = str_replace("localhost", "<font color=\"red\">localhost</font>", $row["host"]);

echo "<td width=\"25%\" ".($i%2==0 ? "class=\"b\"":"class=\"a\"").">";
echo "<a target='_blank' href=\"".$row["url"]."\">".($res_list[0].".php")."</a> ".($CURUSER['id'] == $row["uid"] ? "[<font color=\"blue\">".$tracker_lang['now']."</font>]":"")."<br /><div align=\"right\">".$row["host"]."</div>";
echo "</td>";

echo "</tr>";

++$i;
}

echo "<tr><td align=\"center\" colspan=\"4\">".$pagerbottom."</td></tr>";

echo "</table>";

stdfoot();

?>