<?
require_once("include/bittorrent.php");
dbconn();

$id = (isset($_GET["id"]) ? $_GET["id"] : false);
$md5 = (string) $_GET["secret"];

if (!is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$id);

$res = sql_query("SELECT passhash, editsecret, status, shelter FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['user_is_noexis']);

$row = mysql_fetch_assoc($res);

if ($row["status"] <> "pending") {
header("Location: ok.php?type=confirmed");
exit($tracker_lang['activated']);
}

$sec = hash_pad($row["editsecret"]);
$shelter = $row["shelter"];

if ($md5 <> md5($sec))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": editsecret");

sql_query("UPDATE users SET status = 'confirmed', editsecret = '' WHERE id = ".sqlesc($id)." AND status = 'pending'") or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$id);
logincookie($id, $row["passhash"], $shelter);

header("Location: ok.php?type=confirm");
die;
?>