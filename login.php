<?
require_once("include/bittorrent.php");
dbconn(false);

if ($CURUSER) {
if (!headers_sent())
@header("Location: ".$DEFAULTBASEURL);
else
die("<script>setTimeout('document.location.href=\"".$DEFAULTBASEURL."\"', 10);</script>");
}

stdhead($tracker_lang['takelogin']);

global $SITE_Config;

if ($SITE_Config["maxlogin"] == true)
failedloginscheck();

echo "<table border=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<form method=\"post\" action=\"takelogin.php\">";

echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"><fieldset class=\"fieldset\"><legend><b>".$tracker_lang['warning']."</b></legend><h3>".$tracker_lang['signup_use_cookies']."</h3></fieldset></td></tr>";

if (!empty($_GET["returnto"])) {
$returnto = htmlentities($_GET["returnto"]);
$site_own = (($_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://").htmlspecialchars_uni($_SERVER['HTTP_HOST']);

if (!isset($_GET["nowarn"]))
echo "<tr><td align=\"center\" colspan=\"2\" class=\"b\"><div class=\"error2\">".$tracker_lang['no_inlogin']."<br />
<input type=\"text\" size=\"100\" name=\"fJf\" value=\"".$site_own."/".$returnto."\" readonly style=\"width: 80%;\"/>
</div></td></tr>";
}


if ($SITE_Config["captcha"] == true){

include_once("include/functions_captcha.php");
$acaptcha = creating_captcha();

echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['captcha_confirm'].":</td><td class=\"a\" align=\"left\">

<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">

<tr><td class=\"b\" align=\"center\" style=\"margin: 0px; padding: 0px;\">
<div id=\"result\">".$acaptcha["echo"]."</div>
<input type=\"hidden\" name=\"hash\" value=\"".$acaptcha["hash"]."\" />
</td>
<td class=\"a\" width=\"50px\"><a href=\"#\" id=\"status\" class=\"button button-blue\"><span>".$tracker_lang['captcha_button']."</span></a></td>
</tr>

<tr><td class=\"a\" colspan=\"2\"><input type=\"text\" name=\"captcha\" size=\"20\" value=\"\" /> ".$tracker_lang['captcha_take']."</td></tr>

</table>
</td></tr>";


echo '<script type="text/javascript">
jQuery("#status").click(function() {
var onput = jQuery("#old").attr("value");
jQuery.post("md5.php",{"update": "true", "hash": "'.$acaptcha["hash"].'", "old": onput},
function(response) {jQuery("#result").fadeIn("slow"); jQuery("#result").html(response);}, "html");
});
</script>';
}


echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['signup_username'].":</td><td class=\"a\" align=\"left\"><input type=\"text\" size=\"50\" name=\"username\" style=\"width: 200px; border: 1px solid green\" class=\"login\" onclick=\"if (this.value == '".$tracker_lang['username']."') this.value = '';\" onblur=\"if (this.value == '') this.value = '".$tracker_lang['username']."';\"/> <i>".sprintf($tracker_lang['max_simp_of'], 40)."</i></td></tr>";

echo "<tr><td class=\"b\" style=\"font-weight: bold;\">".$tracker_lang['signup_password'].":</td><td class=\"a\" align=\"left\"><input type=\"password\" size=\"50\" name=\"password\" style=\"width: 200px; border: 1px solid green\" class=\"pass\"/></td></tr>";

if (isset($returnto))
echo("<input type=\"hidden\" name=\"returnto\" value=\"".$returnto."\" />\n");

echo "<tr><td class=\"b\" colspan=\"2\" align=\"center\"><input type=\"submit\" class=\"btn\" style=\"width: 200px;\" value=\"".$tracker_lang['enter_me']."\"/></td></tr>";
echo "</form>";

echo "</table>";
echo "<br />";

stdfoot();

?>