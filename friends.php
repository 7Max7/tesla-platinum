<?
require "include/bittorrent.php";
dbconn(false);

loggedinorreturn();

$userid = (empty($_GET['id']) ? 0: (int) $_GET['id']);
$action = (string) $_GET['action'];

if (empty($userid))
$userid = $CURUSER['id'];

if (!is_valid_id($userid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if ($userid <> $CURUSER["id"])
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$type = (empty($_GET['type']) ? "":(string) $_GET['type']);
$targetid = (empty($_GET['targetid']) ? 0: (int) $_GET['targetid']);

if (!is_valid_id($targetid) && in_array($action, array("add", "delete")))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);


$res = sql_query("SELECT * FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
$user = mysql_fetch_array($res) or stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$dt = get_date_time(gmtime() - 300);


$lnk = false;
if ($action == 'add' && in_array($type, array("friend", "block"))) {

if ($type == 'friend')
$blocks = 0;
else
$blocks = 1;

$r = sql_query("SELECT id FROM friends WHERE userid = ".sqlesc($userid)." AND friendid = ".sqlesc($targetid)." AND blocks = ".sqlesc($blocks)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($r) == 0)
sql_query("INSERT INTO friends VALUES (0,".sqlesc($userid).", ".sqlesc($targetid).", ".sqlesc($blocks).")") or sqlerr(__FILE__, __LINE__);

$lnk = true;

} elseif ($action == 'delete' && in_array($type, array("friend", "block"))) {

if ($type == 'friend'){

sql_query("DELETE FROM friends WHERE userid = ".sqlesc($userid)." AND friendid = ".sqlesc($targetid)." AND blocks = '0'") or sqlerr(__FILE__, __LINE__);

if (mysql_affected_rows() == 0)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

} else {

sql_query("DELETE FROM friends WHERE userid = ".sqlesc($userid)." AND friendid = ".sqlesc($targetid)." AND blocks = '1'") or sqlerr(__FILE__, __LINE__);

if (mysql_affected_rows() == 0)
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

}

$lnk = true;

}

if ($lnk == true){
$returlink = $_SERVER["HTTP_REFERER"];
$site = parse_url($returlink, PHP_URL_HOST);
if (!empty($site))
header("Location: ".$returlink);
else
header("Location: friends.php");

die;
}

stdhead($tracker_lang['friends_list']." / ".$tracker_lang['blocked_list']);

$array[] = array();

function view_friends($res, $userid, $text, $del = 0){

global $tracker_lang, $array;

echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
echo "<tr><td class=\"colhead\">".$text."</td></tr>";

if (mysql_num_rows($res) == 0)
echo "<tr><td class=\"b\"><em>".$tracker_lang['no_data_now'].".</em></td></tr>";

else {

$i = $i2 = 0;

echo "<tr><td class=\"a\">";
echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";

while ($block = mysql_fetch_array($res)) {

if ($i2%2==1){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

if ($i % 6 == 0)
echo "<tr>";

echo "<td ".$cl1.">".($block['last_access'] > get_date_time(gmtime() - 300) ? "<img src=\"./pic/button_online.gif\" border=\"0\" alt=\"".$tracker_lang['online']."\" />":"<img src=\"./pic/button_offline.gif\" border=\"0\" alt=\"".$tracker_lang['offline']."\" />")." <a href=\"userdetails.php?id=".$block['id']."\"><b>".get_user_class_color($block['class'], $block['name'])."</b></a> ".get_user_icons($block);


if (!empty($del))
echo " <a href=\"message.php?action=sendmessage&receiver=".$block['id']."\"><img src=\"./pic/button_pm.gif\" border=\"0\" alt=\"".$tracker_lang['sendmessage']."\"></a>";

if ($del == 1)
echo "<br />[<a title=\"".$tracker_lang['remove_from_blacklist']."\" href=\"friends.php?id=".$userid."&action=delete&type=block&targetid=".$block['id']."\">".$tracker_lang['remove_from_blacklist']."</a>]";
elseif ($del == 2){
echo "<br />[<a title=\"".$tracker_lang['remove_from_friends']."\" href=\"friends.php?id=".$userid."&action=delete&type=friend&targetid=".$block['id']."\">".$tracker_lang['remove_from_friends']."</a>]";
$array[] = $block['id'];
} elseif ($del == 3 && !in_array($block["id"], $array))
echo "<br />[<a title=\"".$tracker_lang['add_my_friends']."\" href=\"friends.php?action=add&type=friend&targetid=".$block['id']."\">".$tracker_lang['add_my_friends']."</a>]";

echo "</td>";

if ($i % 6 == 5){
echo "</tr>";
--$i2;
}

++$i2;
++$i;
}

echo "</table>";
echo "</td></tr>";

}

echo "</table><br />";

}

/// ������ ������
$res = sql_query("SELECT f.friendid AS id, u.username AS name, u.class, u.donor, u.warned, u.enabled, u.last_access 
FROM friends AS f 
LEFT JOIN users as u ON f.friendid = u.id WHERE userid = ".sqlesc($userid)." AND blocks = '0' ORDER BY name") or sqlerr(__FILE__, __LINE__);
view_friends ($res, $userid, $tracker_lang['friends_list'], 2);

/// ������ ������
$res = sql_query("SELECT f.friendid AS id, u.username AS name, u.class, u.donor, u.warned, u.enabled, u.last_access 
FROM friends AS f 
LEFT JOIN users as u ON f.friendid = u.id 
WHERE userid = ".sqlesc($userid)." AND blocks = '1' ORDER BY name") or sqlerr(__FILE__, __LINE__);
view_friends ($res, $userid, $tracker_lang['blocked_list'], 1);

if (get_user_class() == UC_VIP || get_user_class() > UC_MODERATOR){
/// ������ � ���� � �������
$res = sql_query("SELECT f.userid AS id, u.username AS name, u.class, u.donor, u.warned, u.enabled, u.last_access 
FROM friends AS f 
LEFT JOIN users as u ON f.userid = u.id WHERE friendid = ".sqlesc($userid)." AND blocks ='0' ORDER BY name") or sqlerr(__FILE__, __LINE__);
view_friends ($res, $userid, $tracker_lang['ifriends_list'], 3);
}

if (get_user_class() > UC_MODERATOR){
/// �� � ������ ������
$res = sql_query("SELECT f.userid AS id, u.username AS name, u.class, u.donor, u.warned, u.enabled, u.last_access 
FROM friends AS f 
LEFT JOIN users as u ON f.userid = u.id WHERE friendid = ".sqlesc($userid)." AND blocks = '1' ORDER BY name") or sqlerr(__FILE__, __LINE__);
view_friends ($res, $userid, $tracker_lang['iblocked_list']);

}

stdfoot();
?>