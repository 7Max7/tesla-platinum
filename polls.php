<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

$action = (string) $_GET["action"];

$pollid = (int) $_GET["pollid"];
$returnto = htmlspecialchars($_GET["returnto"]);



if ($action == "delete" && get_user_class() > UC_MODERATOR) {

if (!is_valid_id($pollid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if (!isset($_GET["sure"]))
stderr($tracker_lang['delete'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $tracker_lang['polls'], "polls.php?action=delete&pollid=".$pollid."&returnto=".$returnto."&sure=1"));

$res2 = sql_query("SELECT * FROM polls WHERE forum = '0' AND id = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);
$ip2 = mysql_fetch_array($res2);
$question = format_comment($ip2["question"]);

sql_query("DELETE FROM pollanswers WHERE pollid = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM polls WHERE forum = '0' AND id = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM comments WHERE torrent = '0' AND news = '0' AND offer = '0' AND poll = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);

unsql_cache("poll_comment_".$pollid); // �����������
unsql_cache("polls_".$pollid); // ������ ������
unsql_cache("poll_users_".$pollid); // ������ ������

write_log($CURUSER["username"]." ������ ����� ".$question." (# ".$pollid.")", "FF8787", "other");

if ($returnto == "main")
header("Location: ".$DEFAULTBASEURL);
else
header("Location: ".$DEFAULTBASEURL."/polls.php");

die;
}


$rows = sql_query("SELECT COUNT(*) FROM polls WHERE forum = '0'") or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_row($rows);
$pollcount = $row[0];

if ($pollcount == 0)
stderr($tracker_lang['sorry'], $tracker_lang['no_polls']);

$polls = sql_query("SELECT * FROM polls WHERE forum = '0' ORDER BY id DESC LIMIT 1,".($pollcount-1)) or sqlerr(__FILE__, __LINE__);
stdhead($tracker_lang['polls_olders']);

print ("<h1>".$tracker_lang['polls_olders']."</h1>");

function srt($a,$b){
if ($a[0] > $b[0]) return -1;
if ($a[0] < $b[0]) return 1;
return 0;
}

while ($poll = mysql_fetch_assoc($polls)) {

$o = array(format_comment($poll["option0"]), format_comment($poll["option1"]), format_comment($poll["option2"]), format_comment($poll["option3"]), format_comment($poll["option4"]), format_comment($poll["option5"]), format_comment($poll["option6"]), format_comment($poll["option7"]), format_comment($poll["option8"]), format_comment($poll["option9"]), format_comment($poll["option10"]), format_comment($poll["option11"]), format_comment($poll["option12"]), format_comment($poll["option13"]), format_comment($poll["option14"]), format_comment($poll["option15"]), format_comment($poll["option16"]), format_comment($poll["option17"]), format_comment($poll["option18"]), format_comment($poll["option19"]));

echo ("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\">");

echo "<tr><td align=\"left\" class=\"colhead\">";
echo date("Y-m-d", strtotime($poll['added']))." (".(get_elapsed_time(sql_timestamp_to_unix_timestamp($poll["added"])))." ".$tracker_lang['ago'].")";
if (get_user_class() >= UC_ADMINISTRATOR)
echo(" <a class=\"altlink_white\" href=\"makepoll.php?action=edit&pollid=".$poll["id"]."\">".$tracker_lang['edit']."</a> - <a class=\"altlink_white\" href=\"polls.php?action=delete&pollid=".$poll["id"]."\">".$tracker_lang['delete']."</a>");
echo "</td></tr>";

echo "<tr><td align=\"center\" class=\"b\">";

echo ("<table class=\"main\" width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">");

echo "<tr><td class=\"b\">";
echo ("<b>".format_comment($poll["question"])."</b>");
echo "</td></tr>";

echo "<tr><td class=\"b\">";
$pollanswers = sql_query("SELECT selection FROM pollanswers WHERE pollid = ".sqlesc($poll["id"])." AND selection < 20") or sqlerr(__FILE__, __LINE__);

$tvotes = mysql_num_rows($pollanswers);

$vs = $os = array();

while ($pollanswer = mysql_fetch_row($pollanswers))
$vs[$pollanswer[0]] += 1;

reset($o);

for ($i = 0; $i < count($o); ++$i)
if ($o[$i])
$os[$i] = array($vs[$i], $o[$i]);

if ($poll["sort"] == "yes")
usort($os, srt);

echo ("<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");

$i = 0;
while ($a = $os[$i]) {
if ($tvotes > 0)
$p = round($a[0] / $tvotes * 100);
else
$p = 0;

if ($i % 2) $c = "b";
else $c = "a";

echo ("<tr><td class=\"".$c."\">".$a[1]."</td><td class=\"".($c == "a" ? "b":"a")."\"><img src=\"./themes/".$CURUSER["stylesheet"]."/images/bar_left.gif\"><img src=\"./themes/".$CURUSER["stylesheet"]."/images/bar.gif\" height=\"12\" width=\"".($p*3)."\"><img src=\"./themes/".$CURUSER["stylesheet"]."/images/bar_right.gif\"> ".$p."%</td></tr>");
++$i;
}

echo ("</table>");

echo ("<p align=center><b>".$tracker_lang['votes']."</b>: ".number_format($tvotes)."</p>");

echo ("</td></tr></table>");

echo ("</td></tr></table></p>");

}

stdfoot();
?>