<?php
if(!defined('IN_TRACKER') && !defined('IN_ANNOUNCE'))
die('Direct access denied. Benc'); 

ini_set ('memory_limit', '128M');

function benc($obj){
    if (!is_array($obj) || !isset($obj["type"]) || !isset($obj["value"]))
        return;
    $c = $obj["value"];
    switch ($obj["type"]) {
        case "string":
            return benc_str($c);
        case "integer":
            return benc_int($c);
        case "list":
            return benc_list($c);
        case "dictionary":
            return benc_dict($c);
        default:
            return;
    }
}

function benc_str($s){
    return strlen($s) . ":$s";
}

function benc_int($i) {
    return "i" . $i . "e";
}

function benc_list($a){
    $s = "l";
    foreach ($a as $e) {
        $s .= benc($e);
    }
    $s .= "e";
    return $s;
}

function benc_dict($d){
    $s = "d";
    $keys = array_keys($d);
    sort($keys);
    foreach ($keys as $k) {
        $v = $d[$k];
        $s .= benc_str($k);
        $s .= benc($v);
    }
    $s .= "e";
    return $s;
}

function bdec_file($f, $ms = false){

    $fsize = filesize($f);
    if ($ms > $fsize || empty($ms))
        $ms = $fsize;

    $fp = fopen($f, "rb");
    if (!$fp) return;
    $e = fread($fp, $ms);
    fclose($fp);

    return bdec($e);
}

function bdec($s){
    if (preg_match('/^(\d+):/', $s, $m)) {
        $l = $m[1]; unset($m);
        $pl = strlen($l) + 1;
        $v = substr($s, $pl, $l);
        $ss = substr($s, 0, $pl + $l);
        if (strlen($v) <> $l)
            return;
        return array(
            'type' => "string",
            'value' => $v,
            'strlen' => strlen($ss),
            'string' => $ss);
    }
    if (preg_match('/^i(-?\d+)e/', $s, $m)) {
        $v = $m[1]; unset($m);
        $ss = "i" . $v . "e";
        if ($v === "-0")
            return;
        if ($v[0] == "0" && strlen($v) <> 1)
            return;
        return array(
            'type' => "integer",
            'value' => $v,
            'strlen' => strlen($ss),
            'string' => $ss);
    }
    switch ($s[0]) {
        case "l":
            return bdec_list($s);
        case "d":
            return bdec_dict($s);
        default:
            return;
    }
}

function bdec_list($s){
    if ($s[0] <> "l")
        return;
    $sl = strlen($s);
    $i = 1;
    $v = array();
    $ss = "l";
    for (;; ) {
        if ($i >= $sl)
            return;
        if ($s[$i] == "e")
            break;
        $ret = bdec(substr($s, $i));
        if (!isset($ret) || !is_array($ret))
            return;
        $v[] = $ret;
        $i += $ret["strlen"];
        $ss .= $ret["string"];
    } unset($ret);
    
    $ss .= "e";
    return array(
        'type' => "list",
        'value' => $v,
        'strlen' => strlen($ss),
        'string' => $ss);
}

function bdec_dict($s){
    if ($s[0] <> "d")
        return;
    $sl = strlen($s);
    $i = 1;
    $v = array();
    $ss = "d";
    for (;; ) {
        if ($i >= $sl)
            return;
        if ($s[$i] == "e")
            break;
        $ret = bdec(substr($s, $i));
        if (!isset($ret) || !is_array($ret) || $ret["type"] <> "string")
            return;
        $k = $ret["value"];
        $i += $ret["strlen"];
        $ss .= $ret["string"];
        if ($i >= $sl)
            return;
        $ret = bdec(substr($s, $i));
        if (!isset($ret) || !is_array($ret))
            return;
        $v[$k] = $ret;
        $i += $ret["strlen"];
        $ss .= $ret["string"];
    }
    $ss .= "e";
    return array(
        'type' => "dictionary",
        'value' => $v,
        'strlen' => strlen($ss),
        'string' => $ss);
}

function benc_announce_list($announce_urls) {

$announce_urls = array_unique($announce_urls);

for ($i = 0; $i < count($announce_urls); $i++) {

if (!empty($announce_urls[$i])) {
$list[$i][$i] = bdec(benc_str(trim($announce_urls[$i])));
$list[$i]= bdec(benc_list($list[$i]));
}
}

return bdec(benc_list($list));
}


function dict_profiles ($dict = false, $allinone = false){

unset($info["value"]["info"]['value']['pieces']); /// ������ ������� ��� �����, ����� �� ������ ��������� php

$profiles = $dict["value"]["info"]["value"]["profiles"]["value"];
$pduration = $dict["value"]["info"]["value"]["file-duration"]["value"];

if (is_array($profiles)){

foreach ($profiles AS $prof){

$num = 0;
foreach ($prof["value"] AS $dpor => $drfop){

$duration = $pduration[$num]["value"];
if (!empty($duration)){
if ($allinone == true)
$my_value[$num]["duration"] = "duration: ".$duration;
else
$my_value[$num]["duration"] = $duration;
}

if (!empty($drfop["value"])){
if ($allinone == true)
$my_value[$num][$dpor] = $dpor.": ".(in_array($dpor, array("height", "width")) ? $drfop["value"]." pixels":$drfop["value"]);
else
$my_value[$num][$dpor] = (in_array($dpor, array("height", "width")) ? $drfop["value"]." pixels":$drfop["value"]);
}

}

++$num;
}
} else return false;

if (count($my_value))
return $my_value;
else 
return false;

}



if (!function_exists("dict_check_t")) {
    
function dict_check_t($d, $s) {
$a = explode(":", $s);
$dd = $d["value"];
$ret = array();
foreach ($a as $k) {
unset($t);
if (preg_match('/^(.*)\((.*)\)$/', $k, $m)) {
$k = $m[1];
$t = $m[2];
}
if (isset($t)) {
$ret[] = $dd[$k]["value"];
}
else
$ret[] = $dd[$k];
}
return $ret;
}

}

if (!function_exists("dict_get_t")) {

function dict_get_t($d, $k, $t) {
$dd = $d["value"];
$v = $dd[$k];
return $v["value"];
}

}

if (!function_exists("dict_check")) {

function dict_check($d, $s) {

global $tracker_lang;

if ($d["type"] != "dictionary")
stderr($tracker_lang['error'], "not a dictionary");
$a = explode(":", $s);
$dd = $d["value"];

$ret = array();
foreach ($a as $k) {
unset($t);

if (preg_match('/^(.*)\((.*)\)$/', $k, $m)) {
$k = $m[1];
$t = $m[2];
}

if (!isset($dd[$k]))
stderr($tracker_lang['error'], "dictionary is missing key(s)");

if (isset($t)) {

if ($dd[$k]["type"] != $t)
stderr($tracker_lang['error'], "invalid entry in dictionary");

$ret[] = $dd[$k]["value"];
} else
$ret[] = $dd[$k];

}

return $ret;
}

}

if (!function_exists("dict_get")) {

function dict_get($d, $k, $t) {

global $tracker_lang;

if ($d["type"] != "dictionary")
stderr($tracker_lang['error'], "not a dictionary");

$dd = $d["value"];
if (!isset($dd[$k]))
return;

$v = $dd[$k];
if ($v["type"] != $t)
stderr($tracker_lang['error'], "invalid dictionary entry type");

return $v["value"];
}

}





function replace_chunked ($data){

$fp = 0;
$result = '';

$result_ori = $data;

if (preg_match('/Content\-Encoding\: gzip/is', $data) && preg_match('/Transfer\-Encoding\: chunked/is', $data)) {

$data = preg_replace('/^(.*?)\r\n\r\n(.*?)$/is', "\\2", $data);

$d = $data;

$lrn = strlen("\r\n");
do {
$p = @strpos($d, "\r\n", $ofs);

if ($p===false){
$str = ''; break;
}

$len = hexdec(substr($d, $ofs, $p-$ofs));
$str .= substr($d, $p+$lrn, $len);
$ofs = $p+$lrn*2+$len;

} while ($d[$ofs]!=='0');
$d = $str;

$result = @gzinflate(substr($d, 10));

} else {


if (preg_match('/Content\-Encoding\: gzip/is', $date)) {

$data = preg_replace('/^.*?\r\n\r\n(.*?)/is', "\\1", $data);
$result = @gzinflate(substr($data, 10));

} else
$data = preg_replace('/^.*?\r\n\r\n(.*?)$/is', "\\1", $data);


if (preg_match("/\r\n/i", $data)){
while ($fp < strlen($data)) {
$rawnum = substr($data, $fp, strpos(substr($data, $fp), "\r\n") + 2);
$num = hexdec(trim($rawnum));
$fp += strlen($rawnum);
$chunk = substr($data, $fp, $num);
$result.= $chunk;
$fp += strlen($chunk);
}
}


}

if (!empty($result)) return $result;
elseif (empty($data) && !empty($result_ori)) return $result_ori;
else return $data;

}


function get_remote_peers ($url, $info_hash, $timeout = 5, $method = 'scrape', $light = false) {

global $Functs_Patch;

if (!empty($Functs_Patch["remote_peers"])) $timeout = (int) $Functs_Patch["remote_peers"];
else $timeout = 5;

$tstart = timer(); // Start time
$orurl = $url;


$parser_url = @parse_url($url);

$host = $parser_url['host'];
$port = (empty($parser_url['port']) ? "80" : $parser_url['port']);
$scheme = $parser_url['scheme'];


if (preg_match('/announce/is', $url)) $method = 'announce';

$link = ROOT_PATH."/torrents/txt/an_l".$host."_p".$port.".txt";
if ($method == 'scrape' && file_exists($link)) { /// �� ���������

$gdata = trim(file_get_contents($link));
if (in_array($gdata, array("scrape", "announce")) && $gdata <> 'scrape') $method = 'announce';

}


if ($method == "announce" && $light == false)
$options = array(
"info_hash" => pack("H*", $info_hash),
"peer_id" => urlencode('-UT2000-%00FX%e7'),
"port" => rand(8080, 65535),
"uploaded" => rand(1000, 100000),
"downloaded" => rand(1000, 100000),
"compact" => 1,
"corrupt" => 0,
"no_peer_id" => 1,
"left" => 1,
"key" => "6T6E6SLA",
"numwant" => 500);
else
$options = array("info_hash" => pack("H*", $info_hash));



if ($method == "scrape")
$parser_url['path'] = str_replace('announce', 'scrape', $parser_url['path']);

if (empty($url) && empty($host))
return array('tracker' => $host, 'state' => 'no_host');

if ($Functs_Patch["remote_off"] == true)
return array('tracker' => $host, 'state' => 'off');

$http_path = $parser_url['path'];
$query_explode = explode('&', $parser_url['query']);

$new_query = array();
foreach (array_filter($query_explode) as $array_value) {
list ($key, $value) = explode('=', $array_value);
$new_query[$key] = $value;
}

$http_query = htmlspecialchars_decode(http_build_query(@array_merge($new_query, $options)));

$req_uri = $scheme.'://'.$host.":".$port.((empty($http_path) && !empty($http_query)) ? "/" : $http_path).($http_query ? '?'.$http_query : '');

$req_uri = preg_replace('/\/\?/i', "/".$method."?", $req_uri);

///stream_socket_server
$schview = ($scheme <> 'http' ? $scheme.'://':'').$host.($port <> 80 ? ':'.$port:"");
//$schview = $host;


if (function_exists('fsockopen') && $scheme == 'udp'){

$transaction_id = mt_rand(1, 65535);
$fp = fsockopen(($scheme == 'udp' ? "udp://":"").$host, $port, $errno, $errstr);

if (!$fp) $failure_reason = 'Could not open UDP connection: '.htmlspecialchars_uni($errno, false, true).' - '.htmlspecialchars_uni($errstr, false, true);

if ($fp){

stream_set_timeout($fp, $timeout);

$current_connid = "\x00\x00\x04\x17\x27\x10\x19\x80";
$packet = $current_connid . pack("N", 0) . pack("N", $transaction_id); /// Connection request

fwrite($fp, $packet);

$ret = fread($fp, 16); /// Connection response

if (strlen($ret) < 1) $failure_reason = 'No connection response. '.$ret;
if (strlen($ret) < 16) $failure_reason = 'Too short connection response. '.$ret;

if ($light == false && strlen($ret) < 16 && strlen($ret) < 1)
return get_remote_peers($url, $info_hash, $timeout, "announce", true);

if (!empty($ret))
$retd = unpack("Naction/Ntransid", $ret);

if ($retd['action'] <> 0 || $retd['transid'] <> $transaction_id)
$failure_reason = 'Invalid connection response. (#'.$transaction_id.') '.print_r($ret, 1);
else {

$current_connid = substr($ret, 8, 8);
$packet = $current_connid . pack("N", 2) . pack("N", $transaction_id) . pack('H*', $info_hash); ///Scrape request

fwrite($fp, $packet); ///fputs($fp, $packet);

$ret = fread($fp, 20);
if (strlen($ret) < 1) $failure_reason = 'No scrape response. '.print_r($ret, 1);
if (strlen($ret) < 8) $failure_reason = 'Too short scrape response. '.print_r($ret, 1);

}


if (!empty($ret)){

$retd = unpack("Naction/Ntransid", $ret);

if ($retd['action'] <> 2 || $retd['transid'] <> $transaction_id) 
$failure_reason = 'Invalid scrape response.';

if (strlen($ret) < 20)
$failure_reason = 'Too short scrape response. ('.strlen($ret).'/'.strlen($ret).'/20)';
else
$retd = unpack("Nseeders/Ncompleted/Nleechers", substr($ret, 8, 12));

if (empty($gdata) && !empty($link)) file_put_contents($link, $method);
}

fclose($fp);

} else {

/// ������� ����� ����� ��������� ������� �������� ���� ����� ���������� ������ ��������.
if (!empty($gdata) && (time() - filemtime($link)) < 60*60*24*7) @unlink($link);

}


$type = 'fsockopen';

if (count($retd) && is_array($retd))
return array('tracker' => $host, 
'seeders' => (!empty($retd['seeders']) ? $retd['seeders']:"0"),
'leechers' => (!empty($retd['leechers']) ? $retd['leechers']:"0"),
'downloaded' => (!empty($retd['completed']) ? $retd['completed']:"0"),
'state' => 'ok', 'type' => $type,
'error' => (!empty($failure_reason) ? htmlspecialchars_uni($failure_reason, false, true):false));

elseif (!empty($failure_reason))
return array('tracker' => $host, 'state' => 'false', 'type' => $type, 'error' => htmlspecialchars_uni($failure_reason, false, true));
else
return array('tracker' => $host, 'state' => 'false', 'type' => $type );

}


//// ����� ��� ��������� ��������� (��� ��������� udp!!!)

if (function_exists('fsockopen')){

$fp = @fsockopen($host, $port, $errno, $errstr, $timeout);

if ($fp) {
$headers = "GET ".((empty($http_path) && !empty($http_query)) ? "/" : $http_path).(!empty($http_query) ? '?'.$http_query : '')." HTTP/1.1\r\n";
$headers.= "Host: ".$host."\r\n";
$headers.= "Connection: close\r\n";
$headers.= "Content-Length: 0\r\n";
$headers.= "Accept-Encoding: gzip\r\n";
$headers.= "User-Agent: uTorrent/3130\r\n\r\n";

$headers = preg_replace('/\/\?/i', "/".$method."?", $headers);

@fputs($fp, $headers);

$result = '';
while (!@feof($fp)) {
$result.= @fgets($fp, 2024);
}

@fclose($fp);

if ($light == false && stristr($result, 'Invalid'))
return get_remote_peers($url, $info_hash, $timeout, "announce", true);

} else $failure_reason = 'Could not open TCP connection: '.htmlspecialchars_uni($errno, false, true).' - '.htmlspecialchars_uni($errstr, false, true);

$type = 'fsockopen';

} elseif (function_exists('file_get_contents') && ini_get('allow_url_fopen') == 1) {

$opts = array($scheme => array('method' => 'GET', 'protocol_version' => '1.1', 'follow_location' => 0, 'header' => 'Accept-Encoding: gzip\r\nUser-Agent: uTorrent/3130', 'timeout' => $timeout));

$context = @stream_context_create($opts);
$result = @file_get_contents($req_uri, false, $context);

if ($light == false && stristr($http_response_header[0], 'Invalid'))
return get_remote_peers($url, $info_hash, $timeout, "announce", true);

$type = 'file_get_contents';

} elseif (function_exists('curl_init')) {

if ($ch = @curl_init()) {

@curl_setopt($ch, CURLOPT_URL, $req_uri);
@curl_setopt($ch, CURLOPT_PORT, $port);
@curl_setopt($ch, CURLOPT_HEADER, false);
@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
@curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
@curl_setopt($ch, CURLOPT_BUFFERSIZE, 1000);
@curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
@curl_setopt($ch, CURLOPT_USERAGENT, 'uTorrent/3130');

$result = @curl_exec($ch);
@curl_close($ch);

if ($light == false && stristr($result, 'Invalid'))
return get_remote_peers($url, $info_hash, $timeout, "announce", true);

}

if (curl_exec($ch) === false)
$failure_reason = 'Could not open TCP connection: '.htmlspecialchars_uni(curl_error($ch), false, true);

$type = 'curl_init';

} elseif (function_exists('socket_create')){

$sh = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

if ($sh) {

socket_set_option($sh, SOL_SOCKET, SO_SNDTIMEO, array('sec' => ($timeout), 'usec' => 0));
socket_set_option($sh, SOL_SOCKET, SO_RCVTIMEO, array('sec' => ($timeout), 'usec' => 0));

$con = @socket_connect($sh, (!is_numeric(ip2long($host)) ? gethostbyname($host):$host), $port);

if ($con){

$headers = "GET ".((empty($http_path) && !empty($http_query)) ? "/" : $http_path).(!empty($http_query) ? '?'.$http_query : '')." HTTP/1.1\r\n";
$headers.= "Host: ".$host."\r\n";
$headers.= "Connection: close\r\n";
$headers.= "Content-Length: 0\r\n";
$headers.= "Accept-Encoding: gzip\r\n";
$headers.= "User-Agent: uTorrent/3130\r\n\r\n";

$headers = preg_replace('/\/\?/i', "/".$method."?", $headers);

$response = @socket_write($sh, $headers, strlen($headers)); /// ���������� ������ � �����
$result = @socket_read($sh, 1024); /// ������ ������ �� ������

@socket_shutdown($sh, 2);
@socket_close($sh);

if ($light == false && stristr($result, 'Invalid'))
return get_remote_peers($url, $info_hash, $timeout, "announce", true);

}

}

$type = 'socket_connect';

}



$result = replace_chunked($result);

if (!empty($result)){
preg_match("/<title>((([0-9]){3})\s(.*?))<\/title>/i", $result, $erhead);

if (!empty($erhead[1])){

preg_match("/<p>(.*?)<\/p>/i", $result, $ertext); $failure_reason = $erhead[1].(!empty($ertext[1]) ? " (".$ertext[1].")":"");

return array('tracker' => $host, 'state' => 'false',  'type' => $type, 'error' => (!empty($failure_reason) ? htmlspecialchars_uni($failure_reason, false, true):false));

}
}

if ($result == "d5:filesdee" && $method == 'announce')
return array('tracker' => $host, 'state' => 'false',  'type' => $type, 'error' => (!empty($failure_reason) ? htmlspecialchars_uni($failure_reason, false, true):false));

if (empty($result) && !empty($orurl)) {

if ($method == 'scrape')
return get_remote_peers($orurl, $info_hash, $timeout, "announce");
elseif (number_format((timer() - $tstart), 0) >= $timeout)
return array('tracker' => $host, 'state' => 'false',  'type' => $type,  'error' => (!empty($failure_reason) ? htmlspecialchars_uni($failure_reason, false, true):false));
else
return array('tracker' => $host, 'state' => 'false',  'type' => $type,  'error' => (!empty($failure_reason) ? htmlspecialchars_uni($failure_reason, false, true):false));
}

/// �������������� � "torrent" ������ �� ������ ������
$result = @bdec($result);

$failure_reason = trim($result["value"]["failure reason"]["value"]);
if (!empty($failure_reason) || (!is_array($result) && $method == "announce")){

if (!empty($gdata) && !empty($link)) @unlink($link);

return array('tracker' => $host, 'state' => 'false',  'type' => $type,  'error' => (!empty($failure_reason) ? htmlspecialchars_uni($failure_reason, false, true):false));

}

if (empty($gdata) && !empty($link)) file_put_contents($link, $method);

if ($method == 'scrape') {

if (!count($result['value']['files']['value'])) {

if (is_array($result['value']['files']['value']))
$peersarray = array_shift($result['value']['files']['value']);

return array('tracker' => $host, 
'seeders' => (!empty($peersarray['value']['complete']['value']) ? $peersarray['value']['complete']['value']:"0"),
'leechers' => (!empty($peersarray['value']['incomplete']['value']) ? $peersarray['value']['incomplete']['value']:"0"),
'downloaded' => (!empty($peersarray['value']['downloaded']['value']) ? $peersarray['value']['downloaded']['value']:"0"),
'state' => 'ok', 'type' => $type, 
'error' => (!empty($failure_reason) ? htmlspecialchars_uni($failure_reason, false, true):false));

} else
return get_remote_peers($orurl, $info_hash, $timeout, "announce");

} elseif ($method == 'announce') {

if (!empty($result['value']['peers']['value']) && !isset($result['value']['complete']['value']))
$result['value']['complete']['value'] = $result['value']['peers']['value'];

return array('tracker' => $host, 
'seeders' => (is_array($result['value']['complete']['value']) ? count($result['value']['complete']['value']):(int) $result['value']['complete']['value']),
'leechers' => (is_array($result['value']['incomplete']['value']) ? count($result['value']['incomplete']['value']):(int) $result['value']['incomplete']['value']),
'downloaded' => (is_array($result['value']['downloaded']['value']) ? count($result['value']['downloaded']['value']):(int) $result['value']['downloaded']['value']),
'state' => 'ok', 'type' => $type, 

'error' => (!empty($failure_reason) ? htmlspecialchars_uni($failure_reason, false, true):false));

}

}

?>