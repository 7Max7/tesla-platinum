<?
require "include/bittorrent.php";
dbconn();
loggedinorreturn();


function ratios ($up, $down, $color = true) {

if ($down > 0 && $color)
$r = "<font color=\"".get_ratio_color($r)."\">".number_format($up / $down, 2)."</font>";
elseif ($up > 0)
$r = "Inf.";
else
$r = "---";

return $r;
}

if (get_user_class() == UC_SYSOP && isset($_GET["id"])) {

$userid = (!empty($_GET["id"]) ? intval($_GET["id"]):"");

if (empty($userid))
stderr($tracker_lang['error'], $tracker_lang['no_user_intable']);

$res = sql_query("SELECT id, username, class, ip FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['no_user_intable']);

$userid = $arr["id"];
$username = $arr["username"];
$userip = $arr["ip"];

$mask = "255.255.255.0";
$tmpip = explode(".", $userip);
$ip = $tmpip[0].".".$tmpip[1].".".$tmpip[2].".0";
$regex = "/^(((1?\d{1,2})|(2[0-4]\d)|(25[0-5]))(\.\b|$)){4}$/";

if (substr($mask,0,1) == "/"){
$n = substr($mask, 1, strlen($mask) - 1);

if (!is_numeric($n) || $n < 0 || $n > 32)
stderr($tracker_lang['user_ip'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['mask_ip']);
else 
$mask = long2ip(pow(2,32) - pow(2,32-$n));

} elseif (!preg_match($regex, $mask))
stderr($tracker_lang['user_ip'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['mask_ip']);

stdhead($tracker_lang['neighbours'].": ".$username);

$sqrow1 = mysql_fetch_row(sql_query("SELECT u1.id AS previd FROM users AS u1 WHERE u1.id < ".sqlesc($userid)." ORDER BY u1.id DESC LIMIT 1"));
$previd = $sqrow1[0];

$sqrow2 = mysql_fetch_row(sql_query("SELECT u2.id AS nextid FROM users AS u2 WHERE u2.id > ".sqlesc($userid)." ORDER BY u2.id ASC LIMIT 1"));
$nextid = $sqrow2[0];

$prevpict = $tracker_lang['page_prev'];
if (!empty($previd))
$prevpict = "<a class=\"altlink_white\" href=\"subnet.php?id=".$previd."\">".$tracker_lang['page_prev']."</a> (".$previd.")";

$nextpict = $tracker_lang['page_next'];
if (!empty($nextid))
$nextpict = "<a class=\"altlink_white\" href=\"subnet.php?id=".$nextid."\">".$tracker_lang['page_next']."</a> (".$nextid.")";

if (!validip_pmr($ip)){
$ip1 = $tmpip[0].".".$tmpip[1].".0.0";
$ip2 = $tmpip[0].".".$tmpip[1].".255.255";
$viewip = $tmpip[0].".".$tmpip[1].".";
$res = sql_query("SELECT id, username, class, last_access, added FROM users WHERE ip BETWEEN ('".$ip1."') and ('".$ip2."') LIMIT 500") or sqlerr(__FILE__, __LINE__);
} else {
$viewip = $tmpip[0].".".$tmpip[1].".".$tmpip[2].".";
$res = sql_query("SELECT id, username, class, last_access, added FROM users WHERE INET_ATON(ip) & INET_ATON('".$mask."') = INET_ATON('".$ip."') & INET_ATON('".$mask."') LIMIT 500") or sqlerr(__FILE__, __LINE__);
}

$num = mysql_num_rows($res);

echo ("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">");

echo ("<tr><td class=\"colhead\" align=\"center\" colspan=\"4\">");

echo ('<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%"><tr>
<td width="20%" class="colhead">'.$prevpict.' '.$tracker_lang['page'].'</td>
<td align="center" class="colhead">'.$tracker_lang['neighbours'].' (<a href="userdetails.php?id='.$userid.'" target="_blank">'.get_user_class_color($arr["class"], $arr["username"]).'</a>)</td>
<td width="20%" align="right" class="colhead">'.$nextpict.' '.$tracker_lang['page'].'</td>
</tr></table>');

echo ("</td></tr>");

echo ("<tr><td colspan=\"4\" align=\"center\" class=\"b\">".$tracker_lang['subnet_list']."</td></tr>");

echo ("<tr>
<td class=\"colhead\" align=\"center\">".$tracker_lang["signup_username"]."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['registered']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['online_now']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['user_ip']."</td>
</tr>");

if (empty($num))
echo ("<tr><td align=\"center\" colspan=\"4\">".$tracker_lang['no_data_now']."</td></tr>");

else {

$num = 0;
while ($arr = mysql_fetch_assoc($res)){

if ($num%2==0){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

echo ("<tr>");
echo ("<td ".$cl1." align=\"center\"><a href=\"userdetails.php?id=".$arr["id"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a> <a href=\"message.php?action=sendmessage&receiver=".$arr['id']."\"><img src=\"./pic/button_pm.gif\" border=\"0\" alt=\"".$tracker_lang['sendmessage']."\"></a></td>");
echo ("<td ".$cl1." align=\"center\">".$arr["added"]."</td>");
echo ("<td ".$cl1." align=\"center\">".$arr["last_access"]."</td>");
echo ("<td ".$cl1." align=\"center\">".$viewip."*</td>");
echo ("</tr>");

++$num;

}

}

echo ("</table>");
stdfoot();

} else {

$mask = "255.255.255.0";
$tmpip = explode(".",$CURUSER["ip"]);
$ip = $tmpip[0].".".$tmpip[1].".".$tmpip[2].".0";
$regex = "/^(((1?\d{1,2})|(2[0-4]\d)|(25[0-5]))(\.\b|$)){4}$/";

if (substr($mask,0,1) == "/"){
$n = substr($mask, 1, strlen($mask) - 1);

if (!is_numeric($n) || $n < 0 || $n > 32){

stdmsg($tracker_lang['user_ip'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['mask_ip']);
stdfoot();
die();

} else
$mask = long2ip(pow(2,32) - pow(2,32-$n));

} elseif (!preg_match($regex, $mask)){

stdmsg($tracker_lang['user_ip'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['mask_ip']);
stdfoot();
die();

}

$res = sql_query("SELECT id, username, class, last_access, added, uploaded, downloaded FROM users WHERE enabled = 'yes' AND id <> ".sqlesc($CURUSER["id"])." AND INET_ATON(ip) & INET_ATON('".$mask."') = INET_ATON('".$ip."') & INET_ATON('".$mask."')") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res)){

stdhead($tracker_lang['neighbours']);

echo ("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">");

echo ("<tr><td class=\"colhead\" align=\"center\" colspan=\"8\">".$tracker_lang['neighbours']."</td></tr><tr><td colspan=\"8\">".$tracker_lang['subnet_list']."</td></tr>");

echo ("<tr>
<td class=\"colhead\" align=\"left\">".$tracker_lang['signup_username']."</td>
<td class=\"colhead\">".$tracker_lang['uploaded']."</td>
<td class=\"colhead\">".$tracker_lang['downloaded']."</td>
<td class=\"colhead\">".$tracker_lang['ratio']."</td>
<td class=\"colhead\">".$tracker_lang['registered']."</td>
<td class=\"colhead\">".$tracker_lang['online_now']."</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['class']."</td>
<td class=\"colhead\">".$tracker_lang['user_ip']."</td>
</tr>");

$num = 0;
while ($arr = mysql_fetch_assoc($res)){

if ($num%2==0){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

echo ("<tr>
<td ".$cl1." align=\"left\"><b><a href=\"userdetails.php?id=".$arr["id"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a></b> <a href=\"message.php?action=sendmessage&receiver=".$arr['id']."\"><img src=\"./pic/button_pm.gif\" border=\"0\" alt=\"".$tracker_lang['sendmessage']."\"></a></td>
<td ".$cl2.">".mksize($arr["uploaded"])."</td>
<td ".$cl1.">".mksize($arr["downloaded"])."</td>
<td ".$cl2.">".ratios($arr["uploaded"], $arr["downloaded"])."</td>
<td ".$cl1.">".$arr["added"]."</td>
<td ".$cl2.">".$arr["last_access"]."</td>
<td ".$cl1." align=\"left\">".get_user_class_name($arr["class"])."</td>
<td ".$cl2.">".$tmpip[0].".".$tmpip[1].".".$tmpip[2].".*</td>
</tr>");

++$num;
}

echo ("</table>");

stdfoot();

} else
stderr($tracker_lang['neighbours'], $tracker_lang['no_data_now']);
}


?>