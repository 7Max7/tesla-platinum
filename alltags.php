<?
require_once("include/bittorrent.php");
dbconn(false);

/**
* @author 7Max7
* @copyright 2010
*/

stdhead($tracker_lang['all_tags'], true);

?>

<script language="javascript" type="text/javascript" src="js/jquery.js"></script>


<style>
#categories {width: 220px;}
#categories td {color: #707070;font-family: Arial, sans-serif;font-size: 12px;padding-top: 3px;padding-bottom: 3px;}
#categories td.title {border-right: 5px solid #f6f6f4;overflow: hidden;white-space: nowrap;}
#categories td.today {background-color: #ffe4a5;padding-left: 5px;padding-right: 5px;width: 25px;text-align: right;}
#categories td.count {padding-left: 5px;width: 40px;text-align: right;}
#categories .active td.title {background-color: #e5e5e5;}
#tag_cloud {overflow: hidden;width: 100%;}

<?
$res = sql_query("SELECT id, name,(SELECT COUNT(*) FROM torrents WHERE categories.id=torrents.category) AS num_torrent FROM categories ORDER BY sort ASC", $cache = array("type" => "disk", "file" => "browse_genrelist", "time" => 86400));
while ($row = mysql_fetch_assoc_($res)){
echo "#tag_cloud a.level".$row["id"]." {  color: ".random_color().";  font-size: ".rand(18, 30)."px;  text-decoration: none;} \n";
}
?>

#tag_cloud a.highlight {color: #ff7e00;}
#tag_cloud a:hover, #tag_cloud a:active {text-decoration: underline;}
</style>

<script type="text/javascript">
<!--
var tag_cloud = {
highlightTimeout : 0,
clearTimeout : 0,
highlightDelay : 100,
clearDelay : 900,
level : 0,
clear : function(){
$('#tag_cloud .level' + tag_cloud.level).removeClass('highlight');
tag_cloud.level = 0;
},
over : function(level){
clearTimeout(tag_cloud.clearTimeout);
if(level == tag_cloud.level)
return;
clearTimeout(tag_cloud.highlightTimeout);
this.clear();
tag_cloud.level = level;
tag_cloud.highlightTimeout = setTimeout(function(){$('#tag_cloud .level' + level).addClass('highlight');}, tag_cloud.highlightDelay);
},
out : function(){
tag_cloud.clearTimeout = setTimeout(function(){tag_cloud.clear();}, tag_cloud.clearDelay);
}
};
// -->
</script>

<?

echo "<div id=\"tag_cloud\">";

$naname = array();

$wo = array(".",",","","+",":",";","/","(",")","|");

$count_res = sql_query("SELECT * FROM tags GROUP BY name ORDER BY name", $cache = array("type" => "disk", "file" => "alltags", "time" => 86400)) or sqlerr(__FILE__, __LINE__);
while ($row = mysql_fetch_assoc_($count_res)){

if (str_replace($wo, '', $row["name"]) <> $row["name"])
$naname[] = $row["id"];

echo '<a onmouseout="tag_cloud.out();" onmouseover="tag_cloud.over('.$row["category"].');" class="level'.$row["category"].'" href="browse.php?tag='.urlencode($row["name"]).'" title="'.htmlspecialchars_uni($row["name"]).'">'.htmlspecialchars_uni($row["name"]).'</a>  ';

}

echo "</div>";



if (count($naname) && $Torrent_Config["auto_tags"] == true){

echo "<hr> ".$tracker_lang['trun_tagofcat'].": ".count($naname)." ��.";
// ������� ����� ���� ������ �������� ��� 0 ��� 1
sql_query("DELETE FROM tags WHERE name = ''") or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM tags WHERE howmuch = '0'") or sqlerr(__FILE__,__LINE__);

sql_query("DELETE FROM tags WHERE id IN (".implode(",", $naname).")") or sqlerr(__FILE__, __LINE__);
}



stdfoot(true);
?>