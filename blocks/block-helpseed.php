<?
if (!defined('BLOCK_FILE')) {
 Header("Location: ../index.php");
 exit;
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-helpseed", "time" => 60*10, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {

global $tracker_lang;

$content = "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\">";

$res = sql_query("SELECT id, name, sticky, moderatedby, leechers, seeders FROM torrents WHERE (leechers > 0 AND seeders = 0) OR (leechers / seeders >= 4) ORDER BY leechers DESC LIMIT 25") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) > 0) {
$num = 0;
while ($arr = mysql_fetch_assoc($res)) {

if ($num%2 == 1)
$content.= "<tr><td class=\"a\">";
else
$content.= "<tr><td class=\"b\">";

$torrname = $arr['name'];
		
$content .= ($arr["sticky"] == "yes" ? "<b>".$tracker_lang['sticky']."</b>: " : "")."
<b><a href=\"details.php?id=".$arr['id']."\" alt=\"".htmlspecialchars($arr['name'])."\" title=\"".htmlspecialchars($arr['name'])."\">".htmlspecialchars_uni($torrname)."</a></b>
".sprintf($tracker_lang['new_torrents_stats'], "<b><font color=\"green\">".number_format($arr['seeders'])."</font></b>","<b><font color=\"red\">".number_format($arr['leechers'])."</font></b>")."<br />\n";

$content.= "</td></tr>";
++$num;
}

} else

$content .= "<tr><td class=\"text\"><b> ".$tracker_lang['no_need_seeding']." </b></td></tr>";

$content .= "</table>";

file_query($content, $cache = array("type" => "disk", "file" => "block-helpseed", "time" => 60*10, "action" => "set"));
}
?>