<?
/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2011 v.Platinum
 */

global $CURUSER, $tracker_lang;

if ($CURUSER && !empty($CURUSER["notifs"])){
preg_match_all('/\[cat([0-9]+)\]/i', $CURUSER["notifs"], $notif);
if (count($notif[1])) $notifs = $notif[1];
}

$blocktitle = $tracker_lang['fast_category'];

$cget = (isset($_GET["cat"]) ? intval($_GET["cat"]):0);

if (count($notifs))
$bs_cat = sql_query("SELECT id, name, image FROM categories WHERE id IN (".implode(",", $notifs).")", $cache = array("type" => "disk", "time" => 24*7200)) or sqlerr(__FILE__, __LINE__);
else
$bs_cat = sql_query("SELECT id, name, image FROM categories", $cache = array("type" => "disk", "file" => "browse_cat_array", "time" => 24*7200)) or sqlerr(__FILE__, __LINE__);

$content = "<form method=\"get\" action=\"browse.php\" name=\"fastcat\">\n";
$content.= "<select name=\"cat\" onchange=\"if(this.options[this.selectedIndex].value != -1){ forms['fastcat'].submit() }\">\n";
$content.= "<option ".(empty($cget) ? "selected":"").">".$tracker_lang['choose']."</option>\n";

while ($b_cat = mysql_fetch_assoc_($bs_cat)){
$content.= "<option ".($cget == $b_cat["id"] ? "selected":"")." value=".$b_cat["id"].">".htmlspecialchars($b_cat["name"])."</option>\n";
}

$content.= "</select>\n";
$content.= "<br /><input class=\"btn\" type=\"submit\" value=\"".$tracker_lang['selected']."\"></form>\n";

?>