<? 
include_once("include/bittorrent.php");
dbconn(false,true);


stdhead("Error 500 (Internal Server Error)", true); 

$ref = htmlspecialchars_uni($_SERVER["HTTP_REFERER"]);

if ($CURUSER && !empty($ref))
echo "<meta http-equiv='refresh' content=\"30 ".$ref."\">";

echo '<style type="text/css">
.style3 {font-size: 36px;font-family: "Times New Roman", Times, serif;}
.style4 {font-family: "Times New Roman", Times, serif;font-size: 16px;color: #999999;}
.style5 {font-size: 24px;font-family: "Times New Roman", Times, serif;color: #006699;}
.style6 {font-size: 28px;font-family: "Times New Roman", Times, serif;color: #FFFFFF;}
</style>';

echo "
<p align=\"center\" class=\"style5\">Error 500 (Internal Server Error)</p>
<p align=\"center\" class=\"style4\">".$tracker_lang['er_500_i']."</p>
".(!empty($ref) ? "<p align=\"center\" class=\"style5\"><a href=\"".$ref."\" title=\"".$tracker_lang['back_inlink']."\">".$tracker_lang['back_inlink']."</a></p>":"")."
<p align=\"center\" class=\"style6\">".$tracker_lang['hi_from_maks']."</p>
";

stdfoot(true);
?>