<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

if (get_user_class() < UC_MODERATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

$userid = (int) $_POST["userid"]; /// �����
$title = htmlspecialchars_uni(strip_tags($_POST["title"]));
$enabled = (!empty($_POST["enabled"]) ? $_POST["enabled"]:""); /// yes no
$warned = (!empty($_POST["warned"])? $_POST["warned"]:"");  /// yes no
$warnlength = (int) $_POST["warnlength"]; /// �����
$warnpm = htmlspecialchars_uni($_POST["warnpm"]);  /// ������� ��������������
$uploadtoadd = (string) $_POST["amountup"]; //plus minus
$downloadtoadd = (string) $_POST["amountdown"];//plus minus
$formatup = (string) $_POST["formatup"]; // mb gb
$formatdown = (string) $_POST["formatdown"];  // mb gb
$invitetoadd = (int) $_POST["amountinvite"]; /// ����� //����� � �����������
$mpinvite = (string) $_POST["invitechange"]; // plus munis //����� � �����������
$mpup = ($_POST["upchange"] == "plus" ? "plus":"minus"); // plus minus
$mpdown = ($_POST["downchange"] == "plus" ? "plus":"minus"); //plus minus
$support = (string) $_POST["support"]; // yes no
$supportfor = htmlspecialchars_uni($_POST["supportfor"]); /// ������� ���� �������
$modcomm = htmlspecialchars_uni($_POST["modcomm"]); /// ������� ���� �������
$deluser = (!empty($_POST["deluser"])? $_POST["deluser"]:"");
$reset_user_ratio = (!empty($_POST["reset_user_ratio"]) ? (int) $_POST["reset_user_ratio"]:0); /// 1 0
$user_reliases_anonim = (!empty($_POST["user_reliases_anonim"]) ? (int) $_POST["user_reliases_anonim"]:0); /// 1 0
$reset_user_torrents = (!empty($_POST["reset_user_torrents"])? (int) $_POST["reset_user_torrents"]:0); /// 1 0
$release_set_id = (!empty($_POST["release_set_id"]) ? (int) $_POST["release_set_id"]:0); /// �����
$release_unset_id = (!empty($_POST["release_unset_id"]) ? (int) $_POST["release_unset_id"]:0); /// �����
$class = (!empty($_POST["class"]) ? (int) $_POST["class"]:0); // �����

if ($class > UC_SYSOP) /// ���� �� ��� ����� ���� 6 ������ �����
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if (!is_valid_id($userid) || !is_valid_user_class($class))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

/// ��������� ������
$res = sql_query("SELECT * FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

$arr = mysql_fetch_assoc($res) or stderr($tracker_lang['error_data'], sprintf($tracker_lang['no_user_isid'], $userid));

if (get_user_class() <= UC_SYSOP && $arr["override_class"] == UC_SYSOP)
stderr($tracker_lang['error'], $tracker_lang['access_denied'].". ".$tracker_lang['class_default'].": ".get_user_class_name($arr["override_class"]));

if (get_user_class() < UC_ADMINISTRATOR) {

$donor = $arr["donor"];
$uploadpos = $arr["uploadpos"];
$downloadpos = $arr["downloadpos"];
$infouser = $arr["info"];

} else {

$infouser = htmlspecialchars_uni(strip_tags($_POST["info"]));
$signature = htmlspecialchars_uni(strip_tags($_POST["signature"]));
$donor = ($_POST["donor"] == "yes" ? "yes":"no"); /// yes no
$uploadpos = (string) $_POST["uploadpos"]; /// yes no
$downloadpos = (string) $_POST["downloadpos"]; /// yes no
$hiderating = (string) $_POST["hiderating"]; /// yes no
$hidelength = (int) $_POST["hidelength"]; /// ������� - 100

}

$curuploadpos = $arr["uploadpos"];
$curdownloadpos = $arr["downloadpos"];
$curclass = $arr["class"];
$curwarned = $arr["warned"];
$curhiderating = $arr["hiderating"];

if (get_user_class() == UC_SYSOP)
$modcomment = htmlspecialchars_uni($_POST["modcomment"]);
else
$modcomment = $arr["modcomment"];

$usercomment = $arr["usercomment"];


if (get_user_class() > UC_MODERATOR && isset($_POST["addbookmark"])){

$addbookmark = (string) $_POST["addbookmark"]; /// yes no
$bookmcomment = htmlspecialchars_uni(strip_tags($_POST["bookmcomment"]));

$updateset[] = "bookmcomment = ".sqlesc($bookmcomment);
$updateset[] = "addbookmark = ".sqlesc($addbookmark);

}

// ��������� ���� ������ ������� <- ������ -> ������������� ���������� �� �������.
if ($CURUSER['id'] <> $userid && ($curclass >= get_user_class() || $class >= get_user_class()))
stderr($tracker_lang['error'], $tracker_lang['access_denied']);


if (get_user_class() == UC_SYSOP) {

if (!empty($_POST['desysteml'])){

/// ������������ �������� ��� ���������� \ �����������
$updateset[] = "override_class = '255'";
$updateset[] = "last_access = ".sqlesc(get_date_time(gmtime() - 500*86400));
$updateset[] = "class = '0'";
$modcomment = date("Y-m-d")." ��������� ���������� - ".$CURUSER['username'] .".\n".$modcomment;

}

if (isset($_POST['resettestclass']) && $_POST['resettestclass'] == "yes" && $arr["override_class"] <> 255){

$updateset[] = "override_class = 255";
$updateset[] = "class = ".sqlesc($arr["override_class"]);
$updateset[] = "promo_time = ".sqlesc(get_date_time());

}


//������ �����
if (!empty($_POST['resetpass'])) {

$chpassword = generatePassword();
$sec = mksecret();
$passhash = md5($sec . $chpassword . $sec);
$updateset[] = "secret = ".sqlesc($sec);
$updateset[] = "passhash = ".sqlesc($passhash);
$modcomment = date("Y-m-d")." - ������ ������� ������������� ".$CURUSER['username'] ."\n".$modcomment;

$body = <<<EOD
��� ������ ��� �������.

���� ����� ������ ��� ��������:

������������: {$arr["username"]}
����� ������: $chpassword

����� �� ����: $DEFAULTBASEURL/login.php
���� ���� �������� � ����� ������� ��������� � ��������������� ��� ������������� �����.
--
$SITENAME
EOD;

if (!sent_mail($arr["email"], $SITENAME, $SITEEMAIL, "����� ������", $body, false))
stderr($tracker_lang['error'], $tracker_lang['enabled_sendmail']);

} //������ �����


if ($arr["monitoring"] == 'no' && $_POST['monitoring'] == 'yes' && isset($_POST['monitoring_reason']) && !empty($_POST['monitoring_reason'])) {

accessadministration();

$sf = ROOT_PATH."/cache/monitoring_".$userid.".txt";
$fpsf = fopen($sf, "a+");
fputs($fpsf, "������. �������: ".htmlspecialchars_uni($_POST['monitoring_reason'])."##".$arr["ip"]."##".get_date_time()."#\n\r");
fclose($fpsf);

$updateset[] = "monitoring = 'yes'";
}

if ($arr["monitoring"] == 'yes' && $_POST['monitoring'] == 'no') {

accessadministration();

$sf = ROOT_PATH."/cache/monitoring_".$userid.".txt";
$fpsf = fopen($sf,"a+");
fputs($fpsf, "����� �����������.##".$arr["ip"]."##".get_date_time()."#\n\r");
fclose($fpsf);

$updateset[] = "monitoring = 'no'";
}

if (!empty($_POST['prune_monitoring'])=='yes'){

accessadministration();
@unlink(ROOT_PATH."/cache/monitoring_".$userid.".txt");

}

}


if (!empty($_POST['resetsession']) && get_user_class() == UC_SYSOP)
sql_query("DELETE FROM sessions WHERE ip = ".sqlesc($arr["ip"])." OR uid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);


// �������
if ($invitetoadd > 0) {

if ($mpinvite == "plus")
$newinvite = $arr["invites"]+$invitetoadd;
else
$newinvite = $arr["invites"]-$invitetoadd;

if ($newinvite > 0){
$updateset[] = "invites = ".sqlesc($newinvite);
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ".($mpinvite == "plus" ? "�������: " : "�����: ").$invitetoadd." �����������(-��).\n". $modcomment;
}

}
// �������


/// ����� ���� ��������
if (isset($_POST["resetb"]) && $_POST["resetb"] == 'yes')
$updateset[] = "birthday = '0000-00-00'";

// �������� ������� ����� ��������� � ����� ������� �����.
if (!empty($_POST["reschitup"]) && get_user_class() >= UC_ADMINISTRATOR){

$re = sql_query("SELECT SUM(upthis) AS upthiss FROM cheaters WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
$ro = mysql_fetch_assoc($re);

$otherer = ($arr["uploaded"]-$ro["upthiss"]);

$modcomment = gmdate("Y-m-d")." - ".$CURUSER['username'].", ���� ������� ".mksize($ro["upthiss"])." � �������� ��������.\n".$modcomment;

sql_query("UPDATE users SET uploaded = ".sqlesc($otherer)." WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

$arr["uploaded"] = $otherer;

$resch = sql_query("SELECT torrentid, upthis FROM cheaters WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
while ($r = mysql_fetch_assoc($resch)) {

sql_query("UPDATE snatched SET uploaded = IF(uploaded > 0, uploaded - ".sqlesc($r["upthis"]).", 0) WHERE torrent = ".sqlesc($r["torrentid"])." AND userid = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

}

}



if ($uploadtoadd > 0) {

if ($mpup == "plus")
$newupload = $arr["uploaded"] + ($formatup == "mb" ? ($uploadtoadd * 1048576) : ($uploadtoadd * 1073741824));
else
$newupload = $arr["uploaded"] - ($formatup == "mb" ? ($uploadtoadd * 1048576) : ($uploadtoadd * 1073741824));

if ($newupload > 0){

$updateset[] = "uploaded = ".sqlesc($newupload);
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ".($mpup == "plus" ? "�������: " : "�����: ").$uploadtoadd.($formatup == "mb" ? " ��" : " ��")." � �������.\n". $modcomment;

}

}

if ($downloadtoadd > 0) {

if ($mpdown == "plus")
$newdownload = $arr["downloaded"] + ($formatdown == "mb" ? ($downloadtoadd * 1048576) : ($downloadtoadd * 1073741824));
else
$newdownload = $arr["downloaded"] - ($formatdown == "mb" ? ($downloadtoadd * 1048576) : ($downloadtoadd * 1073741824));

if ($newdownload > 0){

$updateset[] = "downloaded = ".sqlesc($newdownload);
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ".($mpdown == "plus" ? "�������: " : "�����: ").$downloadtoadd.($formatdown == "mb" ? " MB" : " GB")." � ���������.\n". $modcomment;

}
}


$warndown = (string) $_POST["warnchange"]; // plus minus
$warntoadd = (int) $_POST["amountwarn"]; // �����

if ($warntoadd > 0) {

if ($warndown == "plus")
$newwarn = $arr["num_warned"] + $warntoadd;
else
$newwarn = $arr["num_warned"] - $warntoadd;

if ($newwarn > 0){

$updateset[] = "num_warned = ".sqlesc($newwarn);
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ".($warndown == "plus" ? "�������: " : "�����: ").$warntoadd ." ��������������.\n". $modcomment;

}

}


if ($curclass <> $class && $userid <> $CURUSER['id']) {

$sender_id = ($_POST["ls_system"] == "yes" ? 0:$CURUSER['id']);
$system_ls = ($_POST["ls_system"] == "yes" ? "��������":"������������� [color=#".get_user_rgbcolor($CURUSER["class"], $CURUSER["username"])."]".$CURUSER["username"]."[/color]");

// ��������� ������������
$what = ($class > $curclass ? "��������" : "��������");

$msg = ("�� ���� [b]".$what."[/b] �� ������ [color=#".get_user_rgbcolor($class, $username)."]".get_user_class_name($class)."[/color] ".$system_ls.".");

$subject = ("�� ���� ".$what);

sql_query("INSERT INTO messages (sender, receiver, msg, added, subject) VALUES(".sqlesc($sender_id).", ".sqlesc($userid).", ".sqlesc($msg).", ".sqlesc(get_date_time()).", ".sqlesc($subject).")") or sqlerr(__FILE__, __LINE__);

$updateset[] = "class = ".sqlesc($class);
$updateset[] = "promo_time = ".sqlesc(get_date_time());

$what = ($class > $curclass ? "�������" : "�������");

$modcomment = date("Y-m-d")." - ".$what." �� ������ ".get_user_class_name($class)." ������������� ".$CURUSER["username"].".\n". $modcomment;
}


$chatban = (int) $_POST["schoutboxpos"];

if (!empty($chatban) && $arr["shoutbox"] == "0000-00-00 00:00:00"){

if ($chatban == 65535) {

$modcomment = gmdate("Y-m-d")." - ��������� ��� �� ��� �� ����� ����� ��� �� ".$CURUSER['username'].".\n".$modcomment;
$updateset[] = "shoutbox = ".sqlesc(get_date_time(gmtime() + 86400 * $chatban));

} else {

$forum_dur = $chatban." ".($chatban > 1 ? "����(�)" : "����");
$modcomment = gmdate("Y-m-d")." - ��� �� ��� �� ".$forum_dur." �� ".$CURUSER['username'] .".\n". $modcomment;
$updateset[] = "shoutbox = ".sqlesc(get_date_time(gmtime() + 86400 * $chatban));
}

}

if (!empty($_POST["chat_pos_off"])) {

$modcomment = gmdate("Y-m-d")." - ������� ��� � ���� - ".$CURUSER['username'].".\n".$modcomment;
$updateset[] = "shoutbox = '0000-00-00 00:00:00'";

}

// ������ ��������� �� � ���������
if (get_user_class() >= UC_ADMINISTRATOR){


if (isset($_POST["cansendpm"])){

$curcansendpm = $arr["cansendpm"];
$cansendpm = (string) $_POST["cansendpm"];

if ($cansendpm == 'yes' && $curcansendpm == 'no') {
$updateset[] = "cansendpm = 'yes'";
$modcomment = gmdate("Y-m-d")." - ��� ����������� �������� �� - ".$CURUSER['username'].".\n".$modcomment;
} elseif ($cansendpm == 'no' && $curcansendpm == 'yes') {
$updateset[] = "cansendpm = 'no'";
$modcomment = gmdate("Y-m-d")." - ���� ����������� �������� �� - ".$CURUSER['username'].".\n".$modcomment;
}

}

if (isset($_POST["commentpos"])){

$commentpos = (string) $_POST["commentpos"];

if ($commentpos == 'yes' && $arr["commentpos"] == 'no') {
$updateset[] = "commentpos = 'yes'";
$modcomment = gmdate("Y-m-d")." - ��� ����������� �������� ��������� - ".$CURUSER['username'].".\n".$modcomment;
} elseif ($commentpos == 'no' && $arr["commentpos"] == 'yes') {
$updateset[] = "commentpos = 'no'";
$modcomment = gmdate("Y-m-d")." - ���� ����������� �������� ��������� - ".$CURUSER['username'].".\n".$modcomment;
}

}


// �������� �������
if (!empty($_POST["reset_user_ratio"])){

sql_query("UPDATE users SET uploaded = 0, downloaded = 0 WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ������� ������� (U:".$arr["uploaded"].", D:".$arr["downloaded"].").\n". $modcomment;

}


// �������� ��������
if (!empty($_POST["recreat_ratio"])){

$res_sn = sql_query("SELECT sum(uploaded) AS uploaded, sum(downloaded) AS downloaded FROM snatched WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
$rowsn = mysql_fetch_assoc($res_sn);

$ne_snat_u = $rowsn["uploaded"];
$ne_snat_d = $rowsn["downloaded"];

sql_query("UPDATE users SET uploaded = ".sqlesc($ne_snat_u).", downloaded = ".sqlesc($ne_snat_d)." WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �������� �������� (U:".mksize($CURUSER["uploaded"]).", D:".mksize($CURUSER["downloaded"])." => U:".mksize($ne_snat_u).", D:".mksize($ne_snat_d).").\n". $modcomment;
}

}



// �������� ���������
if (get_user_class() > UC_ADMINISTRATOR && !empty($_POST["reset_moderatedby"])){

sql_query("UPDATE torrents SET moderatedby = '', moderated = 'no' WHERE moderatedby = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ������� ��� ���������.\n". $modcomment;
}

//������� ���������/�������� ��������
if (!empty($_POST["reset_user_torrents"])){

sql_query("DELETE FROM snatched WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);

$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ���� ��� ����-����� ��������.\n". $modcomment;

}

$po_username = (isset($_POST["username"]) ? htmlspecialchars($_POST["username"]) : "");

// ����� �����
if (get_user_class() >= UC_ADMINISTRATOR && !empty($po_username) && $row['username'] <> $po_username) {

if (!validusername($po_username) || strlen($po_username) > 12)
stderr($tracker_lang['error'], $tracker_lang['validusername']);

$modcomment = date("Y-m-d")." - ������������ �� ".$row['username']." � ".$po_username." ������������� ".$CURUSER['username'].".\n".$modcomment;

sql_query("UPDATE users SET username = ".sqlesc($po_username)." WHERE id = ".sqlesc($userid));

if (mysql_errno() == 1062){
$res3 = sql_query("SELECT id, class, username FROM users WHERE username = ".sqlesc($po_username)) or sqlerr(__FILE__, __LINE__);
$arr3 = mysql_fetch_array($res3);

if (mysql_num_rows($res3) > 0)
stderr($tracker_lang['error'], sprintf($tracker_lang['user_is_signup'], "<a href=\"userdetails.php?id=".$arr3["id"]."\">".get_user_class_color($arr3["class"], $arr3["username"])."</a>"));
}

}


if (get_user_class() >= UC_ADMINISTRATOR) {

$r = sql_query("SELECT id FROM categories") or sqlerr(__FILE__, __LINE__);
$rows = mysql_num_rows($r);

for ($i = 0; $i < $rows; ++$i) {
$a = mysql_fetch_assoc($r);

if (!empty($_POST["cat".$a["id"]]) && $_POST["cat".$a["id"]] == 'yes')
$catedit.= "[cat".$a["id"]."]";

}
if (!empty($catedit))
$updateset[] = "catedit = ".sqlesc($catedit);
}


/// �������� �����, �������� ����� ��� ip
if (get_user_class() == UC_SYSOP){

$text_reason = htmlspecialchars($_POST["text_reason"]);

$email = htmlspecialchars($arr["email"]);
$dom_email = end(explode("@", $email));

/// ��� �����
if (isset($_POST["ban_email"]) && !empty($text_reason)){

$bmail = get_row_count("bannedemails", "WHERE email = ".sqlesc($email));

if (empty($bmail)) {

$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ������� �����: ".$email.".\n". $modcomment;

sql_query("INSERT IGNORE INTO bannedemails (added, addedby, comment, email) VALUES(".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($text_reason." (".htmlspecialchars($arr["username"]).")").", ".sqlesc($email).")") or sqlerr(__FILE__, __LINE__);

}

}
/// ��� �����


/// ��� ������ �����
if (isset($_POST["ban_edom"]) && !empty($text_reason) && !empty($dom_email)){

$bmaid = get_row_count("bannedemails", "WHERE email = ".sqlesc("@".$dom_email));

if (empty($bmaid)) {

$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ������� ����� �����: @".$dom_email.".\n".$modcomment;

sql_query("INSERT IGNORE INTO bannedemails (added, addedby, comment, email) VALUES(".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($text_reason." (".htmlspecialchars($arr["username"]).")").", ".sqlesc("@".$dom_email).")") or sqlerr(__FILE__, __LINE__);

}

}
/// ��� ������ �����


/// ��� ip
if (isset($_POST["ban_ip"]) && !empty($text_reason) && !empty($arr["ip"])){

$true_ban = true;
$first = $last = ip2long(trim($arr["ip"]));

if (!is_numeric($first))
$true_ban = false;

$ip_bans = get_row_count("bans", "WHERE ".sqlesc($first)." >= first AND ".sqlesc($last)." <= last");

if ($first == -1 || !empty($ip_bans))
$true_ban = false;

$banua = get_row_count("users", "WHERE ip >= ".sqlesc(long2ip($first))." AND ip <= ".sqlesc(long2ip($last))." AND class >= ".UC_MODERATOR);

if (!empty($banua)) $true_ban = false;

if ($true_ban == true) {
sql_query("INSERT INTO bans (added, addedby, first, last, bans_time, comment) VALUES(".sqlesc(get_date_time()).", ".sqlesc($CURUSER["id"]).", ".sqlesc($first).", ".sqlesc($last).", ".sqlesc("0000-00-00 00:00:00").", ".sqlesc($text_reason).")") or sqlerr(__FILE__, __LINE__);

$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ������� ip: ".$arr["ip"].".\n".$modcomment;

write_log("IP �����".(long2ip($first) == long2ip($last)? " ".long2ip($first)." ��� �������":"� � ".long2ip($first)." �� ".long2ip($last)." ���� ��������")." ������������� ".$CURUSER["username"].".", "ff3a3a", "bans");

unsql_cache("bans_first_last");
}

}
/// ��� ip

}
/// �������� �����, �������� ����� ��� ip


//������� ������ ������������ ����������
if (!empty($_POST["user_reliases_anonim"])){

sql_query("UPDATE torrents SET owner = 0 WHERE owner = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
$modcomment = date("Y-m-d")." - ".$CURUSER['username']." ������ ������� ������������ ����������.\n". $modcomment;

}


//������� ����� ������� ������
if (isset($_POST["release_set_id"])){

$setid = (int) $_POST["release_set_id"];

$res = sql_query("SELECT id FROM torrents WHERE id = ".sqlesc($setid)) or sqlerr(__FILE__, __LINE__);

$row = mysql_fetch_array($res);

if (mysql_num_rows($res) > 0){

$modcomment = date("Y-m-d")." - ".$CURUSER['username']." �������� ������� id: ".$setid." ����� �����.\n". $modcomment;

sql_query("UPDATE torrents SET owner = ".sqlesc($userid)." WHERE id = ".sqlesc($setid)) or sqlerr(__FILE__,__LINE__);

}

}


//������� �������� � ������
if (isset($_POST["release_unset_id"])) {

$unsetid = (int) $_POST["release_unset_id"];

$res = sql_query("SELECT id, owner FROM torrents WHERE id = ".sqlesc($unsetid)." AND owner = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

$row = mysql_fetch_array($res);

if (mysql_num_rows($res) > 0 && $row["owner"] == $userid){

sql_query("UPDATE torrents SET owner = 0 WHERE id = ".sqlesc($unsetid)) or sqlerr(__FILE__,__LINE__);

$modcomment = date("Y-m-d")." - ".$CURUSER['username']." ������ ������ ��������� � id=".sqlesc($unsetid)." ������.\n". $modcomment;

}

}


if (get_user_class() > UC_MODERATOR){

if (isset($_POST["editinfo"])){

$editinfo = (string) $_POST["editinfo"];

if ($editinfo == 'yes' && $arr["editinfo"] == 'no'){

$updateset[] = "editinfo = 'yes'";
$modcomment = date("Y-m-d")." - ".$CURUSER['username']." �������� ������������� ���� ������������.\n". $modcomment;

} elseif ($editinfo == 'no' && $arr["editinfo"] == 'yes'){

$updateset[] = "editinfo = 'no'";
$modcomment = date("Y-m-d")." - ".$CURUSER['username']." �������� ������������� ���� ������������.\n". $modcomment;

}

}



if (isset($_POST["signatrue"])){

if ($arr["signatrue"] == 'yes' && $_POST["signatrue"] == 'no'){

$updateset[] = "signatrue = 'no'";
$modcomment = date("Y-m-d")." - ".$CURUSER['username']." �������� ������������� ������� ������������.\n". $modcomment;

} elseif ($arr["signatrue"] == 'no' && $_POST["signatrue"] == 'yes'){

$updateset[] = "signatrue = 'yes'";
$modcomment = date("Y-m-d")." - ".$CURUSER['username']." �������� ������������� ������� ������������.\n". $modcomment;

}

}

}


$num_warned = 1 + $arr["num_warned"]; //��� ���-�� ��������������

if (!empty($warned) && $curwarned <> $warned){

$updateset[] = "warned = ".sqlesc($warned);
$updateset[] = "warneduntil = '0000-00-00 00:00:00'";
$subject = ("���� �������������� �����");

if ($warned == 'no'){

$modcomment = date("Y-m-d")." - �������������� (�) (".intval($arr["num_warned"])." ��.) ���� ������������ ".$CURUSER['username'].".\n". $modcomment;
$msg = ("���� �������������� ���� ������������ ".$CURUSER['username'].".");

sql_query("UPDATE users SET num_warned = '0' WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);

}

if (empty($_POST["warn_msg"]))
sql_query("INSERT INTO messages (sender, receiver, msg, added, subject) VALUES (0, ".sqlesc($userid).", ".sqlesc($msg).", ".sqlesc(get_date_time()).", ".sqlesc($subject).")") or sqlerr(__FILE__, __LINE__);

} elseif (!empty($warnlength) && !empty($warnpm)){

if ($warnlength == 255) {

$modcomment = date("Y-m-d")." - ������������ ������������� ".$CURUSER['username'].".\n�������: ".$warnpm."\n".$modcomment;
$msg = ("�� �������� �������������� �� ������������� ����".($warnpm ? "\n�������: ".$warnpm : "")."\n\n[b]��������[/b]: [i]���� � ��� ����� ������ 5 �������������� ([url=mybonus.php]������� ��������������[/url]) �� ��� ������������� �������� �������.[/i]");

$updateset[] = "warneduntil = '0000-00-00 00:00:00'";
$updateset[] = "num_warned = ".sqlesc($num_warned);

} else {

$warneduntil = get_date_time(gmtime() + $warnlength * 604800);
$dur = $warnlength." �����".($warnlength > 1 ? "�" : "�");

$msg = ("�� �������� �������������� �� ".$dur."". ($warnpm ? "\n�������: ".$warnpm : "")."\n\n[b]��������[/b]: [i]���� � ��� ����� ������ 5 �������������� ([url=mybonus.php]������� ��������������[/url]) �� ��� ������������� �������� �������.[/i]");

$modcomment = date("Y-m-d")." - ������������ �� ".$dur." ".$CURUSER['username'].".\n�������: ".$warnpm."\n".$modcomment;

$updateset[] = "warneduntil = ".sqlesc($warneduntil);
$updateset[] = "num_warned = ".sqlesc($num_warned);

}

if (empty($_POST["warn_msg"])){

$subject = ("�� �������� ��������������");
sql_query("INSERT INTO messages (sender, receiver, msg, added, subject) VALUES (0, ".sqlesc($userid).", ".sqlesc($msg).", ".sqlesc(get_date_time()).", ".sqlesc($subject).")") or sqlerr(__FILE__, __LINE__);
$updateset[] = "unread = unread+1";

}
$updateset[] = "warned = 'yes'";
}


// ������� ������
if ($uploadpos <> $curuploadpos) {

if ($uploadpos == 'yes') {

$modcomment = gmdate("Y-m-d")." - ������� ��������� ��������� ������������� ".$CURUSER['username'].".\n".$modcomment;
$msg = ("��� ��� �� ����������� ����� ��������� ��� ����. �� ����� ������ ���������� ��������.");

sql_query("INSERT INTO messages (sender, receiver, msg, added) VALUES (0, ".sqlesc($userid).", ".sqlesc($msg).", ".sqlesc(get_date_time()).")") or sqlerr(__FILE__, __LINE__);

} else {

$modcomment = gmdate("Y-m-d")." - ��������� ���������� �������� ������������� ".$CURUSER['username'].".\n".$modcomment;
$msg = ("��� ��������� ���������� ��������, ������ ����� ���� ��� �� �������� �� ������������ ���������� ���������.");

sql_query("INSERT INTO messages (sender, receiver, msg, added) VALUES (0, ".sqlesc($userid).", ".sqlesc($msg).", ".sqlesc(get_date_time()).")") or sqlerr(__FILE__, __LINE__);
}

$updateset[] = "unread = unread+1";

}
// ������� ������



if (get_user_class() >= UC_MODERATOR){

if (!empty($_POST["parked"]) && $_POST["parked"] == 'yes' && $arr["parked"] == 'no'){

$updateset[] = "parked = 'yes'";
$modcomment = gmdate("Y-m-d")." - ����������� ������������� ".$CURUSER['username'].".\n".$modcomment;

} elseif (!empty($_POST["parked"]) && $_POST["parked"] == 'no' && $arr["parked"] == 'yes') {

$updateset[] = "parked = 'no'";
$modcomment = gmdate("Y-m-d")." - ����� ����������� ������������� ".$CURUSER['username'].".\n".$modcomment;

}

//// ������� ������������ � ���������
if (!empty($_POST["hidecomment"]) && $_POST["hidecomment"] == 'yes' && $arr["hidecomment"] == 'no')
$updateset[] = "hidecomment = 'yes'";
elseif (!empty($_POST["hidecomment"]) && $_POST["hidecomment"] == 'no' && $arr["hidecomment"] == 'yes')
$updateset[] = "hidecomment = 'no'";

}


// ������� - 100
if (!empty($hiderating) && $curhiderating <> $hiderating){

$updateset[] = "hiderating = ".sqlesc($hiderating);
$updateset[] = "hideratinguntil = '0000-00-00 00:00:00'";

if ($hiderating == 'no')
$modcomment = gmdate("Y-m-d")." - ��� ������������ ������� �������� ".$CURUSER['username'].".\n". $modcomment;

} elseif (!empty($hidelength)) {

if ($hidelength == 255) {
$modcomment = gmdate("Y-m-d")." - ��� ������������ ������� ������� ".$CURUSER['username'].".\n".$modcomment;

$updateset[] = "hideratinguntil = '0000-00-00 00:00:00'";

} else {

$dur = $hidelength." �����".($hidelength > 1 && $hidelength < 5 ? "�" : "��");
$modcomment = gmdate("Y-m-d")." - ��� ������������ ������� �� $dur �� ".$CURUSER['username'] .".\n". $modcomment;
$updateset[] = "hideratinguntil = ".sqlesc(get_date_time(gmtime() + $hidelength * 2678400));

}

$updateset[] = "hiderating = 'yes'";
}


//// ����� ���

$warnfban = (isset($_POST["forum_pos"]) ? (int) $_POST["forum_pos"]:0);

if (!empty($warnfban) && $arr["forum_com"] == "0000-00-00 00:00:00"){
if ($warnfban == 64) {

$modcomment = gmdate("Y-m-d")." - ��������� ��� �� ����� ����� ��� �� ".$CURUSER['username'].".\n".$modcomment;
$updateset[] = "forum_com = ".sqlesc(get_date_time(gmtime() + 777 * 666666));

} else {

$forum_dur = $warnfban." �����".($warnfban > 1 ? "�" : "�");
$modcomment = gmdate("Y-m-d")." - ����� ��� �� ".$forum_dur." �� ".$CURUSER['username'] .".\n". $modcomment;
$updateset[] = "forum_com = ".sqlesc(get_date_time(gmtime() + $warnfban * 604800));
}

}

$fban = (isset($_POST["forum_pos_off"]) ? (int) $_POST["forum_pos_off"]:0);

if (!empty($fban) && $arr["forum_com"] <> "0000-00-00 00:00:00"){

$modcomment = gmdate("Y-m-d")." - ����� ��� ���� ".$CURUSER['username'].".\n".$modcomment;
$updateset[] = "forum_com = '0000-00-00 00:00:00'";

}
//// ����� ���


if ($CURUSER["id"] <> $userid && $enabled <> $arr["enabled"]) {

$disreason = (isset($_POST["disreason"]) ? htmlspecialchars_uni($_POST["disreason"]): false);
$enareason = (isset($_POST["enareason"]) ? htmlspecialchars_uni($_POST["enareason"]): false);

if ($enabled == 'yes' && $enareason <> false && !empty($enareason)) {

$modcomment = date("Y-m-d")." - ������� ������������� ".$CURUSER['username'].".\n�������: ".$enareason."\n".$modcomment;
write_log($arr["username"]." ��� ������� ������������� ".$CURUSER['username'].". �������: ".$enareason, "006800", "bans");

} elseif ($enabled == 'no' && $disreason <> false && !empty($disreason)) {

$modcomment = date("Y-m-d")." - �������� ������������� ".$CURUSER['username'].".\n�������: ".$disreason."\n".$modcomment;
write_log($arr["username"]." ��� �������� ������������� ".$CURUSER['username'].". �������: ".$disreason, "bb3939", "bans");

}

$updateset[] = "enabled = ".sqlesc($enabled);

}


////////////////
if (get_user_class() > UC_ADMINISTRATOR) {

/// ����������� ���������
if (!empty($_POST["trun_torrent"]))	{
$num_com = number_format(get_row_count("comments", "WHERE offer = '0' AND torrent <> '0' AND user = ".sqlesc($userid)));
if (!empty($num_com)){
sql_query("DELETE FROM comments WHERE offer = '0' and torrent <> '0' and user = ".sqlesc($userid));

$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �������� ��� ������� ����������� (".$num_com.").\n".$modcomment;
}
}

if (!empty($_POST["trun_reqoff"])){ /// ����������� ��������
$num_com = number_format(get_row_count("comments", "WHERE torrent = '0' AND offer <> '0' AND user = ".sqlesc($userid)));
if (!empty($num_com)){
sql_query("DELETE FROM comments WHERE torrent = '0' AND offer <> '0' AND user = ".sqlesc($userid));
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �������� ��� ������ ����������� (".$num_com.").\n".$modcomment;
}
}

if (!empty($_POST["trun_news"])){ /// ����������� ���������
$num_com = number_format(get_row_count("comments", "WHERE user = ".sqlesc($userid)." and torrent = '0' and poll = '0' and offer = '0'"));
if (!empty($num_com)){
sql_query("DELETE FROM comments WHERE torrent = '0' AND poll = '0' AND offer = '0' AND user = ".sqlesc($userid));
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �������� ��� ��������� ����������� (".$num_com.").\n".$modcomment;
}
}

if (!empty($_POST["trun_polls"])){ /// ����������� �������
$num_com = number_format(get_row_count("comments", "WHERE torrent = '0' AND news = '0' AND offer = '0' AND user = ".sqlesc($userid)));
if (!empty($num_com)){
sql_query("DELETE FROM comments WHERE torrent = '0' and news = '0' and offer = '0' and user = ".sqlesc($userid));
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �������� ��� ����� ����������� (".$num_com.").\n".$modcomment;
}
}

if (!empty($_POST["trun_inbox"])){ /// ��������
$num_com = number_format(get_row_count("messages", "WHERE location = '1' and receiver = ".sqlesc($userid)));
if (!empty($num_com)){
sql_query("DELETE FROM messages WHERE location = '1' and receiver = ".sqlesc($userid));
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �������� ��� �������� �� (".$num_com.").\n".$modcomment;
}
}

if (!empty($_POST["trun_outbox"])){ /// ���������
$num_com = number_format(get_row_count("messages", "WHERE saved = 'yes' AND sender = ".sqlesc($userid)));
if (!empty($num_com)){
sql_query("DELETE FROM messages WHERE saved = 'yes' AND sender = ".sqlesc($userid));
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �������� ��� ��������� �� (".$num_com.").\n".$modcomment;
}
}

}
////////////////

$stylesheet = (string) $_POST["stylesheet"];

if (!is_numeric($stylesheet) && $arr["stylesheet"] <> $stylesheet) {

$res2 = sql_query("SELECT COUNT(*) AS num_themes FROM stylesheets WHERE name = ".sqlesc($stylesheet)) or sqlerr (__FILE__, __LINE__);
$name = mysql_fetch_array($res2);
if (!empty($name["num_themes"]))
$updateset[] = "stylesheet = ".sqlesc($stylesheet);

}

$updateset[] = "uploadpos = ".sqlesc($uploadpos);
$updateset[] = "downloadpos = ".sqlesc($downloadpos);

if (isset($infouser))
$updateset[] = "info = ".sqlesc($infouser);

if (isset($signature))
$updateset[] = "signature = ".sqlesc($signature);

if (isset($donor))
$updateset[] = "donor = ".sqlesc($donor);

if (!empty($_POST["supportfor"]))
$updateset[] = "supportfor = ".sqlesc($supportfor);
elseif ($support == 'no')
$updateset[] = "supportfor = ''";

$updateset[] = "support = ".sqlesc($support);

if ($support <> $arr["support"]){
unsql_cache("block-techpo_guest");
unsql_cache("block-techpo");
}

// ������
$groups = (int) $_POST["groups"];
if (get_user_class() >= UC_ADMINISTRATOR && $arr["groups"] <> $groups)
$updateset[] = "groups = ".sqlesc($groups);
// ������

$updateset[] = "title = ".sqlesc($title);

$website = htmlspecialchars_uni($_POST["website"]);

$purl = parse_url($website);
if (!empty($purl["host"]))
$updateset[] = "website = ".sqlesc((!empty($purl["scheme"]) ? $purl["scheme"]."://":"http://")."".$purl["host"].(!empty($purl["path"]) ? $purl["path"]:"/").(!empty($purl["query"]) ? "?".$purl["query"]:""));
else
$updateset[] = "website = ''";


if (!empty($_POST['resetthemes']) && $_POST['resetthemes'] == 'yes'){

$updateset[] = "stylesheet = ".sqlesc($default_theme);
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." ������� ���� �� ".$default_theme.".\n".$modcomment;

}

$amountbonus = (isset($_POST["amountbonus"]) ? (float) $_POST["amountbonus"]:0);
if (!empty($amountbonus) && get_user_class() >= UC_ADMINISTRATOR){

$bonuschange = ((isset($_POST["bonuschange"]) && $_POST["bonuschange"] == "plus") ? "plus":"minus");

if ($bonuschange == "plus"){

$updateset[] = "bonus = bonus+".sqlesc($amountbonus);
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �������: ".$amountbonus." ����� (��).\n". $modcomment;

} elseif ($bonuschange == "minus" && !empty($arr["bonus"]) && ($arr["bonus"]-$amountbonus) >= 0){

$updateset[] = "bonus = bonus-".sqlesc($amountbonus);
$modcomment = date("Y-m-d")." - ".$CURUSER["username"]." �����: ".$amountbonus." ����� (��).\n". $modcomment;

}

}

if (!empty($modcomm))
$modcomment = date("Y-m-d")." - ".$tracker_lang['note_is']." ".$CURUSER["username"].": ".$modcomm."\n".$modcomment;

$updateset[] = "modcomment = ".sqlesc(htmlspecialchars_uni($modcomment));

if (!empty($_POST['resetkey'])) {

$passkey = md5($CURUSER['username'].get_date_time().$CURUSER['passhash']);
$updateset[] = "passkey = ".sqlesc($passkey);

}

//��������
if (!empty($_POST['resetavatar']) && !empty($arr["avatar"])){
@unlink(ROOT_PATH."/pic/avatar/".$arr["avatar"]);
$updateset[] = "avatar = ''";
}

$usercom = htmlspecialchars_uni($_POST["usercomment"]);

if (get_user_class() > UC_MODERATOR && $usercomment <> $usercom)
$updateset[] = "usercomment = ".sqlesc($usercom);

sql_query("UPDATE users SET	".implode(", ", $updateset)." WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$userid);

$delreason = htmlspecialchars($_POST["delreason"]);

if (!empty($_POST["deluser"]) && !empty($delreason)) {

if ($userid == $CURUSER["id"])
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

sql_query("DELETE FROM cheaters WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM users WHERE id = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM messages WHERE receiver = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM karma WHERE user = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM friends WHERE friendid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM bookmarks WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM invites WHERE inviter = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM peers WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM readposts WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM report WHERE userid = ".sqlesc($userid)." OR usertid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM simpaty WHERE fromuserid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM pollanswers WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM shoutbox WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM ratings WHERE user = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM snatched WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM thanks WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM checkcomm WHERE userid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM sessions WHERE uid = ".sqlesc($userid)) or sqlerr(__FILE__,__LINE__);

if (!empty($arr["avatar"]))
@unlink(ROOT_PATH."/pic/avatar/".$arr["avatar"]);

@unlink(ROOT_PATH."/cache/monitoring_".$userid.".txt");

$comment = $delreason." (".$arr["username"].")";

if (!empty($_POST["banemailu"]))
sql_query("INSERT INTO bannedemails (added, addedby, comment, email) VALUES (".sqlesc(get_date_time()).", 92, ".sqlesc($comment).", ".sqlesc($arr["email"]).")");

write_log("������������ ".$arr["username"]." (".$arr["email"].") ��� ������ ������������� ".$CURUSER["username"].". �������: ".$delreason,"590000","bans");

unsql_cache("bans_first_last");

stderr($tracker_lang['success'], "������������ ������, ��� ����������� ������ ���� ��������� � ������.");

} else {

header("Refresh: 0; url=userdetails.php?id=".$userid);
die;
}


?>