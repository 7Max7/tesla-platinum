<?
require "include/bittorrent.php";
gzip();
dbconn(false);

loggedinorreturn();

if (get_user_class() <= UC_MODERATOR) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

$page = (isset($_GET["page"]) ? (int) $_GET["page"]:0);

if (get_user_class() == UC_SYSOP && isset($_POST["delmp"]) && is_array($_POST["delmp"])) {

accessadministration();

$link = "msg.php?page=".$page;

sql_query("DELETE FROM messages WHERE id IN (".implode(", ", array_map("intval", $_POST["delmp"])).")");

@header("Location: ".$link) or die("<script>setTimeout('document.location.href=\"".$link."\"', 10);</script>");

}


$send = (int) $_GET["send"];
$receiv = (int) $_GET["receiv"];

if (!empty($receiv) && empty($send)){

$on_main = "<tr><td colspan=\"7\" align=\"center\" class=\"b\"><form action=\"msg.php\"><input style=\"height: 25px; width:200px\" class=\"btn\" type=\"submit\" value=\"".$tracker_lang['all_msg']."\"></form></td></tr>";

$rclastd = " class=\"a\" ";

$for_pagers = "?receiv=".$receiv."&";

$sql_rs = "WHERE m.receiver = ".sqlesc($receiv);

} elseif (!empty($send) && empty($receiv)){

$on_main = "<tr><td colspan=\"7\" align=\"center\" class=\"b\"><form action=\"msg.php\"><input type=\"hidden\" name=\"action\" value=\"on_main\"><input type=\"submit\" value=\"".$tracker_lang['all_msg']."\" class=\"btn\"></form></td></tr>";

$sclastd=" class=\"a\" ";

$for_pagers="?send=".$send."&";

$sql_rs = "WHERE m.sender = ".sqlesc($send);

} elseif (isset($_GET['search'])){



$search = $origsearch = htmlspecialchars_uni($_GET['search']);
$search = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", $search));

$sear = array("'","\"","%","$","/","`","`","<",">",",");
$search = str_replace($sear, " ", $search);

$list = explode(" ", $search);

$listrow = $listview = array();

$num = 0;
foreach ($list AS $lis){
$strl = strlen($lis);

if ($strl >= 3) {

if ($num < 5) $listrow[] = "+".$lis; 
else $listrow[] = $lis;

$listview[] = $lis; /// ��������
}

++$num;
}

$view_uniq = trim(implode(" ", array_unique($listrow))); /// ������� ���������


if (!empty($origsearch)){
$for_pagers="?search=".$origsearch."&";
$sql_rs = "WHERE MATCH (m.msg) AGAINST ('".$view_uniq."' IN BOOLEAN MODE)";
} else $for_pagers = "?";


} else {

$for_pagers = "?";

unset($sql_rs);
unset($send);
unset($receiv);
}

$sclastd = " class=\"b\" ";
$rclastd = " class=\"b\" ";


$count = get_row_count("messages AS m", $sql_rs);


if (empty($count)){
stderr($tracker_lang['error'], $tracker_lang['no_data_now']);
die();
}

$perpage = 60;

list ($pagertop, $pagerbottom, $limit) = pager($perpage, $count, "msg.php".$for_pagers);

stdhead($tracker_lang['all_msg'], true);

$res = sql_query("SELECT m.*, s.last_access AS sender_l, s.id AS sender_id ,s.username AS sender_u,s.class AS sender_c, r.last_access AS receiver_l, r.id AS receiver_id, r.username AS receiver_u, r.class AS receiver_c 
FROM messages AS m
LEFT JOIN users AS s ON m.sender = s.id
LEFT JOIN users AS r ON m.receiver = r.id 
".$sql_rs."
ORDER BY m.id DESC ".$limit) or sqlerr(__FILE__, __LINE__);

echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">\n");


echo "<tr><td colspan=\"7\" align=\"center\" class=\"b\"><form action=\"msg.php\" method=\"get\">";
echo "<input type=\"text\" name=\"search\" size=\"50\" class=\"searchgif\" value=\"".htmlspecialchars($origsearch)."\" />&nbsp; <input type=\"submit\" class=\"btn\" value=\"������\" /> <input type=\"reset\" class=\"btn\" value=\"".$tracker_lang['reset']."\" /> ";
echo "</form></td></tr>";



if (!empty($origsearch)){

echo "<tr><td colspan=\"7\" class=\"a\" align=\"center\"><strong>".$tracker_lang['arr_searched']."</strong>: ".$origsearch." (<a class=\"alink\" href=\"msg.php\">".$tracker_lang['reset']."</a>)</td></tr>";

if (!empty($view_uniq) && !empty($count))
echo "<tr><td colspan=\"7\" class=\"a\" align=\"center\"><strong>".$tracker_lang['words_in_search']."</strong>: ".implode(", ", $listview)."</td></tr>";

}

if ($count > $perpage)
echo ("<tr><td class=\"a\" align=\"center\" colspan=\"7\">".$pagertop."</td></tr>\n");

if (get_user_class() == UC_SYSOP) {

echo '<script language="Javascript" type="text/javascript">
var checkflag = "false";
var marked_row = new Array;
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
} else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
}
}
</script>';

echo "<form method=\"post\" name=\"yepi\" action=\"msg.php?page=".$page."\">";
}

echo ("<tr>
<td class=\"colhead\" align=\"center\">".$tracker_lang['sender']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['receiver']."</td>
<td class=\"colhead\" align=\"center\">UR</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['subject']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['description']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['date']."</td>
".(get_user_class() == UC_SYSOP ? "<td class=\"colhead\" align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['mark_all']."\" value=\"".$tracker_lang['mark_all']."\" onclick=\"this.value=check(document.yepi.elements);\"></td>":"")."
</tr>\n");

while ($arr = mysql_fetch_assoc($res)) {

if (!empty($arr["receiver_u"]))
$receiver = "<a href=\"userdetails.php?id=".$arr["receiver_id"]."\"><b>".get_user_class_color($arr["receiver_c"], $arr["receiver_u"])."</b></a>";
else
$receiver = "<font color=\"red\">[<b>id ".$arr["receiver_id"]."</b>]</font><br /><b>".$tracker_lang['anonymous']."</b>";


$receiver.= "<br /><a href=\"msg.php?receiv=".$arr["receiver_id"]."\"><img src=\"/pic/mail-markread.gif\" title=\"".$tracker_lang['outbox']."\"></a>";

$last_access_r = $arr["receiver_l"];
if ($arr["receiver_l"] == "0000-00-00 00:00:00")
$last_access_r = $tracker_lang['never'];

$se = "<br /><a href=\"msg.php?send=".$arr["sender_id"]."\"><img src=\"/pic/mail-send.gif\" title=\"".$tracker_lang['outbox']."\" /></a>";

////////////// ��� ������ ���������
if (!empty($arr["sender_u"]))
$sender = "<a href=\"userdetails.php?id=".$arr["sender_id"]."\"><b>".get_user_class_color($arr["sender_c"], $arr["sender_u"])."</b></a>".$se;
else
$sender = "<font color=\"red\">[<b>id ".$arr["sender_id"]."</b>]</font><br /><b>".$tracker_lang['anonymous']."</b>".$se;

if (empty($arr["sender"]))
$sender = "<font color=\"gray\">[<b>".$tracker_lang['from_system']."</b>]</font>";

$msg = format_comment($arr["msg"]);

$last_access_s = $arr["sender_l"];
if ($arr["sender_l"] == "0000-00-00 00:00:00")
$last_access_s = $tracker_lang['never'];

$added = $arr["added"];

if ($arr["receiver_l"] > $arr["added"])
$added = "<i>".$added."</i>";

$subject = htmlspecialchars_uni($arr["subject"]);

if ($CURUSER["id"] == $arr["receiver"] && !empty($arr["sender"]) && $arr["unread"] == "yes")
$subject = "<a href=\"message.php?action=sendmessage&receiver=".$arr["sender"]."&replyto=".$arr["id"]."\">".$subject."</a>";

if (empty($arr["subject"]) || $arr["subject"] == "Re:")
$subject = "<b>N/A</b>";

if ($arr["unread"] <> "yes")
$newmessageview = "<img style=\"border:none\" alt=\"".$tracker_lang['mail_read']."\" title=\"".$tracker_lang['mail_read']."\" src=\"pic/ok.gif\" />";
else
$newmessageview = "<img style=\"border:none\" alt=\"".$tracker_lang['mail_unread']."\" title=\"".$tracker_lang['mail_unread']."\" src=\"pic/error.gif\" />";

if (($arr["sender_c"] <= get_user_class() && $arr["receiver_c"] <= get_user_class()) || ($arr["sender"] == $CURUSER["id"] || $arr["receiver"] == $CURUSER["id"])){

echo "<tr>";
echo "<td ".$sclastd." align=\"center\">".$sender."<br />".$last_access_s."</td>
<td ".$rclastd." align=\"center\">".$receiver."<br />".$last_access_r."</td>
<td class=\"b\">".$newmessageview."</td>
<td class=\"b\">".$subject."</td>
<td class=\"b\" align=\"left\">".$msg."</td>
<td class=\"b\" align=\"center\">".$added."</td>";

if (get_user_class() == UC_SYSOP)
echo "<td class=\"a\" align=\"center\"><INPUT type=\"checkbox\" id=\"checkbox_tbl_".$id."\" name=\"delmp[]\" value=\"".$arr['id']."\"><br />".$arr["id"]."</td>";

echo "</tr>";

}

}


if ($count > $perpage)
echo ("<tr><td class=\"a\" align=\"center\" colspan=\"7\">".$pagerbottom."</td></tr>\n");


if (get_user_class() == UC_SYSOP) {
echo "<tr><td class=\"a\" align=\"right\" colspan=\"7\"><input type=\"submit\" style=\"height: 25px; width:200px\" class=\"btn\" value=\"".$tracker_lang['delete_marked_messages']."\"/></td></tr>";

echo "</form>";
}

echo $on_main;

echo "</table>";


stdfoot(true);
?>