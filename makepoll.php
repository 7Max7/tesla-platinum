<?
require "include/bittorrent.php";
dbconn();
loggedinorreturn();

if (get_user_class() <= UC_MODERATOR) {
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

/**
 * @author 7Max7
 * @copyright 2012
 */

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$pollid = (int) $_POST["pollid"];
$act = (string) $_POST["action"];

if ($act == 'edit' && !is_valid_id($pollid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$question = htmlspecialchars($_POST["question"]);

$array_opt = $array_upd = array();

for ($i = 0; $i < 20; $i++) {
$opti = htmlspecialchars_uni($_POST['option'.$i]);
$array_upd[] = "option".$i." = ".sqlesc($opti);
$array_opt[] = sqlesc($opti);
}

$returnto = htmlentities($_POST["returnto"]);

if (empty($question) || empty($_POST["option0"]) || empty($_POST["option1"]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$tracker_lang['question']." ".$tracker_lang['or']." ".$tracker_lang['option']."1 ".$tracker_lang['or']." ".$tracker_lang['option']."2");

if (!empty($pollid) && count($array_upd)){

$array_upd[] = "editby = ".sqlesc($CURUSER["id"]);
$array_upd[] = "edittime = ".sqlesc(get_date_time());
$array_upd[] = "question = ".sqlesc($question);
$array_upd[] = "sort = ".sqlesc($_POST["sort"] == "yes" ? "yes":"no");
$array_upd[] = "comment = ".sqlesc($_POST["comme"] == "yes" ? "yes":"no");

sql_query("UPDATE polls SET ".implode(", ", $array_upd)." WHERE forum = '0' AND id = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);

if (mysql_modified_rows())
write_log($CURUSER["username"]." �������������� ����� ".$question." (# ".$pollid.")", "9C9898", "other");

} elseif (count($array_opt) && empty($pollid)){

sql_query("INSERT INTO polls VALUES(0, '', '', ".sqlesc($CURUSER["id"]).", ".sqlesc(get_date_time()).", ".sqlesc($question).", ".implode(", ", $array_opt).", ".sqlesc($_POST["sort"] == "yes" ? "yes":"no").", ".sqlesc($_POST["comme"] == "yes" ? "yes":"no").", '0')") or sqlerr(__FILE__, __LINE__);

write_log($CURUSER["username"]." ������ ����� ".$question." (# ".mysql_insert_id().")", "3CAB54", "other");

}

if (empty($pollid)) $newid = mysql_insert_id();
else $newid = $pollid;

unsql_cache("poll_comment_".$newid); // �����������
unsql_cache("polls_".$newid); // ������ ������

if ($returnto == "main" || empty($pollid))
header("Location: ".$DEFAULTBASEURL);
else
header("Location: ".$DEFAULTBASEURL."/makepoll.php?action=edit&pollid=".$pollid."&returnto=main");

die;
}

/// ����� ���� �������� ��� �������������� ������ ///


$act = (string) $_GET["action"];
$pollid = (int) $_GET["pollid"];


if ($act == "edit") {

stdhead($tracker_lang['edit'].": ".$tracker_lang['polls']);

if (!is_valid_id($pollid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT * FROM polls WHERE forum = '0' AND id = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_poll_with_such_id']);

$poll = mysql_fetch_array($res);

if (!empty($poll["createby"]))
$res_c = sql_query("SELECT username, class FROM users WHERE id = ".sqlesc($poll["createby"])) or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($res) <> 0){
$uce = mysql_fetch_array($res_c);
$ucreat = "<a href=\"userdetails.php?id=".$poll["createby"]."\">".get_user_class_color($uce["class"], $uce["username"])."</a>";
}

if (!empty($poll["editby"]))
$res_e = sql_query("SELECT username, class FROM users WHERE id = ".sqlesc($poll["editby"])) or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($res_e) <> 0){
$ue = mysql_fetch_array($res_e);
$ueditby = "<a href=\"userdetails.php?id=".$poll["editby"]."\">".get_user_class_color($ue["class"], $ue["username"])."</a>";
}

}

stdhead($tracker_lang['creating_polls']);

echo "<form method=\"post\" action=\"makepoll.php\">";
echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

if (!empty($pollid)) {

echo "<tr><td class=\"b\" colspan=\"2\">
".(!empty($ucreat) ? $tracker_lang['is_creat_post'].": <b>".$ucreat."</b> ".$tracker_lang['in']." <b>".$poll["added"]."</b>":"")."
<br />
".(!empty($ueditby) ? $tracker_lang['editing'].": <b>".$ueditby."</b> ".$tracker_lang['in']." <b>".$poll["edittime"]."</b>":"")."
</td></tr>";

} else {

$res = sql_query("SELECT question, added FROM polls WHERE forum = '0' ORDER BY added DESC LIMIT 1") or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res);
if ($arr) {
$hours = floor((gmtime() - sql_timestamp_to_unix_timestamp($arr["added"])) / 3600);
$days = floor($hours / 24);
if ($days < 3) {
$hours -= $days * 24;

if ($days)
$t = $days." ".($days > 1 ? $tracker_lang['day_all']:$tracker_lang['day_one']);
else
$t = $hours." ".($hours > 1 ? $tracker_lang['hours']:$tracker_lang['hour']);

echo "<tr><td class=\"b\" colspan=\"2\" align=\"center\"><p>".$tracker_lang['warning'].": ".sprintf($tracker_lang['poll_exis'], "<b>".htmlspecialchars_uni($arr["question"])."</b>", $t)."</p></td></tr>";
}

}

}

echo "<tr><td class=\"a\" colspan=\"2\" align=\"center\">".$tracker_lang['may_bbcode_used']."</td></tr>";

echo "<tr><td class=\"a\">".$tracker_lang['question']." <font color=\"red\">*</font></td><td align=\"left\"><input name=\"question\" size=\"80\" maxlength=\"255\" value=\"".$poll['question']."\"> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>";

for ($i = 0; $i < 20; $i++) {
echo "<tr><td class=\"a\">".$tracker_lang['option']." ".($i+1)." ".($i < 2 ? "<font color=\"red\">*</font>":"")."</td><td align=\"left\"><input name=\"option".$i."\" size=\"80\" maxlength=\"255\" value=\"".$poll['option'.$i]."\"> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>";
}

echo "<tr><td class=\"a\">".$tracker_lang['sorting']."</td><td>
<label><input type=\"radio\" name=\"sort\" value=\"yes\" ".($poll["sort"] != "no" ? "checked" : "").">".$tracker_lang['allow']."</label>
<label><input type=\"radio\" name=\"sort\" value=\"no\" ".($poll["sort"] == "no" ? "checked" : "")."> ".$tracker_lang['deny']."</label>
</td></tr>";

echo "<tr><td class=\"a\">".$tracker_lang['block_comments']."</td><td>
<label><input type=\"radio\" name=\"comme\" value=\"yes\" ".($poll["comment"] != "no" ? "checked" : "").">".$tracker_lang['allow']."</label>
<label><input type=\"radio\" name=\"comme\" value=\"no\" ".($poll["comment"] == "no" ? "checked" : "")."> ".$tracker_lang['deny']."</label>
</td></tr>";

echo "<tr><td class=\"a\" colspan=\"2\" align=\"center\"><input class=\"btn\" type=\"submit\" value=\"".($pollid ? $tracker_lang['edit']:$tracker_lang['create'])."\"></td></tr>";

echo "
<input type=\"hidden\" name=\"pollid\" value=\"".$poll["id"]."\"/>
<input type=\"hidden\" name=\"action\" value=\"".($pollid ? 'edit' : 'create')."\"/>
<input type=\"hidden\" name=\"returnto\" value=\"".htmlentities($_POST["returnto"])."\"/>
";

echo "</table>";
echo "</form>";

stdfoot(); 

?>