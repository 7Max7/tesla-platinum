<?
require "include/bittorrent.php";
dbconn(true);

loggedinorreturn;

if (get_user_class() < UC_MODERATOR){
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die;
}

stdhead($tracker_lang['bl_uploader']);

$count = get_row_count("users", "WHERE class IN (3)");
if (empty($count))
stderr($tracker_lang['bl_uploader'], $tracker_lang['no_data_now']);


list ($pagertop, $pagerbottom, $limit) = pager(100, $count, "uploaders.php?");

$result = sql_query("SELECT u.id, u.username, u.class, u.added, u.uploaded, u.downloaded, u.donor, u.warned, torrents.added AS date
FROM users AS u
LEFT JOIN torrents ON torrents.owner = u.id
WHERE u.class IN (3) ".$limit) or sqlerr(__FILE__, __LINE__);

echo ("<table cellpadding=\"2\" cellspacing=\"1\" border=\"0\" width=\"100%\">");

if (!empty($count))
echo ("<tr><td colspan=\"7\">".$pagertop."</td></tr>");

echo "<tr>";
echo "<td align=\"center\" class=\"colhead\">#</td>";
echo "<td align=\"left\" class=\"colhead\">".$tracker_lang['signup_username']." / ".$tracker_lang['ratio']."</td>";

echo "<td align=\"center\" class=\"colhead\">".$tracker_lang['torrents']." / ".$tracker_lang['added']."</td>";

echo "<td align=\"center\" class=\"colhead\">".$tracker_lang['sendmessage']."</td>";
echo "</tr>";

$i2 = (isset($_GET["page"]) ? (($_GET["page"]-1)*100+1):1);

while ($arr = mysql_fetch_assoc($result)) {

if ($i2%2==1){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

if (!empty($arr["downloaded"]))
$ratio = number_format($arr["uploaded"] / $arr["downloaded"], 3);
else
$ratio = "---";

$numtorrents = get_row_count("torrents", "WHERE owner = ".sqlesc($arr["id"]));

echo "<tr>";
echo "<td ".$cl1." align=\"center\">".$i2."</td>";

echo "<td ".$cl2." align=\"left\"><a href=\"userdetails.php?id=".$arr["id"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a> ".($arr["donor"] == "yes" ? "<img src=\"/pic/star.gif\">":"")." ".($arr["warned"] == "yes" ? "<img src=\"/pic/warned8.gif\">":"")." (".$ratio.")<br />".mksize($arr["uploaded"])." / ".mksize($arr["downloaded"])."</td>";

echo "<td ".$cl1." align=\"center\">".number_format($numtorrents)."<br/>".(!empty($arr["date"]) ? get_elapsed_time(sql_timestamp_to_unix_timestamp($arr["date"]))." ".$tracker_lang['ago']." (".$arr["date"].")":"")."</td>";

echo "<td ".$cl1." align=\"center\"><a href=\"message.php?action=sendmessage&receiver=".$arr['id']."\"><img src=\"./pic/button_pm.gif\" border=\"0\" alt=\"".$tracker_lang['sendmessage']."\"></a></td>";

echo "</tr>";

++$i2;
}

if (!empty($count))
echo ("<tr><td colspan=\"7\">".$pagerbottom."</td></tr>");

echo "</table>";

stdfoot();

?>