<?
require_once("include/bittorrent.php");
dbconn(false, true);


if (get_user_class() >= UC_ADMINISTRATOR && !count($_GET)) {

require_once(ROOT_PATH.'/include/cleanup.php');

$s_s = $queries;
docleanup();
$s_e = $queries;

$num = $s_e - $s_s;

sql_query("UPDATE avps SET value_i = ".sqlesc($num).", value_s = ".sqlesc(get_date_time(gmtime()))." WHERE arg = 'lastcleantime'");

stdhead($tracker_lang['docleanup_act'], true);

stdmsg($tracker_lang['warning'], $tracker_lang['docleanup_act'].". ".$tracker_lang['number_all'].": ".($s_e - $s_s)." (sql).");

}




if (count($_GET)) {

stdhead($tracker_lang['docleanup_act'], true);


if (isset($_GET["a"])){

$num = 0;
$file_size = 0;
$dh = opendir(ROOT_PATH.'/cache/');
while ($file = readdir($dh)) :
if (preg_match('/^(.+)\.$/si', $file, $matches))
$file = $matches[1];
$file_end = end(explode('.', $file));

$txt_main = array('info_cache_stat.txt', 'sqlerror.txt', 'error_torrent.txt', 'my_license.txt');

if ($file_end == 'txt' && !stristr($file,'monitoring_') && !in_array($file, $txt_main)){
$file_size += filesize(ROOT_PATH."/cache/".$file);
@unlink(ROOT_PATH."/cache/".$file);
++$num;
}

endwhile;
closedir($dh);

stdmsg($tracker_lang['warning'], $tracker_lang['trun_tempfile']." (<strong>/cache/</strong>). ".$tracker_lang['all_size'].": ".mksize($file_size)." (".$num.")");

}

if (isset($_GET["b"])){

include(ROOT_PATH."/include/passwords.php");

$result = mysql_query("SHOW TABLE STATUS FROM ".$Mysql_Config['db']); unset($Mysql_Config);
$array = array();
while ($row = mysql_fetch_array($result)) {
$res_t = mysql_query("ANALYZE TABLE `".$row[0]."`");
$row_t = mysql_fetch_assoc($res_t); ///Table Op Msg_type Msg_text

if ($row_t["Msg_type"] <> "status"){
$rresult_1 = mysql_query("REPAIR TABLE `".$row[0]."` EXTENDED");
$rresult_2 = mysql_query("REPAIR TABLE `".$row[0]."` USE_FRM");

if (!empty($rresult_1) || !empty($rresult_2))
$array[] = $row[0];
}
}

stdmsg($tracker_lang['warning'], $tracker_lang['mysql_repair'].". ".(count($array) ? $tracker_lang['number_all'].": ".implode(", ",$array):""));

}

if (isset($_GET["t"])){

$num = 0;
$file_size = 0;
$dh = opendir(ROOT_PATH.'/torrents/txt/');
while ($file = readdir($dh)) :
if (preg_match('/^(.+)\.$/si', $file, $matches))
$file = $matches[1];
$file_end = end(explode('.', $file));

if ($file_end <> '.htaccess'){
$file_size += filesize(ROOT_PATH."/torrents/txt/".$file);
@unlink(ROOT_PATH."/torrents/txt/".$file);
++$num;
}

endwhile;
closedir($dh);

stdmsg($tracker_lang['warning'], $tracker_lang['trun_tempfile']." (<strong>/torrents/txt/</strong>). ".$tracker_lang['all_size'].": ".mksize($file_size)." (".$num.")");

}

if (isset($_GET["l"])){
@unlink(ROOT_PATH."/cache/admincp_news");
@unlink(ROOT_PATH."/cache/my_license.txt");
}

if (isset($_GET["d"])){

$num = 0;
$file_size = 0;
$dh = opendir(ROOT_PATH.'/torrents/thumbnail/');
while ($file = readdir($dh)) :
if (preg_match('/^(.+)\.$/si', $file, $matches))
$file = $matches[1];
$file_end = end(explode('.', $file));

if ($file_end <> '.htaccess'){
$file_size += filesize(ROOT_PATH."/torrents/thumbnail/".$file);
@unlink(ROOT_PATH."/torrents/thumbnail/".$file);
++$num;
}

endwhile;
closedir($dh);

stdmsg($tracker_lang['warning'], $tracker_lang['trun_tempfile']." (<strong>/torrents/thumbnail/</strong>). ".$tracker_lang['all_size'].": ".mksize($file_size)." (".$num.")");

}


}

stdfoot(true);
?>