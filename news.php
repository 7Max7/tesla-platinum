<?
require "include/bittorrent.php";

dbconn();
loggedinorreturn();

if (get_user_class() < UC_ADMINISTRATOR)
stderr($tracker_lang['error'], $tracker_lang["access_denied"]);

$action = (string) $_GET["action"];
$newsid = (int) $_GET["newsid"];

if (!empty($action) && $action <> 'add' && !is_valid_id($newsid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

$returnto = htmlspecialchars($_GET["returnto"]);

if ($action == 'delete') {

if (!isset($_GET["sure"]))
stderr($tracker_lang['delete']." ".$tracker_lang['news'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $tracker_lang['news'], "news.php?action=delete&newsid=".$newsid."&returnto=".$returnto."&sure=1"));

$res2 = sql_query("SELECT * FROM news WHERE id = ".sqlesc($newsid)) or sqlerr(__FILE__, __LINE__);
$ip2 = mysql_fetch_array($res2);
$question = htmlspecialchars_uni($ip2["subject"]);

/// ����� � ���
write_log($CURUSER["username"]." ������ ������� ".$question." (".$newsid.")\n", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "other");

sql_query("DELETE FROM news WHERE id = ".sqlesc($newsid)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM comments WHERE torrent = '0' AND poll = '0' AND offer = '0' AND news = ".sqlesc($newsid)) or sqlerr(__FILE__, __LINE__);

/// ������� ���
unsql_cache("block-news_admins");
unsql_cache("block-news_guest");
unsql_cache("block-news_users");
/// ������� ���

if (!empty($returnto))
header("Location: ".$returnto);
else
header("Location: news.php");
die;

} elseif ($action == 'add') {

$subject = htmlspecialchars_uni($_POST["subject"]);
if (empty($subject))
stderr($tracker_lang['error'], $tracker_lang['data_noempty'].": ".$tracker_lang['subject']);

$body = htmlspecialchars_uni($_POST["body"]);
if (empty($body))
stderr($tracker_lang['error'], $tracker_lang['data_noempty'].": ".$tracker_lang['news']);

sql_query("INSERT INTO news (userid, added, body, subject) VALUES (".sqlesc($CURUSER['id']).", ".sqlesc(get_date_time()).", ".sqlesc($body).", ".sqlesc($subject).")") or sqlerr(__FILE__, __LINE__);

/// ����� � ���
write_log($CURUSER["username"]." ������� ������� ".$subject." (# ".mysql_insert_id().")", "4AA958", "other");

if (mysql_affected_rows() == 1)	{

/// ������� ���
unsql_cache("block-news_admins");
unsql_cache("block-news_guest");
unsql_cache("block-news_users");
/// ������� ���

if (!empty($returnto))
header("Location: ".$returnto);
else
header("Location: news.php");
die;

}


} elseif ($action == 'edit') {

$res = sql_query("SELECT * FROM news WHERE id = ".sqlesc($newsid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) != 1)
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$newsid);

$arr = mysql_fetch_array($res);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

$subject = htmlspecialchars_uni($_POST["subject"]);
if (empty($subject))
stderr($tracker_lang['error'], $tracker_lang['data_noempty'].": ".$tracker_lang['subject']);

$body = htmlspecialchars_uni($_POST["body"]);
if (empty($body))
stderr($tracker_lang['error'], $tracker_lang['data_noempty'].": ".$tracker_lang['news']);


sql_query("UPDATE news SET body = ".sqlesc($body).", editby = ".sqlesc($CURUSER["id"]).", edittime = ".sqlesc(get_date_time()).", subject = ".sqlesc($subject)." WHERE id = ".sqlesc($newsid)) or sqlerr(__FILE__, __LINE__);

/// ����� � ���
write_log($CURUSER["username"]." �������������� ������� $subject (# $newsid)", "CECECE", "other");

$returnto = htmlentities($_POST['returnto']);

/// ������� ���
unsql_cache("block-news_admins");
unsql_cache("block-news_guest");
unsql_cache("block-news_users");
/// ������� ���

if (!empty($returnto))
header("Location: ".$returnto);
else
header("Location: news.php");
die;

} else {

$returnto = htmlspecialchars($_GET['returnto']);

stdhead($tracker_lang['news']." ".$tracker_lang['editing']);

echo "<form method='post' name='news' action='news.php?action=edit&newsid=".$newsid."'>";
echo "<table border='0' cellspacing='0' cellpadding='5'>";

echo "<tr><td class=colhead>".$tracker_lang['news']." ".$tracker_lang['editing']."</td></tr>";
echo "<tr><td>".$tracker_lang['subject'].": <input type='text' name='subject' maxlength='70' size='50' value=\"".htmlspecialchars($arr["subject"])."\"/></td></tr>";

echo "<tr><td style='padding: 0px'>";
textbbcode("news", "body", htmlspecialchars($arr["body"]));
echo "</textarea></td></tr>";

echo "<tr><td align='center'>
<input type='hidden' name='returnto' value='".$returnto."'>
<input class=\"btn\" type='submit' value='".$tracker_lang['edit']."'>
</td></tr>";
echo "</table>";
echo "</form>";

stdfoot();
die;
}
}

stdhead($tracker_lang['news']);

echo "<form method=\"post\" name=\"news\" action=\"news.php?action=add\">";
echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
echo "<tr><td class=\"colhead\">".$tracker_lang['news']." ".$tracker_lang['create']."</td></tr>";
echo "<tr><td>".$tracker_lang['subject'].": <input type=\"text\" name=\"subject\" maxlength=\"40\" size=\"50\" value=\"".htmlspecialchars($arr["subject"])."\"/></td></tr>";

echo "<tr><td style=\"padding: 0px\">";
textbbcode("news","body","");
echo "</td></tr>";

echo "<tr><td align='center'><input type=\"submit\" value=\"".$tracker_lang['create']."\" class=\"btn\"></td></tr>";
echo "</table></form><br /><br />";


$res = sql_query("SELECT *, (SELECT username FROM users WHERE id = news.userid) AS user, (SELECT class FROM users WHERE id = news.userid) AS classuser 
 FROM news ORDER BY added DESC") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) > 0) {

begin_main_frame();
begin_frame();

while ($arr = mysql_fetch_array($res)){

$newsid = $arr["id"];
$body = $arr["body"];
$subject = $arr["subject"];
$userid = $arr["userid"];

$postername = $arr["user"];
$posterclass = $arr["classuser"];

begin_table(true);

echo "<tr valign=\"top\"><td class=\"colhead\"><b>".$subject."</b> (".(get_elapsed_time(sql_timestamp_to_unix_timestamp($arr["added"])))." ".$tracker_lang['ago'].")</td></tr>";
echo "<tr valign=\"top\"><td class=\"comment\">".format_comment($body)."</td></tr>";

echo "<tr><td class=\"a\">
<b>".$tracker_lang['clock']."</b>: ".$arr["added"]."
".(empty($postername) ? $tracker_lang['anonymous']." [".$userid."]" : "<a href=\"userdetails.php?id=".$userid."\"><b>".get_user_class_color($posterclass, $postername)."</b></a>")."
<a href=\"news.php?action=edit&newsid=".$newsid."\">".$tracker_lang['edit']."</a> <a href=\"news.php?action=delete&newsid=".$newsid."\">".$tracker_lang['delete']."</a>
</td></tr>";

end_table();

}
end_frame();
end_main_frame();
}

else
stderr($tracker_lang['error'], $tracker_lang['no_news']);


stdfoot();
?>