<? 
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

stdhead($tracker_lang['torrent_my']);

$torpage = $CURUSER["torrentsperpage"];
if (empty($torpage)) $torpage = 25;

if (!empty($_GET['sort']) && !empty($_GET['type'])) {

$column = '';
$ascdesc = '';

switch($_GET['sort']) {
case '1': $column = "name"; break;
case '2': $column = "numfiles"; break;
case '3': $column = "comments"; break;
case '4': $column = "added"; break;
case '5': $column = "size"; break;
//case '6': $column = "times_completed"; break;
case '7': $column = "seeders"; break;
case '8': $column = "leechers"; break;
case '9': $column = "owner"; break;
case '10': $column = "moderatedby"; break;
default: $column = "id"; break;
}

switch($_GET['type']) {
case 'asc': $ascdesc = "ASC"; $linkascdesc = "asc"; break;
case 'desc': $ascdesc = "DESC"; $linkascdesc = "desc"; break;
default: $ascdesc = "DESC"; $linkascdesc = "desc"; break;
}

$orderby = "ORDER BY 
".($column=="seeders" ? " (torrents.seeders+torrents.f_seeders) ":"
".($column=="leechers" ? "(torrents.leechers+torrents.f_leechers) ":"torrents." . $column . "")."
")." " . $ascdesc;
$pagerlink = "sort=" . intval($_GET['sort']) . "&type=" . $linkascdesc . "&";
} else {
$orderby = "ORDER BY torrents.sticky ASC, torrents.added DESC";
$pagerlink = "";
}

$where = "WHERE owner = ".$CURUSER["id"]." AND banned <> 'yes'";
$res = sql_query("SELECT COUNT(*) FROM torrents $where") or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($res);
$count = $row[0];

if (!$count) {
stdmsg($tracker_lang['error'], $tracker_lang['user_noupload']);
stdfoot();
die();

} else {

echo "<table class=\"embedded\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";
echo "<tr><td class=\"colhead\" align=\"center\" colspan=\"12\">".$tracker_lang['torrent_my']."</td></tr>";

list ($pagertop, $pagerbottom, $limit) = pager($torpage, $count, "mytorrents.php?");

$res = sql_query("SELECT torrents.comments, (torrents.leechers+torrents.f_leechers) AS leechers, torrents.tags, (torrents.seeders+torrents.f_seeders) AS seeders, IF(torrents.numratings < 1, NULL, ROUND(torrents.ratingsum / torrents.numratings, 1)) AS rating, torrents.id, torrents.name, numfiles, added, size, views, visible, free, hits, times_completed, category FROM torrents
".$where." ".$orderby." ".$limit) or sqlerr(__FILE__, __LINE__);

echo ("<tr><td class=\"index\" colspan=\"12\">".$pagertop."</td></tr>");

torrenttable($res, "mytorrents");

echo ("<tr><td class=\"index\" colspan=\"12\">".$pagerbottom."</td></tr>");

echo ("</table>");
}

stdfoot();

?>