<?
require_once("include/bittorrent.php");

dbconn(false);
loggedinorreturn();
parked();

if (get_user_class() < UC_UPLOADER && get_user_class() <> UC_VIP || $CURUSER["uploadpos"] == 'no')
stderr($tracker_lang['error_data'], $tracker_lang['ban_uploadpos']);

global $Torrent_Config;

$type = (int) $_GET["type"];

$res_cat = sql_query("SELECT name FROM categories WHERE id = ".sqlesc($type)) or sqlerr(__FILE__, __LINE__);

$arr_cat = mysql_fetch_assoc($res_cat);
$cat_name = htmlspecialchars($arr_cat["name"]);

stdhead($tracker_lang['upload_torrent']." / ".$cat_name);

if (!isset($type) || empty($type) || empty($cat_name)){

stdmsg($tracker_lang['error'], $tracker_lang['no_select_cat']);
echo "<script>setTimeout('document.location.href=\"upload.php\"', 5000);</script>";
stdfoot();
exit;

}


if (empty($CURUSER['passkey']) || strlen($CURUSER['passkey']) <> 32) {

$CURUSER['passkey'] = md5($CURUSER['username'].get_date_time().$CURUSER['passhash']);
sql_query("UPDATE users SET passkey = ".sqlesc($CURUSER["passkey"])." WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
unsql_cache("arrid_".$CURUSER["id"]);

}


echo '<form name="upload" enctype="multipart/form-data" action="takeupload.php" method="post">';

echo '<table class="main" border="0" cellspacing="0" width="100%" cellpadding="5">';

echo '<tr><td class="colhead" colspan="2">'.$tracker_lang['upload_torrent'].' / '.$cat_name.'
<script type="text/javascript">function changeText(text){document.getElementById(\'descr\').value = text;}</script>
</td></tr>';

$all_psize = file_upload_max_size();
if ($all_psize < (($Torrent_Config["max_image_size"]*5)+$Torrent_Config["max_torrent_size"]))
echo "<tr><td align=\"center\" class=\"a\" width=\"99%\" colspan=\"2\" style=\"padding: 15px; font-weight: bold;\"><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['warn_limit_size'].": ".mksize($all_psize)."</td></tr>";

if ($Torrent_Config["free_from_3GB"] == true)
echo "<tr><td align=\"center\" width=\"99%\" colspan=\"2\" style=\"padding: 15px; font-weight: bold;\" class=\"row\">".$tracker_lang['free_from_bg']."</td></tr>";

echo "<tr><td align=\"center\" class=\"b\" width=\"99%\" colspan=\"2\" style=\"padding: 15px; font-weight: bold;\"><b>".$tracker_lang['warning']."</b>: ".$tracker_lang['another_tracker']."</td></tr>";

tr($tracker_lang['torrent_file'], "<input type=\"file\" name=\"tfile\" size=\"75\" /> ".$tracker_lang['max_file_size'].": <b>".mksize($Torrent_Config["max_torrent_size"])."</b>".($Functs_Patch["multitracker"] == true ? "<br /><i>".$tracker_lang['max_speed_seed']."</i>":""), 1);

tr($tracker_lang['name'], "<input type=\"text\" name=\"name\" value=\"".$dtitle."\"size=\"73\" /> <i>".sprintf($tracker_lang['max_simp_of'], 128)."</i><br />".$tracker_lang['taken_from_torrent'], 1);

if (get_user_class() >= UC_MODERATOR)
tr($tracker_lang['http_link'], "<input type=\"text\" name=\"webseed\" size=\"73\" /> <i>".sprintf($tracker_lang['max_simp_of'], 1024)."</i><br />".$tracker_lang['web_seed_info'], 1);

tr($tracker_lang['image'], "<table cellspacing=\"3\" cellpadding=\"0\" width=\"100%\" border=\"0\">

<tr><td align=\"left\" class=\"a\" colspan=\"2\">".$tracker_lang['load_from_local']."</td></tr>

<tr><td align=\"left\" class=\"b\"><input type=\"file\" name=\"image0\" size=\"80\" /> ".$tracker_lang['max_file_size'].": <b>".mksize($Torrent_Config["max_image_size"])."</td></tr>

<tr><td align=\"left\" class=\"a\" colspan=\"2\">".$tracker_lang['load_from_ethernet']."</td></tr>

<tr><td align=\"left\" class=\"b\"><input type=\"text\" maxLength=\"128\" size=\"80\" name=\"image0_inet\" value=\"".$image0_inet."\" /> <i>".sprintf($tracker_lang['max_simp_of'], 255)."</i></td></tr>

</table>", 1);

for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

tr($tracker_lang['scrinshot']." #".$xpi_s, "<table cellspacing=\"3\" cellpadding=\"0\" width=\"100%\" border=\"0\">

".($Functs_Patch["take_pic"] <> 1 ? "
<tr><td align=\"left\" class=\"a\" colspan=\"2\">".$tracker_lang['load_from_local']."</td></tr>
<tr><td align=\"left\" class=\"b\"><input type=\"file\" name=\"picture_".$xpi_s."\" size=\"80\" /> ".$tracker_lang['max_file_size'].": <b>".mksize($Torrent_Config["max_image_size"])."</b></td></tr>
":"")."
".($Functs_Patch["take_pic"] <> 2 ? "
<tr><td align=\"left\" class=\"a\" colspan=\"2\">".$tracker_lang['load_from_ethernet']."</td></tr>
<tr><td align=\"left\" class=\"b\"><input name='picture".$xpi_s."' size='80'\" /> <i>".sprintf($tracker_lang['max_simp_of'], 256)."</i></td></tr>
":"")."

</table>", 1);
}



if ($Functs_Patch["take_pic"] <> 1)
tr($tracker_lang['scrinshots'], "<textarea name=\"array_picture\" cols=\"77\" rows=\"3\"></textarea><br />".$tracker_lang['arr_pictures'], 1);

// ��� �������� ������ 2009 ����
$video_ = "[b]��������[/b]: ������� �������� / ���������� (��� �������) ��������\\n\\n[u]���������� � ������[/u]\\n[b]��������: [/b]\\n[b]������������ ��������: [/b]\\n[b]��� ������: [/b]\\n [b]����: [/b]\\n[b]��������: [/b]\\n[b]� �����: [/b]\\n\\n[b]� ������: [/b]\\n[b]��������: [/b]\\n\\n[b]�����������������: [/b]\\n[b]�����������: [/b]\\n\\n[u]�����[/u]\\n[b]������: [/b]\\n[b]��������: [/b]\\n[b]�����: [/b]\\n[b]�����: [/b]";

$audio_ = "[b]��������[/b]: ����������� - ������ (��� �������) ������ �����\\n\\n[u]���������� � ������: [/u]\\n[b]�����������: [/b]\\n[b]�������� �������: [/b]\\n[b]��� �������: [/b]\\n[b]����: [/b]\\n\\n[b]��������: [/b]\\n[u]�����[/u]\\n[b]����� ��������: [/b]\\n[b]������: [/b]\\n[b]��������: [/b]";

$game_ = "[b]� ��������[/b]: ������� �������� / ���������� (��� �������)\\n\\n[u]���������� �� ����[/u]\\n[b]�����������: [/b]\\n[b]��������: [/b]\\n[b]��� ������: [/b]\\n[b]���� ����: [/b] - �� ������� !\\n[b]����: [/b]\\n[b]����: [/b]\\n\\n[b]�� ����: [/b]\\n\\n[b]����������� ����: [/b]\\n\\n[b]���.����������: [/b] - �� ������� !\\n\\n[b]������: [/b]\\n\\n[b]��������� ����������: [/b]\\n\\n";

$soft_ = "[b]��������[/b]: �������� (��� �������) ���� \\n\\n[u]���������� � ���������[/u]\\n[b]��������: [/b]\\n[b]��� ������: [/b]\\n[b]������: [/b]\\n[b]�����������: [/b]\\n[b]���������: [/b]\\n[b]��������: [/b]\\n[b]����: [/b]\\n\\n[b]� ���������: [/b]\\n\\n[b]���. ����������: [/b]\\n\\n[b]��������� ����������: [/b] - �� ������������ ������� !\\n\\n";

$image_ = "[b]��������[/b]: H������� (��� �������)\\n\\n[b]��� �������: [/b]\\n[b]������: [/b]\\n[b]������: [/b]\\n[b]����������: [/b]\\n\\n[b]������� ��������: [/b]\\n\\n";

$clip_ = "[b]��������[/b]: �������� (��� �������) ��������\\n\\n[u]���������� � ������[/u]\\n[b]�����������: [/b]\\n[b]�������� �������: [/b]\\n[b]��� �������: [/b]\\n[b]����: [/b]\\n\\n[b]��������[/b] - ����  ������� �� ������ ��� ������� !\\n[b]�����������������: [/b]\\n\\n[u]�����[/u]\\n[b]������: [/b]\\n[b]��������: [/b]\\n[b]�����: [/b]\\n[b]�����: [/b]\\n\\n";

$books_ = "[b]� ��������[/b]: ����� - �������� (��� �������)\\n\\n[b]�����: [/b]\\n[u]��������: [/u]\\n[b]������������ ��������: [/b]\\n[b]������������: [/b]\\n[b]��� ������� �����: [/b]\\n[b]����: [/b]\\n[b]����: [/b]\\n\\n[b]��������: [/b]\\n\\n[b]������: [/b]\\n[b]���������� �������: [/b]";

$serial_ = "[b]��������[/b]: ������� �������� / ���������� (�����) (��� �������) ��������\\n\\n[u]���������� � �������[/u]\\n[b]��������: [/b]\\n[b]������������ ��������: [/b]\\n[b]��� ������: [/b]\\n[b]����: [/b]\\n[b]��������: [/b]\\n[b]� �����: [/b]\\n\\n[b]� ������: [/b]\\n\\n[b]��������: [/b]\\n[b]�����������������: [/b]\\n[b]�����������: [/b]\\n\\n[u]�����[/u]\\n[b]������: [/b]\\n[b]��������: [/b]\\n[b]�����: [/b]\\n[b]�����: [/b]\\n\\n";

$w = 80;
$h = 20;

echo("<tr><td class=\"rowhead\" style='padding: 3px'><center>".$tracker_lang['choose']." ".$tracker_lang['description']."<br /><b>");

echo("<br /><input class=\"btn\" style=\"width: ".$w."px; height: ".$h."px\" type='button' onclick='changeText(\"".$video_."\")' value='�����'>\n");
echo("<br /><input class=\"btn\" style=\"width: ".$w."px; height: ".$h."px\" type='button' onclick='changeText(\"".$audio_."\")' value='�����'>\n");
echo("<br /><input class=\"btn\" style=\"width: ".$w."px; height: ".$h."px\" type='button' onclick='changeText(\"".$soft_."\")' value='����'>\n");
echo("<br /><input class=\"btn\" style=\"width: ".$w."px; height: ".$h."px\" type='button' onclick='changeText(\"".$game_."\")' value='�������'>\n");
echo("<br /><input class=\"btn\" style=\"width: ".$w."px; height: ".$h."px\" type='button' onclick='changeText(\"".$image_."\")' value='��������'>\n");
echo("<br /><input class=\"btn\" style=\"width: ".$w."px; height: ".$h."px\" type='button' onclick='changeText(\"".$clip_."\")' value='�����'>\n");
echo("<br /><input class=\"btn\" style=\"width: ".$w."px; height: ".$h."px\" type='button' onclick='changeText(\"".$books_."\")' value='�����'>\n");
echo("<br /><input class=\"btn\" style=\"width: ".$w."px; height: ".$h."px\" type='button' onclick='changeText(\"".$serial_."\")' value='�������'>\n");
echo("</center></td><td>");
// ��� �������� ������ 2009 ����

textbbcode("upload", "descr");
echo("</td></tr>\n");

if (get_user_class() >= UC_MODERATOR) {
?>
<script type="text/javascript" src="js/tagto.js"></script>
<script type="text/javascript">
(function(jQuery){
jQuery(document).ready(function(){
jQuery("#from").tagTo("#tags");
});
})(jQuery);
</script>
<?
$tags = taggenrelist($type);

$s = '<input type="text" id="tags" name="tags">';
$s.= '<div id="from" '.(count($tags) > 50 ? 'style=\'height: 150px; overflow: auto;\'':"").'>';

if (!count($tags))
$s .= $tracker_lang['no_tag_oncat'];
else {
foreach ($tags as $row)
$s .= "<a class=\"alink\" title=\"".$tracker_lang['click_on_tag']."\" href='#'>".htmlspecialchars($row["name"])."</a>\n";
}

$s .= "</div>".$tracker_lang['notag_user']."\n";

tr($tracker_lang['all_tags'], $s, 1);
}

if (get_user_class() >= UC_MODERATOR)
tr ($tracker_lang['golden'], "<label><input type='checkbox' name='free' value='yes'> ".$tracker_lang['golden_descr']."</label>", 1);

if (get_user_class() >= UC_ADMINISTRATOR)
tr($tracker_lang['sticky'], "<label><input type=\"checkbox\" name=\"sticky\" value=\"yes\"> <i>".$tracker_lang['sticky_info']."</i></label>", 1);

echo '<script type="text/javascript">function changeText(text){document.getElementById(\'area\').value = text;}</script>';

echo "<tr><td align=\"center\" colspan=\"2\"><input type=\"hidden\" name=\"type\" value=\"".$type."\"><input type=\"submit\" class=btn style=\"height: 35px; width: 450px;\" value=\"".$tracker_lang['upload']."\" /></td></tr>";

echo "</table>";
echo "</form>";

stdfoot();
?>