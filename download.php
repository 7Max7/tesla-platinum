<?
require_once ("include/bittorrent.php");
require_once ("include/benc.php");
dbconn(false);

$ip = getip();

function announce_list($announce_urls) {

$announce_urls = array_unique($announce_urls);

for ($i = 0; $i < count($announce_urls); $i++) {

if (!empty($announce_urls[$i])) {
$list[$i][$i] = bdec(benc_str(trim($announce_urls[$i])));
$list[$i]= bdec(benc_list($list[$i]));
}
}

return bdec(benc_list($list));
}

if (ini_get('output_handler') == 'ob_gzhandler' && ob_get_length() !== false){
@ob_end_clean();
header('Content-Encoding:');
}

$id = (isset($_GET["id"]) ? intval($_GET["id"]):0);
if (empty($id) && !is_numeric($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$passkey = htmlspecialchars_uni(isset($_GET["passkey"]) ? (string) $_GET["passkey"]:"");

$true_pass = false;
if (!empty($passkey) && strlen($passkey) == 32) {

$res_i = sql_query("SELECT * FROM users WHERE passkey = ".sqlesc(strip_tags($passkey))) or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res_i)) {

$arr_i = mysql_fetch_assoc($res_i);

if ($CURUSER && $CURUSER["id"] <> $arr_i["id"])
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

unset($GLOBALS["CURUSER"]);

$GLOBALS["CURUSER"] = $arr_i;

$true_pass = true;
}

}


global $announce_urls, $announce_net, $Functs_Patch, $Torrent_Config;

if ($Functs_Patch["deny_refer"] == false && $true_pass == false){
/// ����� ������
$referer = (isset($_SERVER["HTTP_REFERER"]) ? htmlspecialchars_uni($_SERVER["HTTP_REFERER"]):"");
if (!empty($referer))
$parse_site = parse_url($referer, PHP_URL_HOST);

/// ����������� ������ � �����
$site_own = (($_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://").htmlspecialchars_uni($_SERVER['HTTP_HOST']);
if (!empty($site_own))
$parse_owner = parse_url($site_own, PHP_URL_HOST);

/// ���������� ������
if (empty($refer_parse) && $parse_owner <> $parse_site)
stderr($tracker_lang['error'], "<b>".$tracker_lang['warning']."</b>: ".sprintf($tracker_lang['direct_nodownload'], "details.php?id=".$id));

/// ����� ������
}


if (empty($announce_net) && !$CURUSER)
stderr($tracker_lang['error'], $tracker_lang['download_nologin']);

if ($CURUSER["downloadpos"] == "no" && empty($announce_net))
stderr($tracker_lang['error'], $tracker_lang['is_downloadpos_use']);

$res = sql_query("SELECT info_hash, multi_time, multitracker, viponly, name, size, stop_time, webseed, f_trackers, owner, added FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
stderr($tracker_lang['error'], $tracker_lang['no_torrent_with_such_id']);

$row = mysql_fetch_assoc($res);


if (!empty($row['owner'])){
$res_u = sql_query("SELECT username FROM users WHERE id = ".sqlesc($row['owner'])) or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($res_u) <> 0){
$row_u = mysql_fetch_assoc($res_u);
$row['username'] = $row_u['username'];
}
}


/// ������� ������� ��� ���������� download.php
if ($Functs_Patch["auto_thanks"] == true){
///// ��� ����� �� ������� �� ���������� download.php
$sql = sql_query("SELECT id FROM thanks WHERE torrentid = ".sqlesc($id)." AND userid=".sqlesc($CURUSER["id"])." and touid = ".sqlesc($row["owner"])." limit 1") or sqlerr(__FILE__, __LINE__);
$est_id = mysql_fetch_array($sql);

if (empty($est_id["id"]) && $CURUSER)
sql_query("INSERT INTO thanks (torrentid, userid, touid) VALUES (".sqlesc($id).", ".sqlesc($CURUSER["id"]).", ".sqlesc($row["owner"]).")");
}
/// ������� ������� ��� ���������� download.php



if ($Functs_Patch["kill_cold_announce"] == true && !empty($row["f_trackers"])){

// ������� ������ ����������� ��������
$arrayrow = explode("\n", $row["f_trackers"]);
/// ������� ������ "�����" �������� �� :0:0:0 ������
for ($i = 0, $c = count($arrayrow); $i < $c; $i++) {
if (!preg_match("|(:0:0:0)|U", $arrayrow[$i]) && !preg_match("|(:false)|U", $arrayrow[$i])){
unset($arrayrow[$i]);
} else
$arrayrow[$i] = trim(str_replace(array(":0:0:0",":false"), "", $arrayrow[$i]));
}
/// ���������� �������� �������� ��� �������� "�����"
for($iu = 0, $cu = count($announce_urls); $iu < $cu; $iu++) {
foreach ($arrayrow as $b => $c) {
if (isset($announce_urls[$iu]) && preg_match("/".$c."/Uis", $announce_urls[$iu]))
unset($announce_urls[$iu]);
}
}

}


/// ������������ ���� ����
if ($row["stop_time"] <> "0000-00-00 00:00:00" && get_user_class() < UC_SYSOP){

$subres = sql_query("SELECT id FROM snatched WHERE startdat < ".sqlesc($row["stop_time"])." AND torrent = ".sqlesc($id)." AND userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);
$snatch = mysql_fetch_array($subres);
$snatched = $snatch["id"];

if (empty($snatched))
stderr($tracker_lang['error'], "".$tracker_lang['tor_stop_time'].": <b>".$row["stop_time"]."</b>.");

}
/// ������������ ���� ����



if ($row["viponly"] <> "0000-00-00 00:00:00" && get_user_class() <> UC_VIP && get_user_class() < UC_MODERATOR){

if ($row["viponly"] <= get_date_time())
sql_query("UPDATE torrents SET viponly = '0000-00-00 00:00:00' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
else
stderr($tracker_lang['error'], $tracker_lang['vip_only_of'].": <b>".get_elapsed_time(sql_timestamp_to_unix_timestamp($row["viponly"]))."</b>.");

}


$down_limit = $Download_Config[($CURUSER ? ($CURUSER["class"]+1) : '0')];

if (!empty($down_limit)){

$dres = sql_query("SELECT * FROM download WHERE ".($CURUSER ? "userid = ".sqlesc($CURUSER["id"]) : "userip = ".sqlesc($ip))." AND date = ".sqlesc(date("Y-m-d"))) or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($dres) > 0){

$dnatch = mysql_fetch_array($dres);

$all_count = explode(",", $dnatch["torrents"]);

if (count($all_count) > $down_limit && !in_array($id, $all_count))  
stderr($tracker_lang['error'], $tracker_lang['download_of']."<br /><br />".sprintf($tracker_lang['download_limiter'], "<ins>".get_user_class_name(($CURUSER ? $CURUSER["class"] : "-1"))."</ins>", "<strong>".$down_limit."</strong>"));


if (!in_array($id, $all_count)){

$all_count[] = $id;
sql_query("UPDATE download SET torrents = ".sqlesc(implode(",", $all_count))." WHERE id = ".sqlesc($dnatch["id"])) or sqlerr(__FILE__,__LINE__);

}

} else {

sql_query("INSERT INTO download (".($CURUSER ? "userid": "userip").", torrents, date) VALUES (".($CURUSER ? sqlesc($CURUSER["id"]) : sqlesc($ip))." , ".sqlesc(implode(",", array($id))).", ".sqlesc(date("Y-m-d")).")");

}

}


$fname = $row['name']."_(".str_replace(" ", "", mksize($row["size"])).")";
$fname = preg_replace("/\[((\s|.)+?)\]/is", "", $fname);

if ($SITE_Config["orig_name"] == false)
$fname = str_replace(array("�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�"," "), array("a","b","v","g","d","e","e","zg","z","i","i","k","l","m","n","o","p","r","s","t","y","f","h","c","ch","sh","zh","","u","","e","iu","ia","A","B","V","G","D","E","E","Zg","Z","I","K","L","M","N","O","P","R","S","T","Y","F","H","C","Ch","Sh","Zh","", "U","","E","IU","Ia","_"), $fname);

$fname = str_replace(array(",",".",";","'","\"","/",":","&","~"), "_", $fname);
$fname = substr(str_replace(array("__", ".torrent"), array("_", ""), $fname), 0, 128);

$name = $fname."_".$SITENAME.".torrent";

$fn = ROOT_PATH."/torrents/".$id.".torrent";

if (!$row || !is_file($fn) || !is_readable($fn))
stderr($tracker_lang['error'], $tracker_lang['torrent_filedel']);

sql_query("UPDATE torrents SET hits = hits + 1 WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

if ($CURUSER){

if (empty($CURUSER["passkey"]) || strlen($CURUSER["passkey"]) <> 32) {
$CURUSER["passkey"] = md5($CURUSER["username"].get_date_time().$CURUSER["passhash"]);
sql_query("UPDATE users SET passkey = ".sqlesc($CURUSER["passkey"])." WHERE id = ".sqlesc($CURUSER["id"]));
unsql_cache("arrid_".$CURUSER["id"]);
}


/// ��������� �������
if ($row["multitracker"] == "yes" && $row["multi_time"] == "0000-00-00 00:00:00" && !empty($Torrent_Config["multihours"])){

$announce_m = $announce_urls;

unset($announce_m[0]);

$tracker_cache = array();
$f_leechers = $f_seeders = $f_time_completed = 0;

foreach ($announce_m as $announce) {

$response = get_remote_peers($announce, $row['info_hash'], true);

if ($response['state']=='ok'){
$tracker_cache[] = $response['tracker'].':'.(!empty($response['leechers']) ? $response['leechers'] : 0).':'.(!empty($response['seeders']) ? $response['seeders'] : 0).':'.(!empty($response['downloaded']) ? $response['downloaded'] : 0);
$f_time_completed += $response['downloaded'];
if ($f_leechers < $response['leechers']) $f_leechers = $response['leechers'];
if ($f_seeders < $response['seeders']) $f_seeders = $response['seeders'];

} else
$tracker_cache[] = $response['tracker'].':'.$response['state'];
}

$fpeers = $f_seeders + $f_leechers;
$tracker_cache = implode("\n", $tracker_cache);

$updatef = array();
$updatef[] = "f_trackers = ".sqlesc($tracker_cache);
$updatef[] = "f_leechers = ".sqlesc($f_leechers);
$updatef[] = "f_seeders = ".sqlesc($f_seeders);
$updatef[] = "multi_time = ".sqlesc(get_date_time());
$updatef[] = "f_times = ".sqlesc($f_time_completed);
$updatef[] = "visible = ".sqlesc(!empty($fpeers) ? 'yes':'no');

sql_query("UPDATE torrents SET " . implode(",", $updatef) . " WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

}
/// ��������� �������

}


$dict = bdec_file($fn);


if ($CURUSER)
$announce_urls[0] = $announce_urls[0]."?passkey=".$CURUSER["passkey"];

if ($row["multitracker"] == "yes") {

//unset($dict['value']['announce']); /// ������� �������� ������, ��� ����������� �������������

if (!empty($announce_urls[0])){

$dict['value']['announce'] = bdec(benc_str($announce_urls[0]));

//$dict['value']['announce']['value'] = $announce_urls[0];
//$dict['value']['announce']['string'] = strlen($dict['value']['announce']['value']).":".$dict['value']['announce']['value'];
//$dict['value']['announce']['strlen'] = strlen($dict['value']['announce']['string']);


}

$dict['value']['announce-list'] = announce_list($announce_urls);

} elseif ($row["multitracker"] == "no") {

unset($dict['value']['announce-list']); /// ��������� ������ � �������������

$dict['value']['announce'] = bdec(benc_str($announce_urls[0]));

//$dict['value']['announce']['value'] = $announce_urls[0];
//$dict['value']['announce']['string'] = strlen($dict['value']['announce']['value']).":".$dict['value']['announce']['value'];
//$dict['value']['announce']['strlen'] = strlen($dict['value']['announce']['string']);

}

$site_webs = parse_url($row["webseed"], PHP_URL_HOST);
if (!empty($site_webs))
$dict['value']['url-list'] = bdec(benc_str($row["webseed"])); /// ���������� webseed http ������ �������


/// �����������
$dict['value']['comment'] = bdec(benc_str((!empty($row["username"]) ? $row["username"]:$row["owner"]).", ".$row["added"]));

/// �������� ������ ����� ������� ������
$dict['value']['publisher'] = bdec(benc_str(parse_url($DEFAULTBASEURL, PHP_URL_HOST)));
$dict['value']['publisher-url'] = bdec(benc_str($DEFAULTBASEURL."/details.php?id=".$id));

/// ��������� ������ � ������� ������� (�������� �������� ������� �������, ������������ � ������� �������������) - ��������
//$dict['value']['info']['value']['source'] = bdec(benc_str(strip_tags(VERSION.TBVERSION)));

/// ��������� ������ � �������� ����� �������
$dict['value']['source'] = bdec(benc_str(strip_tags(VERSION.TBVERSION)));


//$encrypted_files = ($dict['value']['info']['value']['encrypted_files']['value']);



/// �������� ������ .torrent ����� �� ������
@list($info) = @dict_check_t($dict, "info");
$infohash = sha1($info["string"]);

if ($row["info_hash"] <> $infohash)
stderr($tracker_lang['error'], $tracker_lang['syntax_error']."<br />".$tracker_lang['hash_sum']." (<u>".$row["info_hash"]."</u> => <b>".$infohash."</b>)");
/// �������� ������ .torrent ����� �� ������


header ("Expires: Tue, 1 Jan 1980 00:00:00 GMT");
header ("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header ("Cache-Control: no-store, no-cache, must-revalidate");
header ("Cache-Control: post-check=0, pre-check=0", false);
header ("Pragma: no-cache");
header ("Accept-Ranges: bytes");
header ("Connection: close");
header ("Content-Transfer-Encoding: binary");
header ("Content-Disposition: attachment; filename=\"".$name."\"");
header ("Content-Type: application/x-bittorrent");
ob_implicit_flush(true);

echo benc($dict);

?>