<?php
require_once("include/bittorrent.php");

gzip();
dbconn(false);

/////////////////
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {

header("Content-Type: text/html; charset=" .$tracker_lang['language_charset']);

$id = (int) $_POST['id'];

if (empty($id)) die;

$url = "/details.php?id=".$id;

$res_s = sql_query("SELECT DISTINCT uid, username, class, ip FROM sessions WHERE time > ".sqlesc(get_date_time(gmtime() - 300))." AND (LEFT(url, ".strlen($url).") = ".sqlesc($url)." OR url = ".sqlesc($url).") ORDER BY time DESC", $cache = array("type" => "disk", "file" => "online_t_".$id, "time" => 10)) or sqlerr(__FILE__,__LINE__);

$arra_online = array();

while ($ar_r = mysql_fetch_assoc_($res_s)) {

$arra_online[] = (!empty($ar_r['uid']) && !empty($ar_r['username']) ? "<a href=\"userdetails.php?id=".$ar_r['uid']."\">".get_user_class_color($ar_r["class"], $ar_r["username"])."</a>":$ar_r['ip']);

}

if (count($arra_online))
echo implode(", ", $arra_online);
else echo $tracker_lang['no_data'];

die;
}

/////////////////

//loggedinorreturn();



$id = (isset($_GET["id"]) ? intval($_GET["id"]):0);

if (!is_valid_id($id) || empty($id))
stderr($tracker_lang['error_data'], $tracker_lang['invalid_id_value']);

$res = sql_query("SELECT torrents.*, IF(torrents.numratings < 1, NULL, ROUND(torrents.ratingsum / torrents.numratings, 1)) AS ratingsum, b.class AS classname, b.username AS classusername, users.username, users.class, users.groups FROM torrents
LEFT JOIN users ON torrents.owner = users.id 
LEFT JOIN users AS b ON torrents.moderatedby = b.id
WHERE torrents.id = ".sqlesc($id))  or sqlerr(__FILE__, __LINE__);

$row = mysql_fetch_assoc($res);


if (isset($_GET['lock_comments']) && get_user_class() >= UC_MODERATOR) {
if ($_GET['lock_comments'] == 'yes') $mysq = "yes";
elseif ($_GET['lock_comments'] == 'no') $mysq = "no";

$row["comment_lock"] = $mysq;
sql_query("UPDATE torrents SET comment_lock = ".sqlesc($mysq)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

header("Location: $DEFAULTBASEURL/details.php?id=$id#comments");
die;
}




if (!empty($row["free_who"])) {

$res_g = sql_query("SELECT username AS free_username, class AS free_class, gender AS free_gender FROM users WHERE id = ".sqlesc($row["free_who"])) or sqlerr(__FILE__, __LINE__);
$row_g = mysql_fetch_assoc($res_g);

$free_username=$row_g["free_username"];
$free_gender=$row_g["free_gender"];
$free_class=$row_g["free_class"];

}


$ratingsum=$row["ratingsum"];


if (checknewnorrent($id, $row["added"]) && $CURUSER)
@setcookie("markview", (isset($_COOKIE['markview']) ? $_COOKIE['markview']. "-" .$id : $id), 0x7fffffff, "/");


$owned = $moderator = 0;
if (get_user_class() >= UC_MODERATOR)
$owned = $moderator = 1;
elseif ($CURUSER["id"] <> $row["owner"])
$owned = 1;

//if ($_GET["page"])
//header("Location: $DEFAULTBASEURL/details.php?id=$id&page=".$_GET["page"]."#pagestart");

if (!$row || ($row["banned"] == "yes" && !$moderator)) {

if ($row["banned"] == "yes")
stderr($tracker_lang['error_data'], sprintf($tracker_lang['torrent_banned'], $row["name"]));
else {

$ifilename = ROOT_PATH."/torrents/".$id.".torrent";
if (@file_exists($ifilename) && !$row) {
$size = filesize($ifilename);
write_log("����� ������� $id ��� ������ �����. �������: ������ ����� � ������� .torrent (".mksize($size).") ����� (������ ��������).", "", "torrent");
@unlink($ifilename);
}

stderr($tracker_lang['error_data'], $tracker_lang['torrent_del_or_move'], $header_url = array("url" => "/404.php", "code" => 301)); /// ��������� ��: ����� ���.
}

} else {



//if ($row["checkpeers"] < get_date_time(gmtime() - 600*2) && ($row["seeders"] + $row["leechers"]) > 0 && $row["checkpeers"] <> '0000-00-00 00:00:00')
//sql_query("UPDATE torrents SET seeders = '0', leechers = '0' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

// ���� � ������ ���� ���� - ������� ������� � ������� �������� (� �������)
if ($row["visible"] == "no" && ($row["seeders"] + $row["leechers"] + $row["f_seeders"] + $row["f_leechers"]) > 0)
sql_query("UPDATE torrents SET visible = 'yes' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
elseif ($row["visible"] == "yes" && ($row["seeders"] + $row["leechers"] + $row["f_seeders"] + $row["f_leechers"]) == 0)
sql_query("UPDATE torrents SET visible = 'no' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
// ���� � ������ ���� ���� - ������� ������� � ������� �������� (� �������)



//$CURUSER && 
if ($row["moderated"] == "yes")
sql_query("UPDATE LOW_PRIORITY torrents SET views = views + 1 WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

if (isset($_GET["tocomm"]))
header("Location: $DEFAULTBASEURL/details.php?id=$id&page=0#startcomments");


if ($CURUSER)
stdhead($row["name"]. " ".(!empty($row["cat_name"])? " :.: ".$row["cat_name"]."":""));
else
stdhead($row["name"], true);

if ($CURUSER["id"] == $row["owner"] || get_user_class() >= UC_MODERATOR)
$owned = 1;
else
$owned = 0;

if (isset($_GET["report"]) && $CURUSER)
stdmsg($tracker_lang['success'], $tracker_lang['complaint_sent']);
elseif (isset($_GET["alreadyreport"]) && $CURUSER)
stdmsg($tracker_lang['error'], $tracker_lang['complaint_against']);
elseif (isset($_GET["ownreport"]) && $CURUSER)
stdmsg($tracker_lang['error'], $tracker_lang['complaint_owner']);
elseif (isset($_GET["helpme"]) && $CURUSER)
stdmsg($tracker_lang['back_snatched_all'], $tracker_lang['reseed_now']);

if ($CURUSER) {

//�������� �� �������������
	
$resour = sql_query("SELECT COUNT(*) AS numc, (SELECT COUNT(*) AS numb FROM bookmarks WHERE userid = ".sqlesc($CURUSER["id"])." AND torrentid = ".sqlesc($id)." LIMIT 1) AS numb FROM checkcomm WHERE userid = ".sqlesc($CURUSER["id"])." AND checkid = ".sqlesc($id)." AND torrent = '1' LIMIT 1") or sqlerr(__FILE__, __LINE__);

$arr_res = mysql_fetch_assoc($resour);

$checkcomm=$arr_res["numc"];
$bookcomm=$arr_res["numb"];

?>
<script type="text/javascript">
function bookmark(id, type, page) {
var loading = "";
var id = id; var type = type;
jQuery("#loading").html(loading);
jQuery.post('/bookmark.php',{'id':id , 'type':type , 'page':page},
function(response) {
jQuery('#bookmark_'+id).html(response);
jQuery("#loading").empty();
}, 'html');
}

function checmark(id, type, page, twopage) {
var loading = "";
var id = id; var type = type;
jQuery("#loading").html(loading);
jQuery.post('/bookmark.php',{'id':id , 'type':type , 'page':page, 'twopage':twopage},

function(response) {
jQuery('#checmark_'+id).html(response);
jQuery("#loading").empty();
}, 'html');
}
</script>
<?

$array_mark = array();

if (empty($checkcomm))
$array_mark[] = "<span id=\"checmark_".$id."\"><a class=\"altlink_white\" href=\"#\" onclick=\"checmark('".$id."', 'add' , 'check', 'details');\"><b>".$tracker_lang['monitor_comments']."</b></a></span>";
else
$array_mark[] = "<span id=\"checmark_".$id."\"><a class=\"altlink_white\" href=\"#\" onclick=\"checmark('".$id."', 'del' , 'check', 'details');\"><b>".$tracker_lang['monitor_comments_disable']."</b></a></span>";

if (empty($bookcomm))
$array_mark[] = "<span id=\"bookmark_".$id."\"><a class=\"altlink_white\" href=\"#\" onclick=\"bookmark('".$id."', 'add' , 'details');\"><b>".$tracker_lang['bookmark_this']."</b></a></span>";
else
$array_mark[] = "<span id=\"bookmark_".$id."\"><a class=\"altlink_white\" href=\"#\" onclick=\"bookmark('".$id."', 'del' , 'details');\"><b>".$tracker_lang['bookmark_off']."</b></a></span>";

}
}

 
echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">\n");
echo("<tr><td class=\"colhead\" colspan=\"2\">

<div style=\"float: left; width: auto;\">:: ".$tracker_lang['torrent_details']." </div>
".($CURUSER && count($array_mark) ? "<div align=\"right\">".implode(" :: ", $array_mark)."</div>":"")." 
</td></tr>");


if ($row["stop_time"] <> "0000-00-00 00:00:00" && get_user_class() < UC_SYSOP){
$subres = sql_query("SELECT id FROM snatched WHERE startdat < ".sqlesc($row["stop_time"])." AND torrent = ".sqlesc($id)." AND userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);
$snatch = mysql_fetch_assoc($subres);
$snatched = $snatch["id"];
}


$array_s = array();

if ($row["banned"] == "yes")
$array_s[] = $tracker_lang['is_banned_reason'].": ".htmlspecialchars($row["banned_reason"]).".";

if (get_user_class() < UC_MODERATOR && $row["moderated"] == "no" && ($CURUSER["id"] <> $row["owner"]))
$array_s[] = $tracker_lang['is_moderated_yes'];

if ($row["stop_time"] <> "0000-00-00 00:00:00" && empty($snatched) && get_user_class()< UC_SYSOP) 
$array_s[] = $tracker_lang['tor_stop_time'].": ".$row["stop_time"];

if (empty($announce_net) && !$CURUSER)
$array_s[] = $tracker_lang['login_download'];

if ($CURUSER && $CURUSER["downloadpos"] == "no")
$array_s[] = $tracker_lang['is_downloadpos_use'];

if ($row["viponly"] <> "0000-00-00 00:00:00" && get_user_class() <> UC_VIP && get_user_class() < UC_MODERATOR && $row["viponly"] > get_date_time())
$array_s[] = $tracker_lang['vips_only'];


$down_limit = $Download_Config[($CURUSER ? ($CURUSER["class"]+1) : '0')];

if (!empty($down_limit)){

$dres = sql_query("SELECT torrents FROM download WHERE ".($CURUSER ? "userid = ".sqlesc($CURUSER["id"]) : "userip = ".sqlesc(getip()))." AND date = ".sqlesc(date("Y-m-d"))) or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($dres) > 0){

$dnatch = mysql_fetch_assoc($dres);

$all_count = explode(",", $dnatch["torrents"]);

if (count($all_count) > $down_limit && !in_array($id, $all_count))  
$array_s[] = $tracker_lang['download_of']."<br />".sprintf($tracker_lang['download_limiter'], "<ins>".get_user_class_name(($CURUSER ? $CURUSER["class"] : "-1"))."</ins>", "<strong>".$down_limit."</strong>");

}

}


//// ������� jquery
if (!empty($bookcomm) && $CURUSER){
?>
<script language="javascript" type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">

jQuery(document).ready(function () {
var id=<?=$id;?>;
function slideout() {setTimeout(function () {jQuery("#response").slideUp("slow", function () {});},5000);}
jQuery(".inlineEdit").bind("click", updateText);
var OrigText, NewText;
jQuery(".save").live("click", function () {
jQuery("#loading").fadeIn('slow');
NewText = jQuery(this).siblings("form").children(".edit").val();
//  var id = jQuery(this).parent().attr("id");
var data = 'id=' + id + '&text=' + NewText;
jQuery.post("bookmark.php", data, function (response) {
jQuery("#response").html(response);
jQuery("#response").slideDown('slow');
slideout();
jQuery("#loading").fadeOut('slow');
});
jQuery(this).parent().html(NewText).removeClass("selected").bind("click", updateText);
});
jQuery(".revert").live("click", function () {jQuery(this).parent().html(OrigText).removeClass("selected").bind("click", updateText);});
function updateText() {jQuery('span').removeClass("inlineEdit");OrigText = jQuery(this).html();
jQuery(this).addClass("selected").html('<?=$tracker_lang['add_note'];?>:<br /><form ><textarea class="edit">' + OrigText + '</textarea></form> <a title="<?=$tracker_lang['add_or_del'];?>" href="#" class="save"><?=$tracker_lang['add_note'];?></a> <?=$tracker_lang['or'];?> <a href="#" title="<?=$tracker_lang['reset'];?>" class="revert"><?=$tracker_lang['reset'];?></a>').unbind('click', updateText);}
});
</script>
<style>
.edit {width: 100%; margin:2px;}
span {width: 100%;}
span:hover {cursor:pointer;}
span.selected:hover {background-image:none;}
span.selected {padding: 10px;width: 100%;}
form {width: 100%;}
.save, .btnCancel {margin:0px 0px 0 5px;}
#response {display:none;padding:10px;background-color:#9F9;}
#loading {display:none;}
</style>
<?

$datamy = sql_query("SELECT mytags FROM bookmarks WHERE torrentid = ".sqlesc($id)." AND userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
$mtags = mysql_fetch_assoc($datamy);
$mytags = $mtags["mytags"];

if (empty($mytags))
$viewtags = $tracker_lang['add_note'];
else
$viewtags = format_comment(strip_tags($mytags));

echo "<tr><td align=\"center\" width=\"99%\" colspan=\"2\" class=\"row\">
<div id=\"response\"></div>
<span style=\"padding: 15px; font-weight: bold;\" class=\"inlineEdit\" id=\"1\">".$viewtags."</span>
</td></tr>";

}
//// ������� jquery


if ($Functs_Patch["multitracker"] == true && $row["multitracker"] == "yes" && ($row["f_seeders"] + $row["f_leechers"]) < 2 && $row["f_times"] < 10)
echo "<tr><td align=\"center\" width=\"99%\" colspan=\"2\" style=\"font-weight: bold;\" class=\"b\">".$tracker_lang['tor_multi_help']."</td></tr>"; 


if ($row["viponly"] <> "0000-00-00 00:00:00") {

if ($row["viponly"] <= get_date_time())
sql_query("UPDATE torrents SET viponly = '0000-00-00 00:00:00' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

echo "<tr><td align=\"center\" width=\"99%\" colspan=\"2\" style=\"color:#000000; padding: 15px; font-weight: bold;background-color:#F5DDDD;  border: 1px #F5F5F5 solid;\" class=\"row\">".$tracker_lang['vip_only_of'].": <span style=\"color:green\"><b>".get_elapsed_time(sql_timestamp_to_unix_timestamp($row["viponly"]))."</b></span>.</td></tr>";

}


if (!$CURUSER)
$edit_link = false;
elseif (get_user_class() < UC_UPLOADER && $CURUSER["id"] <> $row["owner"])
$edit_link = false;	
elseif (get_user_class() < UC_UPLOADER && $CURUSER["id"] == $row["owner"] && $row["moderated"] == "yes")
$edit_link = $tracker_lang['no_edit_tor'].": ".$tracker_lang['sort_moderdate']." ".$row["moderatordate"];	
elseif (!empty($CURUSER["catedit"]) && !preg_match("/\[cat".$row["category"]."\]/is", $CURUSER["catedit"]) && get_user_class() == UC_MODERATOR && $CURUSER["id"] <> $row["owner"])
$edit_link = $tracker_lang['no_edit_tor'].": ".$tracker_lang['no_modcat'];
else
$edit_link = "<a title=\"".$tracker_lang['edit']."\" href=\"edit.php?id=".$id."\">".$tracker_lang['edit']."</a>";	


$array_down = array();

echo "<tr><td align=\"center\" width=\"100%\" colspan=\"2\">
".(isset($edit_link) && $edit_link <> false ? "<div align=\"right\" style=\"margin: 0px; padding: 0px;\">".$edit_link."</div>":"")."
<h3><a href=\"details.php?id=".$id."\">".($row["name"])." ".($row["free"] == "yes" ? "<img style=\"border:none\" alt=\"".$tracker_lang['golden']."\" title=\"".$tracker_lang['golden']."\" src=\"/pic/freedownload.gif\">":"")."</a></h3>
</td></tr>";


if (count($array_s)){

tr($tracker_lang['warning_is'], implode("<br />", $array_s), 1);

} else {

$array_down[] = "<a rel=\"nofollow\" href=\"download.php?id=".$id."\"><strong>".$tracker_lang['get_torrent']."</strong></a>";

if ($Functs_Patch["multitracker"] == true){

$announce_urls[0] = $announce_urls[0].(!empty($CURUSER['passkey']) ? "?passkey=".$CURUSER['passkey']:"");

$array_down[] = "<noindex><a rel=\"nofollow\" title=\"".$tracker_lang['magnet_link']."\" href=\"magnet:?xt=urn:btih:".$row['info_hash']."&amp;dn=".htmlspecialchars($row['name'])."&amp;tr=".implode("&amp;tr=", $announce_urls)."\">".$tracker_lang['magnet_link']."</a></noindex>";

} else {

$array_down[] = "<noindex><a rel=\"nofollow\" title=\"".$tracker_lang['magnet_link']."\" href=\"magnet:?xt=urn:btih:".$row['info_hash']."&amp;dn=".htmlspecialchars($row['name'])."&amp;tr=".$announce_urls[0].(!empty($CURUSER['passkey']) ? "?passkey=".$CURUSER['passkey']:"")."\">".$tracker_lang['magnet_link']."</a></noindex>";

}

if (!empty($row["webseed"]) && $CURUSER) {
$site_webs = parse_url($row["webseed"], PHP_URL_HOST);

if (!empty($site_webs))
$array_down[] = "<a rel=\"nofollow\" title=\"".$tracker_lang['http_link']."\" href=\"".$row["webseed"]."\">".$tracker_lang['http_link']." ".$site_webs."</a>";
}

if (get_user_class() >= UC_MODERATOR || $CURUSER["id"] == $row["owner"])
$array_down[] = "<a title=\"".$tracker_lang['creat_zupload']."\" rel=\"nofollow\" href=\"zupload.php?id=".$id."\"><u>".$tracker_lang['creat_zupload']."</u></a>";

tr($tracker_lang['download'], implode("<br />", $array_down), 1);

}

if (get_user_class() >= UC_MODERATOR)
tr($tracker_lang['info_hash'], $row['info_hash'], 1);



if ($row["stop_time"] <> "0000-00-00 00:00:00")
echo "<tr><td align=\"center\" width=\"99%\" colspan=\"2\" style=\"padding: 15px; font-weight: bold;\" class=\"row\">".sprintf($tracker_lang['suspended'], "<span style=\"color:red\"><b>".$row["stop_time"]."</b></span>")."</td></tr>";

if ($row["moderated"] == "no" && get_user_class() <= UC_MODERATOR && $CURUSER)
echo "<tr><td align=\"center\" width=\"99%\" colspan=\"2\" style=\"padding: 15px; font-weight: bold;\" class=\"row\">".$tracker_lang['no_moderated_yet']."</td></tr>";

if ($row["free"] == "yes")
echo "<tr><td align=\"center\" width=\"99%\" colspan=\"2\" style=\"font-weight: bold;\" class=\"row\">".$tracker_lang['golden_info']."</td></tr>"; 

if (get_user_class() >= UC_MODERATOR || (!empty($row["moderatedby"]) && $row["moderatedby"] == $CURUSER["id"] && $CURUSER["id"] == $row["owner"] && !empty($row["owner"]) && get_user_class() >= UC_VIP && get_user_class() <= UC_UPLOADER)) {

echo("<tr><td align=\"right\" class=\"heading\">".$tracker_lang['moderated']."</td>");

if (get_user_class() > UC_MODERATOR && $row["moderated"] == "yes")
$check_del = true;
elseif (!empty($row["moderatedby"]) && $row["moderatedby"] == $CURUSER["id"] && $CURUSER["id"] == $row["owner"] && !empty($row["owner"]) && get_user_class() >= UC_VIP && get_user_class() <= UC_UPLOADER)
$check_del = true;
else
$check_del = false;

if ($row["moderated"] == "no") {

if (!empty($row["moderatedby"]) && $row["moderatedby"] == $CURUSER["id"] && $CURUSER["id"] == $row["owner"] && !empty($row["owner"]) && get_user_class() >= UC_VIP && get_user_class() <= UC_UPLOADER)
echo ("<td align=\"left\"><a href=\"check.php?id=".$id."\"><b><font color=\"red\">".$tracker_lang['approve']."</font></b></a> <i>(".$tracker_lang['owner_moderated'].")</i></td>\n");
else
echo ("<td align=\"left\"><a href=\"check.php?id=".$id."\"><b>".$tracker_lang['approve']."</b></a> ".($row["moderatedby"] == $row["owner"] ? "<i>(".$tracker_lang['owner_moderated'].")</i>":"<i>(".$tracker_lang['need_moderated'].")</i>")."</td>\n");

} else {

echo ("<td align=\"left\"><b>".($row["classusername"] ? "<a href=\"userdetails.php?id=".$row["moderatedby"]."\">".get_user_class_color($row["classname"], $row["classusername"])."</a>" : "id [".$row["moderatedby"]."]")."</b> ".$tracker_lang['in']." <b>".$row["moderatordate"]."</b> ".($check_del == true ? "<a href=\"checkdelete.php?id=".$id."\"><b>(<font color=\"red\">".$tracker_lang['delete']."</font>)</b></a>":"")." ".($row["moderatedby"] == $row["owner"] ? "<i>(".$tracker_lang['owner_moderated'].")</i>":"<i>(".$tracker_lang['need_moderated'].")</i>")."</td></tr>\n");

}

} elseif (get_user_class() < UC_MODERATOR && $row["moderated"] == "yes" && $CURUSER)
echo ("<tr><td align=\"right\" class=\"heading\">".$tracker_lang['moderated']."</td><td align=\"left\"><b>".(!empty($row["classusername"]) ? "<a href=\"userdetails.php?id=".$row["moderatedby"]."\">".get_user_class_color($row["classname"], $row["classusername"])."</a>" : "id [".$row["moderatedby"]."]")."</b> ".$tracker_lang['in']." <b>".$row["moderatordate"]."</b> ".($check_del == true ? "<a href=\"checkdelete.php?id=".$id."\"><b>(<font color=\"red\">".$tracker_lang['delete']."</font>)</b></a>":"")."</td></tr>\n");

elseif (get_user_class() < UC_MODERATOR && $row["moderated"] == "no" && $CURUSER)
echo ("<tr><td align=\"right\" class=\"heading\">".$tracker_lang['moderated']."</td><td align=\"left\"><b>".$tracker_lang['no']."</b></td>\n");


///// ��� ����� ��� ����� /////
if (!empty($row["tags"])){

$tags = $tag_s = array();
foreach (explode(",", $row["tags"]) as $tag) {
$tags[] = "<a title=\"".htmlspecialchars($tag)."\" style=\"font-weight:normal;\" href=\"browse.php?tag=".urlencode(trim($tag))."\"><strong>".tolower($tag)."</strong></a>";
$tag_s[] = $tag;
}

$tagarray = @array_unique($tag_s); /// ������� ���������
if (count($tag_s) <> count($tagarray))
sql_query("UPDATE torrents SET tags = ".sqlesc(implode(", ", $tagarray))." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (count($tags))
tr($tracker_lang['tags'], @implode(", ", $tags), 1);

} else {

if (($row["views"] < 25 || $row["moderated"] == "no") && $Torrent_Config["auto_tags"] == true){
$tagarray = tags_f_descr($row["descr"]);

if (count($tagarray) && @implode(", ", $tagarray)<>""){
sql_query("UPDATE torrents SET tags = ".sqlesc(implode(", ", $tagarray))." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

foreach ($tagarray as $tag)
sql_query("UPDATE tags SET howmuch = howmuch+1 WHERE name = ".sqlesc($tag));

foreach ($tagarray as $tagi)
sql_query("INSERT INTO tags (category, name, howmuch, added) VALUES (".sqlesc($row["category"]).", ".sqlesc($tagi).", 1, ".sqlesc(get_date_time()).")");

if (count($tagarray)){

$tag_s = array();
foreach ($tagarray as $tag) {
$tag_s[] = "<a title=\"".htmlspecialchars($tag)."\" style=\"font-weight:normal;\" href=\"browse.php?tag=".urlencode(trim($tag))."\"><strong>".tolower($tag)."</strong></a>";
}

tr($tracker_lang['tags'], @implode(", ", $tag_s)." [".$tracker_lang['auto_add_tags']."]");

}

} else
tr($tracker_lang['tags'], "[".$tracker_lang['no_choose'].", ".$tracker_lang['auto_noadd']."]");

} else 
tr($tracker_lang['tags'], "[".$tracker_lang['no_choose']."]");
}
///// ��� ����� ��� ����� /////


if (!empty($row["image1"])) {
$image0 = htmlspecialchars_uni($row["image1"]);

if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image0))
$img1 = "<span class=\"daGallery\"><a title=\"".htmlspecialchars_uni($row["name"])."\" rel=\"group2\" href=\"".$image0."\"><img border='0' src=\"thumbnail.php?image=".$image0."&for=details\"/></a></span>";
elseif (!empty($image0))
$img1 = "<span class=\"daGallery\"><a title=\"".htmlspecialchars_uni($row["name"])."\" rel=\"group2\" href=\"/torrents/images/".$image0."\"><img border='0' src='thumbnail.php?image=".$image0."&for=details'/></a></span>";

if (!empty($img1))
tr($tracker_lang['image'], "<noindex>".$img1."</noindex>", 1);

}



/// ������ ������ �� ������� ������ �� ���
if (!in_array($row["category"], array(10)))
preg_match("/\n.*?(� �����:|� �����)(.*?)\n/is", $row["descr"], $acts);
if (!empty($acts[2]))
preg_match_all("/(]|\s|,)[�-��-���a-zA-Z-.\s]{3,20}\s([�-��-���a-zA-Z-.]{3,20}\s[�-��-���a-zA-Z-.]{3,20}\s[�-��-���a-zA-Z-.]{3,20}|[�-��-���a-zA-Z-.]{3,20}\s[�-��-���a-zA-Z-.]{3,20}|[�-��-���a-zA-Z-.]{3,20})/i", $acts[2], $time);

if (count($time[0])){
foreach ($time[0] AS $ac){
$ac = substr($ac, 1);
$row["descr"] = preg_replace("/".preg_quote($ac)."/is", "[url=persons.php?n=".trim($ac)."]<span itemprop=\"name\">".($ac)."</span>[/url]", $row["descr"], 1);
}
}
/// ������ ������ �� ������� ������ �� ���





if (!empty($row["descr"]))
tr($tracker_lang['description'].(get_user_class() >= UC_MODERATOR ? "<br />(".strlen($row["descr"]).")":""), format_comment($row["descr"]), 1, 1);





$picture = array();
for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

if (!empty($row["picture".$xpi_s]))
$picture[] = "<a rel=\"group2\" href=\"".(!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["picture".$xpi_s]) ? "torrents/images/":"").($row["picture".$xpi_s])."\" title=\"".htmlspecialchars($row["name"])."\" ><img border=\"0\" src=\"thumbnail.php?image=".($row["picture".$xpi_s])."\" width=\"150\"/></a>";

}


if (count($picture))
tr($tracker_lang['scrinshots'], "<noindex><span class=\"galPicList daGallery\">".implode("", $picture)."</span></noindex>", 1, 1);


echo "<script type=\"text/javascript\" src=\"/js/daGallery.js\"></script><script type=\"text/javascript\">DaGallery.init();</script>";


$torrent_com = htmlspecialchars($row["torrent_com"]);
if (get_user_class() >= UC_MODERATOR && !empty($torrent_com))
echo "<tr><td align=right class=\"heading\">".$tracker_lang['history_torrent']." <br />[".strlen($torrent_com)."]</td><td colspan=2 align=left><textarea cols=75 rows=".(strlen($torrent_com) > 180 ? round(strlen($torrent_com)/180)+4:3)." readonly>".$torrent_com."</textarea></td></tr>\n";



/// $sismatch - ��� ����������, ������� ������� � ����� mematch - �����������, ������ ������� ������� �����.
if ($sismatch == false){

echo '<script>
function adjective_ax() {
jQuery.post("block-details_ajax.php" , {action:"simular", id:"'.$id.'", crc:"'.crc32($row["info_hash"].$id).'"}, function(response) {
jQuery("#adjective_ax").html(response);
}, "html");
setTimeout("adjective_ax();", 180000);
}
setTimeout("adjective_ax();", 1000);
</script>';

tr($tracker_lang['match_files'], "<div id=\"adjective_ax\">".$tracker_lang['load_and_update']."</div>");

}



$res_cat = sql_query("SELECT id, name, image FROM categories", $cache = array("type" => "disk", "file" => "browse_cat_array", "time" => 24*7200)) or sqlerr(__FILE__, __LINE__);

while ($arr_cat = mysql_fetch_assoc_($res_cat)){

if ($arr_cat["id"] == $row["category"]){   
$cat_sql_[$arr_cat["id"]] = array("name" => $arr_cat["name"], "image" => $arr_cat["image"]);
break;
}

}

if (isset($cat_sql_[$row["category"]]))
tr($tracker_lang['category'], "<a title=\"".$tracker_lang['click_on_cat']."\" href=\"browse.php?cat=".$row["category"]."\">".$cat_sql_[$row["category"]]["name"]."</a> <a title=\"".$tracker_lang['click_on_rss']."\" href=\"rss.php?cat=".$row["category"]."\"><img alt=\"".$tracker_lang['rss']."\" src=\"pic/rss.gif\" border=\"0\" /></a>");
else
tr($tracker_lang['category'], "(".$tracker_lang['no_choose'].")");
                

if ($row["visible"] == "no" && $row["multitracker"] == "no")
tr($tracker_lang['visible'], "<b>".$tracker_lang['no']."</b> (".$tracker_lang['dead'].")", 1);
            
if ($row["multitracker"] == "no" && $Functs_Patch["multitracker"] == true)
tr($tracker_lang['privat_torrent'], ($row["multitracker"] == "no" ? $tracker_lang['yes']." <b>[</b>".$tracker_lang['disable_funcs'].": ".$tracker_lang['local_dht']."<b>]</b>" : "".$tracker_lang['no']." <b>[</b>".$tracker_lang['enable_funcs'].": ".$tracker_lang['local_dht']."<b>]</b>"), 1);

//if ($moderator)
//tr($tracker_lang['banned'], ($row["banned"] == 'no' ? $tracker_lang['no'] : $tracker_lang['yes']) );

if ($row["multitracker"] == "no" && empty($row["seeders"]))
tr($tracker_lang['seeder'], $tracker_lang['seeder_last_seen']." ".get_elapsed_time(sql_timestamp_to_unix_timestamp($row["last_action"])) . "".$tracker_lang['ago']);






/// �������� ����� ������ ���� ����� ������ = 0 ///
if (empty($row["numfiles"]) || empty($row["size"])){

require_once("include/benc.php");

$ifilename = ROOT_PATH."/torrents/".$id.".torrent";

if (file_exists($ifilename)){
$dict = bdec_file($ifilename, 1024000);
list($info) = dict_check_t($dict, "info");
list($dname, $plen, $pieces) = @dict_check_t($info, "name(string):piece length(integer):pieces(string)");
$filelist = array();
$totallen = @dict_get_t($info, "length", "integer");
if (isset($totallen))
$filelist[] = array($dname, $totallen);
else {
$flist = @dict_get_t($info, "files", "list");
$totallen = 0;
	
if (count($flist)){
foreach ($flist as $sf) {
list($ll, $ff) = @dict_check_t($sf, "length(integer):path(list)");
$totallen += $ll;
$ffa = array();
foreach ($ff as $ffe) {
$ffa[] = $ffe["value"];
}
$filelist[] = array(implode("/", $ffa), $ll);
}
}
}

$size = 0;
sql_query("DELETE FROM files WHERE torrent = ".sqlesc($id));

foreach ($filelist as $file) {
$file[0] = utf8_to_win($file[0]);
$size = $size+$file[1];
sql_query("INSERT INTO files (torrent, filename, size) VALUES (".$id.", ".sqlesc($file[0]).",".sqlesc($file[1]).")");
}

sql_query("UPDATE torrents SET numfiles = ".sqlesc(count($filelist)).", size = ".sqlesc($size)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

$row["numfiles"] = count($filelist);
$row["size"] = $size;
}

}
/// �������� ����� ������ ���� ����� ������ = 0 ///

tr($tracker_lang['views'], $row["views"]." ".$tracker_lang['times']);
tr($tracker_lang['hits'], $row["hits"]." ".$tracker_lang['times']);
//tr($tracker_lang['snatched'], $row["times_completed"]." ".$tracker_lang['times']);

tr ($tracker_lang['files'], $row["numfiles"], 1);
tr ($tracker_lang['size'], mksize($row["size"])." (".number_format($row["size"])." ".$tracker_lang['bytes'].")");



$datad = array();
$data = sql_query("SELECT sum(uploaded) AS datau, sum(downloaded) AS datad FROM snatched WHERE torrent = ".sqlesc($id), $cache = array("type" => "disk", "file" => "snat_".$id, "time" => 60*60*6)) or sqlerr(__FILE__, __LINE__);
$a = mysql_fetch_assoc_($data);

if (!empty($a["datad"]))
$datad[] = "<b>".$tracker_lang['downloaden']."</b>: ".mksize($a["datad"]);

if (!empty($a["datau"]))
$datad[] = "<b>".$tracker_lang['uploaden']."</b>: ".mksize($a["datau"]);

if (count($datad)){

if (($row["seeders"] + $row["leechers"]) > 0 && $CURUSER){

$suql = sql_query("SELECT downloadoffset, uploadoffset FROM peers WHERE torrent = ".sqlesc($id), $cache = array("type" => "disk", "file" => "traffp_".$id, "time" => 60*5)) or sqlerr(__FILE__, __LINE__);

while ($e = mysql_fetch_assoc_($suql)) {
$down_off += $e["downloadoffset"] / max(10, ($e["la"]) - $e["pa"]);
$up_off += $e["uploadoffset"] / max(10, ($e["la"]) - $e["pa"]);
}

}

if (!empty($down_off))
$datad[] = "<b>".$tracker_lang['downloadpos']."</b>: ".mksize($down_off)." /".$tracker_lang['sec']."";
if (!empty($up_off))
$datad[] = "<b>".$tracker_lang['uploadpos']."</b>: ".mksize($up_off)." /".$tracker_lang['sec']."";

tr($tracker_lang['traffic'], implode(", ", $datad),1);  
}



    
if ($CURUSER) {

if (!empty($row["ratingsum"]))
$row_rating = round($row["ratingsum"]/$row["numratings"], 1);
else
$row_rating = 0;

if (get_user_class() >= UC_MODERATOR){
?>
<script type="text/javascript">
function getraticho(tid) { var det = document.getElementById('raticho_'+tid);
if(!det.innerHTML) {
var ajax = new tbdev_ajax();
ajax.onShow ('');
var varsString = "";
ajax.requestFile = "block-details_ajax.php";
ajax.setVar("tid", tid);
ajax.setVar("raticho", "yes");
ajax.method = 'POST';
ajax.element = 'raticho_'+tid;
ajax.sendAJAX(varsString); } else  det.innerHTML = '';}
</script>
<?
}

$xres = sql_query("SELECT ratings.user, ratings.rating, ratings.added, users.username, users.class FROM ratings 
LEFT JOIN users ON users.id = ratings.user
WHERE torrent = ".sqlesc($id)." ORDER BY ratings.added", $cache = array("type" => "disk", "file" => "ratings_".$id, "time" => 60*60*48)) or sqlerr(__FILE__, __LINE__);

while ($xrow = mysql_fetch_assoc_($xres)){

if ($xrow["user"] == $CURUSER["id"]){
$cur_added = $xrow["added"];
$cur_rating = $xrow["rating"];
break;
}

}
$allgol = round(mysql_num_rows_($xres));

$rating = "<table border=\"0\" id=\"tesla_tto_rate\"><tr>
<td>".pic_rating_b(10, $ratingsum)."</td>
<td width=\"100%\" style=\"padding-top: 2px; padding-bottom: 2px; border: 0px;\">
".(isset($cur_added) ? "<b>".$tracker_lang['all_rating']."</b>: ".round($ratingsum, 1)." <b>".$tracker_lang['voted_more']."</b>: ".$allgol."<br /><b>".$tracker_lang['my_rating']."</b> ".$cur_rating." <b>".$tracker_lang['clock']."</b>: ".$cur_added:"<b>".$tracker_lang['all_rating']."</b>: ".round($ratingsum, 1)." <b>".$tracker_lang['voted_more']."</b>: ".$allgol."<br /> <small>".$tracker_lang['finish_rating_snat']."</small>")."
</td></tr></table>";

tr($tracker_lang['rating'], $rating.(get_user_class() >= UC_MODERATOR && !empty($allgol) ? "<span id=\"raticho_".$id."\"></span>":""), 1);
}

tr($tracker_lang['added'], "<a title=\"".sprintf($tracker_lang['search_for_day'], $row["added"])."\" href=\"browse.php?date=".current(explode(" ", $row["added"]))."\"><time itemprop=\"startDate\" datetime=\"".date('c', strtotime($row["added"]))."\">".$row["added"]."</time></a>");




if ($CURUSER["id"] <> $row["owner"] && !empty($row["owner"]))
tr($tracker_lang['uploaded'], (isset($row["username"]) ? "<a href=\"userdetails.php?id=".$row["owner"]."\">".get_user_class_color($row["class"], $row["username"]."</a>") : "<i>".$tracker_lang['anonymous']."- ".$row["owner"]."</i>").'&nbsp;
'.($CURUSER && !empty($row["owner"]) ? '<a href="simpaty.php?action=add&good&targetid='.$row["owner"].'&type='.$id.'" title="'.$tracker_lang['respect'].'"><img src="pic/thum_good.gif" border="0" alt="'.$tracker_lang['respect'].'" title="'.$tracker_lang['respect'].'" /></a>&nbsp;&nbsp;<a href="simpaty.php?action=add&bad&targetid='.$row["owner"].'&type='.$id.'" title="'.$tracker_lang['antirespect'].'"><img src="pic/thum_bad.gif" border="0" alt="'.$tracker_lang['antirespect'].'" title="'.$tracker_lang['antirespect'].'" /></a>':'')."
 (<a title=\"".$tracker_lang['arr_searched'].": ".(isset($row["username"]) ? $row["username"]:$tracker_lang['anonymous']."- ".$row["owner"])."\" href=\"browse.php?userid=".$row["owner"]."\">".$tracker_lang['persons_torsearch']."</a>)", 1);


// ������
if (!empty($row["groups"])){

$thanked_sql = sql_query("SELECT image, name, comment FROM groups WHERE id = ".sqlesc($row["groups"]), $cache = array("type" => "disk", "file" => "details_groups-".$row["groups"], "time" => 86400)) or sqlerr(__FILE__, __LINE__);
while ($name_gre = mysql_fetch_assoc_($thanked_sql)){

if (!empty($name_gre["image"]))
echo ("<tr><td align=\"right\" class=\"heading\"><b>".$tracker_lang['group_realese']."</b></td><td align=\"left\">
<table border=\"0\" style=\"margin: 0px; padding: 0px;\"><tr>
<td width=\"88px\"><a rel=\"nofollow\" href=\"browse.php?gr=".$row["groups"]."\"><img title=\"".htmlspecialchars($name_gre["name"])."\" src=\"/pic/groups/".$name_gre["image"]."\"></a> </td> ".(!empty($name_gre["comment"]) ? "<td style=\"padding-top: 2px; padding-bottom: 4px; border: 0px;\" width=\"100%\">".$name_gre["comment"]."</td>":"")."
</tr></table>
</td></tr>");

}
}
// ������





if ($CURUSER && $Functs_Patch["multitracker"] == true){
?>
<script type="text/javascript">
function getmt(tid) {
var det = document.getElementById('mt_'+tid);
if(!det.innerHTML) {
var ajax = new tbdev_ajax();
ajax.onShow ('');
var varsString = "";
ajax.requestFile = "block-details_ajax.php";
ajax.setVar("id", tid);
ajax.setVar("action", "newmulti");
ajax.method = 'POST';
ajax.element = 'mt_'+tid;
ajax.sendAJAX(varsString); 
} else  det.innerHTML = '';
}
</script>
<?
}


echo '<script>
function multi_view() {
jQuery.post("block-details_ajax.php" , {action:"multi_view", id:"'.$id.'", crc:"'.crc32($row["info_hash"]).'"}, function(response) {
jQuery("#multi_view").html(response);
}, "html");
setTimeout("multi_view();", 120000);
}
setTimeout("multi_view();", 1500);
</script>';

tr ($tracker_lang['thistracker'], "<div id=\"multi_view\">".$tracker_lang['thistracker']." - ".$tracker_lang['load_and_update']."</div>");


if (!empty($row["times_completed"]) && $CURUSER && ($row["seeders"] == 0 || ($row["leechers"] / $row["seeders"]) >= 2) && ($row["leechers"] + $row["seeders"]) <= 2 && intval($row["seeders"] + $row["leechers"] + $row["f_seeders"] + $row["f_leechers"]) < 5)
tr($tracker_lang['is_snatched'], "<form action=\"takereseed.php\"><input type=\"hidden\" name=\"torrent\" value=\"".$id."\" /><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['back_snatched_all']."\" /></form>", 1);
			   
///���������� �������


$procent = 10; //������� ��������� �� ������� �������� (� ��) ����� ������������ (� �������) 
$needbonustot = $row["size"] / (1024*1024); 
$needbonus = round(($needbonustot*$procent/100), 2); 

if ($row["free"] == 'no' && $CURUSER) 
if (get_user_class() <= UC_MODERATOR && ($CURUSER["bonus"] >= $needbonus))
echo "<tr><td align=\"right\" class=\"heading\">".$tracker_lang['is_act_golded']."</td><td align=\"left\">".$tracker_lang['need_bonus_free'].": <b>".$needbonus."</b> <input class=\"btn\" type=\"button\" value=\"".$tracker_lang['is_act_golded']."\" onclick=\"location='get_free.php?id=".$id."'\"></td></tr>\n";

if (!empty($row["free_who"]) && $row["free"] == 'yes') {

if (!empty($free_username))
$who = "<a href=\"userdetails.php?id=".$row["free_who"]."\">" .get_user_class_color($free_class, $free_username). "</a>"; 
else {
$who = "<i>".$tracker_lang['anonymous'].": ".$row["free_who"]."</i>"; 
sql_query("UPDATE torrents SET free_who = '0' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
}

echo "<tr><td align=right>".$tracker_lang['is_act_golded']."</td><td align=\"left\"><img src='pic/freedownload.gif'> ".sprintf($tracker_lang['pay_bon_of_free'], $who, $needbonus)."</td></tr>\n"; 

}
///���������� �������					   

if ($CURUSER){

$thanked_sql = sql_query("SELECT thanks.userid, users.username, users.class FROM thanks INNER JOIN users ON thanks.userid = users.id WHERE thanks.torrentid = ".sqlesc($id), $cache = array("type" => "disk", "file" => "thanks_".$id, "time" => 60*60*48)) or sqlerr(__FILE__, __LINE__);

$count = mysql_num_rows_($thanked_sql);

if ($count == 0) {
$thanksby = $tracker_lang['none_yet'];
} else {

$thanksby_id = array();
while ($thanked_row = mysql_fetch_assoc_($thanked_sql)) {

if ($thanked_row["userid"] == $CURUSER["id"])
$can_not_thanks = true;

$thanksby_id[] = "<a href=\"userdetails.php?id=".$thanked_row["userid"]."\">".get_user_class_color($thanked_row["class"], $thanked_row["username"])."</a>";

}

}

$thanksby_id = @array_unique($thanksby_id);

if ($row["owner"] == $CURUSER["id"])
$can_not_thanks = true;

$thanksby = "<div id=\"ajax\">
".($can_not_thanks == true ? (count($thanksby_id) > 0 ? @implode(", ", $thanksby_id)." (".count($thanksby_id).")":($row["owner"] == $CURUSER["id"] ? $tracker_lang['no_owner_votes']:$tracker_lang['no_votes'])):"<form action=\"thanks.php\" method=\"post\">
<input type=\"submit\" class=\"btn\" name=\"submit\" onclick=\"send(); return false;\" value=\"".$tracker_lang['thanks']."\"".($can_not_thanks == true ? " disabled":"")." />
<input type=\"hidden\" name=\"torrentid\" value=\"".$id."\">".(count($thanksby_id) > 0 ? @implode(", ", $thanksby_id):($row["owner"] == $CURUSER["id"] ? $tracker_lang['no_owner_votes']:$tracker_lang['no_votes']))."
</form>")."
</div>";

?>
<script language="javascript" type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript">
function send() {
var ajax = new tbdev_ajax();
ajax.onShow ('');
var varsString = "";
ajax.requestFile = "thanks.php";
ajax.setVar("torrentid", <?=$id;?>);
ajax.setVar("ajax", "yes");
ajax.method = 'POST';
ajax.element = 'ajax';
ajax.sendAJAX(varsString);
}
</script>
<?

tr($tracker_lang['i_thanked'], $thanksby, 1);

}


?>
<script>
function details_online(id) {
jQuery.post("details.php", {"id":id}, function(response) {
jQuery("#details_online").html(response);
}, "html");
setTimeout("details_online('<?=$id;?>');", 60000);
}
setTimeout("details_online('<?=$id;?>');", 1500);
</script>
<?

echo "<tr><td align=\"right\" class=\"heading\">".$tracker_lang['who_viewers']."</td><td align=\"left\">
<span align=\"center\" id=\"details_online\">".$tracker_lang['who_viewers']." ".$tracker_lang['loading']."</span>
</td></tr>";


if ($CURUSER){
?>
<script type="text/javascript">
var loading = "<img src='../pic/loading_by_strong.gif'>";
jQuery(function() {
jQuery(".tab").click ( function(){
if(jQuery(this).hasClass("active"))
return;
else {
jQuery("#loading").html(loading);
var torrent = jQuery("#body").attr("torrent");
var act = jQuery(this).attr("id");
jQuery(this).toggleClass("active");
jQuery(this).siblings("span").removeClass("active");
jQuery.post("block-details_ajax.php",{"id":torrent,"action":act},function (response) {
jQuery("#body").empty();
jQuery("#body").append(response);
jQuery("#loading").empty();
});
}
});
});
</script>

<?
echo "<tr valign=\"top\"><td colspan=\"2\" align=\"center\">\n";

echo "<div id=\"tabs\">\n";
echo "<span title=\"".$tracker_lang['tab_files_info']."\" class=\"tab\" id=\"files\">".$tracker_lang['files']."</span>\n";

echo "<span title=\"".$tracker_lang['tab_seeding_info']."\" class=\"tab\" id=\"seeding\">".$tracker_lang['seed_and_leech']."</span>\n";

if ($Functs_Patch["multitracker"] == true && $row["multitracker"] == "yes")
echo "<span title=\"".$tracker_lang['tab_multi_info']."\" class=\"tab\" id=\"multi_seeding\">".$tracker_lang['multitracker']."</span>\n";

echo "<span title=\"".$tracker_lang['tab_compl_info']."\" class=\"tab\" id=\"completed\">".$tracker_lang['details_leeched']."</span>\n";
echo "<span title=\"".$tracker_lang['tab_snat_info']."\" class=\"tab\" id=\"snatched\">".$tracker_lang['tab_snatched']."</span>\n";

echo "<span title=\"".$tracker_lang['tab_rat_info']."\" class=\"tab\" id=\"rating\">".$tracker_lang['tab_rating']."</span>\n";
echo "<span title=\"".$tracker_lang['tab_thank_info']."\" class=\"tab\" id=\"thanks\">".$tracker_lang['thanks']."</span>\n";

echo "<span title=\"".$tracker_lang['tab_simuli']."\" class=\"tab\" id=\"simularall\">".$tracker_lang['tab_simularall']."</span>\n";

if (get_user_class() >= UC_MODERATOR)
echo "<span title=\"".$tracker_lang['tabs_i_refurl']."\" class=\"tab\" id=\"refurl\">".$tracker_lang['tabs_refurl']."</span>\n";

echo "<span title=\"".$tracker_lang['tabs_ired']."\" class=\"tab\" id=\"red\">".$tracker_lang['tabs_red']."</span>\n";

echo "<span title=\"".$tracker_lang['tab_repor_info']."\" class=\"tab\" id=\"reports\">".$tracker_lang['complaints']."</span>\n";

if (get_user_class() == UC_SYSOP)
echo "<span title=\"".$tracker_lang['tab_cheaters_info']."\" class=\"tab\" id=\"cheaters\">".$tracker_lang['cheat_is']."</span>\n";

echo "<span title=\"".$tracker_lang['tab_check_info']."\" class=\"tab\" id=\"checked\">".$tracker_lang['tab_checked']."</span>\n";


echo "<span id=\"loading\"></span>\n";
echo "<div id=\"body\" torrent=\"".$id."\" align=\"center\">\n";
echo $tracker_lang['click_on_choose'];
echo "</div>";
echo "</div>";

echo "</td></tr>";

}


echo "</table>";


/////////// ������� �������� ////////////
if (get_user_class() == UC_SYSOP){

echo "<form name=\"edit\" method=\"post\" action=\"takeedit.php\">";
echo "<input type=\"hidden\" name=\"id\" value=\"".$id."\">";

echo "<table style=\"margin-top: 2px;\" class=\"main\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td align=\"right\" class=\"heading\">".$tracker_lang['deleting_torrent']."</td><td align=\"left\">
<label><input type=\"checkbox\" checked name=\"reacon\" value=\"1\"/>".$tracker_lang['del_torr_reason'].":</label><br />
<label><input name=\"reasontype\" type=\"radio\" value=\"1\">".$tracker_lang['torrent_ghouls']."</label><br />
<input name=\"reasontype\" type=\"radio\" value=\"2\"><b>".$tracker_lang['torrent_dybl']."</b>: <input type=\"text\" size=\"40\" name=\"dup\">&nbsp;<i>".$tracker_lang['specify_torrent']."</i><br />
<input name=\"reasontype\" type=\"radio\" value=\"3\"><b>".$tracker_lang['torrent_banrules']."</b>: <input type=\"text\" size=\"40\" name=\"rule\">&nbsp;<i>".$tracker_lang['reason_main']."</i><br />
<input name=\"reasontype\" type=\"radio\" value=\"4\"><b>".$tracker_lang['torrent_license']."</b>: &nbsp;<i>".$tracker_lang['file_license']."</i></td></tr>";

echo "<tr><td align=\"center\" colspan=\"2\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['delete_user']."\" style=\"height: 25px; width: 150px\"></td></tr>";

echo "</td></tr></table></form>";
}
/////////// ������� �������� ////////////


$namhtml = ($row["name"]);
$namhtml = str_replace(array("'","\"","%","$","`","`"), '', $namhtml);
$namhtml = trim($namhtml);

echo "<script type=\"text/javascript\">function highlight(field) {field.focus();field.select();}</script>";
echo "<table style=\"margin-top: 2px;\" class=\"main\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=\"a\" style=\"margin: 0px; padding: 0px;\" align=\"center\">";
echo "<noindex><p align=\"center\"><b>-�- <a title=\"".$tracker_lang['random_tor']."\" rel=\"nofollow\" href=\"random.php?id=".$id."&option=random\"> &#191;?</a> -�-</b><br />
<b><a title=\"".$tracker_lang['prev_tor']."\" rel=\"nofollow\" href=\"random.php?id=".$id."&option=prev\">� ".$tracker_lang['page_prev']." -</a></b>
<b>� ".$tracker_lang['page']." �</b>
<b><a title=\"".$tracker_lang['next_tor']."\" rel=\"nofollow\" href=\"random.php?id=".$id."&option=next\">- ".$tracker_lang['page_next']." �</a></b></p></noindex>";
echo "</td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_link'].'\' onclick=\'highlight(this)\' value=\''.$namhtml.': '.$DEFAULTBASEURL.'/details.php?id='.$id.'\'>';
echo "</td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_html'].'\' onclick=\'highlight(this)\' value=\'<a href="'.$DEFAULTBASEURL.'/details.php?id='.$id.'" target="_blank">'.$namhtml.'</a>\'>';
echo "</td></tr>";

echo "<tr><td class=\"b\" align=\"center\">";
echo '<input size=\'100%\' title=\''.$tracker_lang['bbcode_forum'].'\' onclick=\'highlight(this)\' value=\'[url='.$DEFAULTBASEURL.'/details.php?id='.$id.']'.$namhtml.'[/url]\'>';
echo "</td></tr>";

echo "</table>";



if (($Torrent_Config["guest_hidecom"] == false && !$CURUSER) || ($CURUSER["hidecomment"] <> "yes" && $CURUSER)) {

echo "<p><a name=\"startcomments\"></a></p>\n";

if (!empty($row['comments']))
$count = get_row_count("comments", "WHERE torrent = ".sqlesc($id));

if (empty($CURUSER["postsperpage"]) || !$CURUSER)
$limited = 15; 
else 
$limited = $CURUSER["postsperpage"];

if (!$count) {


echo "<table style=\"margin-top: 2px;\"  class=\"main\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td align=\"left\" colspan=\"2\" class=\"colhead\">";
echo "<div style=\"float: left; width: auto;\" align=\"left\"><a name=\"#pagestart\"></a> :: ".$tracker_lang['comments_list']."</div>";

if ($row["comment_lock"] == 'no') {

if ($CURUSER["commentpos"] == 'yes')
echo "<div align=\"right\"><a href=\"#comments\">".$tracker_lang['add_comment']."</a></div>";
  
echo "</td></tr><tr><td align=\"center\">";

echo "".$tracker_lang['no_comments'].". ".($CURUSER["commentpos"] == 'yes' ? "<br />".$CURUSER['username'].", <a href=\"#comments\">".$tracker_lang['add_comment']."?</a>":"");

echo "</td></tr></table><br />";


echo "<table class=\"main\" style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">";
echo "<tr><td class=\"colhead\" align=\"left\" colspan=\"2\"><a name=\"comments\">&nbsp;</a><b>".$tracker_lang['no_comments']."</b></td></tr>";

}


echo "<tr><td align=\"center\">";
  
if (get_user_class() >= UC_MODERATOR)
echo "<br /><font size=\"-2\"><a class=\"altlink\" href=details.php?id=".$id."&lock_comments=".($row['comment_lock'] <> 'yes' ? "yes>".$tracker_lang['block_comments']: "no>".$tracker_lang['allow_comments'])."</a></font>";

if ($row['comment_lock'] == 'no') {

if ($CURUSER["commentpos"] == 'yes'){
 
echo "<form name=comment method=\"post\" action=\"comment.php?action=add\">";

echo "<table border=\"0\" align=\"center\"><tr><td class=\"clear\">";
echo "<div align=\"center\">". textbbcode("comment", "text", "", 1) ."</div>";
echo "</td></tr></table>";

echo "</td></tr><tr><td align=\"center\" colspan=\"2\">";
echo "<input type=\"hidden\" name=\"tid\" value=\"".$id."\"/>";
  
if (get_user_class() == UC_SYSOP) {
echo "<div align=\"center\"><b>".$tracker_lang['sender'].":&nbsp;&nbsp;</b>
<b>".get_user_class_color($CURUSER['class'], $CURUSER['username'])."</b>
<input name=\"sender\" type=\"radio\" value=\"self\" checked>&nbsp;&nbsp;
<font color=\"gray\">[<b>".$tracker_lang['from_system']."</b>]</font>
<input name=\"sender\" type=\"radio\" value=\"system\"><br />";
}

echo "<input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['send_comment']."\" class=btn value=\"".$tracker_lang['send_comment']."\" />"; 
echo "</td></tr></form>"; 

}
else
echo "<div align=\"center\">".$tracker_lang['commentpos_no']."</div></td></tr>"; 

echo "</table>"; 

if ($CURUSER["commentpos"] == 'yes')
$commentbar = "<p align=center><a class=index href=comment.php?action=add&tid=".$id.">".$tracker_lang['add_comment']."</a></p>\n";

} else {
echo "<table border=\"0\" align=\"center\" width=\"100%\"><tr><td class=\"a\">"; 
echo "<div align=\"center\"><b>".$tracker_lang['comment_lock']."</b></div>"; 
echo "</td></tr></table>"; 


echo "</td></tr></table><br />";

}



} else {

list($pagertop, $pagerbottom, $limit) = pager($limited, $count, "details.php?id=".$id."&", array("lastpagedefault" => ceil($count / $limited)));

$subres = sql_query("SELECT c.id, c.ip, c.text, c.user, c.added, c.editedby, c.editedat, u.avatar, u.warned, u.username, u.enabled, u.title, u.class, u.signature,u.signatrue, u.donor, u.support,u.hiderating, u.supportfor, u.downloaded, u.uploaded, u.last_access, e.username AS editedbyname, e.class AS classbyname FROM comments AS c LEFT JOIN users AS u ON c.user = u.id LEFT JOIN users AS e ON c.editedby = e.id WHERE torrent = ".sqlesc($id)." ORDER BY c.id ".$limit) or sqlerr(__FILE__, __LINE__);

$allrows = array();
while ($subrow = mysql_fetch_assoc($subres))
$allrows[] = $subrow;





echo "<table class=main cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=\"colhead\" align=\"center\">";

echo "<div style=\"float: left; width: auto;\" align=\"left\"> :: ".$tracker_lang['comments_list']."</div>";
if ($CURUSER["commentpos"] == 'yes')
echo "<div align=\"right\"><a href=#comments class=altlink_white>".$tracker_lang['add_comment']."</a></div>";

echo "</td></tr>";

echo "<tr><td>";
echo $pagertop;
echo "</td></tr>";

echo "<tr><td>";
commenttable($allrows);
echo "</td></tr>";

echo "<tr><td>";
echo $pagerbottom;
echo "</td></tr>";

echo "</table>";





if ($row['comment_lock'] == 'yes') {
echo "<table border=\"0\" align=\"center\" width=\"100%\"><tr><td class=\"a\">"; 
echo "<div align=\"center\"><b>".$tracker_lang['comment_lock']."</b></div>"; 
echo "</td></tr></table>"; 
}


if ($CURUSER["commentpos"] == 'yes' && $row['comment_lock'] == 'no'){
echo "<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">";
echo "<tr><td class=\"colhead\" align=\"center\" colspan=\"2\"><a name=comments></a><b>.::: ".$tracker_lang['add_comment']." :::.</b></td></tr>";

echo "<tr><td width=\"100%\" align=\"center\">";
echo "<form name=comment method=\"post\" action=\"comment.php?action=add\">";

echo "<table border=\"0\" align=\"center\"><tr><td class=\"clear\">";
echo "<div align=\"center\">". textbbcode("comment","text","", 1) ."</div>";
echo "</td></tr></table>";

echo "</td></tr><tr><td align=\"center\" colspan=\"2\">";
echo "<input type=\"hidden\" name=\"tid\" value=\"".$id."\"/>";

if (get_user_class() == UC_SYSOP) {
echo "<div align=\"center\"><b>".$tracker_lang['sender'].":&nbsp;&nbsp;</b>
<b>".get_user_class_color($CURUSER['class'], $CURUSER['username'])."</b>
<input name=\"sender\" type=\"radio\" value=\"self\" checked>&nbsp;&nbsp;
<font color=gray>[<b>".$tracker_lang['from_system']."</b>]</font>
<input name=\"sender\" type=\"radio\" value=\"system\"><br />";
}
  
echo "<input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['send_comment']."\" class=btn value=\"".$tracker_lang['send_comment']."\" />";
echo "</td></tr></form></table><br />";
}

}

} elseif ($CURUSER["hidecomment"] == "yes" && $CURUSER) {
echo "<table border=\"0\" align=\"center\" width=\"100%\"><tr><td class=\"a\">"; 
echo "<div align=\"center\"><b>".$tracker_lang['hide_comment_forme']." - <a title='".$tracker_lang['click_on_avatar']."' href='my.php'>".$tracker_lang['yes']."</a></b>.</div>"; 
echo "</td></tr></table><br />"; 
}


if ($CURUSER)
stdfoot();
else
stdfoot(true);

?>