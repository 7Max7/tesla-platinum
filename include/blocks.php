<?

/*
 * @copyright 2014 ��� ������ Tesla TT (Tesla Tracker) v.Platinum IV
 * ������ ������� �� 26.09.2016
*/

if(!defined('IN_TRACKER'))  die('Hacking attempt blocks');

function render_blocks($side, $blockfile, $blocktitle, $content, $bid, $bposition, $md5) {

global $SITE_Config;

if (!empty($blockfile)) {

if (file_exists(ROOT_PATH."/blocks/".$blockfile)) {

if ($SITE_Config["integrity"] == true){

$file_md5 = md5_file(ROOT_PATH."/blocks/".$blockfile);

if ($md5 == $file_md5){
define('BLOCK_FILE', 1);
require (ROOT_PATH."/blocks/".$blockfile);
} else
$content = "<center>���� ".$blockfile." �� ������ �������� �����������!</center>";

} else {
define('BLOCK_FILE', 1);
require (ROOT_PATH."/blocks/".$blockfile);
}

} else {
$content = "<center>���������� �������� � ���� ������!</center>";
//return false;
}
}


if (empty($content)) {
$temp_cache = file_get_contents(ROOT_PATH."/blocks/".$blockfile);

if (empty($temp_cache))
$content = "<center>��� ����� ��� ������� ������</center>";
else
$content = trim($temp_cache);
$side = "p";
}

switch ($side) {

case 'b': $showbanners = $content;
return null;

case 'f': $foot = $content;
return null;

case 'n': echo $content;
return null;

case 'p': return $content;

case 'o': return $blocktitle." - ".$content;
}

themesidebox($blocktitle, $content, $bposition);

return false;
}

function themesidebox($title, $content, $pos) {

global $blockfile, $b_id, $ss_uri;
static $bl_mass;

if ($pos == "s" || $pos == "o") {
if (empty($blockfile)) {
$bl_name = "fly-block-".$b_id;
} else {
$bl_name = "fly-".str_replace(".php", "", $blockfile);
}
} else {
if (empty($blockfile)) {
$bl_name = "block-".$b_id;
} else {
$bl_name = str_replace(".php", "", $blockfile);
}
}

if (!isset($bl_mass[$bl_name])) {

if (file_exists(ROOT_PATH."/themes/".$ss_uri."/html/".$bl_name.".html")) 
$bl_mass[$bl_name]['m'] = true;
else 
$bl_mass[$bl_name]['m'] = false;

}

if ($bl_mass[$bl_name]['m']) {
$thefile = addslashes(file_get_contents(ROOT_PATH."/themes/".$ss_uri."/html/".$bl_name.".html"));
$thefile = "\$r_file=\"".$thefile."\";";
eval($thefile);

if ($pos == "o")
return $r_file;
else
echo $r_file;

} else {

switch ($pos) {
case 'l': $bl_name = "block-left";
break;
case 'r': $bl_name = "block-right";
break;
case 'c': $bl_name = "block-center";
break;
case 'd': $bl_name = "block-down";
break;
default: $bl_name = "block-all";
break;
}

if (!isset($bl_mass[$bl_name])) {

if (file_exists(ROOT_PATH."/themes/".$ss_uri."/html/".$bl_name.".html")) {
$bl_mass[$bl_name]['m'] = true;
$f_str = file_get_contents(ROOT_PATH."/themes/".$ss_uri."/html/".$bl_name.".html");
$f_str = 'global $ss_uri; echo "'.addslashes($f_str)."\";";
$bl_mass[$bl_name]['f'] = create_function('$title, $content', $f_str);
} else {
$bl_mass[$bl_name]['m'] = false;
}
}

if ($bl_mass[$bl_name]['m']) {

if ($pos == "o")
return $bl_mass[$bl_name]['f']($title, $content);
else
$bl_mass[$bl_name]['f']($title, $content);

} else {

$bl_name = 'block-all';
if (!isset($bl_mass[$bl_name])) {
if (file_exists(ROOT_PATH."/themes/".$ss_uri."/html/".$bl_name.".html")) {
$bl_mass[$bl_name]['m'] = true;
$f_str = file_get_contents(ROOT_PATH."/themes/".$ss_uri."/html/".$bl_name.".html");
$f_str = 'global $ss_uri; echo "'.addslashes($f_str)." \";";
$bl_mass[$bl_name]['f'] = create_function('$title, $content', $f_str);
} else {
$bl_mass[$bl_name]['m'] = false;
}
}

if ($bl_mass[$bl_name]['m']) {

if ($pos == "o")
return $bl_mass[$bl_name]['f']($title, $content);
else
$bl_mass[$bl_name]['f']($title, $content);

} else {
echo "<fieldset><legend>".$title."</legend>".$content."</fieldset>";
}

}
}
}

$orbital_blocks = array();

function show_blocks($position) {

global $CURUSER, $use_blocks, $already_used, $orbital_blocks;

if ($use_blocks == true) {

if (!$already_used) {
$blocks_res = sql_query("SELECT * FROM orbital_blocks WHERE active = '1' ORDER BY weight ASC", $cache = array("type" => "disk", "file" => "blocks_all", "time" => 3*7200)) or sqlerr(__FILE__,__LINE__);
while ($blocks_row = mysql_fetch_assoc_($blocks_res))

$orbital_blocks[] = $blocks_row;
if (!$orbital_blocks)
$orbital_blocks = array();
$already_used = true;
}

foreach ($orbital_blocks as $block) {

$bid = $block["bid"];
$md5 = $block["md5"];

$content = $block["content"];
$title = $block["title"];
$blockfile = $block["blockfile"];
$bposition = $block["bposition"];

if ($position <> $bposition)
continue;

$view = $block["view"];
$which = explode(",", $block["which"]);
$module_name = str_replace(".php", "", basename($_SERVER["PHP_SELF"]));

if ($block["which"] == 'home') $which = array('index');

if (!(in_array($module_name, $which) || in_array("all", $which) || (in_array("ihome", $which) && $module_name == "index")))
continue;


if (isset($_SESSION['is_mobile'])){

if ($block["mobile_view"] == 'allow' AND $_SESSION['is_mobile'] == false)
continue;
elseif ($block["mobile_view"] == 'deny' AND $_SESSION['is_mobile'] == true)
continue;

}

//if ($module_name == "forums" && !in_array("forums", $which))
//continue;

if ($view == 0)
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 1 && $CURUSER)
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 2 && $CURUSER && get_user_class() >= UC_MODERATOR)
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 3 && !$CURUSER)
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 4 && $CURUSER && get_user_class() <> UC_VIP && get_user_class() < UC_MODERATOR)
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 5 && $CURUSER && get_user_class() == UC_SYSOP)
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 6 && !$CURUSER && preg_match("/bot/is", $_SERVER["HTTP_USER_AGENT"]))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 7 && !$CURUSER && !preg_match("/bot/is", $_SERVER["HTTP_USER_AGENT"]))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 8 && $CURUSER && get_user_class() == UC_UPLOADER)
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 9 && $CURUSER && ($CURUSER['uploaded']/$CURUSER['downloaded']) < 1 && !empty($CURUSER['uploaded']) && !empty($CURUSER['downloaded']))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 10 && $CURUSER && !empty($CURUSER['groups']))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 11 && $CURUSER && $CURUSER['warned'] == "yes")
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 12 && $CURUSER && $CURUSER['forum_access'] > get_date_time(gmtime() - 60*60*24*7))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 13 && $CURUSER && $CURUSER['forum_access'] < get_date_time(gmtime() - 60*60*24*7))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 14 && $CURUSER && $CURUSER['chat_access'] > get_date_time(gmtime() - 60*60*24*7))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 15 && $CURUSER && $CURUSER['chat_access'] < get_date_time(gmtime() - 60*60*24*7))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 16 && $CURUSER && $CURUSER['added'] > get_date_time(gmtime() - 60*60*24*7))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 17 && $CURUSER && $CURUSER['added'] < get_date_time(gmtime() - 60*60*24*7))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 18 && $CURUSER && ($CURUSER['uploaded']/$CURUSER['downloaded']) >= 1 && !empty($CURUSER['uploaded']) && !empty($CURUSER['downloaded']))
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 19 && $CURUSER && get_user_class() == UC_VIP)
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);
elseif ($view == 20 && $CURUSER && $CURUSER['hiderating'] == "yes")
render_blocks($side, $blockfile, $title, $content, $bid, $bposition, $md5);

}


}
}

?>