<?
require "include/bittorrent.php";
gzip();
dbconn();
loggedinorreturn();

if (isset($_POST["conusr"]) && is_array($_POST["conusr"])){

$id = (isset($_GET["id"]) ? intval($_GET["id"]):"");

sql_query("UPDATE users SET status = 'confirmed' WHERE id IN (".implode(",", array_map('intval', $_POST['conusr'])).") AND status = 'pending' ".(get_user_class() < UC_SYSOP ? "AND invitedby = ".sqlesc($CURUSER["id"]):"")) or sqlerr(__FILE__,__LINE__);

if (!empty($id)){
header("Location: invite.php?id=".$id);
die;
}

}

$type_as = (isset($_GET["type_as"]) ? (string) $_GET["type_as"]:"");
$new_do = (isset($_GET["new_do"]) ? (string) $_GET["new_do"]:"");

if (!empty($type_as) || !empty($new_do)) {

if ($type_as == "new_as") {

$id = (isset($_GET["id_as"]) ? (int) $_GET["id_as"]:0);

if (!is_numeric($id) || empty($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if ($id == 0)
$id = $CURUSER["id"];

if (get_user_class() <= UC_MODERATOR)
$id = $CURUSER["id"];

$re = sql_query("SELECT invites FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$tes = mysql_fetch_assoc($re);

if ($tes["invites"] <= 0)
stderr($tracker_lang['my_invite'], $tracker_lang['no_invites_more']);

stdhead($tracker_lang['my_invite']);

echo "<table class=\"main\" border=\"0\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
<form name=\"invite\" action=\"invite.php?new_do=yes\" method=\"post\">

<tr><td class=\"colhead\" colspan=\"7\">".$tracker_lang['invite_sendemail']."</td></tr>
<tr><td class=\"a\"><b>".$tracker_lang['email']."</b>:</td><td align=\"left\"><input name=\"email\" type=\"text\" size=\"50\"></td></tr>
<tr><td class=\"a\"><b>".$tracker_lang['add_comment']."</b>:</td><td align=\"left\"><textarea cols=\"60%\" maxLength=\"255\" rows=\"2\" name=\"textarea\"></textarea></td></tr>

<tr><td align=\"center\" colspan=\"2\"><input type=\"hidden\" name=\"id\" value=\"".$id."\" /> <input type=\"hidden\" name=\"new_do\" value=\"yes\" /> <input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['invite_sendemail']."\" /></td></tr>

</form></table>";

stdfoot();
die;

} elseif ($new_do == "yes") {

$id = (isset($_POST["id"]) ? (int) $_POST["id"]:0);

if (!is_numeric($id) || empty($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if ($id == 0)
$id = $CURUSER["id"];

$hash = md5(mt_rand(1, 1000000));

if (get_user_class() <= UC_MODERATOR)
$id = $CURUSER["id"];

$re = sql_query("SELECT invites, username FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$tes = mysql_fetch_assoc($re);

if ($tes["invites"] <= 0)
stderr($tracker_lang['my_invite'], $tracker_lang['no_invites_more']);

$email = (isset($_POST["email"]) ? htmlentities($_POST["email"]):"");
$textarea = (isset($_POST["textarea"]) ? htmlspecialchars($_POST["textarea"]):"");

if (!validemail($email))
stderr($tracker_lang['error'], $tracker_lang['validemail']);

$count = get_row_count("users", "WHERE email = ".sqlesc($email));
$count2 = get_row_count("invites", "WHERE email = ".sqlesc($email));
if (!empty($count) || !empty($count2))
stderr($tracker_lang['error'], sprintf($tracker_lang['email_isex'], $email));


$hash = md5(mt_rand(1, 1000000));
$confirmd5 = md5(mt_rand(1, 1008000000));

$name = $tes["username"];

$body = <<<EOD
��� ��������, ����� �������� $name, ���������� ��� �� ���� $SITENAME. �� ������ ��� ����� ��� ����������� ($email).

���� ��� �� ���� ����� ��� �� ������ � ��� ����, ���������� �������������� ��� ������.

������ ����������� � �����������: $textarea

��� ������������� ����� ����������� (������, ������� �� ����� ������ � ������!), ��� ����� ������ �� ��������� ������:

$DEFAULTBASEURL/takeinvite.php?psecret=$confirmd5

��� ��������������� ���: $hash (��� ������ ����������� �� �����)

����� ���� ��� �� ��� ��������, �� ������� ������������ ��� �������. �� ����������� ��� ��������� �������
� ����, ������ ��� �� ������� ������������ $SITENAME.
EOD;

$subject = <<<EOD
����������� �� $SITENAME �� $name
EOD;

if (!sent_mail($email, $SITENAME, $SITEEMAIL, $subject, $body, false))
stderr($tracker_lang['error'], $tracker_lang['enabled_sendmail']);
else {

sql_query("INSERT INTO invites (inviter, invite, time_invited, email, confirmd5) VALUES (".implode(", ", array_map("sqlesc", array($id, $hash, get_date_time(), $email, $confirmd5))).")") or sqlerr(__FILE__,__LINE__);
sql_query("UPDATE users SET invites = invites - 1 WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$id);

stdhead($tracker_lang['my_invite']);

stdmsg($tracker_lang['success'], $tracker_lang['recover_sendemail']);

}

echo "<script>setTimeout('document.location.href=\"invite.php?id=".$id."\"', 15000);</script>";

stdfoot();

}

die;
}

$id = (isset($_GET["id"]) ? (int) $_GET["id"]:0);
$type = (isset($_GET["type"]) ? (string) $_GET["type"]:"");
$invite = (isset($_GET["invite"]) ? htmlentities($_GET["invite"]):"");

if ($id == 0)
$id = $CURUSER["id"];

$res = sql_query("SELECT invites FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$inv = mysql_fetch_assoc($res);

if ($type == 'new') {

if (!is_numeric($id) || !isset($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if (empty($id))
$id = $CURUSER["id"];

if (get_user_class() <= UC_MODERATOR)
$id = $CURUSER["id"];

$re = sql_query("SELECT invites FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$tes = mysql_fetch_assoc($re);

if ($tes["invites"] <= 0)
stderr($tracker_lang['my_invite'], $tracker_lang['no_invites_more']);

$hash = md5(mt_rand(1, 1000000));

sql_query("INSERT INTO invites (inviter, invite, time_invited) VALUES (".implode(", ", array_map("sqlesc", array($id, $hash, get_date_time()))).")") or sqlerr(__FILE__,__LINE__);
sql_query("UPDATE users SET invites = invites - 1 WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$id);

header("Refresh: 0; url=invite.php?id=".$id);
die;

} elseif ($type == 'del') {

$ret = sql_query("SELECT * FROM invites WHERE invite = ".sqlesc($invite)) or sqlerr(__FILE__,__LINE__);
$num = mysql_fetch_assoc($ret);

if ($num["inviter"] == $id) {

sql_query("DELETE FROM invites WHERE invite = ".sqlesc($invite)) or sqlerr(__FILE__,__LINE__);
sql_query("UPDATE users SET invites = invites + 1 WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__,__LINE__);

unsql_cache("arrid_".$CURUSER["id"]);

header("Refresh: 0; url=\"invite.php?id=".$id."\"");
die;

} else
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

} else {

if (get_user_class() <= UC_UPLOADER && !($id == $CURUSER["id"]))
stderr($tracker_lang['error'], $tracker_lang['access_denied']);


stdhead($tracker_lang['my_invite']);


$number = get_row_count("users", "WHERE invitedby = ".sqlesc($id));

$ret = sql_query("SELECT id, username, class, status, warned, enabled, donor, email, uploaded, added FROM users WHERE invitedby = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$num = mysql_num_rows($ret);

$num_pend = 0;

if ($use_10proc == true)
echo "<table width=\"100%\" class=\"main\" cellspacing=\"0\" cellpadding=\"5\">
<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['warning']."</td></tr>
<tr><td class=\"b\" align=\"center\" valign=\"top\" width=\"50%\">".$tracker_lang['use_10proc']."</td></tr>
</table><br />";

echo("<table class=\"main\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\"><form method=\"post\" action=\"invite.php?id=".$id."\">");
echo("<tr><td class=\"colhead\" colspan=\"7\">".$tracker_lang['invite_list']." ".(!empty($number) ? "(".$number.")":"")."</td></tr>");

if (empty($num))
echo ("<tr><td colspan=\"7\">".$tracker_lang['no_data_now']."</td></tr>");
else {

echo ("<tr>
<td class=\"a\" align=\"left\"><b>".$tracker_lang['signup_username']."</b></td>
<td class=\"a\" align=\"left\"><b>".$tracker_lang['email']."</b></td>
<td class=\"a\" align=\"center\"><b>".$tracker_lang['uploaded']."</b> / <b>10%</b></td>
<td class=\"a\" align=\"center\"><b>".$tracker_lang['status']."</b></td>");

if ($CURUSER["id"] == $id || get_user_class() >= UC_SYSOP)
echo ("<td class=\"a\" align=\"center\"><b>".$tracker_lang['action']."</b></td>");

echo ("</tr>");

while ($arr = mysql_fetch_assoc($ret)) {

echo "<tr>";

if ($arr["status"] == 'pending')
echo "<td align=\"left\">".$arr["username"]."</td>";
else
echo "<td align=\"left\"><a href=\"userdetails.php?id=".$arr["id"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a> ". ($arr["warned"]  == "yes" ? "&nbsp;<img src=\"pic/warned.gif\" border=\"0\" alt=\"".$tracker_lang['warned']."\">" : "").($arr["enabled"] == "no" ? "&nbsp;<img src=\"pic/disabled.gif\" border=\"0\" alt=\"".$tracker_lang['disabled']."\">" : "").($arr["donor"]  == "yes" ? "&nbsp;<img src=\"pic/star.gif\" border=\"0\" alt=\"".$tracker_lang['donor']."\">" : "")."</td>";

print "<td>".protectmail($arr["email"])."</td>";

echo "<td align=\"center\">".mksize($arr["uploaded"])." ".($use_10proc == true ? "<b>(".mksize(round($arr["uploaded"]/10)).")</b>":"")."</td>";
echo "<td align=\"center\">".($arr["status"] == 'confirmed' ? "<ins>".$tracker_lang['confirmed']."</ins>":"<b>".$tracker_lang['pending']."</b>")."</td>";

if ($CURUSER["id"] == $id || get_user_class() == UC_SYSOP) {
echo ("<td align=\"center\">");

if ($arr["status"] == 'pending'){

echo ("<input type=\"checkbox\" name=\"conusr[]\" value=\"".$arr["id"]."\" />");
++$num_pend;

} else
echo $tracker_lang['finish_peer'];

echo ("</td>");
}

echo ("</tr>");
}
}

if (($CURUSER["id"] == $id || get_user_class() >= UC_SYSOP) && !empty($num_pend))
echo ("<tr><td colspan=\"7\" align=\"right\"><input class=\"btn\" type=\"submit\" value=\"".$tracker_lang['b_action']."\"></td></tr>");

echo ("</form></table><br />");

$number1 = get_row_count("invites", "WHERE inviter = ".sqlesc($id));

$rer = sql_query("SELECT inviteid, invite, time_invited, email FROM invites WHERE inviter = ".sqlesc($id)." AND confirmed = 'no'") or sqlerr(__FILE__,__LINE__);
$num1 = mysql_num_rows($rer);

echo "<table class=\"main\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">";
echo "<tr><td class=\"colhead\" colspan=\"6\">".$tracker_lang['invite_status']." ".(!empty($number1) ? "(".$number1.")":"")."</td></tr>";

if (empty($num1))
echo "<tr><td colspan=\"6\">".$tracker_lang['no_data_now']."</td></tr>";

else {

echo "<tr>
<td class=\"a\"><b>".$tracker_lang['signup_invite']."</b></td>
<td class=\"a\"><b>".$tracker_lang['email']."</b></td>
<td class=\"a\"><b>".$tracker_lang['clock']."</b></td>
<td class=\"a\"><b>".$tracker_lang['action']."</b></td>
</tr>";

while ($arr1 = mysql_fetch_assoc($rer)){

echo "<tr><td>".$arr1["invite"]."</td>";
echo "<td>".(empty($arr1["email"]) ? $tracker_lang['invite_normal']:protectmail($arr1["email"]))."</td>";
echo "<td>".$arr1["time_invited"]."</td>";
echo "<td><a title=\"".$tracker_lang['delete']."\" href=\"invite.php?invite=".$arr1["invite"]."&type=del\">".$tracker_lang['delete']."</a></td></tr>";

}

}

echo ("</table><br />");

echo("<table width=\"100%\" class=\"main\" cellspacing=\"0\" cellpadding=\"5\">");
echo("<tr><td class=\"colhead\" colspan=\"2\">".$tracker_lang['invite_panel']." ".(!empty($inv["invites"]) ? " (".$inv["invites"].")":"")."</td></tr>");

if (!empty($inv["invites"])){

echo "<tr>

<td class=\"b\" align=\"center\" valign=\"top\" width=\"50%\">
<form method=\"get\" action=\"invite.php\"><input type=\"hidden\" name=\"id\" value=\"".$id."\" /><input type=\"hidden\" name=\"type\" value=\"new\" /><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['invite_creating']."\" /></form>
</td>

<td class=\"b\" align=\"center\" valign=\"top\" width=\"50%\">
<form method=\"get\" action=\"invite.php\"><input type=\"hidden\" name=\"id_as\" value=\"".$id."\" /><input type=\"hidden\" name=\"type_as\" value=\"new_as\" /><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['invite_sendemail']."\" /></form>
</td>

</tr>

<tr>
<td class=\"a\" align=\"left\" valign=\"top\" width=\"50%\"><small>".$tracker_lang['invite_norm_i']."</small></td>
<td class=\"a\" align=\"left\" valign=\"top\" width=\"50%\"><small>".$tracker_lang['invite_send_i']."</small></td>
</tr>";

} else
echo "<tr><td class=\"a\">".$tracker_lang['invite_limit']."</td></tr>";

echo "</table>";

}
stdfoot();

?>