<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

$pollid = (isset($_GET['id']) ? (int) $_GET['id'] : 0);

if (!is_valid_id($pollid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['polls']);

if (get_user_class() >= UC_ADMINISTRATOR) {

$sql = sql_query("SELECT * FROM polls WHERE id = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);

while ($us_row = mysql_fetch_assoc($sql)) {

$all_option = array();

for ($i = 0; $i < 20; $i++) {
if (!empty($us_row['option'.$i]))
$all_option[$i] = $us_row['option'.$i];
}

}

echo ("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">
<tr>
<td class=\"colhead\">".$tracker_lang['option']."</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['users']."</td>
</tr>");

$num = 0;
foreach ($all_option AS $ida => $idpoll){

$sql2 = sql_query("SELECT pollanswers.*, users.username, users.class
FROM pollanswers
INNER JOIN users ON users.id = pollanswers.userid
WHERE pollid = ".sqlesc($pollid)." AND selection = ".sqlesc($ida)." ORDER BY users.id DESC", $cache = array("type" => "disk", "file" => "poll_users_".$ida, "time" => 60*60*24*2)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows_($sql2) <> 0){

$username = array();
while ($useras = mysql_fetch_assoc_($sql2))
$username[] = "<a href=\"userdetails.php?id=".$useras["userid"]."\">".get_user_class_color($useras["class"], $useras["username"])."</a>";

if (count($username))
echo "<tr><td class=\"b\">".$idpoll."</td><td class=\"a\">".implode(", ", $username)."</td></tr>";

++$num;
}

}

if (empty($num))
echo ("<tr><td class=\"b\" align=\"center\" colspan=\"2\">".$tracker_lang['no_votes']."</td></tr>");

echo ("</table><br />");
}


$ss = sql_query("SELECT comment FROM polls WHERE forum = '0' AND id = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);
$su = mysql_fetch_array($ss);

if ($su["comment"] == "no" && get_user_class() < UC_ADMINISTRATOR){
stderr($tracker_lang['error'], $tracker_lang['poll_comnot']);
die();
}

$subres = sql_query("SELECT COUNT(*) FROM comments WHERE torrent = '0' AND news = '0' AND offer = '0' and poll = ".sqlesc($pollid)) or sqlerr(__FILE__, __LINE__);
$subrow = mysql_fetch_array($subres);
$count = $subrow[0];

if ($CURUSER["postsperpage"]=="0")
$limited = 15;
else
$limited = $CURUSER["postsperpage"];

if (empty($count)) {
echo ("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
echo ("<tr><td class=colhead align=\"left\" colspan=\"2\">");
echo ("<div style=\"float: left; width: auto;\" align=\"left\"> :: ".$tracker_lang['comments_list']."</div>");

if ($CURUSER["commentpos"] == 'yes')
echo ("<div align=\"right\"><a href=#comments class=altlink_white>".$tracker_lang['add_comment']."</a></div>");

echo ("</td></tr><tr><td align=\"center\">");
echo ($tracker_lang['no_comments'].". ".($CURUSER["commentpos"] == 'yes' ? "<a href=#comments>".$tracker_lang['add_comment']."?</a>" : "")."");
echo ("</td></tr></table><br />");

} else {

list($pagertop, $pagerbottom, $limit) = pager($limited, $count, "polloverview.php?id=$pollid&", array("lastpagedefault" => 1));

$subres = sql_query("SELECT pc.id, pc.ip, pc.text, pc.user, pc.added, pc.editedby, pc.editedat, u.avatar, u.warned, u.username, u.title, u.class, u.enabled, u.donor, u.downloaded, u.signature,u.signatrue, u.uploaded, u.gender, u.last_access, e.username AS editedbyname, (SELECT class FROM users WHERE id = pc.editedby) AS classbyname
FROM comments AS pc 
LEFT JOIN users AS u ON pc.user = u.id 
LEFT JOIN users AS e ON pc.editedby = e.id 
WHERE torrent = '0' and news = '0' AND offer = '0' and poll = ".sqlesc($pollid)." ORDER BY pc.id ".$limit) or sqlerr(__FILE__, __LINE__);
$allrows = array();

while ($subrow = mysql_fetch_array($subres))
$allrows[] = $subrow;

echo ("<table class=\"main\" cellspacing=\"0\" cellPadding=\"5\" width=\"100%\" >");
echo ("<tr><td class=\"colhead\" align=\"center\">");
echo ("<div style=\"float: left; width: auto;\" align=\"left\"> :: ".$tracker_lang['comments_list']."</div>");

if ($CURUSER["commentpos"] == 'yes')
echo ("<div align=\"right\">".(get_user_class() >= UC_ADMINISTRATOR ? "<a href=\"makepoll.php?action=edit&pollid=".$pollid."\" class=\"altlink_white\">".$tracker_lang['edit']." ".$tracker_lang['poll']."</a> ":"")."<a href=\"#comments\" class=\"altlink_white\">".$tracker_lang['add_comment']."</a></div>");

if ($su["comment"] == "no")
echo "<hr>".$tracker_lang['poll_comnot'];

echo ("</td></tr>");

echo ("<tr><td>".$pagertop."</td></tr>");

echo ("<tr><td>");
echo ("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
print commenttable($allrows, "pollcomment");
echo ("</td></tr>");

echo ("<tr><td>".$pagerbottom."</td></tr>");

echo ("</table>");
}


if ($CURUSER["commentpos"] == 'yes'){

echo ("<table style=\"margin-top: 2px;\" cellpadding=\"5\" width=\"100%\">");
echo ("<form name=comment method=\"post\" action=\"pollcomment.php?action=add\">");

echo ("<tr><td class=colhead align=\"left\" colspan=\"2\"><a name=comments>&nbsp;</a><b>:: ".$tracker_lang['add_comment']."</b></td></tr>");
echo ("<tr><td width=\"100%\" align=\"center\">");

echo ("<table border=\"0\"><tr><td class=\"clear\">");
echo ("<div align=\"center\">".textbbcode("comment", "text", "", 1)."</div>");
echo ("</td></tr></table>");

echo ("</td></tr><tr><td align=\"center\" colspan=\"2\">");
echo ("<input type=\"hidden\" name=\"pid\" value=\"".$pollid."\"/>");
echo ("<input type=\"submit\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['send_comment']."\" class=btn value=\"".$tracker_lang['send_comment']."\" />");
echo ("</td></tr></form></table>");
}

stdfoot();
?>