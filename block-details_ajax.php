<?
require_once("include/bittorrent.php");
require_once('include/benc.php');

dbconn(false, true);
header("Content-Type: text/html; charset=" .$tracker_lang['language_charset']);
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


function getagent($httpagent, $peer_id = "") {

if (preg_match("/^Azureus ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]\_B([0-9][0-9|*])(.+)$)/is", $httpagent, $matches)) return "Azureus/$matches[1]";
elseif (preg_match("/^Java\/([0-9]+\.[0-9]+\.[0-9]+)/is", $httpagent, $matches)) return "Azureus/<2.0.7.0";
elseif (preg_match("/^Azureus ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/is", $httpagent, $matches)) return "Azureus/$matches[1]";
elseif (preg_match("/^BitComet (.+)$/is", $httpagent, $matches)) return "BitComet/$matches[1]";
elseif (preg_match("/^BitComet\/(.+)$/is", $httpagent, $matches)) return "BitComet/$matches[1]";
elseif (preg_match("/^BitTorrent\/(.+)$/is", $httpagent, $matches)) return "BitTorrent/$matches[1]";
elseif (preg_match("/BitTorrent\/S-([0-9]+\.[0-9]+(\.[0-9]+)*)/is", $httpagent, $matches)) return "Shadow's/$matches[1]";
elseif (preg_match("/BitTorrent\/U-([0-9]+\.[0-9]+\.[0-9]+)/is", $httpagent, $matches)) return "UPnP/$matches[1]";
elseif (preg_match("/^BitTor(rent|nado)\\/T-(.+)$/is", $httpagent, $matches)) return "BitTornado/$matches[2]";
elseif (preg_match("/^BitTornado\\/T-(.+)$/is", $httpagent, $matches)) return "BitTornado/$matches[1]";
elseif (preg_match("/^BitTorrent\/ABC-([0-9]+\.[0-9]+(\.[0-9]+)*)/is", $httpagent, $matches)) return "ABC/$matches[1]";
elseif (preg_match("/^ABC ([0-9]+\.[0-9]+(\.[0-9]+)*)\/ABC-([0-9]+\.[0-9]+(\.[0-9]+)*)/is", $httpagent, $matches)) return "ABC/$matches[1]";
elseif (preg_match("/^Python-urllib\/.+?, BitTorrent\/([0-9]+\.[0-9]+(\.[0-9]+)*)/is", $httpagent, $matches)) return "BitTorrent/$matches[1]";
elseif (preg_match("/^BitTorrent\/brst(.+)/is", $httpagent, $matches)) return "Burst";
elseif (preg_match("/^RAZA (.+)$/is", $httpagent, $matches)) return "Shareaza/$matches[1]";
elseif (preg_match("/Rufus\/([0-9]+\.[0-9]+\.[0-9]+)/is", $httpagent, $matches)) return "Rufus/$matches[1]";
elseif (preg_match("/^Python-urllib\\/([0-9]+\\.[0-9]+(\\.[0-9]+)*)/is", $httpagent, $matches)) return "G3 Torrent";
elseif (preg_match("/MLDonkey\/([0-9]+).([0-9]+).([0-9]+)*/is", $httpagent, $matches)) return "MLDonkey/$matches[1].$matches[2].$matches[3]";
elseif (preg_match("/ed2k_plugin v([0-9]+\\.[0-9]+).*/is", $httpagent, $matches)) return "eDonkey/$matches[1]";
elseif (preg_match("/uTorrent\/([0-9]+)([0-9]+)([0-9]+)([0-9A-Z]+)/is", $httpagent, $matches)) return "�Torrent/$matches[1].$matches[2].$matches[3].$matches[4]";
elseif (preg_match("/CT([0-9]+)([0-9]+)([0-9]+)([0-9]+)/is", $peer_id, $matches)) return "cTorrent/$matches[1].$matches[2].$matches[3].$matches[4]";
elseif (preg_match("/Transmission\/([0-9]+).([0-9]+)/is", $httpagent, $matches)) return "Transmission/$matches[1].$matches[2]";
elseif (preg_match("/KT([0-9]+)([0-9]+)([0-9]+)([0-9]+)/is", $peer_id, $matches)) return "KTorrent/$matches[1].$matches[2].$matches[3].$matches[4]";
elseif (preg_match("/rtorrent\/([0-9]+\\.[0-9]+(\\.[0-9]+)*)/is", $httpagent, $matches)) return "rTorrent/$matches[1]";
elseif (preg_match("/^ABC\/Tribler_ABC-([0-9]+\.[0-9]+(\.[0-9]+)*)/is", $httpagent, $matches)) return "Tribler/$matches[1]";
elseif (preg_match("/^BitsOnWheels( |\/)([0-9]+\\.[0-9]+).*/is", $httpagent, $matches)) return "BitsOnWheels/$matches[2]";
elseif (preg_match("/BitTorrentPlus\/(.+)$/is", $httpagent, $matches)) return "BitTorrent Plus!/$matches[1]";
elseif (preg_match("/^Deadman Walking/is", $httpagent, $matches)) return "Deadman Walking";
elseif (preg_match("/^eXeem( |\/)([0-9]+\\.[0-9]+).*/is", $httpagent, $matches)) return "eXeem$matches[1]$matches[2]";
elseif (preg_match("/^libtorrent\/(.+)$/is", $httpagent, $matches)) return "libtorrent/$matches[1]";
elseif (preg_match("/^0P3R4H/", $httpagent, $matches) || preg_match("/opera/is", $httpagent, $matches)) return "Opera BT Client";
elseif (preg_match("/^XBT Client/is", $httpagent, $matches)) return "XBT Client";
elseif (preg_match("/BitSpirit/is", $httpagent, $matches)) return "BitSpirit";
elseif (preg_match("/^DansClient/is", $httpagent, $matches)) return "XanTorrent";
elseif (preg_match("/^Deluge (.+)$/is", $httpagent, $matches)) return "Deluge/$matches[1]";
elseif (preg_match("/^BTAndroid\/(.+)$/is", $httpagent, $matches)) return "BTAndroid/$matches[1]";
elseif (preg_match("/Android ([0-9]+).([0-9]+).([0-9]+)/is", $httpagent, $matches)) return "Android $matches[1].$matches[2].$matches[3]";
elseif (preg_match("/^MediaGet2 (.+)$/is", $httpagent, $matches)) return "MediaGet2/$matches[1]";
elseif (preg_match("/^uTAndroid\/(.+)$/is", $httpagent, $matches)) return "uTAndroid/$matches[1]";
elseif (preg_match("/^BitTorrentMac\/(.+)$/is", $httpagent, $matches)) return "BitTorrentMac/$matches[1]";
elseif (preg_match("/^KTorrent\/(.+)$/is", $httpagent, $matches)) return "KTorrent/$matches[1]";
elseif (preg_match("/^uTorrentMac\/(.+)$/is", $httpagent, $matches)) return "BitTorrentMac/$matches[1]";
elseif (preg_match("/^BTSP\/(.+)$/is", $httpagent, $matches)) return "BTSP/$matches[1]";
elseif (preg_match("/^Flush\/(.+)$/is", $httpagent, $matches)) return "Flush/$matches[1]";
elseif (preg_match("/^iFolder BitTorrent Accelerator$/is", $httpagent, $matches)) return "iFolder BitTorrent Accelerator";
elseif (preg_match("/^Tixati\/(.+)$/is", $httpagent, $matches)) return "Tixati/$matches[1]";
elseif (preg_match("/^libtorrent\/(.+)$/is", $httpagent, $matches)) return "libtorrent/$matches[1]";
elseif (preg_match("/^rtorrent\/(.+)$/is", $httpagent, $matches)) return "rtorrent/$matches[1]";
elseif (preg_match("/^ACEStream\/ACEStream\/(.+)$/is", $httpagent, $matches)) return "ACEStream/$matches[1]";
elseif (preg_match("/^qBittorrent (.+)$/is", $httpagent, $matches)) return "qBittorrent/$matches[1]";

elseif (substr($peer_id, 0, 12 ) == "d0c") return "Mainline";
elseif (substr($peer_id, 0, 1 ) == "M") return "Mainline/Decoded";
elseif (substr($peer_id, 0, 3 ) == "-BB") return "BitBuddy";
elseif (substr($peer_id, 0, 8 ) == "-AR1001-") return "Arctic Torrent/1.2.3";
elseif (substr($peer_id, 0, 6 ) == "exbc\08") return "BitComet/0.56";
elseif (substr($peer_id, 0, 6 ) == "exbc\09") return "BitComet/0.57";
elseif (substr($peer_id, 0, 6 ) == "exbc\0:") return "BitComet/0.58";
elseif (substr($peer_id, 0, 4 ) == "-BC0") return "BitComet/0." . substr($peer_id, 5, 2 );
elseif (substr($peer_id, 0, 7 ) == "exbc\0L") return "BitLord/1.0";
elseif (substr($peer_id, 0, 7 ) == "exbcL") return "BitLord/1.1";
elseif (substr($peer_id, 0, 3 ) == "346") return "TorrenTopia";
elseif (substr($peer_id, 0, 8 ) == "-MP130n-") return "MooPolice";
elseif (substr($peer_id, 0, 8 ) == "-SZ2210-") return "Shareaza/2.2.1.0";
elseif (substr($peer_id, 0, 6 ) == "A310--") return "ABC/3.1";
else
return "Unknown (".$httpagent.")";

}


function dltable($name, $arr, $torrent) {

global $CURUSER, $tracker_lang;

if (!count($arr)){
$s.= "<tr><td class=\"a\" colspan=\"11\" align=\"center\">".($name <> $tracker_lang['details_seeding'] ? "".$tracker_lang['leeching_now'].": 0":"".$tracker_lang['seeding_now'].": 0")."</td></tr>\n";
return $s;
}

$s.= "\n";
$s.= "<tr><td class=\"a\" colspan=\"11\" align=\"center\">".($name <> $tracker_lang['details_seeding'] ? "".$tracker_lang['leeching_now'].": ".count($arr):"".$tracker_lang['seeding_now'].": ".count($arr))."</td></tr>\n";


$now = time();
$moderator = (isset($CURUSER) && get_user_class() >= UC_MODERATOR);
$mod = get_user_class() >= UC_MODERATOR;

foreach ($arr as $e) {

$s .= "<tr>\n";

if (!empty($e["username"]))
$s .= "<td><a href=\"userdetails.php?id=".$e["userid"]."\"><b>".get_user_class_color($e["class"], $e["username"])."</b></a>".($mod ? "&nbsp;[<span title=\"".$e["ip"]."\" style=\"cursor: pointer\">".$tracker_lang['user_ip']."</span>]":"")."</td>";
else
$s .= "<td>".($mod ? $e["ip"] : preg_replace('/\.\d+$/', ".xxx", $e["ip"]))."</td>";

$secs = max(10, ($e["la"]) - $e["pa"]);
$revived = $e["revived"] == "yes";

if (empty($e["port"]))
$e["port"]="no";

if (get_user_class() <= UC_MODERATOR)
$s .= "<td align=\"center\">".($e["connectable"] == "yes" ? "<span style=\"color: green; cursor: help;\" title=\"".$tracker_lang['firewall_off']."\">".$tracker_lang['yes']."</span>" : "<span style=\"color: red; cursor: help;\" title=\"".$tracker_lang['firewall_on']."\">".$tracker_lang['no']."</span>")."</td>\n";
else
$s .= "<td align=\"center\">".($e["connectable"] == "yes" ? "<span style=\"color: green; cursor: help;\" title=\"".$tracker_lang['firewall_off']."\">".$e["port"]."</span>" : "<span style=\"color: red; cursor: help;\" title=\"".$tracker_lang['firewall_on']."\">".$e["port"]."</span>")."</td>\n";


$s .= "<td align=\"right\"><nobr>" . mksize($e["uploaded"]) . "</nobr></td>\n";
$s .= "<td align=\"right\"><nobr>" . mksize($e["uploadoffset"] / $secs) . "/ ".$tracker_lang['sec']."</nobr></td>\n";
$s .= "<td align=\"right\"><nobr>" . mksize($e["downloaded"]) . "</nobr></td>\n";
$s .= "<td align=\"right\"><nobr>" . mksize($e["downloadoffset"] / $secs) . "/ ".$tracker_lang['sec']."</nobr></td>\n";

if ($e["downloaded"]) {
$ratio = floor(($e["uploaded"] / $e["downloaded"]) * 1000) / 1000;
$s .= "<td align=\"right\"><font color=".get_ratio_color($ratio).">".number_format($ratio, 3)."</font></td>\n";
} else

if ($e["uploaded"])
$s .= "<td align=\"right\">Inf.</td>\n";
else
$s .= "<td align=\"right\">---</td>\n";

$s .= "<td align=\"right\">".sprintf("%.2f%%", 100 * (1 - ($e["to_go"] / $torrent["size"])))."</td>\n";
$s .= "<td align=\"right\">".mkprettytime($now - $e["st"])."</td>\n";
$s .= "<td align=\"right\">".mkprettytime($now - $e["la"])."</td>\n";
$s .= "<td align=\"left\">".htmlspecialchars(getagent($e["agent"], $e["peer_id"]))."</td>\n";
$s .= "</tr>\n";
}
return $s;
}


function leech_sort($a,$b) {
if (isset($_GET["usort"]))
return seed_sort($a,$b);
$x = $a["to_go"];
$y = $b["to_go"];
if ($x == $y) return 0;
if ($x < $y) return -1;

return 1;
}

function seed_sort($a,$b) {
$x = $a["uploaded"];
$y = $b["uploaded"];

if ($x == $y) return 0;
if ($x < $y) return 1;

return -1;
}


$act_end = array("simular", "multi_view"); /// ���������� ��� ���� ��������

$id = (int) $_POST["id"];
$action = (string) $_POST["action"];

global $CURUSER, $announce_urls, $Torrent_Config, $redir_parse, $redir_day;

if (!$CURUSER && !in_array($action, $act_end))
die($tracker_lang['loginid']);


/// ����� ����� �� ���� �������
if ($action == "multi_view"){

$sres = sql_query("SELECT name, category, info_hash, f_seeders, f_leechers, multi_time, f_trackers, multitracker, seeders, leechers, checkpeers, f_times, times_completed FROM torrents WHERE id = ".sqlesc($id), $cache = array("type" => "disk", "file" => "multi_viewid_".$id, "time" => 60*20)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc_($sres);


if ($_POST['crc'] <> crc32($row["info_hash"]))
die($tracker_lang['no_data_now'].": crc32");


/// �������� ����� �������������, ����� ���� ��� �� 0 0 0
$dt_multi = get_date_time(gmtime() - $Torrent_Config["multihours"]*3600); // �������� ���������� ����� �� �������
if ($row["multi_time"] < $dt_multi && $row["multitracker"] == "yes" && (!empty($Torrent_Config["multihours"]) || $row["multi_time"] == "0000-00-00 00:00:00")){

$tracker_cache = array();
$f_leechers = $f_time_completed = $f_seeders = 0;

foreach ($announce_urls as $announce) {

$response = get_remote_peers($announce, $row['info_hash'],true);

if($response['state']=='ok'){

$tracker_cache[] = $response['tracker'].':'.($response['leechers'] ? $response['leechers'] : 0).':'.($response['seeders'] ? $response['seeders'] : 0).':'.($response['downloaded'] ? $response['downloaded'] : 0);

if ($f_leechers < $response['leechers'])
$f_leechers = $response['leechers'];

$f_time_completed += $response['downloaded'];

if ($f_seeders < $response['seeders'])
$f_seeders = $response['seeders'];
} else
$tracker_cache[] = $response['tracker'].':'.$response['state'];

}

$fpeers = $f_seeders + $f_leechers;
$tracker_cache = implode("\n", $tracker_cache);

$updatef = array();
$updatef[] = "f_trackers = ".sqlesc($tracker_cache);
$updatef[] = "f_leechers = ".sqlesc($f_leechers);
$updatef[] = "f_seeders = ".sqlesc($f_seeders);
$updatef[] = "f_times = ".sqlesc($f_time_completed);
$updatef[] = "multi_time = ".sqlesc(get_date_time());
$updatef[] = "visible = ".sqlesc(!empty($fpeers) ? 'yes':'no');

sql_query("UPDATE torrents SET " . implode(",", $updatef) . " WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("multi_viewid_".$id);

$row["f_seeders"] = $f_seeders;
$row["f_leechers"] = $f_leechers;
$row["multi_time"] = get_date_time();
$row["f_times"] = $f_time_completed;
$row["f_trackers"] = $tracker_cache;
}
/// �������� ����� �������������, ����� ���� ��� �� 0 0 0


//// �������� ���� ����� ������� ������ (������������ �� cleanup ��� ��� ������ �������)
$dt_multi = get_date_time(gmtime() - 1800); // ������ 30 �����

if ($row["checkpeers"] < $dt_multi && $row["multitracker"] == "no") {

$torrents = array();
$res_cle = sql_query("SELECT seeder, COUNT(*) AS c FROM peers WHERE torrent = ".sqlesc($id)." GROUP BY torrent, seeder") or sqlerr(__FILE__,__LINE__);
while ($row_cle = mysql_fetch_assoc($res_cle)) {

if ($row_cle["seeder"] == "yes")
$key = "seeders";
else
$key = "leechers";

$torrents[$id][$key] = $row_cle["c"];
}


if ($row["checkpeers"] < get_date_time(gmtime() - 1200)) {
$res_cle = sql_query("SELECT COUNT(*) AS c FROM comments WHERE torrent = ".sqlesc($id)." GROUP BY torrent") or sqlerr(__FILE__,__LINE__);
while ($row_cle = mysql_fetch_assoc($res_cle))
$torrents[$id]["comments"] = $row_cle["c"];

$fields = explode(":", "comments:leechers:seeders");
} else {
$fields = explode(":", "leechers:seeders");
}


$res_cle = sql_query("SELECT seeders, leechers, comments FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

while ($row_cle = mysql_fetch_assoc($res_cle)) {

$torr = (isset($torrents[$id]) ? $torrents[$id]:"");
foreach ($fields as $field) {
if (!isset($torr[$field]))
$torr[$field] = 0;
}

$update = array();
foreach ($fields as $field) {

$update[] = $field." = " . $torr[$field];
/// ������ ����� ��������� � ��������
if ($field == "leechers")
$row["leechers"] = $torr["leechers"];

elseif ($field = "seeders")
$row["seeders"] = $torr["seeders"];
/// ������ ����� ��������� � ��������
}

if (count($update)){
$update[] = "checkpeers = ".sqlesc(get_date_time());
sql_query("UPDATE torrents SET ".implode(", ", $update)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
unsql_cache("multi_viewid_".$id);
}

}
//sql_query("UPDATE torrents SET seeders='0', leechers='0' WHERE checkpeers = '0000-00-00 00:00:00'") or sqlerr(__FILE__,__LINE__);
}
//// �������� ���� ����� ������� ������ (������������ �� cleanup ��� ��� ������ �������)


/*
echo "<b><font color=\"".linkcolor($row["seeders"])."\">".($row["seeders"])."</font></b> ";

if ($row["multitracker"] == "yes")
echo "(<b><font color=\"".linkcolor($row["f_seeders"], true)."\">".$row["f_seeders"]."</font></b>) ";

echo "".$tracker_lang['seeders_l'].", ";
echo "<b><font color=\"".linkcolor($row["leechers"])."\">".($row["leechers"])."</font></b> ";

if ($row["multitracker"] == "yes")
echo "(<b><font color=\"".linkcolor($row["f_leechers"], true)."\">".$row["f_leechers"]."</font></b>) ";

echo $tracker_lang['leechers_l']." = ";

///f_times, times_completed
echo "<b><font color=\"".linkcolor($row["times_completed"])."\">".($row["times_completed"])."</font></b> ";

if ($row["multitracker"] == "yes")
echo "(<b><font color=\"".linkcolor($row["f_times"], true)."\">".$row["f_times"]."</font></b>) ";

echo $tracker_lang['times']." ���������";
/// ����������
*/

if (!empty($row["seeders"]) || !empty($row["f_seeders"])){
echo $tracker_lang['seeding_now'].": <b><font color=\"".linkcolor($row["seeders"])."\">".($row["seeders"])."</font></b>";

if ($row["multitracker"] == "yes" && $Functs_Patch["multitracker"] == true)
echo " (<b><font color=\"".linkcolor($row["f_seeders"], true)."\">".$row["f_seeders"]."</font></b>)";

echo ", ";
}

if (!empty($row["leechers"]) || !empty($row["f_leechers"])){
echo $tracker_lang['leeching_now'].": <b><font color=\"".linkcolor($row["leechers"])."\">".($row["leechers"])."</font></b>";

if ($row["multitracker"] == "yes" && $Functs_Patch["multitracker"] == true)
echo " (<b><font color=\"".linkcolor($row["f_leechers"], true)."\">".$row["f_leechers"]."</font></b>)";

echo ", ";
}

echo $tracker_lang['finish_peer'].": <b><font color=\"".linkcolor($row["times_completed"])."\">".($row["times_completed"])."</font></b>"; /// f_times, times_completed

if ($row["multitracker"] == "yes" && $Functs_Patch["multitracker"] == true)
echo " (<b><font color=\"".linkcolor($row["f_times"], true)."\">".$row["f_times"]."</font></b>)";

}


/// ����� ������� ������
if ($action == "simular"){

$sres = sql_query("SELECT name, category, info_hash, f_seeders, f_leechers, multi_time, f_trackers, multitracker, seeders, leechers, checkpeers, f_times, times_completed FROM torrents WHERE id = ".sqlesc($id), $cache = array("type" => "disk", "file" => "multi_viewid_".$id, "time" => 60*60*4)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc_($sres);

if ($_POST['crc'] <> crc32($row["info_hash"].$id)) die($tracker_lang['no_data_now'].": crc <> info_hash 2");

$search = htmlspecialchars_uni($row["name"]);
$search = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", $search));

$sear = array("'","\"","%","$","/","`","`","<",">",",");
$search = str_replace($sear, " ", $search);

$list = explode(" ", $search);

$listrow = $listview = array();

$num = 0;
foreach ($list AS $lis){
$strl = strlen($lis);

if ($strl >= 3) {

if ($num < 5) $listrow[] = "+".$lis; 
else $listrow[] = $lis;

if (count($listview) < 3 && $num < 3)
$listview[] = $lis; /// ��������
}

++$num;
}


if (strlen($search) >= 4){ /// ������ ������ ������ ������ 3 �������� (�� ������ mysql)

$view_uniq = trim(implode(" ", array_unique($listrow))); /// ������� ���������
//echo "1 ������, $view_uniq <br />";
$sql = sql_query("SELECT name, id, size FROM torrents WHERE MATCH (torrents.name) AGAINST ('".$view_uniq."' IN BOOLEAN MODE) ORDER BY added DESC LIMIT 15", $cache = array("type" => "disk", "file" => "simtorid_".$id, "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows_($sql) <= 1){
$view_uniq = trim(implode(" ", array_unique($listview)));
//echo "2 ������, $view_uniq <br />";
$sql = sql_query("SELECT name, id, size FROM torrents WHERE category = ".sqlesc($row["category"])." AND MATCH (torrents.name) AGAINST ('".$view_uniq."' IN BOOLEAN MODE) ORDER BY added DESC LIMIT 8", $cache = array("type" => "disk", "file" => "simtorid_alt_".$id, "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);
}

} else die($tracker_lang['short_of_data']);

$num_p = 0;
$nut = 0;

$pogre = array();

if (mysql_num_rows_($sql) > 5)
echo "<div class=\"match\" style=\"height: 150px; overflow: auto;\">";

while ($t = mysql_fetch_assoc_($sql)){

$name1 = $search;
$name2 = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", $t["name"]));
$sear = array("'","\"","%","$","/","`","`","<",">");
$name2 = str_replace($sear, " ", $name2);

$proc = @similar_text($name1, $name2);

if ($id <> $t["id"] && $proc >= $Torrent_Config["procents"]){

echo "<li><a href=\"details.php?id=".$t['id']."\" class=\"alink\">".htmlspecialchars_uni($t['name'])."</a> ".($CURUSER ? "<a title=\"".$tracker_lang['download']."\" href=\"download.php?id=".$t['id']."\">[".mksize($t["size"])."]</a>":"[".mksize($t["size"])."]")."</li>";

$pogre[] = $proc;
$num_p = 1;
++$nut;

}

}

if (mysql_num_rows_($sql) > 5)
echo "</div>";



$view_uniq = preg_replace("/\+/i", "", $view_uniq);

if (empty($nut)){

if ($proc > $Torrent_Config["procents"])
die("<i>".$tracker_lang['sum_nodata']."</i>");
else
die("<i>".sprintf($tracker_lang['accuracy_of_data'], intval($proc)."%", $Torrent_Config["procents"]."%")."</i>");

} else
die("<br /><small>".sprintf($tracker_lang['shows_data_accuracy'], @min($pogre)."%", @max($pogre)."%").": <a href=\"browse.php?search=".$view_uniq."\" title=\"".$tracker_lang['arr_searched']."\">".$view_uniq."</small>");


}
/// ����� ������� ������


elseif ($action == "newmulti"){


$fn = "torrents/".$id.".torrent";
/// �������� ���������� �� ����
if (is_file($fn) && is_readable($fn) && count($announce_urls) < 3) $dict = bdec_file($fn);
/// ���� �� ������������� � �������
if (count($dict["value"]["announce-list"]["value"])){
//$announce_urls = array(); /// ���������� ����� ������ ��������
foreach ($dict["value"]["announce-list"]["value"] AS $mnan){
if (count($announce_urls) < 5) $announce_urls[] = $mnan["value"]["0"]["value"]; /// ������� ����� ������
}

$announce_urls = array_unique($announce_urls);
}


$sql = sql_query("SELECT info_hash FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
while ($torrent = mysql_fetch_assoc($sql)) {

$tracker_cache = array();
$f_leechers = $f_time_completed = $f_seeders = 0;

foreach ($announce_urls as $announce) {
$response = get_remote_peers($announce, $torrent['info_hash'], true);

if ($response['state']=='ok'){
$tracker_cache[] = $response['tracker'].':'.($response['leechers'] ? $response['leechers'] : 0).':'.($response['seeders'] ? $response['seeders'] : 0).':'.($response['downloaded'] ? $response['downloaded'] : 0);
// $f_leechers += $response['leechers'];
//$f_seeders += $response['seeders'];
$f_time_completed += $response['downloaded'];
if ($f_leechers < $response['leechers']) /// ��� ����� max ��������
$f_leechers = $response['leechers'];

if ($f_seeders < $response['seeders']) /// ��� ����� max ��������
$f_seeders = $response['seeders'];
}
else
$tracker_cache[] = $response['tracker'].':'.$response['state'];
}



$fpeers = $f_seeders + $f_leechers;
$list = $tracker_cache;
$tracker_cache = implode("\n", $tracker_cache);
$updatef = array();
$updatef[] = "f_trackers = ".sqlesc($tracker_cache);
$updatef[] = "f_leechers = ".sqlesc($f_leechers);
$updatef[] = "f_seeders = ".sqlesc($f_seeders);
$updatef[] = "multi_time = ".sqlesc(get_date_time());
$updatef[] = "visible = ".sqlesc(!empty($fpeers) ? 'yes':'no');
$updatef[] = "f_times = ".sqlesc($f_time_completed);

sql_query("UPDATE torrents SET " . implode(",", $updatef) . " WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("multi_viewid_".$id);

echo "<table width=\"100%\" class=\"main\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n";

$s4etik = 0;

foreach ($list AS $lisar){

if ($s4etik % 2 == 0) $clas_tdi = "class=\"a\""; else $clas_tdi = "class=\"b\"";

$ex_lode = explode(":", $lisar);

$li_text = array("off" => $tracker_lang['multi_off'], "false" => $tracker_lang['multi_false'], "timeout" => $tracker_lang['multi_timeout']);

$status = '';
if (in_array($ex_lode[1], array("off","timeout","false")))
$status = $li_text[$ex_lode[1]];

echo "<tr>
<td $clas_tdi align=left>".$ex_lode[0]."</td>
<td $clas_tdi align=\"center\">".(!empty($status) ? "-":"<b><font color=\"".linkcolor($ex_lode[2], true)."\">".$ex_lode[2]."</font></b>")."</td>
<td $clas_tdi align=\"center\">".(!empty($status) ? "-":"<b><font color=\"".linkcolor($ex_lode[1], true)."\">".$ex_lode[1]."</font></b>")."</td>
<td $clas_tdi align=\"center\">".(!empty($status) ? "-":"<b><font color=\"".linkcolor($ex_lode[3], true)."\">".$ex_lode[3]."</font></b>")."</td>
<td $clas_tdi align=\"center\">".(!empty($status) ? $status:$tracker_lang['success_work'])."</td>
</tr>\n";
++$s4etik;
}

echo "<tr><td class=\"a\" align=\"center\" colspan=\"5\">".$tracker_lang['multi_check']."</td></tr>";

echo "</table>\n";

echo '<script>setTimeout("multi_view();", 1500);</script>';

}

}

/// ����� ������� ������
elseif ($action == "simularall"){


$sql = sql_query("SELECT name FROM torrents WHERE id = ".sqlesc($id)." LIMIT 1") or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_assoc($sql);

$search = htmlspecialchars_uni($row["name"]);

$search = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", $search));

$sear = array("'","\"","%","$","/","`","`","<",">",",");
$search = str_replace($sear, " ", $search);

$list = explode(" ", $search);

$listrow = $listview = array();

$num = 0;
foreach ($list AS $lis){
$strl = strlen($lis);

if ($strl >= 3) {

if ($num < 5) $listrow[] = "+".$lis; 
else $listrow[] = $lis;

if (count($listview) < 3 && $num < 3)
$listview[] = $lis; /// ��������
}

++$num;
}


if (strlen($search) >= 4){ /// ������ ������ ������ ������ 3 �������� (�� ������ mysql)

$view_uniq = trim(implode(" ", array_unique($listrow))); /// ������� ���������
//echo "1 ������, $view_uniq <br />";
$sql = sql_query("SELECT name, (leechers+f_leechers) AS leechers, (seeders+f_seeders) AS seeders, size, id FROM torrents WHERE MATCH (torrents.name) AGAINST ('".$view_uniq."' IN BOOLEAN MODE) ORDER BY added DESC LIMIT 50") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($sql) <= 1){
$view_uniq = trim(implode(" ", array_unique($listview)));
//echo "2 ������, $view_uniq <br />";
$sql = sql_query("SELECT name, (leechers+f_leechers) AS leechers, (seeders+f_seeders) AS seeders, size, id FROM torrents WHERE category = ".sqlesc($row["category"])." AND MATCH (torrents.name) AGAINST ('".$view_uniq."' IN BOOLEAN MODE) ORDER BY added DESC LIMIT 50") or sqlerr(__FILE__, __LINE__);
}

} else die($tracker_lang['short_of_data']);


echo "<table width=\"100%\" class=\"main\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n";

echo "<tr><td class=\"a\" align=\"center\" colspan=\"5\">".$tracker_lang['tab_simuli']."</td></tr>";

if (mysql_num_rows($sql) <> 0)
echo "<tr>
<td class=\"colhead\" align=\"left\">".$tracker_lang['name']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['tracker_seeders']." / ".$tracker_lang['tracker_leechers']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['download']." / ".$tracker_lang['size']."</td>
</tr>\n";

$num_p = 0;
$nut = 0;

$pogre = array();
$view_uniq = preg_replace("/\+/i", "", $view_uniq);
$s4etik = 0;

if ($s4etik % 2 == 0) $clas_tdi = "class=\"a\""; else $clas_tdi = "class=\"b\"";

while ($t = mysql_fetch_assoc($sql)){

$name1 = $search;
$name2 = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", $t["name"]));

$proc = @similar_text($name1, $name2);

if ($id <> $t["id"] && $proc >= $Torrent_Config["procents"]){

echo "<tr>
<td $clas_tdi align=\"left\"><a href=\"details.php?id=".$t['id']."\" class=\"alink\">".htmlspecialchars_uni($t['name'])."</a></td>
<td $clas_tdi align=\"center\"><font color=\"".linkcolor($t["seeders"])."\">".$t["seeders"]."</font> / <font color=\"".linkcolor($t["leechers"])."\">".$t["leechers"]."</font></td>
<td $clas_tdi align=\"center\"><a title=\"".$tracker_lang['download']."\" href=\"download.php?id=".$t['id']."\">".$tracker_lang['download']."</a> / ".mksize($t["size"])."</td>
</tr>";

++$s4etik;

$pogre[] = $proc;
$num_p = 1;
++$nut;

}

}

echo "<tr><td class=\"a\" align=\"center\" colspan=\"5\">".$tracker_lang['search_results_for'].": <a href=\"browse.php?search=".$row["name"]."\" title=\"".$tracker_lang['arr_searched']."\">".$row["name"]."</a></td></tr>";

echo "</table>\n";

if (empty($nut)){

if ($proc > $Torrent_Config["procents"])
die("<i>".$tracker_lang['sum_nodata']."</i>");
else
die("<i>".sprintf($tracker_lang['accuracy_of_data'], intval($proc)."%", $Torrent_Config["procents"]."%")."</i>");

} else
die("<br /><small>".sprintf($tracker_lang['shows_data_accuracy'], @min($pogre)."%", @max($pogre)."%").": <a href=\"browse.php?search=".$view_uniq."\" title=\"".$tracker_lang['arr_searched']."\">".$view_uniq."</small>");

}
/// ����� ������� ������

elseif ($action == "checked"){

$res = sql_query("SELECT name, multitracker FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc($res);

$fn = "torrents/".$id.".torrent";

if (!$row || !is_file($fn) || !is_readable($fn))
die($tracker_lang['torrent_filedel']);

$dict = bdec_file($fn);
list($info) = dict_check_t($dict, "info");


/// ������� �������� �������� ���� ������� ��� ������ torrent ����� ����� ��������� ������ - ������ ��� � ������� ������
if (!empty($dict) && count($dict)){

$upd_sql = false;
if (!empty($dict['value']['info']['value']['private']['value']) && $row['multitracker'] == 'yes'){
sql_query("UPDATE torrents SET multitracker = 'no', multi_time = '0000-00-00 00:00:00' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$upd_sql = true;
} elseif (empty($dict['value']['info']['value']['private']['value']) && $row['multitracker'] == 'no'){
sql_query("UPDATE torrents SET multitracker = 'yes' WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
$upd_sql = true;
}

if ($upd_sql == true)
echo "<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr><td align=\"center\"><h1>".$tracker_lang['torrent_updated']."</h1></td></tr></table>\n";
}
/// ������� �������� �������� ���� ������� ��� ������ torrent ����� ����� ��������� ������ - ������ ��� � ������� ������


echo "<table width=\"100%\" class=\"main\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n";

echo "<tr><td class=\"a\" align=\"center\" colspan=\"2\">".$tracker_lang['tab_check_info']."</td></tr>";

echo "<tr>
<td align=\"left\">".$tracker_lang['hash_sum']."</td>
<td align=\"left\">".sha1($info["string"])."</td>
</tr>\n";


echo "<tr>
<td align=\"left\">".$tracker_lang['utorrent_added']."</td>
<td align=\"left\">".get_date_time($dict["value"]["creation date"]["value"])." (".get_elapsed_time(sql_timestamp_to_unix_timestamp(get_date_time($dict["value"]["creation date"]["value"])))." ".$tracker_lang['ago'].")</td>
</tr>\n";

echo "<tr>
<td align=\"left\">".$tracker_lang['privat_torrent']."</td>
<td align=\"left\">".(!empty($dict['value']['info']['value']['private']['value']) ? "<font color=\"red\"><b>".$tracker_lang['yes']."</b></font>":"<font color=\"green\"><b>".$tracker_lang['no']."</b></font> [".$tracker_lang['local_dht']."]")."</td>
</tr>\n";

if (!empty($dict["value"]["created by"]["value"]))
echo "<tr>
<td align=\"left\">".$tracker_lang['utorrent_prog']."</td>
<td align=\"left\">".htmlspecialchars($dict["value"]["created by"]["value"])."</td>
</tr>\n";

if (!empty($dict["value"]["encoding"]["value"]))
echo "<tr>
<td align=\"left\">".$tracker_lang['encoding']."</td>
<td align=\"left\">".htmlspecialchars($dict["value"]["encoding"]["value"])."</td>
</tr>\n";

echo "<tr>
<td align=\"left\">".$tracker_lang['utorrent_segment']."</td>
<td align=\"left\">".mksize($dict['value']['info']['value']['piece length']['value'])."</td>
</tr>\n";

echo "<tr>
<td align=\"left\">".$tracker_lang['size']." .torrent</td>
<td align=\"left\">".mksize(filesize($fn))."</td>
</tr>\n";

if (!empty($dict['value']['info']['value']['length']['value']))
echo "<tr>
<td align=\"left\">".$tracker_lang['utorrent_allfiles']."</td>
<td align=\"left\">".mksize($dict['value']['info']['value']['length']['value'])."</td>
</tr>\n";


if (!empty($dict["value"]["announce"]["value"]) && get_user_class() == UC_SYSOP){

echo "<tr>
<td align=\"left\">".$tracker_lang['utorrent_announce']."</td>
<td align=\"left\">".$dict["value"]["announce"]["value"]."</td>
</tr>\n";

}


if (count($dict["value"]["announce-list"]["value"]) && get_user_class() == UC_SYSOP){

foreach ($dict["value"]["announce-list"]["value"] AS $mnan){
$anno_torrent[] = $mnan["value"]["0"]["value"];
}

if (count($anno_torrent))
echo "<tr>
<td align=\"left\">".$tracker_lang['multi_announce']."</td>
<td align=\"left\">".implode("<br />", $anno_torrent)."</td>
</tr>\n";

}


if (!empty($dict["value"]['comment'])){

if (!empty($dict["value"]['publisher']["value"]))
$comments[] = format_urls(htmlspecialchars($dict["value"]['publisher']["value"]));
if (!empty($dict["value"]['comment']["value"]))
$comments[] = format_urls(htmlspecialchars($dict["value"]['comment']["value"]));
if (!empty($dict["value"]['publisher-url']["value"]))
$comments[] = format_urls(htmlspecialchars($dict["value"]['publisher-url']["value"]));

$comments = array_unique($comments);

if (count($comments))
echo "<tr>
<td align=\"left\">".$tracker_lang['utorrent_note']."</td>
<td align=\"left\">".(implode("<br />", $comments))."</td>
</tr>\n";

}


$source = $dict['value']['info']['value']['source']['value'];
if (!empty($dict['value']['source']['value']))
$source = $dict['value']['source']['value'];

if (!empty($source))
echo "<tr>
<td align=\"left\">".$tracker_lang['utorrent_copyright']."</td>
<td align=\"left\">".(format_urls($source))."</td>
</tr>\n";


echo "</table>\n";

}


elseif ($action == "files"){


echo "<table class=\"main\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=\"b\" colspan=\"2\" align=\"center\">".$tracker_lang['tab_files_info']."</td></tr>\n";

$sres = sql_query("SELECT numfiles FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$srow = mysql_fetch_assoc($sres);

$fn = "torrents/".$id.".torrent";
if (file_exists($fn))
$all_profiles = dict_profiles(bdec_file($fn), true);


$subres = sql_query("SELECT * FROM files WHERE torrent = ".sqlesc($id)." ORDER BY filename ASC") or sqlerr(__FILE__, __LINE__);

echo "<tr>
<td class=\"colhead\">".$tracker_lang['name']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['size']."</td>
</tr>\n";

$s4e = 0;
while ($subrow = mysql_fetch_assoc($subres)) {

if ($s4e % 2 == 0)
$clas_t_d="class=\"a\"";
else 
$clas_t_d="class=\"b\"";

echo "<tr>
<td ".$clas_t_d.">".$subrow["filename"]." ".(isset($all_profiles[$s4e]) ? "(".implode(", ", $all_profiles[$s4e]).")":"")."</td>
<td ".$clas_t_d." align=\"right\">".mksize($subrow["size"])."</td>
</tr>\n";

++$s4e;
}

if (empty($srow["numfiles"]) && !empty($s4e))
sql_query("UPDATE torrents SET numfiles = ".sqlesc($s4e)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

if (empty($s4e)){

$ifilename = ROOT_PATH."/torrents/".$id.".torrent";

if (@file_exists($ifilename)){

$dict = bdec_file($ifilename);
list($info) = dict_check_t($dict, "info");
list($dname, $plen, $pieces) = @dict_check_t($info, "name(string):piece length(integer):pieces(string)");
$filelist = array();
$totallen = @dict_get_t($info, "length", "integer");
if (isset($totallen))
$filelist[] = array($dname, $totallen);
else {
$flist = @dict_get_t($info, "files", "list");
$totallen = 0;

if (count($flist)){
foreach ($flist as $sf) {
list($ll, $ff) = @dict_check_t($sf, "length(integer):path(list)");
$totallen += $ll;
$ffa = array();
foreach ($ff as $ffe) {
$ffa[] = $ffe["value"];
}
$filelist[] = array(implode("/", $ffa), $ll);
}
}
}

$dict=@bdec(@benc($dict));
@list($info) = @dict_check_t($dict, "info");
$infohash = sha1($info["string"]);
$size=0;
if (!empty($totallen)){
sql_query("DELETE FROM files WHERE torrent = ".sqlesc($id));

foreach ($filelist as $file) {
$file[0]=utf8_to_win($file[0]);
$size=$size+$file[1];

echo "<tr>
<td ".$clas_t_d.">".$file[0]." ".(isset($all_profiles[$s4e]) ? "(".implode(", ", $all_profiles[$s4e]).")":"")."</td>
<td ".$clas_t_d." align=\"right\">".mksize($file[1])."</td>
</tr>\n";

sql_query("INSERT INTO files (torrent, filename, size) VALUES (".sqlesc($id).", ".sqlesc($file[0]).",".sqlesc($file[1]).")");
}
}
}

}

echo "</table>\n";


} elseif ($action == "seeding"){


$res = sql_query("SELECT size FROM torrents WHERE id = ".sqlesc($id))  or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc($res);

$downloaders = array();
$seeders = array();
$subres = sql_query("SELECT seeder, downloadoffset, uploadoffset, peers.ip, port, peers.uploaded, peers.downloaded, to_go, UNIX_TIMESTAMP(started) AS st, connectable, agent, peer_id, UNIX_TIMESTAMP(last_action) AS la, UNIX_TIMESTAMP(prev_action) AS pa, userid, users.username, users.class
FROM peers
LEFT JOIN users ON peers.userid = users.id
WHERE torrent = ".sqlesc($id)." LIMIT 250") or sqlerr(__FILE__, __LINE__);

while ($subrow = mysql_fetch_assoc($subres)) {
if ($subrow["seeder"] == "yes")
$seeders[] = $subrow;
else
$downloaders[] = $subrow;
}

usort($seeders, "seed_sort");
usort($downloaders, "leech_sort");

echo "<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">\n";

echo "<tr><td class=\"b\" colspan=\"11\" align=\"center\">".$tracker_lang['tab_seeding_info']." ".sprintf($tracker_lang['limit_ofn'], 250)."</td></tr>\n";

echo "<tr><td class=\"colhead\">".$tracker_lang['user']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['port_open']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['uploaded']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['ul_speed']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['downloaded']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['dl_speed']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['ratio']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['completed']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['connected']."</td>
<td class=\"colhead\" align=\"right\">".$tracker_lang['idle']."</td>
<td class=\"colhead\" align=left>".$tracker_lang['client']."</td></tr>\n";

echo dltable($tracker_lang['details_seeding'], $seeders, $row);
echo dltable($tracker_lang['details_leeching'], $downloaders, $row);

echo "</table>\n";


} elseif ($action == "completed") {

echo "<table class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">\n";

echo "<tr><td align=\"center\" colspan=\"8\" class=\"a\" align=\"center\">".$tracker_lang['tab_compl_info']." ".sprintf($tracker_lang['limit_ofn'], 250)."</td></tr>\n";

$res = sql_query("SELECT size FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc($res);

$size_to_go = $row["size"];

$subres = sql_query("SELECT s.seeder,s.uploaded,s.downloaded,s.startdat,s.last_action,s.port,s.to_go,s.completedat,s.finished,s.connectable, u.username,  u.class, u.id FROM snatched AS s
INNER JOIN users AS u ON s.userid = u.id WHERE s.finished='yes' AND s.torrent = ".sqlesc($id)." ORDER BY s.startdat LIMIT 250") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($subres) == 0)
echo "<tr><td align=\"center\" colspan=\"8\" class=\"a\" align=\"center\">".($tracker_lang['no_user_for_rules'].": ".$action)."</td></tr>\n";
else {
echo "<tr>
<td align=\"center\" class=\"colhead\">".$tracker_lang['signup_username']."/����</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['uploaded']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['downloaded']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['percent']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['begin']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['action']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['completed']."</td>
<td align=\"center\" class=\"colhead\" align=\"right\">".$tracker_lang['filled']."</td>
</tr>\n";
}

$s4et=0;

while ($subrow = mysql_fetch_assoc($subres)) {

$seeder = ($subrow["seeder"]=="yes" ? "<b>".$tracker_lang['yes']."</b>": "<i>".$tracker_lang['no']."</i>");
$who = (isset($subrow["username"]) ? ("<a href=userdetails.php?id=" .$subrow["id"] . ">" .get_user_class_color($subrow["class"],  $subrow["username"]). "</a>") : "<i>".$tracker_lang['anonymous']." [".$subrow["id"]."]</i>");

$s_port= ($subrow["connectable"] == "yes" ? "<span style=\"color: green; cursor: help;\" title=\"".$tracker_lang['firewall_off']."\">".$subrow["port"]."</span>" : "<span style=\"color: red; cursor: help;\" title=\"".$tracker_lang['firewall_on']."\">".$subrow["port"]."</span>");

if ($subrow["finished"]=="yes" || $subrow["seeder"]=="yes")
$finish = "<b>".$tracker_lang['yes']."</b>";
else
$finish = "<i>".$tracker_lang['no']."</i>";

if ($subrow["completedat"]=="0000-00-00 00:00:00")
$completedat = "<i>".$tracker_lang['no']."</i>";
else
$completedat = $subrow["completedat"];


if ($subrow["seeder"]=="no"){

if ($subrow["to_go"]>"0")
$size_togo=number_format(100 * (1 - $subrow["to_go"]/$size_to_go), 1);
else
$size_togo="0";
} else
$size_togo="100";

if ($s4et % 2 == 0)	$clas_td="class=\"a\""; else $clas_td="class=\"b\"";

echo "<tr>
<td ".$clas_td." align=\"center\">".$who ." <br />". $s_port."</td>
<td ".$clas_td." align=\"center\">".mksize($subrow["uploaded"])."</td>
<td ".$clas_td." align=\"center\">".mksize($subrow["downloaded"])."</td>
<td ".$clas_td." align=\"center\">".$size_togo."% </td>
<td ".$clas_td." align=\"center\">".$subrow["startdat"]."</td>
<td ".$clas_td." align=\"center\">".$subrow["last_action"]."</td>
<td ".$clas_td." align=\"center\">".$completedat."</td>
<td ".$clas_td." align=\"center\">".$finish." / ".$seeder."</td>
</tr>\n";

++$s4et;
}
echo "</table>\n";

}

elseif ($action == "snatched"){

$res = sql_query("SELECT users.id, users.username, users.title, users.donor, users.enabled, users.warned, users.last_access, users.class, snatched.startdat, snatched.last_action, snatched.completedat, snatched.seeder, snatched.userid, snatched.uploaded AS sn_up, snatched.downloaded AS sn_dn FROM snatched
INNER JOIN users ON snatched.userid = users.id
WHERE snatched.finished = 'no' AND snatched.torrent = ".sqlesc($id)." ORDER BY users.class DESC LIMIT 500") or sqlerr(__FILE__,__LINE__);

echo "<table width=100% class=main border=0 cellspacing=0 cellpadding=5>\n";

echo "<tr><td align=\"center\" colspan=\"8\" class=\"a\" align=\"center\">".$tracker_lang['tab_snat_info']." ".sprintf($tracker_lang['limit_ofn'], 500)."</td></tr>\n";

if (mysql_num_rows($res) == 0)
echo "<tr><td align=\"center\" colspan=\"8\" class=\"a\" align=\"center\">".($tracker_lang['no_user_for_rules'].": ".$action)."</td></tr>\n";
else {

echo "<tr>
<td class=\"colhead\" align=\"center\">".$tracker_lang['signup_username']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['uploaded']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['downloaded']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['ratio']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['begin']." / ".$tracker_lang['completed']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['action']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['uploadeder']."</td>
</tr>";

}

$s4etik=0;

while ($arr = mysql_fetch_assoc($res)) {

if ($s4etik % 2 == 0) $clas_tdi="class=\"a\""; else $clas_tdi="class=\"b\"";

//start torrent
if ($arr["sn_dn"] > 0) {
$ratio2 = number_format($arr["sn_up"] / $arr["sn_dn"], 2);
$ratio2 = "<font color=" . get_ratio_color($ratio2) . ">".$ratio2."</font>";
}
else
if ($arr["sn_up"] > 0)
$ratio2 = "Inf.";
else
$ratio2 = "---";

$uploaded2 = mksize($arr["sn_up"]);
$downloaded2 = mksize($arr["sn_dn"]);

$snatched_small[] = "<a href=userdetails.php?id=".$arr["userid"].">".get_user_class_color($arr["class"], $arr["username"])." <b>[</b><font color=" . get_ratio_color($ratio) . ">".$ratio."</font><b>]</b></a>";

echo "<tr>
<td ".$clas_tdi." align=\"center\"><a href=\"userdetails.php?id=".$arr["userid"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a>".get_user_icons($arr)."</td>
<td ".$clas_tdi." align=\"center\"><nobr>".$uploaded2."</nobr></td>
<td ".$clas_tdi." align=\"center\"><nobr>".$downloaded2."</nobr></td>
<td ".$clas_tdi." align=\"center\"><nobr>".$ratio2."</td>
<td ".$clas_tdi." align=\"center\"><nobr>" .$arr["startdat"] . "<br />" . $arr["completedat"] . "</nobr></td>
<td ".$clas_tdi." align=\"center\"><nobr>" .$arr["last_action"] . "</nobr></td>
<td ".$clas_tdi." align=\"center\"><nobr>" .($arr["seeder"] == "yes" ? "<b><font color=green>".$tracker_lang['yes']."</font>" : "<font color=red>".$tracker_lang['no']."</font></b>") ."</nobr></td>
</tr>\n";

++$s4etik;

}

echo"</table>\n";


} elseif ($action == "rating"){


$res = sql_query("SELECT ratings.user, ratings.rating, ratings.added, users.username, users.class FROM ratings
INNER JOIN users ON users.id = ratings.user
WHERE torrent = ".sqlesc($id)." ORDER BY ratings.added LIMIT 250", $cache = array("type" => "disk", "file" => "ratings_".$id, "time" => 60*60*5)) or sqlerr(__FILE__, __LINE__);

echo "<table width=\"100%\" class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">\n";

echo "<tr><td class=\"a\" align=\"center\" colspan=\"3\">".$tracker_lang['tab_rat_info']." ".sprintf($tracker_lang['limit_ofn'], 250)."</td></tr>";

if (mysql_num_rows_($res) == 0)
echo "<tr><td align=\"center\" colspan=\"8\" class=\"a\" align=\"center\">".($tracker_lang['no_user_for_rules'].": ".$action)."</td></tr>\n";
else {
echo "<tr>
<td class=\"colhead\" align=\"center\">".$tracker_lang['signup_username']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['news_added']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['tab_rating']."</td>
</tr>";
}

$s4etik=0;
while ($arr = mysql_fetch_assoc_($res)) {

if ($s4etik % 2 == 0) $clas_tdi="class=\"a\""; else $clas_tdi="class=\"b\"";

echo "<tr>
<td $clas_tdi align=\"center\"><a href=\"userdetails.php?id=".$arr["user"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a></td>
<td $clas_tdi align=\"center\">".$arr["added"]."</td>
<td $clas_tdi align=\"center\">".pic_rating_b(10,$arr["rating"])."</td>
</tr>\n";
++$s4etik;
}

echo "</table>\n";


} elseif ($action == "thanks"){

$thanked_sql = sql_query("SELECT thanks.userid, users.username, users.class
FROM thanks
INNER JOIN users ON thanks.userid = users.id
WHERE thanks.torrentid = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($thanked_sql) == 0)
die($tracker_lang['no_user_for_rules'].": ".$action);

echo "<table width=100% class=main border=0 cellspacing=0 cellpadding=5>\n";

echo "<tr><td class=\"a\" align=\"center\">".$tracker_lang['tab_thank_info']."</td></tr>";

$thanked = array();
while ($thanked_row = mysql_fetch_assoc($thanked_sql)) {
$thanked[] = "<a href=\"userdetails.php?id=".$thanked_row["userid"]."\">".get_user_class_color($thanked_row["class"], $thanked_row["username"])."</a>";
}

if (count($thanked))
echo "<tr><td class=\"b\" align=left>".implode(", ", $thanked)."</td></tr>";
else
echo ($tracker_lang['no_user_for_rules'].": ".$action);

echo "</table>\n";


} elseif ($action == "multi_seeding"){

/// ��������������� �������
$sres = sql_query("SELECT info_hash, f_seeders, f_leechers, multi_time, f_trackers, multitracker FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc($sres);

if ($row["multitracker"] == "no")
die($tracker_lang['no_user_for_rules'].": ".$action);


$list = explode("\n", $row["f_trackers"]);

echo "<table width=100% class=main border=0 cellspacing=0 cellpadding=5>\n";

echo "<tr><td class=\"a\" align=\"center\" colspan=\"5\">".$tracker_lang['tab_multi_info']."</td></tr>";

echo "<tr>
<td class=\"colhead\" width=40% align=\"center\">".$tracker_lang['domen']."</td>
<td class=\"colhead\" width=10% align=\"center\">".$tracker_lang['seeding_now']."</td>
<td class=\"colhead\" width=10% align=\"center\">".$tracker_lang['leeching_now']."</td>
<td class=\"colhead\" width=10% align=\"center\">".$tracker_lang['finish_peer']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['status']."</td>
</tr>";

$s4etik = 0;

foreach ($list AS $lisar){

if ($s4etik % 2 == 0) $clas_tdi = "class=\"a\""; else $clas_tdi = "class=\"b\"";

$ex_lode = explode(":", $lisar);

$li_text = array("off" => $tracker_lang['multi_off'], "false" => $tracker_lang['multi_false'], "timeout" => $tracker_lang['multi_timeout']);

$status = '';
if (in_array($ex_lode[1], array("off","timeout","false")))
$status = $li_text[$ex_lode[1]];

echo "<tr>
<td $clas_tdi align=left>".$ex_lode[0]."</td>
<td $clas_tdi align=\"center\">".(!empty($status) ? "-":"<b><font color=\"".linkcolor($ex_lode[2], true)."\">".$ex_lode[2]."</font></b>")."</td>
<td $clas_tdi align=\"center\">".(!empty($status) ? "-":"<b><font color=\"".linkcolor($ex_lode[1], true)."\">".$ex_lode[1]."</font></b>")."</td>
<td $clas_tdi align=\"center\">".(!empty($status) ? "-":"<b><font color=\"".linkcolor($ex_lode[3], true)."\">".$ex_lode[3]."</font></b>")."</td>
<td $clas_tdi align=\"center\">".(!empty($status) ? $status:$tracker_lang['success_work'])."</td>
</tr>\n";
++$s4etik;
}

echo "<tr><td class=\"a\" align=\"center\" colspan=\"5\">".$row["multi_time"]." (".get_elapsed_time(sql_timestamp_to_unix_timestamp($row["multi_time"])) . " ".$tracker_lang['ago'].")</td></tr>";


if (empty($Torrent_Config["multihours"]))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"5\">".$tracker_lang['warning'].": ".$tracker_lang['click_on_multihours']."</td></tr>";

echo "<tr><td class=\"b\" align=\"center\" colspan=\"5\"><a style=\"cursor: pointer;\" onclick=\"getmt('" .$id. "');\">".$tracker_lang['multi_update']."</a><div id=\"mt_" .$id. "\"></div></td></tr>";


echo "</table>\n";


} elseif ($action == "red"){


if ($redir_parse <> "1")
die($tracker_lang['func_disabled']);

if (empty($redir_day))
$redir_day = 2;

$secs = $redir_day * (86400 * 31); // ������� ������ ������ ������ N �������
$dt = get_date_time(gmtime() - $secs);
sql_query("DELETE FROM reaway WHERE date < ".sqlesc($dt)) or sqlerr(__FILE__,__LINE__);

$urld = $DEFAULTBASEURL."/details.php?id=".$id;

$rs = sql_query("SELECT id, parse_url, parse_ref, numb, date, lastdate FROM reaway WHERE LEFT(parse_ref, ".strlen($urld).") = ".sqlesc($urld)." GROUP BY parse_ref ORDER BY date DESC LIMIT 500") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($rs) >= 1) {

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">";

echo "<tr><td class=\"a\" colspan=\"3\" width=\"100%\">".$tracker_lang['tabs_ired']."<br />".sprintf($tracker_lang['tabs_help'], $redir_day, 500)."</td></tr>";

echo "</table>";

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\"><tr>
<td class=\"colhead\">#</td>
<td class=\"colhead\">".$tracker_lang['refer_site']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['clock']." / ".$tracker_lang['update']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['number_all']."</td>";

$invi_num = 1;

while ($inviter = mysql_fetch_assoc($rs)) {

$inviter["parse_ref"] = str_replace($inviter["parse_url"], "<b>".$inviter["parse_url"]."</b>", $inviter["parse_ref"]);

$vexp = explode("/", $tracker_lang['from_to']);

echo "<tr>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"").">".$invi_num."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"").">".trim(current($vexp)).": ".($inviter["parse_ref"])."<br />".trim(end($vexp)).": ".($inviter["parse_url"])."</td>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"")." align=\"center\">".$inviter["date"]."<br />".($inviter["lastdate"]=="0000-00-00 00:00:00" ? $tracker_lang['no']:$inviter["lastdate"])."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"")." align=\"center\">".$inviter["numb"]."<br /></td>
</tr>";

++$invi_num;
}

echo "</table>";

} else die($tracker_lang['no_user_for_rules'].": ".$action);



} elseif ($action == "refurl" && get_user_class() >= UC_MODERATOR) {


if ($refer_parse == "1") {

if (empty($refer_day))
$refer_day = 2;

$secs = $refer_day * (86400 * 31); // ������� ������ ������ ������ N �������
$dt = get_date_time(gmtime() - $secs);
sql_query("DELETE FROM referrers WHERE date < ".sqlesc($dt)) or sqlerr(__FILE__,__LINE__);
}
else
die($tracker_lang['func_disabled']);

$urld = "/details.php?id=".$id;

$rs = sql_query("SELECT id, parse_url, parse_ref, numb, date, lastdate FROM referrers WHERE LEFT(req_url, ".strlen($urld).") = ".sqlesc($urld)." GROUP BY parse_ref ORDER BY date DESC LIMIT 500") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($rs) >= 1) {

echo "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" class=\"main\">";

echo "<tr><td class=\"a\" colspan=\"3\" width=\"100%\">".$tracker_lang['tabs_i_refurl']."<br />".sprintf($tracker_lang['tabs_help'], $refer_day, 500)."</td></tr>";

echo "</table>";

echo "<table align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
<tr>
<td class=\"colhead\">#</td>
<td class=\"colhead\">".$tracker_lang['refer_site']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['clock']." / ".$tracker_lang['update']."</td>
<td align=\"center\" class=\"colhead\">".$tracker_lang['number_all']."</td>
</tr>
";

$invi_num=1;

while ($inviter = mysql_fetch_assoc($rs)) {

$inviter["parse_ref"] = str_replace($inviter["parse_url"],"<b>".$inviter["parse_url"]."</b>",$inviter["parse_ref"]);
$words_search = parse_url_query($inviter['parse_ref']);

echo "<tr>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"").">".$invi_num."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"").">".($inviter["parse_ref"])."".($words_search <> false && !empty($words_search['query']) ? "<br />
".$tracker_lang['search'].": ".str_replace("+", " ", $words_search['query'])."":"")."</td>
<td ".($invi_num%2==0 ? "class=\"b\"":"class=\"a\"")." align=\"center\">".$inviter["date"]."<br />".($inviter["lastdate"]=="0000-00-00 00:00:00" ? $tracker_lang['no']:$inviter["lastdate"])."</td>
<td ".($invi_num%2==0 ? "class=\"a\"":"class=\"b\"")." align=\"center\">".$inviter["numb"]."<br /></td>
</tr>";

++$invi_num;

}


if (!empty($invi_num))
echo "<tr><td class=\"a\" colspan=\"3\" width=\"100%\">".$tracker_lang['my_backlink'].": ".$DEFAULTBASEURL."<a href=\"".$DEFAULTBASEURL.$urld."\">".$urld."</a></td></tr>";

echo "</table>";

} else die($tracker_lang['no_user_for_rules'].": ".$action);


} elseif ($action == "reports"){

if (get_user_class() < UC_ADMINISTRATOR){

//������������
$res2 = sql_query("SELECT owner FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$report_row1 = mysql_fetch_assoc($res2);

$report_sql = sql_query("SELECT userid FROM report WHERE torrentid = ".sqlesc($id));
$report_row = mysql_fetch_assoc($report_sql);

if ($CURUSER["id"] <> $report_row1["owner"] && $report_row["userid"] <> $CURUSER["id"])
echo "<form method=\"post\" action=\"report.php\">&nbsp;<input name=\"motive\" cols=\"40\" value=\"".$tracker_lang['my_reason']."\">&nbsp;<input type=\"hidden\" name=\"torrentid\" value=\"".$id."\" /> <input class=\"btn\" type=\"submit\" value=\"".$tracker_lang['sendmessage']."\" /></form>";
//������������
else
echo $tracker_lang['complaint_sent'];

die;
}


echo "<table border=\"0\" cellspacing=\"0\" width=\"100%\" cellpadding=\"3\">";

$res = sql_query("SELECT r.*, u1.class AS class_u1,u1.username AS username_u1, u2.class AS class_u2,u2.username AS username_u2, t.name
FROM report AS r
LEFT JOIN users AS u1 ON u1.id = r.userid
LEFT JOIN users AS u2 ON u2.id = r.usertid
LEFT JOIN torrents AS t ON t.id = r.torrentid
WHERE r.torrentid = ".sqlesc($id)."  ORDER BY r.added") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res) == 0)
echo "<tr><td class=\"a\" align=\"center\">".($tracker_lang['no_user_for_rules'].": ".$action)."</td></tr>";

else
echo "<tr>
<td class=\"colhead\">#</td>
<td class=\"colhead\">".$tracker_lang['news_added']."</td>
<td class=\"colhead\">".$tracker_lang['sender']."</td>
<td class=\"colhead\">".$tracker_lang['receiver']."</td>
<td class=\"colhead\">".$tracker_lang['reason']."</td>
</tr>";

$num = 0;
while ($row = mysql_fetch_assoc($res)) {

if ($num%2 == 0){
$cl1 = "class=\"b\"";
$cl2 = "class=\"a\"";
} else {
$cl2 = "class=\"b\"";
$cl1 = "class=\"a\"";
}

$reportid = $row["id"];
$torrentid = $row["torrentid"];
$userid = $row["userid"];
$motive = $row["motive"];
$added = $row["added"];
$username = $row["username_u1"];
$userclass = $row["class_u1"];

if (empty($username))
$username = "<b><font color='red'>".$tracker_lang['anonymous']."<font></b>";

if ($row["id"] && $row["name"] && empty($row["username_u2"]))
$torrenturl = "<a href='details.php?id=$torrentid'>".htmlspecialchars($row["name"])."</a>";
elseif ($row["id"] && empty($row["name"]) && empty($row["username_u2"]))
$torrenturl = "<b><font color='red'>".$tracker_lang['not_registered']."<font></b>";

if ($row["id"] && empty($row["name"]) && !empty($row["username_u2"]))
$torrenturl = "<b><a href='userdetails.php?id=".$row["usertid"]."'>".get_user_class_color($row["class_u2"],  $row["username_u2"])."</a></b>";
elseif ($row["id"] && empty($row["name"]) && !empty($row["username_u2"]))
$torrenturl = "<b><font color='red'>".$tracker_lang['anonymous']."<font></b>";

echo "<tr>
<td ".$cl1." align='center'>".$reportid."</td>
<td ".$cl2." align='center'>".$added."</td>
<td ".$cl1." align='center'><b><a href='userdetails.php?id=".$userid."'>".get_user_class_color($userclass, $username)."</a></b></td>
<td ".$cl2." align='center'>".$torrenturl."</td>
<td ".$cl1." align='center'>".$motive."</td>
</tr>";

++$num;

}

echo "</table>";


} elseif ($action == "cheaters" && get_user_class() == UC_SYSOP){


echo "<table class=\"main\" border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"a\" colspan=\"6\" align=\"center\" width=\"100%\">".$tracker_lang['tab_cheaters_info']." ".sprintf($tracker_lang['limit_ofn'], 250)."</td></tr>";

$res = sql_query("SELECT *, u.username, u.class, u.downloaded, u.uploaded, u.enabled, u.warned
FROM cheaters AS c
LEFT JOIN users AS u ON c.userid = u.id WHERE c.torrentid = ".sqlesc($id)."
GROUP BY c.userid ORDER BY c.added DESC LIMIT 250") or sqlerr(__FILE__, __LINE__);


if (mysql_num_rows($res) == 0)
echo "<tr><td colspan=\"8\" class=\"a\" align=\"center\">".($tracker_lang['no_user_for_rules'].": ".$action)."</td></tr>\n";
else {
echo "<tr>
<td class=\"colhead\" width=\"1%\" align=\"center\">#</td>
<td class=\"colhead\">".$tracker_lang['my_info']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['number_all']."</td>
</tr>";
}

while ($arr = mysql_fetch_assoc($res)) {

$rat_u = @number_format($arr["uploaded"] / $arr["downloaded"], 3);
$ratio = "<font color=" . get_ratio_color($rat_u) . ">".($arr["downloaded"] > 0 ? $rat_u : "<a title=\"".$tracker_lang['ratio']."\">---</a>")."</font>";


echo "<tr><td width=\"1%\" align=\"center\" class=\"a\">".$arr["id"]."</td>";

echo "<td align=\"left\" class=\"a\"><a target='_blank' href=\"userdetails.php?id=".$arr["userid"]."\"><b>".get_user_class_color($arr["class"], $arr["username"])."</b></a> (".$ratio."), <u>U</u>: ".mksize($arr["uploaded"]).", <u>D</u>: ".mksize($arr["downloaded"]).",
".($arr["enabled"] == "no" ? "<img src=\"/pic/warned2.gif\" alt=\"".$tracker_lang['disabled']."\">" : "")."
".($arr["warned"] == "yes" ? "<img src=\"/pic/warned9.gif\" alt=\"".$tracker_lang['warned']."\">" : "")."
<a target='_blank' title=\"".$tracker_lang['user_ip']."\" href=\"".$DEFAULTBASEURL."/usersearch.php?ip=".$arr["userip"]."\">".$arr["userip"]."</a>, ".htmlspecialchars($arr["client"])."
</td>";


$rarr = sql_query("SELECT t.name, t.id, t.free, u.upthis, t.size, u.timediff, u.rate, u.numpeers, u.added, u.client, u.id AS uid, s.uploaded, s.downloaded, s.to_go
FROM torrents AS t
LEFT JOIN snatched AS s ON s.torrent = ".sqlesc($id)." AND s.userid = ".sqlesc($arr["userid"])."
INNER JOIN cheaters AS u ON u.torrentid = t.id
WHERE u.torrentid = ".sqlesc($id)." ORDER BY u.added DESC") or sqlerr(__FILE__, __LINE__);

echo "<td align=\"center\" class=\"a\">".mysql_num_rows($rarr)." ".$tracker_lang['time_s']."</td>";

echo "</tr>";

echo "<td align=\"center\" width=\"100%\" colspan=\"6\" style=\"margin: 0px; padding: 0px;\">";

echo "<table class=\"main\" border=\"0\" width=\"100%\" cellspacing=\"0\" cellspacing=\"5\">";

$array_upthis = $array_rate = array();
$num = 1;

while ($row = mysql_fetch_assoc($rarr)) {

$array_upthis[] = $row["upthis"];
$array_rate[] = $row["rate"];

if ($num%2 == 0){
$cl1 = "class=\"b\"";
$cl2 = "class=\"a\"";
} else {
$cl2 = "class=\"b\"";
$cl1 = "class=\"a\"";
}

echo "<tr>
<td ".$cl1." width=\"1%\" align=\"left\">".$row["uid"]."</td>
<td ".$cl2.">".$num.". <a href=\"details.php?id=".$row["id"]."\">".htmlspecialchars_uni($row["name"])."</a> (".number_format($row["numpeers"]).")<br />
<b>".$tracker_lang['size']."</b>: ".mksize($row["size"]).", <b>U</b>: ".mksize($row["uploaded"]).", <b>D</b>: ".mksize($row["downloaded"]).", <b>".$tracker_lang['completed']."</b>: " . sprintf("%.2f%%", 100 * (1 - ($row["to_go"] / $row["size"])))."
</td>
<td ".$cl1." align=\"center\">".mksize($row["upthis"])." - ".mksize($row["rate"])." / ".$tracker_lang['sec']." (".$row["timediff"]." ".$tracker_lang['sec'].")<br />
".$row["added"]."
</td>
</tr>";

++$num;
}
echo "</table></td>";


$i1 = 0;
foreach ($array_upthis as $v1) {
$i1 += $v1;
}

$srednee_1 = $i1/count($array_upthis);

$i2 = 0;
foreach ($array_rate as $v2) {
$i2 += $v2;
}

$srednee_2 = $i2/count($array_rate);


echo "<tr>
<td class=\"a\" align=\"center\" colspan=\"6\">

<table class=\"main\" border=\"1\" width=\"100%\" cellspacing=\"0\" cellspacing=\"0\">
<tr>
<td class=\"a\" align=\"center\" width=\"50%\" valign=\"top\">
<b>".$tracker_lang['flooded']."</b> -> <u>".$tracker_lang['min']."</u>: ".mksize(min($array_upthis)).", <u>".$tracker_lang['average']."</u>: ".mksize($srednee_1).", <u>".$tracker_lang['max']."</u>: ".mksize(max($array_upthis))."
</td>

<td class=\"a\" align=\"center\" width=\"50%\" valign=\"top\">
<b>".$tracker_lang['speed']."</b> -> <u>".$tracker_lang['min']."</u>: ".mksize(min($array_rate))." / ".$tracker_lang['sec'].", <u>".$tracker_lang['average']."</u>: ".mksize($srednee_2)." / ".$tracker_lang['sec'].", <u>".$tracker_lang['max']."</u>: ".mksize(max($array_rate))." / ".$tracker_lang['sec']."
</td>

</tr>
</table></tr>";

echo "<tr><td align=\"center\" class=\"b\" colspan=\"6\"></td></tr>";
}


}


?>