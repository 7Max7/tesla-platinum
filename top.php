<?
require_once("include/bittorrent.php");
gzip();
dbconn(false);


/**
*** ������ TOP.php ������ ��������� ������� ��� ������ Tesla Tracker v.Platinum III 2013 ����.
**/


$cats = genrelist();

$act = ((isset($_GET['act']) && $_GET['act'] == 'top') ? "top":"last");

$trpage = $CURUSER["torrentsperpage"];
if (empty($trpage) || $trpage > 10) $trpage = 5;

if ($CURUSER && !empty($CURUSER["notifs"])){
preg_match_all('/\[cat([0-9]+)\]/i', $CURUSER["notifs"], $notif);
if (count($notif[1])) $notifs = $notif[1];
}

if (count($notifs))
$re = sql_query("SELECT COUNT(*) AS number FROM torrents WHERE category IN (".implode(",", $notifs).") ".($act == "top" ? "AND added > ".sqlesc(get_date_time(gmtime() - 31*24*60*60)):"")." GROUP BY category ORDER BY number DESC LIMIT 1", $cache = array("type" => "disk", "file" => $act."_count_".crc32($CURUSER["notifs"]), "time" => 60*60)) or sqlerr(__FILE__,__LINE__);
else
$re = sql_query("SELECT COUNT(*) AS number FROM torrents ".($act == "top" ? "WHERE added > ".sqlesc(get_date_time(gmtime() - 31*24*60*60)):"")." GROUP BY category ORDER BY number DESC LIMIT 1", $cache = array("type" => "disk", "file" => $act."_count", "time" => 60*60)) or sqlerr(__FILE__,__LINE__);

while ($row = mysql_fetch_assoc_($re)){
$all_limit = $row['number'];
}



list ($pagertop, $pagerbottom, $limit) = pager($trpage, intval($all_limit), "top.php?act=".$act."&");


stdhead(($act == "top" ? $tracker_lang['block_top']:$tracker_lang['block_last']));

?>
<style>
.bordered tr:hover {
    background: none repeat scroll 0 0 #FBF8E9;
    transition: all 0.1s ease-in-out 0s;
}
.bordered th {
    border-left: 1px solid #CCCCCC;
    border-top: 1px solid #CCCCCC;
    font-size: 12px;
    padding: 3px;
    text-align: left;
}
.bordered td {
    border-left: 1px solid #CCCCCC;
    border-top: 1px solid #CCCCCC;
    font-size: 12px;
    padding: 5px;
}
.bordered th {
   /* background-image: -moz-linear-gradient(center top , #EBF3FC, #DCE9F9);*/
    border-top: medium none;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.8) inset;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
}
.bordered td:first-child, .bordered th:first-child {
    border-left: medium none;
}
.bordered th:first-child {
    border-radius: 6px 0 0 0;
}
.bordered th:last-child {
    border-radius: 0 6px 0 0;
}
.bordered th:only-child {
    border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
    border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
    border-radius: 0 0 6px 0;
}
</style>
<?

echo "<table class=\"embedded\" align=\"center\" width=\"100%\">";


echo "<tr><td colspan=\"4\" class=\"b\"><a ".($act == "last" ? "style=\"font-weight: bold;\"":"")." title=\"".$tracker_lang['block_last']."\" class=\"alink\" href=\"top.php?act=last\">".$tracker_lang['block_last']."</a> - <a ".($act == "top" ? "style=\"font-weight: bold;\"":"")." title=\"".$tracker_lang['block_top']."\" class=\"alink\" href=\"top.php?act=top\">".$tracker_lang['block_top']."</a></td></tr>";


if (($all_limit-$trpage) > 10)
echo "<tr><td colspan=\"4\">".$pagertop."</td></tr>";


echo "<tr><td class=\"b\" align=\"center\">";


foreach ($cats AS $cat){

$truerule = true;
if ($CURUSER && !empty($CURUSER["notifs"])){
preg_match_all('/\[cat([0-9]+)\]/i', $CURUSER["notifs"], $notif);
if (count($notif[1]) && !in_array($cat['id'], $notif[1])) $truerule = false;
}

if ($truerule == true)
$res = sql_query("SELECT torrents.id, torrents.name, torrents.added, torrents.comments, torrents.size, (torrents.leechers+torrents.f_leechers) AS leechers, (torrents.seeders+torrents.f_seeders) AS seeders FROM torrents WHERE torrents.category = ".sqlesc($cat['id'])." ".($act == "top" ? "AND added > ".sqlesc(get_date_time(gmtime() - 31*24*60*60))." ORDER BY seeders":"ORDER BY id")." DESC ".$limit, $cache = array("type" => "disk", "file" => $act."_".$cat['id']."_".crc32($limit), "time" => 60*60)) or sqlerr(__FILE__,__LINE__);


if (mysql_num_rows_($res) > 0 && $truerule == true) {


echo "<table width=\"100%\" class=\"bordered\">";

echo "<thead><tr><th colspan=\"4\" align=\"left\"><a title=\"".$tracker_lang['rss']."\" href=\"rss.php?cat=".$cat["id"]."\"><img border=\"0\" src=\"./pic/rss.gif\" tooltip=\"RSS\"></a> <a class=\"alink\"  title=\"".htmlspecialchars($cat["name"])."\" href=\"browse.php?cat=".$cat["id"]."\"><b>".$cat["name"]."</b> ".($act == "last" ? "(".number_format($cat['num_torrent']).")":"")."</a>".(count($notif[1]) ? "<a title=\"".$tracker_lang['changed_category_def']."\" href=\"my.php\">*</a>":"")."

</th></tr></thead>";

echo "<tbody>";

$viw == 0;
while ($row = mysql_fetch_assoc_($res)){


if ($viw%2 == 0){
$clasto = "class = 'b'";
$clastf = "class = 'a'";
} else {
$clasto = "class = 'a'";
$clastf = "class = 'b'";
}


echo "<tr class=\"browse\">";

echo "<td width=\"90%\" ".$clasto."><a title=\"".htmlspecialchars($row['name'])."\" class=\"alink\" href=\"details.php?id=".$row['id']."\">".$row['name']."</a>

".((checknewnorrent($row["id"], $row["added"]) && $CURUSER) ? " <font color=\"red\" size=\"1\">".$tracker_lang['new_torrent']."</font>":"")."

".(!empty($row['comments']) ? "<var style=\"padding-right: 1px; clear: right; float: right;\"><nobr><a title=\"".$tracker_lang['comments_list']."\" class=\"alink\" href=\"details.php?id=".$row['id']."#comments\">".$row['comments']." <img border=\"0\" src=\"/pic/img-resized.png\" tooltip=\"".$tracker_lang['comments_list']."\" /></a></nobr></var>":"")."


</td>";

echo "<td align=\"right\" ".$clasto."><nobr>".mksize($row['size'])."</nobr></td>";

echo "<td ".$clasto."><nobr><img src=\"./pic/arrowup.gif\" tooltip=\"".$tracker_lang['details_seeding']."\"> <font color=\"green\"><b>".$row['seeders']."</b></font> <img src=\"./pic/arrowdown.gif\" tooltip=\"".$tracker_lang['details_leeching']."\"> <font color=\"red\"><b>".$row['leechers']."</b></font></nobr></td>";

echo "</tr>";

++$viw;
}

unset($res, $row);

echo "</tbody></table>";

echo "<div style=\"margin: 10px auto 0;\"> </div>";

}

}


echo "</td></tr>";

if (($all_limit-$trpage) > 10)
echo "<tr><td colspan=\"4\">".$pagerbottom."</td></tr>";


echo "</table>";

stdfoot();

?>