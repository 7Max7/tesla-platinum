<?

if (!defined('BLOCK_FILE')) {   
 Header("Location: ../index.php");   
 exit;   
}

/**
 * @author 7Max7
 * @copyright Tesla Tracker (TT) � 2012 v.Platinum (III)
 */

global $tracker_lang;

$blocktitle = $tracker_lang['block_last']." :: <a class=\"altlink_white\" href=\"browse.php?date=".date("Y-m-d")."\">".$tracker_lang['torrent_today']."</a>";   

?>
<style>
#pichead {
    height: 330px;
    padding-top: 1px;
}
#externalcontrols {
    position: relative;
}
#externalcontrols .thide {
    background: url("../pic/tabs/lr-slider.png") no-repeat scroll 0 0 transparent;
    cursor: pointer;
    padding-top: 56px;
    position: absolute;
    top: 194px;
    width: 36px;
}
#externalcontrols .ext_prev {
    background-position: 0 0;
    left: 0;
}
#externalcontrols .ext_next {
    background-position: -36px 0;
    right: 0;
}
#externalcontrols .ext_prev:hover {
    background-position: 0 -56px;
}
#externalcontrols .ext_next:hover {
    background-position: -36px -56px;
}
body #boutique li {
    background: url("../pic/loading.gif") no-repeat scroll 50% 50% #FFFFFF;
}
#boutique h6 {
    color: #232227;
    font-weight: normal;
    line-height: 130%;
    margin: 0 0 2px;
}
#boutique span {
    background: none repeat scroll 0 0 #FFFFFF;
    color: #444444;
    line-height: 120%;
    margin: 0;
    padding: 1px;
}
#boutique .front {
    margin-top: 0;
}
#boutique .front h6 {
    font-size: 13px;
}
#boutique .front span {
    font-size: 14px;
}
#boutique .behind {
    margin-top: 25px;
}
#boutique .behind h6 {
    font-size: 12px;
}
#boutique .behind span {
    font-size: 13px;
}
#boutique .back {
    margin-top: 60px;
    padding-bottom: 3%;
}
#boutique .back h6 {
    font-size: 10px;
}
#boutique .back span {
    font-size: 11px;
}
#boutique {
    margin: 0 auto;
    padding: 0;
    position: relative;
    width: 820px;
    z-index: 1;
}
#boutique li {
    border-width: 0;
    display: none;
    list-style: none outside none;
    margin: 0;
    padding: 0 0 6%;
    position: absolute;
    z-index: 1;
}
#boutique img {
    border: 0 none;
    height: 270px;
    margin: 0;
    vertical-align: bottom;
    width: 220px;
}
#boutique span {
    bottom: 0;
    color: #33CC00;
    cursor: default;
    display: block;
    left: 0;
    opacity: 1;
    position: absolute;
    right: 0;
    text-align: left;
}
#boutique h6 {
    cursor: default;
}
#boutique a {
    cursor: default;
    text-decoration: none;
}
#boutique img, #boutique li, #boutique a {
    -moz-user-select: none;
}
.shadow {
    background: url("../pic/tabs/shadow.png") no-repeat scroll 50% 50% transparent;
    bottom: -30px;
    left: 0;
    padding-top: 23px;
    position: absolute;
    right: 0;
}
</style>

<script language="javascript" type="text/javascript" src="js/boutique.min.js"></script>

<?

$filecache = file_query("", $cache = array("type" => "disk", "file" => "block-boutique", "time" => 60*5, "action" => "get"));

if ($filecache <> false) {
$content = $filecache;
} else {


$res_ = sql_query("SELECT id, name, image1 FROM torrents ORDER BY added DESC LIMIT 20") or sqlerr(__FILE__, __LINE__);

$content = "<div id=\"pichead\">";
    
$content.= "<div class=\"wrap\">";

$content.= "<div style=\"display: none;\"><script type=\"text/javascript\">
jQuery(document).ready(function(){
jQuery('#boutique').boutique({autointerval: 4000,behind_opac: 0.5,back_opac: 0.3,behind_size: 0.88,back_size: 0.7,hovergrowth: 0,autoplay: true});
});
</script></div>";


  
$content.= "<div id=\"externalcontrols\">";
$content.= "<div onclick=\"boutique_ext_prev()\" class=\"thide ext_prev\"></div>";
$content.= "<div onclick=\"boutique_ext_next()\" class=\"thide ext_next\"></div>";
$content.= "</div>";

$content.= "<ul id=\"boutique\" style=\"height: 270px;\">";



while ($row_ = mysql_fetch_assoc($res_)){

$image1 = htmlspecialchars_uni($row_["image1"]);

if (empty($image1))
$image1 = "default_torrent.png";

$image1 = "thumbnail.php?image=".$image1;

$content.= "<li title=\"".htmlspecialchars($row_["name"])."\"><a href=\"details.php?id=".$row_["id"]."\"><img alt=\"".htmlspecialchars($row_["name"])."\" src=\"".$image1."\" /></a></li>";

}


$content.= "</ul>";

$content.= "</div>";
$content.= "</div>";

file_query($content, $cache = array("type" => "disk", "file" => "block-boutique", "time" => 60*5, "action" => "set"));
}

?>