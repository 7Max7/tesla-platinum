<?
require_once("include/bittorrent.php");

dbconn(false);
loggedinorreturn();

if (get_user_class() == UC_UPLOADER || $CURUSER['override_class'] <> 255 || get_user_class() == UC_VIP)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);


if (get_user_class() < UC_UPLOADER){

$res = sql_query("SELECT * FROM uploadapp WHERE userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($res);

if (isset($_GET["del"]) && $_GET["del"] == "yes" && !empty($row["id"])){

if ($row["active"] == "no")
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

sql_query("DELETE FROM uploadapp WHERE userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);

header("Location: uploadapp.php");
die;

} elseif (isset($_POST["comment"]) && empty($row["id"])){

$comment = (!empty($_POST["comment"]) ? htmlspecialchars_uni($_POST["comment"]):"");

if (empty($comment))
stderr($tracker_lang['error'], $tracker_lang['comment'].": ".$tracker_lang['comment']);

sql_query("INSERT INTO uploadapp (userid, applied, comment) VALUES (".sqlesc($CURUSER["id"]).", ".sqlesc(get_date_time()).", ".sqlesc(trim($comment)).")") or sqlerr(__FILE__, __LINE__);

header("Location: uploadapp.php");
die;

}

stdhead($tracker_lang['offer_uploaders']);

if (empty($row["id"]))
echo "<form action=\"uploadapp.php\" method=\"post\">";

echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
echo "<tr><td colspan=\"3\" class=\"colhead\">".$tracker_lang['offer_uploaders']."</td></tr>";

echo "<tr><td colspan=\"3\" class=\"b\">".$tracker_lang['warning'].": ".$tracker_lang['uploadapp_info']."</td></tr>";

echo "<tr><td class=\"b\">".$tracker_lang['comments_for']."</td><td class=\"a\"><input ".(!empty($row["id"]) ? "disabled":"")." name=\"comment\" value=\"".htmlspecialchars($row["comment"])."\" type=\"text\" size=\"70\" /> ".sprintf($tracker_lang['max_n_limit'], 1024)."</td></tr>";

echo "<tr><td class=\"a\">".$tracker_lang['confirmation']."</td><td class=\"b\"><input ".(!empty($row["id"]) ? "disabled":"")." class=\"btn\" type=\"submit\" value=\"".$tracker_lang['subm_ticket']."\"></td></tr>";

echo "</table>";

if (empty($row["id"]))
echo "</form>";

if (!empty($row["id"])){
echo "<br />
<form action=\"uploadapp.php\" method=\"get\">
<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">

<tr><td class=\"b\" width=\"25%\">".$tracker_lang['sort_added']."</td><td class=\"a\">".$row["applied"]."</td></tr>

<tr><td class=\"a\" width=\"25%\">".$tracker_lang['offer_uploaders']."</td><td class=\"b\"><input type=\"hidden\" name=\"del\" value=\"yes\" /> <input class=\"btn\" type=\"submit\" value=\"".$tracker_lang['delete']."\"></td></tr>

</table>
</form>";

}

stdfoot();
die;

}


if (get_user_class() >= UC_MODERATOR){


if (isset($_POST["usernw"]) && is_array($_POST["usernw"])){

sql_query("DELETE FROM uploadapp WHERE id IN (".implode(",", array_map('intval', $_POST['usernw'])).")") or sqlerr(__FILE__, __LINE__);

header("Location: uploadapp.php");
die;

}

if (isset($_POST["desact"]) && is_array($_POST["desact"])){

$res = sql_query("SELECT up.id, up.userid, up.active, u.class 
FROM uploadapp AS up 
INNER JOIN users AS u ON u.id = up.userid
WHERE up.id IN (".implode(",", array_map('intval', $_POST['desact'])).")") or sqlerr(__FILE__, __LINE__);

while ($row = mysql_fetch_array($res)){

if ($row["class"] < UC_UPLOADER && $row["active"] == "yes"){

$modcomment = gmdate("Y-m-d") . " - ������ �� ���������: ��������.\n";
sql_query("UPDATE users SET promo_time = ".sqlesc(get_date_time()).", class = ".sqlesc(UC_UPLOADER).", modcomment = CONCAT_WS('', ".sqlesc($modcomment).", modcomment) WHERE class < ".sqlesc(UC_UPLOADER)." AND override_class = '255' AND id = ".sqlesc($row["userid"])) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$row["userid"]);

$number = mysql_modified_rows();

$msg = "��� �������� �� ���������!";

if (!empty($number))
sql_query("INSERT INTO messages (sender, receiver, added, msg, poster, location) VALUES (0, ".sqlesc($row["userid"]).", ".sqlesc(get_date_time()).", ".sqlesc($msg).", 0, 1)") or sqlerr(__FILE__, __LINE__);

}

sql_query("UPDATE uploadapp SET active = 'no' WHERE id = ".sqlesc($row["id"])) or sqlerr(__FILE__, __LINE__);

}

header("Location: uploadapp.php");
die;

}


$count = get_row_count("uploadapp");
list ($pagertop, $pagerbottom, $limit) = pager(30, $count, "uploadapp.php?");

if (empty($count))
stderr($tracker_lang['offer_uploaders'], $tracker_lang['no_data_now']);

stdhead($tracker_lang['offer_uploaders']);

$res = sql_query("SELECT up.*, u.username, u.class, u.uploaded, u.downloaded, u.modcomment, u.usercomment, u.promo_time
FROM uploadapp AS up
LEFT JOIN users AS u ON u.id = up.userid ".$limit) or sqlerr(__FILE__,__LINE__);

?>
<script language="Javascript" type="text/javascript">
jQuery(document).ready(function() {

jQuery("#usernw").click(function () {
if (!jQuery("#usernw").is(":checked"))
jQuery(".usernwo").removeAttr("checked");
else
jQuery(".usernwo").attr("checked","checked");
});

jQuery("#desact").click(function () {
if (!jQuery("#desact").is(":checked"))
jQuery(".desacto").removeAttr("checked");
else
jQuery(".desacto").attr("checked","checked");
});

}); 
</script>
<?

echo "<form action=\"uploadapp.php\" method=\"post\">";
echo "<table class=\"main\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"b\" colspan=\"5\" width=\"100%\">".$tracker_lang['offer_uploaders']."</td></tr>";

echo "<tr><td colspan=\"5\">".$pagertop."</td></tr>";

echo "<tr>
<td class=\"colhead\" width=\"2%\">#</td>
<td class=\"colhead\" width=\"20%\">".$tracker_lang['signup_username']." / ".$tracker_lang['ratio']."</td>
<td class=\"colhead\" width=\"10%\">".$tracker_lang['sort_added']." / ".$tracker_lang['time_uppe']."</td>
<td class=\"colhead\">".$tracker_lang['comment']."</td>
<td class=\"colhead\"><label><input type=\"checkbox\" id=\"usernw\" />".$tracker_lang['disselect']."</label> / <label>".$tracker_lang['class_uploader']." <input type=\"checkbox\" id=\"desact\" /></label></td>
</tr>";

$i2 = 0;
while ($arr = mysql_fetch_assoc($res)) {

if ($i2%2==1){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

$rat_u = @number_format($arr["uploaded"] / $arr["downloaded"], 3);
$ratio = "<font color=\"".get_ratio_color($rat_u)."\">".($arr["downloaded"] > 0 ? $rat_u : "<a title=\"".$tracker_lang['ratio']."\">---</a>")."</font>";

echo "<tr>
<td ".$cl1." width=\"2%\">".$arr["id"]."</td>
<td ".$cl2."><nobr>".(!empty($arr["username"]) ? "<a href=\"userdetails.php?id=".$arr["userid"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a> (".$ratio.")<br /><u>U</u>: ".mksize($arr["uploaded"]).", <u>D</u>: ".mksize($arr["downloaded"]):$tracker_lang['anonymous'])."</nobr></td>
<td ".$cl1." align=\"center\"><nobr>".$arr["applied"]."<br />".($arr["active"] == "no" ? "<strong>".$arr["promo_time"]."</strong>":"---")."</nobr></td>
<td ".$cl2.">".(empty($arr["comment"]) ? "N/A":htmlspecialchars($arr["comment"]))."</td>
<td ".$cl1.">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr>
<td width=\"50%\" align=\"center\"><input type=\"checkbox\" class=\"usernwo\" name=\"usernw[]\" value=\"".$arr["id"]."\" /></td>
<td width=\"50%\" align=\"center\">".(($arr["class"] < UC_UPLOADER && !empty($arr["username"]) && $arr["active"] == "yes") ? "<input type=\"checkbox\" name=\"desact[]\" class=\"desacto\" value=\"".$arr["id"]."\" />":"N/A")."</td>
</tr></table>
</td>
</tr>";

$modcomment = htmlspecialchars($arr["modcomment"]);
$usercomment = htmlspecialchars($arr["usercomment"]);

echo "<tr><td class=\"a\" colspan=\"6\" width=\"100%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr>";

echo "<td style=\"padding:1px;\" colspan=\"3\" class=\"b\" width=\"50%\" align=\"center\"><div class=\"spoiler-wrap\" id=\"ad_".$arr['id']."\"><div class=\"spoiler-head folded clickable\">".$tracker_lang['modcomment'].": ".$arr['username']."</div><div class=\"spoiler-body\" style=\"display: none;\"><textarea cols=90% rows=20 readonly>".$modcomment."</textarea></div></div></td>";

if (get_user_class() > UC_MODERATOR)
echo "<td style=\"padding:1px;\" colspan=\"4\" class=\"b\" width=\"50%\" align=\"center\"><div class=\"spoiler-wrap\" id=\"us_".$arr['id']."\"><div class=\"spoiler-head folded clickable\">".$tracker_lang['usercomment'].": ".$arr['username']."</div><div class=\"spoiler-body\" style=\"display: none;\"><textarea cols=90% rows=20 readonly>".$usercomment."</textarea></div></div></td>";

echo "</tr></table></td></tr>";

++$i2;
}

echo "<tr><td colspan=\"5\" align=\"right\" ".$cl2."><input type=\"submit\" class=\"btn\" name=\"submit\" value=\"".$tracker_lang['b_action']."\" /></td></tr>";

echo "<tr><td colspan=\"5\">".$pagerbottom."</td></tr>";

echo "</table>";
echo "</form>";

}

stdfoot();
?>