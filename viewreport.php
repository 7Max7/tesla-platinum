<?
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();

if (get_user_class() < UC_ADMINISTRATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);


/// ������� ��� ������
if ($_POST['deleteall'] && get_user_class() == UC_SYSOP) {

accessadministration();

sql_query("TRUNCATE TABLE report") or sqlerr(__FILE__,__LINE__);

header("Location: viewreport.php");
die;
}

/// ������� ��������� ������
if (isset($_POST['delete']) && is_array($_POST['desact'])) {

sql_query("DELETE FROM report WHERE id IN (".implode(",", array_map('intval', $_POST['desact'])).")") or sqlerr(__FILE__,__LINE__);

header("Location: viewreport.php");
die;
}

$count = get_row_count("report");

if (empty($count))
stderr($tracker_lang['complaints'], $tracker_lang['no_data_now']);

stdhead($tracker_lang['complaints']);

list ($pagertop, $pagerbottom, $limit) = pager(50, $count, "viewreport.php?");

?>
<script language="Javascript" type="text/javascript">
jQuery(document).ready(function() {

jQuery("#desact").click(function () {
if (!jQuery("#desact").is(":checked"))
jQuery(".desacto").removeAttr("checked");
else
jQuery(".desacto").attr("checked","checked");
});

}); 
</script>
<?

echo "<form action=\"viewreport.php\" method=\"post\">";

echo "<table border=\"0\" cellspacing=\"0\" width=\"100%\" cellpadding=\"3\">";

echo "<tr><td class=\"colhead\" colspan=\"6\">".$tracker_lang['viewreport']."</td></tr>";

echo "<tr><td colspan=\"6\">".$pagertop."</td></tr>";

echo "<tr>";
echo "<td class=\"colhead\" align=\"center\">#</td>";
echo "<td class=\"colhead\" width=\"15%\" align=\"center\">".$tracker_lang['clock']."</td>";
echo "<td class=\"colhead\">".$tracker_lang['sender']."</td>";
echo "<td class=\"colhead\">".$tracker_lang['signup_username']." / ".$tracker_lang['torrent']."</td>";
echo "<td class=\"colhead\" align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['mark_all']."\" id=\"desact\" /></td>";
echo "</tr>";

$res = sql_query("SELECT r.*, u1.class AS class_u1, u1.username AS username_u1, u2.class AS class_u2, u2.username AS username_u2, t.name
FROM report AS r
LEFT JOIN users AS u1 ON u1.id = r.userid
LEFT JOIN users AS u2 ON u2.id = r.usertid
LEFT JOIN torrents AS t ON t.id = r.torrentid
".$limit) or sqlerr(__FILE__, __LINE__);

$i2 = 0;

while ($row = mysql_fetch_array($res)) {

if ($i2%2==1){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

echo "<tr>";

echo "<td rowspan=\"2\" ".$cl1." width=\"5%\" align=\"center\">".$row["id"]."</td>";

echo "<td ".$cl2." width=\"15%\" align=\"center\"><nobr>".$row["added"]."</nobr></td>";

if (!empty($row["username_u2"]))
echo "<td ".$cl1."><a href=\"userdetails.php?id=".$row["usertid"]."\">".get_user_class_color($row["class_u2"], $row["username_u2"])."</a></td>";
else
echo "<td ".$cl1.">".$tracker_lang['anonymous']."</td>";

if (!empty($row["name"]) && !empty($row["torrentid"]))
echo "<td ".$cl2."><nobr><a href='details.php?id=".$row["torrentid"]."'>".htmlspecialchars($row["name"])."</a></nobr></td>";
elseif (empty($row["name"]) && !empty($row["torrentid"]))
echo "<td ".$cl2.">".$tracker_lang['torrent_del_or_move']."</td>";
elseif (!empty($row["username_u1"]) && !empty($row["usertid"]))
echo "<td ".$cl2."><nobr><a href=\"userdetails.php?id=".$row["usertid"]."\">".get_user_class_color($row["class_u1"], $row["username_u1"])."</a></nobr></td>";
elseif (empty($row["username_u1"]) && !empty($row["usertid"]))
echo "<td ".$cl2.">".$tracker_lang['anonymous']."</td>";

echo "<td rowspan=\"2\" ".$cl1." align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['delete']."\" name=\"desact[]\" class=\"desacto\" value=\"".$row["id"]."\" /></td>";

echo "</tr>";

echo "<tr><td class=\"b\" align=\"left\" colspan=\"3\"><strong>".$tracker_lang['reason']."</strong>: ".htmlspecialchars($row["motive"])."</td></tr>";

++$i2;

}

echo "<tr><td align=\"right\" colspan=\"6\"><input class=\"btn\" type=\"submit\" name=\"delete\" value=\"".$tracker_lang['b_action']."\" /></td></tr>";

echo "<tr><td colspan=\"6\">".$pagerbottom."</td></tr>";

echo "</table>";

echo "</form>";

if (!empty($count) && get_user_class() == UC_SYSOP)
echo "<form action=\"viewreport.php\" method=\"post\">
<table border=\"0\" cellspacing=\"0\" width=\"100%\" cellpadding=\"3\">
<tr><td align=\"center\">
<input type=\"hidden\" name=\"deleteall\" value=\"deleteall\" /><input class=\"btn\" title=\"".$tracker_lang['delete_user']."\" type=\"submit\" class=\"btn\" value=\"".$tracker_lang['complaints'].": ".$tracker_lang['trunc_date']."\" />
</td></tr>
</table>
</form>";

stdfoot();
?>