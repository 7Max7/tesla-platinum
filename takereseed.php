<?
require "include/bittorrent.php";

dbconn();
loggedinorreturn();

$id = (int) (isset($_GET["torrent"]) ? intval($_GET["torrent"]):"");

if (!is_valid_id($id))
stderr($tracker_lang['back_snatched_all'], $tracker_lang['invalid_id_value']);

$res = sql_query("SELECT torrents.seeders, torrents.banned, torrents.leechers, torrents.name, torrents.times_completed, torrents.id, UNIX_TIMESTAMP(torrents.last_reseed) AS lr, categories.name AS cat_name
FROM torrents
LEFT JOIN categories ON torrents.category = categories.id
WHERE torrents.id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

$row = mysql_fetch_array($res);

if (!$row || $row["banned"] == "yes")
stderr($tracker_lang['back_snatched_all'], $tracker_lang['no_torrent_with_such_id']);

if (empty($row["times_completed"]) || empty($row["leechers"]) || ($row["lr"] > (time() - 24*3600) && get_date_time($row["lr"]) <> "0000-00-00 00:00:00"))
stderr($tracker_lang['back_snatched_all'], $tracker_lang['reseed_info']);

$subject = "�������� ������� - ".htmlspecialchars($row["name"]);

$msg = "������������! ���� ������ ���������� � ������� [url=details.php?id=".$id."]".$row["cat_name"]." :: ".htmlspecialchars($row["name"])."[/url]. ���� �� ������ ������, �� ��� ������� �������-����, ������ ������� ��� [url=download.php?id=".$id."]�����[/url]. \n������� �� ���� ������ � ������� �����!";

sql_query("INSERT INTO messages (sender, receiver, poster, added, subject, msg) SELECT ".sqlesc($CURUSER["id"]).", userid, 0, ".sqlesc(get_date_time()).", ".sqlesc($subject).", ".sqlesc($msg)." FROM snatched WHERE torrent = ".sqlesc($id)." AND userid <> ".sqlesc($CURUSER["id"])." AND finished = 'yes'") or sqlerr(__FILE__, __LINE__);

sql_query("UPDATE torrents SET last_reseed = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

header("Location: details.php?id=".$id."&helpme");
die;

?>