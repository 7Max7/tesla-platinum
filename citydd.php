<?
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

if (get_user_class() < UC_SYSOP) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

accessadministration();

$exp = explode("/", $tracker_lang['my_city_is']);

$link = false;

if (isset($_GET['sure']) && $_GET['sure'] == "yes" && isset($_GET['delid'])) {

$delid = (int) $_GET['delid'];

$ct_v = sql_query("SELECT country_id FROM cities WHERE id = ".sqlesc($delid)) or sqlerr(__FILE__, __LINE__);
$ct_r = mysql_fetch_array($ct_v);

if (!empty($delid))
$sql = sql_query("DELETE FROM cities WHERE id = ".sqlesc($delid)." LIMIT 1") or sqlerr(__FILE__, __LINE__);

if (!empty($ct_r["country_id"]))
unsql_cache("counid_".$ct_r["country_id"]);

$link = true;
}

$delid = (int) $_GET['delid'];

if (!empty($delid) && !isset($_POST["sure"]) && $_GET['sure'] <> "yes")
stderr($tracker_lang['warning'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $exp[1]." (<strong>".htmlspecialchars($_GET['name'])."</strong>)", "citydd.php?delid=".$delid."&sure=yes"));


if (isset($_GET['edited']) && $_GET['edited'] == 1 && isset($_GET['id'])) {

$id = (int) $_GET['id'];
$country_name = htmlspecialchars_uni(strip_tags($_GET['country_name']));
$country_id = (int) $_GET['country_id'];

if (!empty($country_id) && !empty($id)){
sql_query("UPDATE cities SET name = ".sqlesc($country_name).", country_id = ".sqlesc($country_id)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("counid_".$country_id);
}

$link = true;

}

if (isset($_GET['add']) && $_GET['add'] == 'true') {
$country_name = htmlspecialchars_uni(strip_tags($_GET['country_name']));
$country_id = (int) $_GET['country_id'];

if (!empty($country_id) && !empty($country_name)){
$sql = sql_query("INSERT INTO cities SET name = ".sqlesc($country_name).", country_id = ".sqlesc($country_id));

unsql_cache("counid_".$country_id);
}

$link = true;
}

if ($link == true && !headers_sent()){
@header ("Location: citydd.php");
die;
} elseif ($link == true)
die ("<script>setTimeout('document.location.href=\"citydd.php\"', 1);</script>");


stdhead($tracker_lang['my_city_is']);

$editid = $_GET['editid'];
$id = (int) $_GET['id'];
$name = htmlspecialchars_uni(strip_tags($_GET['name']));
$country_id = (int) $_GET['country_id'];

$countries = '';
$ct_r = sql_query("SELECT id, name FROM countries ORDER BY name") or sqlerr(__FILE__, __LINE__);
while ($ct_a = mysql_fetch_array($ct_r))
$countries .= "<option value=\"".$ct_a["id"]."\" ".($country_id == $ct_a['id'] ? "selected" : "").">".$ct_a["name"]."</option>\n";

if (!empty($editid)) {
echo("<form name=\"form1\" method=\"get\" action=\"citydd.php\">");

echo("<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">");

echo("<tr><td colspan=\"2\" class=\"b\"><input type=\"hidden\" name=\"edited\" value=\"1\" />".$tracker_lang['editing'].": <strong> ".$name."</strong></td></tr>");

echo("<input type=\"hidden\" name=\"id\" value=\"".$editid."\" />");

echo("<tr><td class=\"b\">".trim($exp[0]).": </td><td class=\"a\" align=\"right\"><input type=\"text\" size=\"52\" name=\"country_name\" value=\"".$name."\"></td></tr>");

echo("<tr><td class=\"a\">".trim($exp[1]).":</td><td class=\"b\" align=\"right\"><select name=\"country_id\">".$countries."</select></td></tr>");

echo("<tr><td align=\"right\" colspan=\"2\" class=\"b\"><input value=\"".$tracker_lang['edit']."\" class=\"btn\" type='submit'></td></tr>");

echo("</table></form>");

stdfoot();
die();
}

echo ("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");
echo("<form name=\"form\"' method=\"get\" action=\"citydd.php\">");
echo("<table class=\"main\" cellspacing=\"0\" cellpadding=\"5\" width=\"80%\">");
echo("<tr><td colspan=\"2\" class=\"b\">".$tracker_lang['act_add'].": ".trim($exp[1])."</td></tr>");
echo("<tr><td class=\"b\">".trim($exp[1]).": </td><td class=\"a\" align=\"right\"><input type=\"text\" size=\"52\" name=\"country_name\" /></td></tr>");
echo("<tr><td class=\"a\">".trim($exp[0]).": </td><td class=\"b\" align=\"right\"><select name=country_id>".$countries."</select> <input type=\"hidden\" name=\"add\" value=\"true\" /></td></tr>");
echo("<tr><td class=\"b\" colspan=\"2\" align=\"right\"><input class=\"btn\" value=\"".$tracker_lang['act_add']."\" type=\"Submit\"></td></tr>");
echo("</table>");
echo("</form>");
echo("<br />");


$perpage = 100;
$count = get_row_count("cities");

list($pagertop, $pagerbottom, $limit) = pager($perpage, $count, "citydd.php?");

echo("<table class=\"main\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">");

echo("<tr><td class=\"b\" colspan=\"4\">".$pagertop."</td></tr>");

echo("<tr>
<td class=\"colhead\"># ".trim($exp[1])." / # ".trim($exp[0])."</td>
<td class=\"colhead\">".trim($exp[1])."</td>
<td class=\"colhead\">".trim($exp[0])."</td>
<td class=\"colhead\">".$tracker_lang['action']."</td>
</tr>");

$sql = sql_query("SELECT c.*, co.name AS couname, co.flagpic
FROM cities AS c 
LEFT JOIN countries AS co ON co.id = c.country_id
ORDER BY c.id DESC ".$limit) or sqlerr(__FILE__, __LINE__);

$num = 0;
while ($row = mysql_fetch_array($sql)) {

$cl2 = 'class = "b"'; $cl1 = 'class = "a"';

if ($num % 2 == 1){
$cl1 = 'class = "b"';
$cl2 = 'class = "a"';
}

echo("<tr>
<td ".$cl1.">".$row['ID']." / ".$row['country_id']."</td>
<td ".$cl2.">".$row['name']."</td>
<td ".$cl1."><img src=\"/pic/flag/".$row["flagpic"]."\" alt=\"".$row["couname"]."\" style=\"margin-left: 8pt;\"></td>
<td ".$cl2."><a href=\"citydd.php?editid=".$row['ID']."&name=".$row['name']."&country_id=".$row['country_id']."\">".$tracker_lang['edit']."</a> / <a href=\"citydd.php?delid=".$row['ID']."&name=".$row['name']."\">".$tracker_lang['delete']."</a></td>
</tr>");

++$num;
}

echo("</table>");

stdfoot();
?>