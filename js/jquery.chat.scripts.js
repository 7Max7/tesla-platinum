jQuery(document).ready(function(){
	
	// ��������� ����� init, ����� �������� ����� �����:
	chat.init();
	
});

var chat = {
	
	// data �������� ��������� ��� ������������� � �������:
	
	data : {
		lastID 		: 0,
		noActivity	: 0
	},
	
	// Init ����������� ����������� ������� � ������������� �������:
	
	init : function(){
				
		// ������������ div #chatLineHolder � jScrollPane,
		// ��������� API ������� � chat.data:
		
		chat.data.jspAPI = jQuery('#chatLineHolder').jScrollPane({
			verticalDragMinHeight: 12,
			verticalDragMaxHeight: 12
		}).data('jsp');
		
		// ���������� ��������� working ��� ��������������
		// ������������� �������� �����:
		
		var working = false;
		
		
		// ���������� ������ ����� ������ ����:
		
		jQuery('#submitForm').submit(function(){
			
			var text = jQuery('#chatText').val();
			
			if(text.length == 0){
				return false;
			}
			
			if(working) return false;
			working = true;
			
			// ���������� ��������� ID ��� ����:
			var tempID = 't'+Math.round(Math.random()*1000000),
				params = {
					id			: tempID,
					author		: chat.data.name,
					text		: text.replace(/</g,'&lt;').replace(/>/g,'&gt;')
				};

			// ���������� ����� addChatLine, ����� �������� ��� �� ����� 
			// ����������, �� ������ ���������� ������� AJAX:
			
			chat.addChatLine(jQuery.extend({},params));
			
			// ���������� ����� tzPOST, ����� ��������� ���
			// ����� ������ POST AJAX:
			
			jQuery.tzPOST('submitChat',jQuery(this).serialize(),function(r){
				working = false;
				
				if(r.error){
					chat.displayError(r.error);
				}
              
				jQuery('#chatText').val('');
				jQuery('div.chat-'+tempID).remove();
				
				params['id'] = r.insertID;
				chat.addChatLine(jQuery.extend({},params));
			});
			
			return false;
		});

		
		// ��������� ��������� ����������� ������������ (���������� ��������)
		
		jQuery.tzGET('checkLogged',function(r){
			if(r.logged){
				chat.login(r.loggedAs.name,r.loggedAs);
			}
		});
		
		// ����������������� ������� ��������
		
		(function getChatsTimeoutFunction(){
			chat.getChats(getChatsTimeoutFunction);
		})();
		
		(function getUsersTimeoutFunction(){
			chat.getUsers(getUsersTimeoutFunction);
		})();
		
	},
	
	// ����� login ������ ��������� ��� ��� �������� ���������
	// � ������� � ������ ���������
	
	login : function(name){
		
		chat.data.name = name;
		
		jQuery('#loginForm').fadeOut(function(){
			jQuery('#submitForm').fadeIn();
			jQuery('#chatText').focus();
		});
		
	},
	
	// ����� render ���������� �������� HTML, 
	// ������� ����� ��� ������ �������:
	
render : function(template,params){

var arr = [];

switch(template){
	  
case 'chatLine': arr = ['<div class="chat chat-',params.id,' rounded">','</span><span class="text">',params.author,'> ',params.text,'</span><span id="date" date="',params.date,'" class="time">',params.date,'</span></div>']; break;
			
case 'user': arr = [params.name]; break;
}

		// ������������ ����� join ��� ������� ����������� 
		// ��������, ��� ������������� ������� �����
		
		return arr.join('');
		
	},
	
	// ����� addChatLine ��������� ������ ���� �� ��������
	
	addChatLine : function(params){
		
		// ��� ��������� ������� ��������� � ������� ���������� ����� ������������
		
		var d = new Date();
		if(params.time) {
			
			// PHP ���������� ����� � ������� UTC (GMT). �� ���������� ��� ��� ������������ ������� date
			// � ����������� ������ � ������� ���������� ����� ������������. 
			// JavaScript ������������ ��� ��� ���.
			
			d.setUTCHours(params.time.hours,params.time.minutes);
		}
		
		params.time = (d.getHours() < 10 ? '0' : '' ) + d.getHours()+':'+
					  (d.getMinutes() < 10 ? '0':'') + d.getMinutes();
		
		var markup = chat.render('chatLine',params),
			exists = jQuery('#chatLineHolder .chat-'+params.id);

		if(exists.length){
			exists.remove();
		}
		
		if(!chat.data.lastID){
			// ���� ��� ������ ������ � ����, ������� 
			// �������� � ���������� � ���, ��� ��� ������ �� ��������:
			
			jQuery('#chatLineHolder p').remove();
		}
		
		// ���� ��� �� ��������� ������ ����:
		if(params.id.toString().charAt(0) != 't'){
			var previous = jQuery('#chatLineHolder .chat-'+(+params.id - 1));
			if(previous.length){
				previous.after(markup);
			}
			else chat.data.jspAPI.getContentPane().append(markup);
		}
		else chat.data.jspAPI.getContentPane().append(markup);
		
		// ��� ��� �� �������� ����� �������, �����
		// ����� ���������������� ������ jScrollPane:
		
		chat.data.jspAPI.reinitialise();
		chat.data.jspAPI.scrollToBottom(true);
		
	},
	
	// ������ ����� ����������� ��������� ������ � ����
	// (������� � lastID), � ��������� �� �� ��������.
	
	getChats : function(callback){
		jQuery.tzGET('getChats',{lastID: chat.data.lastID},function(r){
			
			for(var i=0;i<r.chats.length;i++){
				chat.addChatLine(r.chats[i]);
			}
			
			if(r.chats.length){
				chat.data.noActivity = 0;
				chat.data.lastID = r.chats[i-1].id;
			}
			else{
				// ���� ��� ������� � ����, ����������� 
				// ������� noActivity.
				
				chat.data.noActivity++;
			}
			
            
			if(!chat.data.lastID){
				chat.data.jspAPI.getContentPane().html('<p class="noChats">������ ��� �� ��������</p>');
			}
 
 
/*
var cvul = jQuery("span[class=time]").attr("date");
jQuery("span[class=time]").attr("onClick", "parent.document.tesla.chatText.focus();parent.document.tesla.chatText.value=' ["+cvul+"] '+parent.document.tesla.chatText.value;return false;");
//.addClass("alink")
*/

			// ������������� ������� ��� ���������� �������
			// � ����������� ���������� ����:
			
			var nextRequest = 1000;
			
			// 2 �������
			if(chat.data.noActivity > 3){
				nextRequest = 2000;
			}
			
			if(chat.data.noActivity > 10){
				nextRequest = 8000;
			}
			
			// 15 ������
			if(chat.data.noActivity > 20){
				nextRequest = 10000;
			}
                       
		
			setTimeout(callback,nextRequest);
		});
	},
	
	// ������ ������ ���� �������������.
	
	getUsers : function(callback){
		jQuery.tzGET('getUsers',function(r){
			
			var users = [];
			
			for(var i=0; i< r.users.length;i++){
				if(r.users[i]){
					users.push(chat.render('user',r.users[i]));
				}
			}
			
			jQuery('#chatUsers').html(users.join(', '));
			
			setTimeout(callback,10000);
		});
	},
	
	// ������ ����� ������� ��������� �� ������ ������� ��������:
	
	displayError : function(msg){
		var elem = jQuery('<div>',{
			id		: 'chatErrorMessage',
			html	: msg
		});
		
		elem.click(function(){
			jQuery(this).fadeOut(function(){
				jQuery(this).remove();
			});
		});
		
		setTimeout(function(){
			elem.click();
		},5000);
		
		elem.hide().appendTo('body').slideDown();
	}
    
    
    
};

// ������������ GET & POST:

jQuery.tzPOST = function(action,data,callback){
	jQuery.post('shoutbox.php?action='+action,data,callback,'json');
}

jQuery.tzGET = function(action,data,callback){
	jQuery.get('shoutbox.php?action='+action,data,callback,'json');
}

// ����� jQuery ��� ����������� ������:

jQuery.fn.defaultText = function(value){
	
	var element = this.eq(0);
	element.data('defaultText',value);
	
	element.focus(function(){
		if(element.val() == value){
			element.val('').removeClass('defaultText');
		}
	}).blur(function(){
		if(element.val() == '' || element.val() == value){
			element.addClass('defaultText').val(value);
		}
	});
	
	return element.blur();
}