<?

/**
 * @author 7Max7
 * @copyright 2014 ��� ������ Tesla TT (Tesla Tracker) v.Platinum IV
 * ������ ������� �� 26.09.2016
 */

define("IMAGE_BASE", 'torrents/images');
define("THUMBS_BASE", 'torrents/thumbnail');


list ($usec, $sec) = explode(" ", microtime());
$tstart = ((float)$usec + (float)$sec); // Start time


/// ������ �������� � ����������
$Thumb_Config = array(

/// ��������� ������� �������� ����������� �������. ��������: ������ �������� �� ftp � ������ ������� ��� ������, ������, �� ����� ������� ����� repair_tags.php - ������� ��� ������ ��� ������ � ������� ���������.
"work" => true, /// false ���������

"TTL" => 0 /// ����� ����� ������ � ����. �������� � ��������� �������� �����.

);



function save_image($image, $file, $extention){

if (file_exists($file)){

@unlink($file);
save_image($image, $file, $extention);

} else {

switch ($extention){

case "jpg" :
case "jpeg" :
				imagejpeg($image, $file);
				header("Content-type: image/jpeg");
				imagejpeg($image);
				break;

case "png" :
				imagepng($image, $file);
				header("Content-type: image/png");
				imagepng($image);
				break;

case "gif" :
				imagegif($image, $file);
				header("Content-type: image/gif");
				imagegif($image);
				break;

}

return true;
}

}



$for = (isset($_GET['for']) ? (string) $_GET['for']:"");

if (!empty($for)) {
	
if ($for == 'block')
define("MAX_WIDTH", 150);

elseif ($for == 'browse')
define("MAX_WIDTH", 100);

elseif ($for == 'details')
define("MAX_WIDTH", 512);

elseif ($for == 'beta')
define("MAX_WIDTH", 85);

elseif ($for == 'rss')
define("MAX_WIDTH", 100);

elseif ($for == 'getdetals')
define("MAX_WIDTH", "400");

} else {
$for = 'details';
define("MAX_WIDTH", "400");
}


if (!is_dir(THUMBS_BASE))
mkdir(THUMBS_BASE); /// � ������ ��� ����� - ������� ��.


$image_file = trim(strip_tags($_GET['image']));
$ext = strtolower(end(explode('.', $image_file)));

if (empty($image_file)) create_error(false, true);

if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image_file)){

$image_path = $image_file;
$outimage = true;

///http://localhost/thumbnail.php?image=http://img215.imageshack.us/img215/6703/412902.png
//$id = (string) current(explode('.', end(explode('/', $image_path))));

$thumb_path = THUMBS_BASE."/".md5($image_file)."_".$for.".".$ext;

} else {

$image_path = IMAGE_BASE."/".$image_file;
$outimage = false;

//$id = (string) current(explode('.',$image_file));

$thumb_path = THUMBS_BASE."/".md5($image_file)."_".$for.".".$ext;

}


if (!in_array($ext, array("gif","png", "jpeg","jpg")))
create_error($ext);

/// ��������� ��� ���������� �������
if ($Thumb_Config["work"] <> true){
  header("Location: ".$image_path);
  die;
}



if (file_exists($thumb_path)) {

if (!empty($Thumb_Config["TTL"]) && date('i')%2==0) {
$expire = 60*60*24*$Thumb_Config["TTL"]; // ���� ���� 1 � TTL
if (filemtime($thumb_path) > (time() - $expire))
@unlink($thumb_path);
}


header("Location: ".$thumb_path);
die();
}

$ori_url = $image_path;

$opts = array($scheme => array('method' => 'GET', 'protocol_version' => '1.1', 'follow_location' => 1, 'header' => 'Accept-Encoding: gzip\r\nUser-Agent: Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'timeout' => 10));
$context = @stream_context_create($opts);
$imageFile = @file_get_contents($image_path, false, $context);

if (!empty($imageFile)){
$tmpfname = tempnam (ini_get('upload_tmp_dir'), "sess_im");

$fp = fopen($tmpfname, "w");
fwrite($fp, $imageFile);
fclose($fp);

unset($imageFile);
$image_path = $tmpfname;
}


$size = @getimagesize($image_path) or create_error('No image '.$image_file, true);

$mime = strtolower(end(explode("/", $size['mime'])));
if (!empty($mime) && $mime <> $ext) $ext = $mime;

if ($ext == 'jpg' || $ext == 'jpeg')
$img = @imagecreatefromjpeg($image_path) or create_error('Not a valid JPG');
elseif ($ext == 'png')
$img = @imagecreatefrompng($image_path) or create_error('Not a valid PNG');
elseif ($ext == 'gif')
$img = @imagecreatefromgif($image_path) or create_error('Not a valid GIF');


# If an image was successfully loaded, test the image for size
if ($img && !empty($size[0])) {

# Get image size and scale ratio
$width = $size[0]; ///$width = imagesx($img);
$height = $size[1]; ///$height = imagesy($img);

$scale = MAX_WIDTH/$width;

# If the image is larger than the max shrink it
if ($scale < 1) {
if ($width > MAX_WIDTH) {
$new_width = floor($scale*$width);
$new_height = floor($scale*$height);
# Create a new temporary image
$tmp_img = imagecreatetruecolor($new_width, $new_height);
# Copy and resize old image into new image
imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
imagedestroy($img);
//imagedestroy($tmp_img);
$img = $tmp_img;
}

} else {

#if (!empty($tmpfname)) create_error('', true);

if (!empty($thumb_path) AND !empty($tmpfname) ){

if (copy($tmpfname, $thumb_path)) {
$ori_url = $thumb_path; 
@unlink($tmpfname);
}

}

header("Location: ".$ori_url);
die;

}

if (!empty($tmpfname)) unlink($tmpfname);

} else create_error();



# Create error image if necessary
function create_error($str = false, $my_poster = false) {

global $img, $for, $image_path, $tstart;

global $tmpfname;
if (!empty($tmpfname)) unlink($tmpfname);

list ($usec, $sec) = explode(" ", microtime());
$time = mb_substr((((float)$usec + (float)$sec) - $tstart), 0, 8);


if ($my_poster == true)
$img = @imagecreatefrompng(IMAGE_BASE."/default_torrent.png");
else {

$img = imagecreate(100,100);
imagecolorallocate($img,255,255,255);

}

$c = imagecolorallocate($img,0,0,0);

if (empty($str))
$str = "No image (".$for.")";

$str2 = $image_path;
// ���������� ���������� ������ ������

$size = 2; // ������ ������
imagefontwidth($size)*mb_strlen($str)-3;
imagefontheight($size)-3;

imagestring($img, $size, 3, 3, $str,$c);
imagestring($img, $size, 3, 13, $str2,$c);

if (!empty($time))
imagestring($img, $size, 3, 23, "Time: ".$time,$c);

header("Content-type: image/jpeg");
imagejpeg($img);

die();
}



save_image($img,$thumb_path,$ext);


?>