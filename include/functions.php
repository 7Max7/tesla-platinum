<?
if(!defined('IN_TRACKER'))
die('Hacking attempt!');
 
require_once(ROOT_PATH.'/include/functions_global.php');

function local_user() {
return $_SERVER["SERVER_ADDR"] == $_SERVER["REMOTE_ADDR"];
}

function mysql_modified_rows () {
$info_str = mysql_info();
$a_rows = mysql_affected_rows();
preg_match("/Rows matched: ([0-9]*)/", $info_str, $r_matched);
return ($a_rows < 1) ? (isset($r_matched[1]) ? $r_matched[1]:0):$a_rows;
}

function accessadministration() {

global $SITE_Config, $tracker_lang;

if ($SITE_Config["accessadmin"] == true){

include('include/passwords.php');

if (get_user_class() < UC_SYSOP || !isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] <> $Sysop_pass['login'] || md5(md5($_SERVER['PHP_AUTH_PW']).$_SERVER['PHP_AUTH_USER']) <> $Sysop_pass['passwords']){

header("WWW-Authenticate: Basic realm=\"Administration\"");
header("HTTP/1.0 401 Unauthorized");

stderr($tracker_lang['error'], $tracker_lang['not_authorized']);
die;
}

unset($Sysop_pass);
}

if (get_user_class() == UC_SYSOP)
@setcookie("debug", "yes", "0x7fffffff", "/");

}

function userlogin ($lightmode = false) {

global $SITE_Config, $default_language, $tracker_lang, $use_lang, $use_ipbans;

unset($GLOBALS["CURUSER"]);


# �������� �� ����������� v2.0 ( �������� android.googlequicksearchbox )
if (!isset($_SESSION['is_mobile']) OR $_SESSION['is_amobile'] <> crc32($_SERVER['HTTP_REFERER']) ){

if ( preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $_SERVER['HTTP_USER_AGENT']) )
$_SESSION['is_mobile'] = true;

elseif ( preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($_SERVER['HTTP_USER_AGENT'], 0, 4)) )
$_SESSION['is_mobile'] = true;

elseif ( preg_match('/^android\-app\:\/\/com\.google\.android/i', $_SERVER['HTTP_REFERER']) )
$_SESSION['is_mobile'] = true;

else
$_SESSION['is_mobile'] = false;

$_SESSION['is_amobile'] = crc32($_SERVER['HTTP_REFERER']);

}
# �������� �� ����������� v2.0


$ip = getip();
$nip = ip2long($ip);



if ($use_ipbans && !$lightmode) {

# ����������� �������� ����� �� ip �� ���� ������
if ( (!isset($_SESSION['bantime']) OR $_SESSION['bantime'] < get_date_time(gmtime() - 60*5)) AND $_SERVER['HTTP_X_REQUESTED_WITH'] <> 'XMLHttpRequest' ){

$bans_sql = sql_query("SELECT bans_time, id FROM bans WHERE ".sqlesc($nip)." >= first AND ".sqlesc($nip)." <= last LIMIT 1") or sqlerr(__FILE__,__LINE__);
if (mysql_num_rows($bans_sql) > 0){
$banned_ar = mysql_fetch_assoc($bans_sql);
} else {
#session_unregister('banned_mas');
unset($_SESSION['banned_mas']);
}

$_SESSION['bantime'] = get_date_time();

} elseif ( isset($_SESSION['banned_mas']) )
$banned_ar = unserialize($_SESSION['banned_mas']);


if ( isset($banned_ar) ){

if ( !isset($_SESSION['banned_mas']) OR $_SESSION['banned_mas'] <> serialize($banned_ar) ) $_SESSION['banned_mas'] = serialize($banned_ar);

if ($use_lang == true)
include_once(ROOT_PATH.'/languages/lang_'.$default_language.'/lang_main.php');

header("HTTP/1.0 403 Forbidden");

$file_name = ROOT_PATH."/torrents/txt/banned_".$banned_ar['id'].".txt";
$file_content = 0;
if (@file_exists($file_name)) $file_content = @file_get_contents($file_name);
++$file_content;
@file_put_contents($file_name, $file_content);

echo "<title>".$tracker_lang['er_403_i']."</title>";

echo "<script><!--
var tit = document.title; var c = 0;
function writetitle() {
document.title = tit.substring(0,c);
if(c==tit.length){
c = 0;
setTimeout(\"writetitle()\", 30000)
} else {
c++;
setTimeout(\"writetitle()\", 500)
}}
writetitle()
// --></script>";


echo "<Script Language=\"JavaScript\">
function initArray()
{this.length = initArray.arguments.length
for (var i = 0; i < this.length; i++)
this[i+1] = initArray.arguments[i]}
colorList = new initArray(\"ffffff\", \"eeeeee\", \"dddddd\", \"cccccc\", \"bbbbbb\", \"aaaaaa\", \"999999\", \"888888\", \"777777\", \"666666\", \"555555\", \"444444\", \"333333\", \"222222\", \"111111\", \"000000\", \"000000\", \"111111\", \"222222\", \"333333\", \"444444\", \"555555\",\"666666\", \"777777\", \"888888\", \"999999\", \"aaaaaa\", \"bbbbbb\",\"cccccc\", \"dddddd\", \"eeeeee\", \"ffffff\");
var currentColor = 1;

function backgroundChanger(){document.bgColor = colorList[currentColor];
if (currentColor++ < 32) setTimeout(\"backgroundChanger()\", 5);
else return}

backgroundChanger();

var sizes = new Array(\"0px\", \"1px\", \"2px\",\"4px\", \"8px\"); 
sizes.pos = 0;

function rubberBand() {
var el = document.all.elastic;
if (null == el.direction) el.direction = 1;
else if ((sizes.pos > sizes.length - 2) || (0 == sizes.pos)) el.direction *= -1;
el.style.letterSpacing = sizes[sizes.pos += el.direction];
}

</Script>";

/*
$added = ROOT_PATH."/bans_added.html";
if (file_exists($added))
echo file_get_contents($added);
*/

echo "<body onload=\"window.tm = setInterval('rubberBand()', 100);\">";
echo "<h1 style=\"font-size: 28px;font-family: \"Times New Roman\", Times, serif;color: #FFFFFF;\" align=\"center\">".$tracker_lang['warning']."</h1>";
echo "<h1 style=\"font-family: \"Times New Roman\", Times, serif;font-size: 16px;color: #999999;\" id=\"elastic\" align=\"center\">".$tracker_lang['er_403_i']."</h1>";
echo "<p align=\"center\" style=\"font-size: 48px;font-family: \"Times New Roman\", Times, serif;color: #006699;\">".sprintf($tracker_lang['you_banip'], "<br />".($banned_ar["bans_time"] == '0000-00-00 00:00:00' ? $tracker_lang['unlimited_time']:$banned_ar["bans_time"]))."</p>";
echo "<p align=\"center\" style=\"font-size: 28px;font-family: \"Times New Roman\", Times, serif;color: #FFFFFF;\">".$tracker_lang['your_ip'].": ".$ip."<br />".$tracker_lang['hi_from_maks']."</p>";
echo "</body>";
die;

}
# ����������� �������� ����� �� ip �� ���� ������

}

if (!isset($_COOKIE["uid"]) || !isset($_COOKIE["pass"]) || (!empty($_COOKIE["pass"]) && strlen($_COOKIE["pass"]) <> 32)){

if ($use_lang == true)
include_once(ROOT_PATH.'/languages/lang_'.$default_language.'/lang_main.php');

logoutcookie();

if (!$lightmode) user_session();

return;
}


$id = (!empty($_COOKIE["uid"]) ? intval($_COOKIE["uid"]):0);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] <> 'XMLHttpRequest'){
$res = sql_query("SELECT * FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc($res);
} else {
$res = sql_query("SELECT * FROM users WHERE id = ".sqlesc($id), $cache = array("type" => "disk", "file" => "arrid_".$id, "time" => 60)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_assoc_($res);
}

if (isset($_GET["cache_deleting"]) && $row["class"] < UC_SYSOP) unset($_GET["cache_deleting"]);


if ($use_lang == true)
include_once(ROOT_PATH.'/languages/lang_'.(!empty($row["language"]) ? $row["language"]:$default_language).'/lang_main.php');

////////////////// encode ///////////////////
if ($row["enabled"] == "no") {
$str = $row["username"];
$checksum = crc32($str);
$data = $str.":".$checksum;
$cookie = base64_encode($data);
@setcookie("offlog", $cookie, "0x7fffffff", "/");

logoutcookie();

if (!$lightmode) user_session();

return;
}
////////////////// encode ///////////////////

/// ������ �� ����� ����� aka 7Max7
$she_md5 = ($row["shelter"] == "ag" ? $row["crc"]:$row["ip"]);
$tesla_ip = "T".md5($row["id"]+$she_md5+$row["id"]);
/// ������ �� ����� ����� aka 7Max7

if ($row["id"] <> $id || $row["status"] <> "confirmed" || $_COOKIE["tesla"] <> $tesla_ip || $_COOKIE["pass"] !== $row["passhash"]){

logoutcookie();

if (!$lightmode) user_session();

return;
}

$updateset = array();

if ($ip <> $row['ip'])
$updateset[] = 'ip = '.sqlesc($ip);



if (strtotime($row['last_access']) < (strtotime(get_date_time()) - 300)){

/// ������� ��������������� ������� ���� �� ��� � ���
if ($row['last_checked'] < $row['last_access']){

$unmark = get_row_count("snatched", "WHERE (SELECT COUNT(*) FROM ratings WHERE torrent = snatched.torrent AND user = ".sqlesc($row["id"]).") < '1' AND snatched.finished = 'yes' AND userid = ".sqlesc($row["id"]));
$updateset[] = "unmark = ".sqlesc($unmark);

if (date('i')%2 == 1){
$unseed = get_row_count("torrents", "WHERE owner = ".sqlesc($row["id"])." AND (leechers / seeders >= 4) AND multitracker = 'no' LIMIT 500");
$updateset[] = "unseed = ".sqlesc($unseed);
} else {
$unmatch = get_row_count("my_match", "WHERE userid = ".sqlesc($row["id"]));
$updateset[] = "unmatch = ".sqlesc($unmatch);
}

}

$left_time = intval((strtotime(get_date_time()) - 300)-strtotime($row['last_access']));
$updateset[] = "on_line = on_line + ".sqlesc($left_time < 100 ? (300+$left_time) : 300);

$updateset[] = "last_access = ".sqlesc(get_date_time());

if ($row['last_access'] < get_date_time(gmtime() - 30*60)){
$updateset[] = "last_checked = ".sqlesc($row['last_access']); /// 30 �����
setcookie("markview", "", 0x7fffffff, "/");
}

if ($row['last_access'] < get_date_time(gmtime() - 30*60)){
/// �������� ���������� ������������� ���������
$unread = get_row_count("messages", "WHERE receiver = ".sqlesc($row["id"])." AND location = '1' AND unread = 'yes'");
$updateset[] = "unread = ".sqlesc($unread);
}

}

if (count($updateset)){
sql_query("UPDATE users SET ".implode(", ", $updateset)." WHERE id = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
unsql_cache("arrid_".$row["id"]);
}

$file_end = end(explode('/', $_SERVER["SCRIPT_FILENAME"]));

$php_over = array("online.php","0nline.php","shoutbox.php","poll.core.php","autocomplete.php","index.php","/","block-user_jquery.php","block-bot_online_jquery.php","md5.php","block-online_jquery.php");

if ($row['monitoring'] == 'yes' && (!in_array($file_end, $php_over) || (!in_array($file_end, $php_over) && $_POST))) {

$sf = ROOT_PATH."/cache/monitoring_".$row["id"].".txt";
$fpsf = fopen($sf,"a+");

$ag = htmlentities($_SERVER["HTTP_USER_AGENT"]);
$from = htmlspecialchars_uni($_SERVER["HTTP_REFERER"]);
$host = htmlspecialchars_uni($_SERVER["REQUEST_URI"]);
$getiing = serialize($_GET ? $_GET:"")."||".serialize($_POST ? $_POST:"");

fputs($fpsf, "#".$host."#".$ip."##".get_date_time()."#".$getiing."\n\r");
fclose($fpsf);

/*
///// ���� ����� ���������������
include (ROOT_PATH."/include/zip.lib.php");
if (@filesize($sf) >= 2000000 && @filesize($sf)<= 3000000) /// ���� ������ 2000000 ���� - 2 �����
{

$dsf=date('H-i-s');
$ziper_newname=ROOT_PATH."/cache/$dsf-monitoring_".$row["id"].".zip";
$ziper = new zipfile();
$ziper->addFiles(array($sf));  //array of files
$ziper->output($ziper_newname);
@unlink ($sf);
} elseif (@filesize($sf) >= 3000000)
@unlink($sf);
/// ������ � ����� ���� ������� ��������
///// ���� ����� ���������������
*/

}

if (isset($_POST["id_styles"])){

$id_themes = intval(!empty($_POST["id_styles"]) ? $_POST["id_styles"]:0);

if (is_valid_id($id_themes)){
$dus = sql_query("SELECT uri FROM stylesheets WHERE id = ".sqlesc($id_themes)) or sqlerr(__FILE__,__LINE__);
$ru = mysql_fetch_array($dus);

if (!empty($ru["uri"]) && is_dir(ROOT_PATH."/themes/".$ru["uri"])){
sql_query("UPDATE users SET stylesheet = ".sqlesc($ru["uri"])." WHERE id = ".sqlesc($row["id"])) or sqlerr(__FILE__, __LINE__);
$row["stylesheet"] = $ru["uri"];
}

}

}

$GLOBALS["CURUSER"] = $row;

if (!$lightmode) user_session();

}

function logincookie($id, $passhash, $shelter, $updatedb = 1, $expires = 0x7fffffff) {

/// ������ �� ����� ����� aka 7Max7
$crc32_now = crc32(htmlentities($_SERVER["HTTP_USER_AGENT"]));
$she_md5 = ($shelter == "ag" ? $crc32_now:getip());
$tesla_ip = "T".md5($id+$she_md5+$id);

setcookie("tesla", $tesla_ip, $expires, "/");
setcookie("uid", $id, $expires, "/");
setcookie("pass", $passhash, $expires, "/");

if ($updatedb) {

if (!empty($crc32_now) && !empty($id)) {

$res = sql_query("SELECT crc, idagent FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($res);

if (!empty($row["idagent"]))
$array_idagent = explode(",", $row["idagent"]);
else
$array_idagent = array();

$agent = htmlentities($_SERVER["HTTP_USER_AGENT"]);
$hache = $crc32_now;

/// ���� ������������ �� ����� �������� '�������'
if (empty($row["crc"])) {

/// ������� �������� '�������' � ������� ���������
sql_query("INSERT INTO useragent (crc32, agent, added) VALUES (".sqlesc($hache).", ".sqlesc($agent).", ".sqlesc(get_date_time()).")");

$next_id = mysql_insert_id();

/// �������������, ���� �� id ��� �������� '�������'
if (empty($next_id)) {
$du = sql_query("SELECT id FROM useragent WHERE crc32 = ".sqlesc($hache)) or sqlerr(__FILE__,__LINE__);
$rodu = mysql_fetch_array($du);
$next_id = $rodu['id'];
}
$array_idagent[] = $next_id;

sql_query("UPDATE users SET crc = ".sqlesc($hache).", idagent = ".sqlesc(implode(",", array_unique($array_idagent)))." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

/// ����� ���������� �������� '�������' � �����
} elseif ($row["crc"] <> $hache) {
/// ��������� id � ������ ������ ��������
$du = sql_query("SELECT id FROM useragent WHERE crc32 = ".sqlesc($hache)) or sqlerr(__FILE__,__LINE__);
$rodu = mysql_fetch_array($du);
$f_id = $rodu['id'];

/// ���� �� ������ �������� �� ������� � ��� ������� �������� � ������� ��� �������������� �������������...
if (!empty($f_id) && !in_array($f_id, $array_idagent)) {
$array_idagent[] = $f_id;

sql_query("UPDATE users SET crc = ".sqlesc($hache).", idagent = ".sqlesc(implode(",", array_unique($array_idagent)))." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
/// ���� ���� � ������� � �� ����� - ������� ����� �������
} elseif (empty($f_id)){

sql_query("INSERT INTO useragent (crc32, agent, added) VALUES (".sqlesc($hache).",".sqlesc($agent).", ".sqlesc(get_date_time()).")");
$next_id = mysql_insert_id();

$array_idagent[] = $next_id;

sql_query("UPDATE users SET crc = ".sqlesc($hache).", idagent = ".sqlesc(implode(",", array_unique($array_idagent)))." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
} else
sql_query("UPDATE users SET crc = ".sqlesc($hache)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);

}
}

sql_query("UPDATE users SET last_login = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
}

}

function logoutcookie() {

if (!empty($_COOKIE["tesla"])) setcookie("tesla", "", 0x7fffffff, "/");
if (!empty($_COOKIE["uid"])) setcookie("uid", "", 0x7fffffff, "/");
if (!empty($_COOKIE["pass"])) setcookie("pass", "", 0x7fffffff, "/");
if (!empty($_COOKIE["debug"])) setcookie("debug", "", 0x7fffffff, "/");

}

function user_session() {

global $CURUSER, $use_sessions, $SITE_Config;

if (!$use_sessions) return;

$ip = getip();
$url = htmlspecialchars_uni($_SERVER["REQUEST_URI"]);

$uid = ($CURUSER ? $CURUSER['id']:-1);
$username = ($CURUSER ? $CURUSER['username']:'');
$class = ($CURUSER ? $CURUSER['class']:-1);

$past = get_date_time(gmtime() - 300);
$sid = session_id();

$where = $updateset = array();

if ($sid)
$where[] = "sid = ".sqlesc($sid);
elseif ($uid)
$where[] = "uid = ".$uid;
else
$where[] = "ip = ".sqlesc($ip);

$agent = htmlentities($_SERVER["HTTP_USER_AGENT"]);
$hache = crc32($agent);

$updateset[] = "sid = ".sqlesc($sid);
$updateset[] = "uid = ".sqlesc($uid);
$updateset[] = "username = ".sqlesc($username);
$updateset[] = "class = ".sqlesc($class);
$updateset[] = "time = ".sqlesc(get_date_time());
$updateset[] = "url = ".sqlesc($url);
$updateset[] = "ip = ".sqlesc($ip);

$s = ($_SERVER['SERVER_PORT'] == 443 ? "https://" : "http://").htmlspecialchars_uni($_SERVER['HTTP_HOST']);
$site = parse_url($s, PHP_URL_HOST);
$updateset[] = "host = ".sqlesc($site);

if ($CURUSER["crc"] <> $hache)
$updateset[] = "useragent = ".sqlesc($agent);

if ($url == "/")
$url = "/index.php";

if (count($updateset) && (stripos($_SERVER["HTTP_REFERER"], $url) != true))
sql_query("UPDATE LOW_PRIORITY sessions SET ".implode(", ", $updateset)." WHERE ".implode(" AND ", $where));
// ���������� ��� ������ ������, � � ���� ����������� ���

if (mysql_modified_rows() == 0){

sql_query("INSERT INTO sessions (sid, uid, username, class, ip, url, host, useragent, time) VALUES (".implode(", ", array_map("sqlesc", array($sid, $uid, $username, $class, $ip, $url, $site, $agent, get_date_time()))).")");



/// ���� http ���� ������
if ($SITE_Config["anti_httpdoss"] == true && !preg_match("/BTWebClient/i", $_SERVER["HTTP_USER_AGENT"]) && !preg_match("/bot/is", $_SERVER["HTTP_USER_AGENT"])) {

global $tracker_lang;

$num_sesson = get_row_count("sessions", "WHERE time > ".sqlesc(get_date_time(gmtime() - 60))." AND ip = ".sqlesc($ip));
if ($num_sesson > 20)
die("<b>".$tracker_lang['antibot_system']."</b>: ".sprintf($tracker_lang['block_session_i'], "<b>".$num_sesson."</b>", get_date_time(gmtime() - 60)." - ".get_date_time()));

}
/// ���� http ���� ������

// ������� ������
sql_query("DELETE FROM sessions WHERE time < ".sqlesc(get_date_time(gmtime() - 1200))) or sqlerr(__FILE__,__LINE__);
// ������� ������

}

if ($CURUSER && !empty($agent)) {

if (!empty($CURUSER["idagent"]))
$array_idagent = explode(",", $CURUSER["idagent"]);
else
$array_idagent = array();

$agent = htmlentities($_SERVER["HTTP_USER_AGENT"]);
$hache = crc32($agent);

/// ���� ������������ �� ����� �������� '�������'
if (empty($CURUSER["crc"])) {

/// ������� �������� '�������' � ������� ���������
sql_query("INSERT INTO useragent (crc32, agent, added) VALUES (".sqlesc($hache).", ".sqlesc($agent).", ".sqlesc(get_date_time()).")");

$next_id = mysql_insert_id();

/// �������������, ���� �� id ��� �������� '�������'
if (empty($next_id)) {
$du = sql_query("SELECT id FROM useragent WHERE crc32 = ".sqlesc($hache)) or sqlerr(__FILE__,__LINE__);
$rodu = mysql_fetch_array($du);
$next_id = $rodu['id'];
}
$array_idagent[] = $next_id;

sql_query("UPDATE users SET crc = ".sqlesc($hache).", idagent = ".sqlesc(implode(",", array_unique($array_idagent)))." WHERE id = ".sqlesc($uid)) or sqlerr(__FILE__,__LINE__);

/// ����� ���������� �������� '�������' � �����
} elseif ($CURUSER["crc"] <> $hache) {
/// ��������� id � ������ ������ ��������
$du = sql_query("SELECT id FROM useragent WHERE crc32 = ".sqlesc($hache)) or sqlerr(__FILE__,__LINE__);
$rodu = mysql_fetch_array($du);
$f_id = $rodu['id'];

/// ���� �� ������ �������� �� ������� � ��� ������� �������� � ������� ��� �������������� �������������...
if (!empty($f_id) && !in_array($f_id, $array_idagent)) {
$array_idagent[] = $f_id;

sql_query("UPDATE users SET crc = ".sqlesc($hache).", idagent = ".sqlesc(implode(",",array_unique($array_idagent)))." WHERE id = ".sqlesc($uid)) or sqlerr(__FILE__,__LINE__);
/// ���� ���� � ������� � �� ����� - ������� ����� �������
} elseif (empty($f_id)){

sql_query("INSERT INTO useragent (crc32, agent, added) VALUES (".sqlesc($hache).",".sqlesc($agent).", ".sqlesc(get_date_time()).")");
$next_id = mysql_insert_id();

$array_idagent[] = $next_id;

sql_query("UPDATE users SET crc = ".sqlesc($hache).", idagent = ".sqlesc(implode(",",array_unique($array_idagent)))." WHERE id = ".sqlesc($uid)) or sqlerr(__FILE__,__LINE__);
} else
sql_query("UPDATE users SET crc = ".sqlesc($hache)." WHERE id = ".sqlesc($uid)) or sqlerr(__FILE__,__LINE__);

}
}




/// ��������� ���� ���� ���� 6 (aka UC_SYSOP)
if (get_user_class() > UC_SYSOP) {

$aclas = $updateset2 = array();
for ($i = $CURUSER["class"]; ($CURUSER["class"]+10); ++$i) {
if (get_user_class_name($i))
$aclas[] = $i;
else
break;
}

if (!in_array($CURUSER["class"], $aclas) && !get_user_class_name($CURUSER["class"])){

$updateset2[] = "usercomment = CONCAT_WS('', ".sqlesc(date("Y-m-d") . " - ��������� ���� � ".$CURUSER["class"]." �� ���� ������������.\n").", usercomment)";
$updateset2[] = "class = '0'";
$updateset2[] = "monitoring = 'yes'";
$updateset2[] = "promo_time = ".sqlesc(get_date_time());

sql_query("UPDATE users SET ".implode(", ", $updateset2)." WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);

}

}
/// ��������� ���� ���� ���� 6 (aka UC_SYSOP)


}


function get_server_load() {

global $tracker_lang, $phpver;

if (strtolower(substr(PHP_OS, 0, 3)) === 'win') {

if (class_exists("COM")) {
if ($phpver=="4") {
$wmi = new COM("WinMgmts:\\\\.");
$cpus = $wmi->InstancesOf("Win32_Processor");
$cpuload = 0;
$i = 0;
while ($cpu = $cpus->Next()) {
$cpuload += $cpu->LoadPercentage;
$i++;
}
$cpuload = round($cpuload / $i, 2);
return $cpuload;
} else {
$wmi = new COM("WinMgmts:\\\\.");
$cpus = $wmi->InstancesOf("Win32_Processor");
$cpuload = 0;
$i = 0;
foreach ($cpus as $cpu) {
$cpuload += $cpu->LoadPercentage;
$i++;
}
$cpuload = round($cpuload / $i, 2);
return $cpuload;
}
} else
return $tracker_lang["unknown"];

return 0;
} elseif (@file_exists("/proc/loadavg")) {
$load = @file_get_contents("/proc/loadavg");
$serverload = explode(" ", $load);
$serverload[0] = round($serverload[0], 4);
if (!$serverload) {
$load = @exec("uptime");
$load = explode("load averages?: ", $load);
$serverload = explode(",", $load[1]);
}
} else {
$load = @exec("uptime");
$load = explode("load averages?: ", $load);
$serverload = explode(",", $load[1]);
}
$returnload = trim($serverload[0]);
if (!$returnload) {
$returnload = $tracker_lang["unknown"];
}
return $returnload;
}

function autoclean() {

global $autoclean_interval;

$docleanup = 0;

$res = sql_query("SELECT value_u FROM avps WHERE arg = 'lastcleantime'") or sqlerr(__FILE__,__LINE__);
$row = mysql_fetch_array($res);

if (empty($row)) {
sql_query("INSERT INTO avps (arg, value_u) VALUES ('lastcleantime', ".time().")") or sqlerr(__FILE__,__LINE__);
return;
}

$ts = $row['value_u'];
if ($ts + $autoclean_interval > time())
return;

sql_query("UPDATE avps SET value_u = ".time()." WHERE arg = 'lastcleantime' AND value_u = ".$ts) or sqlerr(__FILE__,__LINE__);

if (!mysql_affected_rows())
return;

require_once(ROOT_PATH.'/include/cleanup.php');

docleanup();
}

function deadtime($date_time = false) {

global $announce_interval;

//$time = time() - floor($announce_interval * 1.3);
$time = time() - floor($announce_interval * 2);

if ($date_time == false)
return $time;
else
return get_date_time($time);

}

function timesec($input, $time = false) {

global $tracker_lang;

$search = array('January','February','March','April','May','June','July','August','September','October','November','December');
$replace = array($tracker_lang['my_months_january'], $tracker_lang['my_months_february'], $tracker_lang['my_months_march'], $tracker_lang['my_months_april'], $tracker_lang['my_months_may'], $tracker_lang['my_months_june'], $tracker_lang['my_months_july'], $tracker_lang['my_months_august'], $tracker_lang['my_months_september'], $tracker_lang['my_months_october'], $tracker_lang['my_months_november'], $tracker_lang['my_months_december']);

$seconds = strtotime($input);

return str_replace($search, $replace, date("H:i:s", $seconds));

}


function normaltime($input, $time = false) {

global $tracker_lang;

$search = array('January','February','March','April','May','June','July','August','September','October','November','December');
$replace = array($tracker_lang['my_months_january'], $tracker_lang['my_months_february'], $tracker_lang['my_months_march'], $tracker_lang['my_months_april'], $tracker_lang['my_months_may'], $tracker_lang['my_months_june'], $tracker_lang['my_months_july'], $tracker_lang['my_months_august'], $tracker_lang['my_months_september'], $tracker_lang['my_months_october'], $tracker_lang['my_months_november'], $tracker_lang['my_months_december']);

$seconds = strtotime($input);
if ($time == true)
$data = date("j F Y ".$tracker_lang['in']." H:i:s", $seconds);
else
$data = date("j F Y", $seconds);

return str_replace($search, $replace, $data);
}


function mkprettytime($s) {

if ($s < 0)
$s = 0;

$t = array();
foreach (array("60:sec","60:min","24:hour","0:day") as $x) {
$y = explode(":", $x);
if ($y[0] > 1) {
$v = $s % $y[0];
$s = floor($s / $y[0]);
} else
$v = $s;

$t[$y[1]] = $v;
}

if ($t["day"])
return $t["day"] . "d " . sprintf("%02d:%02d:%02d", $t["hour"], $t["min"], $t["sec"]);

if ($t["hour"])
return sprintf("%d:%02d:%02d", $t["hour"], $t["min"], $t["sec"]);

return sprintf("%d:%02d", $t["min"], $t["sec"]);
}

function mkglobal($vars) {

if (!is_array($vars))
$vars = explode(":", $vars);

foreach ($vars as $v) {
if (isset($_GET[$v]))
$GLOBALS[$v] = htmlspecialchars_uni(trim($_GET[$v]));
elseif (isset($_POST[$v]))
$GLOBALS[$v] = htmlspecialchars_uni(trim($_POST[$v]));
else
return 0;
}

return 1;

}

function tr ($x, $y, $noesc = 0, $prints = true, $width = "", $relation = '') {

if ($noesc) $a = $y;
else $a = str_replace("\n", "<br />\n", $y);

if ($prints) {
$print = "<td ".(empty($width) ? "":"width=\"".$width."\"")." class=\"heading\" valign=\"top\" align=\"right\">".$x."</td>";
$colpan = "align=\"left\"";
} else
$colpan = "colspan=\"2\"";

echo "<tr".($relation ? " relation=\"".$relation."\"":"").">".$print."<td class=\"soheading\" valign=\"top\" ".$colpan.">".$a."</td></tr>\n";
}

function validfilename($name) {

//if (mb_detect_encoding($name, array('UTF-8'))) $name = iconv("UTF-8", "cp1251", $name);

$name = trim(preg_replace("/\[((\s|.)+?)\]/is", "", preg_replace("/\(((\s|.)+?)\)/is", "", $name))); /// ����� �� ������ [] � ��

$name = str_replace(array("@","#","%",";","'","\"","/",":","&","~", "|"), "", $name);

//$name = substr(str_replace("_", " ", $name), 0, 256);

//return preg_match('/^[^\0-\x1f:\\\\\/?*\xff#<>|]+$/si', $name);

return preg_match('/^[^\0-\x1f:;\\\\\/?*#<>|]+$/si', $name); 
}


function validimage($file_path, $blank) {

global $CURUSER, $tracker_lang;

if (!$CURUSER)
stderr($tracker_lang['error_data'], $tracker_lang['loginid']);

if (empty($file_path) || !@getimagesize($file_path))
stderr($tracker_lang['tfile_notype'], $tracker_lang['invalid_typeimage']);

$file_contents = @file_get_contents($file_path);

$functions_to_shell = array("include", "file", "fwrite", "script", "body", "java","fopen", "fread", "require", "exec", "system", "passthru", "eval", "copy");

foreach ($functions_to_shell as $funct){

if (preg_match("/" . $funct ."+(\\s||)+[(]/", $file_contents)) {

if (!stristr($CURUSER["usercomment"], "������� ������ shell") && $CURUSER["monitoring"] <> "yes")
sql_query("UPDATE users SET usercomment = CONCAT_WS('', ".sqlesc(get_date_time()." ������� ������ shell (-".$funct."-) � ".$blank.".\n").", usercomment), monitoring = 'yes' WHERE id = ".$CURUSER["id"]) or sqlerr(__FILE__, __LINE__);

write_log($CURUSER["username"]." ������� ������ shell � ".$blank." (".$file["size"].").", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "error");

stderr($tracker_lang['error_data'], $tracker_lang['image_ofshell']);
break;
}

}

}

function validemail($email) {

if (preg_match("%^[A-Za-z0-9._-](([_\.\-]?[a-zA-Z0-9._-]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z])+$%", $email))
return true;
else
return false;

}


function sent_mail($to, $fromname, $fromemail, $subject, $body, $multiple = false, $multiplemail = '', $aattach = array()) {

global $SITENAME, $SITEEMAIL, $Signup_Config, $smtp, $smtp_from, $Mail_config, $tracker_lang;

//if ((empty($Mail_config['password']) || empty($Mail_config['account']) && $Mail_config['smtptype'] <> 'default')) return false;

if (empty($fromemail)) $fromemail = $SITEEMAIL;
if (empty($fromname)) $fromname = $SITENAME;

$SITEEMAIL = $Mail_config['account'];



$body = format_urls($body, false, true);

/// [url]http://www.example.com[/url] � (http|ftp|https|ftps)
$body = preg_replace("/\[url\]((http|ftp|https|ftps):\/\/[^()<>\s]+?)\[\/url\]/is", "<a href=\"\\1\" rel=\"nofollow\">\\1</a>", $body);

/// [url=http://www.example.com]example.com[/url] � (http|ftp|https|ftps)
$body = preg_replace("/\[url=((http|ftp|https|ftps):\/\/[^()<>\s]+?)\](.*?)\[\/url\]/is", "<a href=\"\\1\" rel=\"nofollow\">\\3</a>", $body);

/// [url=browse.php?search=�������]�������[/url]
$body = preg_replace("/\[url=([a-zA-Z_]+.php(.*?))\](.*?)\[\/url\]/is", "<a href=\"\\1\" rel=\"nofollow\">\\3</a>", $body);

/// ����� url ������ � ��������� �� � (http|ftp|https|ftps)
$body = preg_replace("/(\\n|\\s|^)((http|ftp|https|ftps):\/\/[^()<>\s]+)/is", "\\1<a rel=\"nofollow\" href=\"\\2\">\\2</a>", $body);

$body = preg_replace("#\n#is", "<br />\r\n", $body);



$result = true;
if ($Mail_config['smtptype'] == 'default') {
@mail($to, $subject, $body, "From: ".$SITEEMAIL) or $result = false;
} elseif ($Mail_config['smtptype'] == 'advanced') {
# Is the OS Windows or Mac or Linux?
if (strtoupper(substr(PHP_OS,0,3)=='WIN')) {
$eol="\r\n";
$windows = true;
} elseif (strtoupper(substr(PHP_OS,0,3)=='MAC'))
$eol="\r";
else
$eol="\n";

$mid = md5(getip() . $fromname);

$name = $_SERVER["SERVER_NAME"];

$headers.= "From: ".$fromname." <".$fromemail.">".$eol;
$headers.= "Reply-To: ".$fromname." <".$fromemail.">".$eol;
$headers.= "Return-Path: ".$fromname." <".$fromemail.">".$eol;
$headers.= "Message-ID: <".$mid." thesystem@".$name.">".$eol;
$headers.= "X-Mailer: PHP v".phpversion().$eol;
$headers.= "MIME-Version: 1.0".$eol;
$headers.= "Content-type: text/html; charset=".$tracker_lang['language_charset']."".$eol;
$headers.= "X-Sender: PHP".$eol;

if ($multiple)
$headers.= "Bcc: ".$multiplemail.".".$eol;

if ($smtp == "yes") {
ini_set('SMTP', $Mail_config['smtp']);
ini_set('smtp_port', $Mail_config['port']);
if ($windows)
ini_set('sendmail_from', $smtp_from);
}

@mail($to, $subject, $body, $headers) or $result = false;

ini_restore(SMTP);
ini_restore(smtp_port);

if ($windows)
ini_restore(sendmail_from);

} elseif ($Mail_config['smtptype'] == 'external' || empty($Mail_config['smtptype'])) {

require_once(ROOT_PATH.'/include/functions_smtp.lib.php');

$m = new Mail; /// ��������
$m -> From ($fromname.';'.$fromemail); /// �� ���� ������������ �����
$m -> To ($to); /// ���� �����������
$m -> Subject ($subject);
$m -> Body ($body);

if (!empty($aattach))
$m -> attach ($aattach); /// ������ ������ �� �����, ���� ����� ���������� ��� �������� � ������, ����� ������� � �������

if (preg_match("/������/is", $body))
$m -> Priority(2) ; /// ��������� ������

$smtp_server = $Mail_config['smtp'];
if ($Mail_config['port'] == 465 && !preg_match("/^ssl:\/\//is", $Mail_config['smtp'])) /// ����������� �� SSL/TLS
$smtp_server = 'ssl://'.$Mail_config['smtp'];

$m -> smtp_on($smtp_server, $Mail_config['account'], $Mail_config['password'], $Mail_config['port']);

$m -> Send();

if (($m-> send_error) == true){  // � ������ ����� ��������

$match = explode("\r\n\n", $m->smtp_log);
if (count($match)-2 > 1) $match = array_slice($match, count($match)-2);
$sprepl = htmlspecialchars(implode("\r\n\n", $match));
if (!empty($sprepl)) write_log("eMail ��������: ".$sprepl, "#7B5353", "mail");

return false;
}

}

return $result;
}

function sqlwildcardesc($x) {
return str_replace(array("%","_"), array("\\%","\\_"), mysql_real_escape_string($x));
}

function stdhead($title = false, $chat = false) {

global $CURUSER, $SITE_Config, $ICE, $SITENAME, $DEFAULTBASEURL, $ss_uri, $tracker_lang, $default_theme;

if (isset($_GET['copyright'])){



die ('<title>�������� ����� '.$SITENAME.'</title><style type="text/css"><!--.style4 {font-weight: bold; font-family: "Times New Roman", Times, serif; font-size: 16px; color: white;}.style5 { font-size: 28px; font-family: "Times New Roman", Times, serif;	color: white;}-->body {background:#AA55FF;color:#ffffff;font-family:courier;font-size:12pt;text-align:center;margin:10px;} blink {color:yellow;} a { border-top: 1px dashed #5087AD; color: #FFFFFF; padding: 0; text-decoration: none;} </style><body>

<p align="center" class="style5">
���� ������ �� ������ ��� ���������: <b>Tesla</b> (<b>T</b>esla <b>T</b>racker) <a href="http://www.muz-tracker.net"/>�</a> <b>v.Platinum IV</b> 2014 ����. 
<br />���������� ������ �������� 7Max7 (������ ����� � ���������).
<br />��������� ������� ������ ����� ������, ���� ����� 80 wmz (������ ������ ����� ����� <a href="useragreement.php">���������� �����</a>) �� �������������, ����� ���������� �� ��� � ICQ: 225454228 (7Max7)</p>
����������� ������� ��������� ������: <a href="http://www.muz-tracker.net">www.muz-tracker.net</a> �  <a href="http://omn-omn-omn.ru">omn-omn-omn.ru</a> � <a href="http://v3.muz-tracker.net/">v3.muz-tracker.net</a> (�������� ��������, � ������� ������� ��� ����).<br />��������: ��� ������� ������, ���������� ������ ���, ������������ � ������� ���������� �������.<br /> ��� ������ ��� ����� ������ � �������, ������������ ������ � ���������� ������, ����� �������������� ������ ������.
</body>');

}


if ($SITE_Config["site_online"] == false)
die ('<title>'.$tracker_lang['site_off'].'</title><LINK href="pic/favicon.ico" type="image/x-icon" rel="shortcut Icon"><style type="text/css"><!--.style5 { font-size: 48px; font-family: "Times New Roman", Times, serif;	color: white;}-->body {background:blue;color:#ffffff;font-family:courier;font-size:10pt;text-align:center;margin:10px;} </style><body><p align="center" class="style5">'.$tracker_lang['site_offreson'].': '.(empty($SITE_Config["reason_off"]) ? "N/A":"<br />".$SITE_Config["reason_off"]).'</p></body>');

if (!headers_sent()){
header("Content-Type: text/html; charset=" . $tracker_lang['language_charset']);
header("X-Powered-by: Tesla (Tesla Tracker) - www.muz-tracker.net");
header("X-Chocolate-to: ICQ 225454228 (7Max7)");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
}

if (empty($title)) $title = $SITENAME;
else
$title = htmlspecialchars_uni($title)." - ".$SITENAME;

$ss_uri = $CURUSER["stylesheet"];

if (!$CURUSER || empty($CURUSER["stylesheet"])) $ss_uri = $default_theme;
else $ss_uri = $CURUSER["stylesheet"];

require_once(ROOT_PATH."/themes/".($ss_uri)."/template.php");

if ($chat == true)
@require_once(ROOT_PATH."/themes/".($ss_uri)."/stdheadchat.php");
else
@require_once(ROOT_PATH."/themes/".($ss_uri)."/stdhead.php");

$GLOBALS['stHead'] = $chat;
$GLOBALS['stTitle'] = $title;

} // stdhead

function stdfoot($chat = false) {

global $CURUSER, $ss_uri, $tracker_lang, $queries, $tstart, $query_stat, $querytime;

if ($chat == true)
@require_once(ROOT_PATH."/themes/".($ss_uri)."/stdfootchat.php");
else
@require_once(ROOT_PATH."/themes/".($ss_uri)."/stdfoot.php");

$GLOBALS['stFood'] = $chat;

if (DEBUG_MODE && count($query_stat) && get_user_class() == UC_SYSOP && isset($_COOKIE["debug"])) {

foreach ($query_stat as $key => $value) {
echo "<div>[<b>".($key+1)."</b>] => ".(!empty($value["cache"]) ? "<font color=\"green\" title=\"".$tracker_lang['used_cache_sql']."\">".$value["cache"]."</font>":"")." <b>".($value["seconds"] <= 0.0009 ? "<font color=\"#0468f1\" title=\"".$tracker_lang['ultrafast_sql']."\">".$value["seconds"]."</font>":($value["seconds"] >= 0.01 ? "<font color=\"red\" title=\"".$tracker_lang['recommended_optm_sql']."\">".$value["seconds"]."</font>" : "<font color=\"blue\" title=\"".$tracker_lang['time_allowed_sql']."\">".$value["seconds"]."</font>"))."</b> [".$value["query"]."]</div>\n";
}

}

}

function mksecret($length = 20) {

$set = array("a","A","b","B","c","C","d","D","e","E","f","F","g","G","h","H","i","I","j","J","k","K","l","L","m","M","n","N","o","O","p","P","q","Q","r","R","s","S","t","T","u","U","v","V","w","W","x","X","y","Y","z","Z","1","2","3","4","5","6","7","8","9");

$str = "";
for ($i = 1; $i <= $length; $i++)
$str.= $set[rand(0, count($set)-1)];

return $str;
}

function httperr($code = 404) {

$sapi_name = php_sapi_name();
if ($sapi_name == 'cgi' || $sapi_name == 'cgi-fcgi')
header('Status: 404 Not Found');
else
header('HTTP/1.1 404 Not Found');

die;
}

function loggedinorreturn($nowarn = false) {

global $CURUSER, $DEFAULTBASEURL;

if (!$CURUSER) {
// header: 301 - ������������, 302 - ��������
if (!headers_sent()) {
header("Location: ".$DEFAULTBASEURL."/login.php?returnto=".htmlspecialchars_uni(basename($_SERVER["REQUEST_URI"])).($nowarn ? "&nowarn=1":""), true, 301);
die;
} else
die ("<script>setTimeout('document.location.href=\"".$DEFAULTBASEURL."/login.php?returnto=".htmlspecialchars_uni(basename($_SERVER["REQUEST_URI"]))."\"', 100);</script>");

}

}

//// ������� �������
function deletetorrent($id) {

if (!is_valid_id($id)) return false;

$filetor = ROOT_PATH."/torrents/".$id.".torrent";

sql_query("DELETE FROM torrents WHERE id = ".sqlesc($id));




sql_query("DELETE FROM checkcomm WHERE checkid = ".sqlesc($id)." AND torrent = 1");

foreach (explode(".", "bookmarks.report.thanks.cheaters.my_match") as $x)
sql_query("DELETE FROM ".$x." WHERE torrentid = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

foreach (explode(".", "peers.files.comments.ratings.snatched") as $x)
sql_query("DELETE FROM ".$x." WHERE torrent = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

if (file_exists($filetor)) unlink($filetor);

unsql_cache("simtorid_".$id); /// �������� ���� ������� ������
unsql_cache("simtorid_alt_".$id); /// �������� ���� ������� ������
unsql_cache("traffp_".$id);
unsql_cache("multi_viewid_".$id);

}


function pager($rpp, $count, $href, $opts = array()) {

global $tracker_lang;

$pages = ceil($count / $rpp);

if (empty($opts["lastpagedefault"]))
$pagedefault = 0;
else {
$pagedefault = ceil($count / $rpp);
if ($pagedefault < 0) $pagedefault = 0;
}

if (!empty($_GET["page"])) {
$page = (int) $_GET["page"];
if ($page < 0) $page = $pagedefault;
} else
$page = $pagedefault;

if (!empty($_GET["page"]) AND $_GET["page"]=="last"){
$pagedefault = ceil($count / $rpp);
$page = $pagedefault;
}

if ($page==0) {
$pagipage = 1;
$start = $page  * $rpp;
} else {
$pagipage = $page;
$start = ($page -1) * $rpp;
}

$pagerr ='<div class="paginator" id="paginator1"></div>
<div class="paginator_pages">'.$tracker_lang['page_all'].': '.$pages. ' | '.$tracker_lang['page_td'].': '.$rpp.' | '.$tracker_lang['page_total'].': '.$count.' </div>
<script type="text/javascript">
window.onload = function(){
pag1 = new Paginator(\'paginator1\', '.$pages.', 20, "'. ($pagipage ) .'", "'.$href.'page=");
pag2 = new Paginator(\'pag2\', '.$pages.', 20, "'. ($pagipage ) .'", "'.$href.'page=");
Paginator.resizePaginator(pag1);
Paginator.resizePaginator(pag2);
}
</script>';

$pagerre ='<div class="paginator" id="pag2"></div>
<div class="paginator_pages">'.$tracker_lang['page_all'].': '.$pages. ' | '.$tracker_lang['page_td'].': '.$rpp.' | '.$tracker_lang['page_total'].': '.$count.' </div>
<script type="text/javascript">
pag2 = new Paginator(\'pag2\', '.$pages.', 20, "'. ($pagipage ) .'", "'.$href.'page=");
</script></div>';

return array($pagerr, $pagerre, "LIMIT $start,$rpp");

}

//// ���������� ��������� �������� �����
function info() {

$sf = ROOT_PATH."/cache/info_cache_stat.txt";
$fpsf = @fopen($sf,"a+");
@fputs($fpsf, get_date_time()."#".getip()."#".htmlentities($_SERVER['HTTP_USER_AGENT'])."#".htmlspecialchars_uni($_SERVER["HTTP_REFERER"])."#".htmlspecialchars_uni($_SERVER["REQUEST_URI"])."\n");
@fclose($fpsf);

}


function downloaderdata($res) {

$rows = $ids = $peerdata = array();

while ($row = mysql_fetch_assoc($res)) {
$rows[] = $row;
$id = $row["id"];
$ids[] = $id;
$peerdata[$id] = array("downloaders" => 0, "seeders" => 0, "comments" => 0);
}

if (count($ids)) {

$res = sql_query("SELECT COUNT(*) AS c, torrent, seeder FROM peers WHERE torrent IN (".implode(",", $ids).") GROUP BY torrent, seeder");
while ($row = mysql_fetch_assoc($res)) {

if ($row["seeder"] == "yes")
$key = "seeders";
else
$key = "downloaders";

$peerdata[$row["torrent"]][$key] = $row["c"];
}

$res = sql_query("SELECT COUNT(*) AS c, torrent FROM comments WHERE torrent IN (".implode(",", $ids).") GROUP BY torrent");
while ($row = mysql_fetch_assoc($res)) {
$peerdata[$row["torrent"]]["comments"] = $row["c"];
}

}

return array($rows, $peerdata);
}

function commenttable($rows, $redaktor = "comment") {

global $CURUSER, $Ava_config, $DEFAULTBASEURL, $tracker_lang;

foreach ($rows as $row)	{

if ($row["downloaded"] > 0)
$ratio = number_format($row['uploaded'] / $row['downloaded'], 2);
elseif ($row["uploaded"] > 0)
$ratio = "Inf";
else
$ratio = "---";

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";

echo "<tr><td class=\"a\" align=\"left\" colspan=\"2\" height=\"24\">";

if (!empty($row["username"])) {

if (get_user_class() > UC_UPLOADER)
echo "<img src=\"/pic/".(strtotime($row["last_access"]) > gmtime() - 300 ? "button_online.gif":"button_offline.gif")."\" alt=\"".(strtotime($row["last_access"]) > gmtime() - 600 ? $tracker_lang['online']:$tracker_lang['offline'])."\" title=\"".$online_text."\" style=\"position: relative; top: 2px;\" border=\"0\" height=\"14\" /> ";

echo "<a name=\"comm".$row["id"]."\" href=\"".$DEFAULTBASEURL."/userdetails.php?id=".$row["user"]."\" class=\"altlink_white\"><b>".get_user_class_color($row["class"], $row["username"])."</b></a> :: ";

if ($row["donor"] == "yes")
echo "<img src=\"/pic/star.gif\" alt=\"".$tracker_lang['donor']."\" /> ";
if ($row["warned"] == "yes")
echo "<img src=\"/pic/warned.gif\" alt=\"".$tracker_lang['warned']."\" /> ";
if ($row["enabled"] == "no")
echo "<img src=\"/pic/warned2.gif\" alt=\"".$tracker_lang['disabled']."\" /> ";


$title = $row["title"];
if (empty($title))
echo get_user_class_name($row["class"]);
else
echo htmlspecialchars_uni($title);


if ($row["hiderating"] == "yes")
echo " :: R: <font color=\"ff1ac6\"><b>+100%</b></font> :: ";
else
echo " :: U: ".mksize($row["uploaded"]) ." : D: ".mksize($row["downloaded"])." : R: ".$ratio." :: ";


if ($CURUSER["cansendpm"] == 'yes' && $CURUSER["id"] <> $row['user'])
echo "<a href=\"".$DEFAULTBASEURL."/message.php?action=sendmessage&receiver=".$row['user']."\"><img src=\"/pic/button_pm.gif\" border=\"0\" alt=\"".$tracker_lang['sendmessage']."\" ></a>";

} else
echo ($row["user"] == "0" ? "<font color=\"gray\">[<b>".$tracker_lang['from_system']."</b>]</font>" : $tracker_lang['anonymous']." (".$row["id"].")");


////�������
$avatar = (!empty($row["avatar"]) ? htmlspecialchars_uni($DEFAULTBASEURL."/pic/avatar/".$row["avatar"]) : "");
if (empty($avatar)) $avatar = "/pic/default_avatar.gif";
////�������

if ($CURUSER)
$row["text"] = str_replace($CURUSER["username"], "<font color=\"#".get_user_rgbcolor($CURUSER["class"], $CURUSER["username"])."\">".$CURUSER["username"]."</font>", $row["text"]);

echo "</td></tr>";

echo "<tr>";

echo "<td width=\"100%\" colspan=\"2\">";


echo "<var style=\"padding-right: 10px; clear: right; float: left;\">".($CURUSER["id"] == $row["user"] ? "<a href=\"my.php\"><img class=\"avatars\" alt=\"".$tracker_lang['click_on_avatar']."\" title=\"".$tracker_lang['click_on_avatar']."\" border=\"0\" src=\"".$avatar."\" width=\"".$Ava_config['width']."\" /></a>":"<img class=\"avatars\" src=\"".$avatar."\" width=\"".$Ava_config['width']."\" />")."

".($row["support"] == "yes" ? htmlspecialchars_uni($row["supportfor"]):"")."
</var> ";


echo format_comment($row["text"], true);

if ($row["editedby"] && ($CURUSER["id"] == $row["editedby"] || get_user_class() >= UC_MODERATOR))
echo "<div style=\"margin-top: 10px\" align=\"right\"><small>".sprintf($tracker_lang['edit_lasttime'], "<a href=\"userdetails.php?id=".$row["editedby"]."\"><b>".get_user_class_color($row["classbyname"] ,$row["editedbyname"])."</b></a>", $row["editedat"])."</small></div>\n";


if ($row["signatrue"] == "yes" && !empty($row["signature"]))
echo "<div style=\"margin-top: 10px\" align=\"right\"><small>".format_comment($row["signature"])."</small></div>";
echo "</td>";

echo "</tr>";

echo "<tr><td class=\"a\" align=\"center\" colspan=\"2\">";

echo "<div style=\"float: left; width: auto;\">[".get_elapsed_time(sql_timestamp_to_unix_timestamp($row["added"]))." ".$tracker_lang['ago']."]</div>";

echo "<div align=\"right\">";

if (get_user_class() >= UC_MODERATOR && !empty($row["ip"]))
echo "[<a href=\"".$DEFAULTBASEURL."/usersearch.php?ip=".$row["ip"]."\" >".$row["ip"]."</a>] ";
if (!empty($row["user"]) && $CURUSER["commentpos"] == 'yes')
echo "[<a href=\"".$DEFAULTBASEURL."/".$redaktor.".php?action=quote&cid=".$row["id"]."\">".$tracker_lang['quote_message']."</a>] ";
if ($row["editedby"] && get_user_class() >= UC_MODERATOR && $CURUSER["commentpos"] == 'yes')
echo "[<a href=\"".$DEFAULTBASEURL."/".$redaktor.".php?action=vieworiginal&cid=".$row["id"]."\">".$tracker_lang['original_post']."</a>] ";
if ($row["user"] == $CURUSER["id"] || get_user_class() >= UC_MODERATOR && $CURUSER["commentpos"] == 'yes')
echo "[<a href=\"".$DEFAULTBASEURL."/".$redaktor.".php?action=edit&cid=".$row["id"]."\">".$tracker_lang['edit']."</a>] ";
if (get_user_class() >= UC_MODERATOR && $CURUSER["commentpos"] == 'yes')
echo "[<a href=\"".$DEFAULTBASEURL."/".$redaktor.".php?action=delete&cid=".$row["id"]."\" >".$tracker_lang['delete']."</a>]";

echo "</div>";

echo "</td></tr>";

echo "</table><br />";

}

}

function utf8_to_win($str,$to = "w") {

$outstr="";
$recode=array();
$recode[k]=array(0x2500,0x2502,0x250c,0x2510,0x2514,0x2518,0x251c,0x2524,0x252c,0x2534,0x253c,0x2580,0x2584,0x2588,0x258c,0x2590,0x2591,0x2592,0x2593,0x2320,0x25a0,0x2219,0x221a,0x2248,0x2264,0x2265,0x00a0,0x2321,0x00b0,0x00b2,0x00b7,0x00f7,0x2550,0x2551,0x2552,0x0451,0x2553,0x2554,0x2555,0x2556,0x2557,0x2558,0x2559,0x255a,0x255b,0x255c,0x255d,0x255e,0x255f,0x2560,0x2561,0x0401,0x2562,0x2563,0x2564,0x2565,0x2566,0x2567,0x2568,0x2569,0x256a,0x256b,0x256c,0x00a9,0x044e,0x0430,0x0431,0x0446,0x0434,0x0435,0x0444,0x0433,0x0445,0x0438,0x0439,0x043a,0x043b,0x043c,0x043d,0x043e,0x043f,0x044f,0x0440,0x0441,0x0442,0x0443,0x0436,0x0432,0x044c,0x044b,0x0437,0x0448,0x044d,0x0449,0x0447,0x044a,0x042e,0x0410,0x0411,0x0426,0x0414,0x0415,0x0424,0x0413,0x0425,0x0418,0x0419,0x041a,0x041b,0x041c,0x041d,0x041e,0x041f,0x042f,0x0420,0x0421,0x0422,0x0423,0x0416,0x0412,0x042c,0x042b,0x0417,0x0428,0x042d,0x0429,0x0427,0x042a);

$recode[w]=array(0x0402,0x0403,0x201A,0x0453,0x201E,0x2026,0x2020,0x2021,0x20AC,0x2030,0x0409,0x2039,0x040A,0x040C,0x040B,0x040F,0x0452,0x2018,0x2019,0x201C,0x201D,0x2022,0x2013,0x2014,0x0000,0x2122,0x0459,0x203A,0x045A,0x045C,0x045B,0x045F,0x00A0,0x040E,0x045E,0x0408,0x00A4,0x0490,0x00A6,0x00A7,0x0401,0x00A9,0x0404,0x00AB,0x00AC,0x00AD,0x00AE,0x0407,0x00B0,0x00B1,0x0406,0x0456,0x0491,0x00B5,0x00B6,0x00B7,0x0451,0x2116,0x0454,0x00BB,0x0458,0x0405,0x0455,0x0457,0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,0x041F,0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042A,0x042B,0x042C,0x042D,0x042E,0x042F,0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,0x043F,0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044A,0x044B,0x044C,0x044D,0x044E,0x044F);

$recode[i]=array(0x0080,0x0081,0x0082,0x0083,0x0084,0x0085,0x0086,0x0087,0x0088,0x0089,0x008A,0x008B,0x008C,0x008D,0x008E,0x008F,0x0090,0x0091,0x0092,0x0093,0x0094,0x0095,0x0096,0x0097,0x0098,0x0099,0x009A,0x009B,0x009C,0x009D,0x009E,0x009F,0x00A0,0x0401,0x0402,0x0403,0x0404,0x0405,0x0406,0x0407,0x0408,0x0409,0x040A,0x040B,0x040C,0x00AD,0x040E,0x040F,0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,0x041F,0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042A,0x042B,0x042C,0x042D,0x042E,0x042F,0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,0x043F,0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044A,0x044B,0x044C,0x044D,0x044E,0x044F,0x2116,0x0451,0x0452,0x0453,0x0454,0x0455,0x0456,0x0457,0x0458,0x0459,0x045A,0x045B,0x045C,0x00A7,0x045E,0x045F);

$recode[a]=array(0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041a,0x041b,0x041c,0x041d,0x041e,0x041f,0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042a,0x042b,0x042c,0x042d,0x042e,0x042f,0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043a,0x043b,0x043c,0x043d,0x043e,0x043f,0x2591,0x2592,0x2593,0x2502,0x2524,0x2561,0x2562,0x2556,0x2555,0x2563,0x2551,0x2557,0x255d,0x255c,0x255b,0x2510,0x2514,0x2534,0x252c,0x251c,0x2500,0x253c,0x255e,0x255f,0x255a,0x2554,0x2569,0x2566,0x2560,0x2550,0x256c,0x2567,0x2568,0x2564,0x2565,0x2559,0x2558,0x2552,0x2553,0x256b,0x256a,0x2518,0x250c,0x2588,0x2584,0x258c,0x2590,0x2580,0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044a,0x044b,0x044c,0x044d,0x044e,0x044f,0x0401,0x0451,0x0404,0x0454,0x0407,0x0457,0x040e,0x045e,0x00b0,0x2219,0x00b7,0x221a,0x2116,0x00a4,0x25a0,0x00a0);

$recode[d]=$recode[a];

$recode[m]=array(0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,0x041F,0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042A,0x042B,0x042C,0x042D,0x042E,0x042F,0x2020,0x00B0,0x00A2,0x00A3,0x00A7,0x2022,0x00B6,0x0406,0x00AE,0x00A9,0x2122,0x0402,0x0452,0x2260,0x0403,0x0453,0x221E,0x00B1,0x2264,0x2265,0x0456,0x00B5,0x2202,0x0408,0x0404,0x0454,0x0407,0x0457,0x0409,0x0459,0x040A,0x045A,0x0458,0x0405,0x00AC,0x221A,0x0192,0x2248,0x2206,0x00AB,0x00BB,0x2026,0x00A0,0x040B,0x045B,0x040C,0x045C,0x0455,0x2013,0x2014,0x201C,0x201D,0x2018,0x2019,0x00F7,0x201E,0x040E,0x045E,0x040F,0x045F,0x2116,0x0401,0x0451,0x044F,0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,0x043F,0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044A,0x044B,0x044C,0x044D,0x044E,0x00A4);

$and=0x3F;
for ($i=0;$i<strlen($str);$i++) {
$letter=0x0;
$octet=array();
$octet[0]=ord($str[$i]);
$octets=1;
$andfirst=0x7F;

if (($octet[0]>>1)==0x7E) {
$octets=6;
$andfirst=0x1;
} elseif (($octet[0]>>2)==0x3E) {
$octets=5;
$andfirst=0x3;
} elseif (($octet[0]>>3)==0x1E) {
$octets=4;
$andfirst=0x7;
} elseif (($octet[0]>>4)==0xE) {
$octets=3;
$andfirst=0xF;
} elseif (($octet[0]>>5)==0x6) {
$octets=2;
$andfirst=0x1F;
}

$octet[0]=$octet[0] & $andfirst;
$octet[0]=$octet[0] << ($octets-1)*6;
$letter+=$octet[0];

for ($j=1;$j<$octets;$j++) {
$i++;
$octet[$j]=ord($str[$i]) & $and;
$octet[$j]=$octet[$j] << ($octets-1-$j)*6;
$letter+=$octet[$j];
}

if ($letter<0x80) {
$outstr.=chr($letter);
} else {
if (in_array($letter,$recode[$to])) {
$outstr.=chr(array_search($letter,$recode[$to])+128);
}
}
}

return($outstr);
}



function searchfield($s) {
return preg_replace(array('/[^a-z0-9]/si', '/^\s*/s', '/\s*$/s', '/\s+/s'), array(" ", "", "", " "), $s);
}

function genrelist() {
$ret = array();

$res = sql_query("SELECT id, name,(SELECT COUNT(*) FROM torrents WHERE categories.id=torrents.category) AS num_torrent FROM categories ORDER BY sort ASC", $cache = array("type" => "disk", "file" => "browse_genrelist", "time" => 86400));
while ($row = mysql_fetch_assoc_($res))
$ret[] = $row;
return $ret;
}

function taggenrelist($cat) {

$ret = array();
$res = sql_query("SELECT id, name FROM tags WHERE category = ".sqlesc($cat)." ORDER BY name ASC", $cache = array("type" => "disk", "file" => "taggenrelist_".$cat, "time" => 60*60*24)) or sqlerr(__FILE__, __LINE__);
while ($row = mysql_fetch_assoc_($res))
$ret[] = $row;

return $ret;
}

function tag_info() {

///////// cache
$result = sql_query("SELECT DISTINCT name, howmuch FROM tags WHERE howmuch > 0 and name IS NOT NULL ORDER BY howmuch DESC LIMIT 80", $cache = array("type" => "disk", "file" => "block_cloudstags", "time" => 60*60)) or sqlerr(__FILE__, __LINE__);
///////// cache

while($row = mysql_fetch_assoc_($result))
$arr[$row['name']] = $row['howmuch'];

@ksort($arr);
return $arr;
}

function cloud() {

$small = 10;
$big = 32;
$tags = tag_info();

$minimum_count = @min(array_values($tags));
$maximum_count = @max(array_values($tags));

$spread = $maximum_count - $minimum_count;

if ($spread == 0) $spread = 1;

$cloud_html = '';
$cloud_tags = array();

$colour_array = array('#003EFF', '#0000FF', '#7EB6FF', '#0099CC', '#62B1F6');

foreach ($tags as $tag => $count) {
$cloud_tags[] = '<a style="font-weight:normal; color:'.$colour_array[mt_rand(0, 5)].'; font-size:'.floor($small+($count-$minimum_count)*($big-$small)/$spread). 'px" class="tag_cloud" href="browse.php?tag='.urlencode($tag).'" title="'.$tracker_lang['tracker_torrents'].': '.$count.'">'.htmlspecialchars($tag).'</a> ('.$count.')';
}

return implode("\n", $cloud_tags)."\n";

}

function linkcolor ($num, $alt = false) {

if (empty($num) && $alt <> false) return "gray";
elseif (empty($num) && $alt == false) return "red";

if ($alt == false) return "green";
else return "blue";
}


function checknewnorrent($id, $added)  {

global $CURUSER;

$coo_new = (isset($_COOKIE["markview"])? $_COOKIE["markview"]:"");

$diff_add = preg_replace("/[^0-9]/", "", $added);
$diff_lch = preg_replace("/[^0-9]/", "", $CURUSER["last_checked"]);

if ($diff_add > $diff_lch){

if (!empty($coo_new)){

if (explode("-", $coo_new)){

if (!in_array($id, explode("-", $coo_new)))
$new_t = true;

} else {
if($coo_new != $id)
$new_t = true;
}
} else
$new_t = true;

} else

$new_t = false;

return $new_t;
}


function pic_rating_b($all = 10, $first = false) {

$end = $all-$first;

$prating = "";

for ($x1 = 0; $x1 < $all; $x1++){
if ($x1 < $first && $all <= 10)
$prating.="<img src=\"pic/ratio/star_on.gif\" width=\"20\" height=\"20\" border=\"0\"/>";
elseif ($x1 > $first && $all <= 10)
$prating.="<img src=\"pic/ratio/star_off.gif\" width=\"20\" height=\"20\" border=\"0\"/>";
else
$prating.="<img src=\"pic/ratio/star_off.gif\" width=\"20\" height=\"20\" border=\"0\"/>";
}

return $prating;
}


function torrenttable($res, $variant = "index") {

global $DEFAULTBASEURL, $CURUSER, $Torrent_Config, $SITENAME, $tracker_lang, $announce_net, $announce_urls;

$announce_urls[0] = $announce_urls[0].(!empty($CURUSER['passkey']) ? "?passkey=".$CURUSER['passkey']:"");

$mark = 0;
$cat = array();
$file = basename($_SERVER['SCRIPT_FILENAME']);

$res_cat = sql_query("SELECT id, name, image FROM categories", $cache = array("type" => "disk", "file" => "browse_cat_array", "time" => 24*7200)) or sqlerr(__FILE__, __LINE__);

while ($arr_cat = mysql_fetch_assoc_($res_cat)){
$cat_sql[$arr_cat["id"]] = array("name" => $arr_cat["name"], "image" => $arr_cat["image"]);
}


if ($CURUSER){

$checkin = $book = array();

$res_t1 = sql_query("SELECT checkid FROM checkcomm WHERE userid = ".sqlesc($CURUSER["id"])." AND torrent = '1' AND offer = '0'") or sqlerr(__FILE__, __LINE__);
while ($arr_c1 = mysql_fetch_assoc($res_t1)){
$checkin[$arr_c1["checkid"]] = 1;
}

$res_t2 = sql_query("SELECT torrentid FROM bookmarks WHERE userid = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);
while ($arr_c2 = mysql_fetch_assoc($res_t2)){
$book[$arr_c2["torrentid"]] = 1;
}

}

?>
<script type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript">
function getDetails(tid, bu, picid, formid) {
var det = document.getElementById('details_'+tid);
var pic = document.getElementById(picid);
var form = document.getElementById(formid);
if (!det.innerHTML) {
var ajax = new tbdev_ajax();
ajax.onShow ('');
var varsString = "";
ajax.requestFile = "<?=$DEFAULTBASEURL; ?>/gettorrentdetails.php";
ajax.setVar("tid", tid);
ajax.method = 'POST';
ajax.element = 'details_'+tid;
ajax.sendAJAX(varsString);
pic.src = bu + "/pic/minus.gif"; form.value = "minus";
} else
det.innerHTML = '';
pic.src = bu + "/pic/plus.gif"; form.value = "plus";
}
</script>
<? if ($CURUSER){ ?>
<script type="text/javascript">
function bookmark(id, type, page) {
var loading = ""; var id = id; var type = type;
jQuery("#loading").html(loading);
jQuery.post('<?=$DEFAULTBASEURL; ?>/bookmark.php',{'id':id , 'type':type , 'page':page},

function(response) {
jQuery('#bookmark_'+id).html(response);
jQuery("#loading").empty();
}, 'html');
}

function checmark(id, type, page,twopage) {
var loading = ""; var id = id; var type = type;
jQuery("#loading").html(loading);
jQuery.post('<?=$DEFAULTBASEURL; ?>/bookmark.php',{'id':id , 'type':type , 'page':page, 'twopage':twopage},

function(response) {
jQuery('#checmark_'+id).html(response);
jQuery("#loading").empty();
}, 'html');
}
</script>
<? } ?>
<? if (get_user_class() == UC_SYSOP && $variant == "index") {

?>
<script language="Javascript" type="text/javascript">
<!-- Begin
var checkflag = "false";
var marked_row = new Array;
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
}
else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
}
}

</script>
<? } ?>

<div id="loading-layer" style="display:none;font-family: Verdana;font-size: 11px;width:200px;height:50px;background:#FFF;padding:10px;text-align:center;border:1px solid #000"><div style="font-weight:bold" id="loading-layer-text"><?=$tracker_lang['loading'];?></div><br /><img src="/pic/loading.gif" border="0" /></div>
<?

echo "<tr>";

$count_get = 0;
foreach ($_GET as $get_name => $get_value) {

$get_name = mysql_escape_string(strip_tags(str_replace(array("\"","'"), "", $get_name)));
$get_value = mysql_escape_string(strip_tags(str_replace(array("\"","'"), "", $get_value)));

if ($get_name <> "sort" && $get_name <> "type") {

if (!empty($count_get))
$oldlink = $oldlink."&".$get_name."=".$get_value;
else
$oldlink = (isset($oldlink)?$oldlink:"").$get_name."=".$get_value;

++$count_get;
}

}

if (!empty($count_get))
$oldlink = $oldlink."&";

$type = (isset($_GET['type']) ? $_GET['type']:"");
$sort = (isset($_GET['sort']) ? $_GET['sort']:"");

$link1 = "asc";
$link2 = $link12 = $link3 = $link4 = $link5 = $link6 = $link7 = $link8 = $link9 = $link10 = $link11 = "desc";


if ($sort == "1"){
if ($type == "desc") $link1 = "asc";
else $link1 = "desc";
} elseif ($sort == "2") {
if ($type == "desc") $link2 = "asc";
else $link2 = "desc";
} elseif ($sort == "3") {
if ($type == "desc") $link3 = "asc";
else $link3 = "desc";
} elseif ($sort == "4") {
if ($type == "desc") $link4 = "asc";
else $link4 = "desc";
}elseif ($sort == "5") {
if ($type == "desc") $link5 = "asc";
else $link5 = "desc";
} elseif ($sort == "6") {
if ($type == "desc") $link6 = "asc";
else $link6 = "desc";
} elseif ($sort == "7") {
if ($type == "desc") $link7 = "asc";
else $link7 = "desc";
} elseif ($sort == "8") {
if ($type == "desc") $link8 = "asc";
else $link8 = "desc";
} elseif ($sort == "9") {
if ($type == "desc") $link9 = "asc";
else $link9 = "desc";
} elseif ($sort == "10") {
if ($type == "desc") $link10 = "asc";
else $link10 = "desc";
} elseif ($sort == "11") {
if ($type == "desc") $link11 = "asc";
else $link11 = "desc";
} elseif ($sort == "12") {
if ($type == "desc") $link12 = "asc";
else $link12 = "desc";
}


echo "<td class=\"colhead\" align=\"center\">
<a title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_cat"]."\" rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=6&type=".$link6."\" class=\"altlink_white\">".$tracker_lang["type"]."</a></td>";

echo "<td class=\"colhead\" align=\"left\">
<a title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_name"]."\" rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=1&type=".$link1."\" class=\"altlink_white\">".$tracker_lang["name"]."</a>
/
<a title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_added"]."\" rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=11&type=".$link11."\" class=\"altlink_white\">".$tracker_lang["moderated"]."</a>
/
<a title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_multitime"]."\" rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=12&type=".$link12."\" class=\"altlink_white\">".$tracker_lang['multitracker']."</a>
</td>";

if (isset($wait))
echo ("<td class=\"colhead\" align=\"center\">".$tracker_lang['wait']."</td>\n");

if ($variant == "mytorrents")
echo ("<td class=\"colhead\" align=\"center\">".$tracker_lang['visible']."</td>\n");

echo "<td class=\"colhead\" align=\"center\"><a rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=2&type=".$link2."\" class=\"altlink_white\"><img title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_numfile"]."\" src=\"/pic/browse/nimberfiles.gif\" border=\"0\"></a></td>";

echo "<td class=\"colhead\" align=\"center\"><a rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=3&type=".$link3."\" class=\"altlink_white\"><img title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_numcomm"]."\" src=\"/pic/browse/comments.gif\" border=\"0\"></a></td>";

if ($Torrent_Config["use_ttl"] == true)
echo "<td class=\"colhead\" align=\"center\">".$tracker_lang['ttl']."</td>";

echo "<td class=\"colhead\" align=\"center\"><a rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=5&type=".$link5."\">
<img title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_size"]."\" src=\"/pic/browse/size_file.gif\" border=\"0\"></a></td>";

echo "<td class=\"colhead\" align=\"center\"><a rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=7&type=".$link7."\"><img title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_peers"]."\" src=\"/pic/browse/up.gif\" border=\"0\"></a> <a rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=8&type=".$link8."\"><img title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_leech"]."\" src=\"/pic/browse/down.gif\" border=\"0\"></a>
</td>";

if ($variant == "index" || $variant == "bookmarks")
echo("<td class=\"colhead\" align=\"center\"><a rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=9&type=".(isset($link9)?$link9:"")."\" class=\"altlink_white\">".$tracker_lang['uploadeder']."</a></td>\n");

if ($variant == "index")
echo("<td class=\"colhead\" align=\"center\"><a title=\"".$tracker_lang["sorting"].": ".$tracker_lang["sort_added"]."\" rel=\"nofollow\" href=\"".$file."?".(isset($oldlink)?$oldlink:"")."sort=4&type=".$link4."\" class=\"altlink_white\">".$tracker_lang['added']."</a>
</td>");

if (get_user_class() == UC_SYSOP && $variant == "index")
echo("<td class=\"colhead\" align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['mark_all']."\" onclick=\"this.value=check(document.yepi.elements);\"></td>\n");

if ($variant == "bookmarks")
echo("<td class=\"colhead\" align=\"center\">".$tracker_lang['delete']."</td>\n");

echo("</tr>\n");


if (get_user_class() == UC_SYSOP && $variant == "index")
echo("<form method=\"post\" name=\"yepi\" action=\"b_actions.php\">");

if ($variant == "bookmarks")
echo("<form method=\"post\" action=\"takedelbookmark.php\">");

$numcat = $numid = array();

while ($row = mysql_fetch_assoc($res)) {

$id = $row["id"];
$numid[]=$row["id"];
$numcat[]=$row["category"];
$sizeall+=$row["size"];

$sizegoldtor += ($row["free"]=="yes" ? $row["size"]:0);
$numpeers += ($row["seeders"]+$row["leechers"]);

echo "<tr ".((isset($row["sticky"]) && $row["sticky"] == "yes") ? "class=\"highlight\"":"").">";

$thisdate = date('Y-m-d', strtotime($row['added']));

if ($thisdate <> $prevdate){

$days_view = date('-l-, j :M: Y', strtotime($row['added'])); // You can change this to something else

preg_match("/\-(.*?)\-/i", $days_view, $match);
if (count($match) == 2)
$days_view = preg_replace("/\-".$match[1]."\-/is", $tracker_lang[$match[1]], $days_view);

preg_match("/\:(.*?)\:/i", $days_view, $matc_h);
if (count($matc_h) == 2)
$days_view = preg_replace("/\:".$matc_h[1]."\:/is", $tracker_lang[$matc_h[1]], $days_view);

if (isset($row["sticky"]) && $row["sticky"] == "no" && !isset($_GET['date']))
echo "<tr><td colspan=\"15\" class=\"colhead\"><b>".$days_view."</b></td></tr>\n";

}

$prevdate = $thisdate;
/// ��������� ������� �� ����

echo "<tr>";
echo "<td align=\"center\" ".($sort == "6" ? "class=\"a\"":"class=\"b\"")." rowspan=\"2\" width=\"2%\" style=\"margin: 0px; padding: 0px;\">";

$cat_id = $row["category"];

if (!empty($cat_sql[$cat_id]["name"])) {

echo "<a rel=\"nofollow\" href=\"browse.php?cat=".$row["category"].(!empty($_GET["search"])? "&search=".htmlspecialchars($_GET["search"]):"")."\">";

if (isset($cat_sql[$cat_id]["image"]) && !empty($cat_sql[$cat_id]["image"])){

if ($sort == "6")
echo "<img border=\"0\" src=\"".$DEFAULTBASEURL."/pic/cats/".$cat_sql[$cat_id]["image"]."\" alt=\"".$cat_sql[$cat_id]["name"]."\" />";
else
echo "<img border=\"0\" src=\"".$DEFAULTBASEURL."/pic/cats/".$cat_sql[$cat_id]["image"]."\" alt=\"".$cat_sql[$cat_id]["name"]."\" />";

} else
echo $cat_sql[$cat_id]["name"];

echo "</a>";
} else
echo "-";

echo "</td>";

$dispname = htmlspecialchars_uni($row['name']);

echo "<td colspan=\"12\" class=\"b\" align=\"left\"><div style=\"cursor:pointer;\" align=\"left\" onDblClick=\"getDetails('".$id."','".$DEFAULTBASEURL."','warnpic".$id."','".$id."')\" alt=\"".$tracker_lang['preview']."\" title=\"".$tracker_lang['preview']."\">".((isset($row["sticky"]) && $row["sticky"] == "yes") ? "<b>".$tracker_lang['sticky']."</b>: " : "")." <a style=\"font-weight:bolder;\" href=\"details.php?id=".$id."\"><strong>".$dispname."</strong></a> ";

if (checknewnorrent($row["id"], $row["added"]) && $CURUSER) {
echo "<b><font color=\"red\" size=\"1\">".$tracker_lang['new_torrent']."</font></b> ";
++$mark;
}

if ($row["free"] == "yes")
echo "<img src=\"/pic/freedownload.gif\" title=\"".$tracker_lang['golden']."\" alt=\"".$tracker_lang['golden']."\" /> ";

if (isset($row["viponly"]) && $row["viponly"] <> "0000-00-00 00:00:00")
echo "<img border=\"0\" width=\"18px\" alt=\"".$tracker_lang['vips_only']."\" title=\"".$tracker_lang['vips_only']."\" src=\"/pic/vipbig.gif\" /> ";

echo "</div></td></tr><tr>";

echo "<td width=\"45%\" ".(in_array($sort, array("4","1","11","12")) ? "class=\"b\"":"class=\"a\"")." align=\"left\">";

echo "<table cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";

echo "<tr><td colspan=\"2\" ".(in_array($sort, array("4","1","11","12")) ? "class=\"b\"":"class=\"a\"").">";

if ($variant == "bookmarks" && $CURUSER)
echo "<b>".$tracker_lang['notes']."</b>: ".(!empty($row["mytags"]) ? htmlspecialchars_uni(strip_tags($row["mytags"])):$tracker_lang['no'])."<br />";

$tags_array = array();

if (!empty($row["tags"])) {
foreach(explode(",", $row["tags"]) as $tag) {
$tag = trim($tag);
$numtags[] = $tag;

$tags_array[] = "<a rel=\"nofollow\" href=\"browse.php?tag=".urlencode($tag)."\"><strong>".tolower($tag)."</strong></a>";
}
}

echo "<b>".$tracker_lang['tags']."</b>: ".(count($tags_array) ? implode(", ", $tags_array):"[".$tracker_lang['no_choose']."]");

if (isset($row["banned"]) && $row["banned"] == "yes"){
$banned_view = (isset($row["banned_reason"])? htmlspecialchars($row["banned_reason"]):"");
echo "<br />";
echo get_user_class() < UC_MODERATOR ? $tracker_lang['banned']: "<b>".$tracker_lang['reason']."</b>: ".(empty($banned_view) ? $tracker_lang['no']:$banned_view);
}

echo "</td>";

echo "<td align=\"center\" width=\"15%\">";

if ($variant <> "bookmarks" && $CURUSER){

if (empty($checkin[$id]))
echo "<span style=\"cursor: pointer;\" id=\"checmark_".$row['id']."\"><a onclick=\"checmark('".$id."', 'add' , 'check', 'browse');\"><img border=\"0\" src=\"/pic/head2_2.gif\" alt=\"".$tracker_lang['monitor_comments']."\" title=\"".$tracker_lang['monitor_comments']."\" /></a> <span id=\"loading\"></span></span> ";
else
echo "<span style=\"cursor: pointer;\" id=\"checmark_".$row['id']."\"><a onclick=\"checmark('".$id."', 'del' , 'check', 'browse');\"><img class=\"effect\" onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" border=\"0\" src=\"/pic/head2_2.gif\" alt=\"".$tracker_lang['monitor_comments_disable']."\" title=\"".$tracker_lang['monitor_comments_disable']."\" /></a> <span id=\"loading\"></span></span> ";


if(empty($book[$id]))
echo " <span style=\"cursor: pointer;\" id=\"bookmark_".$row['id']."\"><a onclick=\"bookmark('".$id."', 'add' , 'browse');\"><img border=\"0\" src=\"/pic/add.gif\" alt=\"".$tracker_lang['bookmark_this']."\" title=\"".$tracker_lang['bookmark_this']."\" /></a> <span id=\"loading\"></span></span> ";
else
echo " <span style=\"cursor: pointer;\" id=\"bookmark_".$row['id']."\"><a onclick=\"bookmark('".$id."', 'del' , 'browse');\"><img class=effect onmouseover=\"this.className='effect1'\" onmouseout=\"this.className='effect'\" border=\"0\" src=\"/pic/delete.gif\" alt=\"".$tracker_lang['bookmark_off']."\" title=\"".$tracker_lang['bookmark_off']."\" /></a> <span id=\"loading\"></span></span> ";

}

if ((isset($row["owner"]) && $CURUSER["id"] == $row["owner"]) || get_user_class() >= UC_MODERATOR)
$owned = 1;
else
$owned = 0;

if ($owned)
echo("<a href=\"edit.php?id=".$row["id"]."\"><img border=\"0\" src=\"".$DEFAULTBASEURL."/pic/pen.gif\" alt=\"".$tracker_lang['edit']."\" title=\"".$tracker_lang['edit']."\"/></a>\n");



echo "<br />";

/// � ������ ���� ������ ������ ������ - �������� �������� �������
if ($CURUSER || (!empty($announce_net) && $row["multitracker"] == "yes" && !$CURUSER)){

if ($row["magnetonly"] <> 'yes')
echo "<a rel=\"nofollow\" href=\"download.php?id=".$row["id"]."\" alt=\"".$tracker_lang['download']." ".$dispname."\" title=\"".$tracker_lang['download']." ".$dispname."\"><strong>".$tracker_lang['download']."</strong></a>";
else 
echo "<noindex><a rel=\"nofollow\" title=\"".$tracker_lang['magnet_link']."\" href=\"magnet:?xt=urn:btih:".$row['info_hash']."&amp;dn=".htmlspecialchars($row['name'])."&amp;tr=".implode("&amp;tr=", $announce_urls)."\"><strong>".$tracker_lang['magnet_link']."</strong></a></noindex>";

} else {

if ($row["magnetonly"] <> 'yes')
echo "<a rel=\"nofollow\" href=\"download.php?id=".$row["id"]."\" alt=\"".$tracker_lang['download']." ".$dispname."\" title=\"".$tracker_lang['download']." ".$dispname."\"><s><b>".$tracker_lang['download']."</b></s></a>";
else
echo "<s><b>".$tracker_lang['magnet_link']."</b></s>";

}
/// � ������ ���� ������ ������ ������ - �������� �������� �������

echo "</td></tr>";

echo "</table>";

echo ("</td>\n");

if ($variant == "mytorrents") {

echo ("<td class=\"row2\" align=\"center\">");
if ($row["visible"] == "no")
echo ("<font color=\"red\"><b>".$tracker_lang['no']."</b></font>");
else
echo ("<font color=\"green\">".$tracker_lang['yes']."</font>");

echo ("</td>\n");
}

echo("<td ".($sort=="2"? "class=\"b\"":"class=\"row2\"")." align=\"center\"><b>".$row["numfiles"]."</b></td>\n");
echo("<td ".($sort=="3"? "class=\"b\"":"class=\"row2\"")." align=\"center\"><b><a rel=\"nofollow\" href=\"details.php?id=".$row["id"]."&page=last#startcomments\">".$row["comments"]."</a></b></td>");

$ttl = ($Torrent_Config["ttl_days"]*24) - floor((gmtime() - sql_timestamp_to_unix_timestamp($row["added"])) / 3600);
if ($ttl == 1)
$ttl .= "&nbsp;".$tracker_lang['hour'];
else
$ttl .= "&nbsp;".$tracker_lang['hours'];

if ($Torrent_Config["use_ttl"] == true)
echo ("<td class=\"row2\" align=\"center\">".$ttl."</td>\n");

echo ("<td ".($sort=="5"? "class=\"b\"":"class=\"row2\"")." align=\"center\">".mksize($row["size"])."</td>\n");

echo ("<td ".($sort=="7" || $sort=="8"? "class=\"b\"":"class=\"row2\"")." align=\"center\">");
echo ("<b><font color=\"".linkcolor($row["seeders"])."\">".($row["seeders"])."</font></b>\n");
echo (" | ");
echo ("<b><font color=\"".linkcolor(number_format($row["leechers"])). "\">" .number_format($row["leechers"]). "</font></b>\n");
echo ("</td>");

$numseed+=$row["seeders"];
$numleech+=$row["leechers"];

if ($variant == "index" || $variant == "bookmarks")
echo ("<td ".($sort=="9"? "class=\"b\"":"class=\"row2\"")." align=\"center\">".(!empty($row["username"]) ? "<a rel=\"nofollow\" href=\"userdetails.php?id=".$row["owner"]."\"><strong>".get_user_class_color($row["class"], $row["username"])."</strong></a>" : "<i>".$tracker_lang['anonymous']."</i>")."</td>\n");

if ($variant == "bookmarks")
echo ("<td class=\"row2\" align=\"center\"><input class=\"desacto\" type=\"checkbox\" name=\"delbookmark[]\" value=\"".$row["bookmarkid"]."\" /></td>");

$date = current(explode(" ",$row["added"]));
if ($variant == "index")
echo "<td ".($sort=="4"? "class=\"b\"":"class=\"row2\"")." align=\"center\">".get_elapsed_time(sql_timestamp_to_unix_timestamp($row["added"]),false,true)." ".$tracker_lang['ago']." <br /><a rel=\"nofollow\" title=\"".sprintf($tracker_lang['search_for_day'],$date)."\" href=\"browse.php?date=".$date."\">".$date."</a></td>\n";

if (get_user_class() == UC_SYSOP && $variant == "index")
echo ("<td class=\"row2\" align=\"center\"><input type=\"checkbox\" class=\"desacto\" name=\"cheKer[]\" value=\"".$id."\" /></td>\n");

echo ("</tr><tr><td colspan=\"10\" class=\"a\" style=\"margin: 0px; padding: 0px;\"><span id=\"details_".$id."\"></span></td>");

echo ("</tr>");

$oldday = isset($day); // ������ ����
}

if ($CURUSER && isset($_GET["test_demo"])) {

$numcat = @array_unique($numcat);
$numtags = @array_unique($numtags);

if (count($numid)){

$ncurnum = 0;
$sql = sql_query("SELECT uploaded, downloaded FROM peers WHERE userid = ".sqlesc($CURUSER["id"])." AND torrent IN (".implode(", ", $numid).")") or sqlerr(__FILE__,__LINE__);

while ($ressql = mysql_fetch_assoc($sql)){
$ncurup+=$ressql["uploaded"];
$ncurdo+=$ressql["downloaded"];
++$ncurnum;
}

$snurnum = 0;
$sql2 = sql_query("SELECT uploaded, downloaded FROM snatched WHERE userid = ".sqlesc($CURUSER["id"])." AND torrent IN (".implode(", ", $numid).")") or sqlerr(__FILE__,__LINE__);

while ($ressql2 = mysql_fetch_assoc($sql2)){
$sncurup+=$ressql2["uploaded"];
$snurdo+=$ressql2["downloaded"];
++$snurnum;
}

}


echo ("<tr><td class=\"a\" colspan=\"12\" align=\"center\">
<table width=\"100%\">
<tr><td class=\"b\" width=\"50%\" colspan=\"2\" align=\"center\">".$tracker_lang['view_stat_my']."</td></tr>

<tr>
<td class=\"b\" width=\"50%\" align=\"center\" valign=\"top\">
<b>".$tracker_lang['seed_line']."</b> (".$tracker_lang['details_seeded']." / ".$tracker_lang['details_leeched']."): ".number_format($ncurnum)." (".mksize($ncurup)." / ".mksize($ncurdo).")<br />
<b>".$tracker_lang['tabs_completed']."</b> (".$tracker_lang['details_seeded']." / ".$tracker_lang['details_leeched']."): ".number_format($snurnum)." (".mksize($sncurup)." / ".mksize($snurdo).")<br />
</td>

<td class=\"b\" width=\"50%\" align=\"center\" valign=\"top\">
<b>".$tracker_lang['size']."</b> (".$tracker_lang['torrent_privat']." / ".$tracker_lang['torrent_golden']."): ".mksize($sizeall)." (".mksize($sizeall-$sizegoldtor)." / ".mksize($sizegoldtor).")<br />
<b>".$tracker_lang['tracker_peers']."</b> (".$tracker_lang['seed_and_leech']."): ".($numseed+$numleech)." (".$numseed." / ".$numleech.")<br />
<b>".$tracker_lang['tags']."</b>: ".count($numtags)."<br />
<b>".$tracker_lang['active_category']."</b>: ".count($numcat)."<br />
</td>

</tr>
</table>
</td></tr>");
}

if ($variant == "index" && $CURUSER && !empty($mark))
echo ("<tr><td class=\"a\" colspan=\"12\" align=\"center\"><a href=\"browse.php?markread\"><b>".$tracker_lang['mark_read_tor']."</b></a></td></tr>");

if ($variant == "index" && get_user_class() == UC_SYSOP) {

echo "<tr><td align=\"right\" colspan=\"12\">";

echo "<select name=\"actions\" id=\"myoptn\">\n
<option selected>".$tracker_lang['signup_not_selected']."</option>
<option value=\"gold\">".$tracker_lang['act_gold']."</option>
<option value=\"ungold\">".$tracker_lang['act_ungold']."</option>
<option value=\"check\">".$tracker_lang['act_check']."</option>
<option value=\"uncheck\">".$tracker_lang['act_uncheck']."</option>
<option value=\"main\">".$tracker_lang['act_main']."</option>
<option value=\"unmain\">".$tracker_lang['act_unmain']."</option>
<option value=\"newdate\">".$tracker_lang['act_newdate']."</option>
<option value=\"anonim\">".$tracker_lang['act_anonim']."</option>
<option value=\"pravo\">".$tracker_lang['act_pravo']."</option>
<option value=\"movcat\">".$tracker_lang['act_movcat']." ----></option>
<option value=\"tag_new\">".$tracker_lang['act_tag_new']."</option>
<option value=\"untags\">".$tracker_lang['act_untags']."</option>
<option value=\"multi\">".$tracker_lang['act_multi']."</option>
<option value=\"match\">".$tracker_lang['act_match']."</option>
<option value=\"unmatch\">".$tracker_lang['act_unmatch']."</option>
<option value=\"delete\">".$tracker_lang['delete_user']."</option>
</select>\n";

echo "<select name=\"pcat\" id=\"nmovcat\" style=\"display: none;\">\n";
echo "<option selected>".$tracker_lang['no_choose']."</option>";
foreach (genrelist() as $row)
echo "<option value=\"".$row["id"]."\">".htmlspecialchars($row["name"])."</option>";
echo "</select>\n";

echo "<input type=\"hidden\" name=\"referer\" value=\"".ADDREFLINK."\" /> <input type=\"submit\" class=\"btn\" onClick=\"return confirm('".$tracker_lang['warn_sure_action']."')\" value=\"".$tracker_lang['b_action']."\" /></td></tr>\n";

} elseif ($variant == "bookmarks")
echo ("<tr><td colspan=\"12\" align=\"right\"><input class=\"btn\" type=\"submit\" value=\"".$tracker_lang['delete']."\" /></td></tr>\n");

if (($variant == "index" || $variant == "bookmarks") && get_user_class() == UC_SYSOP)
echo ("</form>\n");

return $rows;
}


function get_user_icons($arr, $big = false) {

$pics = '';

if ($big) {
$donorpic = "starbig.gif";
$warnedpic = "warned.gif";
$disabledpic = "disabled.gif";
$style = "style='margin-left: 4pt'";
} else {
$donorpic = "star.gif";
$warnedpic = "warned.gif";
$disabledpic = "disabled.gif";
$style = "style=\"margin-left: 2pt\"";
}

if ($arr["donor"] == "yes")
$pics .= "<img src=\"/pic/".$donorpic."\" alt=\"".$tracker_lang['donor']."\" border=\"0\" ".$style." />";
if ($arr["num_warned"] >= "1" || $arr["warned"] == "yes")
$pics .= "<img src=\"/pic/".$warnedpic."\" alt=\"".$tracker_lang['warned']."\" border=\"0\" ".$style." />";
if ($arr["enabled"] == "no")
$pics .= "<img src=\"/pic/".$disabledpic."\" alt=\"".$tracker_lang['disabled']."\" border=\"0\" ".$style." />";
if (isset($arr["shoutbox"]) && $arr["shoutbox"] <> "0000-00-00 00:00:00")
$pics .= "<img src=\"/pic/warning.gif\" alt=\"".$tracker_lang['banned_onchat'].": ".$arr["shoutbox"]."\" border=\"0\" ".$style." />";
if (isset($arr["forum_com"]) && $arr["forum_com"] <> "0000-00-00 00:00:00")
$pics .= "<img src=\"/pic/warning.gif\" alt=\"".$tracker_lang['banned_on'].": ".$arr["forum_com"]."\" border=\"0\" ".$style." \">";
if ($arr["parked"] == "yes")
$pics .= "<img src=\"/pic/head2_2.gif\" alt=\"".$tracker_lang['my_parked']."\" border=\"0\" ".$style." />";

return $pics;
}

function parked() {

global $CURUSER;
if ($CURUSER["parked"] == "yes")
stderr($tracker_lang['error'], $tracker_lang['my_parked']);

}

//��������� �������
function generatePassword($length = 15) {

$set = array("a","A","b","B","c","C","d","D","e","E","f","F","g","G","h","H","i","I","j","J","k","K","l","L","m","M","n","N","o","O","p","P","q","Q","r","R","s","S","t","T","u","U","v","V","w","W","x","X","y","Y","z","Z","1","2","3","4","5","6","7","8","9");
$str = "";
for ($i = 1; $i <= $length; $i++){
$str.= $set[rand(0, count($set)-1)];
}

return $str;
}
//��������� �������


function cache_clean($all) {

$dh = opendir(ROOT_PATH.'/cache/');
while ($file = readdir($dh)) : if (preg_match('/^(.+)\.$/si', $file, $matches))

$file_end = end(explode('.', $matches[1]));
$txt_main = array('info_cache_stat.txt','sqlerror.txt','error_torrent.txt', 'my_license.txt');

if ($file_end == 'txt' && !stristr($matches[1], 'monitoring_') && !in_array($matches[1], $txt_main))
@unlink(ROOT_PATH."/cache/".$file);

endwhile;
closedir($dh);
}


function failedloginscheck () {

global $SITE_Config, $tracker_lang;

$ip = getip();

$r1 = sql_query("SELECT SUM(attempts) AS num FROM loginattempts WHERE ip = ".sqlesc($ip)) or sqlerr(__FILE__, __LINE__);
$a1  = mysql_fetch_assoc($r1);

$total = number_format($a1["num"], 1);

if ($total >= $SITE_Config["maxlogin_limit"]) {
sql_query("UPDATE loginattempts SET banned = 'yes' WHERE ip = ".sqlesc($ip)) or sqlerr(__FILE__, __LINE__);
stderr($tracker_lang['error_data'], sprintf($tracker_lang['banned_fromlimit'], $SITE_Config["maxlogin_limit"], "<b>".$ip."</b>"));
}

}

function failedlogins ($user, $id) {

global $SITE_Config, $tracker_lang;

$ip = getip();
$added = get_date_time();

$count = get_row_count("loginattempts", "WHERE ip = ".sqlesc($ip));

$upd = remaining();

if ($upd == 1) {
$text = "������������ ������������ ���� ".$ip.". �������: ������� ����� >= ".$SITE_Config["maxlogin_limit"];
sql_query("INSERT INTO sitelog (added, color, txt, type) VALUES (".sqlesc(get_date_time()).", ".sqlesc("6e7d8f").", ".sqlesc($text).", ".sqlesc("bans").")");
}

if (empty($count)) {
sql_query("INSERT INTO loginattempts (ip, added, attempts, comment) VALUES (".sqlesc($ip).", ".sqlesc(get_date_time()).", 1, ".sqlesc($id).")") or sqlerr(__FILE__, __LINE__);
} else {

$res = sql_query("SELECT comment FROM loginattempts WHERE ip = ".sqlesc($ip))  or sqlerr(__FILE__,__LINE__);
if (mysql_num_rows($res) == 0)
$new_id = array();
else {
$arr = mysql_fetch_assoc($res);
$new_id = explode(",", $arr["comment"]);
}

$new_id[] = $id;

$update[] = "attempts = attempts + 1";
$update[] = "comment = ".sqlesc(implode(",", $new_id));
sql_query("UPDATE loginattempts SET ".implode(", ", $update)." WHERE ip = ".sqlesc($ip)) or sqlerr(__FILE__, __LINE__);

}

stderr($tracker_lang['error_data'], sprintf($tracker_lang['loginattempts'], $user, "<b><a href=\"recover.php\">".$tracker_lang['recover']."</a></b>", "<b>".number_format($upd-1)."</b>".$tracker_lang['times']));

}

function remaining () {

global $SITE_Config;

$ip = getip();
$res = sql_query("SELECT SUM(attempts) AS num FROM loginattempts WHERE ip = ".sqlesc($ip)) or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res);

return number_format($SITE_Config["maxlogin_limit"] - $arr["num"]);
}


   
function get_seed_time($time, $is_dist = false) {

global $tracker_lang;

$exp = array_map("trim", explode(",", $tracker_lang['time_array']));

$text = array();

$years = ($is_dist === false) ? 31536000 : 31622400;
$years = 31536000;
$days = 86400;
$hours = 3600;
$minutes = 60;

if ($time >= $years) {
$text[] = $exp[0].": ".floor($time / $years);
$time = ($time % $years);
}

if ($time >= $days) {
$text[] = $exp[1].": ".floor($time / $days);
$time = ($time % $days);
}

if ($time >= $hours) {
$text[] = $exp[2].": ".floor($time / $hours);
$time = ($time % $hours);
}

if ($time >= $minutes) {
$text[] = $exp[3].": ".floor($time / $minutes);
$time = ($time % $minutes);
}

if (!empty($time))
$text[] = $exp[4].": ".floor($time);

if (count($text))
return implode(", ", $text);
else
return "N/A";

}

function karma($karma) {

if ($karma == 0)
$color = "#000000";
elseif ($karma < 0)
$color = "#FF0000";
elseif ($karma > 0 && $karma < 10){
$color = "#000080";
$karma = "+".$karma;
} elseif ($karma > 10) {
$color = "#008000";
$karma = "+".$karma;
}
return "<font style=\"color:".$color.";vertical-align:top;font-size:13px;\">".$karma."</font>";

}

//// ������� ��� ������� php4

if(!function_exists("stripos")) {
function stripos($haystack, $needle, $offset = 0) {
return strpos(strtolower($haystack), strtolower($needle), $offset);
}
}

if(!function_exists("str_ireplace")){
function str_ireplace($search,$replace,$subject){
$token = chr(1);
$haystack = strtolower($subject);
$needle = strtolower($search);
while (($pos=strpos($haystack,$needle))!==FALSE){
$subject = substr_replace($subject,$token,$pos,strlen($search));
$haystack = substr_replace($haystack,$token,$pos,strlen($search));
}
$subject = str_replace($token,$replace,$subject);
return $subject;
}
}


if(!function_exists("memory_get_usage")){
function memory_get_usage(){
$pid = @getmypid();
@exec("ps -o rss -p $pid", $output);
return $output[1] *1024;
}
}

if (!function_exists('highlight')) {
function highlight($search, $subject, $hlstart = '<b><font color=red>', $hlend = '</font></b>'){

$srchlen = strlen($search);    // lenght of searched string
if ($srchlen == 0) return $subject;

$find = $subject;
while ($find = stristr($find, $search)){
$srchtxt = substr($find,0,$srchlen);    // get new search text
$find = substr($find,$srchlen);
$subject = str_replace($srchtxt, $hlstart.$srchtxt.$hlend, $subject);    // highlight founded case insensitive search text
}

return $subject;
}
}
//// ������� ��� ������� php4


function meta($id_torrent = false) {

global $SITENAME, $DEFAULTBASEURL, $tracker_lang, $Seo_Config;

$def_ico = $DEFAULTBASEURL; ///http://localhost
$def_url = $DEFAULTBASEURL.$_SERVER["REQUEST_URI"]; ///http://localhost/
$url = htmlspecialchars_uni($_SERVER["REQUEST_URI"]);

if (empty($_COOKIE["uid"])){


$keywords_added = array();
$s = sql_query("SELECT metakey FROM meta WHERE crcfile = ".sqlesc(crc32($url)), $cache = array("type" => "disk", "file" => "meta_".crc32($url), "time" => 60*60)) or sqlerr(__FILE__, __LINE__);
while ($metaget = mysql_fetch_assoc_($s)){
$keywords_added[] = trim($metaget["metakey"]);
}

unset($url);
 
$id = (!empty($_GET["id"]) ? (int) $_GET["id"]:false);
$base_php = basename($_SERVER['SCRIPT_FILENAME']);

if ($base_php == 'forums.php'){
require_once (ROOT_PATH.'/include/functions_forum.php');
return meta_forum();
die;
}

if ($base_php == 'details.php') {

if (!empty($id)){

$s = sql_query("SELECT name, tags, descr, category, image1 FROM torrents WHERE id = ".sqlesc($id), $cache = array("type" => "disk", "file" => "metadetails_".$id, "time" => 60*60)) or sqlerr(__FILE__, __LINE__);
$meta_bot = mysql_fetch_assoc_($s);

if (!empty($meta_bot["category"])){

$s3 = sql_query("SELECT name FROM categories WHERE id = ".sqlesc($meta_bot["category"]), $cache = array("type" => "disk", "file" => "cat_".$meta_bot["category"], "time" => 60*60*12)) or sqlerr(__FILE__, __LINE__);
$meta_bot3 = mysql_fetch_assoc_($s3);

$rss_cat = array("id" => $meta_bot["category"], "name" => $meta_bot3["name"]);

}

/// ������ ������ �� ������� ������ �� ���
preg_match("/\n.*?(� �����:|� �����)(.*?)\n/is", $meta_bot["descr"], $acts);
preg_match_all("/(]|\s|,)([�-��-���a-zA-Z\s]{3,20})\s([�-��-���a-zA-Z]{3,20})/i", $acts[2], $time);

if (count($time[0])){
$name_orig = strip_tags(implode(",", $time[0])).",";
}
/// ������ ������ �� ������� ������ �� ���

//// �������� ��������
$combody_f = htmlspecialchars(preg_replace("/\[((\s|.)+?)\]/is", "", ($meta_bot["descr"])));

$list = explode("\n", $combody_f);

$array_list_1 = array();
$array_list_2 = array();

foreach ($list AS $lif){
$array_list_1[] = strlen($lif);
$array_list_2[] = $lif;
}

$newid = array_keys($array_list_1,max($array_list_1));
$newdesck = $array_list_2[$newid[0]];

if (stristr($newdesck,"/") && stristr($newdesck,":")){
unset($array_list_1[$newid[0]]);
$newid = array_keys($array_list_1,max($array_list_1));
$newdesck = $array_list_2[$newid[0]];
}

$pos1 = strpos($newdesck, '.', 200); // $pos = 7, not 0
$pos2 = strpos($newdesck, '.', 250); // $pos = 7, not 0

if (strlen($newdesck) >= 200 && !empty($pos1))
$newdesck = substr($newdesck, 0, ($pos1+1));
elseif (strlen($newdesck) >= 250 && !empty($pos2))
$newdesck = substr($newdesck, 0, ($pos2+1));


if (!empty($newdesck))
$meta_bot["descr"] = $newdesck;
//// �������� ��������

$array_file = array();
$s_2 = sql_query("SELECT filename FROM files WHERE torrent = ".sqlesc($id)." ORDER BY size DESC LIMIT 100") or sqlerr(__FILE__, __LINE__);
while ($meta_bot_2 = mysql_fetch_assoc($s_2))//AND CHAR_LENGTH('filename') > 7
if (!stristr($meta_bot_2["filename"], "uTorrentPartFile")) $array_file[] = str_replace(array(" - ", "-"), ', ', current(explode(".", $meta_bot_2["filename"])));
}

$name = $meta_bot["name"].", ".$meta_bot["tags"];
$descr = $meta_bot["descr"];

} elseif ($base_php == 'userdetails.php') {

if (!empty($id)){

$s = sql_query("SELECT username, info, signature, title FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$meta_bot = mysql_fetch_assoc($s);

$name = $meta_bot["username"]." ".$meta_bot["info"]." ".$meta_bot["signature"]." ".$meta_bot["title"];
$descr = $meta_bot["info"]." ".$meta_bot["signature"]." ".$meta_bot["title"];

$name = preg_replace("/\[((\s|.)+?)\]/i", "", $name); /// ����� �� ������ [] � ��
/// ������� ������
$name = preg_replace("/(\A|[^=\]'\"a-zA-Z0-9])((http|ftp|https|ftps):\/\/[^()<>\s]+)/is","", $name);
$name = preg_replace("/&#(\d+);/","",$name);
}




} elseif ($base_php == 'browse.php') {

if (!empty($_GET["tag"])){

$q = trim(htmlspecialchars($_GET["tag"]));
$s = sql_query("SELECT id, name FROM categories WHERE id = (SELECT category FROM tags WHERE name = ".sqlesc($q)." LIMIT 1)", $cache = array("type" => "disk", "file" => "tag_".crc32($q), "time" => 60*60)) or sqlerr(__FILE__, __LINE__);
$meta_bot = mysql_fetch_assoc_($s);
$name = $_GET["tag"];
$descr = $meta_bot["name"]." ".$_GET["tag"];

if (!empty($meta_bot["name"]))
$rss_cat = array("id" => $meta_bot["id"], "name" => $meta_bot["name"]);

} elseif (!empty($_GET["cat"])){

$idcat = (int) $_GET["cat"];

if (!empty($idcat)){

$s = sql_query("SELECT name FROM categories WHERE id = ".sqlesc($idcat), $cache = array("type" => "disk", "file" => "cat_".$idcat, "time" => 60*60*12)) or sqlerr(__FILE__, __LINE__);
$meta_bot=mysql_fetch_assoc_($s);

if (!empty($meta_bot["name"]))
$rss_cat = array("id" => $idcat, "name" => $meta_bot["name"]);

}

$name = $descr = $meta_bot["name"];

} else {
$name = $Seo_Config['browse_name'];
$descr = $Seo_Config['browse_desc'];
}





/// ���� ������� ���� ///
} elseif ($base_php == 'persons.php') {

$n = (string) $_GET["n"];
$n = trim($n);

if (!empty($n)){

$s = sql_query("SELECT aname, bio FROM actors WHERE aname = ".sqlesc($n), $cache = array("type" => "disk", "file" => "bio_".crc32($n), "time" => 60*60*5)) or sqlerr(__FILE__,__LINE__);
$meta_bot = mysql_fetch_assoc_($s);

/// ������� ����
$meta_bot["bio"] = preg_replace("/\[img\](http:\/\/[^\s'\"<>]+(\.(jpg|jpeg|gif|png)))\[\/img\]/is","", $meta_bot["bio"]);
$meta_bot["bio"] = preg_replace("/\(((\s|.)+?)\)/is", "", $meta_bot["bio"]); /// ����� �� ������ [] � ��
$meta_bot["bio"] = str_replace(array(".",":",",","-","=","!","?","*", "|"), " ", $meta_bot["bio"]);
$meta_bot["bio"] = str_replace('&quot;', "", $meta_bot["bio"]);
$meta_bot["bio"] = str_replace(array("'","\"","%","$","/","`","`","<",">"), " ", $meta_bot["bio"]);

$meta_bot["bio"] = preg_replace("/\[(.*?)\]/is", "", $meta_bot["bio"]); /// ����� �� ������ [] � ��
$meta_bot["bio"] = preg_replace("/(\A|[^=\]'\"a-zA-Z0-9])((http|ftp|https|ftps):\/\/[^()<>\s]+)/is","", $meta_bot["bio"]);

$descr = preg_replace("/&#(\d+);/","",$meta_bot["bio"]);
$name = $meta_bot["aname"];

}
/// ���� ������� ���� ///



} elseif ($base_php == 'index.php')
$descr = $Seo_Config['index_desc'];

$name_orig.= $Seo_Config['index_name'];

/// ������� ����
$descr = preg_replace("/\[img\](http:\/\/[^\s'\"<>]+(\.(jpg|jpeg|gif|png)))\[\/img\]/is","", $descr);
$descr = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", $descr)); /// ����� �� ������ [] � ��

/// ������� ������
$descr = preg_replace("/(\A|[^=\]'\"a-zA-Z0-9])((http|ftp|https|ftps):\/\/[^()<>\s]+)/is","", $descr);

//$descr = preg_replace('/[^\w]+/i', ' ', $descr); /// ������ . , �� ������
$descr = preg_replace('/[\r\n\t]/i', ' ', $descr); /// ������ ���������



$pos1 = strpos($descr, '.', 230); // $pos = 7, not 0
$pos2 = strpos($descr, '.', 250); // $pos = 7, not 0

if (strlen($descr) >= 255 && !empty($pos1))
$descr = substr($descr, 0, ($pos1+1));
elseif (strlen($descr) >= 250 && !empty($pos2))
$descr = substr($descr, 0, ($pos2+1));
else 
$descr = trim(substr($descr, 0, 255));



$keywords = strip_tags($name);
//$keywords = preg_replace('/[^\w]+/i', ' ', $keywords);
$keywords = preg_replace("/\s/",",",$keywords); /// �������� ������� �� ,
$keywords = trim(preg_replace('/[\r\n\t]/i', ',', $keywords));


if (count($array_file)){

foreach ($array_file AS $key => $val_file){
$val_file = preg_replace("/^(.*?):(.*?)$/is", "\\1", $val_file);
$val_file = preg_replace("/^(([0-9]{1,3})(\s.|.\s|.))(.*?).([A-Za-z0-9]{2,4})$/is", "\\4", trim($val_file));
$val_file = preg_replace("/.([A-Za-z0-9]{2,4})$/is", "", trim($val_file));
$array_file[$key] = trim(tolower($val_file));
}

if (is_array($keywords_added) AND count($keywords_added))
$keywords_added = array_merge_recursive($keywords_added, $array_file);
else
$keywords_added = $array_file;

$name_orig .= (!empty($name_orig) ? ", ":"").trim(implode(", ", $array_file));

}

$dexplode = $keywords.", ".$name_orig;

$dexplode = str_replace("_", " ", $dexplode);
$dexplode = str_replace(array("]","[","'","`","/","\""), "", $dexplode);
$dexplode = preg_replace("/\(((\s|.)+?)\)/i", "", $dexplode); /// ����� �� ������ [] � ��



/// ���� ��� ������ � ������� � meta ������ �� ������������ ����� 
if (!isset($keywords_w)){
/// ����������� ���� �������� ���� keywords
$keywords_array = array_unique(explode(",", $dexplode));

sort($keywords_array);

$keywords_w = "";
if (count($keywords_array)){

$keywords_array = array_unique($keywords_array);

foreach ($keywords_array AS $keys) {

$keys = trim($keys);
$keys = preg_replace("/^([.,:;\-\+\=])/is", "", $keys);
$keys = preg_replace("/\s+/i", " ", $keys);
$keys = trim($keys);

$strall = strlen($keywords_w);

if ($strall < 1000 && !empty($keys) && strlen($keys) >= 3)
$keywords_w.= (!empty($keywords_w) ? ", ":"").$keys;
elseif ($strall >= 1000)
break;

}

} else $keywords_w = $keywords.(!empty($name_orig) ? $name_orig:"");

$keywords_w = preg_replace("/, \s/",", ",$keywords_w); /// �������� ������� �� ,
/// ����������� ���� �������� ���� keywords
}
/// ���� ��� ������ � ������� � meta ������ �� ������������ ����� 




/// ����������� �������� ���� keyphrases
$keyphrases_w = "";
if (count($keywords_added)){

$keywords_added = array_unique($keywords_added);

foreach ($keywords_added AS $keys) {

$keys = trim($keys);
$strall = strlen($keywords_added);

if ($strall < 1000 && !empty($keys)) $keyphrases_w.= (!empty($keyphrases_w) ? ",":"")." ".$keys;
elseif ($strall >= 1000) break;

}

$keyphrases_w = preg_replace("/, \s/",", ", $keyphrases_w); /// �������� ������� �� ,
}
/// ����������� �������� ���� keyphrases




}

$content = "<meta http-equiv=\"content-type\" content=\"text/html; charset=".$tracker_lang['language_charset']."\"/>\n";
$content.= "<meta name=\"author\" content=\"7Max7, ���������� ������\"/>\n";


$content.= "<meta name=\"publisher-url\" content=\"".($def_url)."\"/>\n";
$content.= "<meta name=\"copyright\" content=\"Tesla Tracker TT v.Platinum (".date("Y").")\"/>\n";
$content.= "<meta name=\"generator\" content=\"PhpDesigner ��. useragreement.php\"/>\n";

if (!empty($keywords_w))
$content.= "<meta name=\"keywords\" content=\"".trim($keywords_w)."\"/>\n";

if (!empty($keyphrases_w) && strlen($keyphrases_w) < 1000)
$content.= "<meta name=\"keyphrases\" content=\"".trim($keyphrases_w)."\"/>\n";

if (!empty($descr)){
$content.= "<meta name=\"description\" content=\"".$descr."\"/>\n";
$content.= "<meta property=\"og:description\" content=\"".$descr."\"/>\n";
}

$content.= "<meta name=\"robots\" content=\"all\"/>\n";
//$content.= "<meta name=\"revisit-after\" content=\"7 day\"/>\n";
$content.= "<meta name=\"rating\" content=\"general\"/>\n";


///
if ($base_php == 'details.php' && count($_GET) <> 1){
$content.= "<link rel=\"canonical\" href=\"".($def_ico)."/details.php?id=".$id."\"/>\n";
$content.= "<meta property=\"og:url\" content=\"".($def_ico)."/details.php?id=".$id."\"/>\n";
}

if (!empty($meta_bot["image1"]) AND !valid_url($meta_bot["image1"]))
$content.= "<meta property=\"og:image\" content=\"".$def_ico."/torrents/images/".$meta_bot["image1"]."\"/>\n";
elseif (!empty($meta_bot["image1"]) AND valid_url($meta_bot["image1"]))
$content.= "<meta property=\"og:image\" content=\"".$def_ico."/thumbnail.php?image=".$meta_bot["image1"]."&for=details\"/>\n";


if (!empty($stTitle))
$content.= "<meta property=\"og:title\" content=\"".$stTitle."\"/>\n";

$content.= "<meta property=\"og:site_name\" content=\"".$SITENAME."\"/>\n";


$content.= "<link rel=\"shortcut icon\" href=\"".$def_ico."/pic/favicon.ico\" type=\"image/x-icon\"/>\n";
$content.= "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"".$tracker_lang['rss_news']."\" href=\"".$def_ico."/rss.php\"/>\n";

if (isset($rss_cat))
$content.= "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"".$rss_cat["name"]."\" href=\"".$def_ico."/rss.php?cat=".$rss_cat["id"]."\"/>\n";


if (basename($_SERVER['SCRIPT_FILENAME']) == 'browse.php')
$content.= "<link rel=\"search\" type=\"application/opensearchdescription+xml\" title=\"������ ������� �� ".$SITENAME."\" href=\"".$def_ico."/js/tracker.xml\"/>\n";
else
$content.= "<link rel=\"search\" type=\"application/opensearchdescription+xml\" title=\"������ ������� ����� google.com �� ����� ".$SITENAME."\" href=\"".$def_ico."/js/google.xml\"/>\n";

echo $content;
}
/// ��� stdhead (chat) �������� meta ����������

function protectmail($s=false) {

$result = '';
$s = "mailto: ".$s;
for ($i = 0; $i < strlen($s); $i++) {
$result .= '&#'.ord(substr($s, $i, 1)).';';
}
return $result;

}

function file_ungzip ($fromFile){

$zp = @gzopen($fromFile, "r");

while (!@gzeof($zp))
$string.= @gzread($zp, 4096);

@gzclose($zp);
return $string;
}

function repair_table ($table = false){

global $SITE_Config, $tracker_lang;

if ($SITE_Config["try_repair_table"] == true){

include("include/passwords.php");

if (!empty($table)){

$rresult_1 = mysql_query("REPAIR TABLE `".$table."` EXTENDED");
$rresult_2 = mysql_query("REPAIR TABLE `".$table."` USE_FRM");

} else {

$result = mysql_query("SHOW TABLE STATUS FROM ".$Mysql_Config['db']);

while ($row = mysql_fetch_array($result)) {
$res_t = mysql_query("ANALYZE TABLE `".$row[0]."`");
$row_t = mysql_fetch_assoc($res_t); ///Table Op Msg_type Msg_text

if ($row_t["Msg_type"] <> "status"){
$rresult_1 = mysql_query("REPAIR TABLE `".$row[0]."` EXTENDED");
$rresult_2 = mysql_query("REPAIR TABLE `".$row[0]."` USE_FRM");
}
}

}

unset($Mysql_Config);

} else die("<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=".$tracker_lang['language_charset']."\">
<meta http-equiv=\"Content-Style-Type\" content=\"text/css\">
<title>".$tracker_lang['mysql_timeout']."</title>
<style type=\"text/css\">
body { min-width: 760px; color: #000000; background: #E3E3E3; font: 16px Verdana; }
.msg { margin: 20%; text-align: center; background: #EFEFEF; border: 1px solid #B7C0C5; }
</style>
</head><body><div class=\"msg\"><p style=\"margin: 1em 0;\">".sprintf($tracker_lang['mysql_t_timeout'], $table).".</p></div></body>");
}






function direct_unmatch ($torrentid, $name = false, $cat = false) {

/**
*** ������� �������� ������ ���� � ������� ��������, ��� ��� �������
**/

if (empty($torrentid)) return false;

if (empty($name) || empty($cat)){
$res_cat = sql_query("SELECT id, name, category FROM torrents WHERE id = ".sqlesc($torrentid)) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($res_cat) == 0) return false;
$arr_cat = mysql_fetch_assoc($res_cat);

if (empty($name)) $name = $arr_cat['name'];
if (empty($cat)) $cat = $arr_cat['category'];
}

$search = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", htmlspecialchars_uni($name)));
$search = trim(preg_replace("/[^�-��-�A-Za-z�0-9_-]/i", " ", $search)); /// ������� ��� ���������� � ������ �������

$q1 = trim(preg_replace("/\\%{2,}/i", "$1", preg_replace("/\s/i", "%", sqlwildcardesc($search))));

$list = explode(" ", $search);

$listrow = array();
foreach ($list AS $lis){
if (strlen($lis)>=3) $listrow[] = "+".$lis;
}


$q2 = trim(htmlspecialchars_uni($name));

$listrow = array_unique($listrow); /// ������� ���������
$view_uniq = trim(implode(" ", $listrow));


$sql_mas = sql_query("SELECT id FROM my_match ORDER BY id DESC LIMIT 1") or sqlerr(__FILE__, __LINE__);
$row_mas = mysql_fetch_assoc($sql_mas);

$indexd = 0;

sql_query("Insert IGNORE Into my_match (mysearch, userid, torrentid)
SELECT id, userid, ".sqlesc($torrentid)."
FROM my_search WHERE LOCATE (my_search.words, ".sqlesc(sqlwildcardesc($q2)).") AND (my_search.cats = '' OR ".sqlesc($cat)." IN (my_search.cats))
") or sqlerr(__FILE__, __LINE__);

$indexd += mysql_insert_id();

if (strlen($search) >= 4){ /// ������ ������ ������ ������ 3 �������� (�� ������ mysql)
sql_query("Insert IGNORE Into my_match (mysearch, userid, torrentid)
SELECT id, userid, ".sqlesc($torrentid)."
FROM my_search WHERE MATCH (my_search.words) AGAINST ('".$view_uniq."' IN BOOLEAN MODE) AND (my_search.cats = '' OR ".sqlesc($cat)." IN (my_search.cats))
") or sqlerr(__FILE__, __LINE__);

$indexd += mysql_insert_id();
}

if (strlen($search) >= 4) {/// ������ ������ ������ ������ 3 �������� (�� ������ mysql)
sql_query("Insert IGNORE Into my_match (mysearch, userid, torrentid)
SELECT id, userid, ".sqlesc($torrentid)."
FROM my_search WHERE my_search.words LIKE ".sqlesc("%".$q1."%")." AND (my_search.cats = '' OR ".sqlesc($cat)." IN (my_search.cats))
") or sqlerr(__FILE__, __LINE__);

$indexd += mysql_insert_id();
}


if (!empty($indexd)){

sql_query("UPDATE users SET unmatch = IF(unmatch > 0, unmatch + 1, 0) WHERE id IN (SELECT GROUP_CONCAT(userid) FROM my_match WHERE my_match.id > ".sqlesc($row_mas['id']).")") or sqlerr(__FILE__, __LINE__);

return get_row_count("my_match", "WHERE id > ".sqlesc($row_mas['id']));

/*
$sql_max = sql_query("SELECT GROUP_CONCAT(userid) AS string FROM my_match WHERE id > ".sqlesc($row_mas['id'])." LIMIT 200") or sqlerr(__FILE__, __LINE__);
while ($row_max = mysql_fetch_assoc($sql_max)){
if (!empty($row_max["string"])) sql_query("UPDATE users SET unmatch = unmatch+1 WHERE id IN (".$row_max["string"].")");
}
*/

}

/*
//if (strlen($search) < 4 || mysql_num_rows($sql) == 0) /// ������ �������� ������� ������ � ������ ������� �������
$sql = sql_query("SELECT * FROM my_search WHERE LOCATE (my_search.words, ".sqlesc(sqlwildcardesc($q2)).")") or sqlerr(__FILE__, __LINE__);

if (strlen($search) >= 4 && mysql_num_rows($sql) == 0) /// ������ ������ ������ ������ 3 �������� (�� ������ mysql)
$sql = sql_query("SELECT * FROM my_search WHERE MATCH (my_search.words) AGAINST ('".$view_uniq."' IN BOOLEAN MODE)") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($sql) == 0) /// ������ �������� ������� ������ � ������ ������� �������
$sql = sql_query("SELECT * FROM my_search WHERE my_search.words LIKE ".sqlesc("%".$q1."%")) or sqlerr(__FILE__, __LINE__);

$count_all = 0;
if (mysql_num_rows($sql) > 0){
while ($row = mysql_fetch_assoc($sql)){

$catay = (!empty($row['cats']) ? explode(",", $row['cats']):false);
if (!empty($row['cats']) && count($catay) && !in_array($cat, $catay)) break;

sql_query("INSERT INTO my_match (mysearch, userid, torrentid) VALUES (".sqlesc($row['id']).", ".sqlesc($row["userid"]).", ".sqlesc($torrentid).")");

$id = mysql_insert_id();
if (!empty($id)){
sql_query("UPDATE users SET unmatch = unmatch+1 WHERE id = ".sqlesc($row["userid"])) or sqlerr(__FILE__,__LINE__);
unsql_cache("arrid_".$row["userid"]);
++$count_all;
}

//print_r($row);
}

return $count_all;
}
*/




//echo "$view_uniq<br />";
//echo "$q2<br />";

//echo $name;
//die;

return true;
}


function stop_dfiles ($name = false) {

/**
*** ������� ������� � ������� ������ �� ������ � ������� (��������� ����� �������)
**/

if (empty($name)) return false;

$search = preg_replace("/\(((\s|.)+?)\)/is", "", preg_replace("/\[((\s|.)+?)\]/is", "", htmlspecialchars_uni($name)));
$search = trim(preg_replace("/[^�-��-�A-Za-z�0-9_-]/i", " ", $search)); /// ������� ��� ���������� � ������ �������

$q1 = trim(preg_replace("/\\%{2,}/i", "$1", preg_replace("/\s/i", "%", sqlwildcardesc($search))));

$list = explode(" ", $search);

$listrow = array();
foreach ($list AS $lis){
if (strlen($lis)>=3) $listrow[] = "+".$lis;
}

$q2 = trim(htmlspecialchars_uni($name));

$listrow = array_unique($listrow); /// ������� ���������
$view_uniq = trim(implode(" ", $listrow));


$sql_mas = sql_query ("SELECT id, key_in, number FROM stop_dfiles WHERE LOCATE (stop_dfiles.key_in, ".sqlesc(sqlwildcardesc($q2)).")") or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($sql_mas) > 0){
$row_mas = mysql_fetch_assoc($sql_mas);

sql_query ("UPDATE stop_dfiles SET number = number+1, lastime = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($row_mas['id'])) or sqlerr(__FILE__, __LINE__);

return array("id" => $row_mas['id'], "key_in" => $row_mas['key_in'], "number" => $row_mas['number']);
}

if (strlen($search) >= 4){ /// ������ ������ ������ ������ 3 �������� (�� ������ mysql)
$sql_mas = sql_query ("SELECT id, key_in, number FROM stop_dfiles WHERE MATCH (stop_dfiles.key_in) AGAINST ('".$view_uniq."' IN BOOLEAN MODE)") or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($sql_mas) > 0){
$row_mas = mysql_fetch_assoc($sql_mas);

sql_query ("UPDATE stop_dfiles SET number = number+1, lastime = ".sqlesc(get_date_time())." WHERE id = ".sqlesc($row_mas['id'])) or sqlerr(__FILE__, __LINE__);

return array("id" => $row_mas['id'], "key_in" => $row_mas['key_in'], "number" => $row_mas['number']);
}

}

return false;
}






?>