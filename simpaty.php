<?
require "include/bittorrent.php";

dbconn();
loggedinorreturn();

$action = (isset($_GET['action']) ? (string) $_GET['action'] : 'add');
$type = intval(isset($_GET['type']) ? $_GET['type'] : 0);
$targetid = intval(isset($_GET['targetid']) ? $_GET['targetid']:0);
$resp_type = (isset($_GET['good']) ? 1:0);

if (empty($action) || $action <> 'add' || $CURUSER["warned"] == 'yes')
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);

if ($CURUSER["id"] == $targetid)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

if (!is_valid_id($targetid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value']);


$count = get_row_count("users", "WHERE id = ".sqlesc($targetid));
if (empty($count))
stderr($tracker_lang['error'], $tracker_lang['no_user_intable']);


$r2 = sql_query("SELECT name FROM torrents WHERE id = ".sqlesc($type)." AND owner = ".sqlesc($targetid)) or sqlerr(__FILE__, __LINE__);
if (mysql_num_rows($r2) == 0)
stderr ($tracker_lang['error'], $tracker_lang['torrent_del_or_move']);

$ar_to = mysql_fetch_array($r2);

$r = sql_query("SELECT id FROM simpaty WHERE touserid = ".sqlesc($targetid)." AND type = ".sqlesc($type)." AND fromuserid = ".sqlesc($CURUSER['id'])) or sqlerr(__FILE__, __LINE__);

if (mysql_num_rows($r) == 1)
stderr ($tracker_lang['error'], $tracker_lang['simpaty_error']);

$description = (isset($_POST["description"]) ? htmlspecialchars_uni($_POST["description"]):"");

if (empty($description)){
stdhead($tracker_lang['reason_simpaty']);

echo "<form action=\"simpaty.php?".($resp_type == 1 ? 'good':'bad')."&targetid=".$targetid."&type=".$type."\" method=\"post\"><table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"2\">

<tr><td class=\"colhead\" align=\"left\" colspan=\"2\">".$tracker_lang['reason_simpaty']."</td></tr>
<tr><td class=\"b\" align=\"left\">".$tracker_lang['torrent']."</td><td class=\"a\" align=\"center\"><a href=\"details.php?id=".$type."\">".$ar_to["name"]."</a></td></tr>
<tr><td class=\"a\" align=\"left\">".$tracker_lang['my_reason']."</td><td class=\"b\" align=\"center\"><input type=\"text\" name=\"description\" maxlength=\"2048\" size=\"100\"></textarea></td></tr>

<tr><td class=\"b\" align=\"right\" colspan=\"2\">
<input type=\"submit\" class=\"btn\" value=\"".($resp_type == 1 ? $tracker_lang['respect']:$tracker_lang['antirespect'])."\" /></td></tr>

</table></form>";

stdfoot();
die;
}

sql_query ('INSERT INTO simpaty (touserid, fromuserid, bad, good, type, respect_time, description) VALUES ('.sqlesc($targetid).', '.sqlesc($CURUSER['id']).', '.(empty($resp_type) ? "'1', '0'":"'0', '1'").', '.sqlesc($type).', '.sqlesc(get_date_time()).', '.sqlesc($description).')') or sqlerr(__FILE__, __LINE__);

if ($resp_type == 1)
sql_query("UPDATE users SET simpaty = simpaty + 1 WHERE id = ".sqlesc($targetid)) or sqlerr(__FILE__, __LINE__);
else
sql_query("UPDATE users SET simpaty = simpaty - 1 WHERE id = ".sqlesc($targetid)) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$targetid);

$msg = "������������ [url=userdetails.php?id=".$CURUSER['id']."]".$CURUSER['username']."[/url] �������� ��� ".($resp_type == 1 ? "�������":"�����������")." �� [url=details.php?id=".$type."]�������[/url] � ��������� �� ��������� ����������: \n[quote]".$description."[/quote]";

sql_query("INSERT INTO messages (sender, receiver, added, msg, poster, subject) VALUES (0, ".sqlesc($targetid).", ".sqlesc(get_date_time()).", ".sqlesc($msg).", 0, \"����������� �� ��������� ���������\")") or sqlerr(__FILE__, __LINE__);

header("Refresh: 5; url=details.php?id=".$type);

stderr ($tracker_lang['warning'], $tracker_lang['sending_ok']);

?>