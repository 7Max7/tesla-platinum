<?php
require "include/bittorrent.php";
gzip();
dbconn();
loggedinorreturn();

if (get_user_class() < UC_SYSOP) {
attacks_log($_SERVER["SCRIPT_FILENAME"]);
stderr($tracker_lang["error"], $tracker_lang["access_denied"]);
}

accessadministration();

stdhead($tracker_lang['b_action']);


$actions = (string) $_POST["actions"]; /// ��� �����
$cheKer = ((isset($_POST["cheKer"]) && is_array($_POST["cheKer"])) ? $_POST["cheKer"]: false); /// ��� ������
$ref = (isset($_POST["referer"]) ? htmlspecialchars_uni($_POST["referer"]):htmlspecialchars_uni($_SERVER["HTTP_REFERER"]));
if ($ref == 'ADDREFLINK') $ref = htmlspecialchars_uni($_SERVER["HTTP_REFERER"]);


echo "<table class=\"embedded\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";

echo "<tr><td class=\"colhead\" align=\"center\" colspan=\"4\">".$tracker_lang['b_action']."</td></tr>";


if (preg_match("/(tags.php|premod.php)/is", $ref)){

echo "<tr><td class=\"b\" colspan=\"5\" align=\"center\">

<form method=\"post\" action=\"tags.php\">
<textarea name=\"test\" cols=\"100\" rows=\"2\"></textarea>
<br /><input type=\"submit\" value=\"".$tracker_lang['tags_test']."\" class=\"btn\" />
</form>

</td></tr>";

}


echo "<tr>
<td class=\"a\" align=\"center\">".$tracker_lang['torrents']."</td>
<td class=\"a\" width=\"25%\" align=\"center\">".$tracker_lang['action']."</td>
</tr>";



if (!count($cheKer) || $cheKer == false) {
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['bact_notor']."</td></tr>\n";
echo "</table>";
stdfoot();
die;
}

if (empty($actions)) {
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['bact_noact']."</td></tr>\n";
echo "</table>";
stdfoot();
die;
}

$updateset = array();
$where = array();
$viewup = "";

if ($actions == "main"){
/// ��������� ��������
$updateset[] = "sticky = 'yes'";
$where = "sticky = 'no'";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." �������� ��������.\n";
$viewup = $tracker_lang['act_main'];

} elseif ($actions == "multi"){
/// �������� ����� �������� �����
$updateset[] = "multi_time = '0000-00-00 00:00:00'";
$where = "multi_time <> '0000-00-00 00:00:00'";

unsql_cache("usermod_2");

$viewup = $tracker_lang['act_multi'];

} elseif ($actions == "unmain"){
/// ����� ��������
$updateset[] = "sticky = 'no'";
$where = "sticky = 'yes'";

$torrent_com = get_date_time() . " ".$CURUSER["username"]." ���� ��������.\n";
$viewup = $tracker_lang['act_unmain'];

} elseif ($actions == "check"){
/// ��������� ���������
$updateset[] = "moderated = 'yes'";

$updateset[] = "moderatedby = ".sqlesc($CURUSER["id"]);
$updateset[] = "moderatordate = ".sqlesc(get_date_time());

$where = "moderated = 'no'";
$torrent_com = get_date_time() . " ".$CURUSER["username"]." ������� �����.\n";
$viewup = $tracker_lang['act_check'];

unsql_cache("usermod_1");

} elseif ($actions == "uncheck"){
/// ����� ���������
$updateset[] = "moderated = 'no'";

unsql_cache("usermod_1");

$where = "moderated = 'yes'";
$torrent_com = get_date_time() . " ".$CURUSER["username"]." ���� ���������.\n";

$viewup = $tracker_lang['act_uncheck'];

} elseif ($actions == "gold"){
/// ��������� ������
$updateset[] = "free = 'yes'";

$where = "free = 'no'";
$viewup = $tracker_lang['act_gold'];

} elseif ($actions == "ungold"){
/// ����� ������
$updateset[] = "free = 'no'";

$where = "free = 'yes'";
$viewup = $tracker_lang['act_ungold'];

} elseif ($actions == "untags"){
/// ����� ������
$updateset[] = "tags = ''";

$where = "tags <> ''";
$viewup = $tracker_lang['act_untags'];

} elseif ($actions == "newdate"){
/// ��������� ����� ����
$updateset[] = "added = ".sqlesc(get_date_time());
$viewup = $tracker_lang['act_newdate'];

} elseif ($actions == "anonim"){
/// ��������� ����� ����
$updateset[] = "owner = '0'";
$viewup = $tracker_lang['act_anonim'];

$torrent_com = get_date_time() . " ".$CURUSER["username"]." �������� ������ �����������.\n". $torrent_com;

} elseif ($actions == "movcat") {

$pcat = (int) $_POST["pcat"];

$res_cat = sql_query("SELECT name FROM categories WHERE id = ".sqlesc($pcat)) or sqlerr(__FILE__,__LINE__);
$row_cat = mysql_fetch_assoc($res_cat);

if (empty($row_cat["name"]) || empty($pcat)){
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_select_cat']."</td></tr>\n";
echo "</table>";
stdfoot();
die;
}

$res = sql_query("SELECT torrents.id, torrents.name, torrents.category, categories.name AS catname
FROM torrents
LEFT JOIN categories ON categories.id=torrents.category
WHERE torrents.id IN (".implode (", ", array_map ("sqlesc", $cheKer)).") AND torrents.category <> ".sqlesc($pcat)) or sqlerr(__FILE__,__LINE__);

if (mysql_affected_rows() == 0)
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_torrent_bact_id']."</td></tr>\n";

while ($row = mysql_fetch_assoc($res)) {

$cat=$row["category"];
$catname=$row["catname"];
$tname=$row["name"];

unsql_cache("multi_viewid_".$row["id"]);

echo "<tr>";
echo "<td class=\"b\" align=\"center\">".$row["id"]." - ".$tname."</td>";
echo "<td width=\"25%\" class=\"b\"><s>".$catname."</s> ".$tracker_lang['in']." <u>".$row_cat["name"]."</u></td>";
echo "</tr>";

}

if (mysql_num_rows($res) <> 0)
sql_query("UPDATE torrents SET category = ".sqlesc($pcat)." WHERE id IN (" . implode(", ", array_map("sqlesc", $cheKer)).")") or sqlerr(__FILE__,__LINE__);

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

echo "</table>";

stdfoot();
die;

} elseif ($actions == "pravo"){

require_once ROOT_PATH."/include/benc.php";

$res = sql_query("SELECT id, name, info_hash, image1, size, picture1, picture2, picture3, picture4 FROM torrents WHERE id IN (".implode (", ", array_map ("sqlesc", $cheKer)).")") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res) == 0)
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_torrent_bact_id']."</td></tr>\n";

while ($row = mysql_fetch_assoc($res)) {

$info_hash = $row["info_hash"];

$descki = array();

$resf = sql_query("SELECT * FROM files WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
if (mysql_num_rows($resf) <> 0){
while ($rowf = mysql_fetch_assoc($resf))
$descki[] = $rowf["filename"].":".mksize($rowf["size"])."\n";
} else {

$ifilename = ROOT_PATH."/torrents/".$row["id"].".torrent";

if (@file_exists($ifilename)){

$dict = bdec_file($ifilename);
list($info) = dict_check_t($dict, "info");
list($dname, $plen, $pieces) = @dict_check_t($info, "name(string):piece length(integer):pieces(string)");
$filelist = array();
$totallen = @dict_get_t($info, "length", "integer");
if (isset($totallen))
$descki[] = utf8_to_win($dname).":".mksize($totallen);
else {
$flist = @dict_get_t($info, "files", "list");
$totallen = 0;

if (count($flist)){
foreach ($flist as $sf) {
list($ll, $ff) = @dict_check_t($sf, "length(integer):path(list)");
$totallen += $ll;
$ffa = array();

foreach ($ff as $ffe) {
$ffa[] = $ffe["value"];
}
$descki[] = utf8_to_win(implode("/", $ffa)).":".mksize($ll);
}
}
}
}

}

$count = count($descki);
if ($count > 1000) {
array_splice($descki, 1000);
$descki[] = '...';
}

$desc = implode("\n", $descki)."\n\n[b]���������� ������[/b]: ".(count($descki) <> $count ? count($descki)." / ".$count:$count)." ��.\n\n[b]����� ������[/b]: ".mksize((!empty($totallen) ? $totallen:$row["size"]));

$tname = htmlspecialchars_uni($row["name"]);

$desc = "[spoiler=������ ������ ($id)]\n".$desc."[/spoiler]";

sql_query("INSERT INTO license VALUES (0,".sqlesc($tname).",".sqlesc($info_hash).",".sqlesc(get_date_time()).",".sqlesc(($desc)).")");

$nid = mysql_insert_id();

unset($desc);

echo "<tr>";
echo "<td class=\"b\" align=\"center\">".$row["id"]." - ".$tname."</td>";
echo "<td width=\"25%\" class=\"b\">".($nid ? "+":"-")." ".$tracker_lang['file_license']."</td>";
echo "</tr>";

$reasonstr = "������� ���������� ������� (browse).";

$ip_user = $CURUSER["ip"];
$user = $CURUSER["username"];

if (count($cheKer)<=101)
write_log("������� ".$row["id"]." ($tname) ��� ������ ������������� $user. �������: $reasonstr\n", "F25B61","torrent");

unsql_cache("block-comment");

@unlink(ROOT_PATH."/torrents/".$row["id"].".torrent");


sql_query("DELETE FROM files WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM comments WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM thanks WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM cheaters WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM torrents WHERE id = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM snatched WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM ratings WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM checkcomm WHERE checkid = ".sqlesc($row["id"])." AND torrent = '1'") or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM bookmarks WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM my_match WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);

unsql_cache("anet_".$row["info_hash"]); // ������ �������
unsql_cache("anto_".$row["info_hash"]); // ������
unsql_cache("multi_viewid_".$row["id"]);

if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["image1"]) && !empty($row["image1"]))
@unlink(ROOT_PATH."/torrents/images/".$row["image1"]);

for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["picture".$xpi_s]) && !empty($row["picture".$xpi_s]))
@unlink (ROOT_PATH."/torrents/images/".$row["picture".$xpi_s]);

}

}

unsql_cache("block-last_files"); /// ��������� �������
unsql_cache("block_showrealese_mulcache"); /// ��������� �������
unsql_cache("block_showrealese_topc"); /// ��� ������
unsql_cache("showRealese_search"); /// ��� ���������
unsql_cache("showRealese_last"); /// ��� ���������
unsql_cache("block-showRealese"); /// �������
unsql_cache("block_3dview_0"); /// ������� ��� ���� 3view

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

echo "</table>";

stdfoot();
die;

} elseif ($actions == "tag_new"){

$res = sql_query("SELECT id, name, descr, category FROM torrents WHERE id IN (".implode (", ", array_map ("sqlesc", $cheKer)).")") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res) == 0)
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_torrent_bact_id']."</td></tr>\n";

while ($row = mysql_fetch_assoc($res)) {

$tname = $row["name"];
$tagarray = tags_f_descr($row["descr"]);

$ttag = trim(implode(", ", $tagarray));

if (count($tagarray) && !empty($ttag)){

sql_query("UPDATE torrents SET tags = ".sqlesc($ttag)." WHERE id=".sqlesc($row["id"])) or sqlerr(__FILE__, __LINE__);

foreach ($tagarray as $tag) {
sql_query("UPDATE tags SET howmuch=howmuch+1 WHERE name = ".sqlesc($tag)) or sqlerr(__FILE__, __LINE__);
}

foreach ($tagarray as $tagi) {
sql_query("INSERT INTO tags (category, name, howmuch,added) VALUES (".sqlesc($row["category"]).", ".sqlesc($tagi).", 1, ".sqlesc(get_date_time()).")");
}
}

echo "<tr>";
echo "<td class=\"b\" align=\"center\">".$row["id"]." - ".$tname." ".(count($tagarray) ? "<br /><b>".$tracker_lang['act_tag_new']."</b>: ".@implode(", ", $tagarray):"")."</td>";
echo "<td width=\"25%\" class=\"b\">".(count($tagarray) ? "".$tracker_lang['success'].": ".count($tagarray):"<i>".$tracker_lang['success'].": ".$tracker_lang['error_data']."</i>")."</td>";
echo "</tr>";

unset($tagarray);
}

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

echo "</table>";

stdfoot();
die;

} elseif ($actions == "delete"){

$res = sql_query("SELECT id, name, image1, category, owner, info_hash FROM torrents WHERE id IN (".implode (", ", array_map ("sqlesc", $cheKer)).")") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res) == 0)
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_torrent_bact_id']."</td></tr>\n";

while ($row = mysql_fetch_assoc($res)) {

$cat=$row["category"];
$cat_user=$CURUSER["catedit"];
$tname=$row["name"];

echo "<tr>";
echo "<td class=\"b\" align=\"center\">".$row["id"]." - ".$tname."</td>";
echo "<td width=\"25%\" class=\"b\">".$tracker_lang['delete']." (".$tracker_lang['success'].")</td>";
echo "</tr>";

$reasonstr = "������ ��� �� �������� ��� ������� (browse).";

$ip_user = $CURUSER["ip"];
$user = $CURUSER["username"];

if (count($cheKer)<=101)
write_log("������� $row[id] ($tname) ��� ������ ������������� $user. �������: $reasonstr\n", "F25B61","torrent");

unsql_cache("block-comment");

@unlink(ROOT_PATH."/torrents/".$row["id"].".torrent");
if (!preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $row["image1"]) && !empty($row["image1"]))
@unlink(ROOT_PATH."/torrents/images/".$row["image1"]);


sql_query("DELETE FROM files WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM comments WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM thanks WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM cheaters WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM torrents WHERE id = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM snatched WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM ratings WHERE torrent = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM checkcomm WHERE checkid = ".sqlesc($row["id"])." AND torrent = '1'") or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM bookmarks WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM my_match WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);

unsql_cache("anet_".$row["info_hash"]); // ������ �������
unsql_cache("anto_".$row["info_hash"]); // ������
unsql_cache("multi_viewid_".$row["id"]);
unsql_cache("simtorid_".$row["id"]); /// �������� ���� ������� ������
unsql_cache("simtorid_alt_".$row["id"]); /// �������� ���� ������� ������

}


unsql_cache("block-last_files"); /// ��������� �������
unsql_cache("block_showrealese_mulcache"); /// ��������� �������
unsql_cache("block_showrealese_topc"); /// ��� ������
unsql_cache("showRealese_search"); /// ��� ���������
unsql_cache("showRealese_last"); /// ��� ���������
unsql_cache("block-showRealese"); /// �������
unsql_cache("block_3dview_0"); /// ������� ��� ���� 3view

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">
<a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a>
</td></tr>\n";

echo "</table>";

stdfoot();
die;
}


elseif ($actions == "match"){

$res = sql_query("SELECT id, name, category FROM torrents WHERE id IN (".implode (", ", array_map ("sqlesc", $cheKer)).")") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res) == 0)
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_torrent_bact_id']."</td></tr>\n";

while ($row = mysql_fetch_assoc($res)) {

$tname = $row["name"];

$add_match = direct_unmatch ($row["id"], $row["name"], $row["category"]);

echo "<tr>";
echo "<td class=\"b\" align=\"center\">".$row["id"]." - ".$tname."</td>";
echo "<td width=\"25%\" class=\"b\">".(is_valid_id($add_match) ? $tracker_lang['act_match']." (".$add_match." ".$tracker_lang['pcs'].".)":"<em>".$tracker_lang['act_match']."</em>")."</td>";
echo "</tr>";

unset($add_match);
}

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

echo "</table>";

stdfoot();
die;

} elseif ($actions == "unmatch"){

$res = sql_query("SELECT id, name, category FROM torrents WHERE id IN (".implode (", ", array_map ("sqlesc", $cheKer)).")") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($res) == 0)
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_torrent_bact_id']."</td></tr>\n";

$nrow = array();
$sql_2 = sql_query("SELECT userid FROM my_match WHERE torrentid IN (".implode (", ", array_map ("sqlesc", $cheKer)).") GROUP BY userid") or sqlerr(__FILE__, __LINE__);
while ($row_2 = mysql_fetch_assoc($sql_2)) {
$nrow[] = $row_2['userid'];
}

while ($row = mysql_fetch_assoc($res)) {

$tname = $row["name"];

sql_query("DELETE FROM my_match WHERE torrentid = ".sqlesc($row["id"])) or sqlerr(__FILE__,__LINE__);
$counter = mysql_affected_rows();

echo "<tr>";
echo "<td class=\"b\" align=\"center\">".$row["id"]." - ".$tname."</td>";
echo "<td width=\"25%\" class=\"b\">".(!empty($counter) ? $tracker_lang['act_unmatch']." (".$counter." ".$tracker_lang['pcs'].".)":"<em>".$tracker_lang['act_unmatch']."</em>")."</td>";
echo "</tr>";

unset($counter);
}

if (mysql_num_rows($res) > 0 && count($nrow)){
foreach ($nrow AS $uid){
/// ��� ��� ������� �������� ����� �������, ����� ������������ ��������������� ����� ��� ������� � �������������, ������ ������� ������� (��� � mematch.php �����)
sql_query("UPDATE users SET unmatch = (SELECT COUNT(*) FROM my_match WHERE userid = ".sqlesc($uid).") WHERE id = ".sqlesc($uid)) or sqlerr(__FILE__,__LINE__);
unsql_cache("arrid_".$uid);
unsql_cache("block-showRealese_last_u".$uid."n");
}
}


if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\"><a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a></td></tr>\n";

echo "</table>";

stdfoot();
die;

}



if ($actions <> "delete" && $actions <> "movcat" && $actions <> "pravo" && $actions <> "tag_new" && count($updateset)){

$res = sql_query("SELECT * FROM torrents WHERE id IN (".implode(", ", array_map("sqlesc", $cheKer)).") ".(!empty($where) ? "AND ".$where:"")) or sqlerr(__FILE__,__LINE__);

if (mysql_affected_rows() == 0)
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">".$tracker_lang['no_torrent_bact_id']."</td></tr>\n";

while ($row = mysql_fetch_assoc($res)){

echo "<tr>";
echo "<td class=\"b\" align=\"left\">".$row["id"]." - ".$row["name"]."</td>";
echo "<td width=\"25%\" align=\"center\" class=\"b\">".$viewup."</td>";
echo "</tr>";

unsql_cache("simtorid_".$row["id"]); /// �������� ���� ������� ������
unsql_cache("simtorid_alt_".$row["id"]); /// �������� ���� ������� ������
unsql_cache("multi_viewid_".$row["id"]);
}

if (!empty($torrent_com))
$updateset[] = "torrent_com = CONCAT_WS('',".sqlesc($torrent_com).", torrent_com)";

if (mysql_affected_rows() <> 0)
sql_query("UPDATE torrents SET ".implode(", ", $updateset)." WHERE id IN (" . implode(", ", array_map("sqlesc", $cheKer)) . ") ".(!empty($where) ? "AND ".$where:"")) or sqlerr(__FILE__,__LINE__);

}

if (!empty($ref))
echo "<tr><td class=\"b\" align=\"center\" colspan=\"3\">
<a class=\"alink\" href=\"".$ref."\">".$tracker_lang['back_inlink']."</a>
</td></tr>\n";

echo "</table>";

stdfoot();
?>