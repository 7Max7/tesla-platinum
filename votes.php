<?
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

if (get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$count = get_row_count("ratings");

list($pagertop, $pagerbottom, $limit) = pager(100, $count, "votes.php?");

$res = sql_query("SELECT r.*, t.name, u.username, u.class, IF(t.numratings < 1, NULL, ROUND(t.ratingsum / t.numratings, 1)) AS ratingsum, t.numratings
FROM ratings AS r 
LEFT JOIN torrents AS t ON t.id = r.torrent 
LEFT JOIN users AS u ON u.id = r.user 
".$limit) or sqlerr(__FILE__, __LINE__);

stdhead($tracker_lang['tab_rating'], true);

//begin_frame($tracker_lang['number_all'].": ".$count);

echo ("<table class=\"embedded\" width=\"100%\">\n");

if ($count > 0) {


echo ("<tr>
<td class=\"colhead\" align=\"center\">#</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['torrent']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['signup_username']."</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['rating']." / ".$tracker_lang['all_rating']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['clock']."</td>
</tr>\n");



echo ("<tr><td colspan=\"5\">".$pagertop."</td></tr>");

$num = 0;

while($row = mysql_fetch_assoc($res)) {

if (!empty($row["ratingsum"]))
$row_rating = round($row["ratingsum"]/$row["numratings"], 1);
else
$row_rating = 0;

$cl2 = 'class = "b"'; $cl1 = 'class = "a"';

if ($num % 2 == 1){
$cl1 = 'class = "b"';
$cl2 = 'class = "a"';
}

echo ("<tr>
<td ".$cl1." align=\"center\">".$row["id"]."</td>
<td ".$cl2.">".(empty($row['name']) ? $tracker_lang['no_torrent_with_such_id'].": " . $row['torrent'] . "":"<a href=\"details.php?id=".$row['torrent']."\">" . $row['name'] . "</a>")."</td>
<td ".$cl1." align=\"center\">".(empty($row['username']) ? $tracker_lang['anonymous'].": ".$row['user']."":"<a href=\"userdetails.php?id=".$row['user']."\">".get_user_class_color($row['class'], $row['username'])."</a>")."</td>
<td ".$cl2." align=\"left\">".pic_rating_b(10, $row['rating'])." (".$row_rating.")</td>
<td ".$cl1." align=\"center\">".$row['added']."</td>
</tr>");

++$num;
}

echo ("<tr><td colspan=\"5\">".$pagerbottom."</td></tr>");

} else
echo ("<tr><td colspan=\"5\" align=\"center\" class=\"b\">".$tracker_lang['no_data_now']."</td></tr>\n");

echo ("</table>\n");

//end_frame();
stdfoot(true);

?>