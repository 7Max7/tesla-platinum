<?
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

$returl = $_SERVER["HTTP_REFERER"];

if (isset($_POST["returnto"]))
$returl = htmlentities($_POST["returnto"]);
elseif (!empty($returl))
$returl = $returl;
else
$returl = "browse.php";

$id = (isset($_GET["id"]) ? intval($_GET["id"]):"");

if (empty($id) || !is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": id");

$updateset = array();

$res = sql_query("SELECT moderated, owner, category, moderatedby, info_hash FROM torrents WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row = mysql_fetch_array($res);

if ($row["moderated"] == "no" && !headers_sent()){
@header ("Location: ".htmlspecialchars($returl));
die;
} elseif ($row["moderated"] == "no")
die ("<script>setTimeout('document.location.href=\"".htmlspecialchars($returl)."\"', 3);</script>");

if (get_user_class() == UC_MODERATOR && !empty($CURUSER["catedit"]) && !preg_match("/\[cat".$row["category"]."\]/is", $CURUSER["catedit"]) && $CURUSER["id"] <> $row["owner"])
stderr($tracker_lang['error'], $tracker_lang['no_modcat']);

$updateset[] = "moderated = 'no'";

if (!empty($row["moderatedby"]) && $row["moderatedby"] == $CURUSER["id"] && $CURUSER["id"] == $row["owner"] && !empty($row["owner"]) && get_user_class() >= UC_VIP && get_user_class() <= UC_UPLOADER){

$updateset[] = "torrent_com = CONCAT_WS('', ".sqlesc(get_date_time() . " ".$CURUSER["username"]." ���� ��������� (��� �.�.).\n").", torrent_com)";

} elseif (get_user_class() <= UC_UPLOADER)
stderr($tracker_lang['error'], $tracker_lang['torrent_verified']);

if (get_user_class() >= UC_MODERATOR){

$updateset[] = "moderatedby = ".sqlesc($CURUSER["id"]);
$updateset[] = "torrent_com = CONCAT_WS('', ".sqlesc(get_date_time() . " ".$CURUSER["username"]." ���� ���������.\n").", torrent_com)";

}

sql_query("UPDATE torrents SET ".implode(", ", $updateset)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

unsql_cache("block-last_files"); //��� ����������� ��������, ������� ��� ����� ���������� ���
unsql_cache("premod"); // ��� ����������� ��������, ������� ��� ����� ���������� ���
unsql_cache("usermod_1");
unsql_cache("anet_".$row["info_hash"]); // ������ �������
unsql_cache("anto_".$row["info_hash"]); // ������

if (!headers_sent())
@header ("Location: ".htmlspecialchars($returl));
else
die ("<script>setTimeout('document.location.href=\"".htmlspecialchars($returl)."\"', 3);</script>");

?>