<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

if (get_user_class() <= UC_MODERATOR){
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
die();
}

$action = (string) $_GET['action'];
$id = (int) $_GET['id'];

if (!empty($action) && in_array($action, array('unban', 'truncate', 'ban', 'delete'))){

if ($action == 'ban' && get_user_class() == UC_SYSOP && !empty($id) && is_valid_id($id))
sql_query("UPDATE loginattempts SET banned = 'yes' WHERE id = ".sqlesc($id))or sqlerr(__FILE__, __LINE__);
elseif ($action == 'unban' && !empty($id) && is_valid_id($id) && get_user_class() == UC_SYSOP)
sql_query("UPDATE loginattempts SET banned = 'no' WHERE id = ".sqlesc($id))or sqlerr(__FILE__, __LINE__);
elseif ($action == 'delete' && !empty($id) && is_valid_id($id))
sql_query("DELETE FROM loginattempts WHERE id = ".sqlesc($id))or sqlerr(__FILE__, __LINE__);
elseif ($action == 'truncate' &&  get_user_class() == UC_SYSOP)
sql_query("TRUNCATE loginattempts")or sqlerr(__FILE__, __LINE__);

if (!headers_sent()){
@header ("Location: maxlogin.php");
die;
} else
die ("<script>setTimeout('document.location.href=\"maxlogin.php\"', 3);</script>");

}

stdhead($tracker_lang['tabs_maxlogin']);


echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

$count = get_row_count("loginattempts");
$perpage = 200;

list($pagertop, $pagerbottom, $limit) = pager($perpage, $count, "maxlogin.php?");

$res = sql_query("SELECT * FROM loginattempts ORDER BY id ASC ".$limit) or sqlerr(__FILE__,__LINE__);

echo "<tr><td align=\"left\" class=\"colhead\" colspan=\"6\">".$tracker_lang['tabs_maxlogin']."</td></tr>";

if (mysql_num_rows($res) == 0) {

echo "<tr><td align=\"center\" colspan=\"6\">".$tracker_lang['no_data_now']."</td></tr>";

} else {

echo "<tr><td align=\"center\" colspan=\"6\">".$pagertop."</td></tr>";

echo "<tr>
<td class=\"colhead\" width=\"1%\">#</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['user_ip']." / ".$tracker_lang['signup_username']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['clock']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['number_all']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['banned']."</td>
<td class=\"colhead\" align=\"center\">".$tracker_lang['try_inlogin']."</td>
</tr>";

while ($arr = mysql_fetch_assoc($res)) {

$nameid = $arr["comment"];
$ips = $username = array();

$r2 = sql_query("SELECT id, username, class FROM users WHERE ip = ".sqlesc($arr["ip"])) or sqlerr(__FILE__,__LINE__);
while ($a2  = mysql_fetch_assoc($r2))
$ips[] = "<a href=\"userdetails.php?id=".$a2["id"]."\">".get_user_class_color($a2["class"], $a2["username"])."</a>";

if (!empty($nameid)){
$r3 = sql_query("SELECT id, username, class FROM users WHERE id IN (".$nameid.")") or sqlerr(__FILE__,__LINE__);
while ($a3  = mysql_fetch_assoc($r3))
$username[] = "<a href=\"userdetails.php?id=".$a3["id"]."\">".get_user_class_color($a3["class"], $a3["username"])."</a> (".count(array_keys(explode(",", $nameid),$a3["id"])).")";
}

echo "<tr>";
echo "<td align=\"center\">".$arr["id"]."</td>";

echo "<td align=\"center\">";
echo $arr["ip"];

if (count($ips))
echo " <br /> (".implode(", ", $ips).")";
else
echo " <br /> (<i>".$tracker_lang['no_data']."</i>)";
echo "</td>";

echo "<td align=\"center\">".$arr["added"]." <br />".get_elapsed_time(sql_timestamp_to_unix_timestamp($arr["added"]), false, true)." ".$tracker_lang['ago']."</td>";
echo "<td align=\"center\">".$arr["attempts"]." ".$tracker_lang['time_s']."</td>";

echo "<td align=\"center\">";

if ($arr["banned"] == "yes")
echo (get_user_class() == UC_SYSOP ? "<a href=\"maxlogin.php?action=unban&id=".$arr["id"]."\"><font color=\"red\">".$tracker_lang['yes']."</font></a> ":"<font color=\"red\">".$tracker_lang['yes']."</font> ");
else
echo (get_user_class() == UC_SYSOP ? "<a href=\"maxlogin.php?action=ban&id=".$arr["id"]."\"><font color=\"green\">".$tracker_lang['no']."</font></a> ":"<font color=\"green\">".$tracker_lang['no']."</font> ");

echo "<a OnClick=\"return confirm('".$tracker_lang['warn_sure_action']."');\" href=\"maxlogin.php?action=delete&id=".$arr["id"]."\">[<b>".$tracker_lang['delete']."</b>]</a>";

echo "</td>";

echo "<td align=\"center\">".implode(", ", $username)."</td>";

echo "</tr>";

}

}

echo "<tr><td align=\"center\" colspan=\"6\">".$pagerbottom."</td></tr>";

echo "<tr><td align=\"center\" class=\"a\" colspan=\"6\">".sprintf($tracker_lang['tabs_help'], $Cleanup_Config["maxlogin_timeout"]." ".$tracker_lang['day_all']." 0", 200)."</td></tr>";

if (!empty($count) && get_user_class() == UC_SYSOP)
echo "<tr><td align=\"center\" class=\"b\" colspan=\"6\"><form method=\"get\" action=\"maxlogin.php\" name=\"add\"><input type=\"hidden\" name=\"action\" value=\"truncate\" /><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['trunc_date']."\" /></form></td></tr>";

echo "</table>";

stdfoot();
?>