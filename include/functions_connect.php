<? if(!defined('IN_TRACKER'))  die('Hacking attempt!'); 

if (!function_exists('curl_init'))
die ("���������� ���������� php ��� ��������� libcurl.so");

function dngethttp ($arurl = false, $numb_freconect = false){

/**
$arurl = array("url" => '', 'cookie' => '', 
'basic_auth' => '', 'post_data' => '',
'encoding' => 'gzip', 'header' => false,
'proxy_iport' => '', 'proxy_userpwd' => '', 
'refer' => '', 'agent' => '', 
'cached_time' => '', 'cached_file' => '', 'verbose' => false,
'count_reconn' = false,
);
*/

$a_header = array();

if ($arurl == false) return false;
$url = trim($arurl['url']);

if (isset($arurl['cached_time']) && !empty($arurl['cached_time']))
$cached = ROOT_PATH.'/torrents/txt/'.(!empty($arurl['cached_file']) ? $arurl['cached_file']:'cached_'.crc32($url).'').'.txt';


if (isset($_GET['cache_deleting'])) {
global $CURUSER;
if ($CURUSER["class"] >= UC_SYSOP AND file_exists($cached)) unlink($cached); 
unset($CURUSER);
}

if (empty($url)) return false;

$refer = trim($arurl['refer']);

/// ���� ���� ���� �������� - ��������� ��� ��� �����
if (isset($arurl['cookie']) && !empty($arurl['cookie'])) 
$rcooc = ROOT_PATH.'/torrents/txt/'.trim($arurl['cookie']).'.txt';

$u_rl = parse_url($url);

$agent = trim($arurl['agent']);


/// ���� ������������ ��� - ����� ���
if (!empty($cached) && file_exists($cached)){

if ((time() - filemtime($cached)) < $arurl['cached_time'])
$cdata = gzinflate(file_get_contents($cached));
else
unlink($cached);

}

/// ����� ����������� ������
if (empty($cdata)) {

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_USERAGENT, (!empty($agent) ? $agent:"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"));


curl_setopt($ch, CURLOPT_HEADER, (isset($arurl['header']) && $arurl['header'] == true ? true:false)); /// TRUE ��� ��������� ���������� � �����. 

if (isset($arurl['post_data']) && !empty($arurl['post_data'])){
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, trim($arurl['post_data']));
}

if (!empty($refer))
curl_setopt($ch, CURLOPT_REFERER, $refer);
else
curl_setopt($ch, CURLOPT_AUTOREFERER, true); /// TRUE ��� �������������� ��������� ���� Referer: � ��������, ���������������� ���������� Location:


/// ���� ����� ������������ ���� � ���������� �� - ���� � ������
if (isset($rcooc)){
curl_setopt($ch, CURLOPT_COOKIEJAR, $rcooc);
curl_setopt($ch, CURLOPT_COOKIEFILE, $rcooc);
}

/// �������� �������� ����������� �������� �����
if ($u_rl['scheme'] == 'https'){
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
#curl_setopt($ch, CURLOPT_SSLVERSION, 3); 
#curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, "rsa_rc4_128_sha");
}

/// � ������ ������ � ������, �������� ������ ip ���� � �����:������ (���� ����)
if (isset($arurl['proxy_iport']) && !empty($arurl['proxy_iport'])){
/*
   curl_setopt($ch, CURLOPT_PROXY, $proxy); 
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, $proxy);
   */
curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
curl_setopt($ch, CURLOPT_PROXY, trim($arurl['proxy_iport'])); /// ip ������, ������ 192.168.88.1:8080

if (isset($arurl['proxy_userpwd']) && !empty($arurl['proxy_userpwd']))
curl_setopt($ch, CURLOPT_PROXYUSERPWD, $arurl['proxy_userpwd']);  /// ����� � ������ ��� ������, ������ login:password

}

curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); /// TRUE ��� ���������� ������ ��������� "Location: ", ������������� �������� � ����� ������ (������, ��� ��� ���������� ����������, PHP ����� ��������� �� ����� ����������� ����������� "Location: ", �� ����������� ������, ����� ����������� ��������� CURLOPT_MAXREDIRS) 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); /// TRUE ��� �������� ���������� �������� � �������� ������ �� curl_exec() ������ ������� ������ � �������. 

if (isset($arurl['time_out']) && !empty($arurl['time_out'])){
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, ($numb_freconect <> false ? ($arurl['time_out']/2):$arurl['time_out']) );
curl_setopt($ch, CURLOPT_TIMEOUT, ($numb_freconect <> false ? ($arurl['time_out']/2):$arurl['time_out']) ); /// ����������� ����������� ���������� ������ ��� ���������� cURL-�������. 
} else {
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, ($numb_freconect <> false ? 5:10));
curl_setopt($ch, CURLOPT_TIMEOUT, ($numb_freconect <> false ? 5:10)); /// ����������� ����������� ���������� ������ ��� ���������� cURL-�������. 
}
//curl_setopt($ch, CURLOPT_BUFFERSIZE, 8192); /// 8192 8k

/// ��������� - ���� (����� �����, � �������� ������������)
$a_header['Host'] = $u_rl['host'];


/// ��������� - ���������� �������, � ������ �������� �� ������� ������� (������ �����)
if (isset($arurl['encoding']) && $arurl['encoding'] == 'gzip'){
$a_header['Accept-Encoding'] = 'gzip, deflate';
curl_setopt($ch, CURLOPT_ENCODING , 'gzip, deflate');
} else
curl_setopt($ch, CURLOPT_ENCODING, "deflare");


/// �����������
if (isset($arurl['basic_auth'])){
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY); //������������ HTTP ������ �����������. CURLAUTH_BASIC, CURLAUTH_DIGEST, CURLAUTH_GSSNEGOTIATE, CURLAUTH_NTLM, CURLAUTH_ANY, and CURLAUTH_ANYSAFE. 
curl_setopt($ch, CURLOPT_USERPWD, $arurl['basic_auth']); 

curl_setopt($ch, CURLOPT_UNRESTRICTED_AUTH, 1); /// ���� ��������������� ��������� - �� �������� ���� ������ � cURL
}
/// �����������


/// �������� ���������� (�������������, ���� ����������)
if (is_array($a_header) && count($a_header))
curl_setopt($ch, CURLOPT_HTTPHEADER, $a_header);



/// ������� ��������
if (isset($arurl['verbose']) && $arurl['verbose'] == true){

curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_FAILONERROR, true);

$fp_err = fopen(ROOT_PATH.'/cache/curl_verbose_file.txt', 'ab+');
fwrite($fp_err, date('Y-m-d H:i:s')."\n\n");            

curl_setopt($ch, CURLOPT_STDERR, $fp_err); 

}
/// ������� ��������



$cdata = curl_exec($ch);

if (curl_exec($ch) === false){

#echo 'Curl ������: '.curl_error($ch);

global $CURUSER, $queries, $tstart;

# ����������������
if (isset($arurl['count_reconn']) AND $numb_freconect <> false AND $arurl['count_reconn'] < $numb_freconect){

++$numb_freconect;

return dngethttp ($arurl, $numb_freconect);

}
# ����������������


} else {

if (isset($cached) && !empty($cached))
file_put_contents($cached, gzdeflate($cdata)); /// ���� ������������ ��� ���������� - ��������� � ������ � ���

}

if (preg_match("/parser\_/is", basename($_SERVER["SCRIPT_FILENAME"]))){
#echo "<br />CURL: ".print_r(curl_getinfo($ch), true)."<br />";
#echo "<br />Other: #".curl_errno($ch).", ".curl_error($ch)."<br />";
}

}

if (isset($arurl['get_info']) && $arurl['get_info'] == true)
return array('info' => curl_getinfo($ch), 'content' => (!empty($cdata) ? $cdata:false));
elseif (!empty($cdata)) return $cdata;
else
return false;

}






?>