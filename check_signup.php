<?
require_once("include/bittorrent.php");
dbconn(false);

function ajaxerr($text){
die ("<div id=\"ajaxerror\" style=\"width: 80%;\">".$text."</div>");
return;
}

function ajaxsucc($text){
die ("<div id=\"ajaxsuccess\" style=\"width: 80%;\">".$text."</div>");
return;
}

function decode_unicode_url($str) {

$res = '';

$i = 0;
$max = strlen($str) - 6;
while ($i <= $max) {
$character = $str[$i];
if ($character == '%' && $str[$i + 1] == 'u') {
$value = hexdec(substr($str, $i + 2, 4));
$i += 6;

if ($value < 0x0080)
$character = chr($value);
else if ($value < 0x0800)
$character =chr((($value & 0x07c0) >> 6) | 0xc0). chr(($value & 0x3f) | 0x80);
else
$character =chr((($value & 0xf000) >> 12) | 0xe0). chr((($value & 0x0fc0) >> 6) | 0x80). chr(($value & 0x3f) | 0x80);
} else
$i++;

$res .= $character;
}

return $res . substr($str, $i);
}

function convert_text($s) {

$out = "";

for ($i=0; $i<strlen($s); $i++) {
$c1 = substr ($s, $i, 1);
$byte1 = ord ($c1);
if ($byte1>>5 == 6){
$i++;
$c2 = substr ($s, $i, 1);
$byte2 = ord ($c2);
$byte1 &= 31;
$byte2 &= 63;
$byte2 |= (($byte1 & 3) << 6);
$byte1 >>= 2;

$word = ($byte1<<8) + $byte2;
if ($word==1025) $out .= chr(168);
elseif ($word==1105) $out .= chr(184);
elseif ($word>=0x0410 && $word<=0x044F) $out .= chr($word-848);
else  {
$a = dechex($byte1);
$a = str_pad($a, 2, "0", STR_PAD_LEFT);
$b = dechex($byte2);
$b = str_pad($b, 2, "0", STR_PAD_LEFT);
$out .= "&#x".$a.$b.";";
}
} else
$out .= $c1;
}

return $out;
}

header("Content-Type: text/html; charset=".$tracker_lang['language_charset']);

if ($_POST["action"] == "username") {

$wantname = htmlspecialchars($_POST["username"]);
$wantusername = convert_text(urldecode(decode_unicode_url($wantname)));
$wantusername = trim($wantusername);

if (!empty($wantusername) && strlen($wantusername) > 12)
ajaxerr($tracker_lang['signup_username'].": ".sprintf($tracker_lang['max_simp_of'], 12));
elseif (!validusername($wantusername) || empty($wantusername))
ajaxerr($tracker_lang['validusername']);
else
$res = mysql_fetch_row(sql_query("SELECT COUNT(*) FROM users WHERE username = ".sqlesc($wantusername)))  or sqlerr(__FILE__, __LINE__);

if (!empty($res[0]))
ajaxerr(sprintf($tracker_lang['user_is_signup'], $wantusername));
else
ajaxsucc($tracker_lang['test_of_work']);

} elseif ($_POST["action"] == "password"){

$wantpass = htmlspecialchars($_POST["password"]);

$wantpassword = convert_text(urldecode(decode_unicode_url($wantpass)));
$pagain = htmlspecialchars($_POST["passagain"]);
$passagain = convert_text(urldecode(decode_unicode_url($pagain)));

if (empty($wantpassword))
ajaxerr($tracker_lang['enter']." ".$tracker_lang['signup_password']);
elseif (empty($passagain))
ajaxerr($tracker_lang['signup_password_again']);
elseif ($wantpassword <> $passagain)
ajaxerr($tracker_lang['password_mismatch']);
elseif (strlen($wantpassword) < 7)
ajaxerr($tracker_lang['signup_password'].": ".sprintf($tracker_lang['min_simp_of'], 7));
elseif (strlen($wantpassword) > 40)
ajaxerr($tracker_lang['signup_password'].": ".sprintf($tracker_lang['max_simp_of'], 40));
else
ajaxsucc($tracker_lang['test_of_work']);

} elseif ($_POST["action"] == "email"){

$email = htmlentities($_POST["email"]);

if (empty($email) || !validemail($email))
ajaxerr($tracker_lang['validemail']);
else {

$em = explode("@", $email);
$email_2 = htmlentities($em[1]);

$rmail = get_row_count("users", "WHERE email = ".sqlesc($email));
$bmail = get_row_count("bannedemails", "WHERE email = ".sqlesc($email));

if (empty($bmail) && !empty($email_2))
$bmail = get_row_count("bannedemails", "WHERE email = ".sqlesc("@".$email_2));

if ($Signup_Config["email_rebans"] == true && check_banned_emails($email, true) == true)
$bmail = 1;

}

if (!empty($rmail))
ajaxerr(sprintf($tracker_lang['email_isex'], $email));
elseif (!empty($bmail))
ajaxerr($tracker_lang['emailbanned']);
else
ajaxsucc($tracker_lang['test_of_work']);

} elseif ($_POST["action"] == "invite" && isset($_POST["invite"])){

$invite = htmlentities($_POST["invite"]);

if (strlen($invite) <> 32 || empty($invite))
ajaxerr($tracker_lang['validinvite']);

list($inviter) = mysql_fetch_row(sql_query("SELECT inviter FROM invites WHERE invite = ".sqlesc($invite)));

if (!$inviter)
ajaxerr($tracker_lang['invalid_invite']);
else
ajaxsucc($tracker_lang['test_of_work']);

}

die;

?>