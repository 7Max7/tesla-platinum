<?
require_once("include/bittorrent.php");
dbconn(false);
loggedinorreturn();

$torrentid = (int) $_POST["torrentid"];
$rating = round($_POST["rate"]);

if (!empty($torrentid) && is_valid_id($torrentid) && isset($_POST["ajax"])) {

$chk = mysql_fetch_array(sql_query("SELECT torrent FROM ratings WHERE user = ".sqlesc($CURUSER["id"])." AND torrent = ".sqlesc($torrentid)." LIMIT 1"));

if ($chk) die;

sql_query("INSERT INTO ratings (torrent, user, rating, added) VALUES (".sqlesc($torrentid).", ".sqlesc($CURUSER["id"]).", ".sqlesc($rating).", ".sqlesc(get_date_time()).")") or sqlerr(__FILE__, __LINE__);

sql_query("UPDATE torrents SET numratings = numratings + 1, ratingsum = ratingsum + ".sqlesc($rating)." WHERE id = ".sqlesc($torrentid)) or sqlerr(__FILE__, __LINE__);

sql_query("UPDATE users SET unmark = unmark-1 WHERE id = ".sqlesc($CURUSER["id"])) or sqlerr(__FILE__, __LINE__);

unsql_cache("arrid_".$CURUSER["id"]);
unsql_cache("ratings_".$torrentid);

} else die;
?>