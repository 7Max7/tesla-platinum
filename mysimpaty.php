<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();

if (!isset($_GET["id"]))
$userid = $CURUSER["id"];
elseif (get_user_class() < UC_MODERATOR && $_GET["id"] <> $CURUSER["id"])
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
elseif (get_user_class() >= UC_MODERATOR || $_GET["id"] == $CURUSER["id"])
$userid = (int) $_GET["id"];


if (isset($_GET['action']) && $_GET['action'] == 'delete') {

if (get_user_class() < UC_SYSOP)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$respect_id = intval(isset($_GET['r_id']) ? $_GET['r_id']:0);
$respect_type = (string) (isset($_GET['r_type']) ? $_GET['r_type']:'');
$touserid = intval(isset($_GET['tid']) ? $_GET['tid']:0);

if (is_valid_id($respect_id) && in_array($respect_type, array("bad", "good")) && is_valid_id($touserid)){
sql_query ("DELETE FROM simpaty WHERE id = ".sqlesc($respect_id)) or sqlerr(__LINE__,__FILE__);
sql_query ("UPDATE users SET simpaty = simpaty ".($respect_type == 'bad' ? "+1":"-1")." WHERE id = ".sqlesc($touserid)) or sqlerr(__LINE__,__FILE__);
unsql_cache("arrid_".$touserid);
}

if ($CURUSER["id"] <> $touserid && !empty($touserid))
header("Refresh: 0; url=mysimpaty.php?id=".$touserid);
else
header("Refresh: 0; url=mysimpaty.php");
die;

}

if (!is_valid_id($userid))
stderr($tracker_lang['respect_my'], $tracker_lang['invalid_id']);

stdhead($tracker_lang['respect_my']);

$count = get_row_count("simpaty", "WHERE touserid = ".sqlesc($userid));

if (!isset($_GET["page"]) && $userid <> $CURUSER["id"]){
sql_query("UPDATE users SET simpaty = ".sqlesc($count)." WHERE id = ".sqlesc($userid)) or sqlerr(__LINE__,__FILE__);
unsql_cache("arrid_".$userid);
}

if (empty($count))
stderr($tracker_lang['respect_my'], $tracker_lang['no_data_now']);

echo "<table class=\"embedded\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";

echo "<tr><td class=\"colhead\" align=\"center\" colspan=\"5\">".$tracker_lang['respect_my']."</td></tr>";

list ($pagertop, $pagerbottom, $limit) = pager(30, $count, "mysimpaty.php?".(!empty($userid) ? "id=".$userid."&":""), array("lastpagedefault" => 0));

echo("<tr><td colspan=\"5\">".$pagertop."</td></tr>");

echo "<tr>";

echo "
<td class=\"colhead\" align=\"center\">#</td>
<td class=\"colhead align=\"center\">".$tracker_lang['type']."</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['sender']." / ".$tracker_lang['clock']."</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['torrent']." / ".$tracker_lang['description']."</td>
";

if (get_user_class() >= UC_ADMINISTRATOR)
echo "<td class=\"colhead\">".$tracker_lang['action']."</td>";

echo "</tr>";

$res = sql_query("SELECT s.*, u.class, u.username, t.name
FROM simpaty AS s
LEFT JOIN torrents AS t ON t.id = s.type
LEFT JOIN users AS u ON u.id = s.fromuserid
WHERE s.touserid = ".sqlesc($userid)." ORDER BY s.id DESC ".$limit) or sqlerr(__FILE__, __LINE__);

while ($arr = mysql_fetch_assoc($res)) {

if ($i%2 == 0){
$cl1 = "class=\"b\"";
$cl2 = "class=\"a\"";
} else {
$cl2 = "class=\"b\"";
$cl1 = "class=\"a\"";
}

++$i;

$respect_type = ($arr["bad"] == 1 ? "bad":"good");

$number = $i + (30 * (int) $_GET["page"]);

echo "<tr>";

echo "<td ".$cl1." align=\"center\">".$number."</td>";
echo "<td ".$cl2." align=\"center\">".($arr["good"] == 1 ? "<img src=\"pic/thum_good.gif\" alt=\"".$tracker_lang['respect']."\" title=\"".$tracker_lang['respect']."\">":"<img src=\"pic/thum_bad.gif\" alt=\"".$tracker_lang['antirespect']."\" title=\"".$tracker_lang['antirespect']."\">")."</td>";
echo "<td ".$cl1." align=\"left\">
".(!empty($arr["username"]) ? "<a href=\"userdetails.php?id=".$arr["fromuserid"]."\">".get_user_class_color($arr["class"], $arr["username"])."</a>":$tracker_lang['anonymous'])."
<br /><a href=\"browse.php?date=".current(explode(" ", $arr["respect_time"]))."\">".$arr["respect_time"]."</a>
</td>";
echo "<td ".$cl2." align=\"left\">".(empty($arr["name"]) ? $tracker_lang['no_torrent_with_such_id']:"<a href=\"details.php?id=".$arr["type"]."\">".htmlspecialchars($arr["name"])."</a>")."<br />".format_comment($arr["description"])."</td>";

if (get_user_class() >= UC_ADMINISTRATOR)
echo "<td ".$cl1." ><a title=\"".$tracker_lang['delete']."\" href=\"mysimpaty.php?action=delete&r_id=".$arr["id"]."&tid=".$arr["touserid"]."&r_type=".$respect_type."\">".$tracker_lang['delete']."</a></td>";

echo "</tr>";

}

echo "<tr><td colspan=\"5\">".$pagerbottom."</td></tr>";

echo "</table>";

stdfoot();

?>