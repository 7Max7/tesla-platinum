<?

if (!defined('BLOCK_FILE')) {   
 Header("Location: ../index.php");   
 exit;   
}


global $CURUSER, $tracker_lang;

$blocktitle = $tracker_lang['mematch'];

$res = sql_query("SELECT my_search.*, (SELECT COUNT(*) FROM my_match WHERE my_match.mysearch = my_search.id) AS number FROM my_search WHERE userid = ".sqlesc($CURUSER["id"])." ORDER BY number DESC LIMIT 10", $cache = array("type" => "disk", "file" => "block_mematch_".$CURUSER["id"], "time" => 60)) or sqlerr(__FILE__, __LINE__);

$content = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";

if (mysql_num_rows_($res) == 0)
$content.= "<tr><td class=\"b\">".sprintf($tracker_lang['mem_nodata'], "<a class=\"alink\" title=\"".$tracker_lang['mem_addwords']."\" href=\"mematch.php?action\">".$tracker_lang['mem_addwords']."</a>")."</td></tr>";

else {

$i = 0;
while ($arr = mysql_fetch_assoc_($res)) {

$number = $arr['number'];

$content.= "<tr><td ".($i%2==0 ? "class=\"b\"":"class=\"a\"")."><a title=\"".$tracker_lang['search_results_for']." ".htmlspecialchars($arr['words'])."\" class=\"alink\" href=\"mematch.php?mymatch=".$arr["id"]."\">".$arr['words']." ".(!empty($number) ? "(".$number.")":"")."</a></td></tr>";

++$i;
}

}

$content.= "</table>";

?>