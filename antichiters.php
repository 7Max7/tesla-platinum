<?
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();

if (get_user_class() < UC_MODERATOR)
stderr($tracker_lang['traffic_tracker'], $tracker_lang['access_denied']);

$cheaters = sql_query("SELECT p.torrent AS tid, t.name AS tname, p.ip, p.port, s.uploaded, s.downloaded, s.to_go, p.seeder, p.agent, p.peer_id, p.userid, u.username, u.class, u.enabled, u.warned, u.donor, (p.uploadoffset / (UNIX_TIMESTAMP(p.last_action) - UNIX_TIMESTAMP(p.prev_action))) AS upspeed, (p.downloadoffset / (UNIX_TIMESTAMP(p.last_action) - UNIX_TIMESTAMP(p.prev_action))) AS downspeed
FROM peers AS p
INNER JOIN users AS u ON u.id = p.userid
INNER JOIN torrents AS t ON t.id = p.torrent
INNER JOIN snatched AS s ON (s.userid = p.userid AND s.torrent = p.torrent)
WHERE p.uploadoffset > '0' ORDER BY p.uploadoffset DESC LIMIT 200") or sqlerr(__FILE__,__LINE__);

if (mysql_num_rows($cheaters) == 0)
stderr($tracker_lang['traffic_tracker'], $tracker_lang['no_data_now']);

stdhead($tracker_lang['traffic_tracker']);

echo "<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">";

echo "<tr>
<td class=\"colhead\">".$tracker_lang['signup_username']."</td>
<td class=\"colhead\">".$tracker_lang['torrent']."</td>
<td class=\"colhead\">".$tracker_lang['uploaded']."</td>
<td class=\"colhead\">".$tracker_lang['downloaded']."</td>
<td class=\"colhead\">".$tracker_lang['left']."</td>
<td class=\"colhead\">".$tracker_lang['uploadpos']."</td>
<td class=\"colhead\">".$tracker_lang['downloadpos']."</td>
<td class=\"colhead\">".$tracker_lang['uploadeder']."</td>
".(get_user_class() > UC_MODERATOR ? "<td class=\"colhead\">".$tracker_lang['port']."</td>":"")."
<td class=\"colhead\">".$tracker_lang['client']."</td>
</tr>";

$num = 0;
while ($cheater = mysql_fetch_array($cheaters)) {

if ($num%2==0){
$cl1 = "class = \"b\"";
$cl2 = "class = \"a\"";
} else {
$cl2 = "class = \"b\"";
$cl1 = "class = \"a\"";
}

list ($tid, $tname, $ip, $port, $uploaded, $downloaded, $left, $seeder, $agent, $peer_id, $userid, $username, $class, $enabled, $warned, $donor, $upspeed, $downspeed) = $cheater;
list ($uploaded, $downloaded, $left, $upspeed, $downspeed) = array_map("mksize", array($uploaded, $downloaded, $left, $upspeed, $downspeed));

if (strlen($tname) > 128)
$tname = substr($tname, 0, 128)."...";

echo "<tr>
<td ".$cl1."><nobr><a href=\"userdetails.php?id=".$userid."\">".get_user_class_color($class, $username)."</a> ".get_user_icons(array("enabled" => $enabled, "donor" => $donor, "warned" => $warned))."</nobr></td>
<td ".$cl2."><a href=\"details.php?id=".$tid."\">".$tname."</a></td>

<td ".$cl1."><nobr>".$uploaded."</nobr></td>
<td ".$cl2."><nobr>".$downloaded."</nobr></td>
<td ".$cl1."><nobr>".$left."</nobr></td>
<td ".$cl2."><nobr>".$upspeed." /".$tracker_lang['sec']."</nobr></td>
<td ".$cl1."><nobr>".$downspeed." /".$tracker_lang['sec']."</nobr></td>
<td align=\"center\">".($seeder == "yes" ? "<span style=\"color: green\">".$tracker_lang['yes']."</span>":"<span style=\"color: red\">".$tracker_lang['no']."</span>")."</td>
".(get_user_class() > UC_MODERATOR ? "<td align=\"center\">".(empty($port) ? "---":$port)."</td>":"")."
<td ".$cl2.">".substr($peer_id, 0, 7)."</td>
</tr>";
++$num;
}

echo "</table>";

stdfoot();
?>