<?
require_once("include/bittorrent.php");
dbconn();

if (!mkglobal("type"))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": type");

if ($type == "signup") {
    
mkglobal("email");
if (!validemail($email)) unset($email);

stdhead($tracker_lang['account_activated']);
stdmsg($tracker_lang['signup_successful'], ($Signup_Config["use_email_act"] == true ? sprintf($tracker_lang['confirmation_mail_sent'], htmlspecialchars($email)) : sprintf($tracker_lang['thanks_for_registering'], $SITENAME)));
stdfoot();

} elseif ($type == "confirmed" || $type == "confirm") {

stdhead($tracker_lang['account_activated']);
stdmsg($tracker_lang['account_activated'], $tracker_lang['activated']);
stdfoot();

} else {

header("Location: index.php");
die;

}

?>