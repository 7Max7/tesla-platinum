<?
if (!defined('BLOCK_FILE')) {
 Header("Location: ../index.php");
 exit;
}

global $tracker_lang;

$blocktitle = $tracker_lang['cloud_tags'];


function tag_info_restore() {

///////// cache
$result = sql_query("SELECT DISTINCT ts.name, (SELECT sum(howmuch) FROM tags WHERE name=ts.name) AS howmuch FROM tags AS ts WHERE ts.howmuch > 0 ORDER BY ts.howmuch DESC LIMIT 80", $cache = array("type" => "disk", "file" => "block_cloudstags_restore", "time" => 3600*72)) or sqlerr(__FILE__, __LINE__);
///////// cache

while($row = mysql_fetch_assoc_($result))
$arr[$row['name']] = $row['howmuch'];

@ksort($arr);
return $arr;
}


function cloud2() {

global $tracker_lang;

//min / max font sizes
$small = 10;
$big = 32;
//get tag info from worker function
$tags = tag_info_restore();
//amounts
$minimum_count = @min(array_values($tags));
$maximum_count = @max(array_values($tags));
$spread = $maximum_count - $minimum_count;

if($spread == 0) {$spread = 1;}

$cloud_html = '';

$cloud_tags = array();

foreach ($tags as $tag => $count) {

$size = $small + ($count - $minimum_count) * ($big - $small) / $spread;
//set up colour array for font colours.
$colour_array = array('#003EFF', '#0000FF', '#7EB6FF', '#0099CC', '#62B1F6');
//spew out some html malarky!
$cloud_tags[] = '<a style="font-weight:normal; color:'.$colour_array[mt_rand(0, 5)].'; font-size: '.floor($size).'px; " class="tag_cloud" href="browse.php?tag='.urlencode($tag).'" title="'.$tracker_lang['torrents'].': '.$count.'">'.htmlentities($tag, ENT_QUOTES, "cp1251").'</a> ('.number_format($count).')';

}

$cloud_html = implode("\n", $cloud_tags)."\n";

return $cloud_html;
}

$content = '
<style type="text/css">
.tag_cloud{padding: 3px; text-decoration: none;font-family: verdana; }
.tag_cloud:link { color: #0099FF; text-decoration:none;border:1px transparent solid;}
.tag_cloud:visited { color: #00CCFF; border:1px transparent solid;}
.tag_cloud:hover { color: #0000FF; background: #ddd;border:1px #bbb solid; }
.tag_cloud:active { color: #0000FF; background: #fff; border:1px transparent solid;}
#tag{line-height:28px;font-family:Verdana, Arial, Helvetica, sans-serif;text-align:justify;}
</style>';

$content .= '<div id="wrapper">';
$content .= '<p id="tag">'.cloud2().'</p>';
$content .= '</div>';

?>