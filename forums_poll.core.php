<?
require_once("include/bittorrent.php");
dbconn(false,true);

header("Content-Type: text/html; charset=" .$tracker_lang['language_charset']);
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$do = (isset($_POST["action"]) ? (string) $_POST["action"] : "load");
$choice = (isset($_POST["choice"]) ? (int) $_POST["choice"] : 0);
$pollId = (isset($_POST["pollId"]) ? (int) $_POST["pollId"] : 0);

$id_topic = (isset($_POST["id"]) ? (int) $_POST["id"] : 0 );

$results = $votes = array();

if (!in_array($do, array("load", "vote")) || !$CURUSER)
die($tracker_lang['loginid']);

if ($do == "load") {

$r_check = sql_query("SELECT p.id,p.added,p.question,pa.selection,pa.userid
FROM polls AS p
LEFT JOIN pollanswers AS pa ON p.id = pa.pollid AND pa.userid = ".sqlesc($CURUSER["id"])." AND pa.forum = ".sqlesc($id_topic)."
WHERE p.forum = ".sqlesc($id_topic)." ORDER BY p.id DESC LIMIT 1") or sqlerr(__FILE__, __LINE__);

$ar_check = mysql_fetch_assoc($r_check);
if (mysql_num_rows($r_check) == 1) {

$r_op = sql_query("SELECT * FROM polls WHERE forum = ".sqlesc($id_topic)." AND id = ".$ar_check["id"], $cache = array("type" => "disk", "file" => "forums_ptop-".$id_topic, "time" => 180))  or sqlerr(__FILE__, __LINE__);

$a_op = mysql_fetch_assoc_($r_op);

if (empty($a_op["question"]) || empty($a_op["option0"]) || empty($a_op["option1"])){
sql_query("DELETE FROM polls WHERE forum = ".sqlesc($id_topic)) or sqlerr(__FILE__, __LINE__);
sql_query("DELETE FROM pollanswers WHERE forum = ".sqlesc($id_topic)) or sqlerr(__FILE__, __LINE__);
sql_query("UPDATE topics SET polls = 'no' WHERE id = ".sqlesc($id_topic)) or sqlerr(__FILE__, __LINE__);
}

for ($i=0; $i<20; $i++){
if (!empty($a_op['option'.$i]))
$options[$i] = format_comment($a_op['option'.$i]);
}

if (empty($ar_check["userid"])){

echo("<div id=\"poll_title\">".format_comment($ar_check["question"])."</div>\n");

foreach($options as $op_id=>$op_val){
echo("<div align=\"left\"><input type=\"hidden\" value=\"".$id_topic."\" name=\"id\" id=\"id\"/><input type=\"radio\" onclick=\"addvote(".$op_id.")\" name=\"choices\" value=\"".$op_id."\" id=\"opt_".$op_id."\" /><label for=\"opt_".$op_id."\">&nbsp;".$op_val."</label></div>\n");
}

echo("<div align=\"left\"><input type=\"radio\" onclick=\"addvote(255)\" name=\"choices\" value=\"255\" id=\"opt_255\" /><label for=\"opt_255\">&nbsp;".$tracker_lang['blank_vote']."</label></div>\n");

echo("<input type=\"hidden\" value=\"\" name=\"choice\" id=\"choice\"/>");
echo("<input type=\"hidden\" value=\"".$ar_check["id"]."\" name=\"pollId\" id=\"pollId\"/>");

echo("<div align=\"center\"><input type=\"button\" value=\"".$tracker_lang['vote']."\" style=\"display:none;\" id=\"vote_b\" onclick=\"vote();\"/></div>");

} else {

$r = sql_query("SELECT count(id) as count, selection  FROM pollanswers WHERE pollid = ".sqlesc($ar_check["id"])." AND selection < 20 AND forum = ".sqlesc($id_topic)." GROUP BY selection") or sqlerr(__FILE__, __LINE__);

while($a = mysql_fetch_assoc($r)){
$total += $a["count"];
$votes[$a["selection"]] = $a["count"];
}

if ($total==0) $total = 1;

foreach($options as $k=>$op){
$results[] = array($votes[$k],$op);
}

function srt($a,$b){
if ($a[0] > $b[0]) return -1;
if ($a[0] < $b[0]) return 1;
return 0;
}

usort($results, srt);

echo("<div id=\"poll_title\">".format_comment($ar_check["question"])."</div>\n");

echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border:none\" id=\"results\" class=\"results\">");
$i= 0;
foreach($results as $result){
echo("<tr>
<td align=\"left\" width=\"40%\" style=\"border:none;\">".$result[1]."</td>
<td style=\"border:none;\" align=\"left\" width=\"60%\" valing=\"middle\">
<div class=\"bar".($i == 0 ? "max" : "")."\"  name=\"".($result[0] / $total * 100)."\" id=\"poll_result\">&nbsp;</div>
</td>
<td style=\"border:none;\">&nbsp;<b>".number_format(($result[0] / $total * 100),2)."%</b></td>
</tr>\n");
$i++;
}
echo("</table>");

echo("<div align=\"center\"><b>".$tracker_lang['votes']."</b>: ".$total."</div>");
}

} elseif (mysql_num_rows($r_check) == 0) echo $tracker_lang['no_polls'];

} elseif ($do == "vote" && !empty($pollId)) {

$check = mysql_result(mysql_query("SELECT count(id) FROM pollanswers WHERE pollid = ".sqlesc($pollId)." AND userid = ".sqlesc($CURUSER["id"])." AND forum = ".sqlesc($id_topic)));

if ($check == 0) {
mysql_query("INSERT INTO pollanswers VALUES(0, ".sqlesc($pollId).", ".sqlesc($CURUSER["id"]).", ".sqlesc($choice).", ".sqlesc($id_topic).")");

if (mysql_affected_rows() == 1)
echo(json_encode(array("status" => 1)));

}

}

?>