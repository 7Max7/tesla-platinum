<?
require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();


if (get_user_class() < UC_MODERATOR) {
stderr($tracker_lang['error'], $tracker_lang['access_denied']);
stdfoot();
die();
}

$add_links = true; //// true ��� false - �������� ��� ��������� ��������� ������ � ������������� (ip � ����), ���������


if (isset($_POST["delete"]) && $_POST["delete"] == "delete" && is_array($_POST["remove"]) && get_user_class() == UC_SYSOP) {

sql_query("DELETE FROM sitelog WHERE id IN (".implode(", ", array_map("sqlesc", $_POST["remove"])).")") or sqlerr(__FILE__, __LINE__);

write_log("�� �������, ������� ���������� ������ (".count($_POST["remove"])." ��.) ������������� <b>".$CURUSER['username']."</b>", "d511ff", $type_clear);

if (isset($_POST['redirect']))
header("Location: ".$_POST['redirect']);
else
header("Location: log.php");
die;

}


stdhead($tracker_lang['log']);

$array_type = array();

if (get_user_class() == UC_SYSOP)
$array_type['all'] = $tracker_lang['all'];

$array_type['tracker'] = $tracker_lang['my_website'];
$array_type['torrent'] = $tracker_lang['torrents'];
$array_type['tfiles'] = $tracker_lang['dox'];

if (get_user_class() >= UC_ADMINISTRATOR)
$array_type['forum'] = $tracker_lang['forum'];

$array_type['bonus'] = $tracker_lang['bonuses'];

if (get_user_class() == UC_SYSOP){
$array_type['bans'] = $tracker_lang['bans'];

$array_type['comment'] = $tracker_lang['comment'];

if ($Torrent_Config["on_search_log"] == true)
$array_type['search'] = $tracker_lang['searchcloud'];

$array_type['error'] = $tracker_lang['from_system'];
$array_type['other'] = $tracker_lang['other'];
$array_type['founds'] = $tracker_lang['search'];
}


$type_clear = (isset($_GET["type_clear"]) ? (string) $_GET["type_clear"] : false);

if (!empty($type_clear) && get_user_class() == UC_SYSOP && !in_array($type_clear, array('all', 'founds'))) {

$count = get_row_count("sitelog", "WHERE type = ".sqlesc($type_clear));

sql_query("DELETE FROM sitelog WHERE type = ".sqlesc($type_clear)) or sqlerr(__FILE__, __LINE__);

unsql_cache("log_sum".$type_clear);

write_log("�� �������, ������� ��� ������ (".$count." ��.) �� ������ ".$array_type[$type_clear].", ������������� <b>".$CURUSER['username']."</b>", "d511ff", $type_clear);
}


$type = (isset($_GET["type"]) ? (string) $_GET["type"] : "all");

if (get_user_class() < UC_SYSOP && $type == 'all' && empty($_GET["type"])) $type = 'tracker';

if (!empty($type_clear)) $type = $type_clear;

$result = sql_query("SHOW TABLE STATUS LIKE 'sitelog'") or sqlerr(__FILE__,__LINE__);
$rowzie = mysql_fetch_assoc($result);

$allsize = $rowzie["Index_length"]+$rowzie["Data_length"];

if (!in_array($type, array('all', 'founds'))){
$resu = sql_query("SELECT sum(LENGTH(txt)) AS size FROM sitelog WHERE type = ".sqlesc($type), $cache = array("type" => "disk", "file" => "log_sum".$type, "time" => 86400)) or sqlerr(__FILE__,__LINE__);
$rowsrd = mysql_fetch_assoc_($resu);
} else
$rowsrd["size"] = $allsize;


echo "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
echo "<tr><td class=\"a\"><strong>".$tracker_lang['all_size']."</strong>: ".mksize($allsize)." (sitelog)</td>";

if ($type <> 'founds')
echo "<td class=\"a\"><strong>".$tracker_lang['size']." - ".$array_type[$type]."</strong>: ".mksize($rowsrd["size"])."</td></tr>";

echo "<tr><td class=\"b\" colspan=\"2\">";
echo "<div id=\"tabs\">\n";
foreach ($array_type AS $act => $text){
echo "<span onClick=\"document.location.href='log.php?type=".$act."'\" class=\"tab ".($type == $act ? "active":"")."\"><nobr>".$text."</nobr></span>\n";
$last = $act;
}

if (get_user_class() == UC_SYSOP && isset($resu) && !empty($type))
echo "<span onClick=\"document.location.href='log.php?type_clear=".$type."'\" class=\"tab ".($type_clear == $act ? "active":"")."\"><nobr>".$tracker_lang['trunc_date'].": ".$array_type[$type]."</nobr></span>";

echo "</div>";
echo "</td></tr>";
echo "</table>";


if (!isset($array_type[$type])) {
stdmsg($tracker_lang['error'], $tracker_lang['access_denied']);
stdfoot();
die();
}

//sql_query("DELETE FROM sitelog WHERE ".gmtime()." - UNIX_TIMESTAMP(added) > ".sqlesc(62* 86400)." and type ='tracker'") or sqlerr(__FILE__, __LINE__);

if (isset($_GET["words"]) && !empty($_GET["words"]) && $type == 'founds' && strlen($_GET["words"]) < 30) {

$words = htmlspecialchars_uni($_GET["words"]);

$words = str_replace('script', '', $words);
$words = str_replace('js', '', $words);
$words = str_replace('src=', '', $words);
$words = str_replace('"', '', $words);

$where = "WHERE txt LIKE '%$words%'";
} elseif ($type <> "all")
$where = "WHERE type = ".sqlesc($type);
else
$where = "";

$count = get_row_count("sitelog", $where);

list ($pagertop, $pagerbottom, $limit) = pager(200, $count, "log.php?type=".$type."&".(!empty($words) ? "&words=".$words."&":""));

$res = sql_query("SELECT * FROM sitelog ".$where." ORDER BY `added` DESC ".$limit) or sqlerr(__FILE__, __LINE__);


if ($type == 'founds')
echo (isset($words) ? "<h3><b>".$tracker_lang['arr_searched']."</b>: ".$words."</h3>":"");

if (mysql_num_rows($res) == 0) {

if ($type == 'founds') {

echo ("<h3>".$tracker_lang['no_data']."</h3><br />\n");

echo  "<table align=\"center\" valign=\"top\" cellpadding=\"0\">
<tr><td class=\"b\"><form method=\"get\" action=\"log.php\"><input type=\"hidden\" name=\"type\" value=\"founds\" /><input type=\"text\" name=\"words\" size=\"50\" /> <input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['search']."\" />
</form></td></tr>
</table>";

} else

echo ("<h3>".$tracker_lang['no_data']."</h3><br />\n");

} else {

echo ("<table border=\"0\" cellspacing=\"0\" width=\"100%\" cellpadding=\"5\">\n");

if (get_user_class() == UC_SYSOP) {

echo ("<script language=\"JavaScript\" type=\"text/javascript\">
var checkflag = \"false\";
function check2(field) {if (checkflag == \"false\") {for (i = 0; i < field.length; i++) {field[i].checked = true;}checkflag = \"true\";return \"".$tracker_lang['mark_all']."\"; }else {for (i = 0; i < field.length; i++) {field[i].checked = false; }checkflag = \"false\";return \"".$tracker_lang['mark_all']."\"; }}
</script>
<form action=\"log.php\" method=\"post\">\n");
}

echo ("<tr>
<td class=\"colhead\" align=\"left\" width=\"12%\">".$tracker_lang['clock']."</td>
<td class=\"colhead\" align=\"left\">".$tracker_lang['action']."</td>
".(get_user_class() == UC_SYSOP ? "<td class=\"colhead\" width=\"2%\" align=\"center\"><input title=\"".$tracker_lang['mark_all']."\" type=\"checkbox\" value=\"".$tracker_lang['mark_all']."\" onclick=\"this.value=check2(this.form.elements['remove[]'])\"/></td>":"")."
</tr>");


if ($count > mysql_num_rows($res))
echo ("<tr><td colspan=\"4\">".$pagertop."</td></tr>");


while ($arr = mysql_fetch_assoc($res)) {

$text = strip_tags($arr["txt"]);

$color_tr = trim($arr["color"]);
if (!preg_match('/^#/', $color_tr)) $color_tr = "#".$color_tr;


if ($add_links == true){
$text = preg_replace('/([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})\s/is', "[url=usersearch.php?ip=\\1.\\2.\\3.\\4]\\1.\\2.\\3.\\4[/url]", $text, 1);
$text = preg_replace('/\(([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})\)/is', "([url=usersearch.php?ip=\\1.\\2.\\3.\\4]\\1.\\2.\\3.\\4[/url])", $text, 1);
$text = preg_replace('/����� ������������\s(.*?)$/is', "����� ������������ [url=usersearch.php?n=\\1]\\1[/url]", $text, 1);
$text = preg_replace('/^([A-z�-�0-9]{1,15})\s����� �����/is', " [url=usersearch.php?n=\\1]\\1[/url] ����� �����", $text, 1);

$text = preg_replace('/����� �����\s\((.*?)\)/is', "����� ����� ([url=browse.php?search=\\1]\\1[/url])", $text, 1);
$text = preg_replace('/([0-9]{1,11})\s\((.*?)\)\s���\s�����/is', "\\1 ([url=details.php?id=\\1]\\2[/url]) ��� �����", $text, 1);
$text = preg_replace('/([0-9]{1,11})\s\((.*?)\)\s���\s������/is', "\\1 ([url=details.php?id=\\1]\\2[/url]) ��� ������", $text, 1);

$text = preg_replace('/���\s�����\s\(zip\s�����\)\s(.*?)$/i', "��� ����� (zip �����) [url=usersearch.php?n=\\1]\\1[/url]", $text, 1);
//$text = preg_replace('/���\s�����\s(.*?)$/i', "��� ����� [url=usersearch.php?n=\\1]\\1[/url]", $text, 1);
}



echo  ("<tr style=\"color: ".$color_tr."\">
<td width=\"5%\">".$arr['added']."</td>
<td align=left width=\"90%\">".format_comment($text)."</td>
".(get_user_class() == UC_SYSOP ? "<td align=\"center\"><input type=\"checkbox\" name=\"remove[]\" value=\"".$arr["id"]."\" id=\"checkbox_tbl_".$arr['id']."\"/></td>":"")."
</tr>");

}

if ($count > mysql_num_rows($res))
echo ("<tr><td colspan=\"4\">".$pagerbottom."</td></tr>");

echo ("</table>");

if (get_user_class() == UC_SYSOP)
echo ("<table align=\"center\" valign=\"top\" cellpadding=\"0\" width=\"100%\"><tr><td class=\"b\" align=\"center\"><input type=\"hidden\" name=\"redirect\" value=\"".strip_tags($_SERVER['REQUEST_URI'])."\" /> <input type=\"hidden\" name=\"delete\" value=\"delete\" /><input style=\"height: 25px; width:200px\" class=\"btn\" type=\"submit\" name=\"submit\" value=\"".$tracker_lang['delete']."\" /></td></tr></form></table>");

}
stdfoot();
?>