<?
require_once("include/bittorrent.php");

dbconn();
loggedinorreturn();

if (isset($_POST["torrentid"])){

$torrentid = (!empty($_POST["torrentid"]) ? intval($_POST["torrentid"]):"");
if (!is_valid_id($torrentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id'].": ".$tracker_lang['invalid_id']);

$motive = (!empty($_POST["motive"]) ? htmlspecialchars_uni($_POST["motive"]):"");
if (empty($motive))
stderr($tracker_lang['error'], $tracker_lang['invalid_id'].": ".$tracker_lang['reason']);

$owntarr = get_row_count("torrents", "WHERE id = ".sqlesc($torrentid)." AND owner = ".sqlesc($CURUSER["id"]));

if (!empty($owntarr)){
header("Location: details.php?id=".$torrentid."&ownreport");
die();
}

$already = get_row_count("report", "WHERE torrentid = ".sqlesc($torrentid)." AND userid = ".sqlesc($CURUSER["id"]));

if (empty($already)) {

sql_query("INSERT INTO report (torrentid, userid, motive, added) VALUES (".sqlesc($torrentid).", ".sqlesc($CURUSER["id"]).", ".sqlesc(trim($motive)).", ".sqlesc(get_date_time()).")") or sqlerr(__FILE__,__LINE__);

for ($x = UC_MODERATOR; $x <= UC_SYSOP; $x++){
unsql_cache("block-class_".$x);
}

header("Location: details.php?id=".$torrentid."&report");
die();

} else {

header("Location: details.php?id=".$torrentid."&alreadyreport");
die();

}

} elseif (isset($_POST["usertid"])) {

$id = (!empty($_POST["usertid"]) ? intval($_POST["usertid"]):"");

if (!is_valid_id($id))
stderr($tracker_lang['error'], $tracker_lang['invalid_id'].": ".$tracker_lang['invalid_id']);

if ($id == $CURUSER["id"])
stderr($tracker_lang['error'], $tracker_lang['complaint_owner']);

$motive = (!empty($_POST["motive"]) ? htmlspecialchars_uni($_POST["motive"]):"");
if (empty($motive))
stderr($tracker_lang['error'], $tracker_lang['invalid_id'].": ".$tracker_lang['reason']);

$res1 = sql_query("SELECT username, class,enabled FROM users WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);
$row1 = mysql_fetch_array($res1);

if (empty($row1["username"]))
stderr($tracker_lang['error'], $tracker_lang['no_user_intable']);

if ($row1["class"] >= UC_ADMINISTRATOR)
stderr($tracker_lang['staff'], $tracker_lang['access_denied']);

if ($row1["enabled"] == "no")
stderr($tracker_lang['error'], $tracker_lang['user_off']);

$anumb = get_row_count("report", "WHERE torrentid = '0' AND userid = ".sqlesc($CURUSER["id"]));
if (!empty($anumb)) {
header("Refresh: 15; url=userdetails.php?id=".$id."&alreadyreport");
die;
}

sql_query("INSERT INTO report (usertid, userid, motive, added) VALUES (".sqlesc($id).", ".sqlesc($userid).", ".sqlesc(trim($motive)).", ".sqlesc(get_date_time()).")") or sqlerr(__FILE__,__LINE__);

for ($x = UC_MODERATOR; $x <= UC_SYSOP; $x++){
unsql_cache("block-class_".$x);
}

header("Location: userdetails.php?id=".$id."&report");
die;

}

?>