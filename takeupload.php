<?
require_once("include/bittorrent.php");
require_once("include/benc.php");

//ini_set("upload_max_filesize", $Torrent_Config["max_torrent_size"]);

// Call to undefined function mb_convert_case() - ������� ���������� posix.so � mbstring.so

dbconn();
loggedinorreturn();
parked();


if (get_user_class() < UC_UPLOADER && get_user_class() <> UC_VIP || $CURUSER["uploadpos"] == 'no')
stderr($tracker_lang['error_data'], $tracker_lang['ban_uploadpos']);

foreach (explode(":","descr:type:name") as $v) {
if (!isset($_POST[$v]))
stderr($tracker_lang['error'], $tracker_lang['invalid_id_value'].": ".$v);
}

$replace = array(", ", " , ", " ,");

$post_tags = str_replace(".", " ",  str_replace("/", ",", $_POST["tags"]));

$tags = trim(str_replace($replace, ",", mb_convert_case($post_tags, MB_CASE_TITLE, $tracker_lang['language_charset'])));
$tags = tolower(htmlspecialchars_uni($tags));

$f = $_FILES["tfile"];
$fname = trim($f["name"]);

if (!isset($f) || empty($fname) || !validfilename($fname) || !preg_match('/^(.+)\.torrent$/si', $fname, $matches))
stderr($tracker_lang['error'], $tracker_lang['no_upload_torrent']);

$descr = trim(htmlspecialchars_uni($_POST["descr"]));

if (empty($descr))
stderr($tracker_lang['error'], $tracker_lang['no_upload_desc']);

$catid = (int) $_POST["type"];
if (!is_valid_id($catid))
stderr($tracker_lang['error'], $tracker_lang['no_select_cat']);

$torrent = htmlspecialchars_uni($_POST["name"]);
$tmpname = $f["tmp_name"];
if (empty($torrent) && !empty($matches[0]))
$torrent = htmlspecialchars_uni($matches[0]);
if (empty($torrent) && !empty($matches[0]))
$torrent = $tmpname;


/////////////////////////// ������� ���� �����, ������ � ������� ������ � ����������� � �������� 
$stop_dfiles = stop_dfiles($torrent);
if ($stop_dfiles <> false)
stderr($tracker_lang['error']." / <a href=\"uploadnext.php?type=".$catid."\"><b>".$tracker_lang['back_inlink']."</b></a>", sprintf($tracker_lang['stop_dfiles_warn'], "<strong>".$torrent."</strong>", "<strong>#".$stop_dfiles['id']."</strong>, <strong>".$stop_dfiles['key_in']."</strong>", $stop_dfiles['number']));
/////////////////////////// ������� ���� �����, ������ � ������� ������ � ����������� � �������� 



if (!is_uploaded_file($tmpname) || !filesize($tmpname))
stderr($tracker_lang['error'], $tracker_lang['dox_cannot_move']);

$dict = bdec_file($tmpname, $Torrent_Config["max_torrent_size"]);
if (!isset($dict))
stderr($tracker_lang['error'], $tracker_lang['no_binary_torrent']);

if ($_POST['free'] == 'yes' && get_user_class() >= UC_MODERATOR)
$free = "yes";
else
$free = "no";

if ($_POST['sticky'] == 'yes' &&  get_user_class() >= UC_ADMINISTRATOR)
$sticky = "yes";
else
$sticky = "no";

list ($info) = dict_check($dict, "info");
list ($dname, $plen, $pieces) = dict_check($info, "name(string):piece length(integer):pieces(string)");

if (($dict['value']['created by']['strlen']) > 128) // 30 or any number < 512
stderr($tracker_lang['error'], "created by field too long");

$filelist = array();
$totallen = dict_get($info, "length", "integer");
if (isset($totallen))
$filelist[] = array($dname, $totallen);
else {

$flist = dict_get($info, "files", "list");
if (!isset($flist))
stderr($tracker_lang['error'], "missing both length and files");

if (!count($flist))
stderr($tracker_lang['error'], "no files");

$totallen = 0;
foreach ($flist as $fn) {
list ($ll, $ff) = dict_check($fn, "length(integer):path(list)");
$totallen += $ll;

$ffa = array();
foreach ($ff as $ffe) {
if ($ffe["type"] != "string")
stderr($tracker_lang['error'], "filename error");
$ffa[] = $ffe["value"];
}

if (!count($ffa))
stderr($tracker_lang['error'], "filename error");

$ffe = implode("/", $ffa);
$filelist[] = array($ffe, $ll);

/*
if ($ffe == 'Thumbs.db')
stderr($tracker_lang['error'], "� ��������� ��������� ������� ����� Thumbs.db!");
*/
}

}

$visi = "yes";

if (!empty($dict['value']['info']['value']['private']['value'])){

$dict['value']['announce'] = bdec(benc_str($announce_urls[0]));  // change announce url to local

$dict['value']['info']['value']['source'] = bdec(benc_str("[".$DEFAULTBASEURL."] ".$SITENAME)); // add link for bitcomet users
unset($dict['value']['announce-list']); /// remove multi-tracker capability
unset($dict['value']['nodes']); /// remove cached peers (Bitcomet & Azareus)
unset($dict['value']['info']['value']['crc32']); /// remove crc32
unset($dict['value']['info']['value']['ed2k']); /// remove ed2k
unset($dict['value']['info']['value']['md5sum']); /// remove md5sum
unset($dict['value']['info']['value']['sha1']); /// remove sha1
unset($dict['value']['info']['value']['tiger']); /// remove tiger
unset($dict['value']['azureus_properties']); /// remove azureus properties
}

if (!empty($dict['value']['info']['value']['private']['value']))
$dict['value']['info']['value']['private'] = bdec('i1e'); /// Privat
//else
//$dict['value']['info']['value']['private'] = bdec('i0e'); /// ������ dht


$dict = bdec(benc($dict)); // double up on the becoding solves the occassional misgenerated infohash

$ret = mysql_query("SHOW TABLE STATUS LIKE 'torrents'");
$row = mysql_fetch_array($ret);
$next_id = $row['Auto_increment'];

if (!empty($dict['value']['info']['value']['private']['value'])){
$visi = "no";
$dict['value']['comment'] = bdec(benc_str($DEFAULTBASEURL."/details.php?id=".$id)); // change torrent comment to URL
$dict['value']['created by'] = bdec(benc_str($CURUSER["username"])); // change created by
$dict['value']['publisher'] = bdec(benc_str($CURUSER["username"])); // change publisher
$dict['value']['publisher.utf-8'] = bdec(benc_str($CURUSER["username"])); // change publisher.utf-8
$dict['value']['publisher-url'] = bdec(benc_str($DEFAULTBASEURL."/userdetails.php?id=".$CURUSER["id"])); // change publisher-url
$dict['value']['publisher-url.utf-8'] = bdec(benc_str($DEFAULTBASEURL."/userdetails.php?id=".$CURUSER["id"])); // change publisher-url.utf-8
}

list ($info) = dict_check($dict, "info");
$infohash = sha1($info["string"]);


//////////////////////////// ��������� ������� - ������ �������
$num_license = get_row_count("license", "WHERE info_hash = ".sqlesc($infohash));
if (!empty($num_license))
stderr($tracker_lang['error'], $tracker_lang['file_copyright']);
//////////////////////////// ��������� ������� - ������ �������



///
$image0_inet = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", $_POST['image0_inet'])); /// ����� �� ������ [] � ��
$image0_inet = htmlentities($image0_inet); ///������� ������ �� ������

///��������� �������� �������
if (!empty($_FILES['image0']['name']))
$inames = uploadimage("image0", '', $next_id);

elseif (!empty($image0_inet) && empty($_FILES['image0']['name']) && preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $image0_inet)){

list ($width, $height) = getimagesize($image0_inet);

/// �������� ���������� http ������
if (!empty($width) && !empty($height)){

$image = @file_get_contents($image0_inet); /// �������� ��� �������� ��� �������� ��

if (empty($image))
stderr($tracker_lang['error'], $tracker_lang['image_empty'].": ".$image0_inet);

if (strlen($image) > $Torrent_Config["max_image_size"])
stderr($tracker_lang['error'], sprintf($tracker_lang['image_isbig'], mksize($Torrent_Config["max_image_size"])));

$caimage = imageshack($image0_inet);

if ($caimage == false || empty($caimage)) $caimage = $image0_inet;

if ($caimage <> false)
$inames = $caimage;

}
}



if ($totallen >= "3221225472" && $Torrent_Config["free_from_3GB"] == true) {
$free = "yes";
$torrent_com = get_date_time() . " ������ �� 3 �����.\n";
}

$ori_web = strip_tags($_POST["webseed"]);

if (get_user_class() >= UC_MODERATOR && strlen($ori_web) > 15 && preg_match("/^(http(s)?:\/\/)(([^\/]+\.)+)\w{2,}(\/)?.*$/i", $ori_web))
$webseed_link = ($ori_web);

$multut = (!empty($dict['value']['info']['value']['private']['value']) ? "no" : "yes");

// Replace punctuation characters with spaces
$torrent = htmlspecialchars(str_replace("_", " ", $torrent));

$ret = sql_query("INSERT INTO torrents (owner, visible, sticky, info_hash, name, size, numfiles, tags, descr, torrent_com, free, image1, category, added, last_action, webseed, multitracker) VALUES (" . implode(",", array_map("sqlesc", array($CURUSER["id"], $visi, $sticky, $infohash, $torrent, $totallen, count($filelist), $tags, $descr, $torrent_com, $free, $inames, $catid, get_date_time(), get_date_time(), $webseed_link, $multut))).")");

if (empty($ret)) {

if (mysql_errno() == 1062){

$res = sql_query("SELECT name, id FROM torrents WHERE info_hash = ".sqlesc($infohash))or sqlerr(__FILE__,__LINE__);
while ($arr = mysql_fetch_assoc($res))
stderr($tracker_lang['error'], $tracker_lang['dybl_torrent']."<br /><a href=\"details.php?id=".$arr["id"]."\">".$arr["name"]."</a>");

stderr($tracker_lang['error'], "mysql puked: ".mysql_error());
}
}

$id = mysql_insert_id();

sql_query("INSERT INTO checkcomm (checkid, userid, torrent) VALUES (".sqlesc($id).", ".sqlesc($CURUSER["id"]).", 1)") or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM files WHERE torrent = ".sqlesc($id));

foreach ($filelist as $file) {
sql_query("INSERT INTO files (torrent, filename, size) VALUES (".sqlesc($id).", ".sqlesc(utf8_to_win($file[0])).", ".sqlesc($file[1]).")");
}

if (count($pictureset))
sql_query("UPDATE torrents SET ".implode(",", $pictureset)." WHERE id = ".sqlesc($id)); /// ��������

move_uploaded_file($tmpname, "torrents/".$id.".torrent");

$fp = fopen("torrents/".$id.".torrent", "w");
if ($fp){
@fwrite($fp, benc($dict), strlen(benc($dict)));
@fclose($fp);
}



$updateset = array();

/// �������������� ������ ��������
if (!empty($_POST["array_picture"])){

$array_picture = explode("\r", $_POST["array_picture"]);

$xpi = 1;
foreach ($array_picture AS $pic) {

if ($xpi < 5 && !empty($pic)){
$_POST['picture'.$xpi] = trim($pic);
++$xpi;
}

}

}


/// picturemod array for Tesla TT
if ($Functs_Patch["take_pic"] <> 1){

for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

$picture_1 = strip_tags(preg_replace("/\[((\s|.)+?)\]/is", "", $row["picture".$xpi_s])); /// ����� �� ������ [] � ��
$picture_1 = htmlentities($picture_1); ///������� ������ �� ������

///��������� �������� �������
if (!empty($_FILES['picture_'.$xpi_s]['name'])) {

$updateset[] = "picture".$xpi_s." = " .sqlesc(uploadimage("picture_".$xpi_s, '', $id, "_".$xpi_s));

unset($_POST["picture".$xpi_s]);
}

}
}

if ($Functs_Patch["take_pic"] <> 2){

for ($xpi_s=1; $xpi_s<5; $xpi_s++) {

$pic = $_POST['picture'.$xpi_s];
$pic = htmlentities($pic);

/// sendpic � funkyimg.com
if (preg_match('#^((http)|(ftp):\/\/[a-zA-Z0-9\-]+?\.([a-zA-Z0-9\-]+\.)+[a-zA-Z]+(:[0-9]+)*\/.*?\.(gif|jpg|jpeg|png)$)#is', $pic) && !stristr($pic,"sendpic.ru") && !stristr($pic,"funkyimg.com") && empty($_FILES['picture_'.$xpi_s]['name'])){

if (list($width, $height) = @getimagesize($pic))
$updateset[] = "picture".$xpi_s." = ".sqlesc($pic);

}

}
}

if (count($updateset))
sql_query("UPDATE torrents SET ".implode(", ", $updateset)." WHERE id = ".sqlesc($id)) or sqlerr(__FILE__, __LINE__);

/// picturemod array for Tesla TT


if (get_user_class() >= UC_MODERATOR) {

////// ��� ����� //////
$ret = array();
$ptype = (int) $_POST["type"];
$res = sql_query("SELECT name FROM tags WHERE category = ".sqlesc($ptype)) or sqlerr(__FILE__, __LINE__);
while ($row = mysql_fetch_array($res))
$ret[] = tolower($row["name"]);

$union = array_intersect($ret, explode(",", $tags));
$ununion = array_diff(explode(",", $tags), $ret);

/// ����������� � ������� ������� ������ ������ - ��� ����������� ����������� ����� (����� �����) ucwords
$tag = tolower($tag);

foreach ($union as $tag) {
sql_query("UPDATE tags SET howmuch = howmuch+1 WHERE name = ".sqlesc($tag)) or sqlerr(__FILE__, __LINE__);
}

foreach ($ununion as $tag) {
sql_query("INSERT INTO tags (category, name, howmuch, added) VALUES (".sqlesc($ptype).", ".sqlesc($tag).", 1, ".sqlesc(get_date_time()).")");
}
////// ��� ����� //////

}

/// ���� ������� ����� �������������� �� ��������� � �������� �� ��.
if (get_user_class() >= UC_MODERATOR){
$updateset[] = "moderated = 'yes'";
$updateset[] = "moderatedby = ".sqlesc($CURUSER["id"]);
$updateset[] = "moderatordate = ".sqlesc(get_date_time());

sql_query("UPDATE torrents SET " . implode(",", $updateset) . " WHERE id = ".sqlesc($id)) or sqlerr(__FILE__,__LINE__);
}


$user = $CURUSER["username"];

write_log("������� ����� $id ($torrent) ��� ����� $user\n", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]), "torrent");

unsql_cache("block-last_files"); /// ��������� �������
unsql_cache("block_showrealese_mulcache"); /// ��������� �������
unsql_cache("block_showrealese_topc"); /// ��� ������
unsql_cache("showRealese_search"); /// ��� ���������
unsql_cache("showRealese_last"); /// ��� ���������
unsql_cache("block-showRealese"); /// �������
unsql_cache("block_3dview_0"); /// ������� ��� ���� 3view

unsql_cache("block_cloudstag"); /// ������� ��� ����� �� browse
unsql_cache("premod"); /// ��� ����������� ��������, ������� ��� ����� ���������� ���
unsql_cache("count_torrent"); /// ������� ��� ��� ���������
unsql_cache("taggenrelist_".$ptype); /// ������� ��� ����� �������������� �������

if (!empty($CURUSER["groups"])){
unsql_cache("group_data_".$CURUSER["groups"]); /// ������� ��� ����� �� browse
unsql_cache("traff_".$CURUSER["groups"]);
unsql_cache("browse_gr_array");
unsql_cache("details_groups-".$CURUSER["groups"]);
unsql_cache("block-groups"); /// ���� ������
}

// ������� ����� ���� ������ �������� ��� 0
if ($Torrent_Config["auto_tags"] == true){
sql_query("DELETE FROM tags WHERE name = ''") or sqlerr(__FILE__,__LINE__);
sql_query("DELETE FROM tags WHERE howmuch = 0") or sqlerr(__FILE__,__LINE__);
}


direct_unmatch ($id, $torrent, $catid); /// ������� �������� ������ ���� � ������� ��������, ��� ��� �������

/// ���� �� ������ � ���� ������ ��� ����� ������� - �������������� ���� ��� ������� 
require_once("include/functions_bot.php"); // ���������� ������� ����
bot_newrelease($id, $torrent); /// � ��� ��� ������� � ����� �������
/// ���� �� ������ � ���� ������ ��� ����� ������� ����������������� - �������������� ���� ��� ������� 



header("Location: ".$DEFAULTBASEURL."/details.php?id=".$id);
die;

?>