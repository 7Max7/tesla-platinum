<?
require "include/bittorrent.php";
gzip();
dbconn(false);

if (get_user_class() > UC_MODERATOR && isset($_POST["delmp"]) && is_array($_POST["delmp"])) {

$link = "humorall.php?page=".(int) $_GET["page"];

sql_query("DELETE FROM humor WHERE id IN (" . implode(", ", array_map("intval", $_POST["delmp"])).")") or sqlerr(__FILE__, __LINE__);

sql_query("DELETE FROM karma WHERE type = 'humor' AND value IN (".implode(", ", array_map("intval", $_POST["delmp"])).")") or sqlerr(__FILE__, __LINE__);

@header("Location: ".$DEFAULTBASEURL."/".$link) or die("<script>setTimeout('document.location.href=\"".$DEFAULTBASEURL."/".$link."\"', 10);</script>");
}

if (!empty($_GET["tu"])){

$where = "h.karma";
$wheretu = "?tu=untop&";
$descsort = "<a href=\"humorall.php\">".$tracker_lang['ratio']."</a>";

} else {

$where = "h.id";
$wheretu = "?";
$descsort = "<a href=\"humorall.php?tu=untop\">".$tracker_lang['added']."</a>";

}


$count = get_row_count("humor");
$perpage = 60;

list ($pagertop, $pagerbottom, $limit) = pager($perpage, $count, "humorall.php".$wheretu);

stdhead($tracker_lang['jokes'], true);

$res = sql_query("SELECT h.*, u.class, u.username ".($CURUSER? ",(SELECT COUNT(*) FROM karma WHERE type = 'humor' AND value = h.id AND user = ".$CURUSER["id"].") AS canrate":"")." FROM humor AS h LEFT JOIN users AS u ON h.uid = u.id ORDER BY ".$where." DESC ".$limit) or sqlerr(__FILE__, __LINE__);

echo ("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">\n");

echo "<tr><td class=\"b\" align=\"center\" colspan=\"4\">".$pagertop."</td></tr>";

if (get_user_class() > UC_MODERATOR)
echo "<form method=\"post\" action=\"humorall.php\" name=\"form1\">";

echo ("<tr>
".(get_user_class() > UC_MODERATOR ? "<td class=\"a\" align=\"center\"><input type=\"checkbox\" title=\"".$tracker_lang['mark_all']."\" value=\"".$tracker_lang['mark_all']."\" onclick=\"this.value=check(document.form1.elements);\"></td>":"<td class=\"a\">#</td>")."
<td class=\"a\" align=\"left\">".$tracker_lang['description']." / ".$tracker_lang['sorting'].": ".$descsort."</td>
<td class=\"a\" align=\"center\">".$tracker_lang['clock']."</td>
<td class=\"a\" align=\"center\">".$tracker_lang['added']."</td>
</tr>\n");

echo '<script language="JavaScript" type="text/javascript">
function karma(id, type, act) {
jQuery.post("karma.php",{"id":id,"act":act,"type":type},function (response) {
jQuery("#karma" + id).empty();
jQuery("#karma" + id).append(response);
});
}
</script>';

if (get_user_class() > UC_MODERATOR)
echo '<script language="Javascript" type="text/javascript">
var checkflag = "false";
var marked_row = new Array;
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
} else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
}
}
</script>';


while ($arr = mysql_fetch_assoc($res)) {

if (empty($arr["username"]))
$sender = "<font color=\"red\">[<b>id ".$arr["uid"]."</b>]</font> <br />".$tracker_lang['anonymous']."";
else
$sender = "<a href=\"userdetails.php?id=".$arr["uid"]."\"><b>".get_user_class_color($arr["class"], $arr["username"])."</b></a>";

if ($CURUSER){
if ($arr["canrate"] > 0)
$karma2 = "<span><img src=\"pic/minus-dis.png\" title=\"".$tracker_lang['is_nolike']."\" alt=\"\" /> ".karma($arr["karma"])." <img src=\"pic/plus-dis.png\" title=\"".$tracker_lang['is_like']."\" alt=\"\" /></span>\n";
else
$karma2 = "<span id=\"karma".$arr["id"]."\"><img src=\"pic/minus.png\" style=\"cursor:pointer;\" title=\"".$tracker_lang['is_nolike']."\" alt=\"\" onclick=\"javascript: karma('".$arr["id"]."', 'humor', 'minus');\" /> ".karma($arr["karma"])." <img src=\"pic/plus.png\" style=\"cursor:pointer;\" onclick=\"javascript: karma('".$arr["id"]."', 'humor', 'plus');\" title=\"".$tracker_lang['is_like']."\" alt=\"\" /></span>\n";
}

echo "<tr>";

if (get_user_class() > UC_MODERATOR)
echo ("<td class=\"a\" align=\"center\" valign=\"top\"><INPUT type=\"checkbox\" name=\"delmp[]\" value=\"".$arr['id']."\"><br /><br /><a href=\"humor.php?id=".$arr['id']."\" title=\"".$tracker_lang['stable_link']."\">".$arr['id']."</a></td>\n");
else
echo ("<td class=\"b\" align=\"center\" valign=\"top\"><a href=\"humor.php?id=".$arr['id']."\" title=\"".$tracker_lang['stable_link']."\">".$arr['id']."</a></td>");


echo ("<td class=\"b\">".htmlspecialchars_uni($arr["txt"])."
<div style=\"text-align:right;\" id=\"karma\">".$karma2."</div>
</td>

<td class=\"b\" align=\"center\">".$arr["date"]."
<div style=\"text-align:center;\">
<a href=\"humor.php?id=".$arr['id']."\"><img style=\"border:none\" title=\"".$tracker_lang['more']."\" src=\"pic/mail-markread.gif\"></a> ".(get_user_class() >= UC_MODERATOR ? "<a title=\"".$tracker_lang['edit']."\" href=\"humor.php?id=".$arr['id']."&do=edit\"><img style=\"border:none\" src=\"pic/mail-create.gif\"></a> <a title=\"".$tracker_lang['delete']."\" href=\"humor.php?id=".$arr['id']."&do=delete\"><img style=\"border:none\" src=\"pic/delete.gif\"></a>":"")."
</div>
</td>
<td class=\"b\" align=\"center\" valign=\"top\">".$sender." </td>");

}

echo ("</tr>");

if (get_user_class() > UC_MODERATOR)
echo "<tr><td class=\"b\" align=\"left\" colspan=\"4\"><input type=\"submit\" class=\"btn\" value=\"".$tracker_lang['delete']."\"/></td></tr>";

echo "<tr><td class=\"b\" align=\"center\" colspan=\"4\">".$pagerbottom."</td></tr>";

echo "</table>";

if (get_user_class() > UC_MODERATOR)
echo "</form>";

echo "<br />";

stdfoot(true);
?>