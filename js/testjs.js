$(document).ready( function() {
   $("body").prepend(getTip());
   $('input, span, img, font, �, div').betterTooltip( {
      speed : 100,
      delay : 5000
   });
});
getTip = function(id) {
   if (id)
      toadd = 'tip' + id;
   else
      toadd = 'tip';
   var tTip = "<div class='"
         + toadd
         + "' style='background: transparent; border: 0px;' width='500'>"
         + "<div style='background: transparent; border: 0px;'>"
         + "<table width='auto' style='background: transparent; border: 0px;' cellspacing=0 cellpadding=0>"
         + "<tr><td>"
         + "<table width='100%' style='background: transparent; border: 0px;' cellspacing=0 cellpadding=0>"
         + "<tr><td width='12px'><div class='tipTopLeft'></div></td><td class='tipTop' width='100%'></td><td width='12px'><div class='tipTopRight'></div></td></tr>"
         + "</table>"
         + "</td></tr><tr><td>"
         + "<table width='auto' style='background: transparent; border: 0px;' cellspacing=0 cellpadding=0>"
         + "<tr><td class='tipMidLeft' width='12px'></td><td width='auto'><div class='tipMid'></div></td><td class='tipMidRight' width='12px'></td></tr>"
         + "</table>"
         + "</td></tr><tr><td>"
         + "<table width='100%' style='background: transparent; border: 0px;' cellspacing=0 cellpadding=0>"
         + "<tr><td width='71px'><div class='tipBtmLeft' ></div></td><td class='tipBtm' width='100%'></td><td width='12px'><div class='tipBtmRight'></div></td></tr>"
         + "</table>"
         + "</td></tr><tr><td>"
         + "<table style='background: transparent; border: 0px;' cellspacing=0 cellpadding=0>"
         + "<tr><td class='tipBtmArrow'></td></tr>"
         + "</table>"
         + "</td><td>"
         + "</table>"
         +"</div>" + "</div>";
   return tTip;
}
function str_replace(search, replace, subject, count) {
   var i = 0, j = 0, temp = '', repl = '', sl = 0, fl = 0, f = []
         .concat(search), r = [].concat(replace), s = subject, ra = r instanceof Array, sa = s instanceof Array;
   s = [].concat(s);
   if (count) {
      this.window[count] = 0;
   }

   for (i = 0, sl = s.length; i < sl; i++) {
      if (s[i] === '') {
         continue;
      }
      for (j = 0, fl = f.length; j < fl; j++) {
         temp = s[i] + '';
         repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
         s[i] = (temp).split(f[j]).join(repl);
         if (count && s[i] !== temp) {
            this.window[count] += (temp.length - s[i].length) / f[j].length;
         }
      }
   }
   return sa ? s : s[0];
}

function htmlspecialchars(text) {
   var chars = Array("&", "<", ">", "\"", "'");
   var replacements = Array("&amp;", "&lt;", "&gt;", "&quot;", "&#039;");
   for ( var i = 0; i < chars.length; i++) {
      text = str_replace(chars[i], replacements[i], text);
   }
   return text;
}
$.fn.betterTooltip = function(options) {
   var defaults = {
      speed : 100,
      delay : 5000
   };
   var options = $.extend(defaults, options);
   var tip = $('.tip');
   var tipInner = $('.tip .tipMid');
   $this_all = $(this);
   $(this).each(
         function() {
            var $this = $(this);
            if (!$this.attr("tooltip")) {
               var title = htmlspecialchars(($this.attr("title")
                     ? $this
                     .attr("title") :  ""));
               if (title)
                  $this.attr("tooltip", title);
               else
                  return;
            }
            $this.attr("alt", "");
            $this.attr("title", "");
            $this.hover( function() {
               var tTitle = $this.attr("tooltip");
               var tWidth = $this.width();
               var tHeight = $this.height();
               var offset = $this.offset();
               var tLeft = offset.left - 58;
               var tTop = offset.top;
               tipInner.html(tTitle);
               setTip(tTop, tLeft, $this);
               showTip();
            }, function() {
               tip.stop(false, true);
               tip.hide();
            });
            setTip = function(top, left, $this) {
               var topOffset = tip.height();
               var leftOffset = $this.width() / 2;
               var xTip = (left + leftOffset) + "px";
               var yTip = (top - topOffset) + "px";
               tip.css( {
                  'top' : yTip,
                  'left' : xTip
               });
            };
            showTip = function() {
               tip.fadeIn(defaults.speed);
            };
         });
};

$.fn.startTooltip = function(options) {
   var defaults = {
      speed : 10000,
      delay : 5000
   };
   var options = $.extend(defaults, options);
   if (!$('.tip' + $(this).attr("id")).text())
      $("body").prepend(getTip($(this).attr("id")));
   var new_tip = $('.tip' + $(this).attr("id"));
   var new_tipInner = $('.tip' + $(this).attr("id") + ' .tipMid');
   new_tip.css( {
      "overflow" : "hidden",
      "display" : "none",
      "position" : "absolute",
      "z-index" : "500",
      "border" : "0px",
      "background" : "transparent"
   });
   new_setTip = function(top, left) {
      var topOffset = new_tip.height();
      var xTip = (left) + "px";
      var yTip = (top - topOffset) + "px";
      new_tip.css( {
         'top' : yTip,
         'left' : xTip
      });
      new_tip.fadeIn(defaults.speed);
   };
   var $this = $(this);
   var offset = $this.offset();
   var tLeft = offset.left;
   var tTop = offset.top;
   var tWidth = $this.width();
   var tHeight = $this.height();
   var tTitle = ($this.attr("title"));
   $this.attr("title", "");
   new_tipInner.html(tTitle);
   new_setTip(tTop, tLeft, new_tip);
   new_tip.show();
};
$.fn.updateTooltip = function(what) {
   new_setTip = function(top, left) {
      var topOffset = new_tip.height();
      var xTip = (left) + "px";
      var yTip = (top - topOffset) + "px";
      new_tip.css( {
         'top' : yTip,
         'left' : xTip
      });
   };
   var new_tip = $('.tip' + $(this).attr("id"));
   var new_tipInner = $('.tip' + $(this).attr("id") + ' .tipMid');
   new_tipInner.html(what);
   var $this = $(this);
   var offset = $this.offset();
   var tLeft = offset.left;
   var tTop = offset.top;
   new_setTip(tTop, tLeft, new_tip);
};
$.fn.stopTooltip = function() {
   var new_tip = $('.tip' + $(this).attr("id"));
   new_tip.hide();
};

<!---http://kinotracker.ru/referal.php---!>