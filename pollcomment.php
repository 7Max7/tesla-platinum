<?
require_once("include/bittorrent.php");
dbconn(false);

loggedinorreturn();
parked();

if ($CURUSER["commentpos"] == 'no')
stderr($tracker_lang['error_data'], $tracker_lang['commentpos_no']);

$action = (string) $_GET["action"];

if ($action == "add") {

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$pid = (int) $_POST["pid"];
if (!is_valid_id($pid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);


$ss = sql_query("SELECT comment FROM polls WHERE forum = '0' AND id = ".sqlesc($pid)) or sqlerr(__FILE__,__LINE__);
$su = mysql_fetch_array($ss);
if ($su["comment"] == "no" && get_user_class() < UC_ADMINISTRATOR){
stderr($tracker_lang['error'], $tracker_lang['poll_comnot']);
die();
}

$text = htmlspecialchars_uni($_POST["text"]);
if (empty($text))
stderr($tracker_lang['error'], $tracker_lang['comment_cant_be_empty']);

sql_query("INSERT INTO comments (user, poll, added, text, ip) VALUES (".sqlesc($CURUSER["id"]).", ".sqlesc($pid).", ".sqlesc(get_date_time()).", ".sqlesc($text).", ".sqlesc(getip()).")") or sqlerr(__FILE__, __LINE__);

$newid = mysql_insert_id();

unsql_cache("poll_comment_".$newid); // �����������
unsql_cache("polls_".$newid); // ������ ������

header("Refresh: 0; url=polloverview.php?id=".$pid."&viewcomm=".$newid."#comm".$newid);
die;
}

$pid = (int)$_GET["pid"];
if (!is_valid_id($pid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);


stdhead($tracker_lang['polls'].": ".$tracker_lang['commentpos']);

begin_frame($tracker_lang['commentpos'], true);

echo "<form name=\"comment\" method=\"post\" action=\"pollcomment.php?action=add\">";
echo "<table class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";

echo "<tr><td class=\"colhead\">".$tracker_lang['add_comment']."</td></tr>";
echo "<tr><td align=\"center\">".textbbcode("comment", "text", "")."
<input type=\"hidden\" name=\"pid\" value=\"".$pid."\"/>
<input class=\"btn\" type=\"submit\" value=\"".$tracker_lang['add_comment']."\" /></td></tr>";

echo "</table>";
echo "</form>";

$res = sql_query("SELECT comments.id, text, comments.ip, comments.added, username, title, class, users.id as user, users.avatar, users.donor, users.enabled, users.warned, users.parked 
FROM comments
LEFT JOIN users ON comments.user = users.id
WHERE torrent = '0' AND news = '0' AND offer = '0' AND poll = ".sqlesc($pid)." ORDER BY comments.id DESC") or sqlerr(__FILE__, __LINE__);

$allrows = array();
while ($row = mysql_fetch_array($res))
$allrows[] = $row;

if (count($allrows))
echo commenttable($allrows, "pollcomment");

stdfoot();
die;

} elseif ($action == "quote") {

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT pc.*, p.id AS pid, u.username 
FROM comments AS pc
LEFT JOIN polls AS p ON pc.poll = p.id AND p.forum = '0'
JOIN users AS u ON pc.user = u.id
WHERE torrent = '0' AND news = '0' AND offer = '0' AND pc.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);

$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['no_poll_with_such_id']);

$ss = sql_query("SELECT comment FROM polls WHERE forum = '0' AND id = ".sqlesc($arr["pid"])) or sqlerr(__FILE__,__LINE__);
$su = mysql_fetch_array($ss);
if ($su["comment"] == "no" && get_user_class() < UC_ADMINISTRATOR){
stderr($tracker_lang['error'], $tracker_lang['poll_comnot']);
die();
}

stdhead($tracker_lang['polls'].": ".$tracker_lang['commentpos']);

begin_frame($tracker_lang['commentpos'], true);

$text = "[quote=".$arr["username"]."]".$arr["text"]."[/quote]\n";

echo("<form method=\"post\" name=\"comment\" action=\"pollcomment.php?action=add\">\n");
echo "<table class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";
echo("<input type=\"hidden\" name=\"pid\" value=\"".$arr["pid"]."\" />\n");

echo "<tr><td class=\"colhead\">".$tracker_lang['quote_message'].": ".$arr["username"]."</td></tr>";
echo "<tr><td align=\"center\">
".textbbcode("comment", "text", htmlspecialchars_uni($text))."
<input type=\"submit\" class=\"btn\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['add_comment']."\" value=\"".$tracker_lang['add_comment']."\" /></td></tr>";

echo "</table>";
echo "</form>";

stdfoot();
die;

} elseif ($action == "edit") {

$commentid = (int) $_GET["cid"];
if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT pc.*, p.id AS pid 
FROM comments AS pc
LEFT JOIN polls AS p
ON pc.poll = p.id AND p.forum = '0'
WHERE torrent = '0' AND news = '0' AND offer = '0' AND pc.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);

$arr = mysql_fetch_array($res);
if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['no_poll_with_such_id']);

if ($arr["user"] <> $CURUSER["id"] && get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$ss = sql_query("SELECT comment FROM polls WHERE forum = '0' AND id = ".sqlesc($arr["pid"])) or sqlerr(__FILE__,__LINE__);
$su = mysql_fetch_array($ss);

if ($su["comment"] == "no" && get_user_class() < UC_ADMINISTRATOR){
stderr($tracker_lang['error'], $tracker_lang['poll_comnot']);
die();
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {

$text = htmlspecialchars_uni($_POST["text"]);
$returnto = htmlentities($_POST["returnto"]);

if (empty($text))
stderr($tracker_lang['error'], $tracker_lang['comment_cant_be_empty']);

$ara_upd = array();

if (empty($arr["ori_text"]))
$ara_upd[] = "ori_text = ".sqlesc($arr["text"]);

$ara_upd[] = "text = ".sqlesc($text);
$ara_upd[] = "editedby = ".sqlesc($CURUSER["id"]);
$ara_upd[] = "editedat = ".sqlesc(get_date_time());

sql_query("UPDATE comments SET ".implode(", ", $ara_upd)." WHERE torrent = '0' AND news = '0' AND offer = '0' AND id = ".sqlesc($commentid)) or sqlerr(__FILE__, __LINE__);

unsql_cache("poll_comment_".$commentid); // �����������

if ($returnto)
header("Location: ".$returnto);
else
header("Location: ".$DEFAULTBASEURL);

die;
}

stdhead($tracker_lang['polls'].": ".$tracker_lang['editing']);

begin_frame($tracker_lang['editing'], true);

echo "<form method=\"post\" name=\"comment\" action=\"pollcomment.php?action=edit&cid=".$commentid."\">";
echo "<table class=\"main\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" width=\"100%\">";

echo "<tr><td align=\"center\">
".textbbcode("comment", "text", htmlspecialchars_uni($arr["text"]))."
<input type=\"hidden\" name=\"cid\" value=\"".$commentid."\" />
<input type=\"hidden\" name=\"returnto\" value=\"polloverview.php?id=".$arr["pid"]."&viewcomm=".$commentid."#comm".$commentid."\" />
<input type=\"submit\" class=\"btn\" name=\"post\" title=\"CTRL+ENTER ".$tracker_lang['edit']."\" value=\"".$tracker_lang['edit']."\" />
</td></tr>";

echo "</table>";

echo "</form>";

stdfoot();
die;


} elseif ($action == "delete") {

if (get_user_class() < UC_MODERATOR)
stderr($tracker_lang['error'], $tracker_lang['access_denied']);

$commentid = (int) $_GET["cid"];

if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

if (!isset($_GET["sure"]) || empty($_GET["sure"]))
stderr($tracker_lang['delete']." ".$tracker_lang['comment'], sprintf($tracker_lang['you_want_to_delete_x_click_here'], $tracker_lang['comment'], "?action=delete&cid=".$commentid."&sure=1"));

$res = sql_query("SELECT poll, user, (SELECT username FROM users WHERE id=comments.user) AS classusername, (SELECT question FROM polls WHERE forum='0' AND id=comments.poll) AS name_poll
FROM comments WHERE torrent = '0' AND news = '0' AND offer = '0' AND id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);

if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['no_poll_with_such_id']);

if (!empty($arr))
$pid = $arr["poll"];

$ss = sql_query("SELECT comment FROM polls WHERE forum = '0' AND id = ".sqlesc($pid)) or sqlerr(__FILE__,__LINE__);
$su = mysql_fetch_array($ss);
if ($su["comment"] == "no" && get_user_class() < UC_ADMINISTRATOR){
stderr($tracker_lang['error'], $tracker_lang['poll_comnot']);
die();
}

sql_query("DELETE FROM comments WHERE torrent = '0' AND news = '0' AND offer = '0' AND id = ".sqlesc($commentid));

unsql_cache("poll_comment_".$pid); // �����������
unsql_cache("polls_".$pid); // ������ ������

write_log($CURUSER["username"]." ������ ����������� ".$arr["classusername"]." (".$commentid.") � ������ '".$arr["name_poll"]."' (".$pid.")", get_user_rgbcolor($CURUSER["class"], $CURUSER["username"]),"comment");

list($commentid) = mysql_fetch_row(sql_query("SELECT id FROM comments WHERE torrent = '0' AND news = '0' AND offer = '0' AND poll = ".sqlesc($pid)." ORDER BY added DESC LIMIT 1"));

header("Location: polloverview.php?id=".$pid.(empty($commentid) ? "":"&viewcomm=".$commentid."#comm".$commentid));
die;

} elseif ($action == "vieworiginal" && get_user_class() > UC_MODERATOR) {

$commentid = (int) $_GET["cid"];

if (!is_valid_id($commentid))
stderr($tracker_lang['error'], $tracker_lang['invalid_id']);

$res = sql_query("SELECT pc.*, p.id AS pid 
FROM comments AS pc
LEFT JOIN polls AS p ON pc.poll = p.id AND p.forum = '0'
WHERE torrent = '0' AND news = '0' AND offer = '0' AND pc.id = ".sqlesc($commentid)) or sqlerr(__FILE__,__LINE__);
$arr = mysql_fetch_array($res);

if (!$arr)
stderr($tracker_lang['error'], $tracker_lang['no_poll_with_such_id']);

$ss = sql_query("SELECT comment FROM polls WHERE forum = '0' AND id = ".sqlesc($arr["pid"])) or sqlerr(__FILE__,__LINE__);
$su = mysql_fetch_array($ss);
if ($su["comment"] == "no" && get_user_class() < UC_ADMINISTRATOR){
stderr($tracker_lang['error'], $tracker_lang['poll_comnot']);
die();
}

if (empty($arr["ori_text"]))
$arr["ori_text"] = $arr["text"];

stdhead($tracker_lang['polls'].": ".$tracker_lang['message_view']);

echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

echo "<tr><td class=\"a\">".$tracker_lang['original_com'].": ".$commentid."</td></tr>";
echo "<tr><td class=\"b\">".format_comment($arr["ori_text"],true)."</td></tr>";
echo "<tr><td class=\"a\"><a href=\"polloverview.php?id=".$arr["pid"]."&viewcomm=".$commentid."#comm".$commentid."\">".$tracker_lang['back_inlink']."</a></td></tr>";

echo "</table>";

stdfoot();
die;

} else
stderr($tracker_lang['error'], $tracker_lang['no_data']);

?>